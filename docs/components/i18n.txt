=========================
-- I18N CLASS EXAMPLES --
=========================

// Check if application is MULTILANGUAGE
Yii::isMultilanguage();
Yii::app()->i18n->is_multilanguage();

// Return DEFAULT language code. Example: 'en'
Yii::defaultLanguage();
Yii::app()->i18n->get_default_language();

// Current language
Yii::currentLanguage();
Yii::app()->i18n->get_current_language();

// Return EXTRA supported language codes in an array format. Example: ['es','de']
Yii::app()->i18n->get_extra_languages();


// Return a Language name
Yii::app()->i18n->language_name('en'); // Return 'English'
Yii::app()->i18n->language_name('es'); // Return 'Español'

// Get DEFAULT languague with following format ['en' => 'English']
Yii::app()->i18n->language_list('default');

// Get all ENABLED languages as a list array. Example ['en' => 'English', 'es' => 'Español', 'de' => 'Deutsch']
Yii::app()->i18n->language_list();

// Get all EXTRAS supported languages as a list array. Example ['es' => 'Español', 'de' => 'Deutsch']
Yii::app()->i18n->language_list('extras');

// Check if language exists and it is enabled
Yii::app()->i18n->is_enabled_language('es');


==================
-- CORE CLASSES --
==================
- \dz\i18n\MessageSource
    + It allows to translate messages from DATABASE and/or PHP FILES (both)
    + It's the MAIN class called from "main.php" config files
    + Internally, this class register these components: DbMessageSource and PhpMessageSource
    + Method loadMessages() load messages first from DATABASE and after from PHP files

- \dz\i18n\DbMessageSource
    + This component is initialized if "dbMessages" configuration property is defined
    + It loads message translations from database

- \dz\i18n\PhpMessageSource
    + This component is initialized if "phpMessages" configuration property is defined
    + It loads message translations from PHP Files


===========================
-- DEFAULT CONFIGRUATION --
===========================
    ...

    // Message translation using database and/or PHP files
    'messages' => [
        'class' => '\dz\i18n\MessageSource',

        // Use message translation from database (comment this for disable database translation)
        'dbMessages' => [
            'sourceMessageTable' => 'translation_source',
            'translatedMessageTable' => 'translation_message'
        ],

        // Use message translation from PHP files (comment this for disable PHP translation)
        'phpMessages' => [
            'basePath' => Yii::getAlias('@core.messages')
        ]
    ],

    ...
