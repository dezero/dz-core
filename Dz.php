<?php
/**
 * Dz is helper class serving common Dz and Yii framework functionality.
 */

if ( ! class_exists('Yii', false) )
{
    require DZ_APP_PATH .'/core/Yii.php';
}

class Dz extends Yii
{
    /**
     * Register an error after saving the model
     */
    public static function saveModelError($model)
    {
        self::log('ERROR saving '. get_class($model) .' model: '. print_r($model->getErrors(), TRUE), "profile", "error");
        return true;
    }


    /**
     * Dump a variable via Kint
     */
    public static function d()
    {
        $argv = func_get_args();
        call_user_func_array(array('Kint', 'dump'), $argv);
    }


    /**
     * Dump and die via Kint
     */
    public static function dd()
    {
        $argv = func_get_args();
        call_user_func_array(array('Kint', 'dump'), $argv);
        die;
    }


    /**
     * Returns the base URL
     */
    public static function baseUrl()
    {
        if ( isset(Yii::app()->params['baseUrl']) )
        {
            return Yii::app()->params['baseUrl'];
        }

        return Yii::app()->request->getBaseUrl();
    }


    /**
     * Returns the root path
     */
    public static function basePath()
    {
        return Yii::app()->path->basePath();
    }


    /**
     * Returns the theme path
     */
    public static function themePath()
    {
        return Yii::app()->path->themePath();
    }


    /**
     * Returns the theme URL
     */
    public static function themeUrl()
    {
        return Yii::app()->theme->getBaseUrl();
    }


    /**
     * Returns the log path
     */
    public static function logPath()
    {
        return Yii::app()->path->logPath();
    }


    /**
     * Get CORE modules for Dz Framework
     */
    public static function getCoreModules()
    {
        return [
            'admin' => [
                'class' => '\dz\modules\admin\Module',
            ],
            'api' => [
                'class' => '\dz\modules\api\Module',
            ],
            'asset' => [
                'class' => '\dz\modules\asset\Module',
            ],
            'auth' => [
                'class' => '\dz\modules\auth\Module',
            ],
            'category' => [
                'class' => '\dz\modules\category\Module',
            ],
            'comment' => [
                'class' => '\dz\modules\comment\Module',
            ],
            'notification' => [
                'class' => '\dz\modules\notification\Module',
            ],
            'search' => [
                'class' => '\dz\modules\search\Module',
            ],
            'settings' => [
                'class' => '\dz\modules\settings\Module',
            ],
            'user' => [
                'class' => '\dz\modules\user\Module',
            ],
            'web' => [
                'class' => '\dz\modules\web\Module',
            ]
        ];
    }


    /**
     * Get CONTRIG (dzlab) modules for Dz Framework
     */
    public static function getContribModules()
    {
        $vec_contrib_modules = [];

        // Get all contrib modules from "/app/vendor/dezero" direcotyr
        $contrib_path = DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'dezero';
        $contrib_dir = Yii::app()->file->set($contrib_path);

        if ( $contrib_dir->getExists() && $contrib_dir->getIsDir() )
        {
            $vec_contrib_directories = $contrib_dir->getContents();
            foreach ( $vec_contrib_directories as $directory_path )
            {
                $vec_directory_path = explode("/", $directory_path);
                $module_id = $vec_directory_path[count($vec_directory_path) - 1];
                if ( Yii::app()->hasModule($module_id) )
                {
                    $vec_contrib_modules[$module_id] = [
                        'class' => "\dzlab\{$module_id}\Module"
                    ];
                }
            }
        }

        return $vec_contrib_modules;
    }


    /**
     * Check if module belongs to Dz Framework core
     */
    public static function isCoreModule($module_id)
    {
        return in_array($module_id, array_keys(self::getCoreModules()));
    }


    /**
     * Check if app is running from the console
     */
    public static function is_console()
    {
        // return get_class(Yii::app()) == 'CConsoleApplication';
        return Yii::app() instanceof \CConsoleApplication;
    }



    /**
     * Returns an environment variable, checking for it in `$_SERVER` and calling `getenv()` as a fallback.
     */
    public static function env($name)
    {
        return $_SERVER[$name] ? getenv($name) : null;
    }


    /**
     * Returns an environment variable, checking for it in `$_SERVER` and calling `getenv()` as a fallback.
     */
    public static function get_environment()
    {
        return self::env('ENVIRONMENT');
    }


    /**
     * Check the environment is currently running in
     */
    public static function check_environment($environment_name)
    {
        return self::env('ENVIRONMENT') === $environment_name;
    }


    /**
     * Check if app is currently running in PRODUCTION environment
     */
    public static function is_production()
    {
        return self::check_environment('prod');
    }


    /**
     * Check if app is currently running in PRODUCTION environment
     * 
     * Alias of Dz::is_production()
     */
    public static function is_live()
    {
        return self::check_environment('prod');
    }


    /**
     * Check if app is currently running in DEVELOPMENT environment
     */
    public static function is_dev()
    {
        return self::check_environment('dev');
    }


    /**
     * Check if app is currently running in TEST environment
     */
    public static function is_test()
    {
        return self::check_environment('test');
    }
}