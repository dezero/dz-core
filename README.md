# Starter for Dz Core Framework

Directory structure:

- docs: Documentation
- lib: Vendor or external libraries (Yii extensions included) not loaded via composer.
- messages: Files with translations.
- src: Internal libraries from Dz Core Framework.
- views: Core template files (views). You'll be able to override them on your project.