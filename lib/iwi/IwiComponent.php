<?php

Yii::import('@lib.iwi.Iwi');
// Yii::import('@lib.iwi.vendors.image.KohanaImageComponent');
Yii::import('@lib.image.KohanaImageComponent');

/**
 * Description of KohanaImageComponent
 *
 * @author Administrator
 */
class IwiComponent extends KohanaImageComponent
{
    public function load($image)
    {
        $config = array(
            'driver' => $this->driver,
            'params' => $this->params,
        );

        return new Iwi($image, $config);
    }
}

?>
