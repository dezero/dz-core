<?php

/**
 * +----------------------------------------------------------------------+
 * | Copyright (C) 2017 IP2Location                                       |
 * +----------------------------------------------------------------------+
 * | This library is free software; you can redistribute it and/or        |
 * | modify it under the terms of the GNU Lesser General Public           |
 * | License as published by the Free Software Foundation; either         |
 * | version 2.1 of the License, or (at your option) any later version.   |
 * |                                                                      |
 * | This library is distributed in the hope that it will be useful,      |
 * | but WITHOUT ANY WARRANTY; without even the implied warranty of       |
 * | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU    |
 * | Lesser General Public License for more details.                      |
 * |                                                                      |
 * | You should have received a copy of the GNU Lesser General Public     |
 * | License along with this library; if not, write to the Free Software  |
 * | Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 |
 * | USA, or view it online at http://www.gnu.org/licenses/lgpl.txt.      |
 * +----------------------------------------------------------------------+
 * | Authors: IP2Location <support@ip2location.com>                       |
 * +----------------------------------------------------------------------+
 *
 * @category Net
 * @package  IP2Location
 * @author IP2Location <support@ip2location.com>
 * @license  LGPL http://www.gnu.org/licenses/lgpl.txt
 */
/**
 * Geolocation class file.
 *
 * @author IP2Location <support@ip2location.com>
 * @link http://www.yiiframework.com/
 * @version 1.1.0
 */
//Yii::import('application.extensions.ip2location.IP2Location');
require_once 'IP2Location.php';

class Geolocation extends CApplicationComponent {
    public $database = './IP2LOCATION-LITE-DB1.BIN';
    public $mode;

    protected static $ip2location;

    public function init() {
        switch($this->mode) {
            case 'MEMORY_CACHE':
                $flags = \IP2Location\Database::MEMORY_CACHE;
            break;

            case 'SHARED_MEMORY':
                $flags = \IP2Location\Database::SHARED_MEMORY;
            break;

            default:
                $flags = \IP2Location\Database::FILE_IO;
            break;
        }

        self::$ip2location = new \IP2Location\Database($this->database, $flags);

        parent::init();
    }

    public function getCountryCode($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::COUNTRY_CODE);
    }

    public function getCountryName($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::COUNTRY_NAME);
    }

    public function getRegionName($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::REGION_NAME);
    }

    public function getCityName($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::CITY_NAME);
    }

    public function getLatitude($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::LATITUDE);
    }

    public function getLongitude($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::LONGITUDE);
    }

    public function getISP($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::ISP);
    }

    public function getDomainName($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::DOMAIN_NAME);
    }

    public function getZIPCode($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::ZIP_CODE);
    }

    public function getTimeZone($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::TIME_ZONE);
    }

    public function getNetSpeed($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::NET_SPEED);
    }
    public function getIDDCode($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::IDD_CODE);
    }

    public function getAreaCode($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::AREA_CODE);
    }

    public function getWeatherStationCode($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::WEATHER_STATION_CODE);
    }

    public function getWeatherStationName($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::WEATHER_STATION_NAME);
    }

    public function getMCC($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::MCC);
    }

    public function getMNC($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::MNC);
    }

    public function getMobileCarrierName($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::MOBILE_CARRIER_NAME);
    }

    public function getElevation($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::ELEVATION);
    }

    public function getUsageType($ip=NULL) {
        return self::$ip2location->lookup(self::getIP($ip), \IP2Location\Database::USAGE_TYPE);
    }

    protected function getIP($ip=NULL) {
        return ($ip) ? $ip : Yii::app()->getRequest()->getUserHostAddress();
    }
}
?>
