<?php
return array(
	'No matches found' => 'No se han encontrado resultados',
	'Please enter {chars} more characters' => ' ',
	'Please enter {chars} less characters' => 'Introduce menos de {chars} caracteres',
	'You can only select {count} items' => 'Únicamente se pueden seleccionar {count} objetos',
	'Loading more results...' => 'Cargando más resultados...',
	'Searching...' => 'Buscando...',
);
