/**
 * jQuery Spellchecker integration
 *
 * @see http://jquery-spellchecker.badsyntax.co/redactor.html
 */
  
if (!RedactorPlugins) var RedactorPlugins = {};

RedactorPlugins.spellchecker = function()
{
    return {
        init: function()
        {
          var spell = this.button.add('spellchecker', 'Spellchecker');

          // make your added buttons as Font Awesome's icon
          // this.button.setAwesome('spellchecker', 'fa-book');

          this.button.addCallback(spell, this.spellchecker.spellchecker);
        },
        create: function()
        {
          var spellchecker_driver = 'aspell';
          var spellchecker_path = '/www/spellchecker.php';
          if ( typeof(window.js_globals.base_url) != "undefined" ) {
            spellchecker_path = window.js_globals.base_url + spellchecker_path;
          }
          if ( typeof(window.js_globals.spellchecker_driver) != "undefined" ) {
            spellchecker_driver = window.js_globals.spellchecker_driver;
          }
          this.jquery_spellchecker = new $.SpellChecker(this.$editor, {
            lang: 'en',
            parser: 'html', // 'text'
            webservice: {
              // path: "http://localhost/ebr.project/www/spellchecker.php",
              path: spellchecker_path,
              driver: spellchecker_driver
            },
            suggestBox: {
              position: 'below'
            },
            incorrectWords: {
              container: '#incorrect-word-list'
            }
          });
     
          // Bind spellchecker handler functions
          this.jquery_spellchecker.on('check.success', function() {
            alert('No misspellings found.');
          });
        },
        spellchecker: function()
        {
          if (!this.jquery_spellchecker) {
            this.button.setActive('spellchecker');
            this.spellchecker.create();
            this.jquery_spellchecker.check();
          } else {
            this.button.setInactive('spellchecker');
            this.jquery_spellchecker.destroy();
            this.jquery_spellchecker = null;
          }
        }
    };
};