<?php

/**
 * Modified version of Zend_Http_Response of Zend for easy integration
 * with Yii as extension.
 *
 * Copyright (c) 2005-2010, Zend Technologies USA, Inc.
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 * 
 *     * Neither the name of Zend Technologies USA, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * EHttpResponse represents an HTTP 1.0 / 1.1 response message. It
 * includes easy access to all the response's different elemts, as well as some
 * convenience methods for parsing and validating HTTP responses.
 *
 * @category Yii
 * @package    EHttpClient
 * @subpackage EHttpResponse
 */
class EHttpResponse
{
    /**
     * List of all known HTTP response codes - used by responseCodeAsText() to
     * translate numeric codes to messages.
     *
     * @var array
     */
    protected static $messages = array(
        // Informational 1xx
        100 => 'Continue',
        101 => 'Switching Protocols',

        // Success 2xx
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',

        // Redirection 3xx
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',  // 1.1
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        // 306 is deprecated but reserved
        307 => 'Temporary Redirect',

        // Client Error 4xx
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',

        // Server Error 5xx
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        509 => 'Bandwidth Limit Exceeded'
    );

    /**
     * The HTTP version (1.0, 1.1)
     *
     * @var string
     */
    protected $version;

    /**
     * The HTTP response code
     *
     * @var int
     */
    protected $code;

    /**
     * The HTTP response code as string
     * (e.g. 'Not Found' for 404 or 'Internal Server Error' for 500)
     *
     * @var string
     */
    protected $message;

    /**
     * The HTTP response headers array
     *
     * @var array
     */
    protected $headers = array();

    /**
     * The HTTP response body
     *
     * @var string
     */
    protected $body;


    /**
     * Function name used on decodeDeflate method
     *
     * @var string
     */
    protected static $decode_deflate_function = 'gzuncompress';


    /**
     * Function name used on decodeGzip method
     *
     * @var string
     */
    protected static $decode_gzip_function = 'gzinflate';


    /**
     * Use decode/uncompress functions from Worpdress
     *
     * @var bool
     */
    protected static $is_use_wordpress = false;


    /**
     * HTTP response constructor
     *
     * In most cases, you would use EHttpResponse::fromString to parse an HTTP
     * response string and create a new EHttpResponse object.
     *
     * NOTE: The constructor no longer accepts nulls or empty values for the code and
     * headers and will throw an exception if the passed values do not form a valid HTTP
     * responses.
     *
     * If no message is passed, the message will be guessed according to the response code.
     *
     * @param int $code EHttpResponse code (200, 404, ...)
     * @param array $headers Headers array
     * @param string $body EHttpResponse body
     * @param string $version HTTP version
     * @param string $message EHttpResponse code as text
     * @throws CException
     */
    public function __construct($code, $headers, $body = null, $version = '1.1', $message = null)
    {
        // Make sure the response code is valid and set it
        if (self::responseCodeAsText($code) === null) {
            throw new CException("{$code} is not a valid HTTP response code");
        }

        $this->code = $code;

        // Make sure we got valid headers and set them
        if (! is_array($headers)) {
            throw new CException('No valid headers were passed');
	}

        foreach ($headers as $name => $value) {
            if (is_int($name))
                list($name, $value) = explode(": ", $value, 1);

            $this->headers[ucwords(strtolower($name))] = $value;
        }

        // Set the body
        $this->body = $body;

        // Set the HTTP version
        if (! preg_match('|^\d\.\d$|', $version)) {
            throw new CException("Invalid HTTP response version: $version");
        }

        $this->version = $version;

        // If we got the response message, set it. Else, set it according to
        // the response code
        if (is_string($message)) {
            $this->message = $message;
        } else {
            $this->message = self::responseCodeAsText($code);
        }
    }

    /**
     * Check whether the response is an error
     *
     * @return boolean
     */
    public function isError()
    {
        $restype = floor($this->code / 100);
        if ($restype == 4 || $restype == 5) {
            return true;
        }

        return false;
    }

    /**
     * Check whether the response in successful
     *
     * @return boolean
     */
    public function isSuccessful()
    {
        $restype = floor($this->code / 100);
        if ($restype == 2 || $restype == 1) { // Shouldn't 3xx count as success as well ???
            return true;
        }

        return false;
    }

    /**
     * Check whether the response is a redirection
     *
     * @return boolean
     */
    public function isRedirect()
    {
        $restype = floor($this->code / 100);
        if ($restype == 3) {
            return true;
        }

        return false;
    }

    /**
     * Get the response body as string
     *
     * This method returns the body of the HTTP response (the content), as it
     * should be in it's readable version - that is, after decoding it (if it
     * was decoded), deflating it (if it was gzip compressed), etc.
     *
     * If you want to get the raw body (as transfered on wire) use
     * $this->getRawBody() instead.
     *
     * @return string
     */
    public function getBody()
    {
        $body = '';

        // Decode the body if it was transfer-encoded
        switch ( $this->getHeader('transfer-encoding') )
        {
            // Handle chunked body
            case 'chunked':
                if ( self::$is_use_wordpress === true )
                {
                    $body = self::wp_decode_chunked($this->body);
                }
                else
                {
                    $body = self::decodeChunkedBody($this->body);
                }
            break;

            // No transfer encoding, or unknown encoding extension:
            // return body as is
            default:
                $body = $this->body;
            break;
        }

        // Decode any content-encoding (gzip or deflate) if needed
        switch ( strtolower($this->getHeader('content-encoding')) )
        {
            // Handle gzip encoding
            case 'gzip':
                if ( self::$is_use_wordpress === true )
                {
                    $body = self::wp_decompress($body);
                }
                else
                {
                    $body = self::decodeGzip($body);
                }
            break;

            // Handle deflate encoding
            case 'deflate':
                if ( self::$is_use_wordpress === true )
                {
                    $body = self::wp_decompress($body);
                }
                else
                {
                    $body = self::decodeDeflate($body);
                }
            break;

            default:
            break;
        }

        return $body;
    }

    /**
     * Get the raw response body (as transfered "on wire") as string
     *
     * If the body is encoded (with Transfer-Encoding, not content-encoding -
     * IE "chunked" body), gzip compressed, etc. it will not be decoded.
     *
     * @return string
     */
    public function getRawBody()
    {
        return $this->body;
    }

    /**
     * Get the HTTP version of the response
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Get the HTTP response status code
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->code;
    }

    /**
     * Return a message describing the HTTP response code
     * (Eg. "OK", "Not Found", "Moved Permanently")
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Get the response headers
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Get a specific header as string, or null if it is not set
     *
     * @param string$header
     * @return string|array|null
     */
    public function getHeader($header)
    {
        $header = ucwords(strtolower($header));
        if (! is_string($header) || ! isset($this->headers[$header])) return null;

        return $this->headers[$header];
    }

    /**
     * Get all headers as string
     *
     * @param boolean $status_line Whether to return the first status line (IE "HTTP 200 OK")
     * @param string $br Line breaks (eg. "\n", "\r\n", "<br />")
     * @return string
     */
    public function getHeadersAsString($status_line = true, $br = "\n")
    {
        $str = '';

        if ($status_line) {
            $str = "HTTP/{$this->version} {$this->code} {$this->message}{$br}";
        }

        // Iterate over the headers and stringify them
        foreach ($this->headers as $name => $value)
        {
            if (is_string($value))
                $str .= "{$name}: {$value}{$br}";

            elseif (is_array($value)) {
                foreach ($value as $subval) {
                    $str .= "{$name}: {$subval}{$br}";
                }
            }
        }

        return $str;
    }

    /**
     * Get the entire response as string
     *
     * @param string $br Line breaks (eg. "\n", "\r\n", "<br />")
     * @return string
     */
    public function asString($br = "\n")
    {
        return $this->getHeadersAsString(true, $br) . $br . $this->getRawBody();
    }

    /**
     * A convenience function that returns a text representation of
     * HTTP response codes. Returns 'Unknown' for unknown codes.
     * Returns array of all codes, if $code is not specified.
     *
     * Conforms to HTTP/1.1 as defined in RFC 2616 (except for 'Unknown')
     * See http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10 for reference
     *
     * @param int $code HTTP response code
     * @param boolean $http11 Use HTTP version 1.1
     * @return string
     */
    public static function responseCodeAsText($code = null, $http11 = true)
    {
        $messages = self::$messages;
        if (! $http11) $messages[302] = 'Moved Temporarily';

        if ($code === null) {
            return $messages;
        } elseif (isset($messages[$code])) {
            return $messages[$code];
        } else {
            return 'Unknown';
        }
    }

    /**
     * Extract the response code from a response string
     *
     * @param string $response_str
     * @return int
     */
    public static function extractCode($response_str)
    {
        preg_match("|^HTTP/[\d\.x]+ (\d+)|", $response_str, $m);

        if (isset($m[1])) {
            return (int) $m[1];
        } else {
            return false;
        }
    }

    /**
     * Extract the HTTP message from a response
     *
     * @param string $response_str
     * @return string
     */
    public static function extractMessage($response_str)
    {
        preg_match("|^HTTP/[\d\.x]+ \d+ ([^\r\n]+)|", $response_str, $m);

        if (isset($m[1])) {
            return $m[1];
        } else {
            return false;
        }
    }

    /**
     * Extract the HTTP version from a response
     *
     * @param string $response_str
     * @return string
     */
    public static function extractVersion($response_str)
    {
        preg_match("|^HTTP/([\d\.x]+) \d+|", $response_str, $m);

        if (isset($m[1])) {
            return $m[1];
        } else {
            return false;
        }
    }

    /**
     * Extract the headers from a response string
     *
     * @param string $response_str
     * @return array
     */
    public static function extractHeaders($response_str)
    {
        $headers = array();
        
        // First, split body and headers
        $parts = preg_split('|(?:\r?\n){2}|m', $response_str, 2);
        if (! $parts[0]) return $headers;
        
        // Split headers part to lines
        $lines = explode("\n", $parts[0]);
        unset($parts);
        $last_header = null;

        foreach($lines as $line) {
            $line = trim($line, "\r\n");
            if ($line == "") break;

            if (preg_match("|^([\w-]+):\s+(.+)|", $line, $m)) {
                unset($last_header);
                $h_name = strtolower($m[1]);
                $h_value = $m[2];

                if (isset($headers[$h_name])) {
                    if (! is_array($headers[$h_name])) {
                        $headers[$h_name] = array($headers[$h_name]);
                    }

                    $headers[$h_name][] = $h_value;
                } else {
                    $headers[$h_name] = $h_value;
                }
                $last_header = $h_name;
            } elseif (preg_match("|^\s+(.+)$|", $line, $m) && $last_header !== null) {
                if (is_array($headers[$last_header])) {
                    end($headers[$last_header]);
                    $last_header_key = key($headers[$last_header]);
                    $headers[$last_header][$last_header_key] .= $m[1];
                } else {
                    $headers[$last_header] .= $m[1];
                }
            }
        }

        return $headers;
    }

    /**
     * Extract the body from a response string
     *
     * @param string $response_str
     * @return string
     */
    public static function extractBody($response_str)
    {
        $parts = preg_split('|(?:\r?\n){2}|m', $response_str, 2);
        if (isset($parts[1])) { 
        	return $parts[1];
        } else {
        	return '';
        }
    }

    /**
     * Decode a "chunked" transfer-encoded body and return the decoded text
     *
     * @param string $body
     * @return string
     */
    public static function decodeChunkedBody($body)
    {
        $decBody = '';
        
        while (trim($body)) {
            if (! preg_match("/^([\da-fA-F]+)[^\r\n]*\r\n/sm", $body, $m)) {
                throw new CException("Error parsing body - doesn't seem to be a chunked message");
            }

            $length = hexdec(trim($m[1]));
            $cut = strlen($m[0]);

            $decBody .= substr($body, $cut, $length);
            $body = substr($body, $cut + $length + 2);
        }

        return $decBody;
    }

    /**
     * Decode a gzip encoded message (when Content-encoding = gzip)
     *
     * Currently requires PHP with zlib support
     *
     * @param string $body
     * @return string
     */
    public static function decodeGzip($body)
    {
        if (! function_exists('gzinflate')) {
            throw new CException('Unable to decode gzipped response ' . 
                'body: perhaps the zlib extension is not loaded?'); 
        }

        // Use "gzuncompress" function instead of "gzinflate"
        if ( self::$decode_gzip_function === 'gzuncompress' )
        {
            return @gzuncompress($body);
        }

        // Use "gzinflate" function (default option)
        return gzinflate(substr($body, 10));
    }

    /**
     * Decode a zlib deflated message (when Content-encoding = deflate)
     *
     * Currently requires PHP with zlib support
     *
     * @param string $body
     * @return string
     */
    public static function decodeDeflate($body)
    {
        if (! function_exists('gzuncompress')) {
            throw new CException('Unable to decode deflated response ' . 
                'body: perhaps the zlib extension is not loaded?'); 
        }

        // Use "gzinflate" function instead of "gzuncompress"
        if ( self::$decode_deflate_function === 'gzinflate' )
        {
            return @gzinflate($body);
        }

        // Use "gzuncompress" function (default option)
    	return gzuncompress($body);
    }

    /**
     * Create a new EHttpResponse object from a string
     *
     * @param string $response_str
     * @return EHttpResponse
     */
    public static function fromString($response_str)
    {
        $code    = self::extractCode($response_str);
        $headers = self::extractHeaders($response_str);
        $body    = self::extractBody($response_str);
        $version = self::extractVersion($response_str);
        $message = self::extractMessage($response_str);

        return new EHttpResponse($code, $headers, $body, $version, $message);
    }


    /**
     * Set the function used on decodeDeflate method
     *
     * @since 09/01/2023
     */
    public function set_decode_deflate_function($function_name)
    {
        self::$decode_deflate_function = $function_name;
    }


    /**
     * Set the function used on decodeGzip method
     *
     * @since 09/01/2023
     */
    public function set_decode_gzip_function($function_name)
    {
        self::$decode_gzip_function = $function_name;
    }


    /**
     * Use Wordpress decode/decompress methods
     *
     * @since 09/01/2023
     */
    public function use_wordpress_methods()
    {
        self::$is_use_wordpress = true;
    }



    /**
     * ----------------------------------------------------------------------------------------------------------------
     *
     * FUNCTIONS FROM WORDPRESS
     *
     *
     * @see wp-includes/class-requests.php
     *
     * ----------------------------------------------------------------------------------------------------------------
     */



    /**
     * Decoded a chunked body as per RFC 2616
     *
     * @see https://tools.ietf.org/html/rfc2616#section-3.6.1
     * @param string $data Chunked body
     * @return string Decoded body
     *
     * @since 14/03/2023
     */
    public static function wp_decode_chunked($data)
    {
        if ( !preg_match('/^([0-9a-f]+)(?:;(?:[\w-]*)(?:=(?:(?:[\w-]*)*|"(?:[^\r\n])*"))?)*\r\n/i', trim($data)) )
        {
            return $data;
        }

        $decoded = '';
        $encoded = $data;

        while (true)
        {
            $is_chunked = (bool) preg_match('/^([0-9a-f]+)(?:;(?:[\w-]*)(?:=(?:(?:[\w-]*)*|"(?:[^\r\n])*"))?)*\r\n/i', $encoded, $matches);

            if ( !$is_chunked )
            {
                // Looks like it's not chunked after all
                return $data;
            }

            $length = hexdec(trim($matches[1]));
            if ($length === 0)
            {
                // Ignore trailer headers
                return $decoded;
            }

            $chunk_length = strlen($matches[0]);
            $decoded     .= substr($encoded, $chunk_length, $length);
            $encoded      = substr($encoded, $chunk_length + $length + 2);

            if (trim($encoded) === '0' || empty($encoded))
            {
                return $decoded;
            }
        }

        // We'll never actually get down here
        // @codeCoverageIgnoreStart
    }


    /**
     * Decompress an encoded body
     *
     * Implements gzip, compress and deflate. Guesses which it is by attempting
     * to decode.
     *
     * @param string $data Compressed data in one of the above formats
     * @return string Decompressed string
     *
     * @since 14/03/2023
     */
    public static function wp_decompress($data)
    {
        if (substr($data, 0, 2) !== "\x1f\x8b" && substr($data, 0, 2) !== "\x78\x9c")
        {
            // Not actually compressed. Probably cURL ruining this for us.
            return $data;
        }

        if (function_exists('gzdecode'))
        {
            // phpcs:ignore PHPCompatibility.FunctionUse.NewFunctions.gzdecodeFound -- Wrapped in function_exists() for PHP 5.2.
            $decoded = @gzdecode($data);
            if ($decoded !== false)
            {
                return $decoded;
            }
        }

        if (function_exists('gzinflate'))
        {
            $decoded = @gzinflate($data);
            if ($decoded !== false)
            {
                return $decoded;
            }
        }

        $decoded = self::wp_compatible_gzinflate($data);
        if ($decoded !== false)
        {
            return $decoded;
        }

        if (function_exists('gzuncompress'))
        {
            $decoded = @gzuncompress($data);
            if ($decoded !== false) {
                return $decoded;
            }
        }

        return $data;
    }



    /**
     * Decompression of deflated string while staying compatible with the majority of servers.
     *
     * Certain Servers will return deflated data with headers which PHP's gzinflate()
     * function cannot handle out of the box. The following function has been created from
     * various snippets on the gzinflate() PHP documentation.
     *
     * Warning: Magic numbers within. Due to the potential different formats that the compressed
     * data may be returned in, some "magic offsets" are needed to ensure proper decompression
     * takes place. For a simple progmatic way to determine the magic offset in use, see:
     * https://core.trac.wordpress.org/ticket/18273
     *
     * @link https://core.trac.wordpress.org/ticket/18273
     * @link https://secure.php.net/manual/en/function.gzinflate.php#70875
     * @link https://secure.php.net/manual/en/function.gzinflate.php#77336
     *
     * @param string $gz_data String to decompress.
     * @return string|bool False on failure.
     *
     * @since 14/03/2023
     */
    public static function wp_compatible_gzinflate($gz_data)
    {
        // Compressed data might contain a full zlib header, if so strip it for
        // gzinflate()
        if (substr($gz_data, 0, 3) === "\x1f\x8b\x08")
        {
            $i   = 10;
            $flg = ord(substr($gz_data, 3, 1));
            if ($flg > 0)
            {
                if ($flg & 4)
                {
                    list($xlen) = unpack('v', substr($gz_data, $i, 2));
                    $i         += 2 + $xlen;
                }
                if ($flg & 8)
                {
                    $i = strpos($gz_data, "\0", $i) + 1;
                }
                if ($flg & 16)
                {
                    $i = strpos($gz_data, "\0", $i) + 1;
                }
                if ($flg & 2)
                {
                    $i += 2;
                }
            }

            $decompressed = self::wp_compatible_gzinflate(substr($gz_data, $i));
            if ($decompressed !== false)
            {
                return $decompressed;
            }
        }

        // If the data is Huffman Encoded, we must first strip the leading 2
        // byte Huffman marker for gzinflate()
        // The response is Huffman coded by many compressors such as
        // java.util.zip.Deflater, Ruby’s Zlib::Deflate, and .NET's
        // System.IO.Compression.DeflateStream.
        //
        // See https://decompres.blogspot.com/ for a quick explanation of this
        // data type
        $huffman_encoded = false;

        // low nibble of first byte should be 0x08
        list(, $first_nibble) = unpack('h', $gz_data);

        // First 2 bytes should be divisible by 0x1F
        list(, $first_two_bytes) = unpack('n', $gz_data);

        if ($first_nibble === 0x08 && ($first_two_bytes % 0x1F) === 0)
        {
            $huffman_encoded = true;
        }

        if ($huffman_encoded)
        {
            $decompressed = @gzinflate(substr($gz_data, 2));
            if ($decompressed !== false)
            {
                return $decompressed;
            }
        }

        if (substr($gz_data, 0, 4) === "\x50\x4b\x03\x04")
        {
            // ZIP file format header
            // Offset 6: 2 bytes, General-purpose field
            // Offset 26: 2 bytes, filename length
            // Offset 28: 2 bytes, optional field length
            // Offset 30: Filename field, followed by optional field, followed
            // immediately by data
            list(, $general_purpose_flag) = unpack('v', substr($gz_data, 6, 2));

            // If the file has been compressed on the fly, 0x08 bit is set of
            // the general purpose field. We can use this to differentiate
            // between a compressed document, and a ZIP file
            $zip_compressed_on_the_fly = ((0x08 & $general_purpose_flag) === 0x08);

            if (!$zip_compressed_on_the_fly)
            {
                // Don't attempt to decode a compressed zip file
                return $gz_data;
            }

            // Determine the first byte of data, based on the above ZIP header
            // offsets:
            $first_file_start = array_sum(unpack('v2', substr($gz_data, 26, 4)));
            $decompressed     = @gzinflate(substr($gz_data, 30 + $first_file_start));
            if ($decompressed !== false)
            {
                return $decompressed;
            }
            return false;
        }

        // Finally fall back to straight gzinflate
        $decompressed = @gzinflate($gz_data);
        if ($decompressed !== false)
        {
            return $decompressed;
        }

        // Fallback for all above failing, not expected, but included for
        // debugging and preventing regressions and to track stats
        $decompressed = @gzinflate(substr($gz_data, 2));
        if ($decompressed !== false)
        {
            return $decompressed;
        }

        return false;
    }
}

