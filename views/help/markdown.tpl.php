<?php
/*
|--------------------------------------------------------------------------
| Markdown help guide
|--------------------------------------------------------------------------
*/
  
  $this->pageTitle = 'Markdown Format Guide';

  // Custom CSS style for this help page
  Yii::app()->clientscript->
    registerCss('markdown-help',
      '.card { width: 450px; }
       .card .card-text {
        font-family: Menlo,Monaco,Consolas,"Courier New",monospace;
      }'
    );
?>
<div class="container-fluid">
  <div class="row" style="max-width:500px; margin:auto;">
    <div class="col-xs-12">
      <h1 class="center mb-40"><?= $this->pageTitle; ?></h1>
      
      <h4>Emphasis</h4>
      <div class="card">
        <div class="card-block">
          <p class="card-text">
            **<strong>bold</strong>**<br>
            *<em>italics</em>*<br>
            ~~<strike>strikethrough</strike>~~
          </p>
        </div>
      </div>
    
      <h4>Headers</h4>
      <div class="card">
        <div class="card-block">
          <p class="card-text">
            # Big header<br>
            ## Medium header<br>
            ### Small header<br>
            #### Tiny header
          </p>
        </div>
      </div>
    
      <h4>Lists</h4>
      <div class="card">
        <div class="card-block">
          <p class="card-text">
            * Generic list item<br>
            * Generic list item<br>
            * Generic list item<br>
            <br>
            1. Numbered list item<br>
            2. Numbered list item<br>
            3. Numbered list item<br>
          </p>
        </div>
      </div>

      <h4>Links</h4>
      <div class="card">
        <div class="card-block">
          <p class="card-text">
            [Text to display](http://www.example.com)
          </p>
        </div>
      </div>
    
      <h4>Quotes</h4>
      <div class="card">
        <div class="card-block">
          <p class="card-text">
            > This is a quote.<br>
            > It can span multiple lines!
          </p>
        </div>
      </div>
    
      <h4>Images</small></h4>
      <div class="card">
        <div class="card-block">
          <p class="card-text">
            ![](http://www.example.com/image.jpg)
          </p>
        </div>
      </div>
      
      <h4>Tables</h4>
      <div class="card">
        <div class="card-block">
          <p class="card-text">
            | Column 1 | Column 2 | Column 3 |<br>
            | -------- | -------- | -------- |<br>
            | John     | Doe      | Male     |<br>
            | Mary     | Smith    | Female   |<br>
            <br>
            <em>Or without aligning the columns...</em><br>
            <br>
            | Column 1 | Column 2 | Column 3 |<br>
            | -------- | -------- | -------- |<br>
            | John | Doe | Male |<br>
            | Mary | Smith | Female |<br>
          </p>
        </div>
      </div>
    
      <h4>Displaying code</h4>
      <div class="card">
        <div class="card-block">
          <p class="card-text">
            `var example = "hello!";`<br>
            <br>
            <em>Or spanning multiple lines...</em><br>
            <br>
            ```<br>
            var example = "hello!";<br>
            alert(example);<br>
            ```
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
