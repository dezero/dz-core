<?php
/*
|--------------------------------------------------------------------------
| Table column partial page for AssetFileStatus model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: AssetFileStatus model
|  - $file_model: AssetFile model
|  - $column: Column name
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "title"
    |--------------------------------------------------------------------------
    */
    case 'title':
  ?>
    <?php if ( $model->file ) : ?>
      <?= $model->file->title; ?>
    <?php elseif ( isset($file_model) && !empty($file_model->title) ) : ?>
      <?= $file_model->title; ?>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "file"
    |--------------------------------------------------------------------------
    */
    case 'file':
  ?>
    <?php if ( $model->file ) : ?>
      <a href="<?= $model->file->download_url(); ?>" target="_blank"><?= $model->file->title(); ?></a>
      <span><?= $model->file->updated_date; ?> <?= Yii::t('app', 'by'); ?> <?= $model->file->updatedUser->fullname(); ?></span>
    <?php else : ?>
      -
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "status_type"
    |--------------------------------------------------------------------------
    */
    case 'status_type':
  ?>
    <?php
      $this->renderPartial('//asset/status/_view_status', [
        'status_type' => !empty($model->status_type) ? $model->status_type : 'not_sent'
      ]);
    ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "comments"
    |--------------------------------------------------------------------------
    */
    case 'comments':
  ?>
    <?= $model->comments; ?>
  <?php break; ?>
<?php endswitch; ?>