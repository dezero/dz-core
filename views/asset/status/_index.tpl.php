<?php
/*
|--------------------------------------------------------------------------
| View partial for AssetFileStatus models
|--------------------------------------------------------------------------
|
| Available variables:
|  - $validation_type: Validation type
|  - $table_id: Table #id
|  - $table_reload_url: URL to reload the table
|  - $vec_file_models: Array with AssetFile models
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;

  // Table file list
  $this->renderPartial('//asset/status/_table', [
    'validation_type'   => $validation_type,
    'table_id'          => $table_id,
    'table_reload_url'  => $table_reload_url,
    'vec_file_models'   => $vec_file_models,
  ]);

// --------------------------------------------------------------------
// Custom JAVASCRIPT libraries for this page:
//  - Dropzone - Krajee Bootstrap File Input plugin
// --------------------------------------------------------------------
Yii::app()->clientscript
  ->registerCssTopFile(Yii::app()->theme->baseUrl .'/js/krajee-fileinput/css/fileinput.min.css')
  ->registerScriptFile(Yii::app()->theme->baseUrl ."/js/krajee-fileinput/js/fileinput.min.js", CClientScript::POS_END);
