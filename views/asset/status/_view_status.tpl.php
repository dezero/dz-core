<?php
/*
|--------------------------------------------------------------------------
| Status type (fragment HTML) for a AssetFileStatus model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $status_type: Current status type
|
*/
  // Get status labels & colors
  $vec_status_labels = Yii::app()->fileManager->status_labels();
  $vec_status_colors = Yii::app()->fileManager->status_colors();
?>
<div class="status-type-wrapper file-status">
  <?php if ( isset($vec_status_labels[$status_type]) ) : ?>
    <?php if ( isset($vec_status_colors[$status_type]) ) : ?>
      <span class="<?= $vec_status_colors[$status_type]; ?>"><i class="wb-medium-point <?= $vec_status_colors[$status_type]; ?>" aria-hidden="true"></i> <?= $vec_status_labels[$status_type]; ?></span>
    <?php else : ?>
      <span><i class="wb-medium-point"></i> <?= $vec_status_labels[$status_type]; ?></span>
    <?php endif; ?>
  <?php elseif ( $status_type === 'not_sent' ) : ?>
    <span class="grey-500"><i class="wb-medium-point grey-500" aria-hidden="true"></i> <?= Yii::t('app', 'Not sent'); ?></span>
  <?php else : ?>
    <span><?= $status_type; ?></span>
  <?php endif; ?>
</div>