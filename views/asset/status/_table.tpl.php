<?php
/*
|--------------------------------------------------------------------------
| Table partial for AssetFileStatus models
|--------------------------------------------------------------------------
|
| Available variables:
|  - $validation_type: Validation type
|  - $table_id: Table #id
|  - $table_reload_url: AJAX URL to reload the table
|  - $vec_file_models: Array with AssetFile models
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<table id="<?= $table_id; ?>" class="table table-striped table-hover grid-view-loading file-status-table" data-url="<?= $table_reload_url; ?>">
  <thead>
    <tr>
      <th class="document-column"><?= Yii::t('app', 'DOCUMENT'); ?></th>
      <th class="files-column"><?= Yii::t('app', 'UPLOAD FILE'); ?></th>
      <th class="status-column"><?= Yii::t('app', 'STATUS'); ?></th>
      <th class="comments-column"><?= Yii::t('app', 'COMMENTS'); ?></th>
      <th class="actions-column"><?= Yii::t('app', 'ACTIONS'); ?></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ( $vec_file_models as $asset_file_model ) : ?>
      <?php
        // Get AssetFileStatus model
        $asset_file_status_model = $asset_file_model->get_status_model($validation_type, true);
      ?>
      <tr>
        <td class="title-column">
          <?php
            $this->renderPartial('//asset/status/_table_column', [
              'column'      => 'title',
              'model'       => $asset_file_status_model,
              'file_model'  => $asset_file_model
            ]);
          ?>
        </td>
        <td class="files-column">
          <?php
            $this->renderPartial('//asset/status/_table_column', [
              'column'    => 'file',
              'model'     => $asset_file_status_model,
              'file_model'  => $asset_file_model
            ]);
          ?>
        </td>
        <td class="status-column">
          <?php
            $this->renderPartial('//asset/status/_table_column', [
              'column'    => 'status_type',
              'model'     => $asset_file_status_model,
              'file_model'  => $asset_file_model
            ]);
          ?>
        </td>
        <td class="comments-column">
          <?php
            $this->renderPartial('//asset/status/_table_column', [
              'column'    => 'comments',
              'model'     => $asset_file_status_model,
              'file_model'  => $asset_file_model
            ]);
          ?>
        </td>
        <td class="actions-column center">
          <?php if ( !empty($asset_file_status_model->file_id) ) : ?>
            <a class="update-action btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip" data-action="change_status" href="<?= Url::to('/asset/status/update', ['validation_type' => $asset_file_status_model->validation_type, 'file_id' => $asset_file_status_model->file_id]); ?>" data-original-title="<?= Yii::t('app', 'Change status'); ?>"><i class="wb-edit"></i></a>
            <a class="update-action btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip" data-action="upload_files" href="<?= Url::to('/asset/status/upload', ['validation_type' => $asset_file_status_model->validation_type, 'file_id' => $asset_file_status_model->file_id]); ?>" data-original-title="<?= Yii::t('app', 'Upload'); ?>"><i class="wb-upload"></i></a>
            <a class="delete-action btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip" data-action="delete" href="<?= Url::to('/asset/status/delete', ['validation_type' => $asset_file_status_model->validation_type, 'file_id' => $asset_file_status_model->file_id]); ?>" data-original-title="<?= Yii::t('app', 'Delete'); ?>" data-validation="<?= $asset_file_status_model->validation_type; ?>" data-file="<?= $asset_file_status_model->file_id; ?>"><i class="wb-trash"></i></a>
          <?php else : ?>
            <a class="update-action btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip" data-action="upload_files" href="<?= Url::to('/asset/status/upload', ['validation_type' => $asset_file_status_model->validation_type]); ?>" data-original-title="<?= Yii::t('app', 'Upload'); ?>"><i class="wb-upload"></i></a>
          <?php endif; ?>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>