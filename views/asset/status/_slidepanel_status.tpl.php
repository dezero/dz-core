<?php
/*
|--------------------------------------------------------------------------
| Change status for an AssetFileStatus model - Slide Panel
|--------------------------------------------------------------------------
|
| Available variables:
|  - $asset_file_model: AssetFile model
|  - $asset_file_status_model: AssetFileStatus model
|
*/
  use dz\helpers\Html;

?>
<header class="slidePanel-header">
  <div class="slidePanel-actions" aria-label="actions" role="group">
    <button type="button" class="btn btn-pure btn-inverse slidePanel-close actions-top icon wb-close" aria-hidden="true"></button>
  </div>
  <h1><?= !empty($asset_file_model->title) ? $asset_file_model->title : Yii::t('app', 'Change Status'); ?></h1>
</header>
<div id="file-status-slidepanel-action" class="slidePanel-inner file-status-slidepanel-wrapper" data-action="change_status">
  <section class="slidePanel-inner-section">
    <div id="slidepanel-errors" class="messages-error alert alert-dismissible dark summary-errors alert-danger hide"></div>
    <div class="row">
      <label class="col-lg-3 form-control-label"><?= Yii::t('app', 'Current status'); ?></label>
      <div class="col-lg-9 file-status-wrapper">
        <?php
          $this->renderPartial('//asset/status/_view_status', [
            'status_type' => $asset_file_status_model->status_type
          ]);
        ?>
      </div>
    </div>

    <div class="row">
      <label class="col-lg-3 pt-5 form-control-label"><?= Yii::t('app', 'New status'); ?></label>
      <div class="col-lg-9">
        <?=
          Html::dropDownList('AssetFileStatus[status_type]', $asset_file_status_model->status_type, $asset_file_status_model->status_type_labels(), [
            'id'              => 'AssetFileStatus_status_type',
            'data-init-value' => $asset_file_status_model->status_type
          ]);
        ?>
      </div>
    </div>

    <div class="row">
      <label class="col-lg-3 form-control-label"><?= Yii::t('app', 'Comments'); ?></label>
      <div class="col-lg-9">
        <textarea id="AssetFileStatus_comments" name="AssetFileStatus[comments]" class="maxlength-textarea form-control mb-sm" rows="3"><?= $asset_file_status_model->comments; ?></textarea>
      </div>
    </div>

    <div class="row">
      <label class="col-lg-3 form-control-label"></label>
      <div class="col-lg-9">
        <button id="file-status-save-btn" class="btn btn-primary" data-dismiss="modal" type="button" data-validation="<?= $asset_file_status_model->validation_type; ?>" data-file="<?= $asset_file_status_model->file_id; ?>"><?= Yii::t('app', 'Save'); ?></button>
      </div>
    </div>

  </section>
</div>