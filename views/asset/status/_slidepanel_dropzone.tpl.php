<?php
/*
|--------------------------------------------------------------------------
| Dropzone file partial for AssetFileStatus models - Slide Panel
|--------------------------------------------------------------------------
|
| Available variables:
|  - $validation_type: Validation type
|  - $upload_url: Upload URL
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<header class="slidePanel-header">
  <div class="slidePanel-actions" aria-label="actions" role="group">
    <button type="button" class="btn btn-pure btn-inverse slidePanel-close actions-top icon wb-close" aria-hidden="true"></button>
  </div>
  <h1><?= !empty($asset_file_model->title) ? $asset_file_model->title : Yii::t('app', 'Upload New File'); ?></h1>
</header>
<div id="file-status-slidepanel-action" class="slidePanel-inner file-status-slidepanel-wrapper" data-action="upload_files">
  <section class="slidePanel-inner-section">
    <div id="slidepanel-errors" class="messages-error alert alert-dismissible dark summary-errors alert-danger hide"></div>
    <div class="row">
      <div class="col-sm-12">
        <div id="file-status-upload-container" class="dropzone-form-wrapper col-sm-12" data-url="<?= $upload_url; ?>">
          <div class="form-group">
            <div id="file-status-file-input-wrapper" class="kv-wrapper">
              <input id="file-status-file-input" name="AssetFiles[]" type="file" class="file-loading" multiple data-validation="<?= $validation_type; ?>">
            </div>
            <div id="file-status-error-message mt-20" style="display:none"></div>
            <div id="file-status-success-message" class="alert alert-success dark in" style="margin-top:20px; display:none"></div>
          </div>
        </div> 
      </div>
    </div>
  </section>
</div>