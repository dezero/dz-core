<?php
/*
|--------------------------------------------------------------------------
| Admin list page for Comment models
|--------------------------------------------------------------------------
|
| Available variables:
|   $comment_model: Comment model class
|
*/  
  // Page title
  $this->pageTitle = Yii::t('app', 'Comments');
?>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
</div>
<div class="page-content">
  <div class="row row-lg">
    <div class="col-lg-12">
      <div class="panel panel-top-summary">
        <div class="panel-body container-fluid">
          <?php
            /*
            |----------------------------------------------------------------------------------------
            | GridView widget
            |----------------------------------------------------------------------------------------
            */
              $this->widget('dz.grid.GridView', array(
                'id'            => 'comment-grid',
                'dataProvider'  => $comment_model->search(),
                'filter'        => $comment_model,
                'emptyText'     => Yii::t('app', 'No comments found'),
                'enableHistory' => true,
                'loadModal'     => true,
                'enableSorting' => false,
                'type'          => ['striped', 'hover'],
                'beforeAjaxUpdate' => 'js:function(id, options){ $("html, body").animate({scrollTop: 0}, 100); }',
                'columns'       => [
                  [
                    'header' => Yii::t('app', 'Date'),
                    'name' => 'created_date',
                    'filter' => false
                  ],
                  [
                    'header' => Yii::t('app', 'User'),
                    'name' => 'user_id',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("//comment/_grid_column", ["column" => "user", "model" => $data]))',
                  ],
                  [
                    'header' => Yii::t('app', 'Page'),
                    'name' => 'entity_filter',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("//comment/_grid_column", ["column" => "entity", "model" => $data]))',
                  ],
                  [
                    'name' => 'comment',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("//comment/_grid_column", ["column" => "comment", "model" => $data]))',
                  ],
                  [
                    'class' => 'dz.grid.ButtonColumn',
                    'header' => Yii::t('app', 'ACTION'),
                    'template' => '{delete}',
                    'deleteButton' => true,
                    'clearButton' => true,
                    'viewButton' => false,
                    'updateButton' => false,
                  ],
                ],                
              ));
            ?>
        </div>
      </div>
    </div>
  </div>
</div>
