<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for Comment model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: Comment model
|  - $column: Column name
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "user"
    |--------------------------------------------------------------------------
    */
    case 'user':
  ?>
    <?php if ( $model->user ) : ?>
      <p><?= $model->user->title(); ?></p>
      <a href="<?= Url::to('update', ['id' => $model->user->id]); ?>" target="_blank"><?= $model->user->email; ?></a>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "entity"
    |--------------------------------------------------------------------------
    */
    case 'entity':
  ?>
    <a href="<?= $model->url(); ?>" target="_blank"><?= $model->entity_type; ?> <?= $model->entity_id; ?></a>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "comment"
    |--------------------------------------------------------------------------
    */
    case 'comment':
  ?>
    <?= $model->comment; ?>
  <?php break; ?>
<?php endswitch; ?>