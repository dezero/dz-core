<?php
/*
|--------------------------------------------------------------------------
| Update form page for WebBlock model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $title: Current title page
|  - $vec_models: Array with WebBlock model
|  - $form_id: Form identifier
|
*/
  $this->pageTitle = $title;

  use dz\helpers\Html;
?>

<div class="page-header">
  <h1 class="page-title"><?= $title; ?></h1>
</div>

<div class="page-content container-fluid">
  <?php
    // Render form
    $this->renderPartial('_form', [
      'vec_models'  => $vec_models,
      'form_id'     => $form_id,
      'button'      => Yii::t('app', 'Save')
    ]);
  ?>
</div>
