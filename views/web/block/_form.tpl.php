<?php
/*
|--------------------------------------------------------------------------
| Form partial page for WebBlock model (page)
|--------------------------------------------------------------------------
|
| Available variables:
|  - $vec_models: Array with WebBlock model
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;

  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal',
      'enctype' => 'multipart/form-data',
      'autocomplete' => 'off'
    ]
  ]);

  // Error summary
  $vec_form_models = [];

  foreach ( $vec_models as $internal_name => $vec_blocks )
  {
    if ( isset($vec_blocks['models']) && !empty($vec_blocks['models']) )
    {
      $vec_form_models = CMap::mergeArray($vec_form_models, $vec_blocks['models']);
    }
  }
  $errors = $form->errorSummary($vec_form_models);
  if ( $errors )
  {
    echo $errors;
  }

  // Multilanguge?
  $is_multilanguage = Yii::app()->blockManager->is_multilanguage();
?>
<input name="StatusChange" id="status-change" class="form-control" type="hidden" value="">
<?php foreach ( $vec_models as $internal_name => $vec_blocks ) : ?>
  <div class="panel">
    <header class="panel-heading">
      <h3 class="panel-title"><?= $vec_blocks['title']; ?></h3>
    </header>
    <div class="panel-body">
      <?php if ( $is_multilanguage ) : ?>
        <div class="nav-tabs-horizontal pt-10" data-plugin="tabs">
          <ul class="nav nav-tabs nav-tabs-line tabs-line-top" role="tablist" data-block-name="<?= $internal_name; ?>">
            <?php foreach ( $vec_blocks['models'] as $language_id => $block_model ) : ?>
              <li class="nav-item" role="presentation">
                <a class="nav-link<?php if ( $language_id == Yii::defaultLanguage() ) : ?> active<?php endif; ?>" data-toggle="tab" href="#<?= $internal_name; ?>-lang-<?= $language_id; ?>" aria-controls="<?= $internal_name; ?>-lang-<?= $language_id; ?>" role="tab" aria-expanded="false" data-language="<?= $language_id; ?>"><span class="flag-icon flag-icon-<?= $language_id; ?>"></span> <?= StringHelper::strtoupper(Yii::app()->i18n->language_name($language_id)); ?></a>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      <?php endif; ?>
      <div class="tab-content pt-20">
        <?php foreach ( $vec_blocks['models'] as $language_id => $block_model ) : ?>
          <div class="tab-pane<?php if ( $language_id == Yii::defaultLanguage() ) : ?> active<?php endif; ?>" id="<?= $internal_name; ?>-lang-<?= $language_id; ?>" role="tabpanel" aria-expanded="false">
            <?php
              /*
              |--------------------------------------------------------------------------
              | TITLE
              |--------------------------------------------------------------------------
              */
              if ( $block_model->is_config('title') ) :
            ?>
              <div class="form-group row<?php if ( $block_model->hasErrors('title') ) : ?> has-danger<?php endif; ?>">
                <?= Html::label($block_model->get_label('title'), $block_model->resolve_id('title'), ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
                <div class="col-lg-10">
                  <?=
                    $form->textField($block_model, 'title', [
                      'name'        => $block_model->resolve_name('title'),
                      'maxlength'   => 255,
                      'placeholder' => '',
                    ]);
                  ?>
                  <?= $form->error($block_model, 'title'); ?>
                </div>
              </div>
            <?php endif; ?>

            <?php
              /*
              |--------------------------------------------------------------------------
              | SUBTITLE
              |--------------------------------------------------------------------------
              */
              if ( $block_model->is_config('subtitle') ) :
            ?>
              <div class="form-group row<?php if ( $block_model->hasErrors('subtitle') ) : ?> has-danger<?php endif; ?>">
                <?= Html::label($block_model->get_label('subtitle'), $block_model->resolve_id('subtitle'), ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
                <div class="col-lg-9 col-sm-10">
                  <?=
                    $form->textField($block_model, 'subtitle', [
                      'name'        => $block_model->resolve_name('subtitle'),
                      'maxlength'   => 255,
                      'placeholder' => '',
                    ]);
                  ?>
                  <?= $form->error($block_model, 'subtitle'); ?>
                </div>
              </div>
            <?php endif; ?>

            <?php
              /*
              |--------------------------------------------------------------------------
              | BODY
              |--------------------------------------------------------------------------
              */
              if ( $block_model->is_config('body') ) :
            ?>
              <div class="form-group row<?php if ( $block_model->hasErrors('body') ) : ?> has-danger<?php endif; ?>">
                <?= Html::label($block_model->get_label('body'), $block_model->resolve_id('body'), ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
                <div class="col-lg-10">
                  <?php
                    /*
                    $this->widget('@lib.redactor.ERedactorWidget', [
                      'model'     => $block_model,
                      'attribute' => 'body',
                      'options'   => [
                        'minHeight' => 350,
                      ]
                    ]);
                    */
                    $this->widget('@ext.DzMarkdown.DzMarkdown', [
                      'model'     => $block_model,
                      'attribute' => 'body',
                      'options'   => [
                        'minHeight' => $block_model->is_config('body', 'min_height') ? $block_model->get_config('body', 'min_height') : 300
                      ],
                      'htmlOptions' => [
                        'name'        => $block_model->resolve_name('body'),
                      ]
                    ]);
                  ?>
                </div>
              </div>
            <?php endif; ?>

            <?php
              //----------------------------------------------------------------
              // IMAGE
              //----------------------------------------------------------------
              if ( $block_model->is_config('image_id') ) :
            ?>
              <div class="form-group row<?php if ( $image_model->hasErrors('file_name') ) : ?> has-danger<?php endif; ?>">
                <label class="col-lg-2 col-sm-2 form-control-label" for="AssetImage_0_file_name"><?= $block_model->getAttributeLabel('image_id'); ?></label>
                <div class="col-lg-10">
                  <?php
                    $this->widget('\dz\widgets\Upload', [
                      'name'      => 'AssetImage[0][file_name]',
                      'model'     => $image_model,
                      'attribute' => 'file_name',
                      'view_mode' => false,
                      'is_image'  => true,
                    ]);
                  ?>
                  <?= $form->error($image_model,'file_name'); ?>
                </div>
              </div>
            <?php endif; ?>

            <?php
              /*
              |--------------------------------------------------------------------------
              | LINK TITLE
              |--------------------------------------------------------------------------
              */
              if ( $block_model->is_config('link_title') ) :
            ?>
              <div class="form-group row<?php if ( $block_model->hasErrors('link_title') ) : ?> has-danger<?php endif; ?>">
                <?= Html::label($block_model->get_label('link_title'), $block_model->resolve_id('link_title'), ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
                <div class="col-lg-10">
                  <?=
                    $form->textField($block_model, 'link_title', [
                      'name'        => $block_model->resolve_name('link_title'),
                      'maxlength'   => 255,
                      'placeholder' => '',
                    ]);
                  ?>
                  <?= $form->error($block_model, 'link_title'); ?>
                </div>
              </div>
            <?php endif; ?>

            <?php
              /*
              |--------------------------------------------------------------------------
              | LINK URL
              |--------------------------------------------------------------------------
              */
              if ( $block_model->is_config('link_url') ) :
            ?>
              <div class="form-group row<?php if ( $block_model->hasErrors('link_url') ) : ?> has-danger<?php endif; ?>">
                <?= Html::label($block_model->get_label('link_url'), $block_model->resolve_id('link_url'), ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
                <div class="col-lg-9 col-sm-10">
                  <?=
                    $form->textField($block_model, 'link_url', [
                      'name'        => $block_model->resolve_name('link_url'),
                      'maxlength'   => 255,
                      'placeholder' => '',
                    ]);
                  ?>
                  <?= $form->error($block_model, 'link_url'); ?>
                </div>
              </div>
            <?php endif; ?>
          </div><!-- .tab-pane -->
        <?php endforeach; ?>
      </div><!-- .tab-content -->
    </div>
  </div>
<?php endforeach; ?>
<?php /*if ( !empty($vec_models) ) : ?>
  <?php foreach ( $vec_models as $language_id => $block_model ) : ?>
    <div class="panel">
      <header class="panel-heading">
        <?php if ( $block_model->is_config('block_title') ) : ?>
          <h3 class="panel-title"><?= $block_model->get_config('block_title'); ?></h3>
        <?php endif; ?>
      </header>

      <div class="panel-body">

        

      </div><!-- .panel-body -->
    </div><!-- .panel -->
  <?php endforeach; ?>
<?php endif; */ ?>

<?php
  /*
  |--------------------------------------------------------------------------
  | ACTIONS
  |--------------------------------------------------------------------------
  */
?>  
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
        $this->widget('@bootstrap.widgets.TbButton', [
          'buttonType'  => 'submit',
          'type'        => 'primary',
          'label'       => $button,
        ]);
      ?>
    </div><!-- form-actions -->
  </div>

<?php
  // End model form
  $this->endWidget();
?>
