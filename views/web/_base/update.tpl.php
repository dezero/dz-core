<?php
/*
|--------------------------------------------------------------------------
| Update form page for Content model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $content_model: WebContent model class
|  - $image_model: AssetImage model class
|  - $file_model: AssetFile model class
|  - $vec_config: WebContent configuration options
|  - $vec_translated_models: Array with translated WebContent model class
|  - $vec_seo_models: Array with SEO models
|  - $default_language: Default language code
|  - $vec_extra_languages: Array with Extra languages
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  
  // Current controller
  $current_controller = Yii::currentController();

  // Page title
  $this->pageTitle = $content_model->title();
?>

<div class="page-header">
  <h1 class="page-title">
    <?= $this->pageTitle; ?>
    <?php if ( ! $content_model->is_enabled() ) : ?>
      <div class="badge badge-danger" data-toggle="tooltip" data-placement="bottom" data-original-title="<?= Yii::t('app', 'DISABLED'); ?>"><?= Yii::t('app', 'DISABLED'); ?></div>
    <?php endif; ?>    
  </h1>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => $content_model->text('index_title'),
        'url' => ['/web/'. $current_controller .'/'],
      ],
      $this->pageTitle
    ]);
  ?>

  <div class="page-header-actions">
    <?=
      // Back
      Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app', 'Back'), ['/web/'. $current_controller], [
          'class' => 'btn btn-default btn-back',
      ]);
    ?>
  </div>
</div>

<div class="page-content container-fluid">
  <div class="row row-lg">
    <div class="col-lg-12">
      <?php
        // Render form
        $this->renderPartial($content_model->get_view_path('_form'), [
          'content_model'         => $content_model,
          'image_model'           => $image_model,
          'file_model'            => $file_model,
          'vec_config'            => $vec_config,
          'vec_translated_models' => $vec_translated_models,
          'vec_seo_models'        => $vec_seo_models,
          'default_language'      => $default_language,
          'vec_extra_languages'   => $vec_extra_languages,
          'form_id'               => $form_id,
          'buttons'               => Yii::t('app', 'update')
        ]);
      ?>
    </div>
  </div>
</div>