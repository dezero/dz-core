<?php
/*
|--------------------------------------------------------------------------
| Create form page for WebContent model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $content_model: WebContent model class
|  - $image_model: AssetImage model class
|  - $file_model: AssetFile model class
|  - $vec_config: WebContent configuration options
|  - $vec_translated_models: Array with translated WebContent models
|  - $vec_seo_models: Array with SEO models
|  - $default_language: Default language code
|  - $vec_extra_languages: Array with Extra languages
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  
  // Current controller
  $current_controller = Yii::currentController();

  $this->pageTitle = Yii::t('app', $content_model->text('create_title'));
?>

<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => $this->text('entities_label'),
        'url' => ['/web/'. $current_controller .'/'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <div class="row row-lg">
    <div class="col-lg-12">
      <?php
      	// Render form
      	$this->renderPartial($content_model->get_view_path('_form'), [
          'content_model'         => $content_model,
          'image_model'           => $image_model,
          'file_model'            => $file_model,
          'vec_config'            => $vec_config,
          'vec_translated_models' => $vec_translated_models,
          'vec_seo_models'        => $vec_seo_models,
          'default_language'      => $default_language,
          'vec_extra_languages'   => $vec_extra_languages,
          'form_id'               => $form_id,
        ]);
      ?>
    </div>
  </div>
</div>