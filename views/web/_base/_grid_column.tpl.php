<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for WebContent model
|--------------------------------------------------------------------------
|
| Available variables:
| $model: CGridView model
| $column: Column name
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "TITLE"
    |--------------------------------------------------------------------------
    */
    case 'title':
  ?>
    <?php $que_label = $model->title; ?>
    <?php
      /*
      if ( $model->translations ) :
      ?>
      <?php
        $vec_translated_title = [];
        foreach ( $model->translations as $translated_content_model )
        {
          $vec_translated_title[] = $translated_content_model->title;
        }
        $que_label .= ' <span class="translated-name-field">('. implode(", ", $vec_translated_title) .')</span>';
      ?>
    <?php endif; */ ?>
    <?= Html::link($que_label, ['update', 'id' => $model->content_id]); ?>
    <?php if ( ! $model->is_enabled() ) : ?>
      <div class="badge badge-danger ml-5 mr-5" data-toggle="tooltip" data-placement="right" data-original-title="<?= $model->disable_date; ?>">DESACTIVADO</div>
    <?php endif; ?>
    <?php if ( $model->load_config() && $model->is_config('subtitle') && !empty($model->subtitle) ) : ?>
      <p><?= $model->subtitle; ?></p>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "image"
    |--------------------------------------------------------------------------
    */
    case 'image':
  ?>
    <?php
      if ( $model->image ) :
    ?>
      <a href="<?= Url::to("/web/". Yii::currentController(true) ."/update", ['id' => $model->content_id]); ?>"><img src="<?= $model->image->image_url('small'); ?>"></a>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "file"
    |--------------------------------------------------------------------------
    */
    case 'file':
  ?>
    <?php
      if ( $model->file ) :
    ?>
      <a href="<?= $model->file->download_url(); ?>" target="_blank"><?= $model->file->title(); ?></a>
      <br><span><?= $model->file->updated_date; ?> <?= Yii::t('app', 'by'); ?> <?= $model->file->updatedUser->fullname(); ?></span>
    <?php endif; ?>
  <?php break; ?>
<?php endswitch; ?>