<?php
/*
|--------------------------------------------------------------------------
| Form partial page for SEO models
|--------------------------------------------------------------------------
|
| Available variables:
|  - $vec_config: WebContent configuration options
|  - $default_language: Default language code
|  - $vec_extra_languages: Array with Extra languages
|  - $vec_seo_models: Array with SEO models
|  - $model: Product model class
|  - $current_action: Current action name
|
*/

  use dz\helpers\StringHelper;
  use dz\helpers\Url;
?>
<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title"><?= Yii::t('app', 'SEO information (optional)') ;?></h3>
  </header>
  <div class="panel-body">
    <?php
      /**
       * EXTRA LANGUAGES in TABS
       */
      if ( $current_action != 'create' && $vec_config['is_multilanguage'] ) :
    ?>
      <div class="nav-tabs-horizontal pt-10" data-plugin="tabs">
        <ul class="nav nav-tabs nav-tabs-line tabs-line-top" role="tablist">
          <li class="nav-item" role="presentation">
            <a class="nav-link active" data-toggle="tab" href="#lang-seo-<?= $default_language; ?>" aria-controls="lang-seo-<?= $default_language; ?>" role="tab" aria-expanded="false" data-language="<?= $default_language; ?>"><span class="flag-icon flag-icon-<?= $default_language; ?>"></span> <?= StringHelper::strtoupper(Yii::app()->i18n->language_name($default_language)); ?></a>
          </li>
          <?php if ( isset($vec_extra_languages) && !empty($vec_extra_languages) ) : ?>
            <?php foreach ( $vec_extra_languages as $language_id ) : ?>
              <?php if ( isset($vec_seo_models[$language_id]) ) : ?>
                <li class="nav-item" role="presentation">
                  <a class="nav-link" data-toggle="tab" href="#lang-seo-<?= $language_id; ?>" aria-controls="lang-seo-<?= $language_id; ?>" role="tab" aria-expanded="false" data-language="<?= $language_id; ?>"><span class="flag-icon flag-icon-<?= $language_id; ?>"></span> <?= StringHelper::strtoupper(Yii::app()->i18n->language_name($language_id)); ?></a>
                </li>
              <?php endif; ?>
            <?php endforeach; ?>
          <?php endif; ?>
        </ul>
      </div>
    <?php endif; ?>

    <div class="tab-content pt-20">
      <?php foreach ( $vec_seo_models as $language_id => $seo_model ) : ?>
        <div class="tab-pane<?php if ( $language_id == $default_language ) : ?> active<?php endif; ?>" id="lang-seo-<?= $language_id; ?>" role="tabpanel" aria-expanded="false">
          <div class="form-group row<?php if ( $seo_model->hasErrors('url_manual') ) : ?> has-danger<?php endif; ?>">
            <?= $form->label($seo_model, 'url_manual', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
            <div class="col-lg-10">
              <?php if ( $vec_config['is_editable'] ) : ?>
                <div class="input-group">
                  <span class="input-group-addon"><?= Url::base() .'/'; ?><?php if ( $language_id != $default_language ) : ?><?= $language_id; ?>/<?php endif; ?></span>
                  <?=
                    $form->textField($seo_model, 'url_manual', [
                      'maxlength'   => 255,
                      'name'        => 'Seo['. $language_id .'][url_manual]',
                      'placeholder' => ''
                    ]);
                  ?>
                </div>
                <?= $form->error($seo_model,'url_manual'); ?>
                <?php if ( !empty($seo_model->url_auto) AND $seo_model->url_auto != $seo_model->url_manual ) : ?>
                  <p class="help-block">URL suggested automatically: <code><?= Url::base() . '/'; ?><?php if ( $language_id != $default_language ) : ?><?= $language_id; ?>/<?php endif; ?><?= $seo_model->url_auto; ?></code></p>
                <?php else : ?>
                  <p class="help-block">If you do not enter an URL, it will be automatically generated taking the <em>Title</em> field value.</p>
                <?php endif; ?>
              <?php else : ?>
                <div class="item-content"><?= $seo_model->url_manual; ?></div>
              <?php endif; ?>
            </div>
          </div>

          <div class="form-group row<?php if ( $seo_model->hasErrors('meta_title') ) : ?> has-danger<?php endif; ?>">
            <?= $form->label($seo_model, 'meta_title', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
            <div class="col-lg-10">
              <?php if ( $vec_config['is_editable'] ) : ?>
                <?=
                  $form->textField($seo_model, 'meta_title', [
                    'maxlength'   => 255,
                    'name'        => 'Seo['. $language_id .'][meta_title]',
                    'placeholder' => ''
                  ]);
                ?>
                <?= $form->error($seo_model,'meta_title'); ?>
                <p class="help-block">By default, the <em>Title</em> field value will be assigned.</p>
              <?php else : ?>
                <div class="item-content"><?= $seo_model->meta_title; ?></div>
              <?php endif; ?>
            </div>
          </div>

          <?php
            /*
            |--------------------------------------------------------------------------
            | META DESCRIPTION
            |--------------------------------------------------------------------------
            */
            if ( $model->is_config('body') ) :
          ?>
            <div class="form-group row<?php if ( $seo_model->hasErrors('meta_description') ) : ?> has-danger<?php endif; ?>">
              <?= $form->label($seo_model, 'meta_description', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
              <div class="col-lg-10">
                <?php if ( $vec_config['is_editable'] ) : ?>
                  <?=
                    $form->textArea($seo_model, 'meta_description', [
                      'rows'  => 3,
                      'name'  => 'Seo['. $language_id .'][meta_description]'
                    ]);
                  ?>
                  <?= $form->error($seo_model,'meta_description'); ?>
                  <?php /*<p class="help-block">By default, the <em>Body</em> field value will be assigned.</p>*/ ?>
                <?php else : ?>
                  <div class="item-content"><?= $seo_model->meta_description; ?></div>
                <?php endif; ?>
              </div>
            </div>
          <?php endif; ?>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</div>