<?php
/*
|--------------------------------------------------------------------------
| Admin list page for WebContent model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: WebContent model class
|  - $vec_config: Content type configuration
|
*/  
  use dz\helpers\Html;
  
  // Page title
  $this->pageTitle = Yii::t('app', $model->text('index_title'));
?>
<div class="page-main">
  <div class="page-header">
    <h1 class="page-title"><?= $this->pageTitle; ?></h1>
    <?php
      if ( $vec_config['is_editable'] && Yii::app()->user->checkAccess("web.page.create") ):
    ?>
      <div class="page-header-actions">
        <?=
          Html::link('<i class="icon wb-plus"></i> '.  $model->text('add_button'), ["create"], [
            'class' => 'btn btn-primary'
          ]);
        ?>
      </div>
    <?php endif; ?>
  </div>
  <div class="page-content">
    <div class="row row-lg">
      <?php if ( isset($vec_config['is_sortable']) && $vec_config['is_sortable'] ) : ?>
        <div class="col-lg-3">
          <div class="panel panel-tree content-tree-wrapper">
            <header class="panel-heading">
              <h3 class="panel-title"><?= Yii::t('app', $model->text('panel_title')); ?></h3>
            </header>
            <div class="panel-body">
              <?php
                /*
                |--------------------------------------------------------------------------
                | CONTENT TREE (TABLE OF CONTENT)
                |--------------------------------------------------------------------------
                */
                $this->renderPartial($model->get_view_path('_tree'), [
                  'model'       => $model,
                  'vec_config'  => $vec_config,
                  'is_ajax'     => false
                ]);
              ?>
            </div>
          </div>
        </div>
        <div class="col-lg-9">
      <?php else : ?>
        <div class="col-lg-12">
      <?php endif; ?>
        <div class="panel panel-content-summary panel-<?= $this->content_type; ?>-summary panel-top-summary">
          <header class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('app', $model->text('list_title')); ?></h3>
          </header>
          <div class="panel-body container-fluid">
            <?php
              $vec_columns = [];

              // Show images
              if ( $model->is_config('image_id') )
              {
                $vec_columns[] = [
                  'name' => 'image_id',
                  'filter' => false,
                  'value' => 'trim($this->grid->getOwner()->renderPartial($data->get_view_path("_grid_column"), ["column" => "image", "model" => $data]))',
                ];
              }

              // Title
              $vec_columns[] = [
                'name' => 'title',
                'value' => 'trim($this->grid->getOwner()->renderPartial($data->get_view_path("_grid_column"), ["column" => "title", "model" => $data]))',
              ];

              // Show files
              if ( $model->is_config('file_id') )
              {
                $vec_columns[] = [
                  'name' => 'file_id',
                  'value' => 'trim($this->grid->getOwner()->renderPartial($data->get_view_path("_grid_column"), ["column" => "file", "model" => $data]))',
                ];
              }

              // Actions
              $vec_actions = [
                'class' => 'dz.grid.ButtonColumn',
                'header' => 'ACTION',
                'viewButton' => false
              ];

              // Extra actions?
              if ( ( isset($vec_config['is_disable_allowed']) && $vec_config['is_disable_allowed'] ) || ( isset($vec_config['is_delete_allowed']) && $vec_config['is_delete_allowed'] ) )
              {
                $vec_actions['menuAction'] = function($data, $row) {
                  $vec_content_config = $data->load_config();
                  $vec_menu_actions = [];

                  // Enable / disable?
                  if ( isset($vec_content_config['is_disable_allowed']) && $vec_content_config['is_disable_allowed'] )
                  {
                    $vec_menu_actions[] = [
                      'label' => Yii::t('app', 'Enable'),
                      'icon' => 'arrow-up',
                      'url' => ['enable', 'id' => $data->content_id],
                      'visible' => '!$data->is_enabled() && Yii::app()->user->checkAccess("web.page.create")',
                      'confirm' => $data->text('enable_confirm'),
                      'htmlOptions' => [
                        'id' => 'enable-content-'. $data->content_type .'-'. $data->content_id,
                        'data-gridview' => 'content-'. $data->content_type .'-grid'
                      ]
                    ];

                    $vec_menu_actions[] = [
                      'label' => Yii::t('app', 'Disable'),
                      'icon' => 'arrow-down',
                      'url' => ['disable', 'id' => $data->content_id],
                      'visible' => '$data->is_enabled() && Yii::app()->user->checkAccess("web.page.create")',
                      'confirm' => $data->text('disable_confirm'),
                      'htmlOptions' => [
                        'id' => 'disable-content-'. $data->content_type .'-'. $data->content_id,
                        'data-gridview' => 'content-'. $data->content_type .'-grid'
                      ]
                    ];
                  }

                  // Delete?
                  if ( isset($vec_content_config['is_delete_allowed']) && $vec_content_config['is_delete_allowed'] )
                  {
                    if ( !empty($vec_menu_actions) )
                    {
                      $vec_menu_actions[] = '---';
                    }

                    $vec_menu_actions[] = [
                      'label' => Yii::t('app', 'Delete'),
                      'icon' => 'trash',
                      'url' => ['delete', 'id' => $data->content_id],
                      'visible' => '$data->is_enabled() && Yii::app()->user->checkAccess("web.page.create")',
                      'confirm' => $data->text('delete_confirm'),
                      'htmlOptions' => [
                        'id' => 'delete-content-'. $data->content_type .'-'. $data->content_id,
                        'data-content-type' => $data->content_type,
                        'data-gridview' => 'content-'. $data->content_type .'-grid',
                        'data-js-after-ajax-update' => 'dzAfterDeleteContentGrid',
                      ]
                    ];
                  }

                  return $vec_menu_actions;
                };
              }

              $vec_columns[] = $vec_actions;

              /*
              |----------------------------------------------------------------------------------------
              | GridView widget
              |----------------------------------------------------------------------------------------
              */
                $vec_gridview_options = [
                  'id'                => 'content-'. $this->content_type .'-grid',
                  'dataProvider'      => $model->search(),
                  'filter'            => $model,
                  'emptyText'         => $model->text('empty_text'),
                  'enableHistory'     => true,
                  'loadModal'         => true,
                  'enableSorting'     => false,
                  'type'              => ['striped', 'hover'],
                  'beforeAjaxUpdate'  => 'js:function() { $("html, body").animate({scrollTop: 0}, 100); }',
                  'columns'           => $vec_columns
                ];

                if ( isset($vec_config['is_sortable']) )
                {
                  $vec_gridview_options['afterAjaxUpdate'] = 'js:function() { $.nestableContentReload("'. $this->content_type .'"); }';
                }

                // Load gridview
                $this->widget('dz.grid.GridView', $vec_gridview_options);
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>