<?php
/*
|--------------------------------------------------------------------------
| Form partial page for WebContent tree widget
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: WebContent model class
|  - $vec_config: WebContent configuration options
|  - $is_ajax: Is this partial loaded via AJAX?
*/
  use dz\modules\web\models\WebContent;
  use dz\helpers\Html;
  use dz\helpers\Url;

  $current_controller = Yii::currentController(true);

  // Get all the content models of this content_type
  $vec_content_models = Yii::app()->contentManager->content_list($model->content_type);
?>
<?php if ( !$is_ajax ) : ?>
  <div id="content-loading-tree" class='dz-loading center hide'></div>
  <div class="dd dd-content-group" id="content-<?= $model->content_type; ?>-nestable-wrapper" data-name="content-<?= $model->content_type; ?>" data-url="<?= Url::to("/web/". $current_controller ."/updateWeight"); ?>?id=0">
<?php endif; ?>
<?php if ( !empty($vec_content_models) ) : ?>
  <ol class="dd-list">
    <?php foreach ( $vec_content_models as $content_model ) : ?>
      <li class="dd-item dd3-item dd-item-group dd-level1" data-rel="level1" data-id="<?= $content_model->content_id; ?>" id="dd-item-<?= $content_model->content_id; ?>">
        <div class="dd-handle dd3-handle"></div>
        <div class="dd3-content">
          <?php
            $vec_html_attributes = [];
            if ( ! $content_model->is_enabled() )
            {
              $vec_html_attributes['class'] = 'text-danger';
            }
            echo Html::link($content_model->title, ['//web/'. $current_controller .'/update', 'id' => $content_model->content_id], $vec_html_attributes);
          ?>
        </div>
      </li>
    <?php endforeach; ?>
  </ol>
<?php else : ?>
  <p><?= $model->text('empty_text'); ?></p>
<?php endif; ?>
<?php if ( ! $is_ajax ) : ?>
  </div>
<?php endif; ?>
<?php if ( ! $is_ajax && $vec_config['is_editable'] ) : ?>
  <hr>
  <div class="buttons">
    <?php
      // Add new web content
      echo Html::link('<i class="wb-plus"></i> '. $model->text('add_button') , ['//web/'. $current_controller .'/create'], [
          'class' => 'btn mr-10 mb-10 btn-primary',
      ]);
    ?>
  </div>
<?php endif; ?>
<?php
  // Custom Javascript nestable code for this page
  if ( ! $is_ajax )
  {
    Yii::app()->clientscript
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/jquery-nestable/jquery.nestable.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/js/dz.nestable.js', CClientScript::POS_END);
  }
    
  if ( ! $vec_config['is_editable'] )
  {
    // Load content nestable
    Yii::app()->clientscript->registerScript('content_tree_js',
      "$('#content-". $model->content_type ."-nestable-wrapper').dzNestable({
          maxDepth: 1,
          readOnly: true
      });"
      , CClientScript::POS_READY);
  }
  else
  {
    // Load content nestable
    Yii::app()->clientscript->registerScript('content_tree_js',
      "$('#content-". $model->content_type ."-nestable-wrapper').dzNestable({
        maxDepth: 1
      });"
      , CClientScript::POS_READY);
  }
?>