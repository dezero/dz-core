<?php
/*
|--------------------------------------------------------------------------
| Form partial page for WebContent model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $content_model: WebContent model class
|  - $image_model: AssetImage model class
|  - $file_model: AssetFile model class
|  - $vec_config: WebContent configuration options
|  - $vec_translated_models: Array with translated WebContent models
|  - $default_language: Default language code
|  - $vec_extra_languages: Array with Extra languages
|  - $vec_seo_models: Array with SEO models
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  
  $current_controller = Yii::currentController();
  $current_action = Yii::currentAction(true);

  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class'   => 'form-horizontal web-content-form '. $this->content_type .'-form',
      'enctype' => 'multipart/form-data',
      'autocomplete' => 'off'
    ]
  ]);

  // Error summary
  if ( $current_action != 'create' && isset($vec_translated_models) && !empty($vec_translated_models) )
  {
    $errors = $form->errorSummary(CMap::mergeArray([$content_model], array_values($vec_translated_models)));  
  }
  else
  {
    $errors = $form->errorSummary($content_model);
  }
  if ( $errors )
  {
    echo $errors;
  }
?>

<input name="StatusChange" id="status-change" class="form-control" type="hidden" value="">

<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title"><?= $content_model->text('entity_label'); ?> Information</h3>
  </header>
  <div class="panel-body">
    <?php
      /**
       * EXTRA LANGUAGES in TABS
       */
      if ( $current_action != 'create' && $vec_config['is_multilanguage'] ) :
    ?>
      <div class="nav-tabs-horizontal pt-10" data-plugin="tabs">
        <ul class="nav nav-tabs nav-tabs-line tabs-line-top" role="tablist">
          <li class="nav-item" role="presentation">
            <a class="nav-link active" data-toggle="tab" href="#lang-<?= $default_language; ?>" aria-controls="lang-<?= $default_language; ?>" role="tab" aria-expanded="false" data-language="<?= $default_language; ?>"><span class="flag-icon flag-icon-<?= $default_language; ?>"></span> <?= StringHelper::strtoupper(Yii::app()->i18n->language_name($default_language)); ?></a>
          </li>
          <?php if ( isset($vec_extra_languages) && !empty($vec_extra_languages) ) : ?>
            <?php foreach ( $vec_extra_languages as $language_id ) : ?>
              <?php if ( isset($vec_translated_models[$language_id]) ) : ?>
                <li class="nav-item" role="presentation">
                  <a class="nav-link" data-toggle="tab" href="#lang-<?= $language_id; ?>" aria-controls="lang-<?= $language_id; ?>" role="tab" aria-expanded="false" data-language="<?= $language_id; ?>"><span class="flag-icon flag-icon-<?= $language_id; ?>"></span> <?= StringHelper::strtoupper(Yii::app()->i18n->language_name($language_id)); ?></a>
                </li>
              <?php endif; ?>
            <?php endforeach; ?>
          <?php endif; ?>
        </ul>
      </div>
      <div class="tab-content pt-20">
        <div class="tab-pane active" id="lang-<?= $default_language; ?>" role="tabpanel" aria-expanded="false">
    <?php endif; ?>

    <?php
      /*
      |--------------------------------------------------------------------------
      | TITLE
      |--------------------------------------------------------------------------
      */
      if ( $content_model->is_config('title') ) :
    ?>
      <div class="form-group row<?php if ( $content_model->hasErrors('title') ) : ?> has-danger<?php endif; ?>">
        <?= Html::label($content_model->get_label('title'), $content_model->resolve_id('title'), ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
        <div class="col-lg-10">
          <?php if ( $vec_config['is_editable'] ) : ?>
            <?=
              $form->textField($content_model, 'title', [
                'maxlength' => 255,
                'placeholder' => ''
              ]);
            ?>
          <?php else : ?>
            <div class="item-content"><?= $content_model->title; ?></div>
          <?php endif; ?>
          <?= $form->error($content_model,'title'); ?>
        </div>
      </div>
    <?php endif; ?>

    <?php
      /*
      |--------------------------------------------------------------------------
      | SUBTITLE
      |--------------------------------------------------------------------------
      */
      if ( $content_model->is_config('subtitle') ) :
    ?>
      <div class="form-group row<?php if ( $content_model->hasErrors('subtitle') ) : ?> has-danger<?php endif; ?>">
        <?= Html::label($content_model->get_label('subtitle'), $content_model->resolve_id('subtitle'), ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
        <div class="col-lg-10">
          <?php if ( $vec_config['is_editable'] ) : ?>
            <?=
              $form->textField($content_model, 'subtitle', [
                'maxlength' => 255,
                'placeholder' => ''
              ]);
            ?>
          <?php else : ?>
            <div class="item-content"><?= $content_model->subtitle; ?></div>
          <?php endif; ?>
          <?= $form->error($content_model,'subtitle'); ?>
        </div>
      </div>
    <?php endif; ?>

    <?php
      /*
      |--------------------------------------------------------------------------
      | BODY
      |--------------------------------------------------------------------------
      */
      if ( $content_model->is_config('body') ) :
    ?>
      <div class="form-group row<?php if ( $content_model->hasErrors('body') ) : ?> has-danger<?php endif; ?>">
        <?= Html::label($content_model->get_label('body'), $content_model->resolve_id('body'), ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
        <div class="col-lg-10">
          <?php if ( $vec_config['is_editable'] ) : ?>
            <?php
              if ( $content_model->is_config('body', 'plugin') && $content_model->get_config('body', 'plugin') === 'redactor' )
              {
                $this->widget('@lib.redactor.ERedactorWidget', [
                  'model'     => $content_model,
                  'attribute' => 'body',
                  'options'   => [
                    'minHeight' => $content_model->is_config('body', 'min_height') ? $content_model->get_config('body', 'min_height') : 300
                  ]
                ]);
              }
              else
              {
                $this->widget('@ext.DzMarkdown.DzMarkdown', [
                  'model'     => $content_model,
                  'attribute' => 'body',
                  'options'   => [
                    'minHeight' => $content_model->is_config('body', 'min_height') ? $content_model->get_config('body', 'min_height') : 300
                  ],
                  'htmlOptions' => [
                    'name'        => $content_model->resolve_name('body'),
                  ]
                ]);
              }
            ?>
          <?php else : ?>
            <div class="item-content"><?= $content_model->body; ?></div>
          <?php endif; ?>
          <?= $form->error($content_model,'body'); ?>
        </div>
      </div>
    <?php endif; ?>

    <?php
      /*
      |--------------------------------------------------------------------------
      | IMAGE
      |--------------------------------------------------------------------------
      */
      if ( $content_model->is_config('image_id') && isset($image_model) && !empty($image_model) ) :
    ?>
      <div class="form-group row<?php if ( $image_model->hasErrors('file_name') ) : ?> has-danger<?php endif; ?>">
        <?= Html::label($content_model->get_label('image_id'), 'AssetImage_0_file_name', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
        <div class="col-lg-10">
          <?php
            $this->widget('\dz\widgets\Upload', [
              'name'      => 'AssetImage[0][file_name]',
              'model'     => $image_model,
              'attribute' => 'file_name',
              'view_mode' => false,
              'is_image'  => true,
            ]);
          ?>
          <p class="help-block">El tamaño debe ser <u>400x200 pixels</u> o superior pero manteniendo el ratio. Por ejemplo, 600x300 o 800x400.</p>
          <?= $form->error($image_model,'file_name'); ?>
        </div>
      </div>
    <?php endif; ?>

    <?php
      /*
      |--------------------------------------------------------------------------
      | FILE
      |--------------------------------------------------------------------------
      */
      if ( $content_model->is_config('file_id') && isset($file_model) && !empty($file_model) ) :
    ?>
      <div class="form-group row<?php if ( $file_model->hasErrors('file_name') ) : ?> has-danger<?php endif; ?>">
        <?= Html::label($content_model->get_label('file_id'), 'AssetFile_1_file_name', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
        <div class="col-lg-10">
          <?php
            $this->widget('\dz\widgets\Upload', [
              'name'      => 'AssetFile[1][file_name]',
              'model'     => $file_model,
              'attribute' => 'file_name',
              'view_mode' => false,
              'is_image'  => false,
            ]);
          ?>
          <?= $form->error($file_model,'file_name'); ?>
        </div>
      </div>
    <?php endif; ?>

    <?php
      /*
      |--------------------------------------------------------------------------
      | LINK TITLE
      |--------------------------------------------------------------------------
      */
      if ( $content_model->is_config('link_title') ) :
    ?>
      <div class="form-group row<?php if ( $content_model->hasErrors('link_title') ) : ?> has-danger<?php endif; ?>">
        <?= Html::label($content_model->get_label('link_title'), $content_model->resolve_id('link_title'), ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
        <div class="col-lg-10">
          <?=
            $form->textField($content_model, 'link_title', [
              'name'        => $content_model->resolve_name('link_title'),
              'maxlength'   => 255,
              'placeholder' => '',
            ]);
          ?>
          <?= $form->error($content_model, 'link_title'); ?>
        </div>
      </div>
    <?php endif; ?>

    <?php
      /*
      |--------------------------------------------------------------------------
      | LINK URL
      |--------------------------------------------------------------------------
      */
      if ( $content_model->is_config('link_url') ) :
    ?>
      <div class="form-group row<?php if ( $content_model->hasErrors('link_url') ) : ?> has-danger<?php endif; ?>">
        <?= Html::label($content_model->get_label('link_url'), $content_model->resolve_id('link_url'), ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
        <div class="col-lg-10">
          <?=
            $form->textField($content_model, 'link_url', [
              'name'        => $content_model->resolve_name('link_url'),
              'maxlength'   => 255,
              'placeholder' => '',
            ]);
          ?>
          <p class="help-block"><?= Yii::t('app', 'The link must start with http:// or https://'); ?></p>
          <?= $form->error($content_model, 'link_url'); ?>
        </div>
      </div>
    <?php endif; ?>

    <?php
      /*
      |--------------------------------------------------------------------------
      | START DATE
      |--------------------------------------------------------------------------
      */
      if ( $content_model->is_config('start_date') ) :
    ?>
      <div class="form-group row<?php if ( $content_model->hasErrors('start_date') || $content_model->hasErrors('expiration_date') ) : ?> has-danger<?php endif; ?>">
        <label class="col-lg-2 col-sm-2 form-control-label" for="<?= $content_model->resolve_name('start_date'); ?>"><?= Yii::t('app', 'Dates'); ?></label>
        <div class="col-lg-10">
          <div id="web-content-date-range" class="input-daterange">
            <div class="input-group">
              <span class="input-group-addon">
                <i class="icon wb-calendar" aria-hidden="true"></i>
              </span>
              <?=
                $form->textField($content_model, 'start_date', [
                  'placeholder' => '',
                  'maxlength' => 10
                ]);
              ?>
            </div>
            <div class="input-group">
              <span class="input-group-addon">to</span>
              <?=
                $form->textField($content_model, 'expiration_date', [
                  'placeholder' => '',
                  'maxlength' => 10
                ]);
              ?>
            </div>
          </div>
          <p class="help-block"><?= Yii::t('app', 'If no date is defined, it will be always visible.<br>You can define only a start date (no expiration date is needed)'); ?></p>
          <?= $form->error($content_model, 'start_date'); ?>
          <?= $form->error($content_model, 'expiration_date'); ?>
        </div>
      </div>
    <?php endif; ?>

    <?php
      /*
      |--------------------------------------------------------------------------
      | EXTRA LANGUAGES in TABS
      |--------------------------------------------------------------------------
      */
      if ( $current_action != 'create' && $vec_config['is_multilanguage'] ) :
    ?>
      </div><!-- .tab-pane -->
      <?php if ( isset($vec_extra_languages) && !empty($vec_extra_languages) ) : ?>
        <?php foreach ( $vec_extra_languages as $language_id ) : ?>
          <?php if ( isset($vec_translated_models[$language_id]) ) : ?>
            <?php $translated_content_model = $vec_translated_models[$language_id]; ?>
            <div class="tab-pane" id="lang-<?= $language_id; ?>" role="tabpanel" aria-expanded="false">

              <?php
                /*
                |--------------------------------------------------------------------------
                | TITLE (translation)
                |--------------------------------------------------------------------------
                */
                if ( $content_model->is_config('title') && $content_model->is_config('title', 'is_translatable') ) :
              ?>
                <div class="form-group row<?php if ( $translated_content_model->hasErrors('title') ) : ?> has-danger<?php endif; ?>">
                  <?= $form->label($translated_content_model, 'title', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
                  <div class="col-lg-10">
                    <?=
                      $form->textField($translated_content_model, 'title', [
                        'name'        => 'TranslatedContent['. $language_id .'][title]',
                        'placeholder' => '',
                      ]);
                    ?>
                    <?= $form->error($translated_content_model,'title'); ?>
                  </div>
                </div>
              <?php endif; ?>

              <?php
                /*
                |--------------------------------------------------------------------------
                | BODY (translation)
                |--------------------------------------------------------------------------
                */
                if ( $content_model->is_config('body') && $content_model->is_config('body', 'is_translatable') ) :
              ?>
                <div class="form-group row<?php if ( $translated_content_model->hasErrors('body') ) : ?> has-danger<?php endif; ?>">
                  <?= $form->label($translated_content_model, 'body', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
                  <div class="col-lg-10">
                    <?php
                      $this->widget('@ext.DzMarkdown.DzMarkdown', [
                        'model'     => $translated_content_model,
                        'attribute' => 'body',
                        'options'   => [
                          'minHeight' => $content_model->is_config('body', 'min_height') ? $content_model->get_config('body', 'min_height') : 300,
                        ],
                        'htmlOptions' => [
                          'name'        => 'TranslatedContent['. $language_id .'][body]',
                        ]
                      ]);
                    ?>
                    <?= $form->error($translated_content_model,'body'); ?>
                  </div>
                </div>
              <?php endif; ?>

            </div><!-- .tab-pane -->
          <?php endif; ?>
        <?php endforeach; ?>
      <?php endif; ?>
      </div><!-- .tab-content -->
    <?php endif; ?>
  </div>
</div>

<?php
  /*
  |--------------------------------------------------------------------------
  | SEO BLOCK
  |--------------------------------------------------------------------------
  */
  if ( isset($vec_config['is_seo_fields']) && $vec_config['is_seo_fields'] && !empty($vec_seo_models) ) :
?>
  <?php
    // SEO form partial
    $this->renderPartial($content_model->get_view_path('_form_seo'), [
      'form'                => $form,
      'model'               => $content_model,
      'vec_config'          => $vec_config,
      'vec_seo_models'      => $vec_seo_models,
      'default_language'    => $default_language,
      'vec_extra_languages' => $vec_extra_languages,
      'current_action'      => $current_action
    ]);
  ?>
<?php endif; ?>

<?php
  /*
  |--------------------------------------------------------------------------
  | ACTION
  |--------------------------------------------------------------------------
  */
?>  
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
        $this->widget('@bootstrap.widgets.TbButton', [
          'buttonType'  => 'submit',
          'type'        => 'primary',
          'label'       => Yii::t('app', 'Save'),
        ]);
        
        // Cancel
        echo Html::link(Yii::t('app', 'Cancel'), ['/web/'. $current_controller], ['class' => 'btn btn-dark']);

        // Right actions: disable, enable or delete
        if ( $current_action !== 'create' )
        {
          if ( isset($vec_config['is_disable_allowed']) && $vec_config['is_disable_allowed'] )
          {
            // Disable button
            if ( $content_model->is_enabled() )
            {
              echo Html::link(Yii::t('app', 'Disable'), ['#'], [
                'id'            => 'disable-content-'. $this->content_type .'-btn',
                'class'         => 'btn btn-danger right',
                'data-confirm'  => $content_model->text('disable_confirm'),
                'data-form'     => $form_id,
                'data-value'    => 'disable',
                'data-plugin'   => 'dz-status-button'
              ]);
            }

            // Enable button
            else
            {
              echo Html::link(Yii::t('app', 'Enable'), ['#'], [
                'id'            => 'enable-content-'. $this->content_type .'-btn',
                'class'         => 'btn btn-success right',
                'data-confirm'  => $content_model->text('enable_confirm'),
                'data-form'     => $form_id,
                'data-value'    => 'enable',
                'data-plugin'   => 'dz-status-button'
              ]);
            }

            // Delete product button
            if ( Yii::app()->user->id == 1 || ( isset($vec_config['is_delete_allowed']) && $vec_config['is_delete_allowed'] ) )
            {
              echo Html::link(Yii::t('app', 'Delete'), ['#'], [
                'id'            => 'delete-content-'. $this->content_type .'-btn',
                'class'         => 'btn btn-delete right',
                'data-confirm'  => $content_model->text('delete_confirm'),
                'data-form'     => $form_id,
                'data-value'    => 'delete',
                'data-plugin'   => 'dz-status-button'
              ]);
            }
          }
        }
      ?>
    </div><!-- form-actions -->
  </div>

<?php
  // End model form
  $this->endWidget();
?>