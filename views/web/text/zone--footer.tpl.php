<?php
/*
|--------------------------------------------------------------------------
| Footer zone with HELP content: tokens replacement and guide text format
|--------------------------------------------------------------------------
|
| Available variables:
|
*/
?>
<a name="guide-format"></a>
<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title">Guía de Formato de Textos</h3>
  </header>
  <div class="panel-body">
    <div class="form-group row">
      <div class="col-lg-10">
        <h5 class="mt-10"></h5>
        <p>Se utiliza <strong>Textile</strong> para el formato. Esto es lo básico. Consulte la <a href="https://txstyle.org/" target="_blank">sintaxis completa</a>.</p>
        <ul class="textile-help">
          <li><strong>Cabeceras</strong> (es necesario un salto de línea al final)<br><code>h1. Título de la cabecera<br><br>h2. Título de la cabecera<br><br></code></li>
          <li><strong>Negrita / bold</strong><br><code>*Texto en bold*</code></li>
          <li><strong>Cursiva</strong><br><code>_Texto en cursiva_</code></li>
          <li><strong>Enlaces</strong><br><code>"Club Vilars":https://www.club.vilarsrurals.com/</code></li>
          <li><strong>Listados</strong><br><code>* Opción A<br>** Opción A1<br>** Opción A2<br>** Opción 2<br>** Opción 3</code></li>
        <ul>
      </div>
    </div>
  </div>
</div>