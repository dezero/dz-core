<?php
/*
|--------------------------------------------------------------------------
| Form partial page for WebContent model
|--------------------------------------------------------------------------
|
| Available variables:
|   $vec_models: Array with WebContent model classes
|   $vec_text_block: Array with text block information
|   $vec_extra_languages: Array with Extra languages
|   $form_id: Form identifier
|
*/
  use dz\helpers\StringHelper;
  
  // Current action name
  $current_action = Yii::currentAction(true);

  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm',array(
    'id' => $form_id,
    'enableAjaxValidation' => FALSE,
    'htmlOptions' => array(
      'class'   => 'form-horizontal',
      'enctype' => 'multipart/form-data',
    )
  ));
?>
<?php foreach ( $vec_text_block as $text_block_id => $que_text_block ) : ?>
  <?php if ( isset($vec_models[$text_block_id]['es']) ) : ?>
    <?php
      $model = $vec_models[$text_block_id]['es'];
      $content_id = $model->content_id;

      // Column "content_id" in a hidden field
      echo $form->hiddenField($model, 'content_id', array(
        'name' => 'WebContent['. $content_id .'][content_id]'
      ));

      // Column "text_block_id" in a hidden field
      echo '<input name="WebContent['. $content_id .'][text_block_id]" type="hidden" value="'. $text_block_id .'">';
    ?>
      <div class="panel">
        <header class="panel-heading">
          <h3 class="panel-title"><?= $que_text_block['title']; ?></h3>
        </header>
        <div class="panel-body">
          <?php
            /**
             * LANGUAGE TABS
             */
            if ( !isset($que_text_block['translatable']) OR $que_text_block['translatable'] ) :
          ?>
            <div class="nav-tabs-horizontal pt-10" data-plugin="tabs">
              <ul class="nav nav-tabs nav-tabs-line tabs-line-top" role="tablist">
                <li class="nav-item" role="presentation">
                  <a class="nav-link active" data-toggle="tab" href="#lang-es-<?= $content_id; ?>" aria-controls="lang-es" role="tab" aria-expanded="false"><span class="flag-icon flag-icon-es"></span> CASTELLANO</a>
                </li>
                <?php if ( isset($vec_extra_languages) AND !empty($vec_extra_languages) ) : ?>
                  <?php foreach ( $vec_extra_languages as $language_id ) : ?>
                    <?php if ( isset($vec_models[$text_block_id][$language_id]) ) : ?>
                      <?php $translated_content_id = $vec_models[$text_block_id][$language_id]->content_id; ?>
                      <li class="nav-item" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#lang-<?= $language_id .'-'. $translated_content_id; ?>" aria-controls="lang-<?= $language_id; ?>" role="tab" aria-expanded="false"><span class="flag-icon flag-icon-<?= $language_id; ?>"></span> <?= StringHelper::uppercase(Yii::app()->i18n->language_name($language_id)); ?></a>
                      </li>
                    <?php endif; ?>
                  <?php endforeach; ?>
                <?php endif; ?>
              </ul>
            </div>
            <div class="tab-content pt-20">
              <?php
                /**
                 * SPANISH TAB
                 */
              ?>
              <div class="tab-pane active" id="lang-es-<?= $content_id; ?>" role="tabpanel" aria-expanded="false">
          <?php
            // Tabs for languages?
            endif;
          ?>
              
              <?php foreach ( $que_text_block['fields'] as $field_name => $que_field ) : ?>
                <div class="form-group row<?php if ( $model->hasErrors($field_name) ) : ?> has-danger<?php endif; ?>">
                  <label class="col-lg-2 col-sm-2 form-control-label" for="WebContent_<?= $field_name; ?>"><?= $que_field['label']; ?></label>
                  <div class="col-lg-10">
                    <?php
                      switch ( $que_field['type'] )
                      {
                        case 'textfield':
                          echo $form->textField($model, $field_name, array(
                            'name'        => 'WebContent['. $content_id .']['. $field_name .']',
                            'maxlength'   => 128,
                            'placeholder' => '',
                          ));
                        break;

                        case 'textarea':
                          $htmlOptions = array(
                            'name'        => 'WebContent['. $content_id .']['. $field_name .']',
                            'placeholder' => '',
                          );
                          if ( isset($que_field['htmlOptions']) )
                          {
                            $htmlOptions = CMap::mergeArray($htmlOptions, $que_field['htmlOptions']);
                          }
                          echo $form->textArea($model, $field_name, $htmlOptions);
                        break;

                        case 'svg_icon':
                          echo '<div class="svg-icon-wrapper">';
                          echo Html::radioButtonListControlGroup('WebContent['. $content_id .']['. $field_name .']', $model->$field_name, DzSvgIcon::model()->icon_list());
                          echo '</div>';
                        break;

                        case 'image':
                          // Image model
                          $image_model = $model->image;
                          if ( empty($image_model) )
                          {
                            $image_model = Yii::createObject(AssetImage::class);
                          }

                          $this->widget('dz.widgets.Upload', array(
                            'name'      => 'WebContent['. $content_id .']['. $field_name .']',
                            'model'     => $image_model,
                            'attribute' => 'file_name',
                            'view_mode' => FALSE,
                            'is_image'  => TRUE,
                          ));
                        break;
                      }
                    ?>
                    <?php if ( isset($que_field['description']) ) : ?>
                      <p class="help-block"><?= $que_field['description']; ?></p>
                    <?php endif; ?>
                    <?php if ( $is_show_footer AND $que_field['type'] == 'textfield' OR $que_field['type'] == 'textarea' ) : ?>
                      <p class="help-block">Ver abajo los <a href="#guide-format">formatos de texto</a></p>
                    <?php endif; ?>
                    <?= $form->error($model, $field_name); ?>
                  </div>
                </div>
              <?php endforeach; ?>

            <?php
              /**
               * TABS?
               */
              if ( !isset($que_text_block['translatable']) OR $que_text_block['translatable'] ) :
            ?>
              </div><!-- .tab-pane -->
            <?php endif; ?>
            
            <?php
              /**
               * EXTRA LANGUAGES PACK
               */
              if ( ( !isset($que_text_block['translatable']) OR $que_text_block['translatable'] ) AND isset($vec_extra_languages) AND !empty($vec_extra_languages) ) :
            ?>
              <?php foreach ( $vec_extra_languages as $language_id ) : ?>
                <?php if ( isset($vec_models[$text_block_id][$language_id]) ) : ?>
                  <?php
                    $translated_content_model = $vec_models[$text_block_id][$language_id];
                    $content_id = $translated_content_model->content_id;

                    // Column "content_id" in a hidden field
                    echo $form->hiddenField($translated_content_model, 'content_id', array(
                      'name' => 'WebContent['. $content_id .'][content_id]'
                    ));

                    // Column "text_block_id" in a hidden field
                    echo '<input name="WebContent['. $content_id .'][text_block_id]" type="hidden" value="'. $text_block_id .'">';
                  ?>
                  <div class="tab-pane" id="lang-<?= $language_id .'-'. $content_id; ?>" role="tabpanel" aria-expanded="false">
                    
                    <?php foreach ( $que_text_block['fields'] as $field_name => $que_field ) : ?>
                      <?php
                        // Check if field is translatable
                        if ( isset($que_field['translatable']) AND $que_field['translatable'] ) :
                      ?>
                        <div class="form-group row<?php if ( $translated_content_model->hasErrors($field_name) ) : ?> has-danger<?php endif; ?>">
                          <label class="col-lg-2 col-sm-2 form-control-label" for="WebContent_<?= $field_name; ?>"><?= $que_field['label_'. $language_id]; ?></label>
                          <div class="col-lg-10">
                            <?php
                              switch ( $que_field['type'] )
                              {
                                case 'textfield':
                                  echo $form->textField($translated_content_model, $field_name, array(
                                    'name'        => 'WebContent['. $content_id .']['. $field_name .']',
                                    'maxlength'   => 128,
                                    'placeholder' => '',
                                  ));
                                break;

                                case 'textarea':
                                  $htmlOptions = array(
                                    'name'        => 'WebContent['. $content_id .']['. $field_name .']',
                                    'placeholder' => '',
                                  );
                                  if ( isset($que_field['htmlOptions']) )
                                  {
                                    $htmlOptions = CMap::mergeArray($htmlOptions, $que_field['htmlOptions']);
                                  }
                                    echo $form->textArea($translated_content_model, $field_name, $htmlOptions);
                                break;

                                case 'image':
                                  // Image model
                                  $image_model = $translated_content_model->image;
                                  if ( empty($image_model) )
                                  {
                                    $image_model = Yii::createObject(AssetImage::class);
                                  }

                                  $this->widget('dz.widgets.Upload', array(
                                    'name'      => 'WebContent['. $content_id .']['. $field_name .']',
                                    'model'     => $image_model,
                                    'attribute' => 'file_name',
                                    'view_mode' => FALSE,
                                    'is_image'  => TRUE,
                                  ));
                                break;
                              }
                            ?>
                            <?php if ( isset($que_field['description_ca']) ) : ?>
                              <p class="help-block"><?= $que_field['description_ca']; ?></p>
                            <?php elseif ( isset($que_field['description']) ) : ?>
                              <p class="help-block"><?= $que_field['description']; ?></p>
                            <?php endif; ?>
                            <?php if ( $is_show_footer AND $que_field['type'] == 'textfield' OR $que_field['type'] == 'textarea' ) : ?>
                              <p class="help-block">Veure abajo los <a href="#guide-format">formatos de texto</a></p>
                            <?php endif; ?>
                            <?= $form->error($translated_content_model, $field_name); ?>
                          </div>
                        </div>
                      <?php endif; ?>
                    <?php endforeach; ?>
                    
                     <?php
                        // Special description for ADVANTAGE TABLE
                        if ( $text_block_id == 'advantage_table' AND $language_id == 'ca') :
                      ?>
                        <div class="form-group row">
                          <label class="col-lg-2 col-sm-2 form-control-label">Imatge pel mòvil</label>
                          <div class="col-lg-10 mt-10">
                            <p>Fes click en aquest botó per pujar la imatge d'avantatges en català:</p>
                            <a class="btn btn-dark" href="<?= Yii::app()->createUrl('/web/text/catalanAdvantage'); ?>">IMATGE PEL MÒVIL</a>
                          </div>
                        </div>
                      <?php endif; ?>

                  </div><!-- .tab-pane -->
                <?php endif; ?>
              <?php endforeach; ?>
            <?php endif; ?>
          <?php
            /**
             * TABS?
             */
            if ( !isset($que_text_block['translatable']) OR $que_text_block['translatable'] ) :
          ?>
            </div><!-- .tab-content -->
          <?php endif; ?>
        </div><!-- . panel-body -->
      </div><!-- .panel -->
  <?php endif; ?>
<?php endforeach; ?>

<?php if ( $form_id == 'web-advantage-form' ) : ?>
  <div class="panel">
    <header class="panel-heading">
      <h3 class="panel-title">Listado Ventajas</h3>
    </header>
    <div class="panel-body">
      <p>Haz click en el siguiente botón para administrar el contenido de todas las ventajas:</p>
      <?= Html::link('Administrar Ventajas', array("/club/advantage"), array('class' => 'btn btn-dark')); ?>
    </div>
  </div>
<?php endif; ?>

<?php
  /**
   * FOOTER help with tokens replacements and guide text formats
   */
  if( $is_show_footer )
  {
    $this->renderPartial('//web/text/zone--footer', array());
  }
?>

<?php
  /*
  |--------------------------------------------------------------------------
  | ACCIONES
  |--------------------------------------------------------------------------
  */
?> 
<div class="form-group row">
  <div class="col-lg-10 form-actions buttons mt-10">
    <?php
      // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
      $this->widget('@bootstrap.widgets.TbButton', array(
        'buttonType'=> 'submit',
        'type'      => 'primary',
        'label'     => $button,
      ));

      // More links
      // echo '<div class="btn-group dropup">'. Html::more_links($this->menuAction(\dz\db\ActiveRecord::extractPkValue($model, true)), array('class' => 'dropdown-menu')) .'</div>';
    ?>
  </div><!-- form-actions -->
</div>
<?php
  // End model form
  $this->endWidget();
?>