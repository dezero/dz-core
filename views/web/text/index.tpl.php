<?php
/*
|--------------------------------------------------------------------------
| Create/update form page for WebContent model
|--------------------------------------------------------------------------
|
| Available variables:
|   $vec_models: Array with WebContent model classes
|   $vec_text_block: Array with text block information
|   $vec_extra_languages: Array with Extra languages
|   $form_id: Form identifier
|
*/
  // Page title
  $this->pageTitle = Yii::t('app', $page_title);
?>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <?php if ( isset($vec_header_actions) ) : ?>
    <div class="page-header-actions">
      <?php foreach ( $vec_header_actions as $que_header_action ) : ?>
        <?= $que_header_action; ?>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>

<div class="page-content container-fluid">
  <?php
    /*
    |--------------------------------------------------------------------------
    | FORM PAGE
    |--------------------------------------------------------------------------
    */
    // Render form
    $this->renderPartial('//web/text/zone--form', array(
      'vec_models'          => $vec_models,
      'vec_text_block'      => $vec_text_block,
      'vec_extra_languages' => $vec_extra_languages,
      'form_id'             => $form_id,
      'is_show_footer'      => isset($is_show_footer) ? $is_show_footer : FALSE,
      'button'              => Yii::t('app', 'Guardar')
    ));
  ?>
</div>