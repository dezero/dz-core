<?php
/*
|--------------------------------------------------------------------------
| Form partial page for WebContent model (page)
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: WebContent model (page)
|  - $seo_model: SEO model
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  use dz\modules\web\models\Seo;
  use dz\modules\web\models\WebContent;

  // Current action name
  $current_action = Yii::currentAction(true);

  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal page-form',
      'enctype' => 'multipart/form-data',
      'autocomplete' => 'off'
    ]
  ]);

  // Error summary
  $errors = $form->errorSummary($model);
  if ( $errors )
  {
    echo $errors;
  }
?>
<input name="StatusChange" id="status-change" class="form-control" type="hidden" value="">
<div class="panel">
  
  <header class="panel-heading">
    <h3 class="panel-title">Página de Contenido</h3>
  </header>

  <div class="panel-body">

    <div class="form-group row<?php if ( $model->hasErrors('title') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'title', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?=
          $form->textField($model, 'title', [
            'maxlength' => 255,
            'placeholder' => ''
          ]);
        ?>
        <?= $form->error($model, 'title'); ?>
      </div>
    </div>

    <div class="form-group row<?php if ( $model->hasErrors('body') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'body', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?php
          /*
          $this->widget('@lib.redactor.ERedactorWidget', [
            'model'     => $model,
            'attribute' => 'body',
            'options'   => [
              'minHeight' => 350,
            ]
          ]);
          */
          $this->widget('@ext.DzMarkdown.DzMarkdown', [
            'model'     => $model,
            'attribute' => 'body',
            'options'   => ['minHeight' => 350]
          ]);
        ?>
      </div>
    </div>

  </div><!-- .panel-body -->
</div><!-- .panel -->

<?php
  // SEO form partial
  $this->renderPartial('_form_seo', [
    'form'          => $form,
    'model'         => $model,
    'seo_model'     => $seo_model,
    'language_id'   => 'es'
  ]);
?>

<?php
  /*
  |--------------------------------------------------------------------------
  | ACCIONES
  |--------------------------------------------------------------------------
  */
?>  
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
        $this->widget('@bootstrap.widgets.TbButton', [
          'buttonType'  => 'submit',
          'type'        => 'primary',
          'label'       => $button,
        ]);
        
        // Cancel
        echo Html::link(Yii::t('app', 'Cancel'), ['/web/page'], ['class' => 'btn btn-dark']);

        if ( $current_action !== 'create' )
        {
          // Disable page button
          if ( $model->is_enabled() )
          {
            echo Html::link(Yii::t('app', 'Disable'), ['#'], [
              'id'            => 'disable-page-btn',
              'class'         => 'btn btn-danger right',
              'data-confirm'  => '<h3>¿Estás seguro de que quieres <span class=\'text-danger\'>DESACTIVAR</span> esta página</h3>',
              'data-form'     => $form_id,
              'data-value'    => 'disable'
            ]);
          }

          // Enable page button
          else
          {
            echo Html::link(Yii::t('app', 'Enable'), ['#'], [
              'id'            => 'enable-page-btn',
              'class'         => 'btn btn-success right',
              'data-confirm'  => '<h3>¿Estás seguro de que quieres <span class=\'text-success\'>ACTIVAR</span> esta página</h3>',
              'data-form'     => $form_id,
              'data-value'    => 'enable'
            ]);
          }
        }
      ?>
    </div><!-- form-actions -->
  </div>

<?php
  // End model form
  $this->endWidget();
?>