<?php
/*
|--------------------------------------------------------------------------
| Form partial page for SEO models
|--------------------------------------------------------------------------
|
| Available variables:
|  - $seo_model: SEO model
|  - $model: WebContent model class
|
*/
  use dz\helpers\Html;
  use dz\modules\web\models\Seo;
  use dz\modules\web\models\WebContent;
?>
<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title">Datos para SEO (opcional)</h3>
  </header>
  <div class="panel-body">
    <div class="form-group row<?php if ( $seo_model->hasErrors('url_manual') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($seo_model, 'url_manual', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <div class="input-group">
          <span class="input-group-addon"><?= Yii::app()->params['baseUrl'] .'/'; ?></span>
          <?=
            $form->textField($seo_model, 'url_manual', [
              'maxlength'   => 255,
              'name'        => 'Seo['. $language_id .'][url_manual]',
              'placeholder' => ''
            ]);
          ?>
        </div>
        <?= $form->error($seo_model,'url_manual'); ?>
        <?php if ( !empty($seo_model->url_auto) AND $seo_model->url_auto != $seo_model->url_manual ) : ?>
            <p class="help-block">URL suggested automatically: <code><?= Url::base() . '/'; ?><?php if ( $language_id != Yii::defaultLanguage() ) : ?><?= $language_id; ?>/<?php endif; ?><?= $seo_model->url_auto; ?></code></p>
        <?php else : ?>
          <p class="help-block">If you do not enter an URL, it will be automatically generated taking the <em>Name</em> field value.</p>
        <?php endif; ?>
      </div>
    </div>

    <div class="form-group row<?php if ( $seo_model->hasErrors('meta_title') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($seo_model, 'meta_title', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?=
          $form->textField($seo_model, 'meta_title', [
            'maxlength'   => 255,
            'name'        => 'Seo['. $language_id .'][meta_title]',
            'placeholder' => ''
          ]);
        ?>
        <?= $form->error($seo_model,'meta_title'); ?>
        <p class="help-block">By default, the <em>Name</em> field value will be assigned.</p>
      </div>
    </div>

    <div class="form-group row<?php if ( $seo_model->hasErrors('meta_description') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($seo_model, 'meta_description', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?=
          $form->textArea($seo_model, 'meta_description', [
            'rows' => 3,
            'name' => 'Seo['. $language_id .'][meta_description]'
          ]);
        ?>
        <?= $form->error($seo_model,'meta_description'); ?>
        <?php /*<p class="help-block">By default, the <em>Description</em> field value will be assigned.</p>*/ ?>
      </div>
    </div>
  </div>
</div>