<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for WebContent model (page)
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: WebContent model
|  - $column: Column name
|
*/
  use dz\helpers\Html;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "title"
    |--------------------------------------------------------------------------
    */
    case 'title':
  ?>
    <a href="<?= Yii::app()->createAbsoluteUrl('/web/page/update', array('id' => $model->content_id)); ?>"><strong><?= $model->title; ?></strong></a>
    <?php
      // Disabled page
      if ( $model->is_disabled() ) :
    ?>
      <span class="badge badge-danger ml-5 mr-5" data-toggle="tooltip" data-placement="right" data-original-title="<?= $model->disable_date; ?>"><?= Yii::t('app', 'desactivada'); ?></span>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "URL"
    |--------------------------------------------------------------------------
    */
    case 'url':
  ?>
    <?php if ( $model->seo ) : ?>
      <?= '/'. $model->seo->url_manual; ?>
    <?php endif; ?>
  <?php break; ?>
<?php endswitch; ?>