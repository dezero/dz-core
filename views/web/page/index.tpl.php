<?php
/*
|--------------------------------------------------------------------------
| Admin list page for WebContent model (page content type)
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: WebContent model (page content type)
|
*/
  use dz\helpers\Html;

  // Page title
  $this->pageTitle = Yii::t('app', 'Administar Páginas');
?>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <?php if ( Yii::app()->user->checkAccess("web.text.create") ): ?>
    <div class="page-header-actions">
      <?=
        Html::link('<i class="icon wb-plus"></i> Añadir página', ["create"], [
          'class' => 'btn btn-primary'
        ]);
      ?>
    </div>
  <?php endif; ?>
</div>
<div class="page-content">
  <div class="row row-lg">
    <div class="col-lg-12">
      <div class="panel panel-top-summary">
        <div class="panel-body container-fluid">
          <?php
            /*
            |----------------------------------------------------------------------------------------
            | GridView widget
            |----------------------------------------------------------------------------------------
            */
              $this->widget('dz.grid.GridView', [
                'id'            => 'page-grid',
                'dataProvider'  => $model->search(),
                'filter'        => $model,
                'emptyText'     => Yii::t('app', 'No se encontraron páginas'),
                'enableHistory' => true,
                'loadModal'     => true,
                'enableSorting' => false,
                'type'          => ['striped', 'hover'],
                'beforeAjaxUpdate' => 'js:function(id, options){ $("html, body").animate({scrollTop: 0}, 100); }',
                'columns'       => [
                  [
                    'name' => 'title',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("_grid_column", ["column" => "title", "model" => $data]))',
                  ],
                  [
                    'name' => 'url',
                    'filter' => false,
                    'value' => 'trim($this->grid->getOwner()->renderPartial("_grid_column", ["column" => "url", "model" => $data]))',
                  ],
                  [
                    'class' => 'dz.grid.ButtonColumn',
                    'header' => 'ACCIÓN',
                    'template' => '{update}{delete}',
                    'deleteButton' => true,
                    'clearButton' => true,
                    'viewButton' => false,
                    'updateButton' => true,
                    'menuAction' => function($data, $row) {
                      return [
                        [
                        'label' => Yii::t('app', 'Enable'),
                        'icon' => 'arrow-up',
                        'url' => ['/settings/page/enable', 'id' => $data->content_id],
                        'visible' => '!$data->is_enabled()',
                        'confirm' => '<h3>¿Estás seguro de que quieres <span class=\'text-success\'>ACTIVAR</span> esta página</h3>',
                        'htmlOptions' => [
                          'id' => 'enable-page-'. $data->content_id,
                          'data-gridview' => 'page-grid'
                          ]
                        ],
                        [
                          'label' => Yii::t('app', 'Disable'),
                          'icon' => 'arrow-down',
                          'url' => ['/settings/page/disable', 'id' => $data->content_id],
                          'visible' => '$data->is_enabled()',
                          'confirm' => '<h3>¿Estás seguro de que quieres <span class=\'text-danger\'>DESACTIVAR</span> esta página</h3>',
                          'htmlOptions' => [
                            'id' => 'disable-page-'. $data->content_id,
                            'data-gridview' => 'page-grid'
                          ]
                        ]
                      ];
                    },
                  ],
                ],
              ]);
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>