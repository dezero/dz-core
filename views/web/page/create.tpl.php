<?php
/*
|--------------------------------------------------------------------------
| Create form page for WebContent model (page)
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: WebContent model (page)
|  - $seo_model: SEO model
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  
  // Page title
  $this->pageTitle = Yii::t('app', 'Crear página');
?>

<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => 'Administrar Páginas',
        'url' => ['/web/page/'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <?php
    // Render form
    $this->renderPartial('_form', [
      'model'     => $model,
      'seo_model' => $seo_model,
      'form_id'   => $form_id,
      'button'    => Yii::t('app', 'Añadir')
    ]);
  ?>
</div>