<?php
/*
|--------------------------------------------------------------------------
| Update form page for WebContent model (page)
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: WebContent model (page)
|  - $seo_model: SEO model
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;

  // Page title
  $this->pageTitle = $model->title();
?>

<div class="page-header">
  <h1 class="page-title">
    <?php if ( ! $model->is_enabled() ) : ?>
      <div class="badge badge-danger" data-toggle="tooltip" data-placement="bottom" data-original-title="BAJA">BAJA</div>    
    <?php endif; ?>    
  </h1>
  <div class="page-header-actions">
    <?=
      Html::link('<i class="wb-chevron-left"></i> Volver', ["/web/page"], [
        'class' => 'btn btn-dark'
      ]);
    ?>
  </div>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => 'Administrar Páginas',
        'url' => ['/web/page'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <?php
    // Render form
    $this->renderPartial('_form', [
      'model'     => $model,
      'seo_model' => $seo_model,
      'form_id'   => $form_id,
      'button'    => Yii::t('app', 'Guardar')
    ]);
  ?>
</div>