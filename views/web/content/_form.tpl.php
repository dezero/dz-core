<?php
/*
|--------------------------------------------------------------------------
| Form partial page for WebContent model (page)
|--------------------------------------------------------------------------
|
| Available variables:
|  - $vec_models: Array with WebContent model
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  use dz\modules\web\models\WebContent;

  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal page-form',
      'enctype' => 'multipart/form-data',
      'autocomplete' => 'off'
    ]
  ]);

  // Error summary
  $errors = $form->errorSummary($vec_models);
  if ( $errors )
  {
    echo $errors;
  }
?>
<input name="StatusChange" id="status-change" class="form-control" type="hidden" value="">
<?php if ( !empty($vec_models) ) : ?>
  <?php foreach ( $vec_models as $content_model ) : ?>
    <div class="panel">
      <header class="panel-heading">
        <?php if ( $content_model->is_config('block_title') ) : ?>
          <h3 class="panel-title"><?= $content_model->get_config('block_title'); ?></h3>
        <?php endif; ?>
      </header>

      <div class="panel-body">

        <?php
          /*
          |--------------------------------------------------------------------------
          | TITLE
          |--------------------------------------------------------------------------
          */
          if ( $content_model->is_config('title') ) :
        ?>
          <div class="form-group row<?php if ( $content_model->hasErrors('title') ) : ?> has-danger<?php endif; ?>">
            <?= Html::label($content_model->get_label('title'), $content_model->resolve_id('title'), ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
            <div class="col-lg-10">
              <?=
                $form->textField($content_model, 'title', [
                  'name'        => $content_model->resolve_name('title'),
                  'maxlength'   => 255,
                  'placeholder' => '',
                ]);
              ?>
              <?= $form->error($content_model, 'title'); ?>
            </div>
          </div>
        <?php endif; ?>

        <?php
          /*
          |--------------------------------------------------------------------------
          | BODY
          |--------------------------------------------------------------------------
          */
          if ( $content_model->is_config('body') ) :
        ?>
          <div class="form-group row<?php if ( $content_model->hasErrors('body') ) : ?> has-danger<?php endif; ?>">
            <?= Html::label($content_model->get_label('body'), $content_model->resolve_id('body'), ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
            <div class="col-lg-10">
              <?php
                /*
                $this->widget('@lib.redactor.ERedactorWidget', [
                  'model'     => $content_model,
                  'attribute' => 'body',
                  'options'   => [
                    'minHeight' => 350,
                  ]
                ]);
                */
                $this->widget('@ext.DzMarkdown.DzMarkdown', [
                  'model'     => $content_model,
                  'attribute' => 'body',
                  'options'   => [
                    'minHeight' => $content_model->is_config('body', 'min_height') ? $content_model->get_config('body', 'min_height') : 300
                  ],
                  'htmlOptions' => [
                    'name'        => $content_model->resolve_name('body'),
                  ]
                ]);
              ?>
            </div>
          </div>
        <?php endif; ?>

        <?php
          /*
          |--------------------------------------------------------------------------
          | LINK TITLE
          |--------------------------------------------------------------------------
          */
          if ( $content_model->is_config('link_title') ) :
        ?>
          <div class="form-group row<?php if ( $content_model->hasErrors('link_title') ) : ?> has-danger<?php endif; ?>">
            <?= Html::label($content_model->get_label('link_title'), $content_model->resolve_id('link_title'), ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
            <div class="col-lg-10">
              <?=
                $form->textField($content_model, 'link_title', [
                  'name'        => $content_model->resolve_name('link_title'),
                  'maxlength'   => 255,
                  'placeholder' => '',
                ]);
              ?>
              <?= $form->error($content_model, 'link_title'); ?>
            </div>
          </div>
        <?php endif; ?>

        <?php
          /*
          |--------------------------------------------------------------------------
          | START DATE
          |--------------------------------------------------------------------------
          */
          if ( $content_model->is_config('start_date') ) :
        ?>
          <div class="form-group row<?php if ( $content_model->hasErrors('start_date') || $content_model->hasErrors('expiration_date') ) : ?> has-danger<?php endif; ?>">
            <label class="col-lg-2 col-sm-2 form-control-label" for="<?= $content_model->resolve_name('start_date'); ?>"><?= Yii::t('app', 'Dates'); ?></label>
            <div class="col-lg-10">
              <div id="discount-date-range" class="input-daterange">
                <div class="input-group">
                  <span class="input-group-addon">
                    <i class="icon wb-calendar" aria-hidden="true"></i>
                  </span>
                  <?=
                    $form->textField($content_model, 'start_date', [
                      'placeholder' => '',
                      'maxlength' => 10
                    ]);
                  ?>
                </div>
                <div class="input-group">
                  <span class="input-group-addon">to</span>
                  <?=
                    $form->textField($content_model, 'expiration_date', [
                      'placeholder' => '',
                      'maxlength' => 10
                    ]);
                  ?>
                </div>
              </div>
              <p class="help-block"><?= Yii::t('app', 'If no date is defined, it will be always visible.<br>You can define only a start date (no expiration date is needed)'); ?></p>
              <?= $form->error($content_model, 'start_date'); ?>
              <?= $form->error($content_model, 'expiration_date'); ?>
            </div>
          </div>
        <?php endif; ?>

      </div><!-- .panel-body -->
    </div><!-- .panel -->
  <?php endforeach; ?>
<?php endif; ?>

<?php
  /*
  |--------------------------------------------------------------------------
  | ACTIONS
  |--------------------------------------------------------------------------
  */
?>  
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
        $this->widget('@bootstrap.widgets.TbButton', [
          'buttonType'  => 'submit',
          'type'        => 'primary',
          'label'       => $button,
        ]);
      ?>
    </div><!-- form-actions -->
  </div>

<?php
  // End model form
  $this->endWidget();
?>