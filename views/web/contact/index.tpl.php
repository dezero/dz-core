<?php
/*
|--------------------------------------------------------------------------
| Admin list page for WebContact model
|--------------------------------------------------------------------------
|
| Available variables:
|   $model: WebContact model class
|
*/  
  // Page title
  $this->pageTitle = Yii::t('app', 'Contact messages');
?>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
</div>
<div class="page-content">
  <div class="row row-lg">
    <div class="col-lg-12">
      <div class="panel panel-top-summary">
        <div class="panel-body container-fluid">
          <?php
            /*
            |----------------------------------------------------------------------------------------
            | GridView widget
            |----------------------------------------------------------------------------------------
            */
              $this->widget('dz.grid.GridView', array(
                'id'            => 'contact-grid',
                'dataProvider'  => $model->search(),
                'filter'        => $model,
                'emptyText'     => Yii::t('app', 'No contact messages found'),
                'enableHistory' => true,
                'loadModal'     => true,
                'enableSorting' => false,
                'type'          => ['striped', 'hover'],
                'beforeAjaxUpdate' => 'js:function(id, options){ $("html, body").animate({scrollTop: 0}, 100); }',
                'columns'       => [
                  [
                    'header' => Yii::t('app', 'Date'),
                    'name' => 'created_date',
                    'filter' => false
                  ],
                  'name',
                  'email',
                  [
                    'name' => 'message',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("//web/contact/_grid_column", ["column" => "message", "model" => $data]))',
                  ],
                  [
                    'class' => 'dz.grid.ButtonColumn',
                    'header' => Yii::t('app', 'ACTION'),
                    'template' => '{delete}',
                    'deleteButton' => true,
                    'clearButton' => true,
                    'viewButton' => false,
                    'updateButton' => false,
                  ],
                ],                
              ));
            ?>
        </div>
      </div>
    </div>
  </div>
</div>
