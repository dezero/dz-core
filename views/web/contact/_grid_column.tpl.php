<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for WebContent model (page)
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: WebContent model
|  - $column: Column name
|
*/
  use dz\helpers\Html;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "message"
    |--------------------------------------------------------------------------
    */
    case 'message':
  ?>
    <?= $model->message; ?>
  <?php break; ?>
<?php endswitch; ?>