<?php
/* @var $this OperationController|TaskController|RoleController */
/* @var $item CAuthItem */
/* @var $ancestorDp AuthItemDataProvider */
/* @var $descendantDp AuthItemDataProvider */
/* @var $formModel AddAuthItemForm */
/* @var $form TbActiveForm */
/* @var $childOptions array */
/*
$this->breadcrumbs = array(
  $this->capitalize($this->getTypeText(true)) => array('index'),
  $item->description,
);
*/
  use dz\helpers\Html;
  
  // Page title
  $this->pageTitle = $item->description;
?>
<div class="page-header">
  <h1 class="page-title"><?php echo Html::encode($this->pageTitle); ?></h1>
  <?php
    // Breadcrumbs
    echo Html::breadcrumbs(array(
      array(
        'label' => $this->capitalize($this->getTypeText(true)),
        'url' => array('index'),
      ),
      $this->pageTitle
    ));
  ?>
  <div class="page-header-actions">
    <?php // $this->widget('@bootstrap.widgets.TbButtonGroup', array(
      echo BsHtml::buttonGroup(array(
        array(
          'label'=>Yii::t('app', 'Edit'),
          'url'=>array('update', 'name'=>$item->name),
          'color' => 'primary',
          'type' => BsHtml::BUTTON_TYPE_LINK
        ),

        // Dezero Operaciones Addon
        /*
        array(
          'label' => Yii::t('app', 'Operaciones'),
          'icon' => 'check',
          'url' => array('/auth/dzAuth/task', 'task_name'=>$item->name),
          'visible' => $item->type == 1,
          'color' => 'white',
          'type' => BsHtml::BUTTON_TYPE_LINK
        ),
        */
        // Dezero Operaciones Addon
              
        array(
          'icon'=>'trash',
          'url'=>array('delete', 'name'=>$item->name),
          'htmlOptions'=>array(
            'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
          ),
          'color' => 'white',
          'type' => BsHtml::BUTTON_TYPE_LINK
        ),
      ), array(
        'class'=>'pull-right',
      ));
    ?>
  </div>
</div>
<div class="page-content">
  <div class="panel">
    <div class="panel-body container-fluid">
      <div class="row">
        <div class="col-sm-12">
          <h4><?php echo $item->description; ?> <small>(<?php echo $item->name; ?>)</small></h4>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-sm-6">
          <h4>
            <?php echo Yii::t('app', 'Ancestors'); ?>
            <small><?php echo Yii::t('app', 'Permissions that inherit this item'); ?></small>
          </h4>

          <?php
            $this->widget('@bootstrap.widgets.BsGridView', array(
              'type' => 'striped condensed hover',
              'dataProvider'=>$ancestorDp,
              'emptyText'=>Yii::t('app', 'This item does not have any ancestors.'),
              'template'=>"{items}",
              'hideHeader'=>true,
              'columns'=>array(
                array(
                  'class' => '\dz\modules\auth\widgets\AuthItemDescriptionColumn',
                  'itemName' => $item->name,
                ),
                array(
                  'class' => '\dz\modules\auth\widgets\AuthItemTypeColumn',
                  'itemName' => $item->name,
                ),
                array(
                  'class' => '\dz\modules\auth\widgets\AuthItemRemoveColumn',
                  'itemName' => $item->name,
                ),
              ),
            ));
          ?>
        </div>

        <div class="col-sm-6">
          <h4>
            <?php echo Yii::t('app', 'Descendants'); ?>
            <small><?php echo Yii::t('app', 'Permissions granted by this item'); ?></small>
          </h4>

          <?php
            // $this->widget('@bootstrap.widgets.BsGridView', array(
            $this->widget('dz.grid.GridView', array(
              'type' => 'striped condensed hover',
              'dataProvider'=>$descendantDp,
              'emptyText'=>Yii::t('app', 'This item does not have any descendants.'),
              'hideHeader'=>true,
              'template'=>"{items}",
              'columns'=>array(
                array(
                  'class' => '\dz\modules\auth\widgets\AuthItemDescriptionColumn',
                  'itemName' => $item->name,
                ),
                array(
                  'class' => '\dz\modules\auth\widgets\AuthItemTypeColumn',
                  'itemName' => $item->name,
                ),
                array(
                  'class' => '\dz\modules\auth\widgets\AuthItemRemoveColumn',
                  'itemName' => $item->name,
                ),
              ),
            ));
          ?>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6 col-sm-offset-6">
          <?php if (!empty($childOptions)): ?>
            <h5><?php echo Yii::t('app', 'Add child'); ?></h5>
            <?php
              $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', array(
                'htmlOptions' => array(
                  'class' => 'form-horizontal'
                )
              ));
            ?>
            <?php echo $form->dropDownList($formModel, 'items', $childOptions, array('label'=>false)); ?>
            <br>
            <?php
              $this->widget('@bootstrap.widgets.TbButton', array(
                'buttonType'=> 'submit',
                'type'    => 'primary',
                'label'=>Yii::t('app', 'Add'),
              ));
            ?>
            <?php $this->endWidget(); ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>