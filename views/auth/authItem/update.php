<?php
/* @var $this OperationController|TaskController|RoleController */
/* @var $model AuthItemForm */
/* @var $item CAuthItem */
/* @var $form TbActiveForm */
/*
$this->breadcrumbs = array(
  $this->capitalize($this->getTypeText(true)) => array('index'),
  $item->description => array('view', 'name' => $item->name),
  Yii::t('app', 'Edit'),
);
*/
  use dz\helpers\Html;

  // Page title
  $this->pageTitle = Yii::t('app', $item->description);
?>
<div class="page-header">
  <h1 class="page-title"><?php echo Html::encode($this->pageTitle); ?></h1>
  <?php
    // Breadcrumbs
    echo Html::breadcrumbs(array(
      array(
        'label' => $this->capitalize($this->getTypeText(true)),
        'url' => array('index'),
      ),
      $this->pageTitle
    ));
  ?>
</div>

<div class="page-content container-fluid">
  <?php
    $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', array(
      'enableAjaxValidation' => FALSE,
      // 'htmlOptions' => array(
      //   'class' => 'form-horizontal'
      // )
    ));

    // Error summary
    $errors = $form->errorSummary($model);
    if ( $errors )
    {
      echo $errors;
    }
  ?>
  <div class="panel">
    <header class="panel-heading">
      <h3 class="panel-title">Actualizar <?php echo $this->getTypeText(); ?></h3>
    </header>
    <div class="panel-body">
      <?php echo $form->hiddenField($model, 'type'); ?>
      <?php
        echo $form->textFieldRow($model, 'name', array(
          'disabled'=>true,
          'title'=>Yii::t('app', 'System name cannot be changed after creation.'),
        ));
      ?>
      <?php echo $form->textFieldRow($model, 'description'); ?>

      <div class="form-actions">
        <?php
          $this->widget('@bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => Yii::t('app', 'Save'),
          ));
        ?>
        <?php
          $this->widget('@bootstrap.widgets.TbButton', array(
            'type' => 'link',
            'label' => Yii::t('app', 'Cancel'),
            'url' => array('index'),
          ));
        ?>
      </div>
    </div>
  </div>
  <?php $this->endWidget(); ?>
</div