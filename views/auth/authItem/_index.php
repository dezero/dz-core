<?php
/* @var $this OperationController|TaskController|RoleController */
/* @var $dataProvider AuthItemDataProvider */

$this->breadcrumbs = array(
	$this->capitalize($this->getTypeText(true)),
);
?>

<h1><?php echo $this->capitalize($this->getTypeText(true)); ?></h1>

<?php $this->widget('@bootstrap.widgets.TbButton', array(
    'type' => 'primary',
    'label' => Yii::t('app', 'Add {type}', array('{type}' => $this->getTypeText())),
    'url' => array('create'),
)); ?>

<?php $this->widget('@bootstrap.widgets.BsGridView', array(
    'type' => 'striped hover',
    'dataProvider' => $dataProvider,
    'emptyText' => Yii::t('app', 'No {type} found.', array('{type}'=>$this->getTypeText(true))),
	'template'=>"{items}\n{pager}",
    'columns' => array(
		array(
			'name' => 'name',
			'type'=>'raw',
			'header' => Yii::t('app', 'System name'),
			'htmlOptions' => array('class'=>'item-name-column'),
			'value' => "CHtml::link(\$data->name, array('view', 'name'=>\$data->name))",
		),
		array(
			'name' => 'description',
			'header' => Yii::t('app', 'Description'),
			'htmlOptions' => array('class'=>'item-description-column'),
		),
		array(
			'class'=>'@bootstrap.widgets.TbButtonColumn',
			'template' => '{update}{delete}',
			// 'viewButtonLabel' => Yii::t('app', 'View'),
			// 'viewButtonUrl' => "Yii::app()->controller->createUrl('view', array('name'=>\$data->name))",
			'updateButtonLabel' => Yii::t('app', 'Edit'),
			'updateButtonUrl' => "Yii::app()->controller->createUrl('update', array('name'=>\$data->name))",
			'deleteButtonLabel' => Yii::t('app', 'Delete'),
			'deleteButtonUrl' => "Yii::app()->controller->createUrl('delete', array('name'=>\$data->name))",
			'deleteConfirmation' => Yii::t('app', 'Are you sure you want to delete this item?'),
		),
    ),
)); ?>
