<?php
/* @var $this OperationController|TaskController|RoleController */
/* @var $dataProvider AuthItemDataProvider */

/*
$this->breadcrumbs = array(
  $this->capitalize($this->getTypeText(true)),
);
*/
  use dz\helpers\Html;
  
  $this->pageTitle = $this->capitalize($this->getTypeText(true));
?>
<div class="page-header">
  <h1 class="page-title"><?php echo $this->pageTitle; ?></h1>
  <div class="page-header-actions">
    <?php
      echo Html::link('<i class="icon wb-plus"></i> Añadir nuevo '. $this->getTypeText(), array("create"), array(
        'class' => 'btn btn-primary'
      ));
    ?>
  </div>
</div>
<div class="page-content">
  <div class="panel">
    <div class="panel-body container-fluid">
      <div class="row row-lg">
        <div class="col-lg-12">
          <?php
            // $this->widget('@bootstrap.widgets.BsGridView', array(
            $this->widget('dz.grid.GridView', array(
              'type' => 'striped hover',
              'dataProvider' => $dataProvider,
              'emptyText' => Yii::t('app', 'No {type} found.', array('{type}'=>$this->getTypeText(true))),
              'template'=>"{items}\n{pager}",
              'columns' => array(
                array(
                  'name' => 'name',
                  'type'=>'raw',
                  'header' => Yii::t('app', 'System name'),
                  'htmlOptions' => array('class'=>'item-name-column'),
                  'value' => "CHtml::link(\$data->name, array('view', 'name'=>\$data->name))",
                ),
                array(
                  'name' => 'description',
                  'header' => Yii::t('app', 'Description'),
                  'htmlOptions' => array('class'=>'item-description-column'),
                ),
                array(
                  'class' => 'dz.grid.ButtonColumn',
                  'template' => '{update}{delete}',
                  // 'viewButtonLabel' => Yii::t('app', 'View'),
                  // 'viewButtonUrl' => "Yii::app()->controller->createUrl('view', array('name'=>\$data->name))",
                  'updateButtonLabel' => Yii::t('app', 'Edit'),
                  'updateButtonUrl' => "Yii::app()->controller->createUrl('update', array('name'=>\$data->name))",
                  'deleteButtonLabel' => Yii::t('app', 'Delete'),
                  'deleteButtonUrl' => "Yii::app()->controller->createUrl('delete', array('name'=>\$data->name))",
                  'deleteConfirmation' => Yii::t('app', 'Are you sure you want to delete this item?'),
                ),
              ),
            ));
          ?>
        </div>
      </div>
    </div>
  </div>
</div>