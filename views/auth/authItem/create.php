<?php
/* @var $this OperationController|TaskController|RoleController */
/* @var $model AuthItemForm */
/* @var $form TbActiveForm */
/*
$this->breadcrumbs = array(
  $this->capitalize($this->getTypeText(true)) => array('index'),
  Yii::t('app', 'New {type}', array('{type}' => $this->getTypeText())),
);
*/
  use dz\helpers\Html;

  // Page title
  $this->pageTitle = Yii::t('app', 'Crear '. $this->getTypeText());
?>
<div class="page-header">
  <h1 class="page-title"><?php echo $this->pageTitle; ?></h1>
  <?php
    // Breadcrumbs
    echo Html::breadcrumbs(array(
      array(
        'label' => $this->capitalize($this->getTypeText(true)),
        'url' => array('index'),
      ),
      $this->pageTitle
    ));
  ?>
</div>
<div class="page-content container-fluid">
  <?php
    $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', array(
      'enableAjaxValidation' => FALSE,
      // 'htmlOptions' => array(
      //   'class' => 'form-horizontal'
      // )
    ));

    // Error summary
    $errors = $form->errorSummary($model);
    if ( $errors )
    {
      echo $errors;
    }
  ?>
  <div class="panel">
    <header class="panel-heading">
      <h3 class="panel-title"><?php echo Yii::t('app', 'New {type}', array('{type}' => $this->getTypeText())); ?></h3>
    </header>
    <div class="panel-body">
      <?php echo $form->hiddenField($model, 'type'); ?>
      <?php echo $form->textFieldRow($model, 'name'); ?>
      <?php echo $form->textFieldRow($model, 'description'); ?>
        
      <div class="form-actions">
        <?php
          $this->widget('@bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => Yii::t('app', 'Create'),
          ));
        ?>
        <?php
          $this->widget('TbButton', array(
            'type' => 'link',
            'label' => Yii::t('app', 'Cancel'),
            'url' => array('index'),
          ));
        ?>
      </div>
    </div>
  </div>
  <?php $this->endWidget(); ?>
</div>
