<?php 
    use dz\widgets\Modal;

    $this->beginContent($this->module->appLayout);
	
    // MODAL - Bootbox modal to confirm
	Modal::createModal('auth-confirm-modal', array(
		'fullWidth' => TRUE,
		'submitHtmlOptions' => array(
			'class' => 'dz-modal-submit-button dz-modal-button btn-primary hide'
		)
	));
?>

<div class="auth-module">

	<?php BsHtml::tabs(array(
		array(
			'label' => Yii::t('app', 'Assignments'),
			'url' => array('/auth/assignment/index'),
			'active' => $this instanceof AssignmentController,
		),
		array(
			'label' => $this->capitalize($this->getItemTypeText(CAuthItem::TYPE_ROLE, true)),
			'url' => array('/auth/role/index'),
			'active' => $this instanceof RoleController,
		),
		array(
			'label' => $this->capitalize($this->getItemTypeText(CAuthItem::TYPE_TASK, true) .' / Permisos'),
			'url' => array('/auth/task/index'),
			'active' => $this instanceof TaskController,
		),
		array(
			'label' => $this->capitalize($this->getItemTypeText(CAuthItem::TYPE_OPERATION, true)),
			'url' => array('/auth/operation/index'),
			'active' => $this instanceof OperationController,
		),
		array(
			'label' => Yii::t('app', 'Roles & users'),
			'url' => array('/auth/dzAuth/roles'),
			'active' => $this instanceof \auth\controllers\DzAuthController && $this->getAction()->getId() === 'roles',
		),
		array(
			'label' => Yii::t('app', 'Roles & permissions'),
			'url' => array('/auth/dzAuth/tasks'),
			'active' => $this instanceof \auth\controllers\DzAuthController && $this->getAction()->getId() === 'tasks',
		),
	));?>

	<?= $content; ?>

</div>

<?php $this->endContent(); ?>