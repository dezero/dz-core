<?php
/* @var $this AssignmentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    Yii::t('app', 'Assignments'),
);
?>

<h1><?= Yii::t('app', 'Assignments'); ?></h1>

<?php $this->widget('@bootstrap.widgets.BsGridView', array(
    'type' => 'striped hover',
    'dataProvider' => $dataProvider,
	'emptyText' => Yii::t('app', 'No assignments found.'),
	'template'=>"{items}\n{pager}",
    'columns' => array(
        array(
            'header' => Yii::t('app', 'User'),
            'class' => 'AuthAssignmentNameColumn',
        ),
        array(
            'header' => Yii::t('app', 'Assigned items'),
            'class' => 'AuthAssignmentItemsColumn',
        ),
        array(
            'class' => 'AuthAssignmentViewColumn',
        ),
    ),
)); ?>
