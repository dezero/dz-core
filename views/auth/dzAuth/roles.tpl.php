<?php
/*
|--------------------------------------------------------------------------
| Displays a checbox table list with users as rows and roles as columns
|--------------------------------------------------------------------------
|
| Available variables:
| 	$users: User model class
| 	$roles: Roles
| 	$assignments: Users and roles assignments
|
*/
    use dz\helpers\Html;

	// Page title
	$this->pageTitle = Yii::t('app', 'Usuarios y Roles');
?>
<style type="text/css">
	#user-role-table tr td,
	#user-role-table tr th {
		text-align: center;
	}

	#user-role-table tr td.username_column {
		text-align: left;
	}	
</style>
<h1><?= $this->pageTitle; ?></h1>

<?php
/*
|----------------------------------------------------------------------------------------
| Table with users as rows and roles as columns
|----------------------------------------------------------------------------------------
*/
?>
<?php if ( !empty($roles) AND !empty($users) ) : ?>
	<?php
		// Start model form (http://yii-booster.clevertech.biz/components.html#forms)
		$form = $this->beginWidget('@bootstrap.widgets.BsActiveForm',array(
			'id' => 'user-role-auth-form',
			'enableAjaxValidation' => FALSE,
		));
	?>
	
	<?php
		/*
		|--------------------------------------------------------------------------
		| TABLA con CHECKBOXES
		|--------------------------------------------------------------------------
		*/
	?>
	<table id="user-role-table" class="items table">
		<thead>
			<tr>
				<th><?= Yii::t('app', 'Users'); ?></th>
				<?php foreach ($roles as $que_role) : ?>
					<th><?= $que_role->description; ?></th>
				<?php endforeach; ?>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($users as $que_user) : ?>
				<tr>
					<td class="username_column">
						<?= Html::link($que_user->username, array("/user/admin/view", "id" => $que_user->id) ); ?>
					</td>
					<td>
						<?php
							$vec_checkboxes = array();
							foreach ($roles as $que_role)
							{
								$vec_checkboxes[$que_role->name] = $que_role->name;
							}
							echo Html::checkBoxList($que_user->username, $assignments[$que_user->username], $vec_checkboxes, array(
								'separator' => '</td><td>',
								'template' => '{input}',
								'container' => '',
							));
						?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	
	<?php
		/*
		|--------------------------------------------------------------------------
		| ACCIONES
		|--------------------------------------------------------------------------
		*/
	?>	
		<div class="form-actions span12 buttons">
			<?php
				// Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
				$this->widget('@bootstrap.widgets.TbButton', array(
					'buttonType'=> 'submit',
					'type'		=> 'primary',
					'label'		=> Yii::t('app', 'Save'),
				));
			?>
		</div><!-- form-actions -->

	<?php
		// End model form
		$this->endWidget();
	?>
<?php endif; ?>

