<?php
/*
|--------------------------------------------------------------------------
| Displays a checbox table list with tasks as rows and roles as columns
|--------------------------------------------------------------------------
|
| Available variables:
|   $tasks: Tasks
|   $roles: Roles
|   $assignments: Tasks and roles assignments
|
*/
  use dz\helpers\Html;

  // Page title
  $this->pageTitle = Yii::t('app', 'Roles and Permissions');
?>
<style type="text/css">
  #task-role-table tr td,
  #task-role-table tr th {
    text-align: center;
  }

  #task-role-table tr td.task_column {
    text-align: left;
  } 
</style>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
</div>

<div class="page-content">
  <div class="panel">
    <div class="panel-body container-fluid">
      <div class="row row-lg">
        <div class="col-lg-12">
          <?php
            /*
            |----------------------------------------------------------------------------------------
            | Table with tasks as rows and roles as columns
            |----------------------------------------------------------------------------------------
            */
           if ( !empty($roles) AND !empty($tasks) ) :
          ?>
            <?php
              // Start model form (http://yii-booster.clevertech.biz/components.html#forms)
              $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm',array(
                'id' => 'user-role-auth-form',
                'enableAjaxValidation' => FALSE,
              ));
            ?>
            <?php
              /*
              |--------------------------------------------------------------------------
              | TABLA con CHECKBOXES
              |--------------------------------------------------------------------------
              */
            ?>
            <table id="task-role-table" class="items table table-striped table-hover">
              <thead>
                <tr>
                  <th style="text-align: left;"><?= Yii::t('app', 'Permissions'); ?></th>
                  <?php foreach ($roles as $que_role) : ?>
                    <th><?= $que_role->description; ?></th>
                  <?php endforeach; ?>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($tasks as $que_task) : ?>
                  <tr>
                    <td class="task_column">
                      <?= Html::link($que_task->description, array("/auth/task/view", "name" => $que_task->name) ); ?>
                    </td>
                    <td>
                      <div class="checkbox-custom checkbox-primary">
                        <?php
                          $vec_checkboxes = array();
                          foreach ($roles as $que_role)
                          {
                            $vec_checkboxes[$que_role->name] = $que_role->name;
                          }
                          echo Html::checkBoxList($que_task->name, $assignments[$que_task->name], $vec_checkboxes, array(
                            'separator' => '</div></td><td><div class="checkbox-custom checkbox-primary">',
                            'template' => '{input}{label}',
                            'container' => '',
                          ));
                        ?>
                      </div>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            
            <?php
              /*
              |--------------------------------------------------------------------------
              | ACCIONES
              |--------------------------------------------------------------------------
              */
            ?>  
            <div class="form-group row">
              <div class="col-lg-12 form-actions buttons">
                <hr>
                <?php
                  // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
                  $this->widget('@bootstrap.widgets.TbButton', array(
                    'buttonType'=> 'submit',
                    'type'    => 'primary',
                    'label'   => Yii::t('app', 'Save'),
                  ));
                ?>
              </div><!-- form-actions -->
            </div>
            <?php
              // End model form
              $this->endWidget();
            ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
