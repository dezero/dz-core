<?php
/*
|--------------------------------------------------------------------------
| Displays a checbox table list for a task with operations as rows
|--------------------------------------------------------------------------
|
| Available variables:
| 	$task: Task auth item
| 	$operations: Operations
| 	$assignments: Operations assignments for a task
|
*/
  use dz\helpers\Html;
  
  // Page title
  $this->pageTitle = Yii::t('app', 'Permisos - Tarea y operaciones');
?>
<style type="text/css">
	#operation-table {
		margin-top: 30px;
	}
	
	#operation-table tr td,
	#operation-table tr th {
		text-align: center;
	}

	#operation-table tr td.operation_column {
		text-align: left;
	}	
</style>
<div class="title-row clearfix">

	<h1 class="pull-left">
		<?= CHtml::encode($task->description); ?>
		<small><?= Yii::t('app', 'Task'); ?></small>
	</h1>

	<?php $this->widget('@bootstrap.widgets.TbButtonGroup', array(
		'htmlOptions'=>array('class'=>'pull-right'),
		'buttons'=>array(
			array(
				'label'=>Yii::t('app', 'Edit'),
				'url'=>array('/auth/task/update', 'name'=>$task->name),
			),

			array(
				'label'=>Yii::t('app', 'View'),
				'icon' => 'eye-open',
				'url'=>array('/auth/task/view', 'name'=>$task->name),
			),
						
			array(
				'icon'=>'trash',
				'url'=>array('/auth/task/delete', 'name'=>$task->name),
				'htmlOptions'=>array(
					'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
				),
			),
		),
	)); ?>

</div>

<?php $this->widget('@bootstrap.widgets.TbDetailView', array(
    'data' => $task,
    'attributes' => array(
        array(
            'name' => 'name',
			'label' => Yii::t('app', 'System name'),
		),
		array(
			'name' => 'description',
			'label' => Yii::t('app', 'Description'),
		),
		/*
        array(
			'name' => 'bizrule',
			'label' => Yii::t('app', 'Business rule'),
		),
		array(
			'name' => 'data',
			'label' => Yii::t('app', 'Data'),
		),
		*/
    ),
)); ?>

<hr />

<div class="row">

	<div class="span12">

		<h3>
			<?= Yii::t('app', 'Operaciones asignadas'); ?>
			<small><?= Yii::t('app', 'Operaciones asignadas a esta tarea'); ?></small>
		</h3>

		<?php
		/*
		|----------------------------------------------------------------------------------------
		| Table with operations as rows
		|----------------------------------------------------------------------------------------
		*/
		?>
		<?php if ( !empty($operations) ) : ?>
			<?php
				// Start model form (http://yii-booster.clevertech.biz/components.html#forms)
				$form = $this->beginWidget('@bootstrap.widgets.TbActiveForm',array(
					'id' => 'user-role-auth-form',
					'enableAjaxValidation' => FALSE,
				));
			?>

			<?php
				/*
				|--------------------------------------------------------------------------
				| TABLA con CHECKBOXES
				|--------------------------------------------------------------------------
				*/
			?>
			<table id="operation-table" class="items table">
				<thead>
					<tr>
						<th><?= Yii::t('app', 'Operaciones - Nombre sistema'); ?></th>
						<th><?= Yii::t('app', 'Operaciones'); ?></th>
						<th><?= $task->description; ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($operations as $que_operation) : ?>
						<tr>
							<td class="operation_column">
								<?= $que_operation->name; ?>
							</td>
							<td class="operation_column">
								<?= Html::link($que_operation->description, array("/auth/operation/view", "task_name" => $que_operation->name) ); ?>
							</td>
							<td>								
								<?=
									Html::checkBox($que_operation->name, in_array($que_operation->name, $assignments), array(
										'template' => '{input}',
										'container' => '',
									));
								?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>

			<?php
				/*
				|--------------------------------------------------------------------------
				| ACCIONES
				|--------------------------------------------------------------------------
				*/
			?>	
				<div class="form-actions span12 buttons">
					<?php
						// Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
						$this->widget('@bootstrap.widgets.TbButton', array(
							'buttonType'=> 'submit',
							'type'		=> 'primary',
							'label'		=> Yii::t('app', 'Save'),
						));
					?>
				</div><!-- form-actions -->

			<?php
				// End model form
				$this->endWidget();
			?>
		<?php endif; ?>
	</div>
</div>