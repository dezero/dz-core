<?php
/*
|--------------------------------------------------------------------------
| Info page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $vec_php_info: PHP Information
|
*/
  use dz\helpers\DateHelper;
  use dz\helpers\StringHelper;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = Yii::t('app', 'System Information');
?>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
</div>

<div class="page-content container-fluid">
  
  <div class="panel">
    <header class="panel-heading address-title">
      <h3 class="panel-title"><?= Yii::t('app', 'Application'); ?></h3>
    </header>
    <div class="panel-body panel-view-content">
      <div class="row">
        <div class="col-lg-12">
          <table class="table table-striped table-hover">
            <tbody>
              <tr>
                <td><?= Yii::t('app', 'App version'); ?></td>
                <td><?= $app_version; ?></td>
              </tr>
              <tr>
                <td><?= Yii::t('app', 'App environment'); ?></td>
                <td><?= StringHelper::uppercase($app_env); ?></td>
              </tr>
              <tr>
                <td><?= Yii::t('app', 'Last Git commit'); ?></td>
                <td><?= nl2br($app_commit); ?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div><!-- .panel -->

  <div class="panel">
    <header class="panel-heading address-title">
      <h3 class="panel-title"><?= Yii::t('app', 'Server'); ?></h3>
    </header>
    <div class="panel-body panel-view-content">
      <div class="row">
        <div class="col-lg-12">
          <table class="table table-striped table-hover">
            <tbody>
              <tr>
                <td><?= Yii::t('app', 'Apache version'); ?></td>
                <td><?= $apache_version; ?></td>
              </tr>
              <tr>
                <td><?= Yii::t('app', 'PHP version'); ?></td>
                <td><?= $php_version; ?></td>
              </tr>
              <tr>
                <td><?= Yii::t('app', 'DB driver & version'); ?></td>
                <td><?= $db_driver; ?></td>
              </tr>
              <tr>
                <td><?= Yii::t('app', 'Image driver & version'); ?></td>
                <td><?= $image_driver; ?></td>
              </tr>
              <tr>
                <td><?= Yii::t('app', 'Yii framework version'); ?></td>
                <td><?= $yii_version; ?></td>
              </tr>
              <tr>
                <td><?= Yii::t('app', 'Dz framework version'); ?></td>
                <td><?= $dz_version; ?></td>
              </tr>
              <tr>
                <td><?= Yii::t('app', 'Dz framework last commit'); ?></td>
                <td><?= nl2br($dz_commit); ?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div><!-- .panel -->

  <div class="panel">
    <header class="panel-heading address-title">
      <h3 class="panel-title"><?= Yii::t('app', 'PHP Variables'); ?></h3>
    </header>
    <div class="panel-body panel-view-content">
      <div class="row">
        <div class="col-lg-6">
          <table class="table table-striped table-hover">
            <tbody>
              <tr>
                <td><?= Yii::t('app', 'PHP version'); ?></td>
                <td><?= $php_version; ?></td>
              </tr>
              <tr>
                <td><?= Yii::t('app', 'Memory Limit'); ?></td>
                <td><?= $memory_limit; ?></td>
              </tr>
              <tr>
                <td><?= Yii::t('app', 'Max. Execution Time'); ?></td>
                <td><?= $max_execution_time; ?> <?= Yii::t('app', 'seconds'); ?></td>
              </tr>
              <tr>
                <td><?= Yii::t('app', 'POST Max. Size'); ?></td>
                <td><?= $post_max_size; ?></td>
              </tr>
              <tr>
                <td><?= Yii::t('app', 'Upload Max. Size'); ?></td>
                <td><?= $upload_max_filesize; ?></td>
              </tr>
            </tbody>
          </table>
          <a class="btn btn-info" href="<?= Url::to('/admin/php'); ?>"><?= Yii::t('app', 'View full PHP Information'); ?></a>
        </div>
      </div>
    </div>
  </div><!-- .panel -->

</div>