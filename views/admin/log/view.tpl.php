<?php
/*
|--------------------------------------------------------------------------
| View page for Log file
|--------------------------------------------------------------------------
|
| Available variables:
|  - $log_file: Log file object
|
*/
  use dz\helpers\DateHelper;
  use dz\helpers\Html;

  // Page title
  $this->pageTitle = Yii::t('app', $log_file->getBasename());
?>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <div class="page-header-actions">
    <?=
      Html::link('<i class="wb-chevron-left"></i> Volver', ["/admin/log"], [
        'class' => 'btn btn-dark'
      ]);
    ?>
  </div>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => 'View Logs',
        'url' => ['/admin/log'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <div class="panel">
    <div class="panel-body panel-view-content">
      <div class="row">
        <div class="col-sm-3">
          <h5>Last modification date</h5>
          <div class="item-content"><?= DateHelper::unix_to_date($log_file->getTimeModified(), "d/m/Y - H:i"); ?></div>
        </div>

        <div class="col-sm-3">
          <h5>Last modification user</h5>
          <div class="item-content"><?= $log_file->getOwner() .':'. $log_file->getGroup(); ?></div>
        </div>

        <div class="col-sm-3">
          <h5>File permissions</h5>
          <div class="item-content"><?= $log_file->getPermissions(); ?></div>
        </div>

        <div class="col-sm-3">
          <h5>File size</h5>
          <div class="item-content"><?= $log_file->getSize(true); ?></div>
        </div>
      </div>

      <hr>

      <div class="row">
        <div class="col-sm-12">
          <div class="item-content log-information-field">
            <?= $content_log; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
