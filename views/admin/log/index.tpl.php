<?php
/*
|--------------------------------------------------------------------------
| Admin list page for Logs files model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $vec_log_files: Array with Files objects
|
*/  
  use dz\helpers\DateHelper;
  use dz\helpers\Html;

  // Page title
  $this->pageTitle = Yii::t('app', 'Log Files');
?>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
</div>

<div class="page-content">
  <div class="row row-lg">
    <div class="col-lg-12">
      <div class="panel panel-top-summary">
        <div class="panel-body container-fluid">
          <table class="items table table-striped table-hover">
            <thead>
              <tr>
                <th>LOG FILE NAME</th>
                <th class="center">LAST UPDATE DATE</th>
                <th class="center">OWNER:GROUP</th>
                <th class="center">PERMISSIONS</th>
                <th class="center">SIZE</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ( $vec_log_files as $log_file ) : ?>
              <tr>
                <td><?= Html::link($log_file->getBasename(), array('/admin/log/view', 'file' => $log_file->getBasename())); ?></td>
                <td class="center"><?= DateHelper::unix_to_date($log_file->getTimeModified(), "d/m/Y - H:i"); ?></td>
                <td class="center"><?= $log_file->getOwner() .':'. $log_file->getGroup(); ?></td>
                <td class="center"><?= $log_file->getPermissions(); ?></td>
                <td class="center"><?= $log_file->getSize(true); ?></td>
              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>