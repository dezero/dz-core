<?php
/*
|--------------------------------------------------------------------------
| Info page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $vec_php_info: PHP Information
|
*/
  use dz\helpers\DateHelper;
  use dz\helpers\Html;

  // Page title
  $this->pageTitle = Yii::t('app', 'Información PHP');
?>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
</div>
<div class="page-content container-fluid">
  <?php foreach ( $vec_php_info as $php_section => $vec_values ) : ?>
    <div class="panel">
      <header class="panel-heading address-title">
        <h3 class="panel-title"><?= $php_section; ?></h3>
      </header>
      <div class="panel-body panel-view-content">
        <div class="row">
          <div class="col-lg-12">
            <table class="table table-striped table-hover">
              <tbody>
                <?php foreach ( $vec_values as $php_key => $php_value ) : ?>
                  <tr>
                    <td><?= $php_key; ?></td>
                    <td>
                      <?php if ( is_array($php_value) ) : ?>
                        <?= implode(", ", $php_value) ; ?>
                      <?php else : ?>
                        <?= $php_value; ?>
                      <?php endif; ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  <?php endforeach ; ?>
</div>