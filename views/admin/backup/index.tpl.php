<?php
/*
|--------------------------------------------------------------------------
| Admin list page for Database Backups files model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $vec_backup_files: Array with Files objects
|
*/  
  use dz\helpers\DateHelper;
  use dz\helpers\Html;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = Yii::t('app', 'Database Backup Files');
?>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <div class="page-header-actions">
    <?=
      Html::link('<i class="icon wb-plus"></i> Crear nuevo backup', ["create"], [
        'class' => 'btn btn-primary'
      ]);
    ?>
  </div>
</div>

<div class="page-content">
  <div class="row row-lg">
    <div class="col-lg-12">
      <div class="panel panel-top-summary">
        <div class="panel-body container-fluid">
          <table class="items table table-striped table-hover">
            <thead>
              <tr>
                <th>DUMP FILE NAME</th>
                <th class="center">LAST UPDATE DATE</th>
                <th class="center">OWNER:GROUP</th>
                <th class="center">PERMISSIONS</th>
                <th class="center">SIZE</th>
                <th class="center">ACTIONS</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ( $vec_backup_files as $backup_file ) : ?>
              <tr>
                <td><?= Html::link($backup_file->getBasename(), array('/admin/backup/download', 'file' => $backup_file->getBasename())); ?></td>
                <td class="center"><?= DateHelper::unix_to_date($backup_file->getTimeModified(), "d/m/Y - H:i"); ?></td>
                <td class="center"><?= $backup_file->getOwner() .':'. $backup_file->getGroup(); ?></td>
                <td class="center"><?= $backup_file->getPermissions(); ?></td>
                <td class="center"><?= $backup_file->getSize(true); ?></td>
                <td class="center">
                  <a class="delete btn btn-sm btn-icon btn-pure btn-default dz-bootbox-confirm" title="" data-toggle="tooltip" data-confirm="<h3>¿Seguro que desea borrar este backup?</h3>" data-redirect="<?= Url::to('/admin/backup/index', ['delete' => $backup_file->getBasename()]); ?>" href="<?= Url::to('/admin/backup/delete', ['file' => $backup_file->getBasename()]); ?>" data-original-title="Borrar"><i class="wb-trash"></i></a>
                </td>
              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>