<?php
/*
|--------------------------------------------------------------------------
| View page for LogApi model
|--------------------------------------------------------------------------
|
| Available variables:
|   - $log_api_model: LogApi model
|
*/
  use dz\helpers\Html;
  use dz\helpers\Http;
  use dz\helpers\Json;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = Html::encode('Log '. $log_api_model->title());
?>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <div class="page-header-actions">
    <?=
      Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app', 'Back'), ["/admin/apiLog"], [
        'class' => 'btn btn-dark'
      ]);
    ?>
  </div>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => 'API Logs',
        'url' => ['/admin/apiLog/'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <div class="panel">
    <header class="panel-heading">
      <h3 class="panel-title"><?= Yii::t('app', 'API Log'); ?></h3>
    </header>
    <div class="panel-body panel-view-content">
      <div class="row">
        <div class="col-sm-3">
          <h5><?= $log_api_model->getAttributeLabel('api_name'); ?></h5>
          <div class="item-content"><?= $log_api_model->api_name; ?></div>
        </div>

        <div class="col-sm-3">
          <h5><?= Yii::t('app', 'Entity'); ?></h5>
          <div class="item-content"><?= $log_api_model->entity_type; ?> - <?= $log_api_model->entity_id; ?></div>
        </div>

        <div class="col-sm-3">
          <h5><?= $log_api_model->getAttributeLabel('created_date'); ?></h5>
          <div class="item-content"><?= $log_api_model->created_date; ?></div>
        </div>

        <div class="col-sm-3">
          <h5><?= Yii::t('app', 'User'); ?></h5>
          <div class="item-content">
            <?php if ( $log_api_model->created_uid > 0 && $log_api_model->createdUser ) : ?>
              <?= $log_api_model->created_uid; ?> - <?= $log_api_model->createdUser->title(); ?>
              <br><a href="<?= Url::to('update', ['id' => $log_api_model->createdUser->id]); ?>" target="_blank"><?= $log_api_model->createdUser->email; ?></a>
            <?php else : ?>
              -
            <?php endif; ?>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <h5><?= Yii::t('app', 'Request / Input'); ?></h5>
          <div clas="item-content">
            <strong><?= $log_api_model->get_endpoint(); ?></strong>
            <?php if ( !empty($log_api_model->request_url) ) : ?>
              <br><?= $log_api_model->request_url; ?>
            <?php endif; ?>
          </div>
          <div class="item-content mt-10 log-information-field">
            <?= Json::pretty_format($log_api_model->get_input(), true, true); ?>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <h5><?= Yii::t('app', 'Response'); ?> (HTTP code)</h5>
          <div class="item-content">
            <strong><?= $log_api_model->response_http_code; ?> - <?= Http::get_status_code_message($log_api_model->response_http_code); ?></strong>
          </div>
          <div class="item-content mt-10 log-information-field">
            <?= Json::pretty_format($log_api_model->response_json, true, true); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>