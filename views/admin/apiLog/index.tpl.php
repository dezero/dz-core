<?php
/*
|--------------------------------------------------------------------------
| Admin list page for LogApi models
|--------------------------------------------------------------------------
|
| Available variables:
|   $log_api_model: LogApi model class
|
*/  
  // Page title
  $this->pageTitle = Yii::t('app', 'API Logs');
?>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
</div>
<div class="page-content">
  <div class="row row-lg">
    <div class="col-lg-12">
      <div class="panel panel-top-summary">
        <div class="panel-body container-fluid">
          <?php
            /*
            |----------------------------------------------------------------------------------------
            | GridView widget
            |----------------------------------------------------------------------------------------
            */
              $this->widget('dz.grid.GridView', array(
                'id'            => 'api-log-grid',
                'dataProvider'  => $log_api_model->search(),
                'filter'        => $log_api_model,
                'emptyText'     => Yii::t('app', 'No logs found'),
                'enableHistory' => true,
                'loadModal'     => true,
                'enableSorting' => false,
                'type'          => ['striped', 'hover'],
                'beforeAjaxUpdate' => 'js:function(id, options){ $("html, body").animate({scrollTop: 0}, 100); }',
                'columns'       => [
                  [
                    'name' => 'api_name',
                    'filter' => $log_api_model->api_name_list()
                  ],
                  [
                    'header' => Yii::t('app', 'Endpoint'),
                    'name' => 'request_endpoint',
                    'filter' => $log_api_model->endpoint_list(),
                    'value' => 'trim($this->grid->getOwner()->renderPartial("//admin/apiLog/_grid_column", ["column" => "request_endpoint", "model" => $data]))',
                  ],
                  [
                    // 'header' => Yii::t('app', 'Entity'),
                    // 'name' => 'entity_filter',
                    // 'value' => 'trim($this->grid->getOwner()->renderPartial("//admin/apiLog/_grid_column", ["column" => "entity_type", "model" => $data]))',
                    'name' => 'entity_type',
                    'filter' => $log_api_model->entity_list(),
                  ],
                  [
                    'name' => 'entity_id'
                  ],
                  [
                    'header' => Yii::t('app', 'Response'),
                    'filter' => false,
                    'value' => 'trim($this->grid->getOwner()->renderPartial("//admin/apiLog/_grid_column", ["column" => "response", "model" => $data]))',
                  ],
                  [
                    'header' => Yii::t('app', 'Date'),
                    'name' => 'created_date',
                    'filter' => false,
                    'value' => 'trim($this->grid->getOwner()->renderPartial("//admin/apiLog/_grid_column", ["column" => "created_date", "model" => $data]))',
                  ],
                  [
                    'class' => 'dz.grid.ButtonColumn',
                    'header' => Yii::t('app', 'ACTION'),
                    'template' => '{view}{delete}',
                    'deleteButton' => true,
                    'clearButton' => true,
                    'viewButton' => true,
                    'updateButton' => false,
                  ],
                ],                
              ));
            ?>
        </div>
      </div>
    </div>
  </div>
</div>
