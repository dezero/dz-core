<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for LogApi model (page)
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: LogApi model
|  - $column: Column name
|
*/
  use dz\helpers\Html;
  use dz\helpers\Http;
  use dz\helpers\Url;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "request_endpoint"
    |--------------------------------------------------------------------------
    */
    case 'request_endpoint':
  ?>
    <strong><?= $model->get_endpoint(); ?></strong>
    <?php if ( !empty($model->request_url) ) : ?>
      <br><?= $model->request_url; ?>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "entity_type"
    |--------------------------------------------------------------------------
    */
    case 'entity_type':
  ?>
    <?= $model->entity_type; ?> - <?= $model->entity_id; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "response"
    |--------------------------------------------------------------------------
    */
    case 'response':
  ?>
    <strong><?= $model->response_http_code; ?></strong>
    <br><?= Http::get_status_code_message($model->response_http_code); ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "created_date"
    |--------------------------------------------------------------------------
    */
    case 'created_date':
  ?>
    <?= $model->created_date; ?>
    <?php if ( $model->created_uid > 0 && $model->createdUser ) : ?>
      <br><em><?= $model->created_uid; ?> - <?= $model->createdUser->title(); ?></em>
    <?php endif; ?>
  <?php break; ?>
<?php endswitch; ?>