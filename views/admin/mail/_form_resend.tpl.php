<?php
/*
|--------------------------------------------------------------------------
| Form partial page for (re)sending an email from a MailHistory model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $mail_history_model: MailHistory model
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
    
  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => 'mail-history-send-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal inline-block',
      'enctype' => 'multipart/form-data',
      'autocomplete' => 'off'
    ]
  ]);

  echo $form->hiddenField($mail_history_model, 'history_id');
?>
  <button id="mail-history-send-btn" class="btn btn-primary" type="submit" name="yt0" data-recipient="<?= $mail_history_model->recipient_emails; ?>"><i class="wb-envelope"></i><?= Yii::t('app', 'Resend Email'); ?></button>
<?php
  // End model form
  $this->endWidget();
?>