<?php
/*
|--------------------------------------------------------------------------------------
| Send an email manually to an user
|--------------------------------------------------------------------------------------
|
| Available variables:
|  - $user_model: User model
|  - $mail_template_model: MailTemplate model
|  - $mail_history_model: MailHistory model
|  - $vec_template_list: List of MailTemplate models
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = Html::encode('Send email to '. $user_model->get_email());
?>
<div class="page-header">
  <h1 class="page-title">Send an email to <em><?= $user_model->get_email(); ?></em></h1>
  <div class="page-header-actions">
    <a href="<?= Url::to('/user/admin/update', ['id' => $user_model->id]); ?>"" class="btn btn-info""><?= Yii::t('app', 'Update User'); ?></a>
  </div>
</div>
<div class="page-content container-fluid">
  <?php
    // Render form
    $this->renderPartial('_form_send', [
      'user_model'          => $user_model,
      'mail_template_model' => $mail_template_model,
      'mail_history_model'  => $mail_history_model,
      'vec_template_list'   => $vec_template_list,
      'form_id'             => 'mail-send-form'
    ]);
  ?>
</div>