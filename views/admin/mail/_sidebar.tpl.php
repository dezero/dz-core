<?php
/*
|--------------------------------------------------------------------------
| Sidebar - Order model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: MailHistory model
|
*/

  use dz\helpers\Url;

  // Get status labels & colors
  $vec_status_labels = $model->status_type_labels();
  $vec_status_colors = $model->status_colors();

?>
<div class="page-aside">
  <div class="page-aside-switch">
    <i class="icon wb-chevron-left" aria-hidden="true"></i>
    <i class="icon wb-chevron-right" aria-hidden="true"></i>
  </div>
  <div class="page-aside-inner page-aside-scroll">
    <div data-role="container">
      <div data-role="content">
        <div class="page-aside-section">
          <div class="list-group mb-10">
            <a class="list-group-item justify-content-between<?php if ( $model->status_type === 'all' ) : ?> active<?php endif; ?>" href="<?= Url::to('/admin/mail', ['MailHistory[status_type]' => 'all']); ?>">
              <span><i class="icon wb-order" aria-hidden="true"></i> <?= Yii::t('app', 'All Emails'); ?></span>
            </a>
          </div>
        </div>
        <section class="page-aside-section">
          <div class="list-group mb-20">
            <a class="list-group-item<?php if ( $model->status_type === 'sent' || empty($model->status_type) ) : ?> active<?php endif; ?> ( " href="<?= Url::to('/admin/mail'); ?>">
              <span><i class="wb-medium-point <?= $vec_status_colors['sent']; ?>" aria-hidden="true"></i><?= $vec_status_labels['sent']; ?></span>
              <?php /* <span class="item-right"><?= $vec_totals['sent']; ?></span> */ ?>
            </a>
            <a class="list-group-item<?php if ( $model->status_type === 'pending' ) : ?> active<?php endif; ?> ( " href="<?= Url::to('/admin/mail/pending'); ?>">
              <span><i class="wb-medium-point <?= $vec_status_colors['pending']; ?>" aria-hidden="true"></i><?= $vec_status_labels['pending']; ?></span>
              <?php /* <span class="item-right"><?= $vec_totals['pending']; ?></span> */ ?>
            </a>
            <a class="list-group-item<?php if ( $model->status_type === 'scheduled' ) : ?> active<?php endif; ?> ( " href="<?= Url::to('/admin/mail/scheduled'); ?>">
              <span><i class="wb-medium-point <?= $vec_status_colors['scheduled']; ?>" aria-hidden="true"></i><?= $vec_status_labels['scheduled']; ?></span>
              <?php /* <span class="item-right"><?= $vec_totals['scheduled']; ?></span> */ ?>
            </a>
            <a class="list-group-item<?php if ( $model->status_type === 'error' ) : ?> active<?php endif; ?> ( " href="<?= Url::to('/admin/mail', ['MailHistory[status_type]' => 'error']); ?>">
              <span><i class="wb-medium-point <?= $vec_status_colors['error']; ?>" aria-hidden="true"></i><?= $vec_status_labels['error']; ?></span>
              <?php /* <span class="item-right"><?= $vec_totals['error']; ?></span> */ ?>
            </a>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>