<?php
/*
|--------------------------------------------------------------------------
| Mail status (fragment HTML) for a MailHistory model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: MailHistory model
|  - $is_show_date: Show date information?
|
*/
  $vec_status_labels = $model->status_type_labels();
  $vec_status_colors = $model->status_colors();
  if ( ! isset($is_show_date) )
  {
    $is_show_date = true;
  }
?>
<div class="status-mail-wrapper">
  <?php if ( isset($vec_status_labels[$model->status_type]) ) : ?>
    <?php if ( isset($vec_status_colors[$model->status_type]) ) : ?>
      <span class="<?= $vec_status_colors[$model->status_type]; ?>"><i class="wb-medium-point <?= $vec_status_colors[$model->status_type]; ?>" aria-hidden="true"></i> <?= $vec_status_labels[$model->status_type]; ?></span>
    <?php else : ?>
      <span><i class="wb-medium-point"></i> <?= $vec_status_labels[$model->status_type]; ?></span>
    <?php endif; ?>
    <?php
      // Show SENDING date
      if ( $is_show_date && $model->status_type === 'sent' && !empty($model->send_date) )
    : ?>
      <br><small><?= $model->send_date; ?></small>
    <?php
      // Show SCHEDULING date
      elseif ( $is_show_date && $model->status_type === 'scheduled' && !empty($model->scheduled_date) )
    : ?>
      <br><small><?= $model->scheduled_date; ?></small>
    <?php
      // Show ERROR DESCRIPTION
      elseif ( !empty($model->error_description) )
    : ?>
      <br><?php if ( $model->status_type !== 'error' && $model->status_type !== 'sent' ) : ?><div class="badge badge-danger mr-5" data-toggle="tooltip" data-placement="bottom" data-original-title="<?= $model->updated_date; ?>"><?= Yii::t('app', 'ERROR'); ?></div><?php endif; ?><small><?= $model->error_description; ?></small>
    <?php endif; ?>
  <?php endif; ?>
</div>