<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for MailHistory model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: MailHistory model
|  - $column: Column name
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "template"
    |--------------------------------------------------------------------------
    */
    case 'template':
  ?>
    <?php if ( $model->template ) : ?>
      <?= $model->template_alias; ?><br><small><?= $model->template->name; ?></small>
    <?php else : ?>
      <i><?= Yii::t('app', 'Free email'); ?></i>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "recipient_emails"
    |--------------------------------------------------------------------------
    */
    case 'recipient_emails':
  ?>
    <?= str_replace(',', ', ', $model->recipient_emails); ?>
    <?php if ( !empty($model->recipient_uid) && $model->recipient_uid > 1 && $model->recipientUser ) : ?>
      <br><a href="<?= Url::to('/user/admin/update', ['id' => $model->recipient_uid]); ?>" target="_blank"><?= $model->recipientUser->title(); ?></a>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "status"
    |--------------------------------------------------------------------------
    */
    case 'status':
  ?>
    <?php
      $this->renderPartial('//admin/mail/_view_status', ['model' => $model]);
    ?>
  <?php break; ?>
<?php endswitch; ?>