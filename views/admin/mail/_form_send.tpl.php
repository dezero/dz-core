
<?php
/*
|--------------------------------------------------------------------------
| Form partial page for (re)sending an email from a MailHistory model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $user_model: User model
|  - $mail_template_model: MailTemplate model
|  - $mail_history_model: MailHistory model
|  - $vec_template_list: List of MailTemplate models
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
    
  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal',
      'enctype' => 'multipart/form-data',
      'autocomplete' => 'off'
    ]
  ]);

  echo $form->hiddenField($mail_history_model, 'entity_id');
  echo $form->hiddenField($mail_history_model, 'entity_type');

  // Error summary
  $errors = $form->errorSummary($mail_history_model);
  if ( $errors )
  {
    echo $errors;
  }
?>

  <?php
    /*
    |--------------------------------------------------------------------------
    | SEND FORM
    |--------------------------------------------------------------------------
    */
  ?>
  <div class="panel">
    
    <header class="panel-heading">
      <h3 class="panel-title"><?= Yii::t('app', 'Send email'); ?></h3>
    </header>

    <div class="panel-body">
      <div class="row">
        <div class="col-sm-12">
          <div class="form-group row">
            <label class="col-lg-2 col-sm-2 form-control-label"><?= $mail_history_model->getAttributeLabel('recipient_emails'); ?></label>
            <div class="col-lg-10">
              <p class="form-control view-field" style="max-width: 450px">
                <?= $user_model->get_email(); ?>
              </p>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="form-group row<?php if ( $mail_history_model->hasErrors('template_id') ) : ?> has-danger<?php endif; ?>">
            <label class="col-lg-2 col-sm-2 form-control-label" for="MailHistory_template_id"><?= Yii::t('app', 'Email Template'); ?></label>
            <div class="col-lg-10">
              <?php
                /*
                |--------------------------------------------------------------------------
                | STEP 1 - SELECT EMAIL TEMPLATE
                |--------------------------------------------------------------------------
                */
                if ( ! $mail_template_model ) :
              ?>
                <div style="max-width: 450px">
                  <?=
                    $form->dropDownList($mail_history_model, 'template_id', $vec_template_list, [
                      'class'             => 'form-control',
                      'data-plugin'       => 'select2',
                      'data-placeholder'  => 'Select a mail template...',
                      'data-allow-clear'  => false,
                      'style'             => 'max-width: 450px',
                    ]);
                  ?>
                </div>
                <p class="help-block"><?= Yii::t('app', 'Select a mail template to continue'); ?>. <a href="<?= Url::to('/settings/mail'); ?>" target="_blank">View email templates</a></p>
                <div class="mt-30">
                  <?php
                    // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
                    $this->widget('@bootstrap.widgets.TbButton', [
                      'buttonType'  => 'submit',
                      'type'        => 'primary',
                      'label'       => Yii::t('app', 'Continue'),
                    ]);
                  ?>
                </div>
              <?php else : ?>
                <?= $form->hiddenField($mail_history_model, 'template_id'); ?>
                <span class="form-control view-field inline-block" style="max-width: 450px">
                  <?php if ( $mail_template_model->alias === 'free' ) : ?>
                    <?= Yii::t('app', '-- Free Email --'); ?>
                  <?php else : ?>
                    <?= $mail_template_model->title(); ?>
                  <?php endif; ?>
                </span>
                <a class="btn btn-dark inline-block ml-10 btn-back" href="<?= Url::to('/admin/mail/send', ['user_id' => $user_model->id]); ?>"><?= Yii::t('app', 'Change'); ?></a>
              <?php endif; ?>
            </div>
          </div>
          
          <?php
            /*
            <h5><?= $mail_history_model->getAttributeLabel('template_id'); ?></h5>
            <div class="item-content">
              <a href="<?= Url::to('/settings/mail/update', ['id' => $mail_template_model->template_id]); ?>" target="_blank"><?= $mail_template_model->name; ?> <em>(<?= $mail_template_model->alias; ?>)</em></a>
            </div>
            */
          ?>
        </div>
      </div>

      <?php
        /*
        |--------------------------------------------------------------------------
        | STEP 2 - MAIL SELECTED
        |--------------------------------------------------------------------------
        */
        if ( $mail_template_model ) :
      ?>
        <div class="form-group row<?php if ( $mail_template_model->hasErrors('subject') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($mail_template_model, 'subject', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
          <div class="col-lg-10">
            <?=
              $form->textField($mail_template_model, 'subject', [
                'placeholder' => '',
                'maxlength' => 255
              ]);
            ?>
            <p class="help-block">The subject line for the email notification. See the <a href="#tokens">replacement tokens</a> below.</p>
            <?= $form->error($mail_template_model,'subject'); ?>
          </div>
        </div>

        <div class="form-group row<?php if ( $mail_template_model->hasErrors('body') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($mail_template_model, 'body', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
          <div class="col-lg-10">
            <?php
              $this->widget('@ext.DzMarkdown.DzMarkdown', [
                'model'     => $mail_template_model,
                'attribute' => 'body',
                'options'   => ['minHeight' => 250]
              ]);

              /*
              $form->textArea($mail_template_model, 'body', [
                'rows' => 12
              ]);
              */
              /*
              $this->widget('@lib.redactor.ERedactorWidget', [
                'model'   => $mail_template_model,
                'attribute' => 'body',
                'options' => [
                  'minHeight' => 200,
                  'buttonSource' => TRUE,
                  // 'buttons' => ['|', 'html'],
                ],
              ]);
              */
            ?>
            <p class="help-block">
              The HTML content of the email notification. See the <a href="<?= Url::to('/help/markdown'); ?>" target="_blank">format guide</a>.<br>
              Replacement tokens are allowed. See the <a href="#tokens">replacement tokens</a> below.
            </p>
            <?= $form->error($mail_template_model,'body'); ?>
          </div>
        </div>
      <?php endif; ?>
    </div>
  </div><!-- .panel -->

  <?php
    /**
     * REPLACEMENT TOKENS HELP
     */
  ?>
  <?php if ( $mail_template_model ) : ?>
    <?php
      $this->renderPartial('//settings/mail/_tokens_help', [
        'language_id' => Yii::app()->language,
        'model'       => $mail_template_model
      ]);
    ?>
  <?php endif; ?>

  <?php
    /*
    |--------------------------------------------------------------------------
    | ACTIONS
    |--------------------------------------------------------------------------
    */
  ?>  
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php if ( $mail_template_model ) : ?>
        <?php
          // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
          $this->widget('@bootstrap.widgets.TbButton', [
            'buttonType'  => 'submit',
            'type'        => 'primary',
            'label'       => Yii::t('app', 'Send'),
          ]);

          // Back
          echo Html::link(Yii::t('app', 'Back'), ['/admin/mail/send', 'user_id' => $user_model->id], ['class' => 'btn btn-dark btn-back']);
        ?>
      <?php else : ?>
        <?php
          // Cancel
          echo Html::link(Yii::t('app', 'Cancel'), ['/admin/mail'], ['class' => 'btn btn-dark']);
        ?>
      <?php endif; ?>
    </div><!-- form-actions -->
  </div>
<?php
  // End model form
  $this->endWidget();
?>