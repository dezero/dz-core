<?php
/*
|--------------------------------------------------------------------------
| Admin list page for MailHistory model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: MailHistory model class
|  - $vec_template_filter: Array with all the mail templates
|
*/  
  
  use dz\helpers\Html;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = Yii::t('app', ucfirst($model->status_type) .' Emails');

  // Filtered params
  $vec_filtered_params = isset($_GET['MailHistory']) ? $_GET['MailHistory'] : [];
?>
<?php
  // Sidebar menu with status filter
  $this->renderPartial('//admin/mail/_sidebar', [
    'model' => $model,
    // 'vec_totals'  => $vec_totals
  ]);
?>
<div class="page-main">
  <div class="page-header">
    <h1 class="page-title"><?= $this->pageTitle; ?></h1>
    <div class="page-header-actions">
      <?php if ( $model->status_type === 'pending' ) : ?>
        <a href="<?= Url::to('/admin/mail/pending', $vec_filtered_params); ?>" class="btn btn-dark"><?= Yii::t('app', 'View Pending Emails'); ?></a>
      <?php else : ?>
        <a href="<?= Url::to('/admin/mail', $vec_filtered_params); ?>" class="btn btn-dark"><?= Yii::t('app', 'View Sent Emails'); ?></a>
      <?php endif; ?>
    </div>
  </div>
  <div class="page-content">
  <div class="row row-lg">
    <div class="col-lg-12">
        <div class="panel panel-top-summary">
          <div class="panel-body container-fluid">
            <?php
              /*
              |----------------------------------------------------------------------------------------
              | GridView widget
              |----------------------------------------------------------------------------------------
              */
                $this->widget('dz.grid.GridView', array(
                  'id'                => 'mail-history-grid',
                  'dataProvider'      => $model->search(),
                  'filter'            => $model,
                  'emptyText'         => Yii::t('app', 'No emails found'),
                  'enableHistory'     => true,
                  'loadModal'         => true,
                  'enableSorting'     => false,
                  'type'              => ['striped', 'hover'],
                  'beforeAjaxUpdate'  => 'js:function(id, options){ $("html, body").animate({scrollTop: 0}, 100); }',
                  'columns'           => [
                    [
                      'header' => 'Date',
                      'name' => 'created_date',
                      'filter' => false
                    ],
                    [
                      'name' => 'recipient_emails',
                      'value' => 'trim($this->grid->getOwner()->renderPartial("_grid_column", ["column" => "recipient_emails", "model" => $data]))',
                    ],
                    [
                      'header' => 'Mail',
                      'name' => 'template_id',
                      'value' => 'trim($this->grid->getOwner()->renderPartial("_grid_column", ["column" => "template", "model" => $data]))',
                      'filter' => $vec_template_filter
                    ],
                    [
                      'header' => 'Status',
                      'name' => 'status_type',
                      'filter' => $model->status_type === 'all' ? $model->status_type_labels() : false,
                      'value' => 'trim($this->grid->getOwner()->renderPartial("_grid_column", ["column" => "status", "model" => $data]))',
                    ],
                    [
                      'class' => 'dz.grid.ButtonColumn',
                      'header' => 'ACTION',
                      'template' => Yii::app()->user->id == 1 ? '{view}{delete}' : '{view}',
                      'clearButton' => true,
                      'deleteButton' => Yii::app()->user->id == 1 ? true : false,
                      'viewButton' => true,
                      'updateButton' => false,
                    ],
                  ],
                ));
              ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!-- .page-main -->
