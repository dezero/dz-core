<?php
/*
|--------------------------------------------------------------------------
| View page for MailHistory model
|--------------------------------------------------------------------------
|
| Available variables:
|   - $mail_history_model: MailHistory model
|
*/
  use dz\helpers\Html;

  // Page title
  $this->pageTitle = Html::encode('Mail sent to '. $mail_history_model->recipient_emails);
?>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <div class="page-header-actions">
    <?=
      Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app', 'Back'), ["/admin/mail"], [
        'class' => 'btn btn-dark'
      ]);
    ?>
    <?php
      // (Re)Send email
      $this->renderPartial('//admin/mail/_form_resend', [
        'mail_history_model'  => $mail_history_model
      ]);
    ?>
  </div>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => 'Sent Emails',
        'url' => ['/admin/mail/'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <?php
    /*
    |--------------------------------------------------------------------------
    | RESUMEN
    |--------------------------------------------------------------------------
    */
  ?>
  <div class="panel">
    <header class="panel-heading">
      <h3 class="panel-title">Mail Details</h3>
    </header>
    <div class="panel-body panel-view-content">
      <div class="row">
        <div class="col-sm-3">
          <h5><?= $mail_history_model->getAttributeLabel('status_type'); ?></h5>
          <div class="item-content">
            <?php
              $this->renderPartial('//admin/mail/_view_status', [
                'model' => $mail_history_model,
                'is_show_date' => false
              ]);
            ?>
          </div>
        </div>

        <div class="col-sm-3">
          <h5><?= $mail_history_model->getAttributeLabel('language_id'); ?></h5>
          <div class="item-content"><?= $mail_history_model->language ? $mail_history_model->language->title() : Yii::app()->i18n->language_name('default'); ?></div>
        </div>

        <?php if ( $mail_history_model->status_type === 'scheduled' && !empty($mail_history_model->scheduled_date) ) : ?>
          <div class="col-sm-3">
            <h5><?= $mail_history_model->getAttributeLabel('scheduled_date'); ?></h5>
            <div class="item-content"><?= $mail_history_model->scheduled_date; ?></div>
          </div>
        <?php else : ?>
          <div class="col-sm-3">
            <h5><?= $mail_history_model->getAttributeLabel('send_date'); ?></h5>
            <div class="item-content"><?= $mail_history_model->send_date; ?></div>
          </div>
        <?php endif; ?>

        <div class="col-sm-3">
          <h5><?= $mail_history_model->getAttributeLabel('created_date'); ?></h5>
          <div class="item-content"><?= $mail_history_model->created_date; ?></div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <h5><?= $mail_history_model->getAttributeLabel('template_id'); ?></h5>
          <div class="item-content">
            <?php if ( $mail_history_model->template ) : ?>
              <?= $mail_history_model->template->name; ?> <em>(<?= $mail_history_model->template->alias; ?>)</em>
            <?php else : ?>
              <i><?= Yii::t('app', 'Free email'); ?></i>
            <?php endif; ?>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <h5><?= $mail_history_model->getAttributeLabel('sender_name'); ?></h5>
          <div class="item-content"><?= $mail_history_model->sender_name .' &lt;'. $mail_history_model->sender_email .'&gt;'; ?></div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <h5><?= $mail_history_model->getAttributeLabel('recipient_emails'); ?></h5>
          <div class="item-content"><?= $mail_history_model->recipient_emails; ?></div>
        </div>
      </div>

      <?php if ( !empty($mail_history_model->cc_emails) ) : ?>
        <div class="row">
          <div class="col-sm-12">
            <h5><?= $mail_history_model->getAttributeLabel('cc_emails'); ?></h5>
            <div class="item-content"><?= $mail_history_model->cc_emails; ?></div>
          </div>
        </div>
      <?php endif; ?>

      <?php if ( !empty($mail_history_model->bcc_emails) ) : ?>
        <div class="row">
          <div class="col-sm-12">
            <h5><?= $mail_history_model->getAttributeLabel('bcc_emails'); ?></h5>
            <div class="item-content"><?= $mail_history_model->bcc_emails; ?></div>
          </div>
        </div>      
      <?php endif; ?>

      <div class="row">
        <div class="col-sm-12">
          <h5><?= $mail_history_model->getAttributeLabel('subject'); ?></h5>
          <div class="item-content"><?= $mail_history_model->subject; ?></div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <h5><?= Yii::t('app', 'Body (not styled)'); ?></h5>
          <div class="item-content mt-10 log-information-field">
            <?= $mail_history_model->get_body(); ?>
          </div>
        </div>
      </div>

      <?php /*
      <div class="row">
        <div class="col-sm-12">
          <h5><?= Yii::t('app', 'Body (styled)'); ?></h5>
          <div class="item-content log-information-field">
            <?= $mail_history_model->get_eml_content(); ?>
          </div>
        </div>
      </div>
      */ ?>

    </div>
  </div>
</div>
