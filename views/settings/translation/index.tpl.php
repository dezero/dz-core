<?php
/*
|--------------------------------------------------------------------------
| Translate form page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $category: Current translation category
|  - $vec_languages: Array of enabled languages
|  - $vec_translation_source_models: Array new TranslationSource models
|  - $vec_new_translation_models: Array with new TranslationSource models
|  - $vec_translation_categories: Array with all translation categories
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  use dz\helpers\Url;
  use dz\modules\settings\models\TranslationMessage;


  // Page title
  $this->pageTitle = $page_title;
?>

<div id="translation-page-header" class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <?php if ( !empty($vec_translation_categories) ) : ?>
    <div class="page-header-actions">
      <label><?= Yii::t('app', 'Category'); ?>: </label>
      <?=
        Html::dropDownList(
          'TranslationCategory[]',
          $category,
          $vec_translation_categories,
          [
            'class'             => 'form-control',
            'id'                => 'translation-categories',
            'autocomplete'      => 'off'
            // 'placeholder'       => 'Select a category',
            // 'data-placeholder'  => 'Select a category',
            // 'empty'             => 'Select a category',
            // 'data-plugin'       => 'select2',
            // 'multiple'          => false,
          ]
        );
      ?>
      <a class="btn btn-dark ml-20" href="<?= Url::to('/settings/translationImport'); ?>"><i class="wb-upload mr-5"></i> <?= Yii::t('app', 'Import Translations'); ?></a>
      <a class="btn btn-primary btn-outline ml-20" href="<?= Url::to('/settings/translationExport'); ?>"><i class="wb-download mr-5"></i> <?= Yii::t('app', 'Export Translations'); ?></a>
    </div>
  <?php endif; ?>
</div>

<div class="page-content">
  <div class="panel">
    <div class="panel-body container-fluid">
      <div class="row row-lg">
        <div class="col-lg-12">
          <?php
            $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
              'id' => $form_id,
              'enableAjaxValidation' => false
            ]);
          ?>
          <input type="hidden" id="category-destination" name="Category" value="">
          <table id="translation-table" class="items table table-striped table-hover">
            <thead>
              <tr>
                <th style="text-align: left; text-transform: uppercase"><?= $category === 'app' ? 'ENGLISH' : Yii::app()->i18n->language_name($default_language); ?></th>
                <?php foreach ( $vec_languages as $language_id => $language_name ) : ?>
                  <th style="text-align: left;"><?= StringHelper::uppercase($language_name); ?></th>
                <?php endforeach; ?>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php if ( !empty($vec_translation_source_models) ) : ?>
                <?php foreach ( $vec_translation_source_models as $translation_source_model ) : ?>
                  <tr id="row-<?= $translation_source_model->id; ?>">
                    <td>
                      <?=
                        $form->textField($translation_source_model, 'message', [
                          'name' => 'TranslationSource['. $translation_source_model->id .']',
                          'maxlength' => 512,
                          'placeholder' => ''
                        ]);
                      ?>
                    </td>
                    <?php foreach ( $vec_languages as $language_id => $language_name ) : ?>
                      <td>
                        <?php
                          $translation_message_model = $translation_source_model->get_translation_model($language_id);
                          if ( $translation_message_model )
                          {
                            echo $form->textField($translation_message_model, 'translation', [
                              'name' => 'TranslationMessage['. $language_id .']['. $translation_message_model->id .']',
                              'maxlength' => 512,
                              'placeholder' => ''
                            ]);
                          }
                          else
                          {
                            $translation_message_model = Yii::createObject(TranslationMessage::class);
                            $translation_message_model->language = $language_id;
                            echo $form->textField($translation_message_model, 'translation', [
                              'name' => 'TranslationMessage['. $language_id .']['. $translation_source_model->id .']',
                              'maxlength' => 512,
                              'placeholder' => ''
                            ]);
                          }
                        ?>
                      </td>
                    <?php endforeach; ?>
                    <td class="remove-col">
                      <a class="btn btn-sm btn-icon btn-pure btn-default remove-btn" data-toggle="tooltip" href="#" data-original-title="<?= Yii::t('app', 'Delete'); ?>"><i class="wb-close"></i></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              <?php endif; ?>
              <?php foreach ( $vec_new_translation_models as $num_translation => $translation_source_model ) : ?>
                <tr id="row-new-<?= $num_translation; ?>"<?php if ( $num_translation > 1 ) : ?> class="hide"<?php endif; ?>>
                  <td>
                   <?php
                      echo $form->textField($translation_source_model, 'message', [
                        'name' => 'TranslationSource[new-'. $num_translation .']',
                        'maxlength' => 512,
                        'placeholder' => ''
                      ]);
                    ?>
                  </td>
                  <?php foreach ( $vec_languages as $language_id => $language_name ) : ?>
                    <td>
                      <?php
                        $translation_message_model = Yii::createObject(TranslationMessage::class);
                        $translation_message_model->language = $language_id;
                        echo $form->textField($translation_source_model, 'message', [
                          'name' => 'TranslationMessage['. $language_id .'][new-'. $num_translation .']',
                          'maxlength' => 512,
                          'placeholder' => ''
                        ]);
                      ?>
                    </td>
                  <?php endforeach; ?>
                  <td class="remove-col">
                    <a class="btn btn-sm btn-icon btn-pure btn-default remove-btn" data-toggle="tooltip" href="#" data-original-title="<?= Yii::t('app', 'Delete'); ?>"><i class="wb-close"></i></a>
                  </td>
                </tr>
              <?php endforeach; ?>
                <?php /*if ( !empty($vec_translation_source_models) ) : ?>
                  <?php foreach ( $vec_translation_source_models as $translation_source_model ) : ?>

                  <?php endforeach; ?>
                <?php endif;*/ ?>
            </tbody>
          </table>
          <p><a class="btn btn-dark" id="add-text-btn" href="#" data-id="2"><i class="icon wb-plus"></i> <?= Yii::t('app', 'Add new text'); ?></a></p>

          <?php
              /*
              |--------------------------------------------------------------------------
              | ACTIONS
              |--------------------------------------------------------------------------
              */
            ?>  
            <div class="form-group row">
              <div class="col-lg-12 form-actions buttons">
                <hr>
                <?php
                  // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
                  $this->widget('@bootstrap.widgets.TbButton', [
                    'buttonType'  => 'submit',
                    'type'        => 'primary',
                    'label'       => Yii::t('app', 'Save'),
                  ]);
                ?>
              </div><!-- form-actions -->
            </div>
          <?php
            // End model form
            $this->endWidget();
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
