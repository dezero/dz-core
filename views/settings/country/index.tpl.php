<?php
/*
|--------------------------------------------------------------------------
| Admin list page for Country model
|--------------------------------------------------------------------------
|
| Available variables:
|   $model: Country model class
|
*/  
  use dz\helpers\Html;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = Yii::t('app', 'Countries List');
?>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
</div>
<div class="page-content">
  <div class="row row-lg">
    <div class="col-lg-12">
      <div class="panel panel-top-summary">
        <div class="panel-body container-fluid">
          <div class="page-content-actions">
            <a href="<?= Url::to('/settings/country', ['clearFilters' => 1]); ?>" class="btn btn-default btn-sm"><i class="wb-close mr-5"></i> <?= Yii::t('app', 'Clear filters'); ?></a>
          </div>
          <?php
            /*
            |----------------------------------------------------------------------------------------
            | GridView widget
            |----------------------------------------------------------------------------------------
            */
              $this->widget('dz.grid.GridView', [
                'id'            => 'country-grid',
                'dataProvider'  => $model->search(),
                'filter'        => $model,
                'emptyText'     => Yii::t('app', 'No countries have been found'),
                'enableHistory' => true,
                'loadModal'     => true,
                'type'          => ['striped', 'hover'],
                'beforeAjaxUpdate' => 'js:function(id, options){ $("html, body").animate({scrollTop: 0}, 100); }',
                'columns'       => [
                  'country_code',
                  'name',
                  [
                    'name' => 'is_eu',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("_grid_column", ["column" => "is_eu", "model" => $data]))',
                    'filter' => ['all' => '- Select -', 0 => 'No', 1 => 'Yes']
                  ],
                  [
                    'class' => 'dz.grid.ButtonColumn',
                    'header' => Yii::t('app', 'ACTION'),
                    'template' => '{update}',
                    'deleteButton' => false,
                    'clearButton' => true,
                    'viewButton' => false,
                    'updateButton' => true,
                  ],
                ],
              ]);
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>