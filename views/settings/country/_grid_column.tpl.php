<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for Country model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: Country model
|  - $column: Column name
|
*/
  use dz\helpers\Html;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "is_eu"
    |--------------------------------------------------------------------------
    */
    case 'is_eu':
  ?>
    <?= ( $model->is_eu == 1 ) ? Yii::t('app', 'Yes') : Yii::t('app', 'No'); ?>
  <?php break; ?>
<?php endswitch; ?>