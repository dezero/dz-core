<?php
/*
|--------------------------------------------------------------------------
| Update form page for Country model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: Country model
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;

  // Page title
  $this->pageTitle = Yii::t('app', 'Update Country');
?>

<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <div class="page-header-actions">
    <?=
      Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app', 'Back'), ["/settings/country"], [
        'class' => 'btn btn-dark'
      ]);
    ?>
  </div>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => 'Countries',
        'url' => ['/settings/country/'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <?php
    // Render form
    $this->renderPartial('_form', [
      'model'   => $model,
      'form_id' => $form_id,
      'button'  => Yii::t('app', 'Save')
    ]);
  ?>
</div>