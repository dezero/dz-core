<?php
/*
|--------------------------------------------------------------------------
| Form partial page for Country model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: Country model
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;

  // Current action name
  $current_action = Yii::currentAction(true);

  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal',
    ]
  ]);

  // Error summary
  $errors = $form->errorSummary($model);
  if ( $errors )
  {
    echo $errors;
  }

  /*
  |--------------------------------------------------------------------------
  | MONEDA
  |--------------------------------------------------------------------------
  */
?>
<div class="panel">
  <div class="panel-body">
    <div class="form-group row<?php if ( $model->hasErrors('country_code') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'country_code', array('class' => 'col-lg-2 col-sm-2 form-control-label')); ?>
      <div class="col-lg-8">
        <?=
          $form->textField($model, 'country_code', array(
            'maxlength' => 4
          ));
        ?>
        <?= $form->error($model, 'country_code'); ?>
        <p class="help-block">Country code following the <a href="https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements" target="_blank">ISO 3166-1 alpha-2 codes</a>. Please, use UPPERCASE.</p>
      </div>
    </div>

    <div class="form-group row<?php if ( $model->hasErrors('name') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'name', array('class' => 'col-lg-2 col-sm-2 form-control-label')); ?>
      <div class="col-lg-8">
        <?=
          $form->textField($model, 'name', array(
            'maxlength' => 64
          ));
        ?>
        <?= $form->error($model, 'name'); ?>
        <p class="help-block">Country name in English.</p>
      </div>
    </div>

    <div class="form-group row<?php if ( $model->hasErrors('is_eu') ) : ?> has-danger<?php endif; ?>">
      <?php echo $form->label($model, 'is_eu', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <div class="form-group form-radio-group">
          <div class="radio-custom radio-default radio-inline">
              <input type="radio" id="is_eu-1" name="Country[is_eu]" value="1"<?php if ( $model->is_eu == 1 ) : ?> checked<?php endif; ?>>
              <label for="Country[is_eu]">Yes</label>
            </div>
            <div class="radio-custom radio-default radio-inline">
              <input type="radio" id="is_eu-0" name="Country[is_eu]" value="0"<?php if ( $model->is_eu == 0 ) : ?> checked<?php endif; ?>>
              <label for="Country[is_eu]">No</label>
            </div>
          </div>
        <?php echo $form->error($model,'is_eu'); ?>
        <p class="help-block">Country belongs to European Union. Check <a href="https://ec.europa.eu/eurostat/statistics-explained/index.php/Glossary:Country_codes" target="_blank">current countries list</a>.</p> 
      </div>
    </div>
  </div><!-- .panel-body -->
</div><!-- .panel -->

<?php
  /*
  |--------------------------------------------------------------------------
  | ACCIONES
  |--------------------------------------------------------------------------
  */
?>  
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
        $this->widget('@bootstrap.widgets.TbButton', [
          'buttonType'=> 'submit',
          'type'      => 'primary',
          'label'     => $button,
        ]);
        
        // Cancel
        echo Html::link(Yii::t('app', 'Cancel'), ['/settings/country'], array('class' => 'btn btn-dark') );
      ?>
    </div><!-- form-actions -->
  </div>

<?php
  // End model form
  $this->endWidget();
?>