<?php
/*
|--------------------------------------------------------------------------
| Form partial page for exporting translation messages into an Excel
|--------------------------------------------------------------------------
|
| Available variables:
|  - $default_language: Default language code
|  - $default_language_model: Language model
|  - $vec_extra_languages: Array with EXTRA languages
|  - $vec_translation_categories: Array with all translation categories
|  - $translation_category: Selected translation category
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;

  // Controller and action names in lowercase
  $current_action = Yii::currentAction(true);
?>
<div class="panel">
  <div class="panel-body">

    <?php
      // Create form object
      $upload_form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
        'id' => $form_id,
        'enableAjaxValidation' => false,
        'htmlOptions' => [
          'class' => 'form-horizontal',
          'enctype' => 'multipart/form-data',
          'autocomplete' => 'off'
        ]
      ]);

      // Error summary
      /*
      $errors = $upload_form->errorSummary([$upload_file_model, $batch_import_model]);
      if ( $errors )
      {
        echo $errors;
      }
      */
    ?>

    <div class="row pb-20">
      <div class="col-lg-12">
        <h3 class="panel-title pt-0 pb-20"><?= Yii::t('app', '1. Select a language'); ?></h3>
        <div class="form-group pl-30">
          <div style="max-width: 350px">
            <?=
              Html::dropDownList(
                'ExportLanguageId',
                null,
                $vec_extra_languages,
                [
                  'class'             => 'form-control',
                  'id'                => 'export-language',
                  'autocomplete'      => 'off'
                  // 'placeholder'       =>  Yii::t('app', 'Select a language'),
                  // 'data-placeholder'  =>  Yii::t('app', 'Select a language'),
                  // 'empty'             =>  Yii::t('app', 'Select a language'),
                  // 'data-plugin'       => 'select2',
                  // 'multiple'          => false,
                  // 'data-allow-clear'  => false,
                  // 'style'             => 'max-width: 350px',
                ]
              );
            ?>
          </div>
          <p class="help-block"><?= Yii::t('backend', 'Default language is :language', [':language' => $default_language_model->title()]); ?></p>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <h3 class="panel-title pt-0 pb-15"><?= Yii::t('backend', '2. Select category'); ?></h3>
        <div class="form-group pl-30">
          <div style="max-width: 350px">
            <?=
              Html::dropDownList(
                'TranslationCategory',
                $translation_category,
                $vec_translation_categories,
                [
                  'class'             => 'form-control',
                  'id'                => 'export-category',
                  'autocomplete'      => 'off'
                  // 'placeholder'       => Yii::t('app', 'Select a category'),
                  // 'data-placeholder'  => Yii::t('app', 'Select a category'),
                  // 'empty'             => Yii::t('app', 'Select a category'),
                  // 'data-plugin'       => 'select2',
                  // 'multiple'          => false,
                  // 'data-allow-clear'  => false,
                  // 'style'             => 'max-width: 350px',
                ]
              );
            ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <div class="pl-30 pt-20">
          <?php
            // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
            $this->widget('@bootstrap.widgets.TbButton', [
              'buttonType'=> 'submit',
              'type'      => 'primary',
              'label'     => Yii::t('backend', 'Export translations'),
            ]);
          ?>
        </div>
      </div>
    </div>

    <?php
      // End form
      $this->endWidget();
    ?>
  </div>
</div>

<?php
  /*
  |--------------------------------------------------------------------------
  | ACTIONS
  |--------------------------------------------------------------------------
  */
?>
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Cancel
        echo Html::link(Yii::t('app', 'Cancel'), ['/settings/translationImport'], ['class' => 'btn btn-dark ml-5']);
      ?>
    </div><!-- form-actions -->
  </div>
