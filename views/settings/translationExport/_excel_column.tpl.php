<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for Export file (Excel)
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: Current model
|  - $column: Column name
|
*/

  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<?php switch ( $column ) :
   /*
    |--------------------------------------------------------------------------
    | COLUMN "translation"
    |--------------------------------------------------------------------------
    */
    case 'translation':
  ?>
    <?php
      $translation_message_model = $model->get_translation_model($language_id);
      if ( $translation_message_model ) :
    ?>
      <?= $translation_message_model->translation; ?>
    <?php endif; ?>
  <?php break; ?>
<?php endswitch; ?>
