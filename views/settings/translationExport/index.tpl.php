<?php
/*
|--------------------------------------------------------------------------
| Export translation messages page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $default_language: Default language code
|  - $default_language_model: Language model
|  - $vec_extra_languages: Array with EXTRA languages
|  - $vec_translation_categories: Array with all translation categories
|  - $translation_category: Selected translation category
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = Yii::t('app', 'Export translation messages');
?>
<div class="page-header">
  <h1 class="page-title">
    <?= $this->pageTitle; ?>
  </h1>
  <div class="page-header-actions">
    <?=
      Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app', 'Back'), ['/settings/translation'], [
        'class' => 'btn btn-dark'
      ]);
    ?>
  </div>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => Yii::t('app', 'Manage translations'),
        'url' => ['/settings/translation'],
      ],
      [
        'label' => Yii::t('app', 'Export translations'),
        'url' => ['/settings/translationExport'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>
<div class="page-content container-fluid">
  <?php
    $this->renderPartial('//settings/translationExport/_form', [
      'default_language'            => $default_language,
      'default_language_model'      => $default_language_model,
      'vec_extra_languages'         => $vec_extra_languages,
      'vec_translation_categories'  => $vec_translation_categories,
      'translation_category'        => $translation_category,
      'form_id'                     => $form_id
    ]);
  ?>
</div>
