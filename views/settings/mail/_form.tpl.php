<?php
/*
|--------------------------------------------------------------------------
| Form partial page for MailTemplate model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: MailTemplate model
|  - $vec_template_files: Array with view files for mail templates
|  - $vec_mail_config: Mail configuration settings
|  - $vec_translated_models: Array with translated MailTemplate models
|  - $default_language: Default language code
|  - $vec_extra_languages: Array with Extra languages
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  use dz\helpers\Url;

  // Current action name
  $current_action = Yii::currentAction(true);

  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal mail-form-wrapper',
      'autocomplete' => 'off'
    ]
  ]);

  // Error summary
  if ( $current_action !== 'create' && isset($vec_translated_models) && !empty($vec_translated_models) )
  {
    $errors = $form->errorSummary(\CMap::mergeArray([$model], $vec_translated_models));  
  }
  else
  {
    $errors = $form->errorSummary($model);
  }
  if ( $errors )
  {
    echo $errors;
  }
?>
<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title">Template</h3>
  </header>
  <div class="panel-body">
    <div class="form-group row<?php if ( $model->hasErrors('alias') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'alias', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?php if ( $current_action == 'create' ) : ?>
          <?=
            $form->textField($model, 'alias', [
              'maxlength' => 64,
              'placeholder' => '',
            ]);
          ?>
        <?php else: ?>
          <div class="form-control view-field">
            <strong><?= $model->alias; ?></strong>
          </div>
        <?php endif; ?>
        <?php /*<p class="help-block">Nombre INTERNO identificativo en inglés. No introducir espacios ni números. Por ejemplo: <em>welcome</em></p>*/ ?>
        <p class="help-block">Identification name written in English for internal use. Only lower case letters are allowed. For example: <em>welcome</em></p>
        <?= $form->error($model,'alias'); ?>
      </div>
    </div>

    <div class="form-group row<?php if ( $model->hasErrors('name') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'name', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?=
          $form->textField($model, 'name', [
            'maxlength' => 128,
            'placeholder' => ''
          ]);
        ?>
        <p class="help-block">Internal name. For example: <em>WELCOME EMAIL</em></p>
        <?= $form->error($model,'name'); ?>
      </div>
    </div>

    <div class="form-group row<?php if ( $model->hasErrors('description') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'description', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?=
          $form->textArea($model, 'description', [
            'rows' => 2
          ]);
        ?>
        <?php /*<p class="help-block">Descripción INTERNA de la plantilla. Por ejemplo: <em>Email que se envía cuando el usuario se registra por primera vez</em></p> */?>
        <p class="help-block">Template description for internal use. For example: <em>This email is sent at the time of the first registration.</em></p>
        <?= $form->error($model,'description'); ?>
      </div>
    </div>
  </div>
</div>

<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title">Email Settings</h3>
  </header>
  <div class="panel-body">

    <div class="form-group row<?php if ( $model->hasErrors('sender_name') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'sender_name', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?=
          $form->textField($model, 'sender_name', [
            'placeholder' => '',
            'maxlength' => 255
          ]);
        ?>
        <?= $form->error($model,'sender_name'); ?>
        <p class="help-block">The name that the email will appear from in your email notification.</p>
      </div>
    </div>

    <div class="form-group row<?php if ( $model->hasErrors('sender_email') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'sender_email', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?=
          $form->textField($model, 'sender_email', [
            'placeholder' => '',
            'maxlength' => 255
          ]);
        ?>
        <?= $form->error($model,'sender_email'); ?>
        <p class="help-block">The email address that the email will appear from in your email notification.</p>
      </div>
    </div>
  </div>
</div>

<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title">Email Content</h3>
  </header>
  <div class="panel-body">
    <?php
      /**
       * EXTRA LANGUAGES in TABS
       */
      if ( $current_action !== 'create' && Yii::app()->i18n->is_multilanguage() ) :
    ?>
      <div class="nav-tabs-horizontal pt-10" data-plugin="tabs">
        <ul class="nav nav-tabs nav-tabs-line tabs-line-top" role="tablist">
          <li class="nav-item" role="presentation">
            <a class="nav-link active" data-toggle="tab" href="#lang-<?= $default_language; ?>" aria-controls="lang-<?= $default_language; ?>" role="tab" aria-expanded="false" data-language="<?= $default_language; ?>"><span class="flag-icon flag-icon-<?= $default_language; ?>"></span> <?= StringHelper::strtoupper(Yii::app()->i18n->language_name($default_language)); ?></a>
          </li>
          <?php if ( isset($vec_extra_languages) && !empty($vec_extra_languages) ) : ?>
            <?php foreach ( $vec_extra_languages as $language_id ) : ?>
              <?php if ( isset($vec_translated_models[$language_id]) ) : ?>
                <li class="nav-item" role="presentation">
                  <a class="nav-link" data-toggle="tab" href="#lang-<?= $language_id; ?>" aria-controls="lang-<?= $language_id; ?>" role="tab" aria-expanded="false" data-language="<?= $language_id; ?>"><span class="flag-icon flag-icon-<?= $language_id; ?>"></span> <?= StringHelper::strtoupper(Yii::app()->i18n->language_name($language_id)); ?></a>
                </li>
              <?php endif; ?>
            <?php endforeach; ?>
          <?php endif; ?>
        </ul>
      </div>
      <div class="tab-content pt-20">
        <div class="tab-pane active" id="lang-<?= $default_language; ?>" role="tabpanel" aria-expanded="false">
    <?php endif; ?>
    
    <div class="form-group row<?php if ( $model->hasErrors('subject') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'subject', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?=
          $form->textField($model, 'subject', [
            'placeholder' => '',
            'maxlength' => 255
          ]);
        ?>
        <p class="help-block">The subject line for the email notification. See the <a href="#tokens">replacement tokens</a> below.</p>
        <?= $form->error($model,'subject'); ?>
      </div>
    </div>

    <div class="form-group row<?php if ( $model->hasErrors('body') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'body', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?php
          $this->widget('@ext.DzMarkdown.DzMarkdown', [
            'model'     => $model,
            'attribute' => 'body',
            'options'   => ['minHeight' => 250]
          ]);

          /*
          $form->textArea($model, 'body', [
            'rows' => 12
          ]);
          */
          /*
          $this->widget('@lib.redactor.ERedactorWidget', [
            'model'   => $model,
            'attribute' => 'body',
            'options' => [
              'minHeight' => 200,
              'buttonSource' => TRUE,
              // 'buttons' => ['|', 'html'],
            ],
          ]);
          */
        ?>
        <p class="help-block">
          The HTML content of the email notification. See the <a href="<?= Url::to('/help/markdown'); ?>" target="_blank">format guide</a>.<br>
          Replacement tokens are allowed. See the <a href="#tokens">replacement tokens</a> below.
        </p>
        <?= $form->error($model,'body'); ?>
      </div>
    </div>

    <?php
      /*
      |--------------------------------------------------------------------------
      | EXTRA LANGUAGES in TABS
      |--------------------------------------------------------------------------
      */
      if ( $current_action !== 'create' && Yii::app()->i18n->is_multilanguage() ) :
    ?>
      </div><!-- .tab-pane -->
      <?php if ( isset($vec_extra_languages) && !empty($vec_extra_languages) ) : ?>
        <?php foreach ( $vec_extra_languages as $language_id ) : ?>
          <?php if ( isset($vec_translated_models[$language_id]) ) : ?>
            <?php
              $translated_mail_model = $vec_translated_models[$language_id];
            ?>
            <div class="tab-pane" id="lang-<?= $language_id; ?>" role="tabpanel" aria-expanded="false">
              
              <div class="form-group row<?php if ( $translated_mail_model->hasErrors('subject') ) : ?> has-danger<?php endif; ?>">
                <?= $form->label($translated_mail_model, 'subject', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
                <div class="col-lg-10">
                  <?=
                    $form->textField($translated_mail_model, 'subject', [
                      'name'        => 'TranslatedMail['. $language_id .'][subject]',
                      'placeholder' => '',
                      'maxlength' => 255
                    ]);
                  ?>
                  <p class="help-block">The subject line for the email notification. See the <a href="#tokens">replacement tokens</a> below.</p>
                  <?= $form->error($translated_mail_model,'subject'); ?>
                </div>
              </div>

              <div class="form-group row<?php if ( $translated_mail_model->hasErrors('body') ) : ?> has-danger<?php endif; ?>">
                <?= $form->label($translated_mail_model, 'body', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
                <div class="col-lg-10">
                  <?php
                    $this->widget('@ext.DzMarkdown.DzMarkdown', [
                      'model'     => $translated_mail_model,
                      'attribute' => 'body',
                      'options'   => [
                        'minHeight' => 250
                      ],
                      'htmlOptions' => [
                        'name'        => 'TranslatedMail['. $language_id .'][body]',
                      ]
                    ]);

                    /*
                    $form->textArea($translated_mail_model, 'body', [
                      'name'  => 'TranslatedMail['. $language_id .'][body]',
                      'rows'  => 12
                    ]);
                    */
                    /*
                    $this->widget('@lib.redactor.ERedactorWidget', [
                      'model'   => $translated_mail_model,
                      'attribute' => 'body',
                      'options' => [
                        'minHeight' => 200,
                        'buttonSource' => TRUE,
                        // 'buttons' => ['|', 'html'],
                      ],
                    ]);
                    */
                  ?>
                  <p class="help-block">
                    The HTML content of the email notification. See the <a href="<?= Url::to('/help/markdown'); ?>" target="_blank">format guide</a>.<br>
                    Replacement tokens are allowed. See the <a href="#tokens">replacement tokens</a> below.
                  </p>
                  <?= $form->error($translated_mail_model,'body'); ?>
                </div>
              </div>

            </div><!-- .tab-pane -->
          <?php endif; ?>
        <?php endforeach; ?>
      <?php endif; ?>
      </div><!-- .tab-content -->
    <?php endif; ?>
  </div>
</div>

<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title">Extra Settings</h3>
  </header>
  <div class="panel-body">

    <div class="form-group row<?php if ( $model->hasErrors('reply_email') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'reply_email', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?=
          $form->textField($model, 'reply_email', [
            'placeholder' => '',
            'maxlength' => 255
          ]);
        ?>
        <?= $form->error($model,'reply_email'); ?>
        <p class="help-block">Optional. The reply-to email address for your email notification. Leave blank to use 'From Email' address</p>
      </div>
    </div>

    <div class="form-group row<?php if ( $model->hasErrors('recipient_emails') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'recipient_emails', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?=
          $form->textField($model, 'recipient_emails', [
            'placeholder' => '',
            'maxlength' => 255
          ]);
        ?>
        <?= $form->error($model,'recipient_emails'); ?>
        <p class="help-block">Optional. Fixed email address(es) you would like to be as email recipient. Separate multiples with commas. Leave blank to not use.</p>
      </div>
    </div>

    <div class="form-group row<?php if ( $model->hasErrors('cc_emails') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'cc_emails', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?=
          $form->textField($model, 'cc_emails', [
            'placeholder' => '',
            'maxlength' => 255
          ]);
        ?>
        <?= $form->error($model,'cc_emails'); ?>
        <p class="help-block">Optional. The email address(es) you would like to be CC'd in the email notification. Separate multiples with commas. Leave blank to not use.</p>
      </div>
    </div>

    <div class="form-group row<?php if ( $model->hasErrors('bcc_emails') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'bcc_emails', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?=
          $form->textField($model, 'bcc_emails', [
            'placeholder' => '',
            'maxlength' => 255
          ]);
        ?>
        <?= $form->error($model,'bcc_emails'); ?>
        <p class="help-block">Optional. The email address(es) you would like to be BCC'd in the email notification. Separate multiples with commas. Leave blank to not use.</p>
      </div>
    </div>

    <div class="form-group row<?php if ( $model->hasErrors('template_file_path') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'template_file_path', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <div style="max-width: 350px">
          <?=
            $form->dropDownList($model, 'template_file_path', $vec_template_files, [
              'class'             => 'form-control',
              'data-plugin'       => 'select2',
              'data-placeholder'  => 'Select a view template file...',
              'data-allow-clear'  => false,
              'style'             => 'max-width: 350px',
            ]);
          ?>
        </div>
        <?= $form->error($model,'template_file_path'); ?>
        <p class="help-block">View file used as mail template. Full path on <em><?= Yii::getAlias($vec_mail_config['viewPath']); ?></em></p>
      </div>
    </div>

  </div>
</div>

<?php
  /**
   * REPLACEMENT TOKENS HELP
   */
?>
<?php
  $this->renderPartial('//settings/mail/_tokens_help', [
    'language_id'=> Yii::app()->language,
    'model' => $model
  ]);
?>

<?php
  /*
  |--------------------------------------------------------------------------
  | ACCIONES
  |--------------------------------------------------------------------------
  */
?>  
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
        $this->widget('@bootstrap.widgets.TbButton', [
          'buttonType'=> 'submit',
          'type'      => 'primary',
          'label'     => $button,
        ]);
        
        // Cancel
        echo Html::link(Yii::t('app', 'Cancel'), ['/settings/mail'], ['class' => 'btn btn-dark']);
      ?>
    </div><!-- form-actions -->
  </div>

<?php
  // End model form
  $this->endWidget();
?>