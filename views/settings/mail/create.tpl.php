<?php
/*
|--------------------------------------------------------------------------
| Create form page for MailTemplate model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: MailTemplate model
|  - $vec_template_files: Array with view files for mail templates
|  - $vec_mail_config: Mail configuration settings
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;

  // Page title
  $this->pageTitle = Yii::t('app', 'Add New Email Template');
?>

<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => Yii::t('app', 'Email Templates'),
        'url' => ['/settings/mail/'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <?php
    // Render form
    $this->renderPartial('//settings/mail/_form', [
      'model'               => $model,
      'vec_template_files'  => $vec_template_files,
      'vec_mail_config'     => $vec_mail_config,
      'form_id'             => $form_id,
      'button'              => Yii::t('app', 'Save')
    ]);
  ?>
</div>