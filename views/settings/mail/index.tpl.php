<?php
/*
|--------------------------------------------------------------------------
| Admin list page for MailTemplate model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: MailTemplate model class
|
*/  
  use dz\helpers\Html;

  // Page title
  $this->pageTitle = Yii::t('app', 'Manage Email Templates');
?>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <div class="page-header-actions">
    <?=
      Html::link('<i class="icon wb-plus"></i> Add new email template', ["create"], [
        'class' => 'btn btn-primary'
      ]);
    ?>
    <?php
      /*
      echo Html::link('<i class="icon wb-envelope"></i> Sent a test email', ["test"], [
        'class' => 'btn btn-warning'
      ]);
      */
    ?>
  </div>
</div>
<div class="page-content">
  <div class="row row-lg">
    <div class="col-lg-12">
      <div class="panel panel-top-summary">
        <div class="panel-body container-fluid">
          <?php
            /*
            |----------------------------------------------------------------------------------------
            | GridView widget
            |----------------------------------------------------------------------------------------
            */
              $this->widget('dz.grid.GridView', [
                'id'            => 'mail-grid',
                'dataProvider'  => $model->search(),
                'filter'        => $model,
                'enableHistory' => true,
                'enableSorting' => false,
                'loadModal'     => true,
                'type'          => ['striped', 'hover'],
                'beforeAjaxUpdate' => 'js:function(id, options){ $("html, body").animate({scrollTop: 0}, 100); }',
                'columns'       => [
                  'alias',
                  'name',
                  'subject',
                  // 'description',
                  [
                    'class' => 'dz.grid.ButtonColumn',
                    'header' => Yii::t('app', 'ACTION'),
                    'template' => '{update}{delete}',
                    'deleteButton' => true,
                    'clearButton' => true,
                    'viewButton' => false,
                    'updateButton' => true,
                  ],
                ],
              ]);
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>