<?php
/*
|--------------------------------------------------------------------------
| Update form page for MailTemplate model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: MailTemplate model
|  - $vec_template_files: Array with view files for mail templates
|  - $vec_mail_config: Mail configuration settings
|  - $vec_translated_models: Array with translated MailTemplate models
|  - $default_language: Default language code
|  - $vec_extra_languages: Array with Extra languages
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;

  // Page title
  $this->pageTitle = Yii::t('app', 'Update Email Template');
?>

<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <div class="page-header-actions">
    <?php
      /*
      echo Html::link('<i class="icon wb-envelope"></i> Send a test email', ["test", 'template_id' => $model->template_id], [
        'class' => 'btn btn-warning'
      ]);
      */
      echo Html::link(Yii::t('app', 'Back'), ["/settings/mail"], [
        'class' => 'btn btn-dark'
      ]);
    ?>
  </div>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => Yii::t('app', 'Email Templates'),
        'url' => ['/settings/mail/'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <?php
    // Render form
    $this->renderPartial('//settings/mail/_form', array(
      'model'                 => $model,
      'vec_template_files'    => $vec_template_files,
      'vec_mail_config'       => $vec_mail_config,
      'vec_translated_models' => $vec_translated_models,
      'default_language'      => $default_language,
      'vec_extra_languages'   => $vec_extra_languages,
      'form_id'               => $form_id,
      'button'                => Yii::t('app', 'Save')
    ));
  ?>
</div>