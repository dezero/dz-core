<?php
/*
|--------------------------------------------------------------------------
| Special page for testing email templates
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: MailHistory model
|  - $vec_dummy_orders: Dummy data with orders
|  - $vec_dummy_customers: Dummy data with customers
|  - $vec_mail_templates: Array with mail templates for dropdown
|  - $vec_languages: Array with defined languages
|
*/
	// Page title
	$this->pageTitle = Yii::t('app', 'Test Plantilla Email');
?>

<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <div class="page-header-actions">
    <?php
      echo Html::link('Administrar plantillas email', array("index"), array(
        'class' => 'btn btn-dark'
      ));
    ?>
  </div>
  <?php
    // Breadcrumbs
    echo Html::breadcrumbs(array(
      array(
        'label' => 'Plantillas Email',
        'url' => array('/config/mailTemplate/'),
      ),
      $this->pageTitle
    ));
  ?>
</div>

<div class="page-content container-fluid">
  <?php
    $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm',array(
      'id' => 'test-mail-template-form',
      'enableAjaxValidation' => FALSE,
      'htmlOptions' => array(
        'class' => 'form-horizontal',
      )
    ));

    $errors = $form->errorSummary($model);
    if ( $errors )
    {
      echo $errors;
    }
  ?>
  <div class="panel">
    <div class="panel-body">

      <div class="form-group row<?php if ( $model->hasErrors('template_id') ) : ?> has-danger<?php endif; ?>">
        <label class="col-lg-2 col-sm-2 form-control-label">Plantilla email</label>
        <div class="col-lg-10">
          <?= $form->dropDownList($model, 'template_id', $vec_mail_templates); ?>
          <?= $form->error($model,'template_id'); ?>
        </div>
      </div>

      <?php if ( isset($vec_languages) AND !empty($vec_languages) ) : ?>
        <div class="form-group row<?php if ( $model->hasErrors('language_id') ) : ?> has-danger<?php endif; ?>">
          <label class="col-lg-2 col-sm-2 form-control-label">Idioma</label>
          <div class="col-lg-3">
            <?= $form->dropDownList($model, 'language_id', $vec_languages); ?>
            <?= $form->error($model,'language_id'); ?>
          </div>
        </div>
      <?php endif; ?>

      <hr>
      
      <div class="form-group row">
        <label class="col-lg-2 col-sm-2 form-control-label">Clientes</label>
        <div class="col-lg-10">
          <?= $form->dropDownList($model, 'recipient_uid', $vec_dummy_customers); ?>
          <p class="help-block">Selecciona un cliente para hacer las pruebas <em>(puede que no sea requerido para la plantilla)</em></p>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-lg-2 col-sm-2 form-control-label">Pedidos</label>
        <div class="col-lg-10">
          <?= $form->dropDownList($model, 'order_id', $vec_dummy_orders); ?>
          <p class="help-block">Selecciona un pedido de compra para hacer las pruebas <em>(puede que no sea requerido para la plantilla)</em></p>
        </div>
      </div>

      <hr>

      <div class="form-group row<?php if ( $model->hasErrors('recipient_mails') ) : ?> has-danger<?php endif; ?>">
        <label class="col-lg-2 col-sm-2 form-control-label">Email destino</label>
        <div class="col-lg-10">
          <?php
            echo $form->textField($model, 'recipient_mails', array(
              'placeholder' => '',
              'maxlength' => 128
            ));
          ?>
          <p class="help-block">Email donde quieres recibir esta prueba</p>
          <?= $form->error($model,'recipient_mails'); ?>
        </div>
      </div>

    </div>
  </div>

  <?php
    /*
    |--------------------------------------------------------------------------
    | ACCIONES
    |--------------------------------------------------------------------------
    */
  ?>  
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
        $this->widget('@bootstrap.widgets.TbButton', array(
          'buttonType'=> 'submit',
          'type'      => 'primary',
          'label'     => 'Enviar prueba',
        ));
        
        // Cancel
        echo Html::link(Yii::t('app', 'Volver'), array('/config/mailTemplate'), array('class' => 'btn btn-dark') );

        // More links
        // echo '<div class="btn-group dropup">'. Html::more_links($this->menuAction(\dz\db\ActiveRecord::extractPkValue($model, true)), array('class' => 'dropdown-menu')) .'</div>';
      ?>
    </div><!-- form-actions -->
  </div>

<?php
  // End model form
  $this->endWidget();
?>
</div>