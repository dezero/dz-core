<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for Language model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: Language model
|  - $column: Column name
|
*/
  use dz\helpers\Html;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "name"
    |--------------------------------------------------------------------------
    */
    case 'name':
  ?>
    <?= Html::link($model->title(), ['update', 'id' => $model->language_id]); ?>
    <?php
      // Disabled language
      if ( $model->is_disabled() ) :
    ?>
      <span class="badge badge-danger ml-5 mr-5" data-toggle="tooltip" data-placement="right" data-original-title="<?= $model->disable_date; ?>"><?= Yii::t('app', 'disabled'); ?></span>
    <?php endif; ?>
    <?php if ( $model->is_default ) : ?>
      <span class="badge badge-primary ml-5 mr-5"><?= Yii::t('app', 'DEFAULT'); ?></span>
    <?php endif; ?>
  <?php break; ?>
<?php endswitch; ?>
