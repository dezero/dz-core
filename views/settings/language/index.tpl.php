<?php
/*
|--------------------------------------------------------------------------
| Admin list page for Language model
|--------------------------------------------------------------------------
|
| Available variables:
|   $model: Language model class
|
*/  
  use dz\helpers\Html;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = Yii::t('app', 'Manage languages');
?>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <div class="page-header-actions">
    <a class="btn btn-primary btn-outline ml-20" href="<?= Url::to('/settings/translation'); ?>"><i class="wb-eye mr-5"></i> <?= Yii::t('app', 'View translations'); ?></a>
    <?=
      Html::link('<i class="icon wb-plus"></i> '. Yii::t('app', 'Add new language'), ["create"], [
        'class' => 'btn btn-primary'
      ]);
    ?>
  </div>
</div>
<div class="page-content">
  <div class="row row-lg">
    <div class="col-lg-12">
      <div class="panel panel-top-summary">
        <div class="panel-body container-fluid">
          <?php
            /*
            |----------------------------------------------------------------------------------------
            | GridView widget
            |----------------------------------------------------------------------------------------
            */
              $this->widget('dz.grid.GridView', [
                'id'            => 'language-grid',
                'dataProvider'  => $model->search(),
                'filter'        => $model,
                'emptyText'     => Yii::t('app', 'No languages found'),
                'enableHistory' => true,
                'loadModal'     => true,
                'type'          => ['striped', 'hover'],
                'beforeAjaxUpdate' => 'js:function(id, options){ $("html, body").animate({scrollTop: 0}, 100); }',
                'columns'       => [
                  [
                    'name' => 'name',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("_grid_column", ["column" => "name", "model" => $data]))',
                  ],
                  'native',
                  'prefix',
                  [
                    'class' => 'dz.grid.ButtonColumn',
                    'header' => 'ACCIÓN',
                    'template' => '{update}{delete}',
                    'deleteButton' => true,
                    'clearButton' => true,
                    'viewButton' => false,
                    'updateButton' => true,
                    'menuAction' => function($data, $row) {
                      return [
                        [
                        'label' => Yii::t('app', 'Enable'),
                        'icon' => 'arrow-up',
                        'url' => ['/settings/language/enable', 'id' => $data->language_id],
                        'visible' => '!$data->is_enabled()',
                        'confirm' => '<h3>Are you sure you want to <span class=\'text-success\'>ENABLE</span> this language?</h3>',
                        'htmlOptions' => [
                          'id' => 'enable-language-'. $data->language_id,
                          'data-gridview' => 'language-grid'
                          ]
                        ],
                        [
                          'label' => Yii::t('app', 'Disable'),
                          'icon' => 'arrow-down',
                          'url' => ['/settings/language/disable', 'id' => $data->language_id],
                          'visible' => '$data->is_enabled()',
                          'confirm' => '<h3>Are you sure you want to <span class=\'text-danger\'>DISABLE</span> this language?</h3>',
                          'htmlOptions' => [
                            'id' => 'disable-language-'. $data->language_id,
                            'data-gridview' => 'language-grid'
                          ]
                        ]
                      ];
                    },
                  ],
                ],
              ]);
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
