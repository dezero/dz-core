<?php
/*
|--------------------------------------------------------------------------
| Create form page for Language model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: Language model
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  
  // Page title
  $this->pageTitle = Yii::t('app', 'Add new language');
?>

<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => 'Idiomas',
        'url' => ['/settings/language/'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <?php
    // Render form
    $this->renderPartial('_form', [
      'model'   => $model,
      'form_id' => $form_id,
      'button'  => Yii::t('app', 'Add')
    ]);
  ?>
</div>
