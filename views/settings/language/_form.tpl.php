<?php
/*
|--------------------------------------------------------------------------
| Form partial page for Language model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: Language model
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  use dz\modules\settings\models\Language;

  // Current action name
  $current_action = Yii::currentAction(true);

  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal',
    ]
  ]);

  // Error summary
  $errors = $form->errorSummary($model);
  if ( $errors )
  {
    echo $errors;
  }

  /*
  |--------------------------------------------------------------------------
  | MONEDA
  |--------------------------------------------------------------------------
  */
?>
<div class="panel">
  <div class="panel-body">
    <?php if ( $model->isNewRecord ) : ?>
      <div class="form-group row<?php if ( $model->hasErrors('language_id') ) : ?> has-danger<?php endif; ?>">
        <label class="col-lg-2 col-sm-2 form-control-label" for="Language_language_id"><?= $model->getAttributeLabel('language_id'); ?></label>
        <div class="col-lg-10">
           <?=
            $form->dropDownList($model, 'language_id', \CMap::mergeArray(['' => ''], Language::model()->language_full_list()), [
              'class'             => 'form-control',
              'data-plugin'       => 'select2',
              'data-placeholder'  => Yii::t('app', 'Select language') .'...',
              'data-allow-clear'  => true,
              'style'             => 'max-width: 350px',
            ]);
          ?>
          <?= $form->error($model, 'language_id'); ?>
        </div>
      </div>
    <?php else: ?>
      <div class="form-group row<?php if ( $model->hasErrors('language_id') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($model, 'language_id', array('class' => 'col-lg-2 col-sm-2 form-control-label')); ?>
        <div class="col-lg-8">
          <div class="form-control view-field">
            <?= $model->language_id; ?>
          </div>
        </div>
      </div>

      <div class="form-group row<?php if ( $model->hasErrors('native') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($model, 'native', array('class' => 'col-lg-2 col-sm-2 form-control-label')); ?>
        <div class="col-lg-8">
          <?=
            $form->textField($model, 'native', array(
              'maxlength' => 16
            ));
          ?>
          <?= $form->error($model, 'native'); ?>
          <p class="help-block"><?= Yii::t('app', 'Language\'s native name. For example, español, français or deutsche.'); ?></p>
        </div>
      </div>

      <div class="form-group row<?php if ( $model->hasErrors('name') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($model, 'name', array('class' => 'col-lg-2 col-sm-2 form-control-label')); ?>
        <div class="col-lg-8">
          <?=
            $form->textField($model, 'name', array(
              'maxlength' => 64
            ));
          ?>
          <?= $form->error($model, 'name'); ?>
          <p class="help-block"><?= Yii::t('app', 'It appears on the the public website'); ?></p>
        </div>
      </div>

      <div class="form-group row<?php if ( $model->hasErrors('prefix') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($model, 'prefix', array('class' => 'col-lg-2 col-sm-2 form-control-label')); ?>
        <div class="col-lg-8">
          <?=
            $form->textField($model, 'prefix', array(
              'maxlength' => 16
            ));
          ?>
          <?= $form->error($model, 'prefix'); ?>
          <p class="help-block"><?= Yii::t('app', 'Code text used as URL prefix to detect the language automatically'); ?></p>
        </div>
      </div>

      <div class="form-group row<?php if ( $model->hasErrors('is_default') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($model, 'is_default', array('class' => 'col-lg-2 col-sm-2 form-control-label')); ?>
        <div class="col-lg-8">
          <div class="form-group form-radio-group">
            <div class="radio-custom radio-default radio-inline">
              <input type="radio" id="is-show-number-0" name="Language[is_default]" value="0"<?php if ( $model->is_default == 0 ) : ?> checked<?php endif; ?>>
              <label for="Language[is_default]"><?= Yii::t('app', 'No'); ?></label>
            </div>
            <div class="radio-custom radio-default radio-inline">
              <input type="radio" id="is-show-number-1" name="Language[is_default]" value="1"<?php if ( $model->is_default == 1 ) : ?> checked<?php endif; ?>>
              <label for="Language[is_default]"><?= Yii::t('app', 'Yes'); ?></label>
            </div>
          </div>
          <?= $form->error($model,'is_default'); ?>
        </div>
      </div>
    <?php endif; ?>
  </div><!-- .panel-body -->
</div><!-- .panel -->

<?php
  /*
  |--------------------------------------------------------------------------
  | ACCIONES
  |--------------------------------------------------------------------------
  */
?>  
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
        $this->widget('@bootstrap.widgets.TbButton', [
          'buttonType'=> 'submit',
          'type'      => 'primary',
          'label'     => $button,
        ]);
        
        // Cancel
        echo Html::link(Yii::t('app', 'Cancel'), ['/settings/language'], array('class' => 'btn btn-dark') );
      ?>
    </div><!-- form-actions -->
  </div>

<?php
  // End model form
  $this->endWidget();
?>
