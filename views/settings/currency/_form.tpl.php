<?php
/*
|--------------------------------------------------------------------------
| Form partial page for Currency model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: Currency model
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  use dz\modules\settings\models\Currency;

  // Current action name
  $current_action = Yii::currentAction(true);

  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal',
    ]
  ]);

  // Error summary
  $errors = $form->errorSummary($model);
  if ( $errors )
  {
    echo $errors;
  }

  /*
  |--------------------------------------------------------------------------
  | MONEDA
  |--------------------------------------------------------------------------
  */
?>
<div class="panel">
  <div class="panel-body">
    <?php if ( $model->isNewRecord ) : ?>
      <div id="famous-currencies-wrapper" class="form-group row<?php if ( $model->hasErrors('currency_id') ) : ?> has-danger<?php endif; ?>">
        <label class="col-lg-2 col-sm-2 form-control-label" for="Currency_currency_id">Monedas más utilizadas</label>
        <div class="col-lg-10">
           <?=
            $form->dropDownList($model, 'currency_id', \CMap::mergeArray(['' => ''], Currency::model()->currency_full_list()), [
              'class'             => 'form-control',
              'data-plugin'       => 'select2',
              'data-placeholder'  => 'Selecciona moneda...',
              'data-allow-clear'  => true,
              'style'             => 'max-width: 350px',
            ]);
          ?>
          <?= $form->error($model, 'currency_id'); ?>
          <p class="help-block"><a href="#" id="all-currencies-link">Ver todas las monedas</a></p>
        </div>
      </div>

      <div id="all-currencies-wrapper" class="form-group row hide<?php if ( $model->hasErrors('numeric_code') ) : ?> has-danger<?php endif; ?>">
        <label class="col-lg-2 col-sm-2 form-control-label" for="Currency_numeric_code">Todas las monedas</label>
        <div class="col-lg-10">
          <?=
            $form->dropDownList($model, 'numeric_code', \CMap::mergeArray(['' => ''], Currency::model()->currency_full_list(false)), [
              'class'             => 'form-control',
              'data-plugin'       => 'select2',
              'data-placeholder'  => 'Selecciona moneda...',
              'data-allow-clear'  => true,
              'style'             => 'max-width: 350px',
            ]);
          ?>
          <?= $form->error($model,'numeric_code'); ?>
          <p class="help-block"><a href="#" id="famous-currencies-link">Ver sólo las monedas más utilizadas</a></p>
        </div>
      </div>
    <?php else: ?>
      <div class="form-group row<?php if ( $model->hasErrors('currency_id') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($model, 'currency_id', array('class' => 'col-lg-2 col-sm-2 form-control-label')); ?>
        <div class="col-lg-8">
          <div class="form-control view-field">
            <?= $model->currency_id; ?>
          </div>
          <p class="help-block">El código alfabético de la moneda en mayúscula. Por ejemplo EUR.</p>
        </div>
      </div>

      <div class="form-group row<?php if ( $model->hasErrors('numeric_code') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($model, 'numeric_code', array('class' => 'col-lg-2 col-sm-2 form-control-label')); ?>
        <div class="col-lg-8">
          <div class="form-control view-field">
            <?= $model->numeric_code; ?>
          </div>
          <p class="help-block">El código numérico de la moneda. Según la ISO4217 this este código consiste en 3 dígitos donde el primero dígito puede ser un 0.</p>
        </div>
      </div>

      <div class="form-group row<?php if ( $model->hasErrors('name') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($model, 'name', array('class' => 'col-lg-2 col-sm-2 form-control-label')); ?>
        <div class="col-lg-8">
          <?=
            $form->textField($model, 'name', array(
              'maxlength' => 64
            ));
          ?>
          <?= $form->error($model, 'name'); ?>
          <p class="help-block">Nombre de la moneda. Estará disponible para ser traducido a todos los idiomas.</p>
        </div>
      </div>

      <div class="form-group row<?php if ( $model->hasErrors('symbol') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($model, 'symbol', array('class' => 'col-lg-2 col-sm-2 form-control-label')); ?>
        <div class="col-lg-8">
          <?=
            $form->textField($model, 'symbol', array(
              'maxlength' => 16
            ));
          ?>
          <?= $form->error($model, 'symbol'); ?>
          <p class="help-block">El símbolo de la moneda. Por ejemplo &euro;.</p>
        </div>
      </div>

      <div class="form-group row<?php if ( $model->hasErrors('symbol_placement') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($model, 'symbol_placement', array('class' => 'col-lg-2 col-sm-2 form-control-label')); ?>
        <div class="col-lg-8">
          <?=
            $form->dropDownList($model, 'symbol_placement', Currency::model()->symbol_placement_labels(), [
              'class'             => 'form-control',
              'data-plugin'       => 'select2',
              'style'             => 'max-width: 350px',
            ]);
          ?>
          <?= $form->error($model, 'symbol_placement'); ?>
          <p class="help-block">Posición del símbolo de la moneda</p>
        </div>
      </div>

      <div class="form-group row<?php if ( $model->hasErrors('minor_unit') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($model, 'minor_unit', array('class' => 'col-lg-2 col-sm-2 form-control-label')); ?>
        <div class="col-lg-8">
          <?=
            $form->textField($model, 'minor_unit', array(
              'maxlength' => 16
            ));
          ?>
          <?= $form->error($model, 'minor_unit'); ?>
          <p class="help-block">Nombre de la unidad menor de la moneda. Por ejemplo, Céntimo</p>
        </div>
      </div>

      <div class="form-group row<?php if ( $model->hasErrors('major_unit') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($model, 'major_unit', array('class' => 'col-lg-2 col-sm-2 form-control-label')); ?>
        <div class="col-lg-8">
          <?=
            $form->textField($model, 'major_unit', array(
              'maxlength' => 16
            ));
          ?>
          <?= $form->error($model, 'major_unit'); ?>
          <p class="help-block">Nombre de la unidad mayor de la moneda. Por ejemplo, Euro</p>
        </div>
      </div>

      <div class="form-group row<?php if ( $model->hasErrors('decimals_number') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($model, 'decimals_number', array('class' => 'col-lg-2 col-sm-2 form-control-label')); ?>
        <div class="col-lg-8">
          <?=
            $form->textField($model, 'decimals_number', array(
              'maxlength' => 2
            ));
          ?>
          <?= $form->error($model, 'decimals_number'); ?>
          <p class="help-block">Número de decimales a mostrar. Por defecto, 2</p>
        </div>
      </div>

      <div class="form-group row<?php if ( $model->hasErrors('thousands_separator') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($model, 'thousands_separator', array('class' => 'col-lg-2 col-sm-2 form-control-label')); ?>
        <div class="col-lg-8">
          <?=
            $form->textField($model, 'thousands_separator', array(
              'maxlength' => 16
            ));
          ?>
          <?= $form->error($model, 'thousands_separator'); ?>
          <p class="help-block">El caracter que separa los millares. Por ejemplo: . (punto)</p>
        </div>
      </div>

      <div class="form-group row<?php if ( $model->hasErrors('decimal_separator') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($model, 'decimal_separator', array('class' => 'col-lg-2 col-sm-2 form-control-label')); ?>
        <div class="col-lg-8">
          <?=
            $form->textField($model, 'decimal_separator', array(
              'maxlength' => 16
            ));
          ?>
          <?= $form->error($model, 'decimal_separator'); ?>
          <p class="help-block">El caracter que separa los decimales. Por ejemplo: , (coma)</p>
        </div>
      </div>

    <?php endif; ?>
  </div><!-- .panel-body -->
</div><!-- .panel -->

<?php
  /*
  |--------------------------------------------------------------------------
  | ACCIONES
  |--------------------------------------------------------------------------
  */
?>  
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
        $this->widget('@bootstrap.widgets.TbButton', [
          'buttonType'=> 'submit',
          'type'      => 'primary',
          'label'     => $button,
        ]);
        
        // Cancel
        echo Html::link(Yii::t('app', 'Cancel'), ['/settings/currency'], array('class' => 'btn btn-dark') );
      ?>
    </div><!-- form-actions -->
  </div>

<?php
  // End model form
  $this->endWidget();
?>
<?php
// Show all currencies or just famous currencies
/*
if ( $model->isNewRecord )
{
  Yii::app()->clientScript->registerScript('js_all_currencies',
    "$('#all-currencies-link').on('click', function(e){
      e.preventDefault();
      $('#famous-currencies-wrapper').removeClass('hide').addClass('hide');
      $('#all-currencies-wrapper').removeClass('hide');
    });
    $('#famous-currencies-link').on('click', function(e){
      e.preventDefault();
      $('#all-currencies-wrapper').removeClass('hide').addClass('hide');
      $('#famous-currencies-wrapper').removeClass('hide');
    });
    "
  , CClientScript::POS_READY);
}
*/
?>