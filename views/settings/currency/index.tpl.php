<?php
/*
|--------------------------------------------------------------------------
| Admin list page for Currency model
|--------------------------------------------------------------------------
|
| Available variables:
|   $model: Currency model class
|
*/  
  use dz\helpers\Html;

  // Page title
  $this->pageTitle = Yii::t('app', 'Gestión de Monedas');
?>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <div class="page-header-actions">
    <?=
      Html::link('<i class="icon wb-plus"></i> Añadir moneda', ["create"], [
        'class' => 'btn btn-primary'
      ]);
    ?>
  </div>
</div>
<div class="page-content">
  <div class="row row-lg">
    <div class="col-lg-12">
      <div class="panel panel-top-summary">
        <div class="panel-body container-fluid">
          <?php
            /*
            |----------------------------------------------------------------------------------------
            | GridView widget
            |----------------------------------------------------------------------------------------
            */
              $this->widget('dz.grid.GridView', [
                'id'            => 'currency-grid',
                'dataProvider'  => $model->search(),
                'filter'        => $model,
                'emptyText'     => Yii::t('app', 'No se encontraron monedas'),
                'enableHistory' => true,
                'loadModal'     => true,
                'type'          => ['striped', 'hover'],
                'beforeAjaxUpdate' => 'js:function(id, options){ $("html, body").animate({scrollTop: 0}, 100); }',
                'columns'       => [
                  [
                    'name' => 'name',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("_grid_column", ["column" => "name", "model" => $data]))',
                  ],
                  [
                    'class' => 'dz.grid.ButtonColumn',
                    'header' => 'ACCIÓN',
                    'template' => '{update}{delete}',
                    'deleteButton' => TRUE,
                    'clearButton' => TRUE,
                    'viewButton' => FALSE,
                    'updateButton' => TRUE,
                    'menuAction' => function($data, $row) {
                      return [
                        [
                        'label' => Yii::t('app', 'Enable'),
                        'icon' => 'arrow-up',
                        'url' => ['/settings/currency/enable', 'id' => $data->currency_id],
                        'visible' => '!$data->is_enabled()',
                        'confirm' => '<h3>¿Estás seguro de que quieres ACTIVAR esta moneda?</h3>',
                        'htmlOptions' => [
                          'id' => 'enable-currency-'. $data->currency_id,
                          'data-gridview' => 'currency-grid'
                          ]
                        ],
                        [
                          'label' => Yii::t('app', 'Disable'),
                          'icon' => 'arrow-down',
                          'url' => ['/settings/currency/disable', 'id' => $data->currency_id],
                          'visible' => '$data->is_enabled()',
                          'confirm' => '<h3>¿Estás seguro de que quieres DESACTIVAR esta moneda?</h3>',
                          'htmlOptions' => [
                            'id' => 'disable-currency-'. $data->currency_id,
                            'data-gridview' => 'currency-grid'
                          ]
                        ]
                      ];
                    },
                  ],
                ],
              ]);
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>