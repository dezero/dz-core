<?php
/*
|--------------------------------------------------------------------------
| Create form page for Currency model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: Currency model
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  
  // Page title
  $this->pageTitle = Yii::t('app', 'Añadir moneda');
?>

<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => 'Monedas',
        'url' => ['/settings/currency/'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <?php
    // Render form
    $this->renderPartial('_form', [
      'model'   => $model,
      'form_id' => $form_id,
      'button'  => Yii::t('app', 'Añadir')
    ]);
  ?>
</div>