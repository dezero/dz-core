<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for Currency model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: Currency model
|  - $column: Column name
|
*/
  use dz\helpers\Html;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "name"
    |--------------------------------------------------------------------------
    */
    case 'name':
  ?>
    <?= Html::link($model->title(), ['update', 'id' => $model->currency_id]); ?>
    <?php
      // Disabled language
      if ( $model->is_disabled() ) :
    ?>
      <span class="badge badge-danger ml-5 mr-5" data-toggle="tooltip" data-placement="right" data-original-title="<?= $model->disable_date; ?>"><?= Yii::t('app', 'desactivada'); ?></span>
    <?php endif; ?>
  <?php break; ?>
<?php endswitch; ?>