<?php
/*
|--------------------------------------------------------------------------
| Create page for TranslationImportExcel model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $upload_file_model: AssetFile model
|  - $batch_import_model: TranslationImportExcel model
|  - $default_language: Default language
|  - $vec_extra_languages: Array with EXTRA languages
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = Yii::t('app', 'Upload Excel file');
?>
<div class="page-header">
  <h1 class="page-title">
    <?= $this->pageTitle; ?>
  </h1>
  <div class="page-header-actions">
    <?=
      Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app', 'Back'), ['/settings/translationImport'], [
        'class' => 'btn btn-dark'
      ]);
    ?>
  </div>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => Yii::t('app', 'Manage translations'),
        'url' => ['/settings/translation'],
      ],
      [
        'label' => Yii::t('app', 'Import translations'),
        'url' => ['/settings/translationImport'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>
<div class="page-content container-fluid">
  <?php
    $this->renderPartial('//settings/translationImport/_form', [
      'upload_file_model'   => $upload_file_model,
      'batch_import_model'  => $batch_import_model,
      'default_language'    => $default_language,
      'vec_extra_languages' => $vec_extra_languages,
      'form_id'             => $form_id
    ]);
  ?>
</div>
