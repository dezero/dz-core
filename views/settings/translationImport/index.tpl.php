<?php
/*
|--------------------------------------------------------------------------
| Import page for TranslationImportExcel models
|--------------------------------------------------------------------------
|
| Available variables:
|  - $language_model: Language model class
|  - $batch_import_model: ImportExcel model
|  - $upload_file_model: AssetFile model to save imported Excel file
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = Yii::t('app', 'Import Translations');

  // Back URL
  $back_url = ['/settings/translation'];
?>
<div class="page-main">
  <div class="page-header">
    <h1 class="page-title"><?= $this->pageTitle; ?></h1>
    <div class="page-header-actions">
      <?=
        Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app', 'Back'), $back_url, [
          'class' => 'btn btn-dark'
        ]);
      ?>
    </div>
    <?=
      // Breadcrumbs
      Html::breadcrumbs([
        [
          'label' => Yii::t('app', 'Manage translations'),
          'url' => ['/settings/translation'],
        ],
        Yii::t('app', 'Import Translations')
      ]);
    ?>
  </div>
</div>
<div class="page-content import-page-content container-fluid pt-0">
  <div class="row row-lg panel-sidebar-wrapper">
    <div class="col-lg-12">
      <div class="panel panel-top-summary">
        <div class="panel-body container-fluid">
          <div class="page-content-actions batch-import-grid-actions">
            <div class="float-right">
              <?php
                // Import Excel actions
                // $this->renderPartial('//settings/translationImport/_form', [
                //   'file_model'  => $upload_file_model
                // ]);
              ?>
              <a class="btn btn-primary btn-md mt-7" href="<?= Url::to('/settings/translationImport/create', ['source' => 'excel']); ?>"><i class="icon wb-upload"></i> <?= Yii::t('app', 'UPLOAD EXCEL FILE'); ?></a>
              <a class="btn btn-dark ml-20 mt-7" href="<?= Url::to('/settings/translationExport'); ?>"><i class="wb-download mr-5"></i> <?= Yii::t('app', 'Export Translations'); ?></a>
              <a class="btn btn-dark btn-outline btn-md mt-7 ml-20 " href="<?= Url::to('/settings/translationImport/download'); ?>" data-toggle="tooltip" data-original-title="<?= Yii::t('app', 'Download Basic Excel Template'); ?>" target="_blank"><i class="icon wb-table"></i> <?= Yii::t('app', 'EXCEL TEMPLATE'); ?></a>
            </div>
            <div class="float-left">
              <h3><?= Yii::t('app', 'Imports'); ?></h3>
            </div>
          </div>
          <?php
            /*
            |----------------------------------------------------------------------------------------
            | GridView widget
            |----------------------------------------------------------------------------------------
            */
            $this->widget('dz.grid.GridView', [
              'id'                => 'batch-import-grid',
              'dataProvider'      => $batch_import_model->search(),
              // 'filter'            => $batch_import_model,
              'emptyText'         => Yii::t('app', 'No imports have been found'),
              'enableHistory'     => false,
              'loadModal'         => true,
              'enableSorting'     => false,
              'enableSelect2'     => false,
              'type'              => ['striped', 'hover'],
              'beforeAjaxUpdate'  => 'js:function(id, options){ $("html, body").animate({scrollTop: 0}, 100); }',
              'columns'           => [
                [
                  'header' => Yii::t('app', 'Import Date'),
                  'name' => 'created_date',
                ],
                [
                  'header' => Yii::t('app', 'File'),
                  'name' => 'file_id',
                  'value' => 'trim($this->grid->getOwner()->renderPartial("//settings/translationImport/_grid_column", ["column" => "file_id", "model" => $data]))',
                ],
                [
                  'header' => Yii::t('app', 'Language'),
                  'name' => 'event',
                  'value' => 'trim($this->grid->getOwner()->renderPartial("//settings/translationImport/_grid_column", ["column" => "language", "model" => $data]))',
                ],
                [
                  'header' => Yii::t('app', 'Results'),
                  'name' => 'summary_json',
                  'value' => 'trim($this->grid->getOwner()->renderPartial("//settings/translationImport/_grid_column", ["column" => "summary_json", "model" => $data]))',
                ],
                [
                  'class' => 'dz.grid.ButtonColumn',
                  'header' => Yii::t('app', 'ACTION'),
                  'template' => '{view}',
                  'deleteButton' => false,
                  'clearButton' => false,
                  'viewButton' => true,
                  'updateButton' => false,
                  'buttons' => [
                    'view' => [
                      'label' => Yii::t('app', 'View Results'),
                      'url' => 'Yii::app()->createAbsoluteUrl("/settings/translationImport/view", ["import_id" => $data->batch_id])',
                      'visible' => 'Yii::app()->user->checkAccess("settings.translation.view")'
                    ],
                  ],
                ],
              ],
            ]);
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
