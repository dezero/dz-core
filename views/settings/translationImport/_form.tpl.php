<?php
/*
|--------------------------------------------------------------------------
| Create form partial page for TranslationImportExcel model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $upload_file_model: AssetFile model
|  - $batch_import_model: TranslationImportExcel model
|  - $default_language: Default language
|  - $vec_extra_languages: Array with EXTRA languages
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;

  // Controller and action names in lowercase
  $current_action = Yii::currentAction(true);
?>
<div class="panel">
  <div class="panel-body">

    <?php
      // Create form object
      $upload_form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
        'id' => $form_id,
        'enableAjaxValidation' => false,
        'htmlOptions' => [
          'class' => 'form-horizontal',
          'enctype' => 'multipart/form-data',
          'autocomplete' => 'off'
        ]
      ]);

      // Error summary
      $errors = $upload_form->errorSummary([$upload_file_model, $batch_import_model]);
      if ( $errors )
      {
        echo $errors;
      }
    ?>

    <div class="row pb-30">
      <div class="col-lg-12">
        <h3 class="panel-title pt-0 pb-20"><?= Yii::t('app', '1. Select a language'); ?></h3>
        <div class="form-group pl-30">
          <div style="max-width: 350px">
            <?=
              Html::dropDownList(
                'ImportLanguageId',
                null,
                $vec_extra_languages,
                [
                  'class'             => 'form-control',
                  'id'                => 'import-language',
                  'autocomplete'      => 'off'
                  // 'placeholder'       =>  Yii::t('app', 'Select a language'),
                  // 'data-placeholder'  =>  Yii::t('app', 'Select a language'),
                  // 'empty'             =>  Yii::t('app', 'Select a language'),
                  // 'data-plugin'       => 'select2',
                  // 'multiple'          => false,
                  // 'data-allow-clear'  => false,
                  // 'style'             => 'max-width: 350px',
                ]
              );
            ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <h3 class="panel-title pt-0 pb-15"><?= Yii::t('backend', '2. Upload an Excel file'); ?></h3>
        <div class="inline-block pl-30"></div>
        <div class="inline-block">
          <?php
            echo $upload_form->hiddenField($upload_file_model, 'entity_type');

            $this->widget('\dz\widgets\Upload', [
              'model'     => $upload_file_model,
              'attribute' => 'file_name',
              'view_mode' => false,
              'is_image'  => false,
              'vec_labels' => [
                'new'     => Yii::t('backend', 'UPLOAD EXCEL FILE'),
                'change'  => Yii::t('backend', 'UPLOADING...'),
                'remove'  => Yii::t('backend', 'REMOVE'),
                'select'  => Yii::t('backend', 'SELECT FILE')
              ],
              'vec_classes' => [
                'new'     => 'btn-primary',
                'remove'  => 'btn-danger hide',
                'select'  => 'btn-primary',
              ],
              'events' => [
                // Change/select file
                'change.bs.fileinput' => 'js:function(){
                  $("#upload-excel-submit").click();
                }',
              ]
            ]);

            // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
            $this->widget('@bootstrap.widgets.TbButton', [
              'buttonType'  => 'submit',
              'type'        => 'primary',
              'label'       => 'Upload File',
              'htmlOptions' => [
                'id'    => 'upload-excel-submit',
                'class' => 'btn btn-primary hide'
              ]
            ]);
          ?>
          <p class="pb-5">Use this <a href="<?= Url::to('/settings/translationImport/download'); ?>" target="_blank"><?= Yii::t('app', 'Excel file template'); ?></a>.</p>
        </div>
      </div>
    </div>

    <?php
      // End form
      $this->endWidget();
    ?>
  </div>
</div>

<?php
  /*
  |--------------------------------------------------------------------------
  | ACTIONS
  |--------------------------------------------------------------------------
  */
?>
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Cancel
        echo Html::link(Yii::t('app', 'Cancel'), ['/settings/translationImport'], ['class' => 'btn btn-dark']);
      ?>
    </div><!-- form-actions -->
  </div>
