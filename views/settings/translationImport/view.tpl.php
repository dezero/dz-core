<?php
/*
|--------------------------------------------------------------------------
| View page for TranslationImportExcel model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $language_model: Language model class
|  - $batch_import_model: TranslationImportExcel model
|  - $file_model: AssetFile model
|  - $total_imported: Total number of imported users
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = $file_model->file_name;

  // Back URL
  $back_url = ['/settings/translationImport'];
  $translation_update_url =  '/settings/translationImport/update';
?>
<div class="page-main">
  <div class="page-header">
    <h1 class="page-title"><?= $this->pageTitle; ?></h1>
    <div class="page-header-actions">
      <?=
        Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app', 'Back'), $back_url, [
          'class' => 'btn btn-dark'
        ]);
      ?>
    </div>
    <?=
      // Breadcrumbs
      Html::breadcrumbs([
        [
          'label' => Yii::t('app', 'Manage translations'),
          'url' => ['/settings/translation'],
        ],
        [
          'label' => Yii::t('app', 'Import translations'),
          'url' => ['/settings/translationImport'],
        ],
        $this->pageTitle
      ]);
    ?>
  </div>
  <div class="page-content container-fluid">
    <div class="row row-lg panel-sidebar-wrapper">
      <div class="col-lg-12">
        <div class="panel panel-top-summary">
          <div class="panel-body container-fluid">
            <?php
              /*
              |--------------------------------------------------------------------------
              | IMPORT EXCEL INFORMATION
              |--------------------------------------------------------------------------
              */
            ?>
            <h4 class="mb-30"><?= Yii::t('app', 'Import Information') ;?></h4>
            <div class="panel-view-content panel-import-excel">
              <div class="row">
                <div class="col-sm-4">
                  <h5><?= Yii::t('app', 'Import File'); ?></h5>
                  <div class="item-content"><a href="<?= $batch_import_model->download_url(); ?>" target="_blank"><?= $file_model->title(); ?></a></div>
                </div>

                <div class="col-sm-2">
                  <h5><?= Yii::t('app', 'Import Date'); ?></h5>
                  <div class="item-content"><?= $batch_import_model->created_date; ?></div>
                </div>

                <div class="col-sm-2">
                  <h5><?= Yii::t('app', 'Language'); ?></h5>
                  <div class="item-content">
                    <?php
                      $language_model = $batch_import_model->get_language();
                      if ( $language_model ) :
                    ?>
                      <?= $language_model->title(); ?>
                    <?php endif; ?>
                  </div>
                </div>

                <div class="col-sm-1 center">
                  <h5><?= Yii::t('app', 'Detected'); ?></h5>
                  <div class="item-content font-size-30 font-weight-100"><?= $batch_import_model->total_items; ?></div>
                </div>

                <div class="col-sm-1 center">
                  <h5><?= Yii::t('app', 'Imported'); ?></h5>
                  <div class="item-content font-size-30 font-weight-100<?php if ( $total_imported > 0 ) : ?> text-success<?php endif; ?>"><?= $total_imported; ?></div>
                </div>

                <div class="col-sm-1 center">
                  <h5><?= Yii::t('app', 'Errors'); ?></h5>
                  <div class="item-content font-size-30 font-weight-100<?php if ( $batch_import_model->num_errors > 0 ) : ?> text-danger<?php endif; ?>"><?= $batch_import_model->num_errors; ?></div>
                </div>
              </div>

              <hr class="mb-30 mt-20">

              <h4 class="mt-30 mb-30"><?= Yii::t('app', 'Import Results') ;?></h4>

              <div class="row">
                <div class="col-sm-12">
                  <?php if ( empty($batch_import_model->results_json) ) : ?>
                    <h5><?= Yii::t('app', 'No imported translations have been found'); ?></h5>
                  <?php else : ?>
                    <table id="import-excel-results" class="table table-striped table-hover import-excel-table">
                      <thead>
                        <tr>
                          <th><?= Yii::t('app', '# Row'); ?></th>
                          <th><?= Yii::t('app', 'Source message'); ?></th>
                          <th><?= Yii::t('app', 'Translation Message'); ?></th>
                          <th><?= Yii::t('app', 'Category'); ?></th>
                          <th><?= Yii::t('app', 'Results'); ?></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ( $batch_import_model->results_json as $num_row => $que_row ) : ?>
                          <tr>
                            <td class="row_column"><?= $num_row; ?></td>
                            <td>
                              <?php
                                // --------------------------------------------------------------------------
                                // SOURCE MESSAGE
                                // --------------------------------------------------------------------------
                                if ( isset($que_row['data']) && isset($que_row['data']['A']) ) :
                              ?>
                                <?= $que_row['data']['A']; ?>
                              <?php else : ?>
                                <em>(none)</em>
                              <?php endif; ?>
                            </td>
                            <td>
                              <?php
                                // --------------------------------------------------------------------------
                                // TRANSLATION MESSAGE
                                // --------------------------------------------------------------------------
                                if ( isset($que_row['data']) && isset($que_row['data']['B']) ) :
                              ?>
                                <?= $que_row['data']['B']; ?>
                              <?php else : ?>
                                <em>(none)</em>
                              <?php endif; ?>
                            </td>
                            <td>
                              <?php
                                // --------------------------------------------------------------------------
                                // CATEGORY
                                // --------------------------------------------------------------------------
                                if ( isset($que_row['data']) && isset($que_row['data']['C']) ) :
                              ?>
                                <?= $que_row['data']['C']; ?>
                              <?php else : ?>
                                <?= 'app'; ?>
                              <?php endif; ?>
                            </td>
                            <td class="results_column">
                              <?php
                                // SUCCESS
                                if ( !isset($que_row['errors']) || empty($que_row['errors']) ) :
                              ?>
                                <span class="text-success"><?= Yii::t('app', 'IMPORTED'); ?></span>
                                <ul>
                                  <?php if ( isset($que_row['certificate']) && !empty($que_row['certificate']) && isset($que_row['certificate']['id']) ) : ?>
                                    <?php if ( isset($que_row['certificate']['new']) && $que_row['certificate']['new'] === 1 ) : ?>
                                      <li><a href="<?= Url::to($translation_update_url, ['id' => $que_row['id']]); ?>" target="_blank"><?= Yii::t('app', 'Created new translation'); ?></a></li>
                                    <?php else : ?>
                                      <li><a href="<?= Url::to($translation_update_url, ['id' => $que_row['id']]); ?>" target="_blank"><?= Yii::t('app', 'Updated existing translation'); ?></a></li>
                                    <?php endif; ?>
                                  <?php endif; ?>
                                </ul>
                              <?php
                                // ERROR
                                else :
                              ?>
                                <span class="text-danger"><?= Yii::t('app', 'ERROR|ERRORS', count($que_row['errors'])); ?></span><br>
                                <?php
                                  $vec_errors = $batch_import_model->output_errors($que_row['errors']);
                                  if ( !empty($vec_errors) )
                                  {
                                    echo Html::ul($vec_errors);
                                  }
                                ?>
                              <?php endif; ?>
                            </td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                  <?php endif; ?>

                </div>
              </div>
            </div>
          </div>
        </div>

          <div class="form-group row">
            <div class="col-lg-12 form-actions buttons">
              <?=
                Html::link('Go Back', ['/settings/translationImport'], [
                  'class' => 'btn btn-dark'
                ]);
              ?>
            </div>
          </div>
      </div>
    </div>
  </div><!-- .page-content -->
</div><!-- .page-main -->
