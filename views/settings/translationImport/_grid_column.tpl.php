<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for TranslationImportExcel model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: TranslationImportExcel model
|  - $column: Column name
|
*/

  use dz\helpers\Html;
  use dz\helpers\Url;

?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "file_id"
    |--------------------------------------------------------------------------
    */
    case 'file_id':
  ?>
    <?php if ( $model->file ) : ?>
      <a href="<?= $model->download_url(); ?>" target="_blank"><?= $model->file->title(); ?></a>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "language"
    |--------------------------------------------------------------------------
    */
    case 'language':
  ?>
    <?php
      $language_model = $model->get_language();
      if ( $language_model ) :
    ?>
      <?= $language_model->title(); ?>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "summary_json"
    |--------------------------------------------------------------------------
    */
    case 'summary_json':
  ?>
    <?php
      $view_url = Url::to('/settings/translationImport/view', ['import_id' => $model->batch_id]);
      if ( !empty($model->summary_json) ) :
    ?>
      <?php if ( $model->total_items > 0 ) : ?><a href="<?= $view_url; ?>"><?= $model->total_items .' '. Yii::t('app', 'translation detected|translations detected', $model->total_items); ?></a><?php endif; ?>
      <ul>
        <?php if ( isset($model->summary_json['inserted']) && $model->summary_json['inserted'] > 0 ) : ?><li><a href="<?= $view_url; ?>"><span class="text-success"><?= $model->summary_json['inserted'] .' '. Yii::t('app', 'translation inserted|translations inserted', $model->summary_json['inserted']); ?></span></a></li><?php endif; ?>
        <?php if ( isset($model->summary_json['updated']) && $model->summary_json['updated'] > 0 ) : ?><li><a href="<?= $view_url; ?>"><span class="text-success"><?= $model->summary_json['updated'] .' '. Yii::t('app', 'translation updated|translations updated', $model->summary_json['updated']); ?></span></a></li><?php endif; ?>
        <?php if ( isset($model->summary_json['errors']) && $model->summary_json['errors'] > 0 ) : ?><li><a href="<?= $view_url; ?>"><span class="text-danger"><?= $model->summary_json['errors'] .' '. Yii::t('app', 'error|errors', $model->summary_json['errors']); ?></span></a></li><?php endif; ?>
      </ul>
    <?php endif; ?>
  <?php break; ?>
<?php endswitch; ?>
