<?php
/*
|--------------------------------------------------------------------------
| Footer zone with HELP content: tokens replacement and guide text format
|--------------------------------------------------------------------------
|
| Available variables:
|  - $language_id: Language
|
*/

  use dz\db\ActiveRecord;
  use dz\helpers\StringHelper;

?>
<a name="tokens"></a>
<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title"><?= Yii::t('app', 'Replacement Tokens'); ?></h3>
  </header>
  <div class="panel-body">
    <div class="form-group token-group row">
      <label class="col-lg-2 col-sm-2 form-control-label">&nbsp;</label>
      <div class="col-lg-10">
        <div class="tokens-block">
          <h5><?= Yii::t('app', 'DEFAULT TOKENS'); ?></h5>
          <ul>
            <?php
              $vec_tokens_help = Yii::app()->mail->tokensHelp();
              foreach ( $vec_tokens_help as $token_name => $token_description ) :
            ?>
              <li><strong><?= $token_name; ?></strong>: <?= $token_description; ?></li>
            <?php endforeach; ?>
          </ul>
        </div>
        <?php 
          if ( !empty(Yii::app()->mail->available_model_tokens) ) :
        ?>
          <?php foreach ( Yii::app()->mail->available_model_tokens as $model_class ) : ?>
            <?php
              $tokens_title = '';
              if ( is_array($model_class) AND isset($model_class['class']) )
              {
                if ( isset($model_class['name']) )
                {
                  $tokens_title = $model_class['name'];
                }
                $model_class = $model_class['class'];
              }

              // Get ActiveRecord class
              $que_model = ActiveRecord::model($model_class);
              if ( $que_model && is_object($que_model) && method_exists($que_model, 'mailHelp') ) :
            ?>
              <?php
                // Get tile
                if ( empty($tokens_title) )
                {
                  $tokens_title = StringHelper::strtoupper(StringHelper::basename(get_class($que_model)));
                }

                // Get help for tokens defined on "mailHelp" method
                $vec_tokens_help = $que_model->mailHelp();
                if ( !empty($vec_tokens_help) ) :
              ?>
                <div class="tokens-block">
                  <h5><?= $tokens_title; ?> TOKENS</h5>
                  <ul>
                    <?php foreach ( $vec_tokens_help as $token_name => $token_description ) : ?>
                      <li><strong><?= $token_name; ?></strong>: <?= $token_description; ?></li>
                    <?php endforeach; ?>
                  </ul>
                </div>
              <?php endif; ?>
            <?php endif; ?>
          <?php endforeach; ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>