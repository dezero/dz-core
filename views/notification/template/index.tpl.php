<?php
/*
|--------------------------------------------------------------------------
| Admin list page for NotificationTemplate model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: NotificationTemplate model class
|
*/  
  use dz\helpers\Html;

  // Page title
  $this->pageTitle = Yii::t('app', 'Manage Notification Templates');
?>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <div class="page-header-actions">
    <?=
      Html::link('<i class="icon wb-plus"></i> Add new notification template', ["create"], [
        'class' => 'btn btn-primary'
      ]);
    ?>
  </div>
</div>
<div class="page-content">
  <div class="row row-lg">
    <div class="col-lg-12">
      <div class="panel panel-top-summary">
        <div class="panel-body container-fluid">
          <?php
            /*
            |----------------------------------------------------------------------------------------
            | GridView widget
            |----------------------------------------------------------------------------------------
            */
              $this->widget('dz.grid.GridView', [
                'id'            => 'notification-template-grid',
                'dataProvider'  => $model->search(),
                'filter'        => $model,
                'enableHistory' => true,
                'enableSorting' => false,
                'loadModal'     => true,
                'type'          => ['striped', 'hover'],
                'beforeAjaxUpdate' => 'js:function(id, options){ $("html, body").animate({scrollTop: 0}, 100); }',
                'columns'       => [
                  'alias',
                  'name',
                  // 'description',
                  [
                    'class' => 'dz.grid.ButtonColumn',
                    'header' => Yii::t('app', 'ACTION'),
                    'template' => '{update}{delete}',
                    'deleteButton' => true,
                    'clearButton' => true,
                    'viewButton' => false,
                    'updateButton' => true,
                  ],
                ],
              ]);
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>