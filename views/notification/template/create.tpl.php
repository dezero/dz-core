<?php
/*
|--------------------------------------------------------------------------
| Create form page for NotificationTemplate model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: NotificationTemplate model
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;

  // Page title
  $this->pageTitle = Yii::t('app', 'Add New Notification Template');
?>

<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => Yii::t('app', 'Notification Templates'),
        'url' => ['/notification/template/'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <?php
    // Render form
    $this->renderPartial('//notification/template/_form', [
      'model'   => $model,
      'form_id' => $form_id,
      'button'  => Yii::t('app', 'Save')
    ]);
  ?>
</div>