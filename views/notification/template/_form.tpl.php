<?php
/*
|--------------------------------------------------------------------------
| Form partial page for NotificationTemplate model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: NotificationTemplate model
|  - $vec_translated_models: Array with translated NotificationTemplate models
|  - $default_language: Default language code
|  - $vec_extra_languages: Array with Extra languages
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  use dz\helpers\Url;

  // Current action name
  $current_action = Yii::currentAction(true);

  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal notification-form-wrapper',
      'autocomplete' => 'off'
    ]
  ]);

  // Error summary
  if ( $current_action !== 'create' && isset($vec_translated_models) && !empty($vec_translated_models) )
  {
    $errors = $form->errorSummary(\CMap::mergeArray([$model], $vec_translated_models));  
  }
  else
  {
    $errors = $form->errorSummary($model);
  }
  if ( $errors )
  {
    echo $errors;
  }
?>
<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title">Template</h3>
  </header>
  <div class="panel-body">
    <div class="form-group row<?php if ( $model->hasErrors('alias') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'alias', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?php if ( $current_action == 'create' ) : ?>
          <?=
            $form->textField($model, 'alias', [
              'maxlength' => 64,
              'placeholder' => '',
            ]);
          ?>
        <?php else: ?>
          <div class="form-control view-field">
            <strong><?= $model->alias; ?></strong>
          </div>
        <?php endif; ?>
        <?php /*<p class="help-block">Nombre INTERNO identificativo en inglés. No introducir espacios ni números. Por ejemplo: <em>welcome</em></p>*/ ?>
        <p class="help-block">Identification name written in English for internal use. Only lower case letters are allowed. For example: <em>welcome</em></p>
        <?= $form->error($model,'alias'); ?>
      </div>
    </div>

    <div class="form-group row<?php if ( $model->hasErrors('name') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'name', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?=
          $form->textField($model, 'name', [
            'maxlength' => 128,
            'placeholder' => ''
          ]);
        ?>
        <p class="help-block">Internal name. For example: <em>DOCUMENTATION UPDATED</em></p>
        <?= $form->error($model,'name'); ?>
      </div>
    </div>

    <div class="form-group row<?php if ( $model->hasErrors('description') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'description', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?=
          $form->textArea($model, 'description', [
            'rows' => 2
          ]);
        ?>
        <?php /*<p class="help-block">Descripción INTERNA de la plantilla. Por ejemplo: <em>Notification que se envía cuando el usuario se registra por primera vez</em></p> */?>
        <p class="help-block">Template description for internal use. For example: <em>This notification is sent when a document has been updated.</em></p>
        <?= $form->error($model,'description'); ?>
      </div>
    </div>

    <?php if ( Yii::app()->user->id == 1 ) : ?>
      <div class="form-group row<?php if ( $model->hasErrors('notification_type') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($model, 'notification_type', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
        <div class="col-lg-10">
          <?=
            $form->textField($model, 'notification_type', [
              'placeholder' => '',
              'maxlength' => 32
            ]);
          ?>
          <?= $form->error($model,'notification_type'); ?>
          <p class="help-block">Optional. Notification type or category</p>
        </div>
      </div>
    <?php endif; ?>
  </div>
</div>

<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title">Notification Content</h3>
  </header>
  <div class="panel-body">
    <?php
      /**
       * EXTRA LANGUAGES in TABS
       */
      if ( $current_action !== 'create' && Yii::app()->i18n->is_multilanguage() ) :
    ?>
      <div class="nav-tabs-horizontal pt-10" data-plugin="tabs">
        <ul class="nav nav-tabs nav-tabs-line tabs-line-top" role="tablist">
          <li class="nav-item" role="presentation">
            <a class="nav-link active" data-toggle="tab" href="#lang-<?= $default_language; ?>" aria-controls="lang-<?= $default_language; ?>" role="tab" aria-expanded="false" data-language="<?= $default_language; ?>"><span class="flag-icon flag-icon-<?= $default_language; ?>"></span> <?= StringHelper::strtoupper(Yii::app()->i18n->language_name($default_language)); ?></a>
          </li>
          <?php if ( isset($vec_extra_languages) && !empty($vec_extra_languages) ) : ?>
            <?php foreach ( $vec_extra_languages as $language_id ) : ?>
              <?php if ( isset($vec_translated_models[$language_id]) ) : ?>
                <li class="nav-item" role="presentation">
                  <a class="nav-link" data-toggle="tab" href="#lang-<?= $language_id; ?>" aria-controls="lang-<?= $language_id; ?>" role="tab" aria-expanded="false" data-language="<?= $language_id; ?>"><span class="flag-icon flag-icon-<?= $language_id; ?>"></span> <?= StringHelper::strtoupper(Yii::app()->i18n->language_name($language_id)); ?></a>
                </li>
              <?php endif; ?>
            <?php endforeach; ?>
          <?php endif; ?>
        </ul>
      </div>
      <div class="tab-content pt-20">
        <div class="tab-pane active" id="lang-<?= $default_language; ?>" role="tabpanel" aria-expanded="false">
    <?php endif; ?>
    
    <div class="form-group row<?php if ( $model->hasErrors('body') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($model, 'body', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?php
          $this->widget('@ext.DzMarkdown.DzMarkdown', [
            'model'     => $model,
            'attribute' => 'body',
            'options'   => ['minHeight' => 250]
          ]);

          /*
          $form->textArea($model, 'body', [
            'rows' => 12
          ]);
          */
          /*
          $this->widget('@lib.redactor.ERedactorWidget', [
            'model'   => $model,
            'attribute' => 'body',
            'options' => [
              'minHeight' => 200,
              'buttonSource' => TRUE,
              // 'buttons' => ['|', 'html'],
            ],
          ]);
          */
        ?>
        <p class="help-block">
          See the <a href="<?= Url::to('/help/markdown'); ?>" target="_blank">format guide</a>.<br>
          Replacement tokens are allowed. See the <a href="#tokens">replacement tokens</a> below.
        </p>
        <?= $form->error($model,'body'); ?>
      </div>
    </div>

    <?php
      /*
      |--------------------------------------------------------------------------
      | EXTRA LANGUAGES in TABS
      |--------------------------------------------------------------------------
      */
      if ( $current_action !== 'create' && Yii::app()->i18n->is_multilanguage() ) :
    ?>
      </div><!-- .tab-pane -->
      <?php if ( isset($vec_extra_languages) && !empty($vec_extra_languages) ) : ?>
        <?php foreach ( $vec_extra_languages as $language_id ) : ?>
          <?php if ( isset($vec_translated_models[$language_id]) ) : ?>
            <?php
              $translated_notification_model = $vec_translated_models[$language_id];
            ?>
            <div class="tab-pane" id="lang-<?= $language_id; ?>" role="tabpanel" aria-expanded="false">

              <div class="form-group row<?php if ( $translated_notification_model->hasErrors('body') ) : ?> has-danger<?php endif; ?>">
                <?= $form->label($translated_notification_model, 'body', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
                <div class="col-lg-10">
                  <?php
                    $this->widget('@ext.DzMarkdown.DzMarkdown', [
                      'model'     => $translated_notification_model,
                      'attribute' => 'body',
                      'options'   => [
                        'minHeight' => 250
                      ],
                      'htmlOptions' => [
                        'name'        => 'TranslatedNotification['. $language_id .'][body]',
                      ]
                    ]);

                    /*
                    $form->textArea($translated_notification_model, 'body', [
                      'name'  => 'TranslatedNotification['. $language_id .'][body]',
                      'rows'  => 12
                    ]);
                    */
                    /*
                    $this->widget('@lib.redactor.ERedactorWidget', [
                      'model'   => $translated_notification_model,
                      'attribute' => 'body',
                      'options' => [
                        'minHeight' => 200,
                        'buttonSource' => TRUE,
                        // 'buttons' => ['|', 'html'],
                      ],
                    ]);
                    */
                  ?>
                  <p class="help-block">
                    See the <a href="<?= Url::to('/help/markdown'); ?>" target="_blank">format guide</a>.<br>
                    Replacement tokens are allowed. See the <a href="#tokens">replacement tokens</a> below.
                  </p>
                  <?= $form->error($translated_notification_model,'body'); ?>
                </div>
              </div>

            </div><!-- .tab-pane -->
          <?php endif; ?>
        <?php endforeach; ?>
      <?php endif; ?>
      </div><!-- .tab-content -->
    <?php endif; ?>
  </div>
</div>

<?php
  /**
   * REPLACEMENT TOKENS HELP
   */
?>
<?php
  $this->renderPartial('//notification/template/_tokens_help', [
    'language_id' => Yii::app()->language,
    'model'       => $model
  ]);
?>

<?php
  /*
  |--------------------------------------------------------------------------
  | ACCIONES
  |--------------------------------------------------------------------------
  */
?>  
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
        $this->widget('@bootstrap.widgets.TbButton', [
          'buttonType'=> 'submit',
          'type'      => 'primary',
          'label'     => $button,
        ]);
        
        // Cancel
        echo Html::link(Yii::t('app', 'Cancel'), ['/notification/template'], ['class' => 'btn btn-dark']);
      ?>
    </div><!-- form-actions -->
  </div>

<?php
  // End model form
  $this->endWidget();
?>