<?php
/*
|--------------------------------------------------------------------------
| Change password page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $user_model: User model class
|
*/

use dz\helpers\StringHelper;
use dz\helpers\Url;

$this->pageTitle = Yii::t('app', 'Change password');
?>
<div class="panel">
  <div class="panel-body">
    <div class="brand">
      <h2 class="brand-text font-size-24 mt-0"><?= StringHelper::uppercase(Yii::t('app', 'Change password')); ?></h2>
    </div>

    <p class="mt-20"><?= Yii::t('app', 'You must change the password to continue'); ?></p>

    <div class="login-wrapper">
      <?php
        // Start model form (http://bootstrap3.pascal-brewing.de/site/css)
        $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm',array(
          'id' => 'change-password-form',
          'enableAjaxValidation' => FALSE,
          'htmlOptions' => array(
            // 'class' => 'form-signin',
            'autocomplete' => 'off'
          )
        ));
      ?>
        <?= $form->errorSummary($user_model); ?>

        <div class="form-group<?php if ( $user_model->hasErrors('password') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($user_model, 'password', ['class' => 'sr-only']); ?>
          <?=
            $form->passwordField($user_model, 'password', [
              'autofocus' => 'autofocus',
              'class' => 'form-control form-control-lg'
            ]);
          ?>
        </div>

        <div class="form-group<?php if ( $user_model->hasErrors('verify_password') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($user_model, 'verify_password', ['class' => 'sr-only']); ?>
          <?=
            $form->passwordField($user_model, 'verify_password', [
              'class' => 'form-control form-control-lg'
            ]);
          ?>
        </div>
        
        <?php
          $this->widget('@bootstrap.widgets.TbButton', [
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => Yii::t('app', 'Save'),
            'htmlOptions' => ['class' => 'btn btn-block btn-lg mt-40'],
          ]);
        ?>
      <?php
        // End model form
        $this->endWidget();
      ?>
    </div>
  </div>
</div>