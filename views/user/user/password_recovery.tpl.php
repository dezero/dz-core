<?php
/*
|--------------------------------------------------------------------------
| Change password page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $user_model: User model class
|
*/
use dz\helpers\Url;

$this->pageTitle = Yii::t('app', 'Forgot password');
?>
<div class="panel">
  <div class="panel-body">
    <div class="brand">
      <h2 class="brand-text font-size-24 mt-0"><?= Yii::t('app', 'FORGOT PASSWORD'); ?></h2>
    </div>

    <p class="mt-20"><?= Yii::t('app', 'Please enter your email address and we will send you the instructions for the password reset.'); ?></p>

    <div class="login-wrapper">
      <?php
        // Start model form (http://bootstrap3.pascal-brewing.de/site/css)
        $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm',array(
          'id' => 'password-recovery-form',
          'enableAjaxValidation' => FALSE,
          'htmlOptions' => array(
            // 'class' => 'form-signin',
            'autocomplete' => 'off'
          )
        ));
      ?>
        <?= $form->errorSummary($user_model); ?>

        <div class="form-group<?php if ( $user_model->hasErrors('email') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($user_model, 'email', ['class' => 'sr-only']); ?>
          <?=
            $form->textField($user_model, 'email', [
              'autofocus' => 'autofocus',
              'class' => 'form-control form-control-lg'
            ]);
          ?>
        </div>
        
        <?php
          $this->widget('@bootstrap.widgets.TbButton', [
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => Yii::t('app', 'Reset Password'),
            'htmlOptions' => ['class' => 'btn btn-block btn-lg mt-40'],
          ]);
        ?>
      <?php
        // End model form
        $this->endWidget();
      ?>
    </div>
    <p><a href="<?= Url::to('/user/login'); ?>"><?= Yii::t('app', 'Back to login page'); ?></a></p>
  </div>
</div>