<?php
/*
|--------------------------------------------------------------------------
| Login page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: User model class
|
*/

use dz\helpers\Url;

$this->pageTitle = Yii::t('app', 'Login');
?>
<?php if ( !empty($loginas_token) ) : ?>
  <div class="loginas-message alert dark alert-success alert-dismissible">
    <h4>"INICIAR SESIÓN COMO..." ACTIVADO PARA ADMINISTRADOR</h4>
    <p>Introduce un nombre de usuario y tu contraseña para iniciar sesión con otro usuario.</p>
  </div>
<?php endif; ?>
<?php
  // Login message
  if ( Yii::app()->user->hasFlash('loginMessage') ) :
?>
  <div class="login-message alert dark alert-success alert-dismissible">
    <?= Yii::app()->user->getFlash('loginMessage'); ?>
  </div>
<?php endif; ?>
<div class="panel">
  <div class="panel-body">

    <div class="brand">
      <?php /*<img class="brand-img" src="<?= Url::theme(); ?>/images/logo.png" alt="<?= Yii::app()->name; ?>">*/ ?>
      <h1><?= Yii::app()->name; ?></h1>
      <h2 class="brand-text font-size-18"><?php // echo Yii::app()->name; ?>ADMINISTRATION PANEL</h2>
    </div>

    <div class="login-wrapper">
      <?php
        // Start model form (http://bootstrap3.pascal-brewing.de/site/css)
        $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm',array(
          'id' => 'login-form',
          'enableAjaxValidation' => FALSE,
          'htmlOptions' => array(
            // 'class' => 'form-signin',
            'autocomplete' => 'off'
          )
        ));
      ?>
        <?= $form->errorSummary($model); ?>
        <?php
          // ADMIN TOKEN (LOGIN AS...)
          if ( !empty($loginas_token) ) :
        ?>
          <?php
            $model->loginas_token = $loginas_token;
            echo $form->hiddenField($model, 'loginas_token');
          ?>
        <?php endif; ?>

        <div class="form-group<?php if ( $model->hasErrors('username') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($model, 'username', ['class' => 'sr-only']); ?>
          <?=
            $form->textField($model, 'username', [
              'placeholder' => Yii::t('user', 'username'),
              'autofocus' => 'autofocus',
              'class' => 'form-control form-control-lg'
            ]);
          ?>
          <?php //<?= $form->error($model, 'username'); ?>
        </div>

        <div class="form-group<?php if ( $model->hasErrors('password') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($model, 'password', ['class' => 'sr-only']); ?>
          <?=
            $form->passwordField($model, 'password', [
              'placeholder' => Yii::t('user', 'password'),
              'class' => 'form-control form-control-lg'
            ]);
          ?>
          <p class="help-block"><a href="<?= Url::to('/user/password'); ?>"><?= Yii::t('app', 'Forgot password?'); ?></a></p>
          <?php //<?= $form->error($model, 'password'); ?>
        </div>
        
        <?php /*
        <label class="checkbox">
          <?= $form->checkBox($model, 'is_remember'); ?> <i><?php echo Yii::t('app', 'Remember'); ?></i>
        </label>
        */ ?>

        <?php
          $this->widget('@bootstrap.widgets.TbButton', [
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => Yii::t('app', 'Login'),
            'htmlOptions' => ['class' => 'btn btn-block btn-lg mt-40'],
          ]);
        ?>
      <?php
        // End model form
        $this->endWidget();
      ?>
    </div>

    <p><a href="<?= Url::to('/'); ?>"><?= Yii::t('app', 'Volver a la web pública'); ?></a></p>
  </div>
</div>
