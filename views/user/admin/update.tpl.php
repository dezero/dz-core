<?php
/*
|--------------------------------------------------------------------------
| Update form page for User model
|--------------------------------------------------------------------------
|
| Available variables:
|   - $model: User model
|   - $roles: Roles (auth)
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
    
  // Page title
  $this->pageTitle = $model->title();
?>
<div class="page-header">
  <h1 class="page-title">
    <?= $this->pageTitle; ?>
    <?php
      // Superadmin?
      if ( $model->is_superadmin == 1 ) :
    ?>
      <span class="badge badge-primary ml-5 mr-5" data-toggle="tooltip" data-placement="top" data-original-title="Admin - Full Access">ADMIN</span>
    <?php endif; ?>
    <?php
      // Disabled user?
      if ( $model->is_disabled() ) :
    ?>
      <span class="badge badge-danger ml-5 mr-5" data-toggle="tooltip" data-placement="top" data-original-title="<?php if ( !empty($model->disable_date) ) : ?>From <?= $model->disable_date; ?><?php else : ?>Inactivo<?php endif; ?>">DISABLE</span>
    <?php
      // Banned user?
      elseif ( $model->is_banned() ) :
    ?>
      <span class="badge badge-danger ml-5 mr-5" data-toggle="tooltip" data-placement="top" data-original-title="Baneado">BANNED</span>
    <?php endif; ?>
    <?php
      // User must change password?
      if ( $model->is_force_change_password == 1 ) :
    ?>
      <span class="badge badge-warning ml-5 mr-5" data-toggle="tooltip" data-placement="top" data-original-title="User must change password at next logon">FORCE PASSWORD CHANGE</span>
    <?php endif; ?>
  </h1>
  <div class="page-header-actions">
    <?=
      Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app', 'Back'), ["/user/admin"], [
        'class' => 'btn btn-dark'
      ]);
    ?>
    <a href="<?= Url::to('/admin/mail/send', ['user_id' => $model->id]); ?>"" class="btn btn-primary""><i class="wb-envelope"></i><?= Yii::t('app', 'Send Email'); ?></a>
  </div>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => Yii::t('app', 'Users'),
        'url' => ['/user/admin/']
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <?php
    /*
    |--------------------------------------------------------------------------
    | UPDATE FORM PAGE
    |--------------------------------------------------------------------------
    */
    $this->renderPartial('_form', [
      'user_model'  => $model,
      'roles'       => $roles,
      'tasks'       => $tasks,
      'form_id'     => 'user-form'
    ]);
  ?>
</div>
