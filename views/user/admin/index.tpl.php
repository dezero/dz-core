<?php
/*
|--------------------------------------------------------------------------
| Admin list page for User model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: User model class
|  - $vec_list_roles: Array with full role list
|
*/  
  use dz\helpers\Html;
  
  // Page title
  $this->pageTitle = Yii::t('app', 'Manage Users');
?>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <div class="page-header-actions">
    <?=
      Html::link('<i class="icon wb-plus"></i> '. Yii::t('app', 'Add new user'), ["create"], [
        'class' => 'btn btn-primary'
      ]);
    ?>
  </div>
</div>
<div class="page-content">
  <div class="panel">
    <div class="panel-body container-fluid">
      <div class="row row-lg">
        <div class="col-lg-12">
          <?php
            /*
            |----------------------------------------------------------------------------------------
            | GridView widget
            |----------------------------------------------------------------------------------------
            */
              $this->widget('dz.grid.GridView', [
                'id'            => 'account-grid',
                'dataProvider'  => $model->search(),
                'filter'        => $model,
                'emptyText'     => Yii::t('app', 'No users found'),
                'enableHistory' => true,
                'loadModal'     => true,
                'type'          => ['striped', 'hover'],
                'beforeAjaxUpdate' => 'js:function(id, options){ $("html, body").animate({scrollTop: 0}, 100); }',
                'columns'       => [
                  [
                    'name' => 'id',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("_grid_column", ["column" => "id", "model" => $data]))',
                  ],
                  [
                    'name' => 'firstname',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("_grid_column", ["column" => "firstname", "model" => $data]))',
                    'header' => Yii::t('app', 'User')
                  ],
                  [
                    'name' => 'email',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("_grid_column", ["column" => "email", "model" => $data]))',
                  ],
                  [
                    'name' => 'role_filter',
                    'header' => 'Roles',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("_grid_column", ["column" => "roles", "model" => $data]))',
                    'filter' => $vec_list_roles
                  ],
                  [
                    'name' => 'last_login_date',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("_grid_column", ["column" => "last_login_date", "model" => $data]))',
                    'filter' => false,
                  ],
                  [
                    'name' => 'last_change_password_date',
                    'value' => 'trim($this->grid->getOwner()->renderPartial("_grid_column", ["column" => "last_change_password_date", "model" => $data]))',
                    'filter' => false,
                  ],
                  [
                    'class' => 'dz.grid.ButtonColumn',
                    'clearButton' => true,
                    'viewButton' => false,
                    'deleteButton' => true,
                    'header' => Yii::t('app', 'ACTION'),
                    // 'menuAction' => '$this->grid->getOwner()->menuAction($data)',
                    'menuAction' => function($data, $row) {
                      return [
                        [
                          'label' => Yii::t('app', 'Permissions'),
                          'icon' => 'fa-check-square-o',
                          'url' => ['/user/admin/permission', 'id' => $data->id],
                        ],
                        '---',
                        [
                          'label' => Yii::t('app', 'Enable'),
                          'icon' => 'arrow-up',
                          'url' => ['/user/admin/enable', 'id' => $data->id],
                          'visible' => '!$data->is_enabled()',
                          'confirm' => '<h3>Are you sure you want to <span class=\'text-success\'>ENABLE</span> this user?</h3><p>This user will be available to access again to the platform.</p>',
                          'htmlOptions' => [
                            'id' => 'enable-user-'. $data->id,
                            'data-gridview' => 'account-grid'
                          ]
                        ],
                        [
                          'label' => Yii::t('app', 'Disable'),
                          'icon' => 'arrow-down',
                          'url' => ['/user/admin/disable', 'id' => $data->id],
                          'visible' => '$data->is_enabled()',
                          'confirm' => '<h3>Are you sure you want to <span class=\'text-danger\'>DISABLE</span> this user?</h3><p><u class=\'text-danger\'>WARNING:</u> This user will not be able to access to the platform.</p>',
                          'htmlOptions' => [
                            'id' => 'disable-user-'. $data->id,
                            'data-gridview' => 'account-grid'
                          ]
                        ]
                      ];
                    },
                  ],
                ],
              ]);
            ?>
        </div>
      </div>
    </div>
  </div>
</div>
