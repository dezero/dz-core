<?php
/*
|--------------------------------------------------------------------------
| Create form page for User model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: User model
|  - $roles: Roles (auth)
|
*/
  use dz\helpers\Html;

  // Page title
  $this->pageTitle = Yii::t('app', 'Create user');
?>

<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => Yii::t('app', 'Users'),
        'url'   => ['/user/admin/'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <?php
    // Render form
    $this->renderPartial('_form', [
      'user_model'  => $model,
      'roles'       => $roles,
      'form_id'     => 'user-form',
      'buttons'     => Yii::t('app', 'create')
    ]);
  ?>
</div>