<?php
/*
|--------------------------------------------------------------------------
| Permission page for User model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: User model class
|  - $roles: Roles (auth)
|  - $tasks: Tasks (auth)
|
*/
  use dz\helpers\Html;
  use user\models\User;
  
  // Page title
  $this->pageTitle = $model->title() .' | Permisos';
?>
<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => Yii::t('app', 'Users'),
        'url' => ['/user/admin/']
      ],
      $this->pageTitle
    ]);
  ?>
</div>
<?php
  // Form
  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => 'user-permission-auth-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal'
    ]
  ]);

  // Error summary
  $errors = $form->errorSummary($model);
  if ( $errors )
  {
    echo $errors;
  }

  /*
  |--------------------------------------------------------------------------
  | USER INFORMATION
  |--------------------------------------------------------------------------
  */
?>
<div class="page-content container-fluid">
  <div class="panel">
    <header class="panel-heading">
      <h3 class="panel-title">Usuario</h3>
    </header>
    <div class="panel-body">
      <div class="form-group row">
        <?= $form->label($model, 'id', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
        <div class="col-lg-8">
          <p class="form-control-static"><?= $model->id; ?></p>
        </div>
      </div>

        <div class="form-group row">
          <?= $form->label($model, 'username', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
            <div class="col-lg-8">
              <div class="form-control view-field">
                <?= $model->username; ?>
              </div>
            </div>
        </div>

        <div class="form-group row">
          <?= $form->label($model, 'status_type', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
            <div class="col-lg-8">
              <div class="form-control view-field">
                <?= $model->status_type == User::STATUS_ACTIVE ? Yii::t('user', 'Active') : Yii::t('user', 'Not active'); ?>
              </div>
              <p class="help-block">Los usuarios INACTIVOS no podrán acceder al sistema</p>
            </div>
        </div>
    </div>
  </div>
    
  <?php
    /*
    |--------------------------------------------------------------------------
    | PERMISSIONS
    |--------------------------------------------------------------------------
    */
  ?>
  <div class="panel">
    <header class="panel-heading">
      <h3 class="panel-title">Roles y permisos</h3>
    </header>
    <div class="panel-body">
      <div class="row row-lg">
        <div class="col-md-6">
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Roles</label>
            <div class="col-sm-9">
              <?php
                  // Assigned roles
                  Html::ul($roles['assigned']);
                ?>
                <?= Html::link(Yii::t('app', 'Asignar roles'), ['admin/update', 'id' => $model->id], ['class' => 'btn btn-default']); ?>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Permisos</label>
            <div class="col-sm-9">
              <div id="permission-field-wrapper" class="field-wrapper span6">
                <table id="permission-table" class="items table">
                  <tbody>
                    <?php foreach ($tasks['list'] as $que_task) : ?>
                      <tr>
                        <td class="task_column">
                          <?= $que_task->description; ?>
                        </td>
                        <td>
                          <?php
                            // Tasks auto-assigned from role (ancestror)
                            if ( isset($roles['tasks'][$que_task->name]) ) :
                          ?>
                            <?=
                              Html::checkBox($que_task->name, true, [
                                'template' => '{input}',
                                'container' => '',
                                'disabled' => 'disabled'
                              ]);
                            ?>
                          <?php else: ?>
                            <?=
                              Html::checkBox($que_task->name, in_array($que_task->name, $tasks['assigned']), [
                                'template' => '{input}',
                                'container' => '',
                              ]);
                            ?>
                          <?php endif; ?>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

      <?php
      /*
      |--------------------------------------------------------------------------
      | BUTTONS
      |--------------------------------------------------------------------------
      */
    ?>
    <div class="form-group row">
      <div class="col-lg-12 form-actions buttons">
        <?php
          // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
          $this->widget('@bootstrap.widgets.TbButton', [
            'buttonType'  => 'submit',
            'type'        => 'primary',
            'label'       => Yii::t('app', 'Save'),
          ]);
        
          // Cancel
          echo Html::link(Yii::t('app', 'Cancel'), ['/user/admin'], ['class' => 'btn btn-dark']);
          
          // More links
          // echo '<div class="btn-group dropup">'. Html::more_links($this->menuAction(\dz\db\ActiveRecord::extractPkValue($model, true)), ['class' => 'dropdown-menu']) .'</div>';
        ?>
      </div>
    </div>
</div>
<?php
  // End model form
  $this->endWidget();
?>