<?php
/*
|--------------------------------------------------------------------------
| Form partial page for User model
|--------------------------------------------------------------------------
|
| Available variables:
|   - $user_model: User model
|   - $roles: Roles (auth)
|   - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use user\models\User;
  
  // Current action name
  $current_action = Yii::currentAction(true);
  
  if ( $current_action == 'create' )
  {
    // Por defecto es ACTIVO
    $user_model->status_type = User::STATUS_ACTIVE;
  }

  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal',
      'autocomplete' => 'off'
    ]
  ]);

  // Error summary
  $errors = $form->errorSummary($user_model);
  if ( $errors )
  {
    echo $errors;
  }

  /*
  |--------------------------------------------------------------------------
  | USER INFORMATION
  |--------------------------------------------------------------------------
  */
?>
<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title"><?= Yii::t('app', 'Usuario'); ?></h3>
  </header>
  <div class="panel-body">
    <div class="row">
      <div class="col-lg-7">
        <?php if ($current_action != 'create') : ?>
          <div class="form-group row">
            <?= $form->label($user_model, 'id', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
            <div class="col-lg-9">
              <p class="form-control-static"><?= $user_model->id; ?></p>
            </div>
          </div>
        <?php endif; ?>

        <div class="form-group row<?php if ( $user_model->hasErrors('email') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($user_model, 'email', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
          <div class="col-lg-9">
            <?=
              $form->textField($user_model, 'email', [
                'maxlength' => 255
              ]);
            ?>
            <?= $form->error($user_model,'email'); ?>
          </div>
        </div>

        <div class="form-group row<?php if ( $user_model->hasErrors('username') ) : ?> has-danger<?php endif; ?>">
          <?php /*<?= $form->label($user_model, 'username', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>*/ ?>
          <label class="col-lg-3 col-sm-3 form-control-label" for="<?= Html::resolve_name($user_model, 'username'); ?>"><?= Yii::t('app', 'Username (internal)'); ?></label>
          <div class="col-lg-9">
            <?=
              $form->textField($user_model, 'username', [
                'placeholder' => Yii::t('user', "username"),
                'maxlength' => 100
              ]);
            ?>
            <?= $form->error($user_model,'username'); ?>
            <p class="text-help">Solo están permitidos caracteres en minúscula o números. No introduzca espacios en blanco.</p>
          </div>
        </div>

        <div class="form-group row<?php if ( $user_model->hasErrors('password') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($user_model, 'password', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
          <div class="col-lg-9">
            <?=
              $form->passwordField($user_model, 'password', [
                'placeholder' => Yii::t('user', "password"),
                'maxlength' => 128
              ]);
            ?>
            <?= $form->error($user_model,'password'); ?>
            <p class="text-help">Contraseña con un mínimo de 6 caracteres.</p>
          </div>
        </div>

        <div class="form-group row<?php if ( $user_model->hasErrors('is_force_change_password') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($user_model, 'is_force_change_password', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
          <div class="col-lg-9">
            <div class="form-group form-radio-group">
              <div class="radio-custom radio-default radio-inline">
                <input type="radio" id="is_force_change_password-1" name="User[is_force_change_password]" value="1"<?php if ( $user_model->is_force_change_password == 1 ) : ?> checked<?php endif; ?>>
                <label for="is_force_change_password-1">Sí</label>
              </div>
              <div class="radio-custom radio-default radio-inline">
                <input type="radio" id="is_force_change_password-0" name="User[is_force_change_password]" value="0"<?php if ( $user_model->is_force_change_password == 0 ) : ?> checked<?php endif; ?>>
                <label for="is_force_change_password-0">No</label>
              </div>
            </div>
            <?= $form->error($user_model,'is_force_change_password'); ?>
            <p class="help-block">Si se activa este campo, el usuario deberá cambiar la contraseña la próxima vez que acceda a la plataforma.</p> 
          </div>
        </div>
      </div><!-- .col-lg-7 -->

      <div class="col-lg-5">
        <div class="form-group row<?php if ( $user_model->hasErrors('status_type') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($user_model, 'status', ['class' => 'col-lg-4 col-sm-4 form-control-label']); ?>
          <div class="col-lg-8">
            <?=
              $form->dropDownList($user_model, 'status_type', User::model()->status_type_labels(), [
                'class'       => 'form-control',
                'data-plugin' => 'select2',
                'style'       => 'max-width: 300px',
              ]);
            ?>
            <?= $form->error($user_model,'status'); ?>
            <p class="text-help">Los usuarios INACTIVOS no podrán acceder al sistema</p>
          </div>
        </div>

        <?php if ($current_action != 'create') : ?>
          <div class="form-group row">
            <?= $form->label($user_model, 'created_date', ['class' => 'col-lg-4 col-sm-4 form-control-label']); ?>
            <div class="col-lg-8">
              <p class="form-control view-field"><?= $user_model->created_date; ?></p>
            </div>
          </div>

          <div class="form-group row">
            <?= $form->label($user_model, 'last_login_date', ['class' => 'col-lg-4 col-sm-4 form-control-label']); ?>
            <div class="col-lg-8">
              <p class="form-control view-field"><?= !empty($user_model->last_login_date) ? $user_model->last_login_date : Yii::t('app', 'Never'); ?></p>
            </div>
          </div>

          <div class="form-group row">
            <?= $form->label($user_model, 'last_change_password_date', ['class' => 'col-lg-4 col-sm-4 form-control-label']); ?>
            <div class="col-lg-8">
              <p class="form-control view-field"><?= ( !empty($user_model->last_change_password_date) && $user_model->last_change_password_date !== $user_model->created_date ) ? $user_model->last_change_password_date : Yii::t('app', 'Never'); ?></p>
            </div>
          </div>
        <?php endif; ?>
      
      </div><!-- .col-lg-5 -->
    </div><!-- .row -->

    <?php
      // SUPERADMIN user can add a SUPER permission to another ADMIN user
      if ( $current_action != 'create' && Yii::app()->user->id == 1 && $user_model->isAdmin() ) :
    ?>
      <div class="row">
        <div class="col-lg-12">
          <hr>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-7">
          <div class="form-group row<?php if ( $user_model->hasErrors('is_superadmin') ) : ?> has-danger<?php endif; ?>">
            <?= $form->label($user_model, 'is_superadmin', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
            <div class="col-lg-9">
              <div class="form-group form-radio-group">
                <div class="radio-custom radio-default radio-inline">
                  <input type="radio" id="is_superadmin-1" name="User[is_superadmin]" value="1"<?php if ( $user_model->is_superadmin == 1 ) : ?> checked<?php endif; ?>>
                  <label for="is_superadmin-1">Sí</label>
                </div>
                <div class="radio-custom radio-default radio-inline">
                  <input type="radio" id="is_superadmin-0" name="User[is_superadmin]" value="0"<?php if ( $user_model->is_superadmin == 0 ) : ?> checked<?php endif; ?>>
                  <label for="is_superadmin-0">No</label>
                </div>
              </div>
              <?= $form->error($user_model,'is_superadmin'); ?>
              <p class="help-block"><span class="text-warning">WARNING:</span> Superadmin will have <u>FULL PERMISSIONS for ENTIRE PLATFORM</u>.</p> 
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>

  </div><!-- .panel-body -->
</div><!-- .panel -->

<?php
  /*
  |--------------------------------------------------------------------------
  | PROFILE INFORMATION
  |--------------------------------------------------------------------------
  */
?>
<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title"><?= Yii::t('app', 'Perfil') ; ?></h3>
  </header>
  <div class="panel-body">
    <div class="panel-body">
      <div class="form-group row<?php if ( $user_model->hasErrors('firstname') OR $user_model->hasErrors('lastname') ) : ?> has-danger<?php endif; ?>">
        <label class="col-lg-2 col-sm-2 form-control-label"><?= Yii::t('app', 'Nombre') ; ?></label>
        <div class="col-lg-10">
          <div class="row">
            <div class="col-lg-6">
              <?= $form->textField($user_model, 'firstname', ['maxlength' => 100]); ?>
              <?= $form->error($user_model,'firstname'); ?>
            </div>
            <div class="col-lg-6">
              <?= $form->textField($user_model, 'lastname', ['maxlength' => 100]); ?>
              <?= $form->error($user_model,'lastname'); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- .panel-body -->
</div><!-- .panel -->


<?php
  /*
  |--------------------------------------------------------------------------
  | ROLES and PERMISSIONS
  |--------------------------------------------------------------------------
  */
?>
<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title">Roles y permisos</h3>
  </header>
  <div class="panel-body">
    <div class="panel-body">
      <?php if ( $current_action == 'create' ) : ?>
        <div class="form-group row">
          <label class="col-lg-2 col-sm-2 form-control-label">Roles</label>
          <div class="col-lg-10">
            <?= Html::checkBoxListControlGroup('user_roles', [], $roles); ?>
          </div>
        </div>
      <?php else: ?>
        <div class="row row-lg">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-lg-3 col-sm-3 form-control-label">Roles</label>
              <div class="col-lg-9">
                <?= Html::checkBoxListControlGroup('user_roles', $roles['assigned'], $roles['list']); ?>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 form-control-label">Permisos</label>
              <div class="col-sm-9">
                <?=
                  // Tasks assigned by role
                  Html::ul($roles['tasks']);
                ?>
                <?=
                  // Tasks assigned manually
                  Html::ul($tasks['assigned']);
                ?>
                <?= Html::link(Yii::t('app', 'Asignar permisos'), ['admin/permission', 'id' => $user_model->id], ['class' => 'btn btn-default'] ); ?>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
    </div>
  </div><!-- .panel-body -->
</div><!-- .panel -->

<?php
  /*
  |--------------------------------------------------------------------------
  | ACCIONES
  |--------------------------------------------------------------------------
  */
?>  
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
        $this->widget('@bootstrap.widgets.TbButton', [
          'buttonType' => 'submit',
          'type'       => 'primary',
          'label'      => Yii::t('app', 'Save'),
        ]);
        
        // Cancel
        echo Html::link(Yii::t('app', 'Cancel'), ['admin'], ['class' => 'btn btn-dark']);
      ?>
    </div><!-- form-actions -->
  </div>

  <?php
    // End model form
    $this->endWidget();
  ?>
</div>