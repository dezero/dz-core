<?php
/*
|--------------------------------------------------------------------------
| Update profile page for User model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $user_model: User model
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = Yii::t('backend', 'Update profile');
?>
<div class="page-header">
  <h1 class="page-title">
    <?= $this->pageTitle; ?>
  </h1>
</div>

<div class="page-content container-fluid">
  <?php
    /*
    |--------------------------------------------------------------------------
    | UPDATE PROFILE FORM
    |--------------------------------------------------------------------------
    */
    $this->renderPartial('//user/profile/_form', [
      'user_model'  => $user_model,
      'form_id'     => $form_id
    ]);
  ?>
</div>
