<?php
/*
|--------------------------------------------------------------------------
| Form partial page for updating profile
|--------------------------------------------------------------------------
|
| Available variables:
|  - $user_model: User model
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;

  // Controller and action names in lowercase
  $current_action = Yii::currentAction(true);

  // Create form object
  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal profile-form-wrapper',
      'enctype' => 'multipart/form-data',
      'autocomplete' => 'off'
    ]
  ]);

  // Error summary
  $errors = $form->errorSummary([$user_model]);
  if ( $errors )
  {
    echo $errors;
  }
?>

<div class="panel">
  <?php /*
  <header class="panel-heading">
    <h3 class="panel-title"><?= Yii::t('backend', 'My profile'); ?></h3>
  </header>
  */ ?>
  <div class="panel-body">
    <div class="row">
      <div class="col-lg-8">
        <div class="form-group row<?php if ( $user_model->hasErrors('firstname') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($user_model, 'firstname', ['class' => 'col-lg-4 form-control-label']); ?>
          <div class="col-lg-6">
            <?=
              $form->textField($user_model, 'firstname', [
                'maxlength' => 100,
                'placeholder' => ''
              ]);
            ?>
            <?= $form->error($user_model, 'firstname'); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-8">
        <div class="form-group row<?php if ( $user_model->hasErrors('lastname') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($user_model, 'lastname', ['class' => 'col-lg-4 form-control-label']); ?>
          <div class="col-lg-6">
            <?=
              $form->textField($user_model, 'lastname', [
                'maxlength' => 100,
                'placeholder' => ''
              ]);
            ?>
            <?= $form->error($user_model, 'lastname'); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-8">
        <div class="form-group row<?php if ( $user_model->hasErrors('email') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($user_model, 'email', ['class' => 'col-lg-4 col-sm-4 form-control-label']); ?>
          <div class="col-lg-6">
            <?=
              $form->textField($user_model, 'email', [
                'maxlength' => 255,
                'placeholder' => ''
              ]);
            ?>
            <?= $form->error($user_model, 'email'); ?>
            <p class="text-help"><?= Yii::t('backend', 'Required. Email user for log in to the platform'); ?></p>
          </div>
        </div>
      </div>
    </div>

    <div class="row password-row hide">
      <div class="col-lg-8">
        <div class="form-group row<?php if ( $user_model->hasErrors('password') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($user_model, 'password', ['class' => 'col-lg-4 col-sm-4 form-control-label']); ?>
          <div class="col-lg-6">
            <?=
              $form->passwordField($user_model, 'password', [
              // $form->textField($user_model, 'password', [
                'maxlength' => 100,
                'placeholder' => ''
              ]);
            ?>
            <?= $form->error($user_model, 'password'); ?>
            <p class="text-help"><?= Yii::t('backend', 'Minimal length 6 characters'); ?></p>
          </div>
        </div>
      </div>
    </div>

    <div class="row password-row hide">
      <div class="col-lg-8">
        <div class="form-group row<?php if ( $user_model->hasErrors('verify_password') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($user_model, 'verify_password', ['class' => 'col-lg-4 col-sm-4 form-control-label']); ?>
          <div class="col-lg-6">
            <?=
              $form->passwordField($user_model, 'verify_password', [
              // $form->textField($user_model, 'verify_password', [
                'maxlength' => 100,
                'placeholder' => ''
              ]);
            ?>
            <?= $form->error($user_model, 'verify_password'); ?>
            <p class="text-help"><?= Yii::t('backend', 'Retype password'); ?></p>
          </div>
        </div>
      </div>
    </div>

    <div class="row password-row">
      <div class="col-lg-8">
        <div class="form-group row">
          <div class="col-lg-4 col-sm-4 form-control-label"></div>
          <div class="col-lg-6">
            <a href="#" id="change-password-btn" class="btn btn-dark btn-outline"><?= Yii::t('backend', 'Set new password'); ?></a>
            <input type="hidden" id="is-password-changed" name="is-password-changed" value="0">
          </div>
        </div>
      </div>
    </div>

  </div>
</div>


<?php
  /*
  |--------------------------------------------------------------------------
  | ACTIONS
  |--------------------------------------------------------------------------
  */
?>
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
        $this->widget('@bootstrap.widgets.TbButton', [
          'buttonType'  => 'submit',
          'type'        => 'primary',
          'label'       => Yii::t('backend', 'Save')
        ]);
      ?>
    </div><!-- form-actions -->
  </div>
<?php
  // End model form
  $this->endWidget();
?>
