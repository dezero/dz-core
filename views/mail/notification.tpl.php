<?php
/*
|--------------------------------------------------------------------------
| Basic mail HTML template
|--------------------------------------------------------------------------
|
| Available variables:
|  - $body: Body content
|
*/
?>
<?php if ( isset($body) ) : ?>
  <p><?= $body; ?></p>
<?php endif; ?>