<?php
/*
|--------------------------------------------------------------------------
| Admin list page for Category model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $category_model: Category model class
|  - $vec_config: Category type configuration
|
*/  
  use dz\helpers\Html;
  
  // Page title
  $this->pageTitle = Yii::t('app', $category_model->text('index_title'));
?>
<div class="page-main">
  <div class="page-header">
    <h1 class="page-title"><?= $this->pageTitle; ?></h1>
    <?php
      // if ( $vec_config['is_editable'] && Yii::app()->user->checkAccess("category.category.create") && ( !isset($vec_config['is_first_level_sortable']) || !$vec_config['is_first_level_sortable'] ) ):
      if ( $vec_config['is_editable'] && Yii::app()->user->checkAccess("category.category.create") ):
    ?>
      <div class="page-header-actions">
        <?= Html::link('<i class="icon wb-plus"></i> '.  $category_model->text('add_button'), ["create"], ['class' => 'btn btn-primary']); ?>
      </div>
    <?php endif; ?>
  </div>
  <div class="page-content">
    <div class="row row-lg">
      <?php if ( isset($vec_config['is_first_level_sortable']) && $vec_config['is_first_level_sortable'] ) : ?>
        <div class="col-lg-3">
          <div class="panel category-tree-wrapper">
            <header class="panel-heading">
              <h3 class="panel-title"><?= Yii::t('app', $category_model->text('panel_title')); ?></h3>
            </header>
            <div class="panel-body">
              <?php
                /*
                |--------------------------------------------------------------------------
                | CATEGORY TREE (TABLE OF CONTENT)
                |--------------------------------------------------------------------------
                */
                $this->renderPartial($category_model->get_view_path("_tree_main"), [
                  'category_model'  => $category_model,
                  'vec_config'      => $vec_config,
                  'is_ajax'         => false
                ]);
              ?>
            </div>
          </div>
        </div>
        <div class="col-lg-9">
      <?php else : ?>
        <div class="col-lg-12">
      <?php endif; ?>
        <div class="panel panel-category-summary panel-top-summary">
          <header class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('app', $category_model->text('list_title')); ?></h3>
          </header>
          <div class="panel-body container-fluid">
            <?php
              /*
              |----------------------------------------------------------------------------------------
              | GridView widget
              |----------------------------------------------------------------------------------------
              */
                $this->widget('dz.grid.GridView', [
                  'id'            => 'category-'. $this->category_type .'-grid',
                  'dataProvider'  => $category_model->search(),
                  'filter'        => $category_model,
                  'emptyText'     => $category_model->text('empty_text'),
                  'enableHistory' => true,
                  'loadModal'     => true,
                  'enableSorting' => false,
                  'type'          => ['striped', 'hover'],
                  'beforeAjaxUpdate' => 'js:function() { $("html, body").animate({scrollTop: 0}, 100); }',
                  'columns'       => [
                    [
                      'header' => Yii::t('app', 'Name'),
                      'name' => 'name',
                      'value' => 'trim($this->grid->getOwner()->renderPartial($data->get_view_path("_grid_column"), ["column" => "name", "model" => $data]))',
                    ],
                    [
                      'header' => Yii::t('app', 'URL'),
                      'name' => 'seo_url_filter',
                      'value' => 'trim($this->grid->getOwner()->renderPartial($data->get_view_path("_grid_column"), ["column" => "seo_url", "model" => $data]))',
                      'visible' => isset($vec_config['is_seo_fields']) && $vec_config['is_seo_fields']
                    ],
                    [
                      'class' => 'dz.grid.ButtonColumn',
                      'header' => Yii::t('app', 'ACTION'),
                      'viewButton' => false,
                      'clearButton' => true,
                      'menuAction' => function($data, $row) {
                        return [
                          [
                          'label' => Yii::t('app', 'Enable'),
                          'icon' => 'arrow-up',
                          'url' => ['enable', 'id' => $data->category_id],
                          'visible' => '!$data->is_enabled() && Yii::app()->user->checkAccess("category.category.create")',
                          'confirm' => $data->text('enable_confirm'),
                          'htmlOptions' => [
                            'id' => 'enable-category-'. $data->category_type .'-'. $data->category_id,
                            'data-gridview' => 'category-'. $data->category_type .'-grid'
                            ]
                          ],
                          [
                            'label' => Yii::t('app', 'Disable'),
                            'icon' => 'arrow-down',
                            'url' => ['disable', 'id' => $data->category_id],
                            'visible' => '$data->is_enabled() && Yii::app()->user->checkAccess("category.category.create")',
                            'confirm' => $data->text('disable_confirm'),
                            'htmlOptions' => [
                              'id' => 'disable-category-'. $data->category_type .'-'. $data->category_id,
                              'data-gridview' => 'category-'. $data->category_type .'-grid'
                            ]
                          ]
                        ];
                      },
                    ],
                  ],
                ]);
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>