<?php
/*
|--------------------------------------------------------------------------
| Form partial page for Category model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $category_model: Category model class
|  - $image_model: AssetImage model class
|  - $vec_config: Category configuration options
|  - $vec_translated_models: Array with TranslatedCategory model class
|  - $default_language: Default language code
|  - $vec_extra_languages: Array with Extra languages
|  - $vec_seo_models: Array with SEO models
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  
  $current_controller = Yii::currentController(true);
  $current_action = Yii::currentAction(true);

  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class'   => 'form-horizontal '. $this->category_type .'-form',
      'enctype' => 'multipart/form-data',
      'autocomplete' => 'off'
    ]
  ]);

  // Error summary
  $vec_error_models = [$category_model];
  if ( $current_action != 'create' && isset($vec_translated_models) && !empty($vec_translated_models) )
  {
    $vec_error_models = CMap::mergeArray($vec_error_models, array_values($vec_translated_models));
  }
  $errors = $form->errorSummary($vec_error_models);
  if ( $errors )
  {
    echo $errors;
  }
?>
<input name="StatusChange" id="status-change" class="form-control" type="hidden" value="">

<?php
  /*
  |--------------------------------------------------------------------------
  | CATEGORY INFORMATION
  |--------------------------------------------------------------------------
  */
?>
<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title"><?php if ( $category_depth > 0) : ?><?= $category_model->text('subentity_label'); ?><?php else : ?><?= $category_model->text('entity_label'); ?><?php endif; ?> - <?= Yii::t('app', 'Information'); ?></h3>
  </header>
  <div class="panel-body">
    <?php
      /**
       * EXTRA LANGUAGES in TABS
       */
      if ( $current_action != 'create' && $vec_config['is_multilanguage'] ) :
    ?>
      <div class="nav-tabs-horizontal pt-10" data-plugin="tabs">
        <ul class="nav nav-tabs nav-tabs-line tabs-line-top" role="tablist">
          <li class="nav-item" role="presentation">
            <a class="nav-link active" data-toggle="tab" href="#lang-<?= $default_language; ?>" aria-controls="lang-<?= $default_language; ?>" role="tab" aria-expanded="false" data-language="<?= $default_language; ?>"><span class="flag-icon flag-icon-<?= $default_language; ?>"></span> <?= StringHelper::strtoupper(Yii::app()->i18n->language_name($default_language)); ?></a>
          </li>
          <?php if ( isset($vec_extra_languages) && !empty($vec_extra_languages) ) : ?>
            <?php foreach ( $vec_extra_languages as $language_id ) : ?>
              <?php if ( isset($vec_translated_models[$language_id]) ) : ?>
                <li class="nav-item" role="presentation">
                  <a class="nav-link" data-toggle="tab" href="#lang-<?= $language_id; ?>" aria-controls="lang-<?= $language_id; ?>" role="tab" aria-expanded="false" data-language="<?= $language_id; ?>"><span class="flag-icon flag-icon-<?= $language_id; ?>"></span> <?= StringHelper::strtoupper(Yii::app()->i18n->language_name($language_id)); ?></a>
                </li>
              <?php endif; ?>
            <?php endforeach; ?>
          <?php endif; ?>
        </ul>
      </div>
      <div class="tab-content pt-20">
        <div class="tab-pane active" id="lang-<?= $default_language; ?>" role="tabpanel" aria-expanded="false">
    <?php endif; ?>

    <div class="form-group row<?php if ( $category_model->hasErrors('name') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($category_model, 'name', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?php if ( $vec_config['is_editable'] ) : ?>
          <?=
            $form->textField($category_model, 'name', [
              'maxlength' => 128,
              'placeholder' => ''
            ]);
          ?>
        <?php else : ?>
          <div class="item-content"><?= $category_model->name; ?></div>
        <?php endif; ?>
        <?= $form->error($category_model,'name'); ?>
      </div>
    </div>

    <?php
      //----------------------------------------------------------------
      // DESCRIPTION
      //----------------------------------------------------------------
      if ( isset($vec_config['is_description']) && $vec_config['is_description'] ) :
    ?>
      <div class="form-group row<?php if ( $category_model->hasErrors('description') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($category_model, 'description', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
        <div class="col-lg-10">
          <?php if ( $vec_config['is_editable'] ) : ?>
            <?=
              $form->textArea($category_model, 'description', [
                'rows' => 3
              ]);
            ?>
          <?php else : ?>
            <div class="item-content"><?= $category_model->description; ?></div>
          <?php endif; ?>
          <?= $form->error($category_model,'description'); ?>
        </div>
      </div>
    <?php endif; ?>

    <?php
      //----------------------------------------------------------------
      // IMAGE
      //----------------------------------------------------------------
      if ( isset($vec_config['is_image']) && $vec_config['is_image'] ) :
    ?>
      <div class="form-group row<?php if ( $image_model->hasErrors('file_name') ) : ?> has-danger<?php endif; ?>">
        <label class="col-lg-2 col-sm-2 form-control-label" for="AssetImage_0_file_name"><?= $category_model->getAttributeLabel('image_id'); ?></label>
        <div class="col-lg-10">
          <?php
            $this->widget('\dz\widgets\Upload', [
              'name'      => 'AssetImage[0][file_name]',
              'model'     => $image_model,
              'attribute' => 'file_name',
              'view_mode' => false,
              'is_image'  => true,
            ]);
          ?>
          <?php /*<p class="help-block">El tamaño debe ser <u>400x200 pixels</u> o superior pero manteniendo el ratio. Por ejemplo, 600x300 o 800x400.</p>*/ ?>
          <?= $form->error($image_model,'file_name'); ?>
        </div>
      </div>
    <?php endif; ?>

    <?php
      /**
       * EXTRA LANGUAGES in TABS
       */
      if ( $current_action != 'create' && $vec_config['is_multilanguage'] ) :
    ?>
      </div><!-- .tab-pane -->
      <?php if ( isset($vec_extra_languages) && !empty($vec_extra_languages) ) : ?>
        <?php foreach ( $vec_extra_languages as $language_id ) : ?>
          <?php if ( isset($vec_translated_models[$language_id]) ) : ?>
            <?php $translated_category_model = $vec_translated_models[$language_id]; ?>
            <div class="tab-pane" id="lang-<?= $language_id; ?>" role="tabpanel" aria-expanded="false">

              <div class="form-group row<?php if ( $translated_category_model->hasErrors('name') ) : ?> has-danger<?php endif; ?>">
                <?= $form->label($translated_category_model, 'name', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
                <div class="col-lg-10">
                  <?=
                    $form->textField($translated_category_model, 'name', [
                      'name'        => 'TranslatedCategory['. $language_id .'][name]',
                      'placeholder' => '',
                    ]);
                  ?>
                  <?= $form->error($translated_category_model,'name'); ?>
                </div>
              </div>

              <?php if ( isset($vec_config['is_description']) && $vec_config['is_description'] ) : ?>
                <div class="form-group row<?php if ( $translated_category_model->hasErrors('description') ) : ?> has-danger<?php endif; ?>">
                  <?= $form->label($translated_category_model, 'description', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
                  <div class="col-lg-10">
                    <?=
                      $form->textArea($translated_category_model, 'description', [
                        'name'      => 'TranslatedCategory['. $language_id .'][description]',
                        'rows'       => 3
                      ]);
                    ?>
                    <?= $form->error($translated_category_model,'description'); ?>
                  </div>
                </div>
              <?php endif; ?>

            </div><!-- .tab-pane -->
          <?php endif; ?>
        <?php endforeach; ?>
        </div><!-- .tab-content -->
      <?php endif; ?>
    <?php endif; ?>
  </div>
</div>

<?php
  /*
  |--------------------------------------------------------------------------
  | SEO DATA
  |--------------------------------------------------------------------------
  */
  if ( isset($vec_config['is_seo_fields']) && $vec_config['is_seo_fields'] && !empty($vec_seo_models) ) :
?>
  <?php
    // SEO form partial
    $this->renderPartial($category_model->get_view_path('_form_seo'), [
      'form'                => $form,
      'category_model'      => $category_model,
      'vec_config'          => $vec_config,
      'vec_seo_models'      => $vec_seo_models,
      'default_language'    => $default_language,
      'vec_extra_languages' => $vec_extra_languages,
      'current_action'      => $current_action
    ]);
  ?>
<?php endif; ?>

<?php
  /*
  |--------------------------------------------------------------------------
  | ACTIONS
  |--------------------------------------------------------------------------
  */
?>  
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
        $this->widget('@bootstrap.widgets.TbButton', [
          'buttonType'  => 'submit',
          'type'        => 'primary',
          'label'       => Yii::t('app', 'Save'),
        ]);
        
        // Cancel
        echo Html::link(Yii::t('app', 'Cancel'), ['/category/'. $current_controller], ['class' => 'btn btn-dark']);

        if ( $current_action !== 'create' )
        {
          if ( isset($vec_config['is_disable_allowed']) && $vec_config['is_disable_allowed'] )
          {
            // Disable category button
            if ( $category_model->is_enabled() )
            {
              echo Html::link(Yii::t('app', 'Disable'), ['#'], [
                'id'            => 'disable-category-btn',
                'class'         => 'btn btn-danger right',
                'data-confirm'  => $category_model->text('disable_confirm'),
                'data-form'     => $form_id,
                'data-value'    => 'disable',
                'data-plugin'   => 'dz-status-button'
              ]);
            }

            // Enable category button
            else
            {
              echo Html::link(Yii::t('app', 'Enable'), ['#'], [
                'id'            => 'enable-category-btn',
                'class'         => 'btn btn-success right',
                'data-confirm'  => $category_model->text('enable_confirm'),
                'data-form'     => $form_id,
                'data-value'    => 'enable',
                'data-plugin'   => 'dz-status-button'
              ]);
            }
          }

          // Delete category button
          if ( Yii::app()->user->id == 1 || ( isset($vec_config['is_delete_allowed']) && $vec_config['is_delete_allowed'] ) )
          {
            echo Html::link(Yii::t('app', 'Delete'), ['#'], [
              'id'            => 'delete-category-btn',
              'class'         => 'btn btn-delete right',
              'data-confirm'  => $category_model->text('delete_confirm'),
              'data-form'     => $form_id,
              'data-value'    => 'delete',
              'data-plugin'   => 'dz-status-button'
            ]);
          }
        }
      ?>
    </div><!-- form-actions -->
  </div>

<?php
  // End model form
  $this->endWidget();
?>