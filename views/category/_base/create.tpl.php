<?php
/*
|--------------------------------------------------------------------------
| Create form page for Category model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $category_model: Category model class
|  - $image_model: AssetImage model class
|  - $category_depth: Category depth (1, 2, 3)
|  - $vec_config: Category configuration options
|  - $vec_translated_models: Array with TranslatedCategory model class
|  - $vec_seo_models: Array with SEO models
|  - $default_language: Default language code
|  - $vec_extra_languages: Array with Extra languages
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  
  // Current controller
  $current_controller = Yii::currentController();

  // Page title
  switch ( $category_depth )
  {
    case 0:
      $this->pageTitle = Yii::t('app', $category_model->text('create_title'));
      break;

    default:
      $this->pageTitle = Yii::t('app', $category_model->text('subcategory_title') , ['{subcategory}' => $category_model->categoryParent->name]);
      break;
  }
?>

<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => $category_model->text('entities_label'),
        'url' => ['/category/'. $current_controller .'/'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <div class="row row-lg">
    <?php if ( $category_model->category_parent_id > 0 ) : ?>
      <div class="col-lg-3">
        <div class="panel category-tree-wrapper">
          <header class="panel-heading">
            <h3 class="panel-title"><?= $category_model->text('panel_title'); ?></h3>
          </header>
          <div class="panel-body">
            <?php
              /*
              |--------------------------------------------------------------------------
              | FAMILIA TREE (TABLE OF CONTENT)
              |--------------------------------------------------------------------------
              */
              $this->renderPartial($category_model->get_view_path('_tree'), [
                'category_model'  => $category_model,
                'category_depth'  => $category_depth,
                'vec_config'      => $vec_config,
                'is_ajax'         => false
              ]);
            ?>
          </div>
        </div>
      </div>
      <div class="col-lg-9">
    <?php else : ?>
      <div class="col-lg-12">
    <?php endif; ?>
      <?php
      	// Render form

      	$this->renderPartial($category_model->get_view_path('_form'), [
          'category_model'        => $category_model,
          'image_model'           => $image_model,
          'category_depth'        => $category_depth,
          'vec_config'            => $vec_config,
          'vec_translated_models' => $vec_translated_models,
          'vec_seo_models'        => $vec_seo_models,
          'default_language'      => $default_language,
          'vec_extra_languages'   => $vec_extra_languages,
          'form_id'               => $form_id,
        ]);
      ?>
    </div>
  </div>
</div>