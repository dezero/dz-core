<?php
/*
|--------------------------------------------------------------------------
| Form partial page for Category tree widget
|--------------------------------------------------------------------------
|
| Available variables:
|  - $category_model: Category model class
|  - $vec_config: Category configuration options
|  - $is_ajax: Is this partial loaded via AJAX?
*/
  use dz\helpers\Html;

  $current_controller = Yii::currentController(true);
  $current_id = 0;
  $second_level_parent_id = 0;
  $current_action = 'update';
  if ( ! $is_ajax )
  {
    $current_action = Yii::currentAction(true);
    if ( isset($_GET['id']) )
    {
      $current_id = $_GET['id'];
    }
  }

  $current_category_model = $category_model;
  if ( $category_model->category_parent_id > 0 )
  {
    // First level
    $current_category_model = $category_model->categoryParent;
    if ( $current_id == 0 )
    {
      $current_id = $category_model->category_parent_id;
    }

    // Second level
    if ( $current_category_model->category_parent_id > 0 )
    {
      // Save parent_id
      $second_level_parent_id = $current_category_model->category_id;

      $current_category_model = $current_category_model->categoryParent;
      if ( $current_id == 0 )
      {
        $current_id = $current_category_model->category_parent_id;
      }
    }
  }
?>
<?php if ( ! $is_ajax ) : ?>
  <div id="category-loading-tree" class='commerce-loading center hide'></div>
  <div class="dd dd-category-group" id="category-nestable-wrapper" data-name="category" data-url="<?= $this->createAbsoluteUrl("/category/". $current_controller ."/updateWeight"); ?>?category_id=<?= $current_category_model->category_id; ?>">
<?php endif; ?>
  <ol class="dd-list">
    <li class="dd-item dd3-item dd-item-group dd-level1<?php if ( $current_id == $current_category_model->category_id ) : ?> active<?php endif; ?>" data-rel="level1" data-id="<?= $current_category_model->category_id; ?>" id="dd-item-<?= $current_category_model->category_id; ?>">
      <?php /*<div class="dd-handle dd3-handle"></div>*/ ?>
      <div class="dd3-content">
        <?php
          $vec_html_attributes = [];
          if ( ! $current_category_model->is_enabled() )
          {
            $vec_html_attributes['class'] = 'text-danger';
          }
          $category_name = $current_category_model->name;
          if ( $current_category_model->countSubcategories > 0 )
          {
            $category_name .= ' ('. $current_category_model->countSubcategories .')';
          }
          echo Html::link($category_name, ['//category/'. $current_controller .'/update', 'id' => $current_category_model->category_id], $vec_html_attributes);
        ?>
      </div>
      <?php
          // 2nd LEVEL - Subcategories of this parent category
          if ( $current_category_model->subCategories ) :
      ?>
        <ol class="dd-list">
          <?php foreach ( $current_category_model->subCategories as $subcategory_model ) : ?>
            <li class="dd-item dd3-item dd-item-subcategory<?php if ( $current_id == $subcategory_model->category_id ) : ?> active<?php endif; ?>" data-rel="level2" data-id="q-<?= $subcategory_model->category_id; ?>" id="dd-item-q-<?= $subcategory_model->category_id; ?>">
              <div class="dd-handle dd3-handle"></div>
              <div class="dd3-content">
                <?php
                  $vec_html_attributes = [];
                  if ( ! $subcategory_model->is_enabled() )
                  {
                    $vec_html_attributes['class'] = 'text-danger';
                  }
                  $subcategory_name = $subcategory_model->name;
                  if ( $subcategory_model->countSubcategories > 0 )
                  {
                    $subcategory_name .= ' ('. $subcategory_model->countSubcategories .')';
                  }
                  echo Html::link($subcategory_name, ['//category/'. $current_controller .'/update', 'id' => $subcategory_model->category_id], $vec_html_attributes);
                ?>
              </div>
              <?php
                // 3rd LEVEL
                if ( ( $current_id == $subcategory_model->category_id OR $second_level_parent_id == $subcategory_model->category_id ) AND $subcategory_model->subCategories ) :
              ?>
                <ol class="dd-list">
                  <?php foreach ( $subcategory_model->subCategories as $sub_subcategory_model ) : ?>
                    <li class="dd-item dd3-item dd-item-subcategory<?php if ( $current_id == $sub_subcategory_model->category_id ) : ?> active<?php endif; ?>" data-rel="level2" data-id="q-<?= $sub_subcategory_model->category_id; ?>" id="dd-item-q-<?= $sub_subcategory_model->category_id; ?>">
                      <div class="dd-handle dd3-handle"></div>
                      <div class="dd3-content">
                        <?php
                          $vec_html_attributes = [];
                          if ( ! $sub_subcategory_model->is_enabled() )
                          {
                            $vec_html_attributes['class'] = 'text-danger';
                          }
                          $sub_subcategory_name = $sub_subcategory_model->name;
                          /*
                          if ( $sub_subcategory_model->countSubcategories > 0 )
                          {
                              $sub_subcategory_name .= ' ('. $sub_subcategory_model->countSubcategories .')';
                          }
                          */
                          echo Html::link($sub_subcategory_name, ['//category/'. $current_controller .'/update', 'id' => $sub_subcategory_model->category_id], $vec_html_attributes);
                        ?>
                      </div>
                    </li>
                  <?php endforeach; ?>
                </ol>
              <?php endif; ?>
            </li>
          <?php endforeach; ?>
        </ol>
      <?php endif; ?>
    </li>
  </ol>
  <?php if ( $current_action != 'create' AND !$is_ajax AND $vec_config['is_editable'] ) : ?>
    <hr/>
    <?php
      // Add new subcategory (2nd level)
      if ( $category_model->category_parent_id > 0 AND isset($vec_config['max_levels']) AND $vec_config['max_levels'] > 1 ) :
    ?>
      <div class="buttons">
        <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true"><i class="wb-plus"></i> Añadir <?= $category_model->text('subentity_label'); ?>...</a>
        <ul class="dropdown-menu">
          <li>
              <?php
                if ( $category_depth == 1 )
                {
                  echo Html::link('Nueva '. $category_model->text('subentity_label') .' de "'. $category_model->categoryParent->name .'"', ['//category/'. $current_controller .'/create', 'parent_id' => $category_model->category_parent_id], [
                    'class' => 'dropdown-item'
                  ]);
                }
                else
                {
                  echo Html::link('Nueva '. $category_model->text('subentity_label') .' de "'. $category_model->categoryParent->categoryParent->name .'"', ['//category/'. $current_controller .'/create', 'parent_id' => $category_model->categoryParent->category_parent_id], [
                    'class' => 'dropdown-item'
                  ]);
                }
              ?>
          </li>
          <li>
            <?php
              if ( $category_depth == 1 )
              {
                echo Html::link('Nueva '. $category_model->text('subentity_label') .' de "'. $category_model->name .'"', ['//category/'. $current_controller .'/create', 'parent_id' => $category_model->category_id], [
                  'class' => 'dropdown-item'
                ]);
              }
              else
              {
                echo Html::link('Nueva '. $category_model->text('subentity_label') .' de "'. $category_model->categoryParent->name .'"', ['//category/'. $current_controller .'/create', 'parent_id' => $category_model->category_parent_id], [
                  'class' => 'dropdown-item'
                ]);
              }
            ?>
          </li>
        </ul>
      </div>
    <?php
        // Add new category (1st level)
        else :
    ?>
        <?php
          // Add new subcategory
          if ( isset($vec_config['max_levels']) AND $vec_config['max_levels'] > 1 )
          {
            echo Html::link('<i class="wb-plus"></i> Añadir '. $category_model->text('subentity_label'), ['//category/'. $current_controller .'/create', 'parent_id' => $current_category_model->category_id], [
                'class' => 'btn mr-10 mb-10 btn-primary',
            ]);
          }
          else
          {
            echo Html::link('<i class="wb-plus"></i> Añadir '. $category_model->text('entity_label'), ['//category/'. $current_controller .'/create'], [
                'class' => 'btn mr-10 mb-10 btn-primary',
            ]);
          }
        ?>
    <?php endif; ?>
  <?php endif; ?>
<?php if ( ! $is_ajax ) : ?>
  </div>
</section>
<?php endif; ?>
<?php
  // Custom Javascript nestable code for this page
  if ( ! $is_ajax )
  {
    Yii::app()->clientscript
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/jquery-nestable/jquery.nestable.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/js/dz.nestable.js', CClientScript::POS_END);
      /*
      ->registerScript('category_reset_category_tree_js',
          "// Refresh 'Familia Tree' via AJAX
          function commerce_reset_category_tree(que_data_tree){
            var \$category_tree = $('#category-loading-tree');
            var \$category_nestable = $('#category-nestable-wrapper');
            \$category_nestable.nestable('destroy');
            \$category_tree.height(\$category_nestable.height()+'px').removeClass('hide');
            \$category_nestable.html(que_data_tree).removeClass('hide');
            \$category_tree.addClass('hide');
          }", CClientScript::POS_READY);
      */
  }
    
  if ( ! $vec_config['is_editable'] )
  {
    Yii::app()->clientscript->registerScript('category_tree_js',
      "$('#category-nestable-wrapper').dzNestable({
          maxDepth: 3,
          readOnly: true
      });"
      , CClientScript::POS_READY);
    /*
    Yii::app()->clientscript->registerScript('category_tree_js',
      "// Nestable
      $('#category-nestable-wrapper').nestable({
          maxDepth: 3,
          readOnly: true
      });", CClientScript::POS_READY);
     */
  }
  else
  {
    // Load category nestable
    Yii::app()->clientscript->registerScript('category_tree_js',
      "$('#category-nestable-wrapper').dzNestable({
        maxDepth: 4
      });"
      , CClientScript::POS_READY);

    /*
    Yii::app()->clientscript->registerScript('category_tree_js',
      "// Nestable
      $('#category-nestable-wrapper').nestable({
          maxDepth: 4
      }).on('change', function(){
          var que_nestable = $(this).nestable('serialize');
          var \$this = $(this);
          $('#category-loading-tree').height(\$this.height()+'px').removeClass('hide');
          \$this.addClass('hide');
          $.ajax({
              url: '". $this->createAbsoluteUrl("/category/". $current_controller ."/updateWeight") ."?category_id=". $current_category_model->category_id ."',
              type: 'post',
              dataType: 'json',
              data: {nestable: que_nestable},
              success: function(data) {
                  if ( $('#". $current_controller ."-grid').size() > 0 ) {
                      $.fn.yiiGridView.update('". $current_controller ."-grid');
                  }
                  $('#category-loading-tree').addClass('hide');
                  \$this.removeClass('hide');
              },
              error: function(request, status, error) {
                  alert('ERROR: '+request.responseText);
              },
              cache: false
          });
          // console.log(window.JSON.stringify(que_nestable));
      });", CClientScript::POS_READY);
    */
  }
?>