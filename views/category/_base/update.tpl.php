<?php
/*
|--------------------------------------------------------------------------
| Update form page for Category model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $category_model: Category model class
|  - $image_model: AssetImage model class
|  - $category_depth: Category depth (1, 2, 3)
|  - $vec_config: Category configuration options
|  - $vec_translated_models: Array with TranslatedCategory model class
|  - $vec_seo_models: Array with SEO models
|  - $default_language: Default language code
|  - $vec_extra_languages: Array with Extra languages
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  
  // Page title
  $this->pageTitle = $category_model->title();

  // Current controller
  $current_controller = Yii::currentController();
?>
<div class="page-header">
  <h1 class="page-title">
    <?= $this->pageTitle; ?>
    <?php if ( ! $category_model->is_enabled() ) : ?>
      <div class="badge badge-danger" data-toggle="tooltip" data-placement="bottom" data-original-title="<?= $category_model->disable_date; ?>"><?= Yii::t('app', 'DISABLED'); ?></div>
    <?php endif; ?>
  </h1>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => $category_model->text('entities_label'),
        'url' => ['/category/'. $current_controller],
      ],
      $this->pageTitle
    ]);
  ?>

  <div class="page-header-actions">
    <?=
      // Back
      Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app',  'Back'), ['/category/'. $current_controller], [
          'class' => 'btn btn-default btn-back',
      ]);
    ?>
  </div>
</div>

<div class="page-content container-fluid">
  <div class="row row-lg">
    <?php if ( isset($vec_config['max_levels']) && $vec_config['max_levels'] > 1  ) : ?>
      <div class="col-lg-3">
        <div class="panel category-tree-wrapper">
          <header class="panel-heading">
            <h3 class="panel-title"><?= $category_model->text('panel_title'); ?></h3>
          </header>
          <div class="panel-body">
            <?php
            /*
            |--------------------------------------------------------------------------
            | CATEGORY TREE (TABLE OF CONTENT)
            |--------------------------------------------------------------------------
            */
              // Render form
              $this->renderPartial($category_model->get_view_path('_tree'), [
                'category_model'  => $category_model,
                'vec_config'      => $vec_config,
                'category_depth'  => $category_depth,
                'is_ajax'         => false
              ]);
            ?>
          </div>
        </div>
      </div>
      <div class="col-lg-9">
    <?php else : ?>
      <div class="col-lg-12">
    <?php endif; ?>
      <?php
        // Render form
        $this->renderPartial($category_model->get_view_path('_form'), [
          'category_model'        => $category_model,
          'image_model'           => $image_model,
          'vec_config'            => $vec_config,
          'vec_translated_models' => $vec_translated_models,
          'vec_seo_models'        => $vec_seo_models,
          'vec_extra_languages'   => $vec_extra_languages,
          'default_language'      => $default_language,
          'category_depth'        => $category_depth,
          'form_id'               => $form_id
        ]);
      ?>
    </div>
  </div>
</div>
