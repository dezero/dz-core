<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for Category model
|--------------------------------------------------------------------------
|
| Available variables:
| $model: CGridView model
| $column: Column name
|
*/
  use dz\helpers\Html;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "NAME"
    |--------------------------------------------------------------------------
    */
    case 'name':
  ?>
    <?php $que_label = $model->name; ?>
    <?php if ( $model->translations ) : ?>
      <?php
        $vec_translated_name = [];
        foreach ( $model->translations as $translated_category_model )
        {
          if ( $translated_category_model->language_id !== Yii::defaultLanguage() )
          {
            $vec_translated_name[] = $translated_category_model->name;
          }
        }
        $que_label .= ' <span class="translated-name-field">('. implode(", ", $vec_translated_name) .')</span>';
      ?>
    <?php endif; ?>
    <?= Html::link($que_label, ['update', 'id' => $model->category_id]); ?>
    <?php if ( ! $model->is_enabled() ) : ?>
      <div class="badge badge-danger ml-5 mr-5" data-toggle="tooltip" data-placement="right" data-original-title="<?= $model->disable_date; ?>">DESACTIVADO</div>
    <?php endif; ?>
    <?php
      // Category config
      $vec_config = $model->get_config();
      if ( !empty($vec_config) AND isset($vec_config['max_levels']) AND $vec_config['max_levels'] > 1 )
      {
        echo Html::link($model->countSubcategories .' '. $model->text('subcategories'), ['update', 'id' => $model->category_id], ['class' => 'btn btn-dark btn-outline btn-sm ml-5']);
      }
    ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "SEO URL"
    |--------------------------------------------------------------------------
    */
    case 'seo_url':
  ?>
    <?php
      $seo_model = $model->get_seo(Yii::defaultLanguage());
      if ( $seo_model ) :
    ?>
      /<?= $seo_model->url_manual; ?>
    <?php endif; ?>
<?php endswitch; ?>