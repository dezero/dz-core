<?php
/**
 * Yii bootstrap file.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright 2008-2013 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 * @package system
 * @since 1.0
 */

if ( ! class_exists('YiiBase', false) ) {
    require DZ_VENDOR_PATH .'/yii/framework/YiiBase.php';
}


/**
 * @inheritdoc
 */
class Yii extends YiiBase
{
    /**
     * Create a path alias.
     * 
     * @see YiiBase::setPathOfAlias
     */
    public static function setAlias($alias, $path)
    {
        parent::setPathOfAlias($alias, $path);
    }


    /**
     * Translates an alias into a file path.
     * 
     * @see YiiBase::getPathOfAlias
     */
    public static function getAlias($alias)
    {
        $alias = parent::getPathOfAlias($alias);
        $alias = str_replace("www/../www/", "www/", $alias);
        return $alias;
    }


    /**
     * Try to check if a string is an alias or a path
     */
    public static function isAlias($alias_or_path)
    {
        // If the string contains a "/", will be considered as a path, unless an alias
        return ! preg_match("/\//", $alias_or_path);
    }


    /**
     * Get current controller name
     */
    public static function currentController($is_lowercase = false)
    {
        return self::app()->controller->currentControllerName($is_lowercase);
    }


    /**
     * Get current action name
     */
    public static function currentAction($is_lowercase = false)
    {
        return self::app()->controller->currentActionName($is_lowercase);
    }


    /**
     * Get current module
     */
    public static function currentModule($is_lowercase = false)
    {
        return self::app()->controller->currentModuleName($is_lowercase);
    }


    /**
     * Create a new object
     * 
     * @see classMap::create()
     */
    public static function createObject($class, $vec_params = [])
    {
        return Yii::app()->classMap->create($class, $vec_params);
    }


    /**
     * Resolve a class name
     * 
     * @see classMap::create()
     */
    public static function resolveClass($class_name)
    {
        return Yii::app()->classMap->get($class_name);
    }


    /**
     * Get current language
     */
    public static function currentLanguage()
    {
        return Yii::app()->language;
    }


    /**
     * Get default language
     * 
     * @see I18N::get_default_language()
     */
    public static function defaultLanguage()
    {
        return Yii::app()->i18n->get_default_language();
    }


    /**
     * App is multilanguage
     * 
     * @see I18N::is_multilanguage()
     */
    public static function isMultilanguage()
    {
        return Yii::app()->i18n->is_multilanguage();
    }
}
