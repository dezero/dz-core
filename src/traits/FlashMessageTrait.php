<?php
/**
 * Flash messages manager trait class
 *
 * @see dz\traits\ErrorTrait
 */

namespace dz\traits;

trait FlashMessageTrait
{
    /**
     * Flash messages
     */
    public $vec_messages = [];


    /**
     * @return array
     */
    public function get_flash_messages($type = 'success')
    {
        return isset($this->vec_messages[$type]) ? $this->vec_messages[$type] : [];
    }


    /**
     * Add message(s)
     */
    public function add_flash_message($vec_messages, $type = 'success')
    {
        if ( !isset($this->vec_messages[$type]) )
        {
            $this->vec_messages[$type] = [];
        }

        if ( is_array($vec_messages) )
        {
            $this->vec_messages[$type] = \CMap::mergeArray($this->vec_messages[$type], $vec_messages);
        }
        else
        {
            $this->vec_messages[$type][] = $vec_messages;
        }
    }
}
