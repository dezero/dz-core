<?php
/**
 * Warning manager trait class
 *
 * @see dz\traits\ErrorTrait
 */

namespace dz\traits;

trait WarningTrait
{
    /**
     * Registered warnings
     */
    public $vec_warnings = [];


    /**
     * @return array
     */
    public function get_warnings()
    {
        return $this->vec_warnings;
    }


    /**
     * Add warning(s)
     */
    public function add_warning($vec_warnings)
    {
        if ( is_array($vec_warnings) )
        {
            $this->vec_warnings = \CMap::mergeArray($this->vec_warnings, $vec_warnings);
        }
        else
        {
            $this->vec_warnings[] = $vec_warnings;
        }
    }
}
