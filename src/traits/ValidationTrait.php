<?php
/**
 * Validation component trait class
 */

namespace dz\traits;

use dz\helpers\Log;
use dz\helpers\StringHelper;
use Yii;

trait ValidationTrait
{
    /**
     * Attributes with errors
     */
    private $vec_error_attributes = [];


    /**
     * Validation rules
     */
    private $vec_rules;


    /**
     * Array with form data
     */
    private $vec_form_data;


    /**
     * @return bool
     */
    /*
    public function run()
    {
        // \DzLog::dev($this->vec_form_data);
        // $this->add_error('DEBUG HERE');

        // Load validation rules
        $this->load_rules();

        // Apply validation rules
        return $this->apply_rules();
    }
    */


    /**
     * @return array
     */
    public function get_attributes_errors()
    {
        return array_values($this->vec_error_attributes);
    }


    /**
     * Check if an attribute has an error
     */
    private function is_attribute_error($attribute_name)
    {
        return isset($this->vec_error_attributes[$attribute_name]);
    }


    /**
     * Load validation rules
     */
    private function load_rules()
    {
        $this->vec_rules = [];
    }


    /**
     * Apply validation rules
     */
    private function apply_rules()
    {
        $is_validated = true;

        if ( !empty($this->vec_rules) )
        {
            foreach ( $this->vec_rules as $rule_name => $vec_data )
            {
                $vec_rule_name = explode(":", $rule_name);
                if ( !empty($vec_rule_name) )
                {
                    // Get method name & check if it exists
                    $validation_method = 'validate_'. $vec_rule_name[0];
                    if ( method_exists($this, $validation_method) )
                    {
                        $vec_extra_params = [];

                        // Extra params
                        if ( count($vec_rule_name) > 1 )
                        {
                            unset($vec_rule_name[0]);
                            $vec_extra_params = array_values($vec_rule_name);
                        }

                        // Execute validation rule
                        if ( ! $this->validate($validation_method, $vec_data, $vec_extra_params) )
                        {
                            $is_validated = false;
                        }
                    }
                }
            }
        }

        return $is_validated;
    }


    /**
     * Execute validation rule
     */
    private function validate($validation_method, $vec_data, $vec_params = [])
    {
        // Default error message
        if ( ! isset($vec_data['message']) || empty($vec_data['message']) )
        {
            $vec_data['message'] = '{attribute} és incorrecte';
        }

        if ( !empty($vec_data) && isset($vec_data[0]) && is_array($vec_data[0]) )
        {
            $is_full_validated = true;
            foreach ( $vec_data[0] as $attribute_name )
            {
                if ( isset($this->vec_form_data[$attribute_name]) && ! $this->is_attribute_error($attribute_name) )
                {
                    // Call validation method dynamically
                    $is_validated = call_user_func_array([$this, $validation_method], [$attribute_name, $vec_params]);
                    if ( ! $is_validated )
                    {
                        $this->vec_error_attributes[$attribute_name] = $attribute_name;
                        $error_message = Yii::t('frontend', $vec_data['message'], ['{attribute}' => Yii::app()->diagnosisManager->attributeLabel($attribute_name)]);
                        if ( isset($vec_data['message_prefix']) )
                        {
                            $error_message = $vec_data['message_prefix'] . $error_message;
                        }
                        $this->add_error($error_message);
                        $is_full_validated = false;
                    }
                }
            }

            return $is_full_validated;
        }

        return true;
    }


    /**
     * Apply REQUIRED validation rule
     */
    private function validate_required($attribute_name, $vec_params = [])
    {
        $attribute_value = StringHelper::trim($this->vec_form_data[$attribute_name]);
        return $attribute_value !== '';
    }


    /**
     * Apply MIN validation rule
     */
    private function validate_min($attribute_name, $vec_params = [])
    {
        if ( !empty($vec_params) && isset($vec_params[0]) )
        {
            $min_value = (float)$vec_params[0];
            $attribute_value = (float)$this->vec_form_data[$attribute_name];
            return $attribute_value >= $min_value;
        }

        return true;
    }



    /**
     * Apply MAX validation rule
     */
    private function validate_max($attribute_name, $vec_params = [])
    {
        if ( !empty($vec_params) && isset($vec_params[0]) )
        {
            $max_value = (float)$vec_params[0];
            $attribute_value = (float)$this->vec_form_data[$attribute_name];
            return $attribute_value <= $max_value;
        }

        return true;
    }


    /**
     * Apply REQUIRED_IF validation rule
     */
    private function validate_required_if($attribute_name, $vec_params = [])
    {
        if ( !empty($vec_params) && isset($vec_params[0]) )
        {
            $required_attribute_name = $vec_params[0];
            if ( isset($this->vec_form_data[$required_attribute_name]) )
            {
                $if_attribute_value = StringHelper::trim($this->vec_form_data[$required_attribute_name]);
                $current_attribute_value = StringHelper::trim($this->vec_form_data[$attribute_name]);

                // Boolean?
                if ( preg_match("/^is\_/", $required_attribute_name) )
                {
                    return $if_attribute_value == '0' || $current_attribute_value !== '';
                }
                return $if_attribute_value === '' || $current_attribute_value !== '';
            }
        }

        return true;
    }
}
