<?php
/**
 * Error manager trait class
 *
 * @see dz\traits\FlashMessageTrait
 */

namespace dz\traits;

trait ErrorTrait
{
    /**
     * Registered errors
     */
    public $vec_errors = [];


    /**
     * @return array
     */
    public function get_errors()
    {
        return $this->vec_errors;
    }


    /**
     * Add error(s)
     */
    public function add_error($vec_errors)
    {
        if ( is_array($vec_errors) )
        {
            $this->vec_errors = \CMap::mergeArray($this->vec_errors, $vec_errors);
        }
        else
        {
            $this->vec_errors[] = $vec_errors;
        }
    }
}
