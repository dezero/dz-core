<?php
/**
 * CheckBoxColumn class file
 *
 * CheckBoxColumn is the base class for select a row with 
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2020 Dezero
 */

namespace dz\grid;

use dz\helpers\Html;
use Yii;

Yii::import('zii.widgets.grid.CCheckBoxColumn');

class CheckBoxColumn extends \CCheckBoxColumn
{
    /**
     * Checkbox name attribute
     */
    public $checkbox_name;


    /**
     * Checkbox id attribute
     */
    public $checkbox_id;


    /**
     * Override CCheckBoxColumn::init() method
     * 
     * Initializes the column.
     * This method registers necessary client script for the checkbox column.
     */
    public function init()
    {
        // Name attribute
        if ( isset($this->checkBoxHtmlOptions['name']) )
        {
            $this->checkbox_name = $this->checkBoxHtmlOptions['name'];
        }
        else
        {
            $this->checkbox_name = $this->grid->id .'-checkbox[]';
            $this->checkBoxHtmlOptions['name'] = $this->checkbox_name;
        }

        // Id attribute
        if ( isset($this->checkBoxHtmlOptions['id']) )
        {
            $this->checkbox_id = $this->checkBoxHtmlOptions['id'];
        }
        else
        {
            $this->checkbox_id = strtr($this->checkbox_name, ['[]' => '']);
            $this->checkBoxHtmlOptions['id'] = $this->checkbox_id;
        }

        // Class attribute
        if ( isset($this->checkBoxHtmlOptions['class']) )
        {
            $this->checkBoxHtmlOptions['class'] .= ' checkbox-gridview';
        }
        else
        {
            $this->checkBoxHtmlOptions['class'] = 'checkbox-gridview';
        }
    }


    /**
     * Override CCheckBoxColumn::getFilterCellContent() method
     * 
     * Returns the filter cell content.
     * The default implementation simply returns an empty column.
     * This method may be overridden to customize the rendering of the filter cell (if any).
     * 
     * @return string the filter cell content.
     * @since 1.1.16
     */
    /*
    public function getFilterCellContent()
    {
        // Render checkbox
        $vec_options = $this->checkBoxHtmlOptions;

        $name = $vec_options['name'];
        unset($vec_options['name']);

        // $vec_options['id'] = $this->id .'_'. $row;
        $vec_options['id'] = $this->checkbox_id .'-all';
        
        $output = Html::checkBox($name, false, $vec_options);

        return '<div class="checkbox-custom checkbox-primary checkbox-lg">'. $output .'<label for="'. $vec_options['id'] .'"></label></div>';
    }
    */



    /**
     * Override CCheckBoxColumn::getFilterCellContent() method
     * 
     * Returns the header cell content.
     * This method will render a checkbox in the header when {@link selectableRows} is greater than 1
     * or in case {@link selectableRows} is null when {@link CGridView::selectableRows} is greater than 1.
     * @return string the header cell content.
     * @since 1.1.16
     */
    public function getHeaderCellContent()
    {
        if ( trim($this->headerTemplate) === '' )
        {
            return $this->grid->blankDisplay;
        }

        // Render checkbox
        $vec_options = [
            'class' => 'checkbox-gridview-all',
            'id'    => $this->checkbox_id .'-all',
        ];

        $output = Html::checkBox( $this->checkbox_id .'-all', false, $vec_options);

        return '<div class="checkbox-custom checkbox-primary checkbox-lg">'. $output .'<label for="'. $vec_options['id'] .'"></label></div>';
    }


    /**
     * Override CCheckBoxColumn::getDataCellContent() method
     * 
     * Returns the data cell content.
     * This method renders a checkbox in the data cell.
     * 
     * @param integer $row the row number (zero-based)
     * @return string the data cell content.
     * @since 1.1.16
     */
    public function getDataCellContent($row)
    {
        $data = $this->grid->dataProvider->data[$row];
        
        if ( $this->value !== null )
        {
            $value = $this->evaluateExpression($this->value, ['data'=>$data,'row'=>$row]);
        }
        elseif ( $this->name !== null )
        {
            $value = Html::value($data, $this->name);
        }
        else
        {
            $value = $this->grid->dataProvider->keys[$row];
        }

        $is_checked = false;
        if ( $this->checked !== null )
        {
            $is_checked = $this->evaluateExpression($this->checked, ['data'=>$data,'row'=>$row]);
        }

        // Render checkbox
        $vec_options = $this->checkBoxHtmlOptions;
        if ( $this->disabled !== null )
        {
            $vec_options['disabled'] = $this->evaluateExpression($this->disabled, ['data'=>$data,'row'=>$row]);
        }

        $name = $vec_options['name'];
        unset($vec_options['name']);

        $vec_options['value'] = $value;
        // $vec_options['id'] = $this->id .'_'. $row;
        $vec_options['id'] = $this->checkbox_id .'-'. $row;
        
        $output = Html::checkBox($name, $is_checked, $vec_options);

        return '<div class="checkbox-custom checkbox-primary checkbox-lg">'. $output .'<label for="'. $vec_options['id'] .'"></label></div>';
    }
}