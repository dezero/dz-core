<?php
/**
 * Pager class file
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2017 Dezero
 */

namespace dz\grid;

use dz\helpers\Html;
use Yii;

// Yii::import('zii.widgets.grid.CDataColumn');
Yii::import('@bootstrap.widgets.BsPager');

class Pager extends \BsPager
{
	/**
     * Runs the widget.
     */
    public function run()
    {
        $links = $this->createPageLinks();
        if ( !empty($links) )
        {
            echo Html::pagination($links, $this->htmlOptions);
        }
    }


	/**
     * Creates a page link.
     * @param string $label the link label text.
     * @param integer $page the page number.
     * @param boolean $disabled whether the link is disabled.
     * @param boolean $active whether the link is active.
     * @return string the generated link.
     */
    protected function createPageLink($label, $page, $disabled, $active)
    {
        return [
            'label'         => $label,
            'url'           => $this->createPageUrl($page),
            'disabled'      => $disabled,
            'active'        => $active,
            'htmlOptions'   => ['class' => 'page-item']
            // 'activeLabelSrOnly' => $this->activeLabelSrOnly,
        ];
    }
}
