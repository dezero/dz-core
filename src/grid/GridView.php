<?php
/**
 * GridView widget class file
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2012 Dezero
 */

namespace dz\grid;

use CClientScript;
use dz\db\ActiveRecord;
use dz\grid\DataColumn as DataColumn;
use dz\helpers\Html;
use Yii;

// Import core CGridView
Yii::import('zii.widgets.grid.CGridView');
Yii::import('@bootstrap.widgets.BsGridView');

/**
 * Dezero Zii grid view.
 */
class GridView extends \BsGridView
{
    /**
     * @var array the configuration for the pager.
     */
    public $pager = ['class' => '\dz\grid\Pager'];
    
    
    /**
     * Sortable AJAX action URL where save new sortable fields
     *
     * @var string
     */
    public $sortableAjaxAction;
    
    
    /**
     * Load modal widget?
     *
     * @var bool
     */
    public $loadModal = false;

    public $afterTableRow;


    /**
     * Header column fixed?
     *
     * @var bool
     */
    public $headerFixed = false;


    /**
     * Header prefix HTML
     *
     * @var HTML
     */
    public $beforeHeaderRow;


    /**
     * The template to be used to control the layout of various sections in the view.
     * These tokens are recognized: {summary}, {items} and {pager}.
     *
     * @var string
     */
    // public $template = '{items}{pager}{summary}';
     
    public $type = 'striped bordered hover';


    /**
     * @var bool    Use Bootstrap tooltip widget?
     */
    public $enableTooltip = true;


    /**
     * @var bool    Use select2 widget on filters?
     */
    public $enableSelect2 = true;


    /**
     * @var bool    Use bootbox library thrown .dz-bootbox-confirm class
     */
    public $enableBootbox = true;
    

    /**
     * Initializes the widget.
     */
    public function init()
    {
        // Override "jquery.yiigridview.js"
        if ( $this->baseScriptUrl === null )
        {
            $this->baseScriptUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('@dz.grid.assets.js'));
        }

        // CGridView::init();
        parent::init();
        
        // Loading
        // $js_loading_after = "if (jQuery('body').children('.modal-scrollable').length > 0 ) { jQuery('body').modalmanager('loading'); } ";

        // jQuery CGridView ID selector
        $js_grid_id = "#". $this->id;


        // Header fixed
        if ( $this->headerFixed )
        {
            $js_headerFixed = "jQuery('{$js_grid_id} .table').dzHeaderFixed(); ";
            Yii::app()->clientScript->registerScript(__CLASS__.'#header-fixed-'. $this->id, $js_headerFixed , CClientScript::POS_READY);
        }

        // $js_code = $js_loading_after;
        $js_code = ' ';

        // Tooltip
        if ( $this->enableTooltip && ( ! isset($this->afterAjaxUpdate) || ( isset($this->afterAjaxUpdate) && !preg_match("/tooltip/", $this->afterAjaxUpdate) ) ) ) 
        {
            $js_code .= "jQuery('{$js_grid_id} [data-toggle=\"tooltip\"]').tooltip('hide'); jQuery('{$js_grid_id} [data-toggle=\"tooltip\"]').tooltip();";
        }

        // Bootbox
        if ( $this->enableBootbox )
        {
            $js_code .= "jQuery('{$js_grid_id} .dz-bootbox-confirm').dzBootbox();";
        }

        // Javascript Select2
        if ( $this->filter )
        {
            $js_select2 = '';
            if ( $this->enableSelect2 )
            {
                $js_select2 .= "jQuery('{$js_grid_id} table thead select').select2({allowClear: false, placeholder: '". Yii::t('app', '- All -') ."'}); ";
                $js_select2 .= "jQuery('{$js_grid_id}-search .field-wrapper select').select2({allowClear: true}); ";
                Yii::app()->clientScript->registerScript($js_grid_id .'-cgridview_filter_select2', $js_select2, CClientScript::POS_READY);
            }

            // Javascript clear input text filters
            $js_clear  = "jQuery('{$js_grid_id} .filters .dz-search-choice-close, {$js_grid_id}-search .search-filters .dz-search-choice-close').on('click', function() { $(this).prev('input').val('').change(); });";
            $js_clear .= "if (jQuery('{$js_grid_id} .filters .dz-search-choice-close').size() == 0){ jQuery('{$js_grid_id} .filters input[type=text]').not('.select2-container input[type=text]').after('<abbr class=\"dz-search-choice-close\"></abbr>'); }";
            $js_clear .= "if (jQuery('{$js_grid_id}-search .search-filters .dz-search-choice-close').size() == 0){ jQuery('{$js_grid_id}-search .search-filters input[type=text]').not('.select2-container input[type=text]').after('<abbr class=\"dz-search-choice-close\"></abbr>'); }";
            Yii::app()->clientScript->registerScript($js_grid_id .'-cgridview_filter_clear', $js_clear, CClientScript::POS_READY);      

            // Javascript for "Filtro/s Seleccionado/s" in Advanced Search
            $js_code .= $js_select2. $js_clear; // . $js_filters;

            // Header fixed
            if ( $this->headerFixed )
            {
                $js_code .= "jQuery('{$js_grid_id} .table').dzHeaderFixed(); ";
            }
        }
        else if ( $this->enableSelect2 )
        {
            $js_select2 = "jQuery('{$js_grid_id}-search .field-wrapper select').select2({allowClear: true}); ";
            Yii::app()->clientScript->registerScript($js_grid_id.'-cgridview_filter_select2', $js_select2, CClientScript::POS_READY);
            $js_code .= $js_select2;
        }

        // After AJAX update
        if ( !isset($this->afterAjaxUpdate) )
        {
            $this->afterAjaxUpdate = "js:function() {". $js_code ."}";
        }
        else
        {
            $this->afterAjaxUpdate = str_replace("js:function() {", "js:function() {" . $js_code, $this->afterAjaxUpdate);
        }
    }
    
    
    /**
     * Renders a table body row
     *
     * @param integer $row the row number (zero-based).
     */
    public function renderTableRow($row)
    {
        $data = $this->dataProvider->data[$row];
        if( $this->rowCssClassExpression !== null )
        {
            $class = $this->evaluateExpression($this->rowCssClassExpression, ['row' => $row, 'data' => $data]);
        }
        else if ( is_array($this->rowCssClass) && ( $n = count($this->rowCssClass) ) > 0 )
        {
            $class = $this->rowCssClass[$row%$n];
        }
        else
        {
            $class='';
        }
        
        // Custom row attributes        
        $class .= ' row-'.$row;
        
        
        /*
        |--------------------------------------------------------------------------
        | Attributes for SORTABLE
        |--------------------------------------------------------------------------
        */
        $vec_row_options = ['class' => $class];

        // Add data-model-id attribute
        /*
        if ( !empty($data) && is_object($data) && get_class($data) != 'CAuthItem' )
        {
            $model_id = ActiveRecord::extractPkValue($data, true);
            if ( is_string($model_id) )
            {
                $vec_row_options['data-model-id'] = $model_id;
            }
        }
        */

        // Print row tag
        echo Html::openTag('tr', $vec_row_options);
        
        // print columns
        foreach ($this->columns as $column)
        {
            $column->renderDataCell($row);
        }
        echo "</tr>\n";


        /*
        |--------------------------------------------------------------------------
        | AfterTableRow event (dezero custom event)
        |--------------------------------------------------------------------------
        */      
        if ( $this->afterTableRow )
        {
            $_expression_ = $this->afterTableRow['value'];
            if ( ! preg_match("/return/", $_expression_) )
            {
                $_expression_ = 'return ('. $_expression_ .')';
            }
            $_output_expression_ = eval($_expression_ . ';');
            if ( !empty($_output_expression_) )
            {
                echo "<tr class='". $class ." extra-row extra-row-". $row ."'><td class='extra-row-column' colspan='". count($this->columns) ."'>". $_output_expression_ ."</td></tr>\n";
            }
        }
    }


    /**
     * Renders the view.
     * This is the main entry of the whole view rendering.
     * Child classes should mainly override {@link renderContent} method.
     * 
     * @see CBaseListView::run()
     */
    public function run()
    {
        $this->registerClientScript();

        echo "<div id=\"{$this->id}-container\" class=\"dz-grid-container\">\n";
        echo Html::openTag($this->tagName,$this->htmlOptions) ."\n";
        echo "<div class=\"dz-loader-overlay\"><div class=\"dz-loader loader loader-circle\"></div></div>\n";

        $this->renderContent();
        $this->renderKeys();

        echo Html::closeTag($this->tagName);

        echo '</div>';
    }
    
    
    /**
     * Renders the table footer.
     */
    public function renderTableFooter()
    {
        $hasFilter = $this->filter!==null && $this->filterPosition===self::FILTER_POS_FOOTER;
        $hasFooter = $this->getHasFooter();
        if ( $hasFilter || $hasFooter )
        {
            echo "<tfoot>\n";
            if ($hasFooter)
            {
                echo "<tr>\n";
                foreach($this->columns as $column)
                {
                    $column->renderFooterCell();
                }
                echo "</tr>\n";
            }
            if ($hasFilter)
            {
                $this->renderFilter();
            }

            echo "</tfoot>\n";
        }
    }
    
    
    /**
     * Creates a {@link DataColumn} based on a shortcut column specification string
     *
     * @param string $text the column specification string
     * @return DataColumn the column instance
     */
    protected function createDataColumn($text)
    {
        /*
        |-------------------------------------------------
        |   Code from "Yii/zii/widgets/grid/CGridView"
        |-------------------------------------------------
        */
        if ( ! preg_match('/^([\w\.]+)(:(\w*))?(:(.*))?$/',$text,$matches) )
        {
            throw new \CException(Yii::t('zii','The column must be specified in the format of "Name:Type:Label", where "Type" and "Label" are optional.'));
        }
            
        $column = new DataColumn($this);
        
        $column->name = $matches[1];
        if ( isset($matches[3]) && $matches[3]!=='' )
        {
            $column->type = $matches[3];
        }
        
        if ( isset($matches[5]) )
        {
            $column->header = $matches[5];
        }
        
        return $column;
        /*
        |-------------------------------------------------
        |   Code from "Yii/zii/widgets/grid/CGridView"
        |-------------------------------------------------
        */
    }
    

    /**
     * Creates column objects and initializes them.
     */
    protected function initColumns()
    {
        /*
        |-------------------------------------------------
        |   Code from "Yii/zii/widgets/grid/CGridView"
        |-------------------------------------------------
        */
        if ( $this->columns === [] )
        {
            if ( $this->dataProvider instanceof \CActiveDataProvider )
            {
                $this->columns = $this->dataProvider->model->attributeNames();
            }
            else if($this->dataProvider instanceof \IDataProvider)
            {
                // use the keys of the first row of data as the default columns
                $data = $this->dataProvider->getData();
                if ( isset($data[0]) && is_array($data[0]) )
                {
                    $this->columns = array_keys($data[0]);
                }
            }
        }
        /*
        |-------------------------------------------------
        |   Code from "Yii/zii/widgets/grid/CGridView"
        |-------------------------------------------------
        */

        // For array columns, set "DataColumn" as default column class
        foreach ( $this->columns as $id_column => $que_column )
        {
            if ( is_array($que_column) )
            {
                if ( !isset($que_column['class']) )
                {
                    $this->columns[$id_column]['class'] = '\dz\grid\DataColumn';
                }

                // Widget namespace conversion --> Transform from "dz.grid.ButtonColumn" to "\dz\grid\ButtonColumn"
                else if ( preg_match("/^dz\./", $que_column['class']) )
                {
                    $this->columns[$id_column]['class'] = '\\'. str_replace('.', '\\', $que_column['class']);
                }
            }
        }
        parent::initColumns();
    }
    

    /**
     * Registers necessary client scripts.
     */
    public function registerClientScript()
    {   
        // Get assets path for this extension
        $dz_ext_path = realpath(dirname(__FILE__) . '/..');
        
        // ClientScript to register Javascript & CSS
        $cs = Yii::app()->getClientScript();

        // Loading class (with Modal)
        $loading_assets_url = Yii::app()->assetManager->publish(Yii::getAlias('@core.src.widgets.assets.modal'));
        
        return parent::registerClientScript();
    }


    /**
     * Renders the summary text.
     */
    public function renderSummary()
    {
        switch ( get_class($this->dataProvider) )
        {
            case 'CArrayDataProvider':
            case '\CArrayDataProvider':
                $que_model = call_user_func([$this->dataProvider->id, 'model']);
                $selected_fiters = $que_model->selectedFilter();
                break;

            default:
                $selected_fiters = $this->dataProvider->model->selectedFilter();
                break;
        }

        if ( $this->filter )
        {
            echo Html::hiddenField('selected_filter', $selected_fiters, [
                'class' => 'selected-filter',
                'id' => $this->id.'-selected-filter'
            ]);
        }

        return parent::renderSummary();
    }


    /**
     * Renders the table body.
     */
    function renderTableBody()
    {
        // Load modal just one time
        // if ( $this->loadModal && ! Yii::app()->getClientScript()->isScriptRegistered(__CLASS__.'#dz-modal-'. $this->id,  CClientScript::POS_READY) )
        if ( !Yii::app()->getRequest()->getIsAjaxRequest() && $this->loadModal && !Yii::app()->getClientScript()->isScriptRegistered(__CLASS__.'#dz-modal-'. $this->id,  CClientScript::POS_READY) )
        {
            Yii::app()->controller->beginWidget('dz.widgets.Modal', [
                'id' => 'dz-modal-'. $this->id,
                'runJavascript' => false,
                'htmlOptions' => [
                    'aria-hidden' => 'true',
                ]
            ]);
            
            // Modal HTML code
            echo '<div class="modal-header">
                    <a class="close" data-dismiss="modal">&times;</a>
                    <h4 class="dz-modal-title"></h4>
                    <div class="dz-modal-subtitle"></div>
                </div>
                
                <div class="dz-modal-body modal-body">
                    <p>'. Yii::t('app', 'Loading...') .'</p>
                </div>

                <div class="dz-modal-footer modal-footer">';

            // View button
            Yii::app()->controller->widget('@bootstrap.widgets.TbButton', [
                'label'     => Yii::t('app', 'More details'),
                'htmlOptions' => [
                    'class' => 'dz-modal-view-button dz-modal-button hide',
                    'data-url' => '',
                    'target' => '_blank'
                ]
            ]);

            // Update button
            Yii::app()->controller->widget('@bootstrap.widgets.TbButton', [
                'label'     => Yii::t('app', 'Update'),
                'htmlOptions' => [
                    'class' => 'dz-modal-update-button dz-modal-button hide',
                    'data-url' => '',
                    'target' => '_blank'
                ]
            ]);

            // Save/submit button
            Yii::app()->controller->widget('@bootstrap.widgets.TbButton', [
                'label'     => Yii::t('app', 'Save'),
                'htmlOptions' => [
                    'class' => 'dz-modal-submit-button dz-modal-button btn-primary hide',
                    'onclick' => 'jQuery("#dz-modal-'. $this->id .' .dz-modal-body form").submit();'
                ]
            ]);

            // Close button
            Yii::app()->controller->widget('@bootstrap.widgets.TbButton', [
                'label' => Yii::t('app', 'Close'),
                // 'type' => 'inverse',
                'url' => '#',
                'htmlOptions' => [
                    'data-dismiss' => 'modal',
                    'aria-hidden'  => "true"
                ],
            ]);

            echo '</div>';
            Yii::app()->controller->endWidget();
            Yii::app()->getClientScript()->registerScript(__CLASS__.'#dz-modal-'. $this->id, "jQuery('#dz-modal-". $this->id ."').appendTo(document.body).modal({show: false});", CClientScript::POS_READY);
        }
        return parent::renderTableBody();
    }


    /**
     * Renders the data items for the grid view.
     */
    public function renderItems()
    {
        if ( $this->dataProvider->getItemCount() > 0 || $this->showTableOnEmpty )
        {
            // echo "<div class='table-responsive'>";
            parent::renderItems();
            // echo "</div>";
        }
        else
        {
            parent::renderItems();
        }
    }


    /**
     * Renders the table header.
     */
    public function renderTableHeader()
    {
        if( !$this->hideHeader )
        {
            echo "<thead>\n";

            if ( $this->filterPosition === self::FILTER_POS_HEADER )
            {
                $this->renderFilter();
            }

            if ( !empty($this->beforeHeaderRow) )
            {
                echo $this->beforeHeaderRow;
            }

            echo "<tr>\n";
            foreach($this->columns as $column)
                $column->renderHeaderCell();
            echo "</tr>\n";

            if ( $this->filterPosition === self::FILTER_POS_BODY )
                $this->renderFilter();

            echo "</thead>\n";
        }
        
        elseif ( $this->filter!==null && ($this->filterPosition===self::FILTER_POS_HEADER || $this->filterPosition===self::FILTER_POS_BODY) )
        {
            echo "<thead>\n";
            $this->renderFilter();
            echo "</thead>\n";
        }
    }


    /**
     * Renders the filter
     * 
     * @since 1.1.1
     */
    public function renderFilter()
    {
        if ( $this->filter !== null )
        {
            echo "<tr id=\"{$this->id}-filters\" class=\"{$this->filterCssClass}\">\n";
            foreach ( $this->columns as $column )
            {
                $column->renderFilterCell();
            }
            echo "</tr>\n";
        }
    }
}
