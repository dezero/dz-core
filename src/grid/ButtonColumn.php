<?php
/**
 * ButtonColumn class file
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2012 Dezero
 */

namespace dz\grid;

use CJavaScript;
use dz\helpers\Html;
use dz\helpers\Url;
use Yii;

Yii::import('@bootstrap.widgets.TbButtonColumn');

class ButtonColumn extends \TbButtonColumn
{
    /**
     * The actions button icon
     *
     * @var string
     */
    public $menuAction = [];


    /**
     * Template buttons list with actions button
     *
     * @var string
     */
    public $templateWithAction = '{view}{update}{actions}';


    /**
     * @var string the view button icon (defaults to 'eye-open').
     */
    public $viewButtonIcon = 'eye';


    /**
     * @var string the update button icon (defaults to 'pencil').
     */
    public $updateButtonIcon = 'edit';


    /**
     * @var string the delete button icon (defaults to 'trash-o').
     */
    public $deleteButtonIcon = 'trash';


    /**
     * Use clear button?
     *
     * @var bool
     */
    public $clearButton = false;


    /**
     * Use view button?
     *
     * @var bool
     */
    public $viewButton = true;


    /**
     * Use update button?
     *
     * @var bool
     */
    public $updateButton = true;


    /**
     * Use delete button?
     *
     * @var bool
     */
    public $deleteButton = false;


    /**
     * Fixed text
     *
     * @var array
     */
    public $fixedText;


    /**
     * Show "destination" query parameter in URLs
     *
     * @var bool
     */
    public $showDestinationUrl = false;


    /**
     * Array with clear button Options:
     *
     *  + 'visible'    : a PHP expression for determining whether the button is visible
     *
     *  + 'beforeClear': JS code to be invoked when the button is clicked, this is invoked before clearing the form fields
     *                   Returning false from this code fragment prevents the AJAX to be executed. Only use 'return' block when you want to stop further steps execution.
     *
     *  + 'afterClear' : JS code to be invoked when the button is clicked, this is invoked after clearing the form fields, before AJAX
     *                   Returning false from this code fragment prevents the AJAX to be executed. Only use 'return' block when you want to stop further steps execution.
     *
     *  + 'htmlOptions': Associative array of html elements to be passed for the button.
     *                   Default is: array('class'=>'clear','id'=>'cbcwr_clear','style'=>'text-align:center;display:block;');
     *
     *  + 'imageUrl'   : image URL of the button. If not set or false, a text link is used.
     *                   Default is: $this->grid->baseScriptUrl.'/delete.png'
     *
     *  + 'icon'       : Bootstrap name icon.
     *                   Default is: remove
     *
     *  + 'url'        : a PHP expression for generating the URL of the button
     *                   Default is: Yii::app()->controller->createUrl(Yii::app()->controller->action->ID,["clearFilters"=>1])
     *
     *  + 'label'      : Label tag to be used on the button when no URL is given.
     *                   Default is: Clear Filters
     *
     * @var array
     */
    public $clearOptions = [];

    
    /**
     * Filter HTML content
     *
     * @var string
     */
    public $filterHtmlContent;


    /**
     * Private member to store internally the button definition
     *
     * @var array
     */
    private $_clearButton;


    /**
     * Private member to store as a backup the template usage.
     *
     * @var string
     */
    protected $_templateBackup;


    /**
     * Initializes the default buttons (view, update, delete & action).
     */
    protected function initDefaultButtons()
    {
        // parent::initDefaultButtons()

        // <CODE> Code from CButtonColumn and TbButtonColumn        
        if ( $this->viewButtonLabel === null )
        {
            $this->viewButtonLabel = Yii::t('zii','View');
        }
        
        if ( $this->updateButtonLabel === null )
        {
            $this->updateButtonLabel = Yii::t('zii','Update');
        }
        
        if ( $this->deleteButtonLabel === null )
        {
            $this->deleteButtonLabel = Yii::t('zii','Delete');
        }
        
        if ( $this->viewButtonImageUrl === null )
        {
            $this->viewButtonImageUrl = $this->grid->baseScriptUrl.'/view.png';
        }
        
        if ( $this->updateButtonImageUrl === null )
        {
            $this->updateButtonImageUrl = $this->grid->baseScriptUrl.'/update.png';
        }
        
        if ( $this->deleteButtonImageUrl === null )
        {
            $this->deleteButtonImageUrl = $this->grid->baseScriptUrl.'/delete.png';
        }
        
        if ( $this->deleteConfirmation === null )
        {
            $this->deleteConfirmation = Yii::t('zii','Are you sure you want to delete this item?');
        }


        foreach ( ['view','update','delete'] as $id )
        {
            $button = [
                'label'     => $this->{$id.'ButtonLabel'},
                'url'       => $this->{$id.'ButtonUrl'},
                'imageUrl'  => $this->{$id.'ButtonImageUrl'},
                'options'   => $this->{$id.'ButtonOptions'},
            ];
            
            if ( isset($this->buttons[$id]) )
            {
                $this->buttons[$id] = \CMap::mergeArray($button, $this->buttons[$id]);
            }
            else
            {
                $this->buttons[$id] = $button;
            }
        }

        if ( $this->viewButtonIcon !== false && !isset($this->buttons['view']['icon']) )
        {
            $this->buttons['view']['icon'] = $this->viewButtonIcon;
        }

        if ( $this->updateButtonIcon !== false && !isset($this->buttons['update']['icon']) )
        {
            $this->buttons['update']['icon'] = $this->updateButtonIcon;
        }

        if ( $this->deleteButtonIcon !== false && !isset($this->buttons['delete']['icon']) )
        {
            $this->buttons['delete']['icon'] = $this->deleteButtonIcon;
        }

        // Custom DELETE button
        $this->buttons['delete']['confirm'] = $this->deleteConfirmation;
        $this->buttons['delete']['options']['data-gridview'] = $this->grid->id;

        if ( !empty($this->menuAction) )
        {
            $this->template = $this->templateWithAction;
            $this->buttons['actions'] = [
                'label' => Yii::t('app', 'Actions'),
                'options' => [
                    'class' => 'actions dropdown-toggle',
                    'data-toggle' => 'dropdown'
                ],
            ];

            // menuAction attribute as array. Useful when you need to set "visible" attribute value
            if ( is_array($this->menuAction) && isset($this->menuAction['value']) )
            {
                if ( isset($this->menuAction['visible']) )
                {
                    $this->buttons['actions']['visible'] = $this->menuAction['visible'];
                }
                $this->menuAction = $this->menuAction['value'];
            }
            
            // Delete button enabled?
            if ( $this->deleteButton )
            {
                $this->template = '{view}{update}{delete}{actions}';
            }

            // Disable view link?
            if ( $this->viewButton === false )
            {
                $this->template = str_replace('{view}', '', $this->template);
            }

            // Disable update link?
            if ( $this->updateButton === false )
            {
                $this->template = str_replace('{update}', '', $this->template);
            }
        }

        // Check access
        $operation_name = '';
        if ( ($module = Yii::app()->controller->getModule()) !== null )
        {
            $operation_name = $module->getId() .".";
        }
        $operation_name .= Yii::app()->controller->getId();

        if ( $this->viewButton && !isset($this->buttons['view']['visible']) )
        {
            $this->buttons['view']['visible'] = "Yii::app()->user->checkAccess('". $operation_name .".view')";
        }
        
        if ( $this->updateButton && !isset($this->buttons['update']['visible']) )
        {
            $this->buttons['update']['visible'] = "Yii::app()->user->checkAccess('". $operation_name .".update')";
        }

        if ( $this->deleteButton && !isset($this->buttons['delete']['visible']) )
        {
            $this->buttons['delete']['visible'] = "Yii::app()->user->checkAccess('". $operation_name .".delete')";
        }
        
        // Clear button (based on EButtonColumnWithClearFilters extension --> http://www.yiiframework.com/forum/index.php?/user/8824-pentium10/)
        if ( $this->clearButton )
        {
            $clearOptions = \CMap::mergeArray([
                'beforeClear' => '',
                'afterClear' => '',
                'visible' => true,
                'htmlOptions' => [
                    'class' => 'clear',
                    'id'    => 'cbcwr_clear',
                    'style' => 'text-align:center;display:block;'
                ],
                'url' => 'Yii::app()->controller->createUrl(Yii::app()->controller->action->ID,["clearFilters"=>1])',
                'imageUrl' => $this->grid->baseScriptUrl.'/delete.png',
                'icon' => 'close',
                'label' => Yii::t('app', 'Clear filters')
            ], $this->clearOptions);
            
            // Handle custom JS setup
            if ( !empty($clearOptions['beforeClear']) )
            {
                $clearOptions['beforeClear'] .= ';';
            }
            if ( !empty($this->clearOptions['afterClear']) )
            {
                $clearOptions['afterClear'] .= ";\r\n";
            }
    
            // Turn custom setup into representative output
            $clearOptions['click'] = "js:function() {{$clearOptions['beforeClear']} return $.dzGridView.clearFields('". $this->grid->id ."') }";
            $clearOptions['visible'] = is_bool($clearOptions['visible']) ? ( ($clearOptions['visible']) ? 'true' : 'false') : $clearOptions['visible'];
            

            // Define the button structure to be used
            $this->_clearButton = [
                'label'     => $clearOptions['label'],       // text label of the button
                'url'       => $clearOptions['url'],         // a PHP expression for generating the URL of the button
                'icon'      => $clearOptions['icon'],        // Bootstrap icon 
                'imageUrl'  => $clearOptions['imageUrl'],    // image URL of the button. If not set or false, a text link is used
                'options'   => $clearOptions['htmlOptions'], // HTML options for the button tag
                'visible'   => $clearOptions['visible'],     // a PHP expression for determining whether the button is visible
                'click'     => $clearOptions['click'],       // a JS function to be invoked when the button is clicked
            ];
            $this->buttons = \CMap::mergeArray(
                $this->buttons,
                ['clear' => $this->_clearButton]
            );

            $this->_templateBackup = $this->template;
    
            $this->template .= "{clear}";
        }
    }
    
    
    /**
     * Renders the filter cell
     */
    public function renderFilterCell()
    {
        if ( $this->clearButton )
        {
            // Restore template
            // initialise variables
            $this->template = $this->_templateBackup;
            echo '<td>';
            $this->renderButton('clear', $this->_clearButton, 0, null);
            echo '</td>';
        }
        else if ( !empty($this->filterHtmlContent) )
        {
            echo "<td>{$this->filterHtmlContent}</td>";
        }
        else
        {
            parent::renderFilterCell();
        }
    }


    /**
     * Renders the data cell content.
     * This method renders the view, update and delete buttons in the data cell.
     * @param integer $row the row number (zero-based)
     * @param mixed $data the data associated with the row
     */
    protected function renderDataCellContent($row, $data)
    {
        // Render a fixed text in this table column
        if ( !empty($this->fixedText) && is_array($this->fixedText) )
        {
            if ( !isset($this->fixedText['visible']) || (isset($this->fixedText['visible']) && $this->evaluateExpression($this->fixedText['visible'], ['row' => $row, 'data' => $data]) ) )
            {
                echo '<div class="fixed-text">'. $this->fixedText['value'] .'</div>';
                return;
            }
        }

        // Render button column
        $tr = [];
        ob_start();
        // echo '<div class="btn-group">';
        foreach ( $this->buttons as $id => $button )
        {
            $this->renderButton($id, $button, $row, $data);
            $tr['{'.$id.'}'] = ob_get_contents();
            ob_clean();
        }
        ob_end_clean();
        echo strtr($this->template, $tr);
        // echo '</div>';
    }


    /**
     * Renders a link button
     *
     * @param string $id the ID of the button
     * @param array $button the button configuration which may contain 'label', 'url', 'imageUrl' and 'options' elements.
     * @param integer $row the row number (zero-based)
     * @param mixed $data the data object associated with the row
     */
    protected function renderButton($id, $button, $row, $data)
    {       
        if ( ! isset($button['options']['class']) )
        {
            $button['options']['class'] = '';
        }
        
        if ( $id == 'actions' && !empty($this->menuAction) )
        {
            if ( isset($button['visible']) && ! $this->evaluateExpression($button['visible'], ['row'=>$row, 'data'=>$data]) )
            {
                return;
            }

            $button['options']['class'] .= ' btn btn-sm btn-icon btn-pure btn-default';
            $button['options']['href'] = '#';
            $label = isset($button['label']) ? $button['label'] : $id;
            $options = isset($button['options']) ? $button['options'] : [];
            
            if ( !isset($options['title']) )
            {
                $options['title'] = $label;
            }
            if ( !isset($options['data-toggle']) )
            {
                $options['data-toggle'] = 'tooltip';
                // $options['class'] .= ' tooltips';
                // $options['data-container'] = 'body';
            }
            
            // Menu dropdown
            $vec_actions = $this->evaluateExpression($this->menuAction, ['data' => $data, 'row' => $row]);
            if ( !empty($vec_actions) )
            {
                echo '<div class="dropdown dropdown-actions">';
                echo '<button type="button" class="btn btn-pure" data-toggle="dropdown" aria-expanded="true"><span class="icon wb-chevron-down-mini" aria-hidden="true"></span></button>';
                echo '<div class="dropdown-menu button-column-menu dropdown-menu-right" role="menu">';
                foreach ( $vec_actions as $que_action )
                {
                    // HTML Options
                    $item_htmlOptions = [];
                    if ( isset($que_action['htmlOptions']) )
                    {
                        $item_htmlOptions = $que_action['htmlOptions'];
                    }
                
                    if ( is_array($que_action) )
                    {
                        // Visible attribute?
                        $is_visible = true;
                        if ( isset($que_action['visible']) )
                        {
                            $is_visible = $this->evaluateExpression($que_action['visible'], ['row' => $row, 'data' => $data]);
                        }

                        if ( $is_visible )
                        {
                            // Icon button
                            if ( isset($que_action['icon']) )
                            {
                                // $que_action['label'] = '<i class="wb-'. $que_action['icon'] .'"></i> '. $que_action['label'];
                                $que_action['label'] = Html::icon($que_action['icon']) . $que_action['label'];
                            }
                            
                            // Id attribute is requried. If not exists, generate a random one
                            if ( ! isset($item_htmlOptions['id']) )
                            {
                                $item_htmlOptions['id'] = uniqid();
                            }
                        
                            // Class "dz-bootbox-confirm" needed
                            if ( ! isset($item_htmlOptions['class']) )
                            {
                                $item_htmlOptions['class'] = 'dropdown-item';
                            }
                            else
                            {
                                $item_htmlOptions['class'] .= ' dropdown-item';
                            }

                            // Confirm box??
                            if ( isset($que_action['confirm']) )
                            {
                                $item_htmlOptions['data-confirm'] = $que_action['confirm'];
                                $item_htmlOptions['class'] .= ' dz-bootbox-confirm';
                            }

                            if ( isset($que_action['url']) )
                            {
                                echo Html::link($que_action['label'], $que_action['url'], $item_htmlOptions);
                            }
                            else
                            {
                                echo $que_action['label'];
                            }
                        }
                    }
                    // Separator
                    else if ( $que_action == '---' )
                    {
                        echo '<div class="dropdown-divider"></div>';
                    }
                }
                echo '</div></div>';
                // echo Html::openTag('a', $options) . '<span class="caret"></span>' .'</a>';
                // echo Html::dropdown_menu($vec_actions, ['class' => 'dropdown-menu button-column-menu dropdown-menu-right']);
            }
        }
        else
        {
            if ( $id == 'clear' )
            {
                $button['options']['class'] .= ' btn btn-default';
            }
            else
            {
                $button['options']['class'] .= ' btn btn-sm btn-icon btn-pure btn-default';
            }
            return $this->_renderButton($id, $button, $row, $data);
        }       
    }

    
    /**
     * Renders a link button (code from TbButtonColumn)
     * @param string $id the ID of the button
     * @param array $button the button configuration which may contain 'label', 'url', 'imageUrl' and 'options' elements.
     * @param integer $row the row number (zero-based)
     * @param mixed $data the data object associated with the row
     */
    private function _renderButton($id, $button, $row, $data)
    {
        if ( isset($button['visible']) && ! $this->evaluateExpression($button['visible'], ['row' => $row, 'data' => $data]) )
        {
            return;
        }

        $label = isset($button['label']) ? $button['label'] : $id;
        $url = isset($button['url']) ? $this->evaluateExpression($button['url'], ['data' => $data, 'row' => $row]) : '#';
        $options = isset($button['options']) ? $button['options'] : [];

        if ( $this->showDestinationUrl && !empty($url) )
        {
            if ( preg_match("/\/\?/", $url) )
            {
                $url .= '&';
            }
            else
            {
                $url .= '?';
            }
            $url .= 'destination='. strtolower(Yii::app()->controller->getAction()->getId());
        }

        if ( ! isset($options['title']) )
        {
            $options['title'] = $label;
        }

        if ( ! isset($options['data-toggle']) )
        {
            $options['data-toggle'] = 'tooltip';
            // $options['class'] .= ' tooltips';
            // $options['data-container'] = 'body';
        }

        // Confirm box??
        if ( isset($button['confirm']) )
        {
            $options['data-confirm'] = '<h3>'. $button['confirm'] .'</h3>';
            $options['class'] .= ' dz-bootbox-confirm';
        }

        // DEZERO - Add text, instead of icon image
        if ( isset($button['icon-text']) && is_string($button['icon-text']) )
        {
            echo Html::link('<i class="icon-text">P</i>', $url, $options);
        }

        // Icon image (from Bootstrap library)
        else if ( isset($button['icon']) && is_string($button['icon']) )
        {
            if (strpos($button['icon'], 'icon') === false)
            {
                $button['icon'] = 'wb-'.implode(' wb-', explode(' ', $button['icon']));

                // Allow icons from "Font Awesome" library
                $button['icon'] = str_replace('wb-fa-', 'fa-', $button['icon']);
            }
            if ( isset($options['icon']) )
            {
                unset($options['icon']);
            }
            echo Html::link(Html::icon($button['icon']), $url, $options);
        }

        // Image URL (from Yii original core feature)
        else if ( isset($button['imageUrl']) && is_string($button['imageUrl']) )
        {
            echo Html::link(Html::image($button['imageUrl'], $label), $url, $options);
        }
        else
        {
            echo Html::link($label, $url, $options);
        }
    }


    /**
     * Static method to check if a model uses a certain behavior class
     *
     * @param CModel $model
     * @param string $behaviorClass
     * @return boolean
     */
    private static function modelUsesBehavior($model, $behaviorClass)
    {
        $behaviors = $model->behaviors();
        if ( is_array($behaviors) )
        {
            foreach ( $behaviors as $behavior => $behaviorDefine )
            {
                $className = $behaviorDefine;
                if ( is_array($behaviorDefine) )
                {
                    $className = $behaviorDefine['class'];
                }

                if ( strpos($className, $behaviorClass) !== false )
                {
                    return true;
                }
            }
        }
        return false;
    }
    

    /**
     * Custom clear filters function
     */
    public static function clearFilters($controller, $model)
    {
        $model->unsetAttributes();
        try
        {
            if ( self::modelUsesBehavior($model, 'ERememberFiltersBehavior') )
            {
                $model->unsetFilters();
            }
        }
        catch (\Exception $e) { }
        $controller->redirect([$controller->action->ID]);
    }
}
