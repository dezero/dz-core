<?php
/**
 * DataColumn class file
 *
 * CGridColumn is the base class for all grid view column classes
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2012 Dezero
 */

namespace dz\grid;

use dz\helpers\Html;
use Yii;

// Yii::import('zii.widgets.grid.CDataColumn');
Yii::import('@bootstrap.widgets.BsDataColumn');

class DataColumn extends \BsDataColumn
{
    /**
     * Filter type
     */
    public $filter_type;


    /**
     * Filter type options
     */
    public $filter_type_options;


	/**
	 * Renders the filter cell
	 */
	public function renderFilterCell()
	{
		$this->filterHtmlOptions = \CMap::mergeArray(
			array('class' => $this->name. '_filter'),
			$this->filterHtmlOptions
		);
		parent::renderFilterCell();
	}


	/**
	 * Renders the header cell
	 */
	public function renderHeaderCell()
	{
		$this->headerHtmlOptions['id'] = $this->id;
		$classes = $this->name. '_header';

		// Check if this column is sortable
		if ( $this->grid->enableSorting && $this->sortable && $this->name!==null )
		{
			$sort = $this->grid->dataProvider->getSort();
			if ( $sort->resolveAttribute($this->name) !== FALSE )
			{
				$classes .= ' sortable';
			}
		}

		$this->headerHtmlOptions = \CMap::mergeArray(
			array('class' => $classes),
			$this->headerHtmlOptions
		);
		parent::renderHeaderCell();
	}
	
	
	/**
	 * Renders a data cell
	 *
	 * @param integer $row the row number (zero-based)
	 */
	public function renderDataCell($row)
	{
		$this->htmlOptions = \CMap::mergeArray(
			array('class' => $this->name. '_column'),
			$this->htmlOptions
		);
		parent::renderDataCell($row);
	}


	/**
	 * Renders the footer cell
	 */
	public function renderFooterCell()
	{
		$this->footerHtmlOptions = \CMap::mergeArray(
			array('class' => $this->name. '_footer'),
			$this->footerHtmlOptions
		);
		parent::renderFooterCell();
	}
	
	
	/**
	 * Redefine evaluateExpression function
	 *
	 * It allows for a more complex PHP code snippet to be run for the calculation of 'value'
	 *
	 * @link http://www.yiiframework.com/extension/pcphpdatacolumn/
	 */
	public function evaluateExpression($_expression_, $_data_ = array())
	{
		if ( is_string($_expression_) )
		{
			extract($_data_);
			if ( ! preg_match("/return/", $_expression_) )
			{
				$_expression_ = 'return ('. $_expression_ .')';
			}
			return eval($_expression_ . ';');
		}
		else
		{
			$_data_[] = $this;
			return call_user_func_array($_expression_, $_data_);
		}
	}
	
	
	/**
 	 * Renders the filter cell content.
	 */
	public function renderFilterCellContent()
	{
		echo Html::openTag("div", array('class' => 'filter-wrapper'));
		// parent::renderFilterCellContent();


		if ( is_string($this->filter) )
		{
            echo $this->filter;
        }
        else
        {
            if ($this->filter !== false && $this->grid->filter !== null && $this->name !== null && strpos($this->name, '.') === false )
            {
                if ($this->filterInputOptions)
                {
                    $filterInputOptions = $this->filterInputOptions;
                    if (empty($filterInputOptions['id']))
                    {
                        $filterInputOptions['id'] = false;
                    }
                }
                else
                {
                    $filterInputOptions = array();
                }
                
                // Add "form-control" as default class
                if ( isset($filterInputOptions['class']) )
                {
                    $filterInputOptions['class'] .= ' form-control';
                }
                else
                {
                    $filterInputOptions['class'] = 'form-control';
                }

                // Try to disable autocomplete on inputs
                if ( !isset($filterInputOptions['autocomplete']) )
                {
                    $filterInputOptions['autocomplete'] = 'off';
                }

                if ( is_array($this->filter) )
                {
                    $filterInputOptions['prompt'] = '';
                    // $filterInputOptions['data-plugin'] = "select2";
                    echo Html::activeDropDownList(
                        $this->grid->filter,
                        $this->name,
                        $this->filter,
                        $filterInputOptions
                    );
                }
                else if ( $this->filter === null )
                {
                    // By default, add filter type "date" for all columns ended in "_date"
                    if ( empty($this->filter_type) && preg_match("/\_date$/", $this->name) )
                    {
                        $this->filter_type = 'date';
                    }

                    if ( !empty($this->filter_type) )
                    {
                        switch ( $this->filter_type )
                        {
                            case 'date':
                                echo '<div class="input-group">';
                                echo Html::activeTextField($this->grid->filter, $this->name, $filterInputOptions);
                                echo '<span class="input-group-addon"><i class="icon wb-calendar" aria-hidden="true"></i></span></div>';
                            break;

                            case 'date_range':
                                // Date range
                                if ( !empty($this->filter_type_options) && isset($this->filter_type_options['date_from']) && isset($this->filter_type_options['date_to']) )
                                {
                                    echo '<div class="input-daterange"><div class="input-group"><span class="input-group-addon input-group-addon-icon"><i class="icon wb-calendar" aria-hidden="true"></i></span>';
                                    echo Html::activeTextField($this->grid->filter, $this->filter_type_options['date_from'], $filterInputOptions) .'</div>';
                                    echo '<div class="input-group"><span class="input-group-addon input-group-addon-text">'. Yii::t('app', 'to') .'</span>';
                                    echo Html::activeTextField($this->grid->filter, $this->filter_type_options['date_to'], $filterInputOptions) .'</div>';
                                    echo '</div>';
                                }
                                else
                                {
                                    echo '<div class="input-group">';
                                    echo Html::activeTextField($this->grid->filter, $this->name, $filterInputOptions);
                                    echo '<span class="input-group-addon"><i class="icon wb-calendar" aria-hidden="true"></i></span></div>';
                                }
                            break;
                        }
                    }
					else
					{
						echo Html::activeTextField($this->grid->filter, $this->name, $filterInputOptions);
					}
                    
                }
            }
            else
            {
                parent::renderFilterCellContent();
            }
        }
		echo '</div>';
	}
}