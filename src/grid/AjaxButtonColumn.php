<?php
/**
 * AjaxButtonColumn class file
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 */

namespace dz\grid;

use CClientScript;
use dz\grid\ButtonColumn;
use dz\widgets\Modal;
use dz\helpers\Html;
use Yii;

class AjaxButtonColumn extends ButtonColumn
{
	/**
	 * Load modal widget?
	 *
	 * @var bool
	 */
	public $loadModal = TRUE;


	/**
	 * Modal options
	 *
	 * @var array
	 */
	public $modalOptions = array();


	/**
	 * Use view button?
	 *
	 * @var bool
	 */
	public $viewButton = FALSE;


	/**
	 * View mode (not edit/insert data)
	 *
	 * @var string
	 */
	public $viewMode = FALSE;


	/**
	 * Initializes the default buttons (view, update, delete & action).
	 */
	protected function initDefaultButtons()
	{
		parent::initDefaultButtons();

		if ( $this->viewMode )
		{
			// We only need "view" button
			if ( !empty($this->buttons) )
			{
				foreach ( $this->buttons as $button_id => $button_content )
				{
					// Remove this button link
					if ( $button_id != 'view' )
					{
						unset($this->buttons[$button_id]);
						$this->template = str_replace("{". $button_id ."}", "", $this->template);
						$this->_templateBackup = str_replace("{". $button_id ."}", "", $this->_templateBackup);
					}
				}

				// Disable update link?
				if ( $this->updateButton === FALSE )
				{
					$this->template = str_replace('{update}', '', $this->template);
					$this->_templateBackup = str_replace("{update}", "", $this->_templateBackup);
				}
			}
		}
		else
		{
			// Remove "view" button link
			if ( isset($this->buttons['view']) AND $this->viewButton === FALSE )
			{
				unset($this->buttons['view']);
				$this->template = str_replace("{view}", "", $this->template);
				$this->_templateBackup = str_replace("{view}", "", $this->_templateBackup);
			}
		}

		// Disable update link?
		if ( $this->updateButton === FALSE )
		{
			$this->buttons['update']['visible'] = "FALSE";
		}

		// Load modal just one time
		if ( $this->loadModal AND ! Yii::app()->getClientScript()->isScriptRegistered(__CLASS__.'#ajax-modal-grid',  CClientScript::POS_READY) )
		{
			$vec_modal_options = \CMap::mergeArray(array(
				'id' => 'dz-modal-ajax-grid',
				'runJavascript' => FALSE
			), $this->modalOptions);

			Yii::app()->controller->beginWidget('dz.widgets.Modal',  $vec_modal_options);
			
			// Modal HTML code
			echo '<div class="modal-header">
				    <a class="close" data-dismiss="modal">&times;</a>
				    <h4 id="dz-modal-title"></h4>
				    <div id="dz-modal-subtitle"></div>
				</div>
				
				<div id="dz-modal-body" class="modal-body">
				    <p>'. Yii::t('app', 'Loading...') .'</p>
				</div>

				<div id="dz-modal-footer" class="modal-footer">';

			if ( ! $this->viewMode )
			{
				// Submit button
				Yii::app()->controller->widget('@bootstrap.widgets.TbButton', array(
					'label'		=> Yii::t('app', 'Save'),
					'htmlOptions' => array(
						'class' => 'btn-primary',
						'onclick' => 'jQuery("#dz-modal-body form").submit();'
					)
				));

				// Close button
				Yii::app()->controller->widget('@bootstrap.widgets.TbButton', array(
			        'label' => Yii::t('app', 'Close'),
					// 'type' => 'inverse',
			        'url' => '#',
			        'htmlOptions' => array(
						'data-dismiss' => 'modal',
						'aria-hidden'  => "true"
					),
			    ));
			}
			else
			{
				// Close button
				Yii::app()->controller->widget('@bootstrap.widgets.TbButton', array(
			        'label' => Yii::t('app', 'Close'),
					'type' => 'inverse',
			        'url' => '#',
			        'htmlOptions' => array(
						'data-dismiss' => 'modal',
						'aria-hidden'  => "true"
					),
			    ));
			}


			echo '</div>';
			Yii::app()->controller->endWidget();

			Yii::app()->getClientScript()->registerScript(__CLASS__.'#ajax-modal-grid', "jQuery('#dz-modal-ajax-grid').appendTo(document.body);", CClientScript::POS_READY);
		}
	}


	/**
	 * Renders a link button
	 *
	 * @param string $id the ID of the button
	 * @param array $button the button configuration which may contain 'label', 'url', 'imageUrl' and 'options' elements.
	 * @param integer $row the row number (zero-based)
	 * @param mixed $data the data object associated with the row
	 */
	protected function renderButton($id, $button, $row, $data)
	{		
		$is_modal = TRUE;
		if ( isset($button['options']['is_modal']) )
		{
			$is_modal = $button['options']['is_modal'];
			unset($button['options']['is_modal']);
		}

		if ( $is_modal AND $id != 'delete' AND isset($button['url']) AND isset($button['options']['class']) AND $button['options']['class'] != 'clear' )
		{
			$button['options']['data-url'] = $this->evaluateExpression($button['url'], array('data'=>$data,'row'=>$row));
			// $button['options']['data-grid_id'] = '#'. $this->grid->id;
			$button['options']['data-grid_id'] = $this->grid->id;
			unset($button['url']);
			$button['options']['class'] .= ' dz-ajax-btn';
		}
		else
		{
			if ( isset($button['url']) AND $button['url'] == '#' )
			{
				unset($button['url']);
			}
			if ( isset($button['options']['data-url']) )
			{
				$button['options']['data-url'] = $this->evaluateExpression($button['options']['data-url'], array('data'=>$data,'row'=>$row));
			}

			// <DEZERO> Check if there're some button starting with data
			if ( isset($button['options']) AND !empty($button['options']) )
			{
				foreach ( $button['options'] as $button_name => $button_value )
				{
					// "data-some-key" => "eval($data->some_data)"
					if ( preg_match("/^data\-/", $button_name) AND preg_match("/^eval\(/", $button_value) )
					{
						// Remove "eval(" and last character ")"
						$button_value = substr(substr($button_value, 5), 0, -1);
						$button['options'][$button_name] = $this->evaluateExpression($button_value, array('data'=>$data,'row'=>$row));
					}
				}
			} 
		}
		
		parent::renderButton($id, $button, $row, $data);
	}
}