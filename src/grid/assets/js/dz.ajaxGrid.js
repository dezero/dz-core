$(function () {
	// Ajax Form Button  ==============================================
	$.fn.dzAjaxFormButton = function(options) {
		function init() {
			$base.on('click', function(){
				// $('body').modalmanager('loading');
				$('#dz-modal-title', $modal).html("&nbsp;");
				$('#dz-modal-subtitle').html("");
				$('#dz-modal-body', $modal).html("<p>Cargando</p>");
				$.ajax({
					url: $(this).data('url'),
					dataType: settings.dataType,
					success: function(data) {
						$('#dz-modal-title', $modal).html(data.title);
						if ( data.hasOwnProperty('subtitle') ) {
							$('#dz-modal-subtitle', $modal).html(data.subtitle);
						}
						$('#dz-modal-body', $modal).html(data.content);
						$modal.modal({
							show: true,
							replace: true,
							modalOverflow: true,
							focusOn: 'input:first'
							// maxWidth: $(window).height() - 95
						});
						settings.afterModalSuccess(data);
					},
					error: function(request, status, error) {
						alert('Código incorrecto. \n\nERROR: '+request.responseText);
					},
					cache: false
				});
			});
		}

		var $base = $(this);
		var settings = {
			dataType: 'json',
			afterModalSuccess: function(data) {
				//$.dzAjaxGridRefresh(grid_id);
			}
		};
		if ( ! $.isEmptyObject(options) ) {
			$.extend(settings, options);
		}
		if ( $(this).size() > 0 ) {
			var grid_id = $(this).data('grid_id');
			if ( options.modal_id !== undefined ) {
				var $modal = $("#"+ options.modal_id);
			} else {
				var $modal = $("#dz-modal-ajax-grid");
			}
			init();
		}
	};

	// Ajax Grid Refresh  ==============================================
	$.dzAjaxGridRefresh = function(que_grid_id) {
		if ( $("#"+que_grid_id+'-url').length > 0 ) {
			$.fn.yiiGridView.update(que_grid_id, {
				url: $("#"+que_grid_id+'-url').val()
			});
		} else {
			$.fn.yiiGridView.update(que_grid_id);
		}
		// $.fn.yiiGridView.update(dz_ajax_grid.grid_id);
	};

	// Ajax Grid Modal Link (details)  ==============================================
	$.fn.dzAjaxGridModalLink = function() {
		function init() {
			$base.on('click', function(){
				// $('body').modalmanager('loading');
				$('.dz-modal-title', $modal).html($(this).data('title'));
				$('.dz-modal-subtitle', $modal).html($(this).data('subtitle'));
				$('.dz-modal-body', $modal).html("<p>Cargando...</p>");
				$('.dz-modal-button', $modal).hide();
				if ( typeof($(this).data('view_url')) != 'undefined' ) {
					$('.dz-modal-view-button', $modal).attr('href', $(this).data('url').replace('/teaser','/view')).show();
				}
				$.ajax({
					url: $(this).data('url'),
					dataType: 'html',
					success: function(data) {
						$('.dz-modal-body', $modal).html(data);
						$modal.modal({
							show: true,
							replace: true,
							modalOverflow: true
						});
					},
					error: function(request, status, error) {
						alert('Código incorrecto. \n\nERROR: '+request.responseText);
					},
					cache: false
				});
			});
		}

		var $base = $(this);
		if ( $(this).size() > 0 ) {
			var grid_id = $(this).data('grid_id');
			var $modal = $("#dz-modal-"+ grid_id);
			init();
		}
	};

	// Ajax Grid Modal Form Link  ==============================================
	$.fn.dzAjaxGridModalFormLink = function() {
		function init() {
			$base.on('click', function(){
				// $('body').modalmanager('loading');
				$('.dz-modal-title', $modal).html($(this).data('title'));
				$('.dz-modal-subtitle', $modal).html($(this).data('subtitle'));
				$('.dz-modal-body', $modal).html("<p>Cargando</p>");
				$('.dz-modal-button', $modal).hide();
				$('.dz-modal-submit-button', $modal).show();
				$.ajax({
					url: $(this).data('url'),
					dataType: 'json',
					success: function(data) {
						if ( data.hasOwnProperty('title') ) {
							$('.dz-modal-title', $modal).html(data.title);
						}
						if ( data.hasOwnProperty('subtitle') ) {
							$('.dz-modal-subtitle', $modal).html(data.subtitle);
						}
						$('.dz-modal-body', $modal).html(data.content);
						$modal.modal({
							show: true,
							replace: true,
							modalOverflow: true,
							focusOn: 'input:first'
							// maxWidth: $(window).height() - 95
						});
					},
					error: function(request, status, error) {
						alert('Código incorrecto. \n\nERROR: '+request.responseText);
					},
					cache: false
				});
			});
		}

		var $base = $(this);
		if ( $(this).size() > 0 ) {
			var grid_id = $(this).data('grid_id');
			var $modal = $("#dz-modal-"+ grid_id);
			init();
		}
	};
});