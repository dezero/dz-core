<?php
/**
 * AjaxGridView widget class file
 *
 * This extension makes CGridView works with an AJAX behavior
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2013 Dezero
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */

namespace dz\grid;

use CJavaScript;
use dz\grid\GridView;
use dz\helpers\Html;
use Yii;

class AjaxGridView extends GridView
{	
	/**
	 * File path to assets
	 *
	 * @var string
	 */
	protected $assetsPath;


	/**
	 * URL to assets
	 *
	 * @var string
	 */
	protected $assetsUrl;
	

	/**
	 * AJAX options
	 *
	 * @var array
	 */
	public $dzModalOptions;


	/**
	 * Remove loading (from Modal)?
	 *
	 * @var bool
	 */
	public $removeLoading = FALSE;

	/**
	 * Initializes the widget
	 */
	public function init()
	{
		parent::init();

		// Assets
		if ( $this->assetsPath === NULL )
		{
			$this->assetsPath = dirname(__FILE__) . DIRECTORY_SEPARATOR .'assets';
		}
		
		if ( $this->assetsUrl === NULL )
		{
			$this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
		}

		// Special class
		$this->htmlOptions['class'] .= ' dz-ajax-grid-view';
	}

	
	/**
	 * Register required CSS and script files
	 */
	public function registerClientScript()
	{
		// Javascript needed for this widget
		// $cs = Yii::app()->getClientScript();
		// $cs->registerScriptFile("{$this->assetsUrl}/js/dz.ajaxGrid.js");

		$ajax_options = '';
		if ( !empty($this->dzModalOptions) )
		{
			$ajax_options = CJavaScript::encode($this->dzModalOptions);
		}
		
		$js_ajax_button = "jQuery('#{$this->id} .dz-ajax-btn').dzAjaxFormButton(". $ajax_options ."); jQuery('#{$this->id}-create-link .dz-ajax-btn').dzAjaxFormButton(". $ajax_options .");";

		// Bootbox confimr
		$js_ajax_button .= "jQuery('#{$this->id} .dz-bootbox-confirm').dzBootbox();";

		// Button links show a Bootstrap modal
		Yii::app()->getClientScript()->registerScript(__CLASS__.'#ajax-'. $this->id, $js_ajax_button, CClientScript::POS_READY);

		// Create button link don't refresh via AjaxUpdate
		$js_ajax_button = "jQuery('#{$this->id} .dz-ajax-btn').dzAjaxFormButton(". $ajax_options .");";
		
		/*
		Yii::app()->clientScript->registerScript(__CLASS__.'#ajax_vars-'. $this->id,
			'<!--//--><![CDATA[//><!--
			var dz_ajax_grid = '. CJSON::encode(array(
				'grid_id' => $this->id
			)) .';
			//--><!]]>',
			CClientScript::POS_HEAD
		);
		*/

		// After AJAX update
		if ( !isset($this->afterAjaxUpdate) )
		{
			$this->afterAjaxUpdate = $js_ajax_button;
		}
		else
		{
			$this->afterAjaxUpdate = str_replace("js:function() {", "js:function() {" .$js_ajax_button, $this->afterAjaxUpdate);

			// En AjaxGridView quitamos el loading porque sinó se queda fijo
			/*
			if ( $this->removeLoading )
			{
				$this->afterAjaxUpdate = str_replace("jQuery('body').modalmanager('loading'); ", '', $this->afterAjaxUpdate);
			}
			*/
		}
		return parent::registerClientScript();
	}
}