<?php
/**
 * ApplicationComponent class file for Dz Framework
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2018 Fabián Ruiz
 */

namespace dz\base;

abstract class ApplicationComponent extends \CApplicationComponent
{
    /**
     * Cached data
     */
    public $vec_cache = [];


    /**
     * Cached errors
     */
    public $vec_errors = [];


	/**
     * Returns the static object of this component
     * 
     * EVERY derived ApplicationComponent class must override this method as follows,
	 * <pre>
	 * public static function component($className=__CLASS__)
	 * {
	 *     return parent::component($className);
	 * }
	 * </pre>
	 *
	 * @param string $className active record class name.
	 * @return static active record model instance.
     */
    public static function component($className = __CLASS__)
	{
		$component = new $className(null);
		$component->init();
		return $component;
	}


    /**
     * Add configuration options
     */
    public function add_config($vec_config)
    {
        if ( !empty($vec_config) && is_array($vec_config) )
        {
            foreach ( $vec_config as $key => $value )
            {
                $this->{$key} = $value;
            }

            return true;
        }

        return false;
    }



	/**
     * Add error/s
     */
    public function add_error($vec_errors)
    {
        if ( is_array($vec_errors) )
        {
            $this->vec_errors = \CMap::mergeArray($this->vec_errors, $vec_errors);
        }
        else
        {
            $this->vec_errors[] = $vec_errors;
        }
    }
}