<?php
/**
 * RestController class file
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2017 Fabián Ruiz
 */

namespace dz\rest;

use dz\helpers\Json;
use dz\rest\RestResource;
use dz\web\Controller;
use Yii;

/**
 * RestController is the base class for the REST API controllers
 */
abstract class RestController extends Controller
{
    /**
     * Initializes the controller.
     */
    public function init() {        

        parent::init();

        // Custom error & exception handler
        // Yii::app()->errorHandler->errorAction = $this->actionError();
        Yii::app()->attachEventHandler('onError', [$this, 'handleError']);
        Yii::app()->attachEventHandler('onException', [$this, 'handleError']);
    }


	/**
	 * Returns the filter configurations
	 *
	 * @return array
	 */
	public function filters()
	{
		return [
            ['dz\filters\RestFilter'],
            // ['dz\filters\AuthFilter'],
		];
	}


    /**
     * Custom handleError method
     * 
     * @see https://stackoverflow.com/questions/11127479/yii-catching-all-exceptions-for-a-specific-controller
     */
    public function handleError(\CEvent $event)
    {        
        $status_code = 500;
        $error_message = '';

        // Handle exception
        if ( $event instanceof \CExceptionEvent )
        {
            if ( isset($event->exception->statusCode) )
            {
                $status_code = $event->exception->statusCode;
            }
            $error_code = $event->exception->getCode();
            $error_message = $event->exception->getMessage();
        }

        // Handle error
        else if( $event instanceof \CErrorEvent )
        {
            $error_code = $event->code;
            $error_message = $event->message;
        }

        if ( !empty($error_message) )
        {
            if ( $status_code == 500 && $error_code > 0 )
            {
                $status_code = $error_code;
            }

            $que_error = 'Error '. $status_code .' - '. str_replace('"', "'", $error_message);
            if ( $status_code == 404 )
            {
                $que_error = 'Endpoint not found. '. $que_error;
            }
            $vec_response = [
                'status_code'   => $status_code,
                'errors'        => [$que_error],
                'results'       => []
            ];

            // Save log
            $rest_resource = new RestResource;
            $rest_resource->vec_response = $vec_response;
            $rest_resource->save_log('rest_error', $vec_response['status_code']);

            // Return JSON and end application
            $this->jsonOutput(200, Json::encode($vec_response));
        }

        $event->handled = true;
    }


	/**
	 * This is the action to handle external exceptions.
	 *
	 * Called from SiteController::actionError (default action for errors)
	 */
	public function actionError()
	{
		if ( $vec_error = Yii::app()->errorHandler->error )
		{
			$que_error = 'Error '. $vec_error['code'] .' - '. str_replace('"', "'", $vec_error['message']);
			if ( $vec_error['code'] == 404 )
			{
				$que_error = 'Resouce / endpoint not found. '. $que_error;
			}
			$vec_response = [
				'status_code' 	=> $vec_error['code'],
				'error' 		=> $que_error
			];

			// Save log
			$rest_resource = new RestResource;
			$rest_resource->vec_response = $vec_response;
			$rest_resource->save_log('rest', $vec_response['status_code']);

			// Return JSON and end application
			$this->jsonOutput(200, Json::encode($vec_response));
		}
	}
}
