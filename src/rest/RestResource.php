<?php
/**
 * DzRestResource class file
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2017 Fabián Ruiz
 */

namespace dz\rest;

use dz\helpers\Http;
use dz\helpers\Json;
use dz\helpers\RestHelper;
use dz\helpers\StringHelper;
use dz\modules\api\models\LogApi;
use Yii;

/**
 * Resource class for REST API
 */
class RestResource
{
	/**
	 * API name
	 */
	public $api_name = 'dz';


    /**
     * Log category
     */
    public $log_category = 'rest';


    /**
     * Log category for errors
     */
    public $log_category_error = 'rest_error';


	/**
	 * Array with input params
	 */
	public $vec_input;


	/**
	 * Array with HTTP response
	 */
	public $vec_response;


	/**
	 * HTTP request type
	 */
	public $request_type;


	/**
	 * Current model instance
	 */
	public $model;


	/**
	 * List filters
	 */
	public $vec_list_filters;


	/**
	 * Is input validated?
	 */
	protected $is_validated;


	/**
	 * API configuration
	 */
	protected $vec_config = [];


	/**
	 * Expand options
	 */
	protected $vec_expand_options = [];


	/**
	 * Initializes the resource
	 */
	public function __construct()
	{
		// Init vars
		$this->is_validated = false;
		$this->vec_input = [];
		$this->vec_list_filters = [];
		$this->request_type = Yii::app()->getRequest()->getRequestType();
		$this->vec_response = [
			'status_code' 	=> 100,
			'errors' 		=> ['Access denied']
		];

		// Debug
		/*
		$this->is_debug = false;
		if ( isset(Yii::app()->params['rest']) && isset(Yii::app()->params['rest']['is_debug']) && Yii::app()->params['rest']['is_debug'] === true )
		{
			$this->is_debug = true;
		}
		*/

        // Check authorization
        if ( ! $this->check_auth() )
        {
            // Send HTTP header 401 - Unauthorized
            $this->send_error(401, 'Access denied', 401);
        }

		// Load input
		$this->load_input();
	}


    /**
     * Check authorization
     */
    public function check_auth()
    {
        // Load auth configuration value
        $vec_auth = $this->get_config_value('auth');
        if ( !empty($vec_auth) && $vec_auth !== false && isset($vec_auth['token']) )
        {
            $auth_prefix = isset($vec_auth['prefix']) ? $vec_auth['prefix'] : 'Bearer';
            return $this->check_auth_token($vec_auth['token'], $auth_prefix);
        }

        return true;
    }


    /**
     * Check authorization via simple token comparison
     */
    public function check_auth_token($auth_token, $auth_prefix = 'Bearer')
    {
        // Get HTTP headers
        $vec_headers = Yii::app()->request->getHeaders();

        // Check if header Authorization has been defined
        if ( !isset($vec_headers['Authorization']) )
        {
            return false;
        }

        // Compare auth header
        $auth_header = $auth_token;
        if ( !empty($auth_prefix) )
        {
            $auth_header = $auth_prefix .' '. $auth_header;
        }

        return $vec_headers['Authorization'] === $auth_header;
    }


	/**
	 * Load input params
	 */
	public function load_input()
	{
		switch ( $this->request_type )
		{
			case 'POST':
			case 'PUT':
			case 'DELETE':
				$this->vec_input = $this->jsonInput();
			break;

			case 'GET':
				$this->vec_input = $_GET;
			break;
		}
	}


	/**
	 * Send HTTP response with output and END application
	 */
	public function send_response($http_status_code = 200, $is_save_log = true, $api_name = 'dz')
	{
		// Log
		if ( $is_save_log )
		{
			$this->save_log($this->log_category, $http_status_code, $api_name);
		}

		// Return JSON and end application
		$this->jsonOutput($http_status_code, Json::encode($this->vec_response));
	}


	/**
	 * Send an error via JSON
	 */
	public function send_error($status_code, $error_message, $http_status_code = 200, $api_name = 'dz')
	{
		$this->vec_response = [
			'status_code'	=> $status_code,
			'errors'		=> is_array($error_message) ? $error_message : [$error_message]
		];

		// Log
		$this->save_log($this->log_category_error, $http_status_code);

		// Send response and END application
		$this->send_response($http_status_code, false);
	}


	/**
	 * Add a warning message
	 */
	public function add_warning($warning_message)
	{
		if ( !isset($this->vec_response['warnings']) )
		{
			$this->vec_response['warnings'] = [];
		}
		$this->vec_response['warnings'][] = $warning_message;

		return true;
	}


	/**
	 * Save request and response in a LOG (database or file)
	 */
	public function save_log($log_category = null, $http_status_code = 200, $api_name = 'dz', $log_destination = null)
	{
        // Log category
        if ( $log_category === null )
        {
            $log_category = $this->log_category;
        }

		// Check API configuration: log destination		
        if ( $log_destination === null )
        {
            $config_log_destination = $this->get_config_value('log_destination', $api_name);
            $log_destination = !empty($config_log_destination) ? $config_log_destination : 'file';
        }

		switch ( $log_destination )
		{
			// Save log into database
			case 'db':
				if ( $this->request_type == 'GET' )
				{
					$this->vec_input['_raw_input'] = $_GET;
				}
				
                $log_api_model = Yii::createObject(LogApi::class);
				$log_api_model->setAttributes([
					'api_name'				=> $api_name,
					'request_type'			=> $this->request_type,
					'request_url'			=> Yii::app()->getRequest()->getPathInfo(),
					'request_endpoint'		=> $this->request_type .'___'. Yii::app()->getRequest()->getPathInfo(),
					'request_hostname'		=> Yii::app()->getRequest()->getUserHostAddress(),
					'request_input_json'	=> is_array($this->vec_input) ? Json::encode($this->vec_input) : $this->vec_input,
					'response_http_code'	=> $http_status_code,
					'response_json'			=> Json::encode($this->vec_response)
				]);
                
				if ( ! $log_api_model->save() )
				{
					Yii::log('ERROR saving LogApi model: '. print_r($log_api_model->getErrors(), true), "profile", $this->log_category_error);
					return false;
				}
			break;

			// Save log in "rest.log" file (or "<log_category>.log" file)
			case 'file':
                $json_input = is_array($this->vec_input) ? Json::encode($this->vec_input) : $this->vec_input;

				$message  = "\n";
				$message .= " - IP: ". Yii::app()->getRequest()->getUserHostAddress() ."\n";
				$message .= " - Endpoint: ". Yii::app()->getRequest()->getPathInfo() ."\n";
				$message .= " - Método: ". $this->request_type ."\n";
				$message .= " - Parámetros: ". $json_input ."\n";
				$message .= " - Respuesta (HTTP code ". $http_status_code ."): ". Json::encode($this->vec_response) ."\n";
				Yii::log($message, 'profile', $log_category);
			break;
		}

		return true;
	}


    /**
     * Save request and response in a LOG (database)
     */
    public function save_db_log($api_name = 'dz')
    {
        return $this->save_log($this->log_category, 200, $api_name, 'db');
    }



	/*
	|--------------------------------------------------------------------------
	| BACKBONE FUNCTIONS from YiiBackbone extension
	|
	| @see https://github.com/clevertech/YiiBackbone
	|--------------------------------------------------------------------------
	*/


	/**
	 * Gets RestFul data and decodes its JSON request
	 * @return mixed
	 */
	public function jsonInput()
	{
		return Json::decode(file_get_contents('php://input'));
	}


    /**
	 * Send raw HTTP response
	 * 
	 * @param int $status HTTP status code
	 * @param string $body The body of the HTTP response
	 * @param string $contentType Header content-type
	 * @return HTTP response
	 */
	public function jsonOutput($status = 200, $body = '', $contentType = 'application/json')
	{
		// Set the status
		$statusHeader = 'HTTP/1.1 ' . $status . ' ' . $this->get_status_code_message($status);
		header($statusHeader);
		
		// Set the content type
		header('Content-type: ' . $contentType);

        // CORS subdomain - Access-Control-Allow-Origin headers
        if ( isset(Yii::app()->params['api_allowed_hosts']) )
        {
            $vec_allowed_origins = $this->get_allowed_hosts();
            if ( isset($_SERVER['HTTP_ORIGIN']) && in_array($_SERVER["HTTP_ORIGIN"], $vec_allowed_origins) )
            {
                header("Access-Control-Allow-Origin: " . $_SERVER["HTTP_ORIGIN"]);
            }
        }

        // Security - Access-Control-Allow-Methods
        header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");

        // Body
		echo $body;
		
		Yii::app()->end();
	}


	/**
	 * Return the http status message based on integer status code
	 * 
	 * @param int $status HTTP status code
	 * 
	 * @return string status message
	 */
	protected function get_status_code_message($status)
	{
		return Http::get_status_code_message($status);
	}


	/**
	 * Simple date format validation
	 *
	 * Allowed formats:
	 * 	- YYYY-MM-DD          (example: 2016-02-23)
	 * 	- YYYY-MM-DD hh:mm    (example: 2016-02-23 11:45)
	 * 	- YYYY-MM-DD hh:mm:ss (example: 2016-02-23 11:45:32)
	 */
	public function validate_date($date)
	{
        return RestHelper::validate_date($date);
	}


    /**
     * Return a formated date
     */
    public function format_date($date)
    {
        return RestHelper::format_date($date);
    }


	/**
	 * Parse array errors into a string ready to view on JSON output
	 */
	public function parse_errors($vec_errors)
	{
		return RestHelper::parse_errors($vec_errors);
	}


	/**
	 * Get output from an asset model for REST API
	 */
	public function get_asset_response($asset_model)
	{
		return RestHelper::get_asset_response($asset_model);
	}


    /**
     * Load configuration given an API name
     */
    public function load_config($api_name = 'dz')
    {
        if ( empty($this->vec_config) )
        {
            $this->vec_config = Yii::app()->config->get('components.api');
        }
    }


	/**
	 * Load and returns configuration for an API
	 */
	public function get_config($api_name = 'dz')
	{
        // Load configuration
		$this->load_config($api_name);

        // Return configuration given input params
		if ( !empty($this->vec_config) && !empty($api_name) && isset($this->vec_config[$api_name]) )
		{
			return $this->vec_config[$api_name];
		}

		// Default values
		$this->vec_config[$api_name] = [
            'log_destination'   => 'file',
            'auth'              => false,
            'allowed_hosts'     => []
		];

		return $this->vec_config[$api_name];
	}


    /**
     * Return a config value
     */
    public function get_config_value($config_value, $api_name = 'dz')
    {
        // Load config
        $this->load_config($api_name);

        // Return configuration given input params
        if ( !empty($this->vec_config) && !empty($api_name) && isset($this->vec_config[$api_name]) && isset($this->vec_config[$api_name][$config_value]) )
        {
            return $this->vec_config[$api_name][$config_value];
        }

        return null;
    }


    /**
     * Return allowed hosts
     */
    public function get_allowed_hosts($api_name = 'dz')
    {
        // First of all, check the "api_allowed_hosts" param on "params.php" file
        if ( isset(Yii::app()->params['api_allowed_hosts']) && !empty(Yii::app()->params['api_allowed_hosts']) )
        {
            return Yii::app()->params['api_allowed_hosts'];
        }

        // Check on configuration
        $vec_allowed_hosts = $this->get_config_value('allowed_hosts');
        if ( !empty($vec_allowed_hosts) )
        {
            return $vec_allowed_hosts;
        }

        return [];
    }
}
