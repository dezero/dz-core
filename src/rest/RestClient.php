<?php
/**
 * RestClient component
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2018
 */

namespace dz\rest;

use dz\base\ApplicationComponent;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\api\models\LogApi;
use Yii;

// Import EHttpClient library (Yii port of Zend_Http_Client)
Yii::import("@lib.EHttpClient.*");

/**
 * Client class for REST API
 */
class RestClient extends ApplicationComponent
{
	/**
	 * @var EHttpClient. Client HTTP request
	 */	
	public $client;


	/**
	 * @var EHttpResponse. Response HTTP request
	 */	
	public $response;


	/**
	 * @var string. HTTP method
	 */	
	public $method = 'GET';


	/**
	 * @var array. Input HTTP params
	 */	
	public $input;


	/**
	 * @var string. URI
	 */	
	public $uri;


	/**
	 * @var string. Base URL for endpoints
	 */	
	public $base_url;


	/**
	 * @var string. API key
	 */	
	public $api_key;


	/**
	 *  @var string. Authorization key
	 */	
	public $auth_key;


	/**
	 * @var char. Action type:
	 *	+ C: CREATE
	 *	+ U: UPDATE
	 *	+ X: DELETE
	 *	+ D: DISABLE
	 *	+ E: ENABLE
	 */	
	public $action_type;


	/**
	 * @var bool. Uses caches?
	 */
	public $is_cache = false;


    /**
     * @var bool. Debug mode? (log all responses)
     */ 
    public $is_debug = false;


    /**
     * @var string. Last error message
     */
    public $last_error;


    /**
     * @var string Entity id
     */
    public $entity_id;


    /**
     * @var string Entity type
     */
    public $entity_type;


    /**
     * @var integer. Identifier for last LogApi model
     */
    public $log_api_id;


	/**
     * Init function
     */
    public function init()
    {
        parent::init();
    }


    /**
     * Returns the static object of this component
     */
    public static function component($className=__CLASS__)
    {
        return parent::component($className);
    }


    /**
     * Send a GET request to an URI
     */
    public function get($uri, $vec_query_string = [], $vec_headers = [] )
    {
    	return $this->send($uri, 'GET', $vec_query_string, $vec_headers);
    }


    /**
     * Send a POST request to an URI
     */
    public function post($uri, $input = NULL, $vec_headers = [] )
    {
    	if ( empty($this->action_type) )
    	{
    		$this->action_type = 'C';
    	}

        // Input with JSON format
        if ( !empty($input) && is_array($input) )
        {
            $input = Json::encode($input);
        }

    	return $this->send($uri, 'POST', $input, $vec_headers);
    }



    /**
     * Send a PUT request to an URI
     */
    public function put($uri, $input = NULL, $vec_headers = [] )
    {
    	if ( empty($this->action_type) )
    	{
    		$this->action_type = 'U';
    	}

        // Input with JSON format
        if ( !empty($input) && is_array($input) )
        {
            $input = Json::encode($input);
        }

    	return $this->send($uri, 'PUT', $input, $vec_headers);
    }


    /**
     * Send a DELETE request to an URI
     */
    public function delete($uri, $input = NULL, $vec_headers = [] )
    {
    	if ( empty($this->action_type) )
    	{
    		$this->action_type = 'X';
    	}

        // Parameters for DELETE cannot be an array
        if ( is_array($input) )
        {
            $input = Json::encode($input);
        }

    	return $this->send($uri, 'DELETE', $input, $vec_headers);
    }


    /**
     * Send an HTTP request
     * 
     * @return EHttpResponse
     */
    public function send($uri, $method = '', $input = NULL, $vec_headers = [] )
    {
    	// HTTP method
    	if ( empty($method) )
    	{
    		$method = $this->method;
    	}
    	else
    	{
    		$this->method = $method;
    	}

    	// Save URI
    	$this->uri = $uri;

    	// Build URL and create HTTP Client
		$this->client = new \EHttpClient($this->base_url . $this->uri);
		$this->client->setMethod($method);

		// Body or GET/POST input paremeters
		if ( !empty($input) )
		{
			$this->input = $input;
			if ( is_array($this->input) )
			{
				if ( $method == 'GET' )
				{
					// $this->client = new EHttpClient($this->base_url . $this->uri . "?" . http_build_query($this->input, "", "&"));
					$this->client->setParameterGet($this->input);
				}
				else
				{
					$this->client->setParameterPost($this->input);
				}
			}
			else
			{
				$this->client->setRawData($this->input, 'application/json');
			}
		}

		// Some HTTP headers?
		if ( !empty($vec_headers) )
		{
			$this->client->setHeaders($vec_headers);
		}

		// Event "beforeSend()"
		$this->beforeSend();

		// Finally, make the request
		$this->response = $this->client->request();

		// Event "afterSend()"
		$this->afterSend();

        // Save last error
        if ( empty($this->last_error) && ! $this->is_last_action_success() )
        {
            $this->set_last_error();
        }

		return $this->response;
    }


    /**
     * Custom event before sending HTTP request
     */
    public function beforeSend()
    {
    }


    /**
     * Custom event after sending HTTP request
     */
    public function afterSend()
    {
    }


    /**
     * Get last EHttpResponse object
     */
    public function get_response()
    {
        return $this->response;
    }


    /**
     * Process and return response data into an array
     */
    public function get_response_body($is_json = false)
    {
        if ( $this->response && $this->response->isSuccessful() )
        {
            if ( $is_json )
            {
                return Json::decode($this->response->getBody());
            }
            
            return $this->response->getBody();
        }

        return null;
    }


    /**
     * Return last error
     */
    public function get_last_error()
    {
        return $this->last_error;
    }


    /**
     * Set last error
     */
    public function set_last_error()
    {
        $this->last_error = trim($this->response->getRawBody());
    }


    /**
     * Clear last error
     */
    public function clear_last_error()
    {
        $this->last_error = null;
    }


    /**
     * Return requested URL
     */
    public function get_request_url()
    {
        return $this->client ? $this->client->getUri() : null;
    }


    /**
     * Check if last action was succedded
     */
    public function is_last_action_success()
    {
        // HTTP response is OK
        if ( $this->response && $this->response->isSuccessful() )
        {
            // Check if error has been returned into the response
            $last_error = $this->get_last_error();
            if ( empty($last_error) )
            {
                return true;
            }
        }

        return false;
    }


    /**
	 * Save request and response in a LOG (database or file)
	 */
	public function save_log($category = 'rest', $log_destination = 'file')
	{
		if ( $this->client )
		{
            switch ( $log_destination )
            {
                // Save log into database
                case 'db':
                    // Process response
                    $response_json = null;
                    if ( $this->response )
                    {
                        // Get response body
                        $response = $this->response->isSuccessful() ? trim($this->response->getBody()) : trim($this->response->getRawBody());

                        // Encode in JSON format?
                        if ( !empty($response) )
                        {
                            $response_json = StringHelper::is_json($response) ? $response : Json::encode($response);
                        }
                    }

                    $log_api_model = Yii::createObject(LogApi::class);
                    $log_api_model->setAttributes([
                        'api_name'              => $category,
                        'request_type'          => $this->method,
                        'request_url'           => $this->get_request_url(),
                        'request_endpoint'      => $this->method .'___'. $this->uri,
                        'entity_id'             => $this->entity_id,
                        'entity_type'           => $this->entity_type,
                        // 'request_hostname'      => Yii::app()->getRequest()->getUserHostAddress(),
                        'request_input_json'    => is_array($this->input) ? Json::encode($this->input) : $this->input,
                        'response_http_code'    => $this->response ? $this->response->getStatus() : 200,
                        'response_json'         => $response_json
                    ]);
                    
                    if ( ! $log_api_model->save() )
                    {
                        Yii::log('ERROR saving LogApi model: '. print_r($log_api_model->getErrors(), true), "profile", "rest_error");
                        return false;
                    }
                    else
                    {
                        $this->log_api_id = $log_api_model->log_api_id;
                    }
                break;

                // Save log in a log file
                case 'file':
        			$message  = "\n";
                    if ( !empty($this->entity_type) )
                    {
                        $message .= " - Entity: ". $this->entity_type;
                        if ( !empty($this->entity_id) )
                        {
                            $message ." #". $this->entity_id;
                        }
                        $message ."\n";
                    }
        			$message .= " - Method: ". $this->method ."\n";
        			$message .= " - URI: ". $this->get_request_url() ."\n";
                    $message .= " - Endpoint: ". $this->uri ."\n";

                    $json_input = is_array($this->input) ? Json::encode($this->input) : $this->input;
        			$message .= " - Input params: ". $json_input ."\n";
        			
                    if ( $this->response )
        			{
        				if ( $this->response->isSuccessful() )
        				{
        					$message .= " - Response (OK - HTTP code ". $this->response->getStatus() ."): ". trim($this->response->getBody()) ."\n";
        				}
        				else
        				{
        					$message .= " - Response (ERROR - HTTP code ". $this->response->getStatus() ."): ". trim($this->response->getRawBody()) ."\n";
        				}
        			}
        			else
        			{
        				$message .= " - ERROR - NO RESPONSE \n";
        			}
        			Yii::log($message, 'profile', $category);
			     break;
            }

            return true;
		}

		return false;
	}


    /**
     * Save request and response in a LOG (database)
     */
    public function save_db_log($category = 'rest')
    {
        return $this->save_log($category, 'db');
    }


    /**
     * Save entity information: entity_id and entity_type
     */
    public function save_entity_info($entity_type, $entity_id = null)
    {
        $this->entity_type = $entity_type;
        $this->entity_id = $entity_id;
        
        // Does it exist a LOG saved in database? If so, update it with new entity information
        if ( !empty($this->log_api_id) )
        {
            Yii::app()->db->createCommand("UPDATE log_api SET entity_type = :entity_type, entity_id = :entity_id WHERE log_api_id = :log_api_id")
                ->bindValue(':entity_type', $this->entity_type)
                ->bindValue(':entity_id', $this->entity_id)
                ->bindValue(':log_api_id', $this->log_api_id)
                ->execute();
        }

        return true;
    }
}
