<?php

/**
 * The DzDompdf application component class
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @version 0.1
 * @link http://code.google.com/p/dompdf/
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @package application.extensions.dompdf.DzDompdf
 *
 */

namespace dz\pdf;

use dz\base\ApplicationComponent;
use Yii;

class DzDompdf extends ApplicationComponent
{
	/**
	 * The path to the directory where DOMPDF library is stored
	 *
	 * @var string
	 */
	// protected $libraryPath = '@app.lib.dompdf';
	protected $libraryPath = '@vendor.dompdf';

	/**
	 * The path to the directory where generated PDF files are stored
	 *
	 * @var string
	 */
	protected $destinationPath = '@www.files.pdf';
	
	
	/**
	 * List of constant value used in DOMPDF application for configuration
	 *
	 * @var array
	 */
	protected $constants = array();
		
	/**
	 * The internal DOMPDF object.
	 *
	 * @var object DOMPDF
	 */
	private $_dompdf;

   /**
    * Constructor. Here the instance of DOMPDF is created.
    */
	public function __construct()
	{
		if ( empty($this->_dompdf) )
		{
			$this->initDompdf();
		}
		$this->_dompdf = new \DOMPDF();
	}
	
	
   /**
    * Initialize dompdf library
    */
    protected function initDompdf()
    {
        if ( !isset($this->libraryPath) )
		{
            throw new \DzDompdfException(Yii::t('dz-dompdf', 'You must set parameters first'), 500);
		}

		// Problem with Yii autoload method and DOMPDF
		// --> http://www.yiiframework.com/forum/index.php/topic/33423-problem-getting-dompdf-to-work
		
		// Temporary directory
		$temp_dir = Yii::app()->params['temporaryPath'];
		if ( empty($temp_dir) )
		{
			$temp_dir = sys_get_temp_dir();
		}
		define('DOMPDF_TEMP_DIR', $temp_dir);
		
		// unregister Yii's autoloader
		spl_autoload_unregister(array('YiiBase', 'autoload'));
		
		// register dompdf's autoloader
		require_once Yii::getAlias('@core.src.pdf') . DIRECTORY_SEPARATOR . 'dompdf_config.inc.php';
		
		// register Yii's autoloader again
		spl_autoload_register(array('YiiBase', 'autoload'));
		
		// Constants
		if ( isset($this->constants) )
		{
	        foreach( (array)$this->constants as $constantName => $constantValue )
			{
				define($constantName, $constantValue);
			}
		}
    }


   //====================================
   // Magic functions
   //====================================

	/**
	 * Call a DOMPDF function
	 *
	 * @param string $method the method to call
	 * @param array $params the parameters
	 * @return mixed
	 */
	public function __call($method, $params)
	{
		if ( is_object($this->_dompdf) && get_class($this->_dompdf) === 'DOMPDF' )
		{
			return call_user_func_array(array($this->_dompdf, $method), $params);
		}
		else
		{
			throw new \CException(Yii::t('DzDompdf', 'Can not call a method of a non existent object'));
		}
	}

	/**
	 * Setter
	 *
	 * @param string $name the property name
	 * @param string $value the property value
	 */
	public function __set($name, $value)
	{
		if ( is_object($this->_dompdf) && get_class($this->_dompdf) === 'DOMPDF' )
		{
			$this->_dompdf->$name = $value;
		}
		else
		{
			throw new \CException(Yii::t('DzDompdf', 'Can not set a property of a non existent object'));
		}
	}

	/**
	 * Getter
	 *
	 * @param string $name
	 * @return mixed
	 */
	public function __get($name)
	{
		if ( is_object($this->_dompdf) && get_class($this->_dompdf) === 'DOMPDF' )
		{
			return $this->_dompdf->$name;
		}
		else
		{
			throw new \CException(Yii::t('DzDompdf', 'Can not access a property of a non existent object'));
		}
	}

	/**
	 * Cleanup work before serializing.
	 * This is a PHP defined magic method.
	 * @return array the names of instance-variables to serialize.
	 */
	public function __sleep()
	{
	}

	/**
	 * This method will be automatically called when unserialization happens.
	 * This is a PHP defined magic method.
	 */
	public function __wakeup()
	{
	}
	
	
   //====================================
   // Utilities
   //====================================
	public function generatePDF($content, $fileName, $filePath = '', $orientation = '')
	{
		// Aumentamos la reserva de memoria
		// ini_set("memory_limit","128M");

		if ( empty($filePath) )
		{
			$filePath = Yii::getPathOfAlias($this->destinationPath);
		}
		
		// Check filename extension is ".pdf"
		if ( ! preg_match("/\.pdf$/", $fileName) )
		{
			$fileName .= '.pdf';
		}
		
		// Paper size
		// CPDF_Adapter::$PAPER_SIZES['damm_custom'] = array(0,0,595.28,841.89);	// "a4" => array(0,0,595.28,841.89),
		\CPDF_Adapter::$PAPER_SIZES['a4'] = array(0,0,595.28,841.89);
		Yii::app()->dompdf->set_paper('a4', $orientation);
		
		// Render HTML content to PDF
		Yii::app()->dompdf->load_html(utf8_decode($content));
		Yii::app()->dompdf->render();
		// Yii::app()->dompdf->stream("sample.pdf");
		$pdfContent = Yii::app()->dompdf->output();
		
		// Save PDF file
		$pdfPath = $filePath. DIRECTORY_SEPARATOR .$fileName;
		file_put_contents($pdfPath, $pdfContent);

		if ( file_exists($pdfPath) )
		{
			return TRUE;
		}
		return FALSE;
	}
}

