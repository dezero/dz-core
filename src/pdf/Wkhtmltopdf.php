<?php

/**
 * The Wkhtmltopdf application component class
 * @link https://github.com/mikehaertl/phpwkhtmltopdf
 *
 * Some functions based on Laravel Snappy library
 * @see https://github.com/barryvdh/laravel-snappy/blob/master/src/PdfWrapper.php
 * 
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @license http://www.opensource.org/licenses/bsd-license.php
 */

namespace dz\pdf;

use dz\base\ApplicationComponent;
use mikehaertl\wkhtmlto\Pdf;
use Yii;

class Wkhtmltopdf extends ApplicationComponent
{
	/**
	 * The path where WkHtmlToPdf binary program is saved
	 *
	 * @var string
	 */
	public $binary_path = '/usr/local/bin/wkhtmltopdf';


	/**
	 * The path to the directory where generated PDF files are stored
	 *
	 * @var string
	 */
	public $destination_path = '@www.files.pdf';


    /**
     * Temporary path (PRIVATE temp path)
     *
     * @var string
     */
    public $tmp_path = '@privateTmp.pdf';


	/**
	 * HTML content to be exported
	 */
	public $html;


    /**
     * PDF Instance
     */
    protected $pdf;


	/**
     * Init function
     */
    public function init()
    {
        parent::init();

        // Get local settings
        if ( isset(Yii::app()->params['pdf']) )
    	{
            if ( isset(Yii::app()->params['pdf']['binary_path']) )
            {
                $this->binary_path = Yii::app()->params['pdf']['binary_path'];
            }

            if ( isset(Yii::app()->params['pdf']['destination_path']) )
            {
                $this->destination_path = Yii::app()->params['pdf']['destination_path'];
            }
    	}

        // Init PDF instance with default options
        $tmp_path = Yii::getAlias($this->tmp_path);
        $this->pdf = new Pdf([
            // Binary path where "wkhtmltopdf" command is located
        	'binary'       => $this->binary_path,

            // Do not put an outline into the pdf. Make Chrome not complain
            'no-outline',

            // Temp directory
            'tmpDir'        => $tmp_path,

            // Margins
            'margin-top'    => 0,
            'margin-right'  => 0,
            'margin-bottom' => 0,
            'margin-left'   => 0,

            // Paper configuration
            'page-size'     => 'A4',
            'orientation'   => 'Portrait', // Landscape
            'dpi'           => '300',

            // Whether to ignore any errors if a PDF file was still created
            'ignoreWarnings' => true,

            // Language and encoding options
            'encoding' => 'UTF-8',
            'commandOptions' => [
                'useExec' => true,      // Can help if generation fails without a useful error message
                'procEnv' => [
                    // Check the output of 'locale -a' on your system to find supported languages
                    'LANG' => 'en_US.utf-8',
                ],
            ],

            // Disable the intelligent shrinking strategy used by WebKit that makes the pixel/dpi ratio none constant
            'disable-smart-shrinking',

            // Be less verbose, maintained for backwards compatibility; Same as using --log-level none
            'quiet'
        ]);

        // Options on "params.php" file
        if ( isset(Yii::app()->params['pdf']) AND isset(Yii::app()->params['pdf']['options']) )
        {
            $this->setOptions(Yii::app()->params['pdf']['options']);
        }

        // Check if "tmp_path" exists
        $temp_dir = Yii::app()->file->set($tmp_path);
        if ( ! $temp_dir->isDir )
        {
            $default_permissions = 755;
            $temp_dir->createDir($default_permissions, $tmp_path);
        }
    }


    /**
     * Returns the static object of this component
     */
    public static function component($className=__CLASS__)
    {
        return parent::component($className);
    }


    /**
     * Get the PDF instance
     *
     * @return \mikehaertl\wkhtmlto\Pdf
     */
    public function pdf()
    {
        return $this->pdf;
    }


    /**
     * Set temporary path
     */
     public function setTemporaryPath($path)
    {
         $this->pdf->setOptions(['tmpDir' => $path]);
         return $this;
    }


    /**
     * Set destination path
     */
    public function setDestinationPath($path)
    {
        $this->destination_path = $path;
        return $this;
    }


    /**
     * Set the paper size (default A4)
     */
    public function setPaper($paper_size, $orientation = null, $dpi = null)
    {
        $vec_options = ['page-size' => $paper_size];
        
        if ( $orientation )
        {
            $vec_options['orientation'] = $orientation;
        }

        if ( $dpi )
        {
            $vec_options['dpi'] = $dpi;
        }

        $this->setOptions($vec_options);
        return $this;
    }


    /**
     * Set the orientation (default Portrait)
     */
    public function setOrientation($orientation)
    {
        $this->pdf->setOptions(['orientation' => $orientation]);
        return $this;
    }


    /**
     * Set options to PDF object
     */
    public function setOptions($vec_options)
    {
        return $this->pdf->setOptions($vec_options);
    }


    /**
     * Set HTML content to be saved or send
     */
    public function loadHtml($html)
    {
    	$this->html = $html;
    }


    /**
     * Reset the component
     */
    public function reset()
    {
        $this->html = null;
        $this->init();
    }


    /**
     * Generate PDF and save as a file
     */
    public function save($file_name, $file_path = null, $html = null)
    {
    	if ( ! $file_path )
		{
            $file_path = $this->destination_path;
            if ( Yii::isAlias($file_path) )
            {
                $file_path = Yii::getAlias($this->destination_path);
            }
		}
		
		// Check filename extension is ".pdf"
		if ( ! preg_match("/\.pdf$/", $file_name) )
		{
			$file_name .= '.pdf';
		}

    	if ( $html )
    	{
    		$this->html = $html;
    	}

    	$this->pdf->addPage($this->html);
    	$result = $this->pdf->saveAs($file_path . DIRECTORY_SEPARATOR . $file_name);
    	if ( ! $result )
    	{
    		$this->add_error($this->pdf->getError());

    		return false;
    	}

    	return true;
    }


    /**
     * Generate PDF and send to download
     */
    public function download($file_name, $html = '')
    {
        // Check filename extension is ".pdf"
        if ( ! preg_match("/\.pdf$/", $file_name) )
        {
            $file_name .= '.pdf';
        }

    	if ( $html )
    	{
    		$this->html = $html;
    	}

        // Add content
    	$this->pdf->addPage($this->html);

        // Send to client to be downloaded
    	$result = $this->pdf->send($file_name);
    	if ( ! $result )
    	{
    		$this->add_error($this->pdf->getError());

    		return false;
    	}

    	return true;
    }
}
