<?php
/**
 * Controller class file for CONSOLE applications
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2021 Dezero
 * @see https://gist.github.com/thinkt4nk/1004737
 */

namespace dz\console;

use dz\web\Controller as WebController;
use Yii;

/**
 * Controller class file for CONSOLE applications
 */
class Controller extends WebController
{
    /**
     * View path
     */
    public $viewPath;


    /**
     * View file extension
     */
    public $viewExtension = '.tpl.php';


    /**
     * Renders a partial view
     */
    public function renderPartial($view, $data = null, $return = false, $processOutput = false)
    {
        if ( ($viewFile = $this->getViewFile($view) ) !== false )
        {
            $output = $this->renderFile($viewFile, $data, true);
            if ( $return )
            {
                return $output;
            }
            else
            {
                echo $output;
            }
        }
        else
        {
            throw new \CException(Yii::t('yii','{controller} cannot find the requested view "{view}".',
                array('{controller}'=>get_class($this), '{view}'=>$view)));
        }
    }   
    

    /**
     * Renders a view file
     */
    public function renderFile($viewFile, $data = null, $return = false)
    {
        return $this->renderInternal($viewFile, $data, $return);
    }

    
    /**
     * Looks for the view file according to the given view name
     */
    public function getViewFile($viewName)
    {
        return $this->resolveViewFile($viewName, $this->getViewPath(), $this->getViewPath());
    }


    /**
     *  Returns the directory containing view files for this controller
     */
    public function getViewPath()
    {
        return !empty($this->viewPath) ? $this->viewPath : Yii::getAlias('@theme.views');
    }


    /**
     * Finds a view file based on its name.
     */
        public function resolveViewFile($viewName, $viewPath, $basePath, $moduleViewPath = null)
    {
        if ( empty($viewName) )
        {
            return false;
        }

        if ( $moduleViewPath === null )
        {
            $moduleViewPath = $basePath;
        }

        // View file extensions
        $extension = $this->viewExtension;

        if ( $viewName[0] === '/' )
        {
            if ( strncmp($viewName,'//',2) === 0 )
            {
                $viewFile = $basePath . $viewName;

                // <DEZERO> - Vendor modules
                if ( preg_match("/\/app\/vendor\//", $viewPath) )
                {
                    // Transform from "//commerce/order/index" to "/order/index"
                    $vec_view_name = explode("/", $viewName);
                    if ( count($vec_view_name) > 3 )
                    {
                        unset($vec_view_name[0]);
                        unset($vec_view_name[1]);
                        unset($vec_view_name[2]);
                        $viewFile = $moduleViewPath . "/". implode("/", $vec_view_name);
                    }
                }
                // <DEZERO> - Vendor modules
            }
            else
            {
                $viewFile = $moduleViewPath . $viewName;
            }
        }
        elseif ( strpos($viewName,'.') )
        {
            $viewFile = Yii::getPathOfAlias($viewName);
        }
        else
        {
            $viewFile = $viewPath . DIRECTORY_SEPARATOR . $viewName;
        }

        
        // <DEZERO> - Avoid double dashes
        // $viewFile = str_replace('//', '/', $viewFile);
        /*
        \DzLog::dev([
            'viewName' => $viewName,
            'viewPath' => $viewPath,
            'basePath' => $basePath,
            'moduleViewPath' => $moduleViewPath,
            'viewFile' => $viewFile
        ]);
        */

        if ( is_file($viewFile . $extension) )
        {
            return Yii::app()->findLocalizedFile($viewFile . $extension);
        }
        elseif ( $extension !== '.php' && is_file($viewFile.'.php') )
        {
            return Yii::app()->findLocalizedFile($viewFile.'.php');
        }

        return false;
    }


    /**
     * Renders a view file
     * 
     * This method includes the view file as a PHP script
     * and captures the display result if required.
     */
    /*
    public function renderInternal($_viewFile_, $_data_ = null, $_return_ = false)
    {
        // we use special variable names here to avoid conflict when extracting data
        if ( is_array($_data_) )
        {
            extract($_data_,EXTR_PREFIX_SAME,'data');
        }
        else
        {
            $data = $_data_;
        }

        if ($_return_)
        {
            ob_start();
            ob_implicit_flush(false);
            require($_viewFile_);
            return ob_get_clean();
        }
        else
        {
            require($_viewFile_);
        }
    }
    */ 
}