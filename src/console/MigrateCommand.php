<?php
/**
 * MigrateCommand class file for Dz Framework
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es/
 */

namespace dz\console;

use dz\db\Migration;
use dz\db\SchemaBuilderTrait;
use dz\helpers\DateHelper;
use dz\helpers\Html;
use dz\helpers\StringHelper;
use Dz;
use Yii;

Yii::import('system.cli.commands.MigrateCommand');

/**
 * MigrateCommand is the base class for cron tasks
 */
class MigrateCommand extends \MigrateCommand
{
    use SchemaBuilderTrait;

    /**
     * Current module (optional) to use for current command
     *
     * Examples:
     *  --module=product
     */
    public $module = null;


    /**
     * Core migration path
     */
    public $core_migration_path = '@core.src.migrations';


    /**
     * Array with migrations information
     */
    public $vec_migrations = [];


    /**
     * Get the path of migration
     */
    public function beforeAction($action, $params)
    {
        // Save original core migration path
        $this->core_migration_path = Yii::getAlias($this->migrationPath);

        // Get migration path if "--module" parameter is present
        if ( !empty($this->module) )
        {
            if ( !is_string($this->module) )
            {
                echo 'Parameter --module must be a comma seperated list of modules or a single module name!';
                exit;
            }

            else
            {
                $vec_modules = explode(',', $this->module);

                foreach ( $vec_modules as $que_module )
                {
                    $core_module = str_replace("core_", "", $que_module);
                    $dzlab_module = str_replace("dzlab_", "", $que_module);
                    if ( ! Yii::app()->hasModule($que_module) && ! Yii::app()->hasModule($core_module) && ! Yii::app()->hasModule($dzlab_module) )
                    {
                        echo 'Module '. $que_module .' does not exists';
                        exit;
                    }
                    else
                    {
                        $migration_path = $this->getMigrationPathByModule($que_module);
                        $migration_dir = Yii::app()->file->set($migration_path);
                        if ( $migration_dir->getExists() && $migration_dir->getIsDir() )
                        {
                            if ( ! preg_match("/^core\_|^dzlab\_/", $que_module) )
                            {
                                $this->migrationPath = 'app.modules.'. $que_module .'.migrations';
                            }
                            else
                            {
                                // Core module
                                if ( preg_match("/^core\_/", $que_module) )
                                {
                                    $this->migrationPath = 'app.core.src.modules.'. $core_module .'.migrations';
                                }

                                // Dz Lab module
                                else
                                {
                                    $this->migrationPath = 'app.vendor.dezero.'. $dzlab_module .'.src.migrations';
                                }
                            }
                        }
                        else
                        {
                            echo 'Directory '. $migration_path .' is not a valid directory';
                            exit;
                        }
                    }
                }
            }
        }

        return parent::beforeAction($action, $params);
    }


    /**
     * Extended from MigrateCommand.migrateUp()
     */
    protected function migrateUp($class)
    {
        if($class===self::BASE_MIGRATION)
            return;

        echo "*** applying $class\n";
        $start = microtime(true);
        $this->migrationPath = $this->getMigrationPath($class);
        $migration = $this->instantiateMigration($class);
        if ( $migration->up() !== false )
        {
            $vec_attributes = [
                'name'          => $class,
                'apply_date'    => DateHelper::mysql_date(),
                'uuid'          => StringHelper::UUID()
            ];
            if ( !empty($this->module) )
            {
                $vec_attributes['module'] = $this->module;
            }

            $this->getDbConnection()->createCommand()->insert($this->migrationTable, $vec_attributes);
            $time = microtime(true) - $start;
            echo "*** applied $class (time: ".sprintf("%.3f",$time)."s)\n\n";
        }
        else
        {
            $time = microtime(true) - $start;
            echo "*** failed to apply $class (time: ".sprintf("%.3f",$time)."s)\n\n";
            return false;
        }
    }


    /**
     * Override MigrateCommand.actionTo()
     */
    public function actionTo($args)
    {
        if( ! isset($args[0]) )
        {
            $this->usageError('Please specify which version, timestamp or datetime to migrate to.');
        }

        if ( (string)(int)$args[0] == $args[0] )
        {
            return $this->migrateToTime($args[0]);
        }
        elseif ( ($time=strtotime($args[0])) !== false )
        {
            return $this->migrateToTime($time);
        }
        
        return $this->migrateToVersion($args[0]);
    }


    /**
     * Override MigrateCommand.actionMark()
     */
    public function actionMark($args)
    {
        if ( isset($args[0]) )
        {
            $version = $args[0];
        }
        else
        {
            $this->usageError('Please specify which version to mark to.');
        }

        $originalVersion = $version;
        if ( preg_match('/^m?(\d{6}_\d{6})(_.*?)?$/',$version,$matches) )
        {
            $version = 'm'. $matches[1];
        }
        else
        {
            echo "Error: The version option must be either a timestamp (e.g. 101129_185401)\nor the full name of a migration (e.g. m101129_185401_create_user_table).\n";
            return 1;
        }

        $db = $this->getDbConnection();

        // try mark up
        $vec_migrations = $this->getNewMigrations();
        foreach ( $vec_migrations as $i => $migration )
        {
            if ( strpos($migration, $version .'_') === 0 )
            {
                if ( $this->confirm("Set migration history at $originalVersion?") )
                {
                    $command = $db->createCommand();
                    for ( $j=0; $j<= $i; ++$j )
                    {
                        $que_migation = $vec_migrations[$j];
                        $vec_attributes = [
                            'name'          => $que_migation,
                            'apply_date'    => DateHelper::mysql_date(),
                            'uuid'          => StringHelper::UUID(),
                            'module'        => 'core'
                        ];

                        if ( !empty($this->vec_migrations) && isset($this->vec_migrations[$que_migation]) && isset($this->vec_migrations[$que_migation]['module']) && $this->vec_migrations[$que_migation]['module'] != 'core' )
                        {
                            $vec_attributes['module'] = $this->vec_migrations[$que_migation]['module'];
                        }
                        $command->insert($this->migrationTable, $vec_attributes);
                    }
                    echo "The migration history is set at $originalVersion.\nNo actual migration was performed.\n";
                }
                return 0;
            }
        }

        // try mark down
        $vec_migrations = array_keys($this->getMigrationHistory(-1));
        foreach ( $vec_migrations as $i => $migration )
        {
            if ( strpos($migration, $version .'_') === 0 )
            {
                if ( $i===0 )
                {
                    echo "Already at '$originalVersion'. Nothing needs to be done.\n";
                }
                else
                {
                    if ( $this->confirm("Set migration history at $originalVersion?") )
                    {
                        $command = $db->createCommand();
                        for( $j=0; $j<$i; ++$j )
                        {
                            $command->delete($this->migrationTable, $db->quoteColumnName('name').'=:name', [':name'=>$vec_migrations[$j]]);
                        }
                        echo "The migration history is set at $originalVersion.\nNo actual migration was performed.\n";
                    }
                }
                return 0;
            }
        }

        echo "Error: Unable to find the version '$originalVersion'.\n";
        return 1;
    }


    /**
     * Extended from MigrateCommand.actionMark()
     */
    protected function getNewMigrations()
    {
        $vec_applied = [];
        foreach ( $this->getMigrationHistory(-1) as $version => $time )
        {
            $vec_applied[substr($version,1,13)] = true;
        }

        $vec_migrations = [];

        // Get all Yii modules and Dz Core modules to check if there are new migrations
        $vec_modules = $this->getModules();
        foreach ( $vec_modules as $module_id => $que_module )
        {
            // Get migration path for each module
            $que_migration_path = $this->getMigrationPathByModule($module_id);

            // Check if migration directory exists
            $migration_dir = Yii::app()->file->set($que_migration_path);
            if ( $migration_dir->getExists() && $migration_dir->getIsDir() )
            {
                $handle = opendir($que_migration_path);
                while( ($file=readdir($handle)) !== false )
                {
                    if ( $file==='.' || $file==='..' )
                    {
                        continue;
                    }

                    $path = $que_migration_path . DIRECTORY_SEPARATOR . $file;

                    if ( preg_match('/^(m(\d{6}_\d{6})_.*?)\.php$/', $file,$matches) && is_file($path) && !isset($vec_applied[$matches[2]]) )
                    {
                        $que_migration = $matches[1];
                        $this->vec_migrations[$que_migration] = [
                            'module'        => $que_module,
                            'file_path'     => $path
                        ];
                        $vec_migrations[] = $que_migration;
                    }
                }
                closedir($handle);
            }
        }

        sort($vec_migrations);
        
        return $vec_migrations;
    }


    /**
     * Get old migration
     */
    protected function getMigrationHistory($limit)
    {
        $db = $this->getDbConnection();
        if ( $db->schema->getTable($this->migrationTable,true)===null )
        {
            $this->createMigrationHistoryTable();
        }

        return Html::listData($db->createCommand()
            ->select('name, apply_date')
            ->from($this->migrationTable)
            ->order('name DESC')
            ->limit($limit)
            ->queryAll(), 'name', 'apply_date');
    }


    /**
     * Create Migration History Module
     */
    protected function createMigrationHistoryTable()
    {
        $db = $this->getDbConnection();
        echo 'Creating migration history table "'.$this->migrationTable.'"...';
        
        // CREATE TABLE
        $db->createCommand()->createTable($this->migrationTable, [
            'migration_id'  => $this->primaryKey(),
            'name'          => $this->string()->notNull(),
            'apply_date'    => $this->timestamp()->notNull(),
            'module'        => $this->string(64),
            'uuid'          => $this->uuid(),
        ]);

        // Add indexes
        $db->createCommand()->createIndex($db->getIndexName($this->migrationTable, ['module'], false), $this->migrationTable, ['module'], false);
        $db->createCommand()->createIndex($db->getIndexName($this->migrationTable, ['uuid'], false), $this->migrationTable, ['uuid'], false);
        // $db->createCommand()->createIndex('migration_module_index', $this->migrationTable, 'module');

        // INSERT DATA
        $db->createCommand()->insert($this->migrationTable, [
            'name'          => self::BASE_MIGRATION,
            'apply_date'    => DateHelper::mysql_date(),
            'module'        => 'core',
            'uuid'          => StringHelper::UUID()
        ]);

        echo "done.\n";
    }


    /**
     * Get the migration path of a migration file
     */
    protected function getMigrationPath($migration_class)
    {
        // Get all Yii modules and Dz Core modules to check if there are new migrations
        $vec_modules = $this->getModules();
        foreach ( $vec_modules as $module_id => $que_module )
        {
            // Get migration path for each module
            $que_migration_path = $this->getMigrationPathByModule($module_id);

            // Check if migration file exists
            $migration_file = Yii::app()->file->set($que_migration_path . DIRECTORY_SEPARATOR . $migration_class .'.php');
            if ( $migration_file->getExists() && $migration_file->getIsFile() )
            {
                $this->module = $module_id;
                return $que_migration_path;
            }
        }

        // Default value
        $this->module = 'core';
        return $this->core_migration_path;
    }


    /**
     * Return Yii modules and Dz Core & Contrib modules to check migrations
     */
    public function getModules()
    {
        // Core modules
        $vec_modules = ['core' => 'core'];
        $vec_core_modules = Dz::getCoreModules();
        if ( !empty($vec_core_modules) )
        {
            foreach ( $vec_core_modules as $module_id => $que_module )
            {
                if ( Yii::app()->hasModule($module_id) )
                {
                    $vec_modules['core_'. $module_id] = $que_module;
                }
            }
        }

        $vec_dzlab_modules = Dz::getContribModules();
        if ( !empty($vec_dzlab_modules) )
        {
            foreach ( $vec_dzlab_modules as $module_id => $que_module )
            {
                if ( Yii::app()->hasModule($module_id) )
                {
                    $vec_modules['dzlab_'. $module_id] = $que_module;
                }
            }
        }

        return \CMap::mergeArray($vec_modules, Yii::app()->getModules());
    }


    /**
     * Get migration path given a module
     */
    public function getMigrationPathByModule($module_id)
    {
        // Core global path on {$this->migrationPath}
        if ( $module_id == 'core' )
        {
            return $this->core_migration_path;
        }

        // Core modules path on "core.src.modules"
        else if ( preg_match("/^core\_/", $module_id) )
        {
            return Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR .'app'. DIRECTORY_SEPARATOR .'core'. DIRECTORY_SEPARATOR .'src'. DIRECTORY_SEPARATOR .'modules'. DIRECTORY_SEPARATOR . str_replace("core_", "", $module_id) . DIRECTORY_SEPARATOR .'migrations';
        }

        // Dz contrib modules path on "vendor.dezero"
        else if ( preg_match("/^dzlab\_/", $module_id) )
        {
            return Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR .'app'. DIRECTORY_SEPARATOR .'vendor'. DIRECTORY_SEPARATOR .'dezero'. DIRECTORY_SEPARATOR . str_replace("dzlab_", "", $module_id) . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR .'migrations';
        }

        // App modules path on "app.modules"
        return Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR .'app'. DIRECTORY_SEPARATOR .'modules'. DIRECTORY_SEPARATOR . $module_id . DIRECTORY_SEPARATOR .'migrations';
    }


    /**
     * Override private method migrateToTime with a new name: _migrateToTime
     */
    private function _migrateToTime($time)
    {
        $data=$this->getDbConnection()->createCommand()
            ->select('name, apply_date')
            ->from($this->migrationTable)
            ->where('apply_date <= :time',[':time'=>$time])
            ->order('apply_date DESC')
            ->limit(1)
            ->queryRow();

        if ( $data===false )
        {
            echo "Error: Unable to find a version before ". date('Y-m-d H:i:s',$time). ".\n";
            return 1;
        }
        
        echo "Found version ". $data['version'] ." applied at ". date('Y-m-d H:i:s',$data['apply_time']). ", it is before ". date('Y-m-d H:i:s',$time).".\n";
        
        return $this->migrateToVersion(substr($data['version'], 1, 13));
    }
}
