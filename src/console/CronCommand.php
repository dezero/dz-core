<?php
/**
 * CronCommand class file
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2014 Dezero
 */

namespace dz\console;

use Yii;

/**
 * CronCommand is the base class for cron tasks
 */
abstract class CronCommand extends \CConsoleCommand
{
    /**
     * This method is invoked right before an action is to be executed.
     * You may override this method to do last-minute preparation for the action.
     * 
     * @param string $action the action name
     * @param array $params the parameters to be passed to the action method.
     * @return boolean whether the action should be executed.
     */
    protected function beforeAction($action,$params)
    {
        // Enable "log" in real time
        $this->set_log_realtime(TRUE);

        return parent::beforeAction($action, $params);
    }


	/**
	 * Save a log message
	 */
    public function log($log_message, $category = 'cron')
    {
    	if ( is_array($log_message) )
    	{
    		$log_message = \CJSON::encode($log_message);
    	}
    	Yii::log($log_message, "profile", $category);
    	return TRUE;
    }


    /**
     * Set real time logging mode
     */
    public function set_log_realtime($is_active = TRUE)
    {
        // Enable realtime logging
        if ( $is_active )
        {
            Yii::getLogger()->autoDump = true;
            Yii::getLogger()->autoFlush = 1;
        }

        // Yii default values
        else
        {
            Yii::getLogger()->autoDump = false;
            Yii::getLogger()->autoFlush = 10000;
        }
    }
}
