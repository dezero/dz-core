<?php
/**
 * Migration class {ClassName}
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class {ClassName} extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		/*
		// Create "my_table" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('my_table', true);

        $this->createTable('my_table', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128),
            'description' => $this->text(),
            'reference_id' => $this->integer()->unsigned()->notNull(),
            'is_important' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'content_type' => $this->enum('content_type', ['page', 'block', 'text']),
            'disable_date' => $this->date(),
            'disable_uid' => $this->integer()->unsigned(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);
    
        // Primary key (alternative method)
        $this->addPrimaryKey(null, 'my_table', 'id');

        // Create indexes
        $this->createIndex(null, 'my_table', ['name'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'my_table', ['disable_uid'], 'user_users', ['id'], 'SET NULL', null);
        $this->addForeignKey(null, 'my_table', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'my_table', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

        
        // Permissions
        // -------------------------------------------------------------------------
        $this->insertMultiple('user_auth_item', [
            // Create new role(s)
            [
                'name'          => 'manager',
                'type'          => 2,
                'item_type'     => 'role',
                'description'   => 'Manager',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],

            // Define operations
            [
                'name'          => 'settings.language.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Settings - Language - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'settings.language.create',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Settings - Language - Create new languages',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'settings.language.update',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Settings - Language - Edit languages',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'settings.language.view',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Settings - Language - View languages',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],

            // Define tasks
            [
                'name'          => 'language_manage',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Settings - Full access to languages',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'language_edit',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Settings - Edit languages',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'language_view',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Settings - View languages',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
        ]);

        // Set items relationships
        $this->insertMultiple('user_auth_item_child', [
            [
                'parent'    => 'language_manage',
                'child'     => 'settings.language.*'
            ],
            [
                'parent'    => 'language_edit',
                'child'     => 'settings.language.update'
            ],
            [
                'parent'    => 'language_edit',
                'child'     => 'settings.language.view'
            ],
            [
                'parent'    => 'language_view',
                'child'     => 'settings.language.view'
            ],
            [
                'parent'    => 'admin',
                'child'     => 'language_manage'
            ],
            [
                'parent'    => 'editor',
                'child'     => 'language_edit'
            ],
        ]);


		/*
		// Add new column via $this->addColumn
		$this->addColumn('my_table', 'description', $this->string(255)->after('name'));
		*/

		/*
		// Alter column via $this->alterColumn
		$this->alterColumn('my_table', 'name', $this->string(128)->notNull());
		*/

        /*
        // Rename column via $this->renameColumn
        $this->renameColumn('my_table', 'name', 'new_name');
        */

        /*
        // Rename table via $this->renameTable
        $this->renameTable('my_table', 'new_name');
        */

        /*
        // Disable FOREIGH KEY check integrity
        $this->getDbConnection()->getSchema()->checkIntegrity(false);

        $this->dropTableIfExists('my_table');

        // Enable again FOREIGH KEY check integrity
        $this->getDbConnection()->getSchema()->checkIntegrity(true);
        */

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

