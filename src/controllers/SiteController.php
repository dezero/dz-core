<?php
/*
|--------------------------------------------------------------------------
| Controller class for site
|--------------------------------------------------------------------------
*/

use dz\web\Controller;

class SiteController extends Controller
{
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ( $error = Yii::app()->errorHandler->error )
        {
            // Errors on REST API?
            if ( preg_match("/^api\/v/", Yii::app()->request->getPathInfo()) )
            {
                Yii::import('@core.components.DzRest.*');
                $controller = new DzRestController('api');
                $controller->actionError();
            }

            else
            {
                if ( Yii::app()->request->isAjaxRequest )
                {
                    echo $error['message']; 
                }
                else
                {
                    $this->render('//layouts/error', $error);
                }
            }
        }
    }
}
