<?php
/*
|--------------------------------------------------------------------------
| Controller class for searching models
|--------------------------------------------------------------------------
*/

use dz\helpers\Html;
use dz\web\Controller;

class SearchController extends Controller
{
	private $_model;
	
	/**
	 * Creates the action instance based on the action name
	 *
	 * @param string	ID of the action
	 * @return CAction
	 */
	public function createAction($action_id)
	{
		if ( $action_id == 'global' )
		{
			return new CInlineAction($this, $action_id);
		}
		else
		{
			$que_model_name = ucfirst($action_id);
			if ( class_exists($que_model_name) )
			{
				// Same as $this->_model_name::model()
				$this->_model = call_user_func(array($que_model_name, 'model'));
				return new CInlineAction($this, 'search');
			}
		}
	}

	
	/**
	 * AJAX call to search models by name or ID
	 *
	 * @param mixed		Search term
	 * @return JSON
	 */
	public function actionSearch()
	{
		$que_label = $this->_model->label();
		$vec_return = array(
			// 'error' => 'It does not exist a '. $que_label .' with this code',
			'error' => Yii::t('app', 'It does not exist a '. $que_label .' with this code'),
			'results' => '',
		);

		if ( Yii::app()->request->isAjaxRequest )
		{
			$que_query = '';
			if ( isset($_GET['q']) AND !empty($_GET['q']) )
			{
				$que_query = Yii::app()->input->get('q');
			}

			// Min length = 3
			if ( strlen($que_query) > 2 )
			{
				// if ( !is_numeric($que_query) )
				// {
				// 	$que_query = str_replace(" ", "%", strtoupper($que_query));
				// }

				// Strip HTML Tags
				$strip_html_tags = FALSE;
				if ( isset($_GET['strip_html_tags']) AND !empty($_GET['strip_html_tags']) )
				{
					$strip_html_tags = Yii::app()->input->get('strip_html_tags');
				}

				// Limit result
				$que_limit = 10;
				if ( isset($_GET['page_limit']) AND !empty($_GET['page_limit']) )
				{
					$que_limit = Yii::app()->input->get('page_limit');
				}

				// Loaded entities in Global Search
				$model_class_name = get_class($this->_model);
				/*
				$vec_global_entities = array(
					'Product'	=> 'empresa_cliente',
				);
				*/


				/*
				|--------------------------------------------------------------------------
				| USE GLOBAL SEARCH (selected entities)
				|--------------------------------------------------------------------------
				*/
				if ( $model_class_name == 'Product' )
				{
					$sql = "SELECT product_id, product_code, product_name, product_description, active FROM search_product WHERE (";

					// Search by ID, it means search by Primary Key "ID"
					// if ( is_numeric($que_query) AND $que_query < 999999 )
					// {
					// 	// $sql .= "entity_id LIKE '". $que_query ."%') ORDER BY entity_id ASC";
					// 	$sql .= "entity_id = '". $que_query ."') ORDER BY entity_id ASC";
					// }

					// Search by STRING in name and description fields
					// else
					// {
						$sql .= "product_code LIKE '%". $que_query ."%' OR product_name LIKE '%". $que_query ."%' OR product_description LIKE '%". $que_query ."%') ";
						$sql .= "ORDER BY active DESC, CASE WHEN product_code LIKE '". $que_query ."%' THEN 0 ELSE 1 END, CASE WHEN product_name LIKE '%". $que_query ."%' THEN 0 ELSE 1 END ";
					// }
					$sql .= "LIMIT 0,$que_limit";

					// Debug SQL sentence
					// print $sql . "\n";

					$vec_return['error'] = '';
					$vec_rows = Yii::app()->db->createCommand($sql)->queryAll(); 
					if ( count($vec_rows) )
					{
						foreach ( $vec_rows as $que_row )
						{
							if ( $strip_html_tags )
							{
								$que_title = Html::encode($que_row['product_name']);
							}
							else
							{
								// $que_title = str_replace($que_query, '<em>'. $que_query .'</em>', Html::encode($que_row['product_name']));
								$que_title = $que_query;
								if ( strlen($que_title) > 60 )
								{
									$que_title = substr($que_title, 0, 60) .'&hellip;';
								}
								$que_title = str_replace($que_query, '<em>'. $que_query .'</em>', Html::encode($que_row['product_name']));
							}

							$product_image_url = FALSE;
							// $product_model = Product::model()->findByAttributes(array('sku' => $que_row['product_description']));
							$product_model = Product::model()->findByPk($que_row['product_id']);
							if ( $product_model )
							{
								$product_image_url = $product_model->preview_images(1, TRUE);
								$vec_return['results'][] = array(
									// 'id' => $que_row['product_code'],
									'id' => $que_row['product_id'],
									'name' => $que_title,
									'image' => $product_image_url,
									'status' => $product_model->status_type,
									'active' => $que_row['active'],
									'product_code' => $que_row['product_code']
								);
							}
						}
					}
				}


				/*
				|--------------------------------------------------------------------------
				| USE ajaxSearchResult() MODEL METHOD CLASS
				|--------------------------------------------------------------------------
				*/
				/*
				switch ( $model_class_name )
				{
					case 'Product':
						$vec_columns = array('sku', 'name');
						$vec_data = $this->_model->ajaxSearchResult($que_query, $vec_columns);
					break;

					default:
						$representing_column = $this->_model->representingColumn();
						$vec_columns = array($representing_column);

						$vec_data = $this->_model->ajaxSearchResult($que_query, $vec_columns, array('search_by_pk' => TRUE)); // , array('condition' => ''));
					break;
				}
				if ( count($vec_data) )
				{
					$pk_column = $this->_model->tableSchema->primaryKey;
					$vec_return['error'] = '';
					foreach ( $vec_data as $que_data )
					{
						switch ( $model_class_name )
						{
							case 'Product':									
								$que_result = $que_data->sku .' - '. $que_data->name;
							break;

							default:
								$que_result = $que_data->$representing_column;
							break;
						}

						if ( !empty($que_result) )
						{
							$que_title = Html::encode($que_result);
							if ( ! $strip_html_tags )
							{
								$que_title = str_replace($que_query, '<em>'. $que_query .'</em>', Html::encode($que_result));
							}
						}
						
						$vec_return['results'][] = array(
							'id' => $que_data->$pk_column,
							'name' => $que_title
						);
					}
				}
				*/
			}
		}
		echo CJSON::encode($vec_return);
		Yii::app()->end();
	}
	

	/**
	 * AJAX call to GLOBAL searchs
	 *
	 * @param mixed		Search term
	 * @return JSON
	 */
	/*
	public function actionGlobal()
	{
		// Global action only allowed by AJAX requests
		if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
		{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}

		$vec_return = array(
			'message' => 'No se han encontrado resultados',
			'results' => '',
		);

		if ( isset($_GET['q']) AND !empty($_GET['q']) )
		{
			$que_module = 'all';
			if ( isset($_GET['m']) AND !empty($_GET['m']) )
			{
				$que_module = Yii::app()->input->get('m');
			}


			$que_query = Yii::app()->input->get('q');
			if ( !is_numeric($que_query) )
			{
				$que_query = str_replace(" ", "%", strtoupper($que_query));
			}

			$vec_entities = array(
				'Clientes' => array(
					'title' => 'Clientes',
					'entity_type' => 'empresa_cliente',
					'url' => array(
						'view' => $this->createAbsoluteUrl("empresas/cliente/view/"),
						'list' => $this->createAbsoluteUrl("empresas/cliente/index/?Cliente%5Bglobal_search_filter%5D="),
					)
				),
				'Acreedores' => array(
					'title' => 'Acreedores',
					'entity_type' => 'empresa_acreedor',
					'url' => array(
						'view' => $this->createAbsoluteUrl("empresas/acreedor/view/"),
						'list' => $this->createAbsoluteUrl("empresas/acreedor/index/?Acreedor%5Bglobal_search_filter%5D="),
					)
				),
				'Equipos' => array(
					'title' => 'Equipos',
					'entity_type' => 'equipo_equipo',
					'url' => array(
						'view' => $this->createAbsoluteUrl("equipos/equipo/view/"),
						'list' => $this->createAbsoluteUrl("equipos/equipo/index/?Equipo%5Bglobal_search_filter%5D="),
					)
				),
				'Recambios' => array(
					'title' => 'Recambios',
					'entity_type' => 'recambio_recambio',
					'url' => array(
						'view' => $this->createAbsoluteUrl("recambios/recambio/view/"),
						'list' => $this->createAbsoluteUrl("recambios/recambio/index/?Recambio%5Bglobal_search_filter%5D="),
					)
				),
			);
			
			$vec_selected_entities = array();
			$limit = 5;
			switch ( $que_module )
			{
				// Módulo CLIENTES
				case 'Empresas':
					$vec_selected_entities = array(
						$vec_entities['Clientes'],
						$vec_entities['Acreedores'],
					);
				break;

				// Módulo EQUIPOS
				case 'Equipos':
					$vec_selected_entities = array(
						$vec_entities['Equipos'],
					);
				break;

				// Módulo RECAMBIOS
				case 'Recambios':
					$vec_selected_entities = array(
						$vec_entities['Recambios'],
					);
				break;

				// Búsqueda global en todos los módulos
				default:
					$limit = 3;
					$vec_selected_entities = array(
						$vec_entities['Clientes'],
						$vec_entities['Acreedores'],
						$vec_entities['Equipos'],
						$vec_entities['Recambios'],
					);
				break;
			}

			foreach ( $vec_selected_entities as $entity_name => $que_entity )
			{
				$vec_return['results'][$entity_name] = array(
					'title' => $que_entity['title'],
					'view_url' => $que_entity['url']['view'],
					'list_url' => $que_entity['url']['list'],
					'items' => array()
				);

				$sql = "SELECT entity_id, name, description FROM dz_search_results WHERE entity_type = '". $que_entity['entity_type'] ."' AND (";

				// Search by ID, it means search by Primary Key "ID"
				if ( is_numeric($que_query) AND $que_query < 999999 )
				{
					// $sql .= "entity_id LIKE '". $que_query ."%') ORDER BY entity_id ASC";
					$sql .= "entity_id = '". $que_query ."') ORDER BY entity_id ASC";
				}

				// Search by STRING in name and description fields
				else
				{
					$sql .= "name LIKE '%". $que_query ."%' OR description LIKE '%". $que_query ."%') ";
					$sql .= "ORDER BY CASE WHEN name LIKE '". $que_query ."%' THEN 0 ELSE 1 END, CASE WHEN description LIKE '%". $que_query ."%' THEN 0 ELSE 1 END";
				}
				$sql .= " LIMIT 0,$limit";

				// Debug SQL sentence
				// print $sql . "\n";

				$vec_rows = Yii::app()->db->createCommand($sql)->queryAll(); 
				if ( count($vec_rows) )
				{
					$vec_return['message'] = '';
					foreach ( $vec_rows as $que_row )
					{
						// $que_title = str_replace($que_query, '<em>'. $que_query .'</em>', $que_row['name']);
						$que_data = array(
							'id' => $que_row['entity_id'],
							'name' => Html::encode($que_row['name']),
							'description' => Html::encode($que_row['description'])
						);

						if ( strlen($que_data['name']) > 60 )
						{
							$que_data['name'] = substr($que_data['name'], 0, 60) .'&hellip;';
						}

						if ( strlen($que_data['description']) > 75 )
						{
							$que_data['description'] = substr($que_data['description'], 0, 75) .'&hellip;';
						}

						$vec_return['results'][$entity_name]['items'][] = $que_data;
					}
				}
				else
				{
					$vec_return['message'] = '';
					// unset($vec_return['results'][$entity_name]);
				}
			}
		}

		echo CJSON::encode($vec_return);
		Yii::app()->end();
	}
	*/

	/**
	 * AJAX call to get Cliente teaser mode
	 *
	 * @param integer
	 * @return HTML
	 */
	public function actionTeaser($id)
	{
		$this->renderPartial('_view', $this->loadModel($id), FALSE, TRUE);
		Yii::app()->end();
	}
}