<?php
/*
|--------------------------------------------------------------------------
| Controller class for help
|--------------------------------------------------------------------------
*/

namespace dz\controllers;

use dz\web\Controller;
use Yii;

class HelpController extends Controller
{
    /**
     * Markdown Guide
     * 
     * @see https://simplemde.com/markdown-guide
     */
    public function actionMarkdown()
    {
        // Force "backend" theme and "clean layout" for this action
        Yii::app()->theme = 'backend';
        $this->layout = '//layouts/html_clean';

        // Render markdown guide
        $this->render('//help/markdown');
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'markdown'      => 'is_logged_in',
        ];
    }
}
