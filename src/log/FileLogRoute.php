<?php
/**
 * DzFileLogRoute class file
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 */

namespace dz\log;

use Yii;

/**
 * Override CFileLogRoute class that records log messages in files.
 */
class FileLogRoute extends \CFileLogRoute
{
    /**
     * Initializes the route.
     * This method is invoked after the route is created by the route manager.
     */
    public function init()
    {
        parent::init();

        if ( isset(Yii::app()->params['logPath']) )
        {
            $this->setLogPath(Yii::getAlias(Yii::app()->params['logPath']));
        }
        else
        {
            $this->setLogPath(Yii::getAlias('@storage.logs'));
        }
    }
}
    