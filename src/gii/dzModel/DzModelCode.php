<?php
/**
 * DzModelCode class file
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @copyright Copyright &copy; Fabián Ruiz 2012
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */

use dz\db\ActiveRecord;

Yii::import('gii.generators.model.ModelCode');
Yii::import('@lib.gii.giix-core.giixModel.GiixModelCode');
Yii::import('@lib.gii.giix-core.helpers.*');

/**
 * DzModelCode is the template for DZ model generator
 */
// class GiixModelCode extends ModelCode {
class DzModelCode extends GiixModelCode
{
    /**
     * @var string The (base) model base class name.
     */
    public $baseClass = 'ActiveRecord';

    /**
     * @var string The path of the base model.
     */
    public $baseModelPath;

    /**
     * @var string The base model class name.
     */
    public $baseModelClass;


    /**
     * @var array Columns with ENUM type
     */
    public $vec_enum_columns;



    /**
     * Prepares the code files to be generated.
     */
    public function prepare()
    {
        if ( ($pos = strrpos($this->tableName, '.')) !== false )
        {
            $schema = substr($this->tableName, 0, $pos);
            $tableName = substr($this->tableName, $pos + 1);
        }
        else {
            $schema = '';
            $tableName = $this->tableName;
        }

        if ($tableName[strlen($tableName) - 1] === '*')
        {
            $tables = Yii::app()->db->schema->getTables($schema);
            if ($this->tablePrefix != '')
            {
                foreach ($tables as $i => $table)
                {
                    if (strpos($table->name, $this->tablePrefix) !== 0)
                    {
                        unset($tables[$i]);
                    }
                }
            }
        }
        else
        {
            $tables = array($this->getTableSchema($this->tableName));
        }

        $this->files = array();
        $templatePath = $this->templatePath;

        $this->relations = $this->generateRelations();

        foreach ($tables as $table)
        {
            $tableName = $this->removePrefix($table->name);
            $className = $this->generateClassName($table->name);

            // Generate the pivot model data.
            $pivotModels = array();
            if ( isset($this->relations[$className]) )
            {
                foreach ( $this->relations[$className] as $relationName => $relationData )
                {
                    if  ( preg_match('/^array\(self::MANY_MANY,.*?,\s*\'(.+?)\(/', $relationData, $matches) )
                    {
                        // Clean the table name if needed.
                        $pivotTableName = str_replace(array('{', '}'), '', $matches[1]);
                        $pivotModels[$relationName] = $this->generateClassName($pivotTableName);
                    }
                }
            }

            // Review columns TYPES
            foreach ( $table->columns as $column_name => $que_column )
            {
                if ( preg_match("/int\(/",$que_column->dbType) )
                {
                    $table->columns[$column_name]->type = 'integer';
                }

                if ( $que_column->dbType == 'timestamp' )
                {
                    $table->columns[$column_name]->type = 'datetime';
                }
            }

            $params = array(
                'tableName' => $schema === '' ? $tableName : $schema . '.' . $tableName,
                'modelClass' => $className,
                'columns' => $table->columns,
                'labels' => $this->generateLabelsEx($table, $className),
                'rules' => $this->generateRules($table),
                'relations' => isset($this->relations[$className]) ? $this->relations[$className] : array(),
                'representingColumn' => $this->getRepresentingColumn($table), // The representing column for the table.
                'pivotModels' => $pivotModels, // The pivot models.
                'vec_enum_columns' => $this->vec_enum_columns
            );

            // Setup base model information.
            $this->baseModelPath = $this->modelPath . '._base';
            $this->baseModelClass = $className;

            // Generate the model.
            $this->files[] = new CCodeFile(
                            Yii::getPathOfAlias($this->modelPath . '.' . $className) . '.php',
                            $this->render($templatePath . DIRECTORY_SEPARATOR . 'model.php', $params)
            );

            // Generate the base model.
            $this->files[] = new CCodeFile(
                            Yii::getPathOfAlias($this->baseModelPath . '.' . $this->baseModelClass) . '.php',
                            $this->render($templatePath . DIRECTORY_SEPARATOR . '_base' . DIRECTORY_SEPARATOR . 'basemodel.php', $params)
            );
        }
    }


    /**
     * Generate a name for use as a relation name (inside relations() function in a model).
     *
     * @param string the name of the table to hold the relation
     * @param string the foreign key name
     * @param boolean whether the relation would contain multiple objects
     * @return string the relation name
     */
    protected function generateRelationName($tableName, $fkName, $multiple)
    {
        // Call to ModelCode::generateRelationName()
        $name = parent::generateRelationName($tableName, $fkName, $multiple);

        // Fields end with "_uid" => relations end with "User".
        // Example: 'created_uid' field will be "createdUser" relation name
        if (strcasecmp(substr($fkName,-3),'uid')===0 && strcasecmp($fkName,'uid') )
        {
            $name .= 'ser';
        }
        return $name;
    }


    /**
     * Finds the relation data for all the relations of the specified model class.
     *
     * @param string $className The model class name.
     * @return array An array of arrays with the relation data.
     *      The array will have one array for each relation.
     *      The key is the relation name. There are 5 values:
     *          0: the relation type,
     *          1: the related active record class name,
     *          2: the joining (pivot) table (note: it may come with curly braces) (if the relation is a MANY_MANY, else null),
     *          3: the local FK (if the relation is a BELONGS_TO or a MANY_MANY, else null),
     *          4: the remote FK (if the relation is a HAS_ONE, a HAS_MANY or a MANY_MANY, else null).
     *          Or null if the model has no relations.
     */
    public function getRelationsData($className)
    {
        $result = parent::getRelationsData($className);

        if ( !empty($result) )
        {
            foreach ( $result as $key_result => $que_result )
            {
                if ( isset($que_result[1]) && $que_result[1] == 'UserUsers' )
                {
                    $result[$key_result][1] = 'User';
                }
            }
        }

        return $result;
    }


    /**
     * Finds the relation data of the specified relation name.
     *
     * @param string $className The model class name.
     * @param string $relationName The relation name.
     * @param array $relations An array of relations for the models
     * @return array The relation data. The array will have 3 values:
     */
    public function getRelationData($className, $relationName, $relations = array())
    {
        $relationData = parent::getRelationData($className, $relationName, $relations);

        // Custom relation data for User relations
        if ( $relationData == NULL )
        {
            if ( empty($relations) )
            {
                if (!empty($this->relations) )
                {
                    $relations = $this->relations;
                }
                else
                {
                    $relations = $this->generateRelations();
                }
            }
            if (isset($relations[$className]) && isset($relations[$className][$relationName]))
            {
                $relation = $relations[$className][$relationName];
            }
            else
            {
                return null;
            }

            // if ( preg_match("/^array\(([\w:]+?),\s?'(\w+)',\s?'([\w\s\(\),]+?)'\)$/", $relation, $matches_base) )
            if ( preg_match("/^array\(([\w:]+?),\s?'(\w+)',\s?(\w+)/", $relation, $matches_base) )
            {
                if ( $matches_base[2] == 'User' )
                {
                    $relationData = array();
                    $relationData[1] = $matches_base[2]; // the related active record class name

                    $vec_relation = explode(",", $relation);
                    $fk_name = strtr($vec_relation[2], array(
                        ' ' => '',
                        '\'' => '',
                        'array(' => '',
                        ' => \'id\'))' => ''
                    ));
                    switch ($matches_base[1])
                    {
                        case 'self::BELONGS_TO':
                            $relationData[0] = ActiveRecord::BELONGS_TO; // the relation type
                            $relationData[2] = null;
                            $relationData[3] = $fk_name; // the local FK
                            $relationData[4] = null;
                        break;
                        case 'self::HAS_ONE':
                            $relationData[0] = ActiveRecord::HAS_ONE; // the relation type
                            $relationData[2] = null;
                            $relationData[3] = null;
                            $relationData[4] = $fk_name; // the remote FK
                        break;
                        case 'self::HAS_MANY':
                            $relationData[0] = ActiveRecord::HAS_MANY; // the relation type
                            $relationData[2] = null;
                            $relationData[3] = null;
                            $relationData[4] = $fk_name; // the remote FK
                        break;
                    }
                }
            }
        }

        return $relationData;
    }


    /**
     * Generate relation data for all the relations
     */
    protected function generateRelations()
    {
        $vec_relations = parent::generateRelations();
        if ( !empty($vec_relations) )
        {
            foreach ( $vec_relations as $relation_field => $que_relation )
            {
                if ( !empty($que_relation) )
                {
                    foreach ( $que_relation as $relation_id => $the_relation )
                    {
                        // Exclude MANY_MANY relationships
                        if ( preg_match("/self::MANY_MANY/", $the_relation) )
                        {
                            unset($vec_relations[$relation_field][$relation_id]);
                        }
                        else
                        {
                            /**
                             * Explode using ' character. It is the result:
                             *
                             *    [0] => array(self::BELONGS_TO,
                             *    [1] => User
                             *    [2] => ,
                             *    [3] => created_uid
                             *    [4] => )
                             */
                            $vec_relation = explode("'", $the_relation);
                            if ( preg_match("/User$/", $relation_id) AND preg_match("/_uid'\)/", $the_relation) )
                            {
                                $vec_relation[3] = "['". $vec_relation[3] ."' => 'id']";
                            }
                            else
                            {
                                $vec_relation[3] = "'". $vec_relation[3] ."'";
                            }

                            $vec_relations[$relation_field][$relation_id] = str_replace("array(self::", "[self::", $vec_relation[0]) . $vec_relation[1] .'::class, '. $vec_relation[3] .']';
                        }
                    }
                }
            }
        }

        return $vec_relations;
    }


    /**
     * Generates the rules for table fields.
     *
     * This method has been rewrited
     *
     * @see ModelCode::generateRules() method
     */
    public function generateRules($table)
    {
        $vec_required_columns = [];
        $vec_integer_columns = [];
        $vec_numerical_columns = [];
        $vec_length_columns = [];
        $vec_null_columns = [];

        // Process DATETIME, ENUM columns & rewrites SAFE columns
        $vec_enum_columns = [];
        $vec_safe_columns = [];

        foreach ($table->columns as $column)
        {
            // --> GET RULES BY TYPE
            //-----------------------------------------------------------------
            if ( $column->autoIncrement )
            {
                continue;
            }

            $is_required = !$column->allowNull && $column->defaultValue === null;

            // REQUIRED columns
            if ( $is_required )
            {
                $vec_required_columns[] = $column->name;
            }

            // NULL columns
            if ( !(!$column->allowNull && $column->defaultValue === null) )
            {
                $vec_null_columns[] = $column->name;
            }

            // INTEGER columns
            if ( $column->type==='integer' )
            {
                $vec_integer_columns[] = $column->name;
            }

            // NUMERICAL columns
            elseif ( $column->type==='double')
            {
                $vec_numerical_columns[] = $column->name;
            }

            // STRING columns with LENGTH
            elseif ( $column->type === 'string' && $column->size > 0)
            {
                // ENUM columns
                if ( $column->type === 'string' && preg_match("/^enum\(/", $column->dbType) )
                {
                    $vec_enum_columns[$column->name] = strtr($column->dbType, ['enum(' => '', ')' => '']);
                }

                // VARCHAR columns
                else
                {
                    $vec_length_columns[$column->size][] = $column->name;
                }
            }

            // SAFE columns
            elseif ( !$column->isPrimaryKey && !$is_required )
            {
                $vec_safe_columns[] = $column->name;
            }
        }


        // --> WRITE RULES
        //-----------------------------------------------------------------

        // REQUIRED rules
        if ( $vec_required_columns !== [] )
        {
            $vec_rules[] = "['".implode(', ',$vec_required_columns)."', 'required']";
        }

        // INTEGER rules
        if ( $vec_integer_columns !== [] )
        {
            $vec_rules[] = "['".implode(', ',$vec_integer_columns)."', 'numerical', 'integerOnly' => true]";
        }

        // NUMERCIAL rules
        if ( $vec_numerical_columns !== [] )
        {
            $vec_rules[] = "['".implode(', ',$vec_numerical_columns)."', 'numerical']";
        }

        // NUMERCIAL rules
        if ( $vec_length_columns !== [] )
        {
            foreach ( $vec_length_columns as $max_length => $column_values )
            {
                $vec_rules[] = "['".implode(', ',$column_values)."', 'length', 'max'=> $max_length]";
            }
        }

        // IN rules
        if ( $vec_enum_columns !== [] )
        {
            $this->vec_enum_columns = $vec_enum_columns;

            foreach ( $vec_enum_columns as $column_name => $column_values )
            {
                $vec_rules[] = "['". $column_name ."', 'in', 'range' => [". str_replace(",", ", ", $column_values) ."]]";
            }
        }

        // NULL rules
        if ( $vec_null_columns !== [] )
        {
            $vec_rules[] = "['" . implode(', ', $vec_null_columns) . "', 'default', 'setOnEmpty' => true, 'value' => null]";
        }

        // SAFE rules
        if ( $vec_safe_columns !== [] )
        {
            $vec_rules[] = "['".implode(', ',$vec_safe_columns)."', 'safe']";
        }

        return $vec_rules;
    }


    /**
     * Finds the class name for the specified table name
     */
    protected function generateClassName($tableName)
    {
        $className = parent::generateClassName($tableName);
        if ( $className == 'UserUsers' )
        {
            $className = 'User';
        }
        return $className;
    }


    /**
     * Selects the representing column of the table.
     * The "representingColumn" method is the responsible for the string representation of the model instance.
     *
     * @param CDbTableSchema $table The table definition.
     * @return string|array The name of the column as a string or the names of the columns as an array.
     * @see GxActiveRecord::representingColumn
     * @see GxActiveRecord::__toString
     */
    protected function getRepresentingColumn($table)
    {
        $columns = $table->columns;

        // If this is not a MANY_MANY pivot table
        if ( !$this->isRelationTable($table) )
        {
            // Dezero - First we look columns with names like "name" or "nombre"
            foreach ($columns as $name => $column)
            {
                if ( ($column->name == 'name' || $column->name == 'nombre') && ($column->type === 'string' && !$column->allowNull && !$column->isPrimaryKey && !$column->isForeignKey && stripos($column->dbType, 'int') === false) )
                {
                    return $name;
                }
            }


            // Then we look for a string, not null, not pk, not fk column, not original number on db.
            foreach ($columns as $name => $column)
            {
                if ($column->type === 'string' && !$column->allowNull && !$column->isPrimaryKey && !$column->isForeignKey && stripos($column->dbType, 'int') === false)
                {
                    return $name;
                }
            }
            // Then a string, not null, not fk column, not original number on db.
            foreach ($columns as $name => $column)
            {
                if ($column->type === 'string' && !$column->allowNull && !$column->isForeignKey && stripos($column->dbType, 'int') === false)
                {
                    return $name;
                }
            }

            // Then the first string column, not original number on db.
            foreach ($columns as $name => $column)
            {
                if ($column->type === 'string' && stripos($column->dbType, 'int') === false)
                {
                    return $name;
                }
            }
        }

        // If the appropriate column was not found or if this is a MANY_MANY pivot table. Then the pk column(s).
        $pk = $table->primaryKey;
        if ($pk !== null)
        {
            if (is_array($pk))
            {
                return $pk;
            }
            return (string) $pk;
        }

        // Then the first column
        return reset($columns)->name;
    }


    /**
     * Generate the namespace from an alias
     */
    protected function generateNamespace($aliasPath)
    {
        return str_replace(".", "\\", $aliasPath);
    }


    /**
     * Validate base class. Allow ActiveRecord class
     */
    public function validateBaseClass($attribute, $params)
    {
        if ( $this->baseClass != 'ActiveRecord' )
        {
            $class = @Yii::import($this->baseClass,true);

            if( !is_string($class) || !$this->classExists($class) )
            {
                $this->addError('baseClass', "Class '{$this->baseClass}' does not exist or has syntax error.");
            }
            elseif($class!=='CActiveRecord' && !is_subclass_of($class,'CActiveRecord'))
            {
                $this->addError('baseClass', "'{$this->baseClass}' must extend from CActiveRecord.");
            }
        }
    }
}
