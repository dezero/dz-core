<?php
/**
 * DzModelCode class file
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @copyright Copyright &copy; Fabián Ruiz 2012
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */

Yii::import('gii.generators.crud.ModelGenerator');
Yii::import('@lib.gii.giix-core.giixModel.GiixModelGenerator');

/**
 * DzModelGenerator is the controller for DZ model generator
 */
// class GiixModelGenerator extends CCodeGenerator {
class DzModelGenerator extends GiixModelGenerator
{
	public $codeModel = '@core.src.gii.dzModel.DzModelCode';
}