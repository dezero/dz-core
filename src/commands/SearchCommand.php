<?php
/**
 * SearchCommand class file
 *
 * This component is used to load data in MySQL table "search_product"
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2017 Fabián Ruiz
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package commands
 */

namespace dz\commands;

use dz\console\CronCommand;
use Yii;

class SearchCommand extends CronCommand
{
	/**
     * Clear caches
     *
     * ./yiic search clear
     */
    public function actionClear()
    {
        echo "Clearing search product caches...\n";
        
        $total_cache_products = Yii::app()->searchManager->reload_caches_product();
        
        echo "OK --> ". $total_cache_products ." search products have been reloaded\n";
    }
}