<?php
/**
 * DzCronTask class file
 *
 * This component is used to start and notify cron changes
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2015 Fabián Ruiz
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package commands
 */

namespace dz\commands;

use dz\components\Mailer;
use dz\console\CronCommand;

class DzCronTaskCommand extends CronCommand
{
	/**
	 * Before cron task actions
	 */
	public function actionBefore()
	{
		// $cron_file_path = Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . 'log'. DIRECTORY_SEPARATOR . 'last-cron.log';
		$cron_file_path = Yii::getPathOfAlias(Yii::app()->params['logPath']) . DIRECTORY_SEPARATOR . 'last-cron.log';
		$cron_file = Yii::app()->file->set($cron_file_path);
		if ( $cron_file->getExists() )
		{
			$cron_file->delete();
		}
	}


	/**
	 * After cron task actions
	 */
	public function actionAfter()
	{
		// Sends an email
		$mailer = new Mailer();
		$mailer->setTo(Yii::app()->params['mail']['cron_notification']);
		$mailer->setSubject('Importación / Exportación realizada con éxito');
		$mailer->setView('blank');
		$mailer->setData(array('content'=> 'Importación / Exportación realizada con éxito'));

		// Attach log file
		// $cron_file_path = Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . 'log' . DIRECTORY_SEPARATOR . 'last-cron.log';
		$cron_file_path = Yii::getPathOfAlias(Yii::app()->params['logPath']) . DIRECTORY_SEPARATOR . 'last-cron.log';
		$mailer->setAttachment($cron_file_path);
		
		// Sending email
		$mail_sending_result = $mailer->Send();

		if ( !$mail_sending_result )
		{
			$this->log("Error en el envío de ". $mailer->send_type ." a ". CJSON::encode($mailer->getToAddress())." - [Fichero: '". $mailer->filename ."'] - ERROR: ". CJSON::encode($mailer->ErrorInfo));
		}
	}
}