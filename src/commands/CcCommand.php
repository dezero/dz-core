<?php

namespace dz\commands;

use dz\helpers\CacheHelper;

use Yii;

/**
 * Yiic command to clear assets and cache
 * 
 * @link http://www.yiiframework.com/extension/console-clearcache-and-assets/
 */
class CcCommand extends \CConsoleCommand
{
    /**
     * Clear cache, assets and temp directories
     * 
     * > ./yiic cc all
     *
     * @param string name of property in configuration, comma seperated for multiple caches
     * @param string defaults to /assets
     * @return void
     */
    public function actionAll($cache_id = 'cache', $assets_path = null)
    {
        echo "------------------------------\n";
        $this->actionCache($cache_id);

        echo "\n------------------------------\n";
        $this->actionAssets($assets_path);

        echo "\n------------------------------\n";
        $this->actionTemp();
    }


    /**
     * Flush cache
     * 
     * > ./yiic cc cache
     */
    public function actionCache($cache_id = 'cache')
    {
        $vec_output = CacheHelper::clear_cache($cache_id);
        if ( !empty($vec_output) )
        {
            echo "Clearing caches...\n";
            if ( isset($vec_output['messages']) && !empty($vec_output['messages']) )
            {
                foreach ( $vec_output['messages'] as $que_message )
                {

                    echo " - {$que_message}\n";
                }
            }

            if ( isset($vec_output['errors']) && !empty($vec_output['errors']) )
            {
                foreach ( $vec_output['errors'] as $que_error )
                {

                    echo " - ERROR - {$que_error}\n";
                }
            }
        }
    }


    /**
     * Clear assets directory
     * 
     * > ./yiic cc assets
     */
    public function actionAssets($asset_path = null)
    {
        echo "Clearing ASSETS directory...\n";

        if ( $asset_path === null )
        {
            // $asset_path = Yii::getAlias('@www.assets');
            $asset_path = Yii::app()->path->assetsPath() . DIRECTORY_SEPARATOR;
            echo " - No asset path submitted. Assuming ". $asset_path ."\n";
        }

        // Clear directory
        $this->clearDirectory($asset_path, true);
    }


    /**
     * Clear TEMP directory
     * 
     * > ./yiic cc temp
     */
    public function actionTemp()
    {
        echo "Clearing TEMP directories...\n";

        // Clear PUBLIC TEMP path
        $temp_path = Yii::app()->path->get('temp') . DIRECTORY_SEPARATOR;
        $this->clearDirectory($temp_path);

        // Clear PRIVATE TEMP path
        $private_temp_path = Yii::app()->path->get('privateTempPath') . DIRECTORY_SEPARATOR;
        $this->clearDirectory($private_temp_path);
    }


    /**
     * Clear directory.
     * 
     * Remove all files and all the content from subdirectories (keeping the first level)
     * If $is_clear_all = true, it will remove all subdirectories.
     */
    public function clearDirectory($dir_path, $is_clear_all = false)
    {
        $temp_dir = Yii::app()->file->set($dir_path);
        if ( $temp_dir->isDir )
        {
            echo " - Deleting content from ". $dir_path ." directory.\n";
            
            // Finally, remove directory
            Yii::app()->file->removeDirectory($dir_path, $is_clear_all);
        }
    }
}