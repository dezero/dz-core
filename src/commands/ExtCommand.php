<?php

namespace dz\commands;

use dz\commands\contrib\clefCommand;
use Yii;

/**
 * Yiic command to download & get extensions info
 * 
 * @link http://www.yiiframework.com/extension/clef/
 */
class ExtCommand extends clefCommand
{
    /**
     * Download action
	 * Action executed by default, if action doesn't exist
     */
    public function actionDl($extension)
	{
		print_r($extension);
		return parent::actionDownload($extension);
	}


	/**
	 * Executes the command.
	 *
	 * @param array $args command line parameters for this command.
	 * @return integer application exit code, which is returned by the invoked action. 0 if the action did not return anything.
	 */
	public function run($args)
	{
		// Check if action methos exists. Unless we'll use "actionDl" by default
		$param_args = $args;
		list($action, $options, $vec_args) = $this->resolveRequest($args);
		$methodName = 'action'.$action;
		if ( !preg_match('/^\w+$/',$action) || !method_exists($this,$methodName) )
		{
			$args = array(
				'dl',
				'--extension='. $param_args[0]
			);
		}
		return parent::run($args);
	}
	
    public function getHelp() {
        return <<<EOD
USAGE
  yiic clef [action] [parameter]

DESCRIPTION
  This command provides a CLI interface to the official Yii Extensions
  repository. You can use this command to search, browse & download
  any extension avaliable to the website. The action parameter can 
  be used to configure the task you want to perform and can take the
  following values: search, info, download.
  Each of the above actions takes different parameters. Their usage can 
  be found in the following examples.

EXAMPLES
 * yiic ext search --query=[query-string]
   Searches through Yii Extension Repository Search for every extension
   matching this term and returns a list with extension names followed by
   a short description.

 * yiic ext info --extension=[extension-name] --type=[simple|extended]
   Returns information for the selected extension. (Note: extended is not
   yet implemented)

 * yiic ext dl --extension=[extension-name]
   Downloads the latest version of the requested extension to the current
   directory.

 * yiic ext list --type=[option]
   Lists top extensions by type based on one of the following options:
   - top-rated
   - newest
   - top-commented
   - top-downloaded

 * yiic ext listcategory
   Lists extensions by category type. This command needs no arguments as it 
   runs in interactive mode.

EOD;
    }
}