<?php
/**
 * DateHelper class file
 *
 * Helper class for working with date and time
 * 
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2012 Dezero
 */

namespace dz\helpers;

use DateTime;
use Yii;

class DateHelper
{
	/**
     * Hora de inicio de la jornada laboral. Defult 450 (07:30)
     */
	const HORARIO_INICIO = 450;


	/**
     * Hora de fin de la jornada laboral. Defult 1170 (19:30)
     */
	const HORARIO_FIN = 1170;


    /**
     * Validate a date string using a format in $format parameter
     */
    public static function validate_date($date, $format = 'd/m/Y')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }


	/**
     * Transformación de fechas
     * 
     * Parser from YYYYmmddHHii to UNIX timestamp format
     */
	public static function date_to_unix($que_date)
	{
		if ( !empty($que_date) )
		{
			// Formato YYYYmmdd
			if ( strlen($que_date) == 8 )
			{
				$que_date .= '0000';
			}
			
			$new_date = substr($que_date,6,2) .'-'. substr($que_date,4,2) .'-'. substr($que_date,0,4).' '.substr($que_date,8,2) .':'. substr($que_date,10,2);
			return strtotime(date($new_date));
		}
		return $que_date;
	}
	
	
	/**
     * Transformación de fechas
     * 
     * Parser from UNIX timestamp format to string date "d/m/Y H:i" format
     */
	public static function unix_to_date($timestamp, $format = 'd/m/Y H:i')
	{
		return date($format, $timestamp);
	}
	
	
	/**
     * Transformación de fechas
     * 
     * Parser from dd/mm/YYYY to UNIX timestamp format
     */
	public static function date_format_to_unix($que_date, $date_from = '')
	{
		if ( empty($que_date) )
		{
			return "";
		}
		
		$que_date_format = self::date_format($que_date, $date_from);
		return self::date_to_unix($que_date_format .'0000');
	}


	/**
	 * From dd/mm/YYYY - H:i to UNIX timestamp format
	 */
	public static function date_format_hour_to_unix($que_date)
	{
		if ( preg_match("/\//", $que_date) )
		{
			$vec_date = explode("/", $que_date);
			$output_date = $vec_date[0] .'-'. $vec_date[1] .'-';
			if ( ! preg_match ("/\:/", $vec_date[2]) )
			{
				$output_date .= $vec_date[2] .' 00:00';
			}
			else
			{
				$output_date .= str_replace(" -", "", $vec_date[2]);
			}
			return strtotime(date($output_date));
		}
		return "";
	}


	/**
     * Transformación de fechas
     * 
     * Parser from dd/mm/YY to YYmmdd format
     */
	public static function date_format($que_date, $date_from = '')
	{
		if ( empty($que_date) )
		{
			return "";
		}
		
		/*
		if ( preg_match("/:/", $que_date) )
		{
			$vec_date_time = explode(":", $que_date);
		}
		*/

		$vec_date = explode("/", $que_date);
		if ( !isset($vec_date[2]) )
		{
			return "";
		}

		if ( strlen($vec_date[2]) == 2 )
		{
			if ( !empty($date_from) )
			{
				$date_from_year = substr($date_from, 2, 2);
				if ( $date_from_year < $vec_date[2] )
				{
					$vec_date[2] = '19'. $vec_date[2];
				}
				else
				{
					$vec_date[2] = '20'. $vec_date[2];
				}
			}
			else
			{
				if ( preg_match("/^9/", $vec_date[2]) )
				{
					$vec_date[2] = '19'. $vec_date[2];
				}
				else
				{
					$vec_date[2] = '20'. $vec_date[2];
				}
			}
		}

		return $vec_date[2].$vec_date[1].$vec_date[0];
	}


	/**
	 * Turns a total minute value into hh:mm format
	 *
	 * @param int
	 * @param string
	 * @return string
	 */
    public static function hour_format($value, $unit_type = 'minute')
    {
    	// Convert seconds to minutes
    	if ( $value > 0 AND $unit_type == 'second' )
    	{
    		$value = floor($value/60);
    	}

    	// $value = round($value, 0);
		return sprintf("%02d:%02d", floor($value/60), $value%60);
    }


    /**
	 * Turns a string with hh:mm format into total minute value
	 *
	 * @param array
	 * 
	 * @return string
	 */
    public static function hour_to_minutes($hour_value)
    {
    	// Transform hour fields from "hh:mm" format to total minutes (integer int DB)
		if ( preg_match("/:/", $hour_value) )
		{
			$vec_hour = explode(":", $hour_value);
			return ($vec_hour[0] * 60) + $vec_hour[1];
		}
		return $hour_value;
    }


    /**
     * Get the total days between 2 dates
     */
    public static function range_to_days($date_from, $date_to, $is_round = true)
    {
        if ( $date_to > $date_from )
        {
            $range_to_days = ($date_to - $date_from) / 86400;
            return $is_round ? floor($range_to_days) : $range_to_days;
        }

        return 0;
    }


    /**
     * Get the total hours between 2 dates
     */
    public static function range_to_hours($date_from, $date_to, $is_round = true)
    {
        if ( $date_to > $date_from )
        {
            $range_to_days = ($date_to - $date_from) / 3600;
            return $is_round ? round($range_to_days) : $range_to_days;
        }

        return 0;
    }


	/**
	 * Calcula el número de minutos entre 2 fechas (range)
	 *
	 * @param int 	Fecha inicio del intervalo (UNIX timestamp format)
	 * @param int 	Fecha fin del intervalo (UNIX timestamp format)
	 * @param int 	Hora inicio de la jornada laboral (minutes format). Default: 450 (07:30)
	 * @param int 	Hora final de la jornada laboral (minutes format). Default: 1170 (19:30)
	 * @return int
	 */	
    public static function range_to_minutes($date_init, $date_end, $hour_init = '', $hour_end = '')
    {
    	// Valores por defecto
    	if ( empty($hour_init) )
    	{
    		$hour_init = self::HORARIO_INICIO;
    	}
		if ( empty($hour_end) )
    	{
    		$hour_end = self::HORARIO_FIN;
    	}
    	$hour_init = $hour_init * 60;
    	$hour_end = $hour_end * 60;

    	// Obtenemos la fecha de inicio y fin en formato "Y-m-d 00:00:00"
    	$date_init_YMD = strtotime(date("Y-m-d", $date_init) ." 00:00:00");
    	$date_end_YMD = strtotime(date("Y-m-d", $date_end) ." 00:00:00");

    	//------------------------------------------------------------------------------
    	// PASO 1 - Contamos los días laborables
		//------------------------------------------------------------------------------
    	$num_days = $num_minutes = 0;
    	$full_day_minutes = 24*60*60;
    	$que_date = $date_init_YMD;
    	while ( $que_date < $date_end_YMD )
    	{
    		if ( self::check_laborable($que_date) )
    		{
    			$num_days++;
    		}
    		$que_date += $full_day_minutes;
    	}


    	//------------------------------------------------------------------------------
    	// PASO 2 - Regulación de horas teniendo en cuenta las horas de una jornada
    	//          laboral (antes de las 7:30 o después de las 19:30)
		//------------------------------------------------------------------------------

    	// Hora de la fecha de inicio
    	$vec_date_init = self::regulariza_date_laborable($date_init, $hour_init, $hour_end);
    	$vec_date_end = self::regulariza_date_laborable($date_end, $hour_init, $hour_end);


		//------------------------------------------------------------------------------
    	// PASO 3 - Calcular el número exacto de minutos
		//------------------------------------------------------------------------------

		// Fecha inicio y fin corresponde al mismo día
		if ( $num_days == 0 )
		{
			$num_minutes = $vec_date_end['date_hour'] - $vec_date_init['date_hour'];
		}

		// Más de un día
		else
		{
			$num_minutes = $num_days * ($hour_end - $hour_init);				
			$num_minutes = $num_minutes + $vec_date_end['date_hour'] - $vec_date_init['date_hour'];
		}


		// Debido a errores de registros encontrados en fechas teóricamente festivas
		if ($num_minutes < 0)
		{
			$num_minutes = 0;
		}

    	return $num_minutes;
    }


    /**
	 * Devuelve la fecha laborable pasado $num_minutes a partir de $que_date
	 *
	 * @param int 	Fecha inicio (UNIX timestamp format)
	 * @param int 	Minutos a sumar
	 * @param int 	Hora inicio de la jornada laboral (minutes format). Default: 450 (07:30)
	 * @param int 	Hora final de la jornada laboral (minutes format). Default: 1170 (19:30)
	 * @return int
	 */	
    public static function add_minutes_to_date($que_date, $num_minutes, $hour_init = '', $hour_end = '')
    {
    	// Valores por defecto
    	if ( empty($hour_init) )
    	{
    		$hour_init = self::HORARIO_INICIO;
    	}
		if ( empty($hour_end) )
    	{
    		$hour_end = self::HORARIO_FIN;
    	}

    	// Pasamos todo a SEGUNDOS (formato función PHP time())
    	$hour_init = $hour_init * 60;
    	$hour_end = $hour_end * 60;
    	$num_minutes = $num_minutes * 60;

    	// Regularizamos la fecha de la jornada laboral
    	$vec_date = self::regulariza_date_laborable($que_date, $hour_init, $hour_end);

		// Duración de la joranda laboral (en segundos)
		$jornada_duration = $hour_end - $hour_init;

		// Los minutos que se suman superan a una jornada laboral -> Sumamos fechas
		$next_date_YMD = $vec_date['date_YMD'];
		$next_date_hour = $vec_date['date_hour'] + $num_minutes;
		if ( $next_date_hour > $hour_end )
		{
			$num_days = floor(($next_date_hour - $hour_init) / $jornada_duration);
			$next_date_YMD = self::add_days_to_date($vec_date['date_YMD'], $num_days);
			$next_date_hour = $vec_date['date_hour'] + ( $num_minutes - ($num_days*$jornada_duration) );
		}
		return $next_date_YMD + $next_date_hour;
    }


    /**
	 * Devuelve la siguiente fecha laboral pasado $num_days días a partir de $que_date
	 *
	 * @param 	int 	Fecha de inicio
	 * @param 	int 	Número de días que se desea sumar
	 * @param 	bool 	(Opcional) Comprueba solo fechas laborables. Por defecto, TRUE
	 * @return
	 */
	public static function add_days_to_date($que_date, $num_days, $check_laborable = TRUE )
	{		
		$next_date = $que_date;
		for ( $que_day = 0; $que_day < $num_days; $que_day++ )
		{
			// Sumamos 1 día y buscamos el siguiente día laborable
			$next_date += 24*60*60;
			if ( $check_laborable )
			{
				$next_date = self::next_laborable($next_date);
			}
		}
		return $next_date;
	}


    /**
	 * Indica si la fecha introducida es laborable
	 *
	 * @param int 
	 * @return bool
	 */
    public static function check_laborable($que_date)
    {
    	// date("N"), return 1 for monday and 7 for sunday
    	$week_day = date("N", $que_date);

    	// Exclude weekend
    	if ( $week_day > 5 )
    	{
    		return FALSE;
    	}
    	return TRUE;
    }


    /**
	 * Dada una fecha, comprueba si es laborable.
	 * Si no lo es, devuelve el siguiente día laborable en el calendario
	 *
	 * @param int 
	 * @return int
	 */
    public static function next_laborable($que_date)
    {
    	$is_laborable = self::check_laborable($que_date);
    	if ( !$is_laborable )
    	{
    		// Sumamos 1 día
			$que_date += 24*60*60;
			return self::next_laborable($que_date);
    	}
    	return $que_date;
    }


    /**
	 * Regulariza las horas de una fecha teniendo en cuenta el horario de la jornada laboral
	 * Es decir, no se aceptan horas antes de las 07:30 ni después de las 19:30
	 *
	 * @param int 	Fecha a regularizar
	 * @param int 	Hora inicio de la jornada laboral (minutes format). Default: 450 (07:30)
	 * @param int 	Hora final de la jornada laboral (minutes format). Default: 1170 (19:30)
	 * @return array
	 */
    public static function regulariza_date_laborable($que_date, $hour_init = '', $hour_end = '')
    {
    	// Valores por defecto
    	if ( empty($hour_init) )
    	{
    		$hour_init = self::HORARIO_INICIO * 60;
    	}
		if ( empty($hour_end) )
    	{
    		$hour_end = self::HORARIO_FIN * 60;
    	}

    	// Obtenemos la fecha de inicio y fin en formato "Y-m-d 00:00:00"
    	$que_date_YMD = strtotime(date("Y-m-d", $que_date) ." 00:00:00");

    	// Regularización de horas que se salen de las horas de una jornada laboral (antes de las 7:30 o después de las 19:30)
		$que_date_hour = ( date("G", $que_date) * 60 * 60 ) + ( (int)date("i", $que_date) * 60 );
		if ( $que_date_hour < $hour_init )
		{
			$que_date_hour = $hour_init;
		}
		else if ( $que_date_hour > $hour_end )
		{
			$que_date_hour = $hour_end;
		}


		// Sumamos las horas a la fecha
		return array(
			'date' 		=> $que_date_YMD + $que_date_hour,	// strtotime(date("Y-m-d h:i"))
			'date_YMD' 	=> $que_date_YMD,					// strtotime(date("Y-m-d 00:00"))
			'date_hour' => $que_date_hour					// strtotime(date("h:i"))
		);
    }


    
    /**
	 * Función para operar con MESES (sumar y restar meses)
	 *
	 * Ejemplos:
	 * 	+ add_months_to_date(2);  				// Devuelve 2 meses después
	 * 	+ add_months_to_date(-2,0,"Y-m-d");  	// Devuelve 2 meses antes con el FORMATO de fechas dados
	 * 	+ add_months_to_date(3,"01/01/2000");	// Devuelve 3 meses después de la fecha dada	 
	 *
	 * @access 	public
	 * @param	int 	$months 	Número de meses que se desea sumar o restar
	 * @param	string 	$date 		(Opcional) Fecha a partir de la cual se desea sumar/restar
	 * @param	string 	$format 	(Opcional) Formato en el que se desea devolver la fecha
	 * @return	int
	 */		
	public static function add_months_to_date($months, $date=null , $format="")
	{ 
		// $date = ( $date ? $date : date("Ymd") ); 
		$date = $date ? $date : time();
		if ( !empty($format) )
		{
			return date($format, strtotime($months." months", $date)); 
		}
		return strtotime($months." months", $date); 
	}


	/**
     * Transformación de fechas
     * 
     * Parser from UNIX timestamp format to "MySQL datetime". It means, string date "Y-m-d hh:mm:ii" format
     */
	public static function unix_to_datetime($timestamp)
	{
		return date("Y-m-d H:i:s", $timestamp);
	}


	/**
     * Transformación de fechas
     * 
     * Parser from "MySQL datetime" to custom date. It means, string date "Y-m-d hh:mm:ii" format
     */
	public static function datetime_to_date($datetime)
	{
		return date("d/m/Y - H:i", strtotime($datetime));
	}


	/**
	 * Parse a datetime to "X time ago" string format
	 */
	public static function unix_to_timeago($datetime, $full = FALSE )
	{
		$now = new DateTime;
	    $ago = new DateTime($datetime);
	    $diff = $now->diff($ago);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day',
	        'h' => 'hour',
	        'i' => 'minute',
	        's' => 'second',
	    );
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }

	    if (!$full) $string = array_slice($string, 0, 1);
	    return $string ? implode(', ', $string) . ' ago' : 'just now';
	}


	/**
	 * Get month name
	 */
	public static function get_month_name($month)
	{
		$vec_month = array(
			1 => Yii::t('app', 'January'),
			2 => Yii::t('app', 'February'),
			3 => Yii::t('app', 'March'),
			4 => Yii::t('app', 'April'),
			5 => Yii::t('app', 'May'),
			6 => Yii::t('app', 'June'),
			7 => Yii::t('app', 'July'),
			8 => Yii::t('app', 'August'),
			9 => Yii::t('app', 'September'),
			10 => Yii::t('app', 'October'),
			11 => Yii::t('app', 'November'),
			12 => Yii::t('app', 'December'),
		);

		if ( isset($vec_month[$month]) )
		{
			return $vec_month[$month];
		}
		return $month;
	}


    /**
     * Return a date in DATETIME mysql format
     * 
     * @param integer Date in UNIX format. If it's empty, it will return current date (now)
     */
    public static function mysql_date($date = '')
    {
        if ( !empty($date) )
        {
            return date('Y-m-d H:i:s', $date);
        }

        return date('Y-m-d H:i:s');
    }
}
