<?php
/**
 * AuthHelper class file
 * 
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2017 Dezero
 */

namespace dz\helpers;

use user\models\User;
use Yii;

class AuthHelper
{
    private static $_admin;

    /**
     * Check if user is admin
     */
    public static function isAdmin()
    {
        if ( Yii::app()->user->isGuest )
        {
            return false;
        }
        
        // Cache this function var
        if ( self::$_admin === null )
        {
            self::$_admin = false;
            $user_model = User::findOne(Yii::app()->user->id);
            if ( $user_model )
            {
                self::$_admin = (bool)$user_model->is_superadmin;
            }
        }

        return self::$_admin;
    }


    /**
     * Get roles and tasks for an user
     */
    public static function roles_tasks($user_id, $view_mode = false)
    {
        // Auth Manager
        $am = Yii::app()->getAuthManager();
        $vec_roles = $am->getAuthItems(2);
        $vec_assigned_roles = $am->getAuthItems(2, $user_id);
        
        // Roles & task lists
        $vec_list_roles = [];
        $vec_list_tasks = [];
        $vec_list_assigned = [];
        if ( !empty($vec_roles) )
        {
            foreach ( $vec_roles as $que_role )
            {
                $vec_list_roles[$que_role->name] = $que_role->description;
            }
            
            foreach ( $vec_assigned_roles as $que_role )
            {
                // Task list
                $vec_tasks = $am->getDescendants($que_role->name);
                if ( !empty($vec_tasks) ) 
                {
                    foreach ( $vec_tasks as $que_task )
                    {
                        if ( $que_task['item']->type == 1 AND !isset($vec_list_tasks[$que_task['name']]) )
                        {
                            $vec_list_tasks[$que_task['name']] = $que_task['item']->description;
                        }
                    }
                }
                
                if ( $view_mode )
                {
                    $vec_list_assigned[$que_role->name] = $que_role->description;
                }
            }
        }
        
        // Assigned roles
        if ( !$view_mode )
        {
            $vec_list_assigned = array_keys($vec_assigned_roles);
        }
        
        return [
            'list' =>  $vec_list_roles,
            'assigned' => $vec_list_assigned,
            'tasks' => $vec_list_tasks,
        ];
    }
    
    
    /**
     * Get assigned task manually for an user
     */
    public static function manual_tasks($user_id)
    {
        // Assigned tasks
        $vec_tasks_assigned = Yii::app()->getAuthManager()->getAuthItems(1, $user_id);
        
        $output = [];
        foreach ( $vec_tasks_assigned as $que_task_assigned )
        {
            $output[$que_task_assigned->name] = $que_task_assigned->description;
        }
        
        return $output;
    }


    /**
     * Get role list
     */
    public static function role_list()
    {
        // Roles list
        $vec_roles = Yii::app()->getAuthManager()->getAuthItems(2);
        $vec_output = [];
        if ( !empty($vec_roles) )
        {
            foreach ( $vec_roles as $que_role )
            {
                $vec_output[$que_role->name] = $que_role->description;
            }
        }

        return $vec_output;
    }


    /**
     * Get the tasks list
     */
    public static function tasks_list()
    {
        $vec_tasks = Yii::app()->getAuthManager()->getAuthItems(1);
        $vec_output = [];
        if ( !empty($vec_tasks) )
        {
            foreach ( $vec_tasks as $que_task )
            {
                $vec_output[$que_task->name] = $que_task->description;
            }
        }

        return $vec_output;
    }
}