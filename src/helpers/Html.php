<?php
/**
 * Html helper class file for Dz Framework
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2012 Dezero
 */

namespace dz\helpers;

use BsArray;
use dz\helpers\DateHelper;
use dz\helpers\Expression;
use dz\helpers\StringHelper;
use Yii;

/**
 * Html extends GxHtml & CHtml and provides additional features
 */
class Html extends \GxHtml
{
	/**
     * @var string the CSS class for displaying error summaries.
     */
    public static $errorSummaryCss = 'messages messages-error';


	/**
	 * Generates the data suitable for list-based HTML elements.
	 * #MethodTracker (complex changes)
	 * This method is based on {@link CHtml::listData}, from version 1.1.7 (r3135). Changes:
	 * <ul>
	 * <li>This method supports {@link GxActiveRecord::representingColumn} and {@link GxActiveRecord::toString}.</li>
	 * <li>This method supports tables with composite primary keys.</li>
	 * </ul>
	 * @see CHtml::listData
	 * @param array $models A list of model objects. This parameter
	 * can also be an array of associative arrays (e.g. results of {@link CDbCommand::queryAll}).
	 * @param string $valueField The attribute name for list option values.
	 * Optional. If not specified, the primary key (or keys) will be used.
	 * @param string $textField The attribute name for list option texts.
	 * Optional. If not specified, the {@link GxActiveRecord::__toString} method will be used.
	 * @param string $groupField The attribute name for list option group names. If empty, no group will be generated.
	 * @return array The list data that can be used in {@link dropDownList}, {@link listBox}, etc.
	 */
	public static function listDataEx($models, $valueField = null, $textField = null, $groupField = '')
	{
		return parent::listDataEx($models, $valueField, $textField, $groupField);
	}
	
	
	/**
	 * Returns label attribute of a model and a column
	 */
	static public function labelEx($modelClass, $name)
	{
		// Get label attributes
		$rc = new ReflectionClass($modelClass);
		$modelInstance = $rc->newInstance();
		$vec_labels = $modelInstance->attributeLabels();
		if ( isset($vec_labels[$name]) )
		{
			return $vec_labels[$name];
		}
		
		return $name;
	}
	
	
	/**
	 * Evaluates the value of the specified attribute for the given model.
	 * #MethodTracker
	 * This method improves {@link CHtml::value}, from version 1.1.7 (r3135). Changes:
	 * <ul>
	 * <li>This method supports {@link GxActiveRecord::representingColumn} and {@link GxActiveRecord::toString}.</li>
	 * </ul>
	 * @see CHtml::value
	 * @param mixed $model The model. This can be either an object or an array.
	 * @param string $attribute The attribute name (use dot to concatenate multiple attributes).
	 * Optional. If not specified, the {@link GxActiveRecord::__toString} method will be used.
	 * In this case, the fist parameter ($model) can not be an array, it must be an instance of GxActiveRecord.
	 * @param mixed $defaultValue The default value to return when the attribute does not exist.
	 * @return mixed The attribute value.
	 */
	public static function valueEx($model, $attribute = null, $defaultValue = null)
	{
		if ($attribute === NULL)
		{
			if ( is_object($model) && is_subclass_of($model, 'GxActiveRecord') )
			{
				return $model->__toString();
			}
			return $defaultValue;
		}
		
		if ( is_array($attribute) && isset($attribute['columns']) )
		{
			$vec_values = [];
			foreach ( $attribute['columns'] as $que_column )
			{
				$vec_values[] = parent::value($model, $que_column, $defaultValue);
			}
			if ( !isset($attribute['separator']) )
			{
				$attribute['separator'] = ' ';
			}
			return implode($attribute['separator'], $vec_values);
		}
		
		// $attribute is a string
		return parent::value($model, $attribute, $defaultValue);
	}
	
	

	/**
	 * Generates a text field input.
	 * @param string $name the input name
	 * @param string $value the input value
	 * @param array $htmlOptions additional HTML attributes. Besides normal HTML attributes, a few special
	 * attributes are also recognized (see {@link clientChange} and {@link tag} for more details.)
	 * @return string the generated input field
	 * @see clientChange
	 * @see inputField
	 */
	public static function textField($name,$value='',$htmlOptions=[])
	{
		$output = '';
		$que_append = '';
		$vec_classes = [];

		if ( isset($htmlOptions['prepend']) )
		{
			$vec_classes[] = 'input-prepend';
		}

		if ( isset($htmlOptions['append']) )
		{
			$vec_classes[] = 'input-append';
			$que_append = $htmlOptions['append'];
			unset($htmlOptions['append']);
		}

		if ( !empty($vec_classes) )
		{
			$output .= self::openTag('div', ['class' => implode(" ", $vec_classes)]);
			if ( isset($htmlOptions['prepend']) )
			{
				$output .= '<span class="add-on prepend-on">'. $htmlOptions['prepend'] .'</span>';
				unset($htmlOptions['prepend']);
			}
		}

		$output .=  parent::textField($name, $value, $htmlOptions);

		if ( !empty($vec_classes) )
		{
			if ( !empty($que_append) )
			{
				$output .= '<span class="add-on append-on">'. $que_append .'</span>';
			}
			$output .= '</div>';
		}
		return $output;
	}


	/*
	|--------------------------------------------------------------------------
	| CUSTOM FUNCTIONS
	|--------------------------------------------------------------------------
	*/
	
	/**
	 * Generate an ordered list of items.
	 *
	 * @param  array   $list
	 * @param  array   $htmlOptions
	 * @return string
	 */
	public static function ol($list, $htmlOptions = [])
	{
		return self::listing('ol', $list, $htmlOptions);
	}


	/**
	 * Generate an un-ordered list of items.
	 *
	 * @param  array   $list
	 * @param  array   $htmlOptions
	 * @return string
	 */
	public static function ul($list, $htmlOptions = [])
	{
		return self::listing('ul', $list, $htmlOptions);
	}


	/**
	 * Generate a menu with an un-ordered list of items.
	 *
	 * @param  array   $links
	 * @param  array   $htmlOptions
	 * @return string
	 */
	public static function ul_menu($links, $htmlOptions = [], $show_active_class = false, $menu_tag = 'ul')
	{
		// Check visible
		if ( !empty($links) )
		{
			foreach ( $links as $num_link => $que_link )
			{
				if ( isset($que_link['visible']) && eval('return '. $que_link['visible']. ';') === false )
				{
					unset($links['num_link']);
				}
			}
		}

		$output = '';
		if ( !empty($links) )
		{
			$output = self::listing($menu_tag, $links, $htmlOptions, true);

			if ( $show_active_class )
			{
				// Get URL from module, controller and action
				$que_url = '';
				if ( isset(Yii::app()->controller->module) )
				{
					$que_url = strtolower(Yii::app()->controller->module->id);
				}
				$que_url .= '/'. strtolower(Yii::app()->controller->getId());
				// $que_url .= strtolower(Yii::app()->controller->getAction()->getId());

				// $output = str_replace(Yii::app()->request->requestUri .'"', '" class="active"', $output);
				$output = str_replace($que_url .'/"', $que_url .'/" class="active"', $output);
			}
		}

		return $output;
	}


	/**
	 * Button "more links" using Bootstrap Dropdown menu widget
	 *
	 * @param  array   $vec_items
	 * @param  array   $htmlOptions
	 * @return string
	 */
	public static function more_links($vec_items, $htmlOptions = [])
	{
		$output = '';
		
		// More links dropdown
		if ( count($vec_items) )
		{
			// $output  = '<a class="btn btn-more-links dropdown-toggle" data-toggle="dropdown" href="#">'. Yii::t('app', 'More') .' <span class="caret"></span></a>';
			// $output  = '<a class="btn btn-more-links dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i></a>';
			$output  = '<a class="btn btn-more-links btn-white dropdown-toggle" data-toggle="dropdown" href="#">'. Yii::t('app', 'More') .' <i class="icon fa fa-chevron-up"></i></a>';
			$output .= self::dropdown_menu($vec_items, $htmlOptions);
		}
		return $output;
	}
	
	
	/**
	 * Generates a Bootstrap Dropdown menu widget
	 *
	 * @param  array   $vec_items
	 * @param  array   $htmlOptions
	 * @return string
	 */
	public static function dropdown_menu($vec_items, $htmlOptions = [])
	{
		$output = '';
		
		// Dropdown menu item
		if ( count($vec_items) )
		{
			$output = self::openTag('div', $htmlOptions );
			foreach ( $vec_items as $que_item )
			{
				if ( is_array($que_item) )
				{
					// Check access
					if ( isset($que_item['visible']) && !Expression::evaluate($que_item['visible']) )
					{
						continue;
					}

					// HTML Options
					$item_htmlOptions = [];
					if ( isset($que_item['htmlOptions']) )
					{
						$item_htmlOptions = $que_item['htmlOptions'];
					}
				
					// Icon button
					if ( isset($que_item['icon']) )
					{
                        if ( ! preg_match("/^wb\-|^fa\-/", $que_item['icon']) )
                        {
                            $que_item['icon'] = 'wb-'. $que_item['icon'];
                        }
						$que_item['label'] = '<i class="icon '. $que_item['icon'] .'"></i> '. $que_item['label'];
					}
					
					// Id attribute is requried. If not exists, generate a random one
					if ( !isset($item_htmlOptions['id']) )
					{
						$item_htmlOptions['id'] = uniqid();
					}
				
					// Class "dz-bootbox-confirm" needed
					if ( !isset($item_htmlOptions['class']) )
					{
						$item_htmlOptions['class'] = 'dropdown-item';
					}
					else
					{
						$item_htmlOptions['class'] .= ' dropdown-item';	
					}
									
					// Confirm box??
					if ( isset($que_item['confirm']) )
					{
						$item_htmlOptions['data-confirm'] = $que_item['confirm'];
						$item_htmlOptions['class'] .= ' dz-bootbox-confirm';
					}
					
					// Sub-navigation?
					if ( isset($que_item['items']) && !empty($que_item['items']) )
					{
						if ( empty($que_item['url']) )
						{
							$que_item['url'] = '#';
						}
						
						$submenu = self::dropdown_menu($que_item['items'], ['class' => 'dropdown-menu']);
						if ( !empty($submenu) )
						{
							// $output .=  '<li class="dropdown-submenu pull-left">'. self::link($que_item['label'], $que_item['url'], $item_htmlOptions) . $submenu .'</li>';
						}
					}
					
					// Single item
					else
					{
						if ( isset($que_item['url']) )
						{
							// $output .= '<li>'. self::link($que_item['label'], $que_item['url'], $item_htmlOptions) .'</li>';
							$output .= self::link($que_item['label'], $que_item['url'], $item_htmlOptions);
						}
						else
						{
							// $output .= '<li>'. $que_item['label'] .'</li>';
							$output .= $que_item['label'];
						}	
					}					
				}
				
				// Separator
				else if ( $que_item == '---' )
				{
					$output .= '<div class="divider"></div>';
				}
			}
			
			// Check if there's one child '<li>' at least in menu
			if ( $output == self::openTag('div', $htmlOptions ) )
			{
				return '';
			}
			$output .= '</div>';
		}
		return $output;
	}
	

	/**
	 * Generate an ordered or un-ordered list.
	 *
	 * @param  string  $type
	 * @param  array   $list
	 * @param  array   $htmlOptions
	 * @param  bool    $is_menu
	 * @return string
	 */
	private static function listing($type, $list, $htmlOptions = [], $is_menu = false)
	{
		$html = '';

		$current_module = Yii::currentModule(true);
		$current_controller =  Yii::currentController(true);
		$current_action =  Yii::currentAction(true);

		if ( count($list) <= 0 )
		{
			return $html;
		}

		foreach ($list as $key => $value)
		{
			// If the value is an array, we will recurse the function so that we can
			// produce a nested list within the list being built. Of course, nested
			// lists may exist within nested lists, etc.
			if (is_array($value))
			{
				// Check access
				if ( isset($value['visible']) && !Expression::evaluate($value['visible']) )
				{
					continue;
				}
					
				// Menu (check current URL)
				if ( $is_menu )
				{
					if ( strtolower($value['url'][0]) == '/'. $current_module .'/'. $current_controller .'/'. $current_action || ( $current_action == Yii::app()->controller->defaultAction && (strtolower($value['url'][0]) == '/'. $current_module .'/'. $current_controller) ) )
					{
						$html .= '<li class="active">';
					}
					else
					{
						$html .= '<li>';
					}
					$html .= self::link($value['label'], $value['url']) .'</li>';
				}
				else
				{
					if (is_int($key))
					{
						$html .= self::listing($type, $value);
					}
					else
					{
						$html .= '<li>'. $key . self::listing($type, $value) .'</li>';
					}
				}
			}
			else
			{
				$html .= '<li>'. $value .'</li>';
			}
		}

		return self::openTag($type, $htmlOptions) . $html . '</'.$type.'>';
	}
	
	
	/**
	 * Generates a hyperlink tag
	 *
	 * @param string
	 * @param mixed
	 * @param array
	 * @return string the generated hyperlink
	 */
	public static function link($text, $url = '#', $htmlOptions = [])
	{
	    // Add a bootstrap icon?
	    if ( isset($htmlOptions['icon']) )
	    {
            if ( ! preg_match("/^wb\-|^fa\-/", $htmlOptions['icon']) )
            {
                $htmlOptions['icon'] = 'wb-'. $htmlOptions['icon'];
            }
	        $text = '<i class="'. $htmlOptions['icon'] .'"></i> ' . $text;
	        unset($htmlOptions['icon']);
	    }
	    if ( $url == '#')
	    {
	    	$url = 'javascript:void(0);';
	    }
	    return parent::link($text, $url, $htmlOptions);
    }


    /**
     * Generates a mailto link.
     */
    public static function mailto($text, $email='', $htmlOptions = [])
    {
    	if ( !isset($htmlOptions['target']) )
    	{
    		$htmlOptions['target'] = '_blank';
    	}
    	return parent::mailto($text, $email, $htmlOptions);
    }


	/**
	 * Generates a tag list from an array
	 *
	 * @param array
	 * @return string
	 */
    public static function tagList($vec_tags, $max_tags = 0)
    {
    	$output = '<div class="tagList-wrapper">';
    	if ( !empty($vec_tags) )
    	{
    		if ( $max_tags > 0 && count($vec_tags) > $max_tags )
    		{
    			$output = '<div class="tagList-wrapper tagList-more-wrapper tooltips" data-toggle="tooltip" data-container="body" data-original-title="Show all tags">';

    			// Javascript behavior
				Yii::app()->clientScript->registerScript('js_tag_list', "$('.tagList-more-wrapper').dzMoreTags();",  CClientScript::POS_READY);
    		}
    		foreach ( $vec_tags as $num_tag => $que_tag )
    		{
    			if ( $max_tags > 0 )
    			{
    				// Show more with "ellipsis"
    				if ( $num_tag == $max_tags )
    				{
    					$output .= '<span class="more-tags">...</span>';
    				}
    				
    				if ( $num_tag >= $max_tags )
    				{
    					$output .= '<span class="myTag hide-tag">'. $que_tag .'</span>';
    				}
    				else
    				{
    					$output .= '<span class="myTag">'. $que_tag .'</span>';
    				}
    			}
    			else
    			{
    				$output .= '<span class="myTag">'. $que_tag .'</span>';
    			}
    		}
    		// $output .= '<span class="myTag">' . implode('</span><span class="myTag">', $vec_tags) .'</span>';
    	}
    	return $output .'</div>';
    }


    /**
	 * Turns a total minute value into hh:mm format
	 *
	 * @param int
	 * @param string
	 * @return string
	 */
    public static function hour_format($value, $unit_type = 'minute')
    {
    	/*
    	// Convert seconds to minutes
    	if ( $value > 0 && $unit_type == 'second' )
    	{
    		$value = floor($value/60);
    	}

    	// $value = round($value, 0);
		return sprintf("%02d:%02d", floor($value/60), $value%60);
		*/
		return DateHelper::hour_format($value, $unit_type);
    }


    /**
     * Generates a breadcrumb menu
     */
    public static function breadcrumbs($links)
    {
    	return self::ul_menu($links, ['class' => 'breadcrumb'], false, 'ol');
    }


    /**
     * Generates a control group with a check box list.
     */
    public static function checkBoxListControlGroup($name, $select = '', $vec_data = [], $htmlOptions = ['container' => ''])
    {
    	$output = '';
		if ( !empty($vec_data) )
		{
   			$output .= '<div class="checkbox-custom checkbox-primary">';
   			$output .= str_replace('<br />', '</div><div class="checkbox-custom checkbox-primary">', self::checkBoxList($name, $select, $vec_data, $htmlOptions));
   			$output .= '</div>';
		}

		return $output;
    }


    /**
     * Generates a control group with a check box list.
     */
    public static function radioButtonListControlGroup($name, $select = '', $vec_data = [], $htmlOptions = ['container' => ''])
    {
    	$output = '';
		if ( !empty($vec_data) )
		{
   			$output .= '<div class="radio-custom radio-primary">';
   			$output .= str_replace('<br />', '</div><div class="radio-custom radio-primary">', self::radioButtonList($name, $select, $vec_data, $htmlOptions));
   			$output .= '</div>';
		}

		return $output;
    }


    /**
     * Generates a pagination.
     * @param array $items the pagination buttons.
     * @param array $htmlOptions additional HTML attributes.
     * @return string the generated pagination.
     */
    public static function pagination($items, $htmlOptions = [])
    {
        if ( !empty($items) )
        {
            self::addCssClass('pagination', $htmlOptions);
            /*
            $size = BsArray::popValue('size', $htmlOptions);
            if ( !empty($size) )
            {
                self::addCssClass('pagination-' . $size, $htmlOptions);
            }
            */
            $align = BsArray::popValue('align', $htmlOptions);
            if ( !empty($align) )
            {
                self::addCssClass('pagination-' . $align, $htmlOptions);
            }
            $listOptions = BsArray::popValue('listOptions', $htmlOptions, []);
            $output = self::openTag('ul', $htmlOptions, $listOptions);
            foreach ($items as $itemOptions)
            {
                // todo: consider removing the support for htmlOptions.
                $options = BsArray::popValue('htmlOptions', $itemOptions, []);
                if ( !empty($options) )
                {
                    $itemOptions = BsArray::merge($options, $itemOptions);
                }
                $label = BsArray::popValue('label', $itemOptions, '');
                $url = BsArray::popValue('url', $itemOptions, false);
                $output .= self::paginationLink($label, $url, $itemOptions);
            }
            $output .= '</ul>';
            return $output;
        }
        return '';
    }


    /**
     * Generates a pagination link.
     * @param string $label the link label text.
     * @param mixed $url the link url.
     * @param array $htmlOptions additional HTML attributes.
     * @return string the generated link.
     */
    public static function paginationLink($label, $url, $htmlOptions = [])
    {
        $linkOptions = BsArray::popValue('linkOptions', $htmlOptions, []);
        if ( BsArray::popValue('active', $htmlOptions, false) )
        {
            $label .= self::tag('span', ['class' => 'sr-only'], BsArray::popValue('activeLabelSrOnly', $htmlOptions, '(current)'));
            self::addCssClass('active', $htmlOptions);
        }
        if ( BsArray::popValue('disabled', $htmlOptions, false) )
        {
            self::addCssClass('disabled', $htmlOptions);
        }
        $linkOptions['class'] = 'page-link';
        $content = self::link($label, $url, $linkOptions);
        return self::tag('li', $htmlOptions, $content);
    }


    /**
	 * Displays a summary of validation errors for one or several models.
	 * @param mixed $model the models whose input errors are to be displayed. This can be either
	 * a single model or an array of models.
	 * @param string $header a piece of HTML code that appears in front of the errors
	 * @param string $footer a piece of HTML code that appears at the end of the errors
	 * @param array $htmlOptions additional HTML attributes to be rendered in the container div tag.
	 * A special option named 'firstError' is recognized, which when set true, will
	 * make the error summary to show only the first error message of each attribute.
	 * If this is not set or is false, all error messages will be displayed.
	 * This option has been available since version 1.1.3.
	 * @return string the error summary. Empty if no errors are found.
	 * @see CModel::getErrors
	 * @see errorSummaryCss
	 */
	public static function errorSummary($model, $header=null, $footer=null, $htmlOptions=[])
	{
		$content='';
		
		if ( !is_array($model) )
		{
			$model = [$model];
		}
		
		if ( isset($htmlOptions['firstError']) )
		{
			$firstError = $htmlOptions['firstError'];
			unset($htmlOptions['firstError']);
		}
		else
		{
			$firstError = false;
		}

		foreach( $model as $m )
		{
			foreach ( $m->getErrors() as $errors )
			{
				foreach ( $errors as $error )
				{
					if ( $error!='' )
					{
						$content .= "<li>". $error ."</li>\n";
					}
					if ( $firstError )
					{
						break;
					}
				}
			}
		}
		if ( $content !== '' )
		{
			// if($header===null)
			// $header='<p>'.Yii::t('yii','Please fix the following input errors:').'</p>';
			if ( !isset($htmlOptions['class']) )
			{
				$htmlOptions['class'] = self::$errorSummaryCss;
			}

			if ( !isset($htmlOptions['id']) )
			{
				$htmlOptions['id'] = 'form-messages';
			}
			return self::tag('div', $htmlOptions, $header ."\n<ul>\n". $content ."</ul>". $footer);
		}
		else
		{
			return '';
		}
	}


    /**
     * Generates HTML name for given model.
     * @see CHtml::setModelNameConverter()
     * @param CModel|string $model the data model or the model class name
     * @return string the generated HTML name value
     * @since 1.1.14
     */
    public static function modelName($model)
    {
        if ( is_callable(self::$_modelNameConverter) )
        {
            return call_user_func(self::$_modelNameConverter,$model);
        }

        $className = is_object($model) ? StringHelper::basename(get_class($model)) : (string)$model;
        return trim(str_replace('\\','_',$className),'_');
    }


    /**
     * Render an icon image
     */
    public static function icon($icon, $tag = 'i')
    {
        // By default, use wb library (https://github.com/thecreation/web-icons)
        $class_name = $icon;        

        // Use Font Awesome icons?
        if ( preg_match("/^fa\-/", $icon) )
        {
            Yii::app()->clientscript->registerCssFile(Yii::app()->theme->baseUrl .'/fonts/font-awesome/font-awesome.min.css');
            $class_name = $icon;
        }
        else if ( !preg_match("/^wb\-/", $icon) )
        {
            $class_name = 'wb-'. $icon;
        }

        return '<'. $tag .' class="'. $class_name .'"></'. $tag .'>';
    }


    /**
     * Appends new class names to the given options.
     * 
     * @param mixed $className the class(es) to append.
     * @param array $htmlOptions the options.
     * @return array the options.
     */
    public static function addCssClass($className, &$htmlOptions)
    {
        // Always operate on arrays
        if ( is_string($className) )
        {
            $className = explode(' ', $className);
        }
        
        if ( isset($htmlOptions['class']) )
        {
            $classes = array_filter(explode(' ', $htmlOptions['class']));
            foreach ( $className as $class )
            {
                $class = trim($class);
                // Don't add the class if it already exists
                if ( array_search($class, $classes) === false )
                {
                    $classes[] = $class;
                }
            }
            $className = $classes;
        }
        $htmlOptions['class'] = implode(' ', $className);
    }


    /**
     * Generates input name for a model attribute.
     * 
     * @see CHtml::resolveName
     */
    public static function resolve_name($model, $attribute)
    {
        return self::resolveName($model, $attribute);
    }


    /**
     * Generates input field ID for a model attribute.
     * 
     * @see CHtml::activeId
     */
    public static function resolve_id($model, $attribute)
    {
        return self::activeId($model, $attribute);
    }

}