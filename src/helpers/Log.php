<?php
/**
 * Log class file
 *
 * Helper class for logging
 * 
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2018 Fabián Ruiz
 */

namespace dz\helpers;

use dz\db\ActiveRecord;
use Yii;

Yii::import('@core.src.helpers.DzLog');

class Log extends \DzLog
{
	/**
	 * Magic method __callStatic to invoke methods like "dev", "error" or "warning"
	 */
	public static function __callStatic($method, $args)
    {
        // echo __METHOD__ . "\n";
        $args[] = $method;
        return call_user_func_array(__CLASS__ . '::log', $args);
    }
}
	