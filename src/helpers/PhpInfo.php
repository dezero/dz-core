<?php
/**
 * PhpInfo utility class
 * 
 * @see https://github.com/craftcms/cms/blob/develop/src/utilities/PhpInfo.php
 */

namespace dz\helpers;

class PhpInfo
{
    /**
     * Parses and returns the PHP info.
     *
     * @return array
     */
    public static function info()
    {
        // Remove any arrays from $_SERVER to get around an "Array to string conversion" error
        $serverVals = [];

        if (isset($_SERVER)) {
            foreach ($_SERVER as $key => $value) {
                if (is_array($value)) {
                    $serverVals[$key] = $value;
                    $_SERVER[$key] = 'Array';
                }
            }
        }

        ob_start();
        phpinfo(INFO_ALL);
        $phpInfoStr = ob_get_clean();

        // Put the original $_SERVER values back
        foreach ($serverVals as $key => $value) {
            $_SERVER[$key] = $value;
        }

        $replacePairs = [
            '#^.*<body>(.*)</body>.*$#ms' => '$1',
            '#<h2>PHP License</h2>.*$#ms' => '',
            '#<h1>Configuration</h1>#' => '',
            "#\r?\n#" => '',
            '#</(h1|h2|h3|tr)>#' => '</$1>' . "\n",
            '# +<#' => '<',
            "#[ \t]+#" => ' ',
            '#&nbsp;#' => ' ',
            '#  +#' => ' ',
            '# class=".*?"#' => '',
            '%&#039;%' => ' ',
            '#<tr>(?:.*?)"src="(?:.*?)=(.*?)" alt="PHP Logo" /></a><h1>PHP Version (.*?)</h1>(?:\n+?)</td></tr>#' => '<h2>PHP Configuration</h2>' . "\n" . '<tr><td>PHP Version</td><td>$2</td></tr>' . "\n" . '<tr><td>PHP Egg</td><td>$1</td></tr>',
            '#<h1><a href="(?:.*?)\?=(.*?)">PHP Credits</a></h1>#' => '<tr><td>PHP Credits Egg</td><td>$1</td></tr>',
            '#<tr>(?:.*?)" src="(?:.*?)=(.*?)"(?:.*?)Zend Engine (.*?),(?:.*?)</tr>#' => '<tr><td>Zend Engine</td><td>$2</td></tr>' . "\n" . '<tr><td>Zend Egg</td><td>$1</td></tr>',
            '# +#' => ' ',
            '#<tr>#' => '%S%',
            '#</tr>#' => '%E%',
        ];

        $phpInfoStr = preg_replace(array_keys($replacePairs), array_values($replacePairs), $phpInfoStr);

        $sections = explode('<h2>', strip_tags($phpInfoStr, '<h2><th><td>'));
        unset($sections[0]);

        $phpInfo = [];

        foreach ($sections as $section) {
            $heading = substr($section, 0, strpos($section, '</h2>'));

            if (preg_match_all('#%S%(?:<td>(.*?)</td>)?(?:<td>(.*?)</td>)?(?:<td>(.*?)</td>)?%E%#', $section, $matches, PREG_SET_ORDER) !== 0) {
                /** @var array[] $matches */
                foreach ($matches as $row) {
                    if (!isset($row[2])) {
                        continue;
                    }

                    $value = $row[2];
                    $name = $row[1];

                    $phpInfo[$heading][$name] = $value;
                }
            }
        }

        return $phpInfo;
    }


    /**
     * Get PHP memory_limit
     */
    public static function memory_limit()
    {
        return ini_get('memory_limit');
    }


    /**
     * Get PHP max_execution_time
     */
    public static function max_execution_time()
    {
        return (int)trim(ini_get('max_execution_time'));
    }


    /**
     * Get PHP post_max_size
     */
    public static function post_max_size()
    {
        return ini_get('post_max_size');
    }


    /**
     * Get PHP upload_max_filesize
     */
    public static function upload_max_filesize()
    {
        return ini_get('upload_max_filesize');
    }
}