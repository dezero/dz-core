<?php
/**
 * PHP Expression class file for Dz Framework
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2019 Dezero
 */

namespace dz\helpers;

use Yii;

class Expression
{
    /**
     * Redefine evaluateExpression function
     *
     * It allows for a more complex PHP code snippet to be run for the calculation of 'value'
     *
     * @link http://www.yiiframework.com/extension/pcphpdatacolumn/
     */
    static public function evaluate($_expression_, $_data_ = [])
    {
        if ( is_string($_expression_) )
        {
            extract($_data_);
            if ( ! preg_match("/return/", $_expression_) )
            {
                $_expression_ = 'return ('. $_expression_ .')';
            }
            return eval($_expression_ . ';');
        }
        else
        {
            $_data_[] = self;
            return call_user_func_array($_expression_, $_data_);
        }
    }
}