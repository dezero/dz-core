<?php
/**
 * MarkdownHelper class file for Dz Framework
 *
 * Helper class for working with Parsedown
 * 
 * @see https://github.com/erusev/parsedown
 */

namespace dz\helpers;

use dz\helpers\StringHelper;
use dz\utils\Parsedown;
use Yii;

class MarkdownHelper
{
    /**
     * Converts markdown into HTML
     * 
     * @see https://github.com/erusev/parsedown
     */
    public static function convert($content, $vec_config = [])
    {
        // $parsedown = \Parsedown::instance();
        // $parsedown = \ParsedownExtra::instance();
        $parsedown = Parsedown::instance();

        if ( isset($vec_config['breaks_enabled']) )
        {
            $parsedown->setBreaksEnabled($vec_config['breaks_enabled']);
        }

        if ( isset($vec_config['markup_escaped']) )
        {
            $parsedown->setMarkupEscaped($vec_config['markup_escaped']);
        }

        if ( isset($vec_config['safe_mode']) )
        {
            $parsedown->setSafeMode($vec_config['safe_mode']);
        }

        if ( isset($vec_config['urls_linked']) )
        {
            $parsedown->setUrlsLinked($vec_config['urls_linked']);
        }

        // DZ custom - URL internal
        if ( isset($vec_config['is_internal_url']) )
        {
            $content = str_replace('url://', Url::base() .'/', $content);
        }

        // Inline?
        if ( isset($vec_config['is_inline']) )
        {
            return $parsedown->line($content);
        }

        // Get converted text
        $output = $parsedown->text($content);

        // Clean HTML?
        if ( isset($vec_config['vec_allowed_tags']) )
        {
            $output = StringHelper::clean_html($output, $vec_config['vec_allowed_tags']);
        }

        return $output;
    }
}
