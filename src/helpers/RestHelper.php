<?php
/**
 * RestHelper class file for Dz Framework
 *
 * Helper class for working with REST API
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2023 Fabián Ruiz
 */

namespace dz\helpers;

use dz\helpers\DateHelper;
use Yii;

class RestHelper
{
    /**
     * Convert an associative array to REST API format:
     *
     *  ['key' => 'value'] to ['name' => 'key', 'value' => 'value']
     */
    public static function array($vec_data)
    {
        if ( ! is_array($vec_data) || empty($vec_data) )
        {
            return $vec_data;
        }

        $vec_output = [];
        foreach ( $vec_data as $key => $value )
        {
            $vec_output[] = [
                'name'  => $key,
                'value' => is_array($value) ? self::array($vec_data) : $value
            ];
        }

        return $vec_output;
    }


    /**
     * Convert a date to REST API format: "Y-m-d H:i:s"
     */
    public static function date($date)
    {
        if ( empty($date) )
        {
            return $date;
        }

        // Parser from dd/mm/YYYY to UNIX timestamp format
        if ( preg_match("/\//", $date) )
        {
            if ( preg_match("/\:/", $date) )
            {
                $date = DateHelper::date_format_hour_to_unix($date);
            }
            else
            {
                $date = DateHelper::date_format_to_unix($date);
            }
        }

        return date("Y-m-d H:i:s", $date);
    }


    /**
     * Return a formatted date
     */
    public static function format_date($date)
    {
        return preg_match("/\//", $date) ? $date : DateHelper::unix_to_date($date);
    }


    /**
     * Simple date format validation
     *
     * Allowed formats:
     *  - YYYY-MM-DD          (example: 2016-02-23)
     *  - YYYY-MM-DD hh:mm    (example: 2016-02-23 11:45)
     *  - YYYY-MM-DD hh:mm:ss (example: 2016-02-23 11:45:32)
     */
    public static function validate_date($date)
    {
        // Check allowed format lenght
        $que_length = strlen($date);
        if ( $que_length != 10 && $que_length != 16 && $que_length != 19 )
        {
            return false;
        }

        // Check "YYYY-MM-DD" format
        // @see https://stackoverflow.com/questions/13194322/php-regex-to-check-date-is-in-yyyy-mm-dd-format
        // if ( ! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])/", $date) )
        // {
        //  return false;
        // }

        // Check "YYYY-MM-DD" format using strtotime return value
        if ( $que_length == 10 )
        {
            $date .= ' 00:00:00';
        }

        // Check "YYYY-MM-DD hh::mm" format using strtotime return value
        else if ( $que_length == 16 )
        {
            $date .= ':00';
        }

        $unix_date = strtotime(date($date));
        if ( $unix_date === false )
        {
            return false;
        }

        return date($date);
    }


    /**
     * Parse array errors into a string ready to view on JSON output
     */
    public static function parse_errors()
    {
        $error_output = '';
        if ( !empty($vec_errors) )
        {
            if ( is_array($vec_errors) )
            {
                foreach ( $vec_errors as $que_field => $vec_field_errors )
                {
                    if ( !empty($error_output) )
                    {
                        $error_output = ' | ';
                    }
                    else
                    {
                        $error_output = '[';
                    }

                    if ( is_array($vec_field_errors) )
                    {
                        foreach ( $vec_field_errors as $que_error )
                        {
                            $error_output .= $que_field .': '. $que_error;
                        }
                    }
                    else
                    {
                        $error_output .= $que_field .': '. $vec_field_errors;
                    }
                }
                $error_output .= ']';
            }
        }
        return $error_output;
    }


    /**
     * Get output from an asset model for REST API
     */
    public static function get_asset_response($asset_model)
    {
        $file_url = str_replace("www/", "", $asset_model->file_url());

        $vec_response = [
            'id'            => (int)$asset_model->file_id,
            'url'           => $file_url,
            'file_name'     => $asset_model->file_name,
            'created_date'  => self::date($asset_model->raw_attributes['created_date']),
            'updated_date'  => self::date($asset_model->raw_attributes['updated_date']),
        ];

        return $vec_response;
    }
}
