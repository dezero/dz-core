<?php
/**
 * XmlParser class file
 *
 * Helper class to work with XML files
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2023 Dezero
 */

namespace dz\helpers;

use dz\helpers\StringHelper;
use Yii;

class XmlParser
{
    /**
     * Parser a XML string to a PHP array using SimpleXML library
     */
    static public function xml_to_array($raw_xml_string)
    {
        $simple_xml = simplexml_load_string($raw_xml_string, 'SimpleXMLElement', LIBXML_NOCDATA);
        if ( !empty($simple_xml) )
        {
            $vec_data = json_decode(json_encode((array)$simple_xml), true);
            return self::xml_filter_array($vec_data);
        }

        return [];
    }


    /**
     * Removes empty values on an array
     *
     * <PRODUCT></PRODUCT> turns into [PRODUCT] => Array(0 => '')
     */
    static public function xml_filter_array($vec_array)
    {
        foreach ( $vec_array as $que_tag => $que_value )
        {
            if ( is_array($que_value) )
            {
                if ( isset($que_value[0]) && count($que_value) == 1 && empty(StringHelper::trim($que_value[0])) )
                {
                    $vec_array[$que_tag] = "";
                }
                else
                {
                    $vec_array[$que_tag] = self::xml_filter_array($que_value);
                }
            }
        }

        return $vec_array;
    }


    /**
     * Remove white spaces
     *
     * First version (used here) extracted from BASIPIM when exporting data to XML files
     *
     * Second version in https://pageconfig.com/post/remove-undesired-characters-with-trim_all-php
     */
    public static function trim_all($text)
    {
        return StringHelper::trim_all($text);
    }


    /**
     * Escape XML strings
     *
     * @see http://stackoverflow.com/questions/3426090/how-do-you-make-strings-xmlsafe
     */
    public static function escape($xml_content, $is_cdata = false, $is_html = true)
    {
        $output_xml = '';
        $xml_content = StringHelper::trim($xml_content);

        if ( $xml_content !== "" )
        {
            // Remove all non printables ASCII characets
            // @see http://stackoverflow.com/questions/1176904/php-how-to-remove-all-non-printable-characters-in-a-string
            // $xml_content = preg_replace('/[^[:print:]]/', '', $xml_content);

            // Remove invisible characters
            $xml_content = StringHelper::remove_invisible_characters($xml_content);

            // Remove all CONTROL characters
            $xml_content = StringHelper::remove_control_characters($xml_content);

            // Remove carriage returns, tabs and line feeds
            // @see http://stackoverflow.com/questions/3059091/how-to-remove-carriage-returns-from-output-of-string
            // $xml_content = str_replace(["\n", "\t", "\r"], '', $xml_content);

            // CDATA
            if ( $is_cdata )
            {
                // HTML tags is allowed. Let's clean HTML tags
                if ( $is_html )
                {
                    $xml_content = str_replace(['<span></span>', '<p></p>', '<li></li>', ''], '', $xml_content);
                    $xml_content = str_replace(['<br></p>'], '</p>', $xml_content);
                    $xml_content = str_replace(['<p><br></p>'], '<br>', $xml_content);
                }

                // HTML tags is not allowed
                else
                {
                    $xml_content = StringHelper::clean_html($xml_content);
                }
                $output_xml = '<![CDATA['. $xml_content .']]>';
            }
            else
            {
                // Convert XML strings
                $output_xml = htmlspecialchars($xml_content, ENT_XML1, 'UTF-8');
            }
        }

        return $output_xml;
    }
}
