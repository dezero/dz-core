<?php
/**
 * Database helper class file
 * 
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2018 Dezero
 */

namespace dz\helpers;

use Yii;

class DB
{
    /**
     * Shortcut to Yii::app()->db->createCommand()
     * 
     * Executes a SQL query
     */
    public static function sql($sql = '', $vec_params = [])
    {
        $command = Yii::app()->db->createCommand($sql);
        if ( !empty($vec_params) )
        {
            foreach ( $vec_params as $column => $value )
            {
                $command->bindParam($column, $value, PDO::PARAM_STR);
            }
        }
        return $command;
    }


    /**
     * Escape a string for MySQL param
     * 
     * @see CDbCriteria::addSearchCondition()
     */
    public static function escape($keyword)
    {
        return strtr($keyword,array('%'=>'\%', '_'=>'\_', '\\'=>'\\\\'));
    }


    /**
     * Export array data in INSERT SQL sentences
     */
    public static function export_as_sql($table_name, $vec_data, $vec_options = array() )
    {
        $sql_output = PHP_EOL.PHP_EOL."--\n-- Dumping data for table `$table_name`\n--".PHP_EOL.PHP_EOL;
        if ( !empty($vec_data) )
        {
            // Field names
            if ( isset($vec_options['excluded_fields']) )
            {
                foreach ( $vec_options['excluded_fields'] as $que_excluded_field )
                {
                    if ( isset($vec_data[0][$que_excluded_field]) )
                    {
                        unset($vec_data[0][$que_excluded_field]);
                    }
                }
            }
            $vec_field_names = array_keys($vec_data[0]);

            // SQL INSERT
            $sql_output .= 'INSERT INTO '. Yii::app()->db->quoteTableName($table_name) .' ('. implode(', ', $vec_field_names) .') VALUES' . PHP_EOL;
            $total_rows = count($vec_data);
            foreach ( $vec_data as $num_row => $que_data )
            {
                if ( !empty($que_data) )
                {
                    $que_row = array();
                    foreach ( $que_data as $row_key => $row_value )
                    {
                        $excluded_field = FALSE;
                        if ( isset($vec_options['excluded_fields']) AND in_array($row_key, $vec_options['excluded_fields']) )
                        {
                            $excluded_field = TRUE;
                        }

                        if ( !$excluded_field )
                        {
                            if ( $row_value === null )
                            {
                                $que_row[$row_key] = 'NULL';
                            }
                            else
                            {
                                $que_row[$row_key] = Yii::app()->db->quoteValue($row_value);
                            }
                        }
                    }
                    $sql_output .= '('. implode(',', $que_row) .')';
                    if ( $num_row == $total_rows - 1 )
                    {
                        $sql_output .= ';'. PHP_EOL. PHP_EOL;
                    }
                    else
                    {
                        $sql_output .= ','. PHP_EOL;
                    }
                }
            }
        }
        return $sql_output;
    }
}