<?php
/**
 * DzCache class file
 *
 * Simplest class to save data in cache
 * 
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2017 Dezero
 */

namespace dz\helpers;

use Yii;

class CacheHelper
{
	/**
     * Caching data
     */
    public static $vec_data = [];


    /**
     * Set data to cache
     */
    public static function set_cache($entity_type, $id, $vec_attributes = [])
    {
    	if ( ! isset(self::$vec_data[$entity_type]) )
		{
			self::$vec_data[$entity_type] = [];
		}

		self::$vec_data[$entity_type][$id] = $vec_attributes;
		
        return true;
    }


    /**
     * Get data from cache
     */
    public static function get_cache($entity_type, $id)
    {
    	if ( ! isset(self::$vec_data[$entity_type]) || ! isset(self::$vec_data[$entity_type][$id]) )
		{
			return false;
		}

		return self::$vec_data[$entity_type][$id];
    }


    /**
     * Clear / flush caches
     */
    public static function clear_cache($cache_id = 'cache')
    {
        $vec_output = [
            'messages'  => [],
            'errors'    => []
        ];

        $vec_caches = explode(',', $cache_id);
        foreach ( $vec_caches as $cache )
        {
            $cache = trim($cache);
            if ( isset(Yii::app()->$cache) )
            {
                if ( Yii::app()->$cache instanceof CApcCache )
                {
                    $vec_output['errors'][] = "Cache '{$cache}' is CApcCache and therefor unsupported";
                }
                else if ( Yii::app()->$cache->flush() )
                {
                    $vec_output['messages'][] = "Cache '{$cache}' cleared";
                }
                else
                {
                    $vec_output['errors'][] =  "Cache '{$cache}' - Clear cache failed";
                }
            }
            else
            {
                $vec_output['messages'][] = "Cache '{$cache}' - No cache configured";
            }
        }

        return $vec_output;
    }
}
