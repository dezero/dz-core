<?php
/**
 * Http class file
 *
 * Helper class for HTTP requests
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2015 Dezero
 */

namespace dz\helpers;

use Yii;

class Http
{
	/**
	 * Return the http status message based on integer status code
	 *
	 * @param int $status HTTP status code
	 *
	 * @return string status message
	 */
	public static function get_status_code_message($status)
	{
	    $vec_codes = array(
			100 => 'Continue',
			101 => 'Switching Protocols',
			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative Information',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Found',
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',
			306 => '(Unused)',
			307 => 'Temporary Redirect',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Timeout',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested Range Not Satisfiable',
			417 => 'Expectation Failed',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway',
			503 => 'Service Unavailable',
			504 => 'Gateway Timeout',
			505 => 'HTTP Version Not Supported',
	    );

	    // List
	    if ( $status == 'list' )
	    {
	    	foreach ( $vec_codes as $num_code => $message_code )
	    	{
	    		$vec_codes[$num_code] = $num_code .' - '. $message_code;
	    	}
	    	return $vec_codes;
	    }
	    return (isset($vec_codes[$status])) ? $vec_codes[$status] : '';
	}


	/**
     * Save a file from external URL to an internal directory path
     */
    public static function download_external_file($source_url, $file_path, $file_name, $is_save_log = TRUE)
    {
    	// Import needed library classes
        Yii::import("@lib.EHttpClient.*");

    	// Configure EHttpClient
		$source_uri = \EUri::factory($source_url);
		$http_config = array(
			'adapter'	=>	'EHttpClientAdapterCurl',
			'timeout'	=>	'60',
		);
		if ( !empty(Yii::app()->params['temporaryPath']) )
		{
			$http_config['stream_tmp_dir'] = Yii::app()->params['temporaryPath'];
		}

		// Download file stream
		$client = new \EHttpClient($source_uri, $http_config);
		$client->setStream();
		$response = $client->request("GET");

		if ( $response->isSuccessful() )
		{
			// Remove basePath (just in case). Because it's added after
			$file_path = str_replace(Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR, '', $file_path);

			//--> Save the FILE in filesystem with CFile extension

	    	// Destination directory exists
	    	$destination_dir = Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . $file_path;
	    	$new_dir = Yii::app()->file->set($destination_dir);
			if ( !$new_dir->isDir )
			{
				$new_dir->createDir(Yii::app()->file->default_permissions, $destination_dir);
			}

			// Copy temp file into Yii structure
			$image_filename = basename($source_url);
			$file = Yii::app()->file->set($response->getStreamName(), TRUE);
			$destination_path = Yii::app()->params["basePath"] . DIRECTORY_SEPARATOR . $file_path . DIRECTORY_SEPARATOR . $file_name;

			if ( $file->copy($destination_path) )
			{
				$destination_file = Yii::app()->file->set($destination_path);

				// Body is encoded by GZIP or DEFLATE?
				$zip_enconding = strtolower($response->getHeader('content-encoding'));
				if ( $zip_enconding == 'gzip' OR $zip_enconding == 'deflate' )
				{
					$destination_file->setContents($response->getBody());
				}

				return Yii::app()->file->set($destination_path);
			}
			else if ( $is_save_log )
			{
				Yii::log("Error saving external file from ". $source_url ." to ". $destination_path,  "profile", "error");
			}
		}
		else if ( $is_save_log )
		{
			// Yii::log("Error downloading external from  ". $source_url ." - ". print_r($response->getRawBody(), TRUE),  "profile", "error");
			Yii::log("Error downloading external from  ". $source_url ." - ". print_r($response, TRUE),  "profile", "error");
		}

		return FALSE;
    }


    /**
     * Send a file to the browser
     */
    public static function send_file($file, $file_name = null, $mime_type = null)
    {
        // Get the contents of the file
        $content = $file->getContents();

        // Get the file name
        if ( $file_name === null )
        {
            $file_name = $file->getBasename();
        }

        // Get the MIME type
        if ( $mime_type === null )
        {
            $mime_type = $file->getMimeType();
        }

        // Configure the HTTP headers
        header('Content-Type: '. $mime_type);
        header('Content-Length: ' . strlen($content));
        header('Content-Disposition: inline; filename="' . $file->getBasename() . '"');
        header('Cache-Control: private, max-age=0, must-revalidate');
        header('Pragma: public');

        // send the file content
        echo $content;

        Yii::app()->end();
    }
}
