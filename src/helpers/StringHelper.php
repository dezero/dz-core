<?php
/**
 * StringHelper class file for Dz Framework
 *
 * Helper class for working with strings
 * 
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2022 Fabián Ruiz
 */

namespace dz\helpers;

use CHtmlPurifier;
use dz\helpers\App;
use Yii;

class StringHelper
{
    const UUID_PATTERN = '[A-Za-z0-9]{8}-[A-Za-z0-9]{4}-4[A-Za-z0-9]{3}-[89abAB][A-Za-z0-9]{3}-[A-Za-z0-9]{12}';

    /**
     * Matches Unicode characters that are word boundaries.
     *
     * Characters with the following General_category (gc) property values are used
     * as word boundaries. While this does not fully conform to the Word Boundaries
     * algorithm described in http://unicode.org/reports/tr29, as PCRE does not
     * contain the Word_Break property table, this simpler algorithm has to do.
     * - Cc, Cf, Cn, Co, Cs: Other.
     * - Pc, Pd, Pe, Pf, Pi, Po, Ps: Punctuation.
     * - Sc, Sk, Sm, So: Symbols.
     * - Zl, Zp, Zs: Separators.
     *
     * Non-boundary characters include the following General_category (gc) property
     * values:
     * - Ll, Lm, Lo, Lt, Lu: Letters.
     * - Mc, Me, Mn: Combining Marks.
     * - Nd, Nl, No: Numbers.
     *
     * Note that the PCRE property matcher is not used because we wanted to be
     * compatible with Unicode 5.2.0 regardless of the PCRE version used (and any
     * bugs in PCRE property tables).
     *
     * @see http://unicode.org/glossary
   */
    const PREG_CLASS_WORD_BOUNDARY = <<<'EOD'
\x{0}-\x{2F}\x{3A}-\x{40}\x{5B}-\x{60}\x{7B}-\x{A9}\x{AB}-\x{B1}\x{B4}
\x{B6}-\x{B8}\x{BB}\x{BF}\x{D7}\x{F7}\x{2C2}-\x{2C5}\x{2D2}-\x{2DF}
\x{2E5}-\x{2EB}\x{2ED}\x{2EF}-\x{2FF}\x{375}\x{37E}-\x{385}\x{387}\x{3F6}
\x{482}\x{55A}-\x{55F}\x{589}-\x{58A}\x{5BE}\x{5C0}\x{5C3}\x{5C6}
\x{5F3}-\x{60F}\x{61B}-\x{61F}\x{66A}-\x{66D}\x{6D4}\x{6DD}\x{6E9}
\x{6FD}-\x{6FE}\x{700}-\x{70F}\x{7F6}-\x{7F9}\x{830}-\x{83E}
\x{964}-\x{965}\x{970}\x{9F2}-\x{9F3}\x{9FA}-\x{9FB}\x{AF1}\x{B70}
\x{BF3}-\x{BFA}\x{C7F}\x{CF1}-\x{CF2}\x{D79}\x{DF4}\x{E3F}\x{E4F}
\x{E5A}-\x{E5B}\x{F01}-\x{F17}\x{F1A}-\x{F1F}\x{F34}\x{F36}\x{F38}
\x{F3A}-\x{F3D}\x{F85}\x{FBE}-\x{FC5}\x{FC7}-\x{FD8}\x{104A}-\x{104F}
\x{109E}-\x{109F}\x{10FB}\x{1360}-\x{1368}\x{1390}-\x{1399}\x{1400}
\x{166D}-\x{166E}\x{1680}\x{169B}-\x{169C}\x{16EB}-\x{16ED}
\x{1735}-\x{1736}\x{17B4}-\x{17B5}\x{17D4}-\x{17D6}\x{17D8}-\x{17DB}
\x{1800}-\x{180A}\x{180E}\x{1940}-\x{1945}\x{19DE}-\x{19FF}
\x{1A1E}-\x{1A1F}\x{1AA0}-\x{1AA6}\x{1AA8}-\x{1AAD}\x{1B5A}-\x{1B6A}
\x{1B74}-\x{1B7C}\x{1C3B}-\x{1C3F}\x{1C7E}-\x{1C7F}\x{1CD3}\x{1FBD}
\x{1FBF}-\x{1FC1}\x{1FCD}-\x{1FCF}\x{1FDD}-\x{1FDF}\x{1FED}-\x{1FEF}
\x{1FFD}-\x{206F}\x{207A}-\x{207E}\x{208A}-\x{208E}\x{20A0}-\x{20B8}
\x{2100}-\x{2101}\x{2103}-\x{2106}\x{2108}-\x{2109}\x{2114}
\x{2116}-\x{2118}\x{211E}-\x{2123}\x{2125}\x{2127}\x{2129}\x{212E}
\x{213A}-\x{213B}\x{2140}-\x{2144}\x{214A}-\x{214D}\x{214F}
\x{2190}-\x{244A}\x{249C}-\x{24E9}\x{2500}-\x{2775}\x{2794}-\x{2B59}
\x{2CE5}-\x{2CEA}\x{2CF9}-\x{2CFC}\x{2CFE}-\x{2CFF}\x{2E00}-\x{2E2E}
\x{2E30}-\x{3004}\x{3008}-\x{3020}\x{3030}\x{3036}-\x{3037}
\x{303D}-\x{303F}\x{309B}-\x{309C}\x{30A0}\x{30FB}\x{3190}-\x{3191}
\x{3196}-\x{319F}\x{31C0}-\x{31E3}\x{3200}-\x{321E}\x{322A}-\x{3250}
\x{3260}-\x{327F}\x{328A}-\x{32B0}\x{32C0}-\x{33FF}\x{4DC0}-\x{4DFF}
\x{A490}-\x{A4C6}\x{A4FE}-\x{A4FF}\x{A60D}-\x{A60F}\x{A673}\x{A67E}
\x{A6F2}-\x{A716}\x{A720}-\x{A721}\x{A789}-\x{A78A}\x{A828}-\x{A82B}
\x{A836}-\x{A839}\x{A874}-\x{A877}\x{A8CE}-\x{A8CF}\x{A8F8}-\x{A8FA}
\x{A92E}-\x{A92F}\x{A95F}\x{A9C1}-\x{A9CD}\x{A9DE}-\x{A9DF}
\x{AA5C}-\x{AA5F}\x{AA77}-\x{AA79}\x{AADE}-\x{AADF}\x{ABEB}
\x{E000}-\x{F8FF}\x{FB29}\x{FD3E}-\x{FD3F}\x{FDFC}-\x{FDFD}
\x{FE10}-\x{FE19}\x{FE30}-\x{FE6B}\x{FEFF}-\x{FF0F}\x{FF1A}-\x{FF20}
\x{FF3B}-\x{FF40}\x{FF5B}-\x{FF65}\x{FFE0}-\x{FFFD}
EOD;

    /**
     * Create machine readable name
     */
    public static function readable_name($text)
    {
        $text = strtr($text, [
            'á' => 'a',
            'Á' => 'a',
            'à' => 'a',
            'À' => 'a',
            'ã' => 'a',
            'Ã' => 'a',
            'â' => 'a',
            'Â' => 'a',
            'é' => 'e',
            'è' => 'e',
            'É' => 'e',
            'È' => 'e',
            'í' => 'i',
            'Í' => 'i',
            'ó' => 'o',
            'Ó' => 'o',
            'ò' => 'o',
            'Ò' => 'o',
            'õ' => 'o',
            'Õ' => 'o',
            'ú' => 'u',
            'Ú' => 'u',
            'ñ' => 'n',
            'Ñ' => 'n',
            'ç' => 'c',
            'Ç' => 'c'
        ]);
        $text = self::strtolower($text);
        return preg_replace('@[^a-z0-9_]+@', '-', $text);
    }


    /**
     * Escape XML strings
     *
     * @see http://stackoverflow.com/questions/3426090/how-do-you-make-strings-xmlsafe
     */
    public static function escape_xml($xml_content, $is_cdata = false, $is_html = true)
    {
        return \dz\helpers\XmlParser::escape($xml_content, $is_cdata, $is_html);
    }


    /**
     * Escape SPANISH characters
     */
    public static function escape_spanish_characters($content)
    {
        return strtr($content, [
            'á' => 'a',
            'Á' => 'A',
            'é' => 'e',
            'É' => 'E',
            'í' => 'i',
            'Í' => 'I',
            'ó' => 'o',
            'Ó' => 'O',
            'ú' => 'u',
            'Ú' => 'U',
            'ñ' => 'n',
            'Ñ' => 'N',
        ]);
    }


    /**
     * PDF escape
     */
    public static function escape_pdf($pdf_content)
    {
        return self::escape_spanish_characters($pdf_content);
    }


    /**
     * Convert a string to UPPERCASE but having SPANISH special characters into account
     */
    public static function strtoupper($text, $is_use_mbstring = false)
    {
        if ( $is_use_mbstring )
        {
            $text = mb_strtoupper($text);
        }
        else
        {
            // Use C-locale for ASCII-only uppercase.
            $text = strtoupper($text);

            // Case flip Latin-1 accented letters
            $text = preg_replace_callback('/\\xC3[\\xA0-\\xB6\\xB8-\\xBE]/', '\dz\helpers\StringHelper::unicode_caseflip', $text);

            /*
            $text = str_replace('á', 'Á', $text);
            $text = str_replace('é', 'É', $text);
            $text = str_replace('í', 'Í', $text);
            $text = str_replace('ó', 'Ó', $text);
            $text = str_replace('ú', 'Ú', $text);
            $text = str_replace('ñ', 'Ñ', $text);
            */
        }
        return $text;
    }


    /**
     * Convert a string to LOWERCASE but having SPANISH special characters into account
     */
    public static function strtolower($text, $is_use_mbstring = false)
    {
        if ( $is_use_mbstring )
        {
            $text = mb_strtolower($text);
        }
        else
        {
            // Use C-locale for ASCII-only lowercase.
            $text = strtolower($text);

            // Case flip Latin-1 accented letters.
            $text = preg_replace_callback('/\\xC3[\\x80-\\x96\\x98-\\x9E]/', '\dz\helpers\StringHelper::unicode_caseflip', $text);

            /*
            $text = str_replace('Á', 'á', $text);
            $text = str_replace('É', 'é', $text);
            $text = str_replace('Í', 'í', $text);
            $text = str_replace('Ó', 'ó', $text);
            $text = str_replace('Ú', 'ú', $text);
            $text = str_replace('Ñ', 'ñ', $text);
            */
        }
        return $text;
    }


    /**
     * Alias of StringHelper::strtoupper() method
     * 
     * Convert a string to UPPERCASE but having SPANISH special characters into account
     */
    public static function uppercase($text, $is_use_mbstring = false)
    {
        return self::strtoupper($text, $is_use_mbstring);
    }


    /**
     * Alias of StringHelper::strtolower() method
     * 
     * Convert a string to LOWERCASE but having SPANISH special characters into account
     */
    public static function lowercase($text, $is_use_mbstring = false)
    {
        return self::strtolower($text, $is_use_mbstring);
    }


    /**
     * Capitalizes the first character of a UTF-8 string.
     *
     * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Component%21Utility%21Unicode.php/8.2.x
     */
    public static function ucfirst($text)
    {
        return self::strtoupper(self::substr($text, 0, 1)) . self::substr($text, 1);
    }


    /**
     * Converts the first character of a UTF-8 string to lowercase.
     *
     * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Component%21Utility%21Unicode.php/8.2.x
     */
    public static function lcfirst($text)
    {
        return self::strtolower(self::substr($text, 0, 1)) . self::substr($text, 1);
    }


    /**
     * Capitalizes the first character of each word in a UTF-8 string.
     *
     * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Component%21Utility%21Unicode.php/8.2.x
     */
    public static function ucwords($text)
    {
        // First of all, convert to lowercase
        $text = self::strtolower($text);

        $regex = '/(^|[' . static::PREG_CLASS_WORD_BOUNDARY . '])([^' . static::PREG_CLASS_WORD_BOUNDARY . '])/u';
        return preg_replace_callback($regex, function (array $matches)
        {
            return $matches[1] . self::strtoupper($matches[2], true);
        }, $text);
    }


    /**
     * Convert a string to CAMELCASE
     *
     * Convert "My controller class" to "MyControllerClass"
     *
     * @see https://api.drupal.org/api/drupal/vendor%21easyrdf%21easyrdf%21lib%21Utils.php/9.1.x
     */
    public static function camelcase($text, $is_use_mbstring = false)
    {
        $cc = '';
        foreach (preg_split('/[\\W_]+/', $text) as $part)
        {
           $cc .= self::ucfirst(self::strtolower($part));
        }

        return $cc;
    }


    /**
     * Compares UTF-8-encoded strings in a binary safe case-insensitive manner.
     *
     * @return int Returns < 0 if $str1 is less than $str2; > 0 if $str1 is greater than $str2, and 0 if they are equal.
     */
    public static function strcasecmp($str1, $str2)
    {
        return strcmp(self::strtoupper($str1), self::strtoupper($str2));
    }


    /**
     * Alias of StringHelper::strcasecmp() method
     * 
     * Compares UTF-8-encoded strings in a binary safe case-insensitive manner.
     */
    public static function compare($str1, $str2)
    {
        return self::strcasecmp($str1, $str2);
    }


    /**
     *  This function is an extension to str_pad, it manipulates the referenced 
     *  string '$str' and stretches or reduces it to the specified length. It 
     *  returns the number of characters, that were added or stripped.
     *
     * @see  http://php.net/manual/es/function.str-pad.php
     */
    public static function str_const_len($str, $len, $char = ' ', $str_pad_const = STR_PAD_RIGHT)
    {
        $origLen = strlen($str);
        $str2 = $str;
        
        // stretch string
        if (strlen($str) < $len)
        { 
            $str2 = str_pad($str, $len, $char, $str_pad_const);
        }
        
        // Reduce string
        else
        {
            switch ($str_pad_const)
            {
                case STR_PAD_LEFT:
                    $str2 = substr($str2, (strlen($str2) - $len), $len);
                break;

                case STR_PAD_BOTH:
                    $shorten = (int) ((strlen($str) - $len) / 2);
                    $str2 = substr($str2, $shorten, $len);
                break;

                default:
                    $str2 = substr($str2, 0, $len);
                break;
            }
        }
        return $str2;
    }


    /**
     * Remove white spaces
     * 
     * First version (used here) extracted from BASIPIM when exporting data to XML files
     * 
     * Second version in https://pageconfig.com/post/remove-undesired-characters-with-trim_all-php
     */
    public static function trim_all($text)
    {
        return preg_replace('~\s*(<([^>]*)>[^<]*</\2>|<[^>]*>)\s*~', '$1', $text);
    }


    /**
     * Special trim with UTF-8 characters
     * 
     * @see https://stackoverflow.com/questions/12837682/non-breaking-utf-8-0xc2a0-space-and-preg-replace-strange-behaviour
     */
    public static function trim($text)
    {
        $text = str_replace("\xc2\xa0", " ", $text);
        return trim($text);
    }


    /**
     * Clean text
     * 
     * WARNING: It removes characteres like ¿ or accents(á,é,í,...)
     * 
     * @see https://alvinalexander.com/php/how-to-remove-non-printable-characters-in-string-regex
     */
    public static function clean_text($text)
    {
        $text = self::remove_invisible_characters($text);

        // Remove all CONTROL characters
        if ( ! Yii::app() instanceof \CConsoleApplication )
        {
            // return preg_replace('/[[:cntrl:]]/', '', $text);
            return preg_replace('/[[:^print:]]/', '', $text);
        }

        // return preg_replace('/[[:cntrl:]]/u', '', $text);
        return preg_replace('/[[:^print:]]/u', '', $text);
    }


    /**
     * Remove non printable characters from a string
     * 
     * @see https://github.com/bcit-ci/CodeIgniter/blob/b862664f2ce2d20382b9af5bfa4dd036f5755409/system/core/Common.php
     */
    public static function remove_invisible_characters($str, $is_url_encoded = true)
    {
        $non_displayables = [];

        // every control character except newline (dec 10),
        // carriage return (dec 13) and horizontal tab (dec 09)
        if ($is_url_encoded)
        {
            $non_displayables[] = '/%0[0-8bcef]/i'; // url encoded 00-08, 11, 12, 14, 15
            $non_displayables[] = '/%1[0-9a-f]/i';  // url encoded 16-31
            $non_displayables[] = '/%7f/i'; // url encoded 127
        }
        
        $non_displayables[] = '/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+/S';   // 00-08, 11, 12, 14-31, 127
        
        do
        {
            $str = preg_replace($non_displayables, '', $str, -1, $count);
        }
        while ($count);
        
        return $str;
    }


    /**
     * Remove CONTROL characters
     */
    public static function remove_control_characters($str)
    {
        if ( Yii::app() instanceof \CConsoleApplication )
        {
            return preg_replace('/[[:cntrl:]]/u', '', $str);
        }

        return preg_replace('/[[:cntrl:]]/', '', $str);
    }


    /**
     * Counts the number of characters in an UTF-8 string.
     */
    public static function strlen($text, $is_use_mbstring = true, $is_html = false)
    {
        if ( $is_html )
        {
            $text = self::clean_html($text);
        }

        if ( $is_use_mbstring )
        {
            return mb_strlen($text);
        }

        // Do not count UTF-8 continuation bytes.
        return strlen(preg_replace("", '', $text));
    }



    /**
     * Get part of an UTF-8 string.
     */
    public static function substr($text, $start, $length = null, $is_use_mbstring = true, $is_html = false)
    {
        if ( $is_html )
        {
            $text = self::clean_html($text);
        }

        if ( $is_use_mbstring )
        {
            return mb_substr($text, $start, $length);
        }

        // Do not count UTF-8 continuation bytes.
        return substr(preg_replace("", '', $text), $start, $length);
    }


    /**
     * Parse a URL query string encoded to an array
     * 
     * @see https://www.php.net/manual/es/function.parse-str.php
     */
    public static function parse_str($text, $is_use_mb_parse_str = true)
    {
        $vec_params = [];
        if ( $is_use_mb_parse_str )
        {
            mb_parse_str($text, $vec_params);
        }
        else
        {
            parse_str($text, $vec_params);
        }

        return $vec_params;
    }


    /**
     * Checks whether a string is valid UTF-8.
     *
     * All functions designed to filter input should use drupal_validate_utf8 to ensure they operate on valid UTF-8 strings to prevent bypass of the filter.
     * 
     * When text containing an invalid UTF-8 lead byte (0xC0 - 0xFF) is presented as UTF-8 to Internet Explorer 6, the program may misinterpret subsequent bytes. When these subsequent bytes are HTML control characters such as quotes or angle brackets, parts of the text that were deemed safe by filters end up in locations that are potentially unsafe; An onerror attribute that is outside of a tag, and thus deemed safe by a filter, can be interpreted by the browser as if it were inside the tag.
     * 
     * The function does not return false for strings containing character codes above U+10FFFF, even though these are prohibited by RFC 3629.
     *
     * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Component%21Utility%21Unicode.php/function/Unicode%3A%3AvalidateUtf8/8.2.x
     */
    public static function validate_utf8($text)
    {
        if ( strlen($text) == 0)
        {
            return true;
        }

        // With the PCRE_UTF8 modifier 'u', preg_match() fails silently on strings
        // containing invalid UTF-8 byte sequences. It does not reject character
        // codes above U+10FFFF (represented by 4 or more octets), though.
        return preg_match('/^./us', $text) == 1;
    }


    /**
     * Flip U+C0-U+DE to U+E0-U+FD and back. Can be used as preg_replace callback.
     * 
     * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Component%21Utility%21Unicode.php/function/Unicode%3A%3AcaseFlip/8.2.x
     */
    public static function unicode_caseflip($matches)
    {
        return $matches[0][0] . chr(ord($matches[0][1]) ^ 32);
    }


    /**
     * Converts data to UTF-8.
     * 
     * Requires the iconv, GNU recode or mbstring PHP extension.
     * 
     * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Component%21Utility%21Unicode.php/function/Unicode%3A%3AconvertToUtf8/8.2.x
     */
    public static function convert_to_utf8($data, $encoding)
    {
        if ( function_exists('iconv') )
        {
            return @iconv($encoding, 'utf-8', $data);
        }

        if ( function_exists('mb_convert_encoding') )
        {
            return @mb_convert_encoding($data, 'utf-8', $encoding);
        }

        if ( function_exists('recode_string') )
        {
            return @recode_string($encoding . '..utf-8', $data);
        }

        // Cannot convert.
        return false;
    }


    /**
     * Gets the encoding of the given string.
     *
     * @param string $str The string to process.
     * @return string The encoding of the string.
     */
    public static function encoding($text)
    {
        return mb_strtolower(mb_detect_encoding($text, mb_detect_order(), true));
    }


    /**
     * Clean all HTML tags of a string
     */
    public static function clean_html($text, $vec_allowed_tags = [])
    {
        $CHtmlPurifier = new CHtmlPurifier();
        $CHtmlPurifier->options = ['HTML.AllowedElements' => $vec_allowed_tags];
        return $CHtmlPurifier->purify($text);
    }


    /**
     * Generates a random string of latin alphanumeric characters that defaults to a $length of 36.
     * If $is_extended_chars set to true, additional symbols can be included in the string.
     * 
     * @see https://github.com/craftcms/cms/blob/9a7b018de6e003c3d3d129dc0391671b74d71635/src/helpers/StringHelper.php
     */
    public static function random_string($length = 36, $is_extended_chars = false)
    {
        $valid_chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        if ($is_extended_chars)
        {
            $valid_chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890`~!@#$%^&*()-_=+[]\{}|;:\'",./<>?"';
        }

        return self::random_string_with_chars($valid_chars, $length);
    }


    /**
     * Generates a random string of character
     * 
     * @see https://github.com/craftcms/cms/blob/9a7b018de6e003c3d3d129dc0391671b74d71635/src/helpers/StringHelper.php
     */
    public static function random_string_with_chars($valid_chars, $length)
    {
        $output = '';

        // Count the number of chars in the valid chars string so we know how many choices we have
        $num_valid_chars = self::strlen($valid_chars);

        // PHP5.x Polyfill for random_int() function. Available from PHP 7.0 version
        require_once Yii::getAlias("@lib.random_compat.lib") . DIRECTORY_SEPARATOR . "random.php";

        // Repeat the steps until we've created a string of the right length
        for ($i = 0; $i < $length; $i++)
        {
            // Pick a random number from 1 up to the number of valid chars
            $random_pick = random_int(1, $num_valid_chars);
            
            // Take the random character out of the string of valid chars
            $random_char = $valid_chars[$random_pick - 1];
            
            // add the randomly-chosen char onto the end of our string
            $output .= $random_char;
        }

        return $output;
    }


    /**
     * Generate a timestamp (YmdHi) with a random string prefix/suffix
     * 
     * Example: 201909181556_f4R2EY96sD
     */
    public static function random_timestamp($position = 'suffix', $length = 10, $separator = '_')
    {
        if ( $position === 'suffix' )
        {
            return date('YmdHi') . $separator . strtolower(self::random_string($length));
        }

        return strtolower(self::random_string($length)) . $separator . date('YmdHi');
    }


    /**
     * Encrypts a string value
     */
    public static function encrypt($string = '', $hash_method = '')
    {
        // If user module is enabled, get default value from it
        if ( empty($hash_method) && ! App::is_console() )
        {
            $hash_method = Yii::app()->user->hash_method;
        }

        // Unless, MD5 will be the default method
        if ( empty($hash_method) )
        {
            $hash_method = 'md5';
        }
        

        switch ( $hash_method )
        {
            case 'md5':
                return \md5($string);
                break;
            
            case 'sha1':
                return \sha1($string);
                break;
            
            default:
                return \hash($hash_method, $string);
                break;
        }
    }


    /**
     * Check if a strnig is a valid JSON
     */
    public static function is_json($string)
    {
        $result = json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }



    /*
    |--------------------------------------------------------------------------
    | Methods from Yii2 BaseStringHelper class
    |--------------------------------------------------------------------------
    */

    /**
     * Returns the number of bytes in the given string.
     * This method ensures the string is treated as a byte array by using `mb_strlen()`.
     * 
     * @param string $string the string being measured for length
     * @return int the number of bytes in the given string.
     */
    public static function byteLength($string)
    {
        return mb_strlen($string, '8bit');
    }

    /**
     * Returns the portion of string specified by the start and length parameters.
     * This method ensures the string is treated as a byte array by using `mb_substr()`.
     * 
     * @param string $string the input string. Must be one character or longer.
     * @param int $start the starting position
     * @param int $length the desired portion length. If not specified or `null`, there will be
     * no limit on length i.e. the output will be until the end of the string.
     * @return string the extracted part of string, or false on failure or an empty string.
     * 
     * @see http://www.php.net/manual/en/function.substr.php
     */
    public static function byteSubstr($string, $start, $length = null)
    {
        return mb_substr($string, $start, $length === null ? mb_strlen($string, '8bit') : $length, '8bit');
    }

    /**
     * Returns the trailing name component of a path.
     * This method is similar to the php function `basename()` except that it will
     * treat both \ and / as directory separators, independent of the operating system.
     * This method was mainly created to work on php namespaces. When working with real
     * file paths, php's `basename()` should work fine for you.
     * Note: this method is not aware of the actual filesystem, or path components such as "..".
     *
     * @param string $path A path string.
     * @param string $suffix If the name component ends in suffix this will also be cut off.
     * @return string the trailing name component of the given path.
     * @see http://www.php.net/manual/en/function.basename.php
     */
    public static function basename($path, $suffix = '')
    {
        if (($len = mb_strlen($suffix)) > 0 && mb_substr($path, -$len) === $suffix) {
            $path = mb_substr($path, 0, -$len);
        }
        $path = rtrim(str_replace('\\', '/', $path), '/\\');
        if (($pos = mb_strrpos($path, '/')) !== false) {
            return mb_substr($path, $pos + 1);
        }

        return $path;
    }

    /**
     * Returns parent directory's path.
     * This method is similar to `dirname()` except that it will treat
     * both \ and / as directory separators, independent of the operating system.
     *
     * @param string $path A path string.
     * @return string the parent directory's path.
     * @see http://www.php.net/manual/en/function.basename.php
     */
    public static function dirname($path)
    {
        $pos = mb_strrpos(str_replace('\\', '/', $path), '/');
        if ( $pos !== false )
        {
            return mb_substr($path, 0, $pos);
        }

        return '';
    }


    /**
     * Check if given string starts with specified substring.
     * Binary and multibyte safe.
     *
     * @param string $string Input string
     * @param string $with Part to search inside the $string
     * @param bool $caseSensitive Case sensitive search. Default is true. When case sensitive is enabled, $with must exactly match the starting of the string in order to get a true value.
     * @return bool Returns true if first input starts with second input, false otherwise
     */
    public static function startsWith($string, $with, $caseSensitive = true)
    {
        if (!$bytes = static::byteLength($with)) {
            return true;
        }
        if ($caseSensitive) {
            return strncmp($string, $with, $bytes) === 0;

        }
        $encoding = Yii::$app ? Yii::$app->charset : 'UTF-8';
        return mb_strtolower(mb_substr($string, 0, $bytes, '8bit'), $encoding) === mb_strtolower($with, $encoding);
    }

    /**
     * Check if given string ends with specified substring.
     * Binary and multibyte safe.
     *
     * @param string $string Input string to check
     * @param string $with Part to search inside of the $string.
     * @param bool $caseSensitive Case sensitive search. Default is true. When case sensitive is enabled, $with must exactly match the ending of the string in order to get a true value.
     * @return bool Returns true if first input ends with second input, false otherwise
     */
    public static function endsWith($string, $with, $caseSensitive = true)
    {
        if (!$bytes = static::byteLength($with)) {
            return true;
        }
        if ($caseSensitive) {
            // Warning check, see http://php.net/manual/en/function.substr-compare.php#refsect1-function.substr-compare-returnvalues
            if (static::byteLength($string) < $bytes) {
                return false;
            }

            return substr_compare($string, $with, -$bytes, $bytes) === 0;
        }

        $encoding = Yii::$app ? Yii::$app->charset : 'UTF-8';
        return mb_strtolower(mb_substr($string, -$bytes, mb_strlen($string, '8bit'), '8bit'), $encoding) === mb_strtolower($with, $encoding);
    }

    /**
     * Explodes string into array, optionally trims values and skips empty ones.
     *
     * @param string $string String to be exploded.
     * @param string $delimiter Delimiter. Default is ','.
     * @param mixed $trim Whether to trim each element. Can be:
     *   - boolean - to trim normally;
     *   - string - custom characters to trim. Will be passed as a second argument to `trim()` function.
     *   - callable - will be called for each value instead of trim. Takes the only argument - value.
     * @param bool $skipEmpty Whether to skip empty strings between delimiters. Default is false.
     * @return array
     * @since 2.0.4
     */
    public static function explode($string, $delimiter = ',', $trim = true, $skipEmpty = false)
    {
        $result = explode($delimiter, $string);
        if ($trim) {
            if ($trim === true) {
                $trim = 'trim';
            } elseif (!is_callable($trim)) {
                $trim = function ($v) use ($trim) {
                    return trim($v, $trim);
                };
            }
            $result = array_map($trim, $result);
        }
        if ($skipEmpty) {
            // Wrapped with array_values to make array keys sequential after empty values removing
            $result = array_values(array_filter($result, function ($value) {
                return $value !== '';
            }));
        }

        return $result;
    }

    /**
     * Counts words in a string.
     * @since 2.0.8
     *
     * @param string $string
     * @return int
     */
    public static function countWords($string)
    {
        return count(preg_split('/\s+/u', $string, null, PREG_SPLIT_NO_EMPTY));
    }

    /**
     * Returns string representation of number value with replaced commas to dots, if decimal point
     * of current locale is comma.
     * @param int|float|string $value
     * @return string
     * @since 2.0.11
     */
    public static function normalizeNumber($value)
    {
        $value = (string)$value;

        $localeInfo = localeconv();
        $decimalSeparator = isset($localeInfo['decimal_point']) ? $localeInfo['decimal_point'] : null;

        if ($decimalSeparator !== null && $decimalSeparator !== '.') {
            $value = str_replace($decimalSeparator, '.', $value);
        }

        return $value;
    }

    /**
     * Encodes string into "Base 64 Encoding with URL and Filename Safe Alphabet" (RFC 4648).
     *
     * > Note: Base 64 padding `=` may be at the end of the returned string.
     * > `=` is not transparent to URL encoding.
     *
     * @see https://tools.ietf.org/html/rfc4648#page-7
     * @param string $input the string to encode.
     * @return string encoded string.
     * @since 2.0.12
     */
    public static function base64UrlEncode($input)
    {
        return strtr(base64_encode($input), '+/', '-_');
    }

    /**
     * Decodes "Base 64 Encoding with URL and Filename Safe Alphabet" (RFC 4648).
     *
     * @see https://tools.ietf.org/html/rfc4648#page-7
     * @param string $input encoded string.
     * @return string decoded string.
     * @since 2.0.12
     */
    public static function base64UrlDecode($input)
    {
        return base64_decode(strtr($input, '-_', '+/'));
    }

    /**
     * Safely casts a float to string independent of the current locale.
     *
     * The decimal separator will always be `.`.
     * @param float|int $number a floating point number or integer.
     * @return string the string representation of the number.
     * @since 2.0.13
     */
    public static function floatToString($number)
    {
        // . and , are the only decimal separators known in ICU data,
        // so its safe to call str_replace here
        return str_replace(',', '.', (string) $number);
    }

    /**
     * Checks if the passed string would match the given shell wildcard pattern.
     * This function emulates [[fnmatch()]], which may be unavailable at certain environment, using PCRE.
     * @param string $pattern the shell wildcard pattern.
     * @param string $string the tested string.
     * @param array $options options for matching. Valid options are:
     *
     * - caseSensitive: bool, whether pattern should be case sensitive. Defaults to `true`.
     * - escape: bool, whether backslash escaping is enabled. Defaults to `true`.
     * - filePath: bool, whether slashes in string only matches slashes in the given pattern. Defaults to `false`.
     *
     * @return bool whether the string matches pattern or not.
     * @since 2.0.14
     */
    public static function matchWildcard($pattern, $string, $options = [])
    {
        if ($pattern === '*' && empty($options['filePath'])) {
            return true;
        }

        $replacements = [
            '\\\\\\\\' => '\\\\',
            '\\\\\\*' => '[*]',
            '\\\\\\?' => '[?]',
            '\*' => '.*',
            '\?' => '.',
            '\[\!' => '[^',
            '\[' => '[',
            '\]' => ']',
            '\-' => '-',
        ];

        if (isset($options['escape']) && !$options['escape']) {
            unset($replacements['\\\\\\\\']);
            unset($replacements['\\\\\\*']);
            unset($replacements['\\\\\\?']);
        }

        if (!empty($options['filePath'])) {
            $replacements['\*'] = '[^/\\\\]*';
            $replacements['\?'] = '[^/\\\\]';
        }

        $pattern = strtr(preg_quote($pattern, '#'), $replacements);
        $pattern = '#^' . $pattern . '$#us';

        if (isset($options['caseSensitive']) && !$options['caseSensitive']) {
            $pattern .= 'i';
        }

        return preg_match($pattern, $string) === 1;
    }


    /**
     * Splits a string into chunks on a given delimiter.
     *
     * @param string $string The string
     * @param string $delimiter The delimiter to split the string on (defaults to a comma)
     * @return string[] The segments of the string
     * 
     * @see craft\helpers\StringHelper
     */
    public static function split($string, $delimiter = ',')
    {
        return preg_split('/\s*' . preg_quote($delimiter, '/') . '\s*/', $string, -1, PREG_SPLIT_NO_EMPTY);
    }



    /**
     * Returns is the given string matches a v4 UUID pattern.
     *
     * Version 4 UUIDs have the form xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx where x
     * is any hexadecimal digit and y is one of 8, 9, A, or B.
     *
     * @param string $uuid The string to check.
     * @return bool Whether the string matches a v4 UUID pattern.
     */
    public static function isUUID($uuid)
    {
        return !empty($uuid) && preg_match('/^' . self::UUID_PATTERN . '$/', $uuid);
    }


    /**
     * Generates a valid v4 UUID string. See [http://stackoverflow.com/a/2040279/684]
     *
     * @return string The UUID
     */
    public static function UUID()
    {
        // PHP 7 or greater
        if ( PHP_MAJOR_VERSION >= 7 )
        {
            return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                random_int(0, 0xffff), random_int(0, 0xffff),
                // 16 bits for "time_mid"
                random_int(0, 0xffff),
                // 16 bits for "time_hi_and_version", four most significant bits holds version number 4
                random_int(0, 0x0fff) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res", 8 bits for "clk_seq_low", two most significant bits holds zero and
                // one for variant DCE1.1
                random_int(0, 0x3fff) | 0x8000,
                // 48 bits for "node"
                random_int(0, 0xffff), random_int(0, 0xffff), random_int(0, 0xffff)
            );
        }

        // PHP 5.6 or lower
        else
        {
             return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

                // 16 bits for "time_mid"
                mt_rand( 0, 0xffff ),

                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand( 0, 0x0fff ) | 0x4000,

                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand( 0, 0x3fff ) | 0x8000,

                // 48 bits for "node"
                mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
            );
        }
    }


    /**
     * Validate an email
     */
    public static function validate_email($email)
    {
        if ( !empty($email) )
        {
            $validator = new \CEmailValidator;
            return $validator->validateValue($email);
        }

        return false;
    }


    /**
     * Turn all URLs in clickable links.
     * 
     * @param string $value
     * @param array  $protocols  http/https, ftp, mail, twitter
     * @param array  $attributes
     * @return string
     * 
     * @see https://gist.github.com/jasny/2000705
     */
    public static function linkify($value, $protocols = ['http', 'mail'], array $attributes = [])
    {
        // Link attributes
        $attr = '';
        foreach ( $attributes as $key => $val )
        {
            $attr .= ' ' . $key . '="' . htmlentities($val) . '"';
        }
        
        $links = array();
        
        // Extract existing links and tags
        $value = preg_replace_callback('~(<a .*?>.*?</a>|<.*?>)~i', function ($match) use (&$links) { return '<' . array_push($links, $match[1]) . '>'; }, $value);
        
        // Extract text links for each protocol
        foreach ( (array)$protocols as $protocol )
        {
            switch ($protocol)
            {
                case 'http':
                case 'https':   $value = preg_replace_callback('~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) { if ($match[1]) $protocol = $match[1]; $link = $match[2] ?: $match[3]; return '<' . array_push($links, "<a $attr href=\"$protocol://$link\">$link</a>") . '>'; }, $value); break;
                case 'mail':    $value = preg_replace_callback('~([^\s<]+?@[^\s<]+?\.[^\s<]+)(?<![\.,:])~', function ($match) use (&$links, $attr) { return '<' . array_push($links, "<a $attr href=\"mailto:{$match[1]}\">{$match[1]}</a>") . '>'; }, $value); break;
                case 'twitter': $value = preg_replace_callback('~(?<!\w)[@#](\w++)~', function ($match) use (&$links, $attr) { return '<' . array_push($links, "<a $attr href=\"https://twitter.com/" . ($match[0][0] == '@' ? '' : 'search/%23') . $match[1]  . "\">{$match[0]}</a>") . '>'; }, $value); break;
                default:        $value = preg_replace_callback('~' . preg_quote($protocol, '~') . '://([^\s<]+?)(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) { return '<' . array_push($links, "<a $attr href=\"$protocol://{$match[1]}\">{$match[1]}</a>") . '>'; }, $value); break;
            }
        }
        
        // Insert all link
        return preg_replace_callback('/<(\d+)>/', function ($match) use (&$links) { return $links[$match[1] - 1]; }, $value);
    }
}
