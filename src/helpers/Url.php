<?php
/**
 * Url class file
 * 
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2019 Dezero
 */

namespace dz\helpers;

use Yii;
use Dz;

class Url
{  
    /**
     * Cached data
     */
    static public $vec_cache = [];


    /**
     * Creates an absolute URL based on the given controller and action information.
     * 
     * ```php
     * Url::to('product/product/update', ['id' => 5])
     * ```
     * 
     * @see CApplication::createAbsoluteUrl
     * 
     * @param string $route the URL route. This should be in the format of 'ControllerID/ActionID'.
     * @param array $params additional GET parameters (name=>value). Both the name and value will be URL-encoded.
     * @param string $schema schema to use (e.g. http, https). If empty, the schema used for the current request will be used.
     * @param string $ampersand the token separating name-value pairs in the URL.
     * @return string the constructed URL
     */
    static public function to($route, $params=[], $schema='', $ampersand='&')
    {
        // It's an alias if $route starts with "@"
        if ( preg_match("/^\@/", $route) )
        {
            $route = str_replace(Dz::basePath(), '', Yii::getAlias($route));
        }

        $url = Yii::app()->urlManager->createUrl($route, $params, $ampersand);

        if ( strpos($url,'http')===0 || strpos($url,'//')===0 )
        {
            return $url;
        }

        return Yii::app()->request->getHostInfo($schema) . $url;
    }


    /**
     * Get current slug URL from a source url
     */
    static public function slug($route, $params=[], $schema='', $ampersand='&')
    {
        return Yii::app()->seo->slug_url($route, $params, $schema, $ampersand);
    }


    /**
     * Creates a URL by using the current route WITHOUT the GET parameters.
     */
    static public function canonical()
    {
        return self::to(self::route());
    }


    /**
     * Creates a URL by using the current route and the GET parameters.
     * 
     * This method allow to modify or add params to current URL
     */
    static public function current($params = [], $schema='', $ampersand='&')
    {
        // Add current query params
        $currentParams = Yii::app()->request->getQueryParams();
        if ( !empty($currentParams) )
        {
            $params = array_replace_recursive($currentParams, $params);
        }

        // Get route
        $route = self::route();

        return self::to($route, $params, $schema, $ampersand);
    }


    /**
     * Get current route: <module>/<controller>/<action>
     */
    static public function route()
    {
        // Cached route?
        if ( ! isset(self::$vec_cache['current_route']) )
        {
            // Get <crontroller>/<action>
            $action_name = Yii::app()->controller->currentActionName();
            $route = Yii::app()->controller->getId();

            // Exclude default action
            if ( $action_name !== Yii::app()->controller->defaultAction )
            {
                $route .= '/'. $action_name;
            }
            
            // Get <module>/<crontroller>/<action>
            $module = Yii::app()->controller->currentModuleName();
            if ( !empty($module) )
            {
                $route = $module .'/'. $route;
            }

            // Save route to cache
            self::$vec_cache['current_route'] = $route;
        }

        return self::$vec_cache['current_route'];
    }


    /**
     * Get current URL using HttpRequest class
     * 
     * Examples:
     *  - $is_full_url = true  --> http://mysite.local/en/my-product
     *  - $is_full_url = false --> /en/my-product
     */
    static public function current_request($is_full_url = true)
    {
        return $is_full_url ? Yii::app()->request->hostInfo . Yii::app()->request->url : Yii::app()->request->url;
    }


    /**
     * Get current slug URL for a language
     */
    static public function current_slug($language_id = '', $is_included_params = false)
    {
        // Cached URL?
        if ( ! isset(self::$vec_cache['current_slug_'. $language_id]) )
        {
            self::$vec_cache['current_slug_'. $language_id] = Yii::app()->seo->current_url($language_id);
        }

        // Include query params
        if ( $is_included_params )
        {
            return Yii::app()->seo->current_url($language_id,  Yii::app()->request->getQueryParams());
        }

        return self::$vec_cache['current_slug_'. $language_id];
    }


    /**
     * Return path info. Does not include language prefix
     * 
     * Example: "my-product" or "product/2"
     * 
     * @see HttpRequest::pathInfo()
     */
    static public function path_info($is_full_url = false)
    {
        $path_info = Yii::app()->request->getPathInfo();
        return $is_full_url ? Url::to($path_info) : $path_info;
    }


    /**
     * Returns the relative URL for the application.
     * This is a shortcut method to {@link CHttpRequest::getBaseUrl()}.
     * 
     * @see CApplication::getBaseUrl
     * 
     * @param boolean $absolute whether to return an absolute URL. Defaults to false, meaning returning a relative one.
     * @return string the relative URL for the application
     * @see CHttpRequest::getBaseUrl()
     */
    static public function base($absolute = false)
    {
        if ( isset(Yii::app()->params['baseUrl']) )
        {
            $base_url = Yii::app()->params['baseUrl'];
        }
        else
        {
            $base_url = Yii::app()->request->getBaseUrl($absolute);
        }

        // Add language prefix?
        if ( Yii::isMultilanguage() && Yii::currentLanguage() !== Yii::defaultLanguage() )
        {
            $base_url .= '/'. Yii::currentLanguage();
        }

        return $base_url;
    }


    /**
     * Returns the current theme URL
     */
    static public function theme()
    {
        return Dz::is_console() ? Url::base() : Yii::app()->theme->getBaseUrl();
    }


    /**
     * Return the home page URL
     */
    static public function home()
    {
        // Cached URL?
        if ( ! isset(self::$vec_cache['home']) )
        {
            // Home defined on "routes.php" config file
            $vec_rules = Yii::app()->urlManager->rules;
            if ( !empty($vec_rules) && isset($vec_rules['']) )
            {
                self::$vec_cache['home'] = Url::to($vec_rules['']);
            }

            // Base URL
            else
            {
                self::$vec_cache['home'] = self::base();   
            }
        }

        return self::$vec_cache['home'];
    }
}
