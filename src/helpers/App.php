<?php
/**
 * App information helper class file
 *
 * Helper class for a DEZERO project
 * 
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2021 Fabián Ruiz
 */

namespace dz\helpers;

use dz\db\DbCriteria;
use dz\helpers\Json;
use dz\helpers\StringHelper;
use dz\modules\settings\models\Currency;
use mikehaertl\shellcommand\Command as ShellCommand;
use Yii;

class App
{
    /**
     * Check if app is running from the console
     */
    public static function is_console()
    {
        // return get_class(Yii::app()) == 'CConsoleApplication';
        return Yii::app() instanceof \CConsoleApplication;
    }


    /**
     * Returns the PHP version, without the distribution info.
     *
     * @return string
     */
    public static function phpVersion()
    {
        return PHP_MAJOR_VERSION . '.' . PHP_MINOR_VERSION . '.' . PHP_RELEASE_VERSION;
    }


    /**
     * Returns the Apache version
     *
     * @return string
     */
    public static function apacheVersion()
    {
        if ( isset($_SERVER['SERVER_SOFTWARE']) )
        {
            return $_SERVER['SERVER_SOFTWARE'];
        }

        return apache_get_version();
    }


    /**
     * Returns the Yii version
     *
     * @return string
     */
    public static function yiiVersion()
    {
        return Yii::getVersion();
    }


    /**
     * Returns the DB driver name and version number
     *
     * @return string
     */
    public static function dbDriver()
    {
        return 'MySQL '. Yii::app()->db->getServerVersion();
    }


    /**
     * Returns the image driver name and version
     *
     * @return string
     */
    public static function imageDriver()
    {
        if ( ! function_exists('gd_info') )
        {
            return 'GD not installed';
        }

        $image_driver = Yii::app()->image->driver;
        if ( $image_driver == 'GD' )
        {
            return 'GD - '. phpversion('gd');
        }

        return '';
    }


    /**
     * Get app version from composer.json file
     */
    public static function  appVersion()
    {
        $vec_composer_json = self::composerInfo();
        if ( !empty($vec_composer_json) AND isset($vec_composer_json['version']) )
        {
            return $vec_composer_json['version'];   
        }

        return '-';
    }


    /**
     * Get Dz Core Framework version from composer.json file
     */
    public static function dzVersion()
    {
        $composer_path = Yii::getAlias('@core') . DIRECTORY_SEPARATOR .'composer.json';
        $vec_composer_json = self::composerInfo($composer_path);
        if ( !empty($vec_composer_json) AND isset($vec_composer_json['version']) )
        {
            return $vec_composer_json['version'];   
        }

        return '-';
    }


    /**
     * Get information on composer.json file
     */
    public static function composerInfo($composer_path = '')
    {
        if ( empty($composer_path) )
        {
            $composer_path = DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'composer.json';
        }
        $composer_file = Yii::app()->file->set($composer_path);
        if ( $composer_file && $composer_file->getExists() )
        {
            $content_composer = $composer_file->getContents();
            if ( !empty($content_composer) )
            {
                $content_composer = str_replace("\t", "&nbsp;", $content_composer);
                $content_composer = StringHelper::clean_text($content_composer);
                $content_composer = StringHelper::remove_invisible_characters($content_composer);
                return Json::decode($content_composer);
            }
        }

        return [];
    }


    /**
     * Get last git commit of Dz Core Framework
     */
    public static function dzLastCommit()
    {
        return self::gitLastCommit(Yii::app()->path->corePath());
    }


    /**
     * Get last git commit of current application
     */
    public static function appLastCommit()
    {
        return self::gitLastCommit(Yii::app()->path->basePath());
    }


    /**
     * Try to get information about last GIT commit pulled into the app
     */
    public static function gitLastCommit($git_path = '')
    {
        $output = '';

        // Default git last path is base path
        if ( empty($git_path) )
        {
            $git_path = Yii::app()->path->basePath();
        }

        // First try with GIT command
        $git_command = 'git';
        if ( isset(Yii::app()->params['git_command']) )
        {
            $git_command = Yii::app()->params['git_command'];
        }

        $shellCommand = new ShellCommand();
        $shellCommand->setCommand('cd '. $git_path .' && '. $git_command .' show -s');

        // If we don't have proc_open, maybe we've got exec
        if ( !function_exists('proc_open') && function_exists('exec') )
        {
            $shellCommand->useExec = true;
        }

        // Execute command
        $is_success = $shellCommand->execute();

        if ( $is_success )
        {
            $output = $shellCommand->getOutput();
        }

        // GIT STATUS fail, try to get via file .git directory
        else
        {
            // return 'ERROR: '. $shellCommand->getError();

            $git_file_path = $git_path . DIRECTORY_SEPARATOR .'.git';
            $git_directory = Yii::app()->file->set($git_file_path);
            if ( $git_directory && $git_directory->getExists() )
            {
                // Read .git/FECTH_HEAD file
                $git_head_file = Yii::app()->file->set($git_file_path . DIRECTORY_SEPARATOR . 'FETCH_HEAD');
                if ( $git_head_file && $git_head_file->getExists() )
                {
                    $output = 'commit '. $git_head_file->getContents();
                }

                // Read .git/COMMIT_EDITMSG file
                $git_commit_file = Yii::app()->file->set($git_file_path . DIRECTORY_SEPARATOR . 'COMMIT_EDITMSG');
                if ( $git_commit_file && $git_commit_file->getExists() )
                {
                    $output .= "\n" . $git_commit_file->getContents();
                }
            }
            
        }

        return $output;
    }


    /**
     * Generate PhpMyAdminUrl
     */
    public static function phpMyAdmin($database_name = '')
    {
        $url = getenv('PHPMYADMIN_URL');
        if ( empty($url) )
        {
            $url = 'http://localhost/phpMyAdmin/';
        }
        
        // Add database name as URL parameter
        if ( empty($database_name) )
        {
            $database_name = \getenv('DB_DATABASE');
        }
        $url .= 'index.php?server=1&db='. $database_name .'&target=script';

        return $url;
    }


    /**
     * Get currencies
     */
    public static function get_currencies($search_type, $return_type = '')
    {
        $criteria = new DbCriteria;
        switch ( $search_type )
        {
            case 'enabled':
                $criteria->addCondition('t.disable_date IS NULL OR t.disable_date = "0"');
            break;
        }

        $criteria->order = 'weight ASC';
        $vec_currency_models = Currency::model()->findAll($criteria);
        
        // Return empty array
        if ( empty($vec_currency_models) )
        {
            return [];
        }

        // Return type: list or models
        switch ( $return_type )
        {
            case 'list':
                $vec_currency_list = [];
                foreach ( $vec_currency_models as $currency_model)
                {
                    $vec_currency_list[$currency_model->currency_id] = $currency_model->title();
                }
                return $vec_currency_list;
            break;

            default:
                return $vec_currency_models;
            break;
        }
    }
}