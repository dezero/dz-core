<?php
/**
 * EntityList component class file
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2021 Dezero
 */

namespace dz\entities;

use dz\base\ApplicationComponent;
use dz\db\DbCriteria;
use dz\helpers\Url;
use Yii;

/**
 * EntityList component to build custom list with Entities
 */
abstract class EntityList extends ApplicationComponent
{
    /**
     * @var dz\db\DbCriteria
     */
    protected $criteria;


    /**
     * @var dz\db\DbCriteria
     */
    protected $criteria_count;


    /**
     * @var array List filters
     */
    protected $vec_filters = [];


    /**
     * @var int Current page
     */
    protected $current_page = 1;


    /**
     * @var int Page size (total items per page)
     */
    protected $page_size = 10;


    /**
     * @var string Attribute used for the "ORDER BY" SQL
     */
    protected $order_by;


    /**
     * @var int Total items
     */
    protected $total_items = 0;


    /**
     * Init function
     */
    public function init()
    {
        $this->criteria = new DbCriteria;
        $this->criteria_count = new DbCriteria;

        parent::init();
    }


    /**
     * Return current page
     */
    public function get_current_page()
    {
        return $this->current_page;
    }


    /**
     * Set current page
     */
    public function set_current_page($page)
    {
        $this->current_page = $page;
        return $this;
    }


    /**
     * Return page size (total items per page)
     */
    public function get_page_size()
    {
        return $this->page_size;
    }


    /**
     * Set page size (total items per page)
     */
    public function set_page_size($page_size)
    {
        $this->page_size = $page_size;
        return $this;
    }


    /**
     * Return order attribute or name
     */
    public function get_order()
    {
        return $this->order_by;
    }


    /**
     * Set list order attribute or name
     */
    public function set_order($order_by)
    {
        $this->order_by = $order_by;
        return $this;
    }


    /**
     * Add list filters
     */
    public function set_filters($vec_filters)
    {
        $this->vec_filters = $vec_filters;
        return $this;
    }


    /**
     * Get total results
     */
    public function get_total_items()
    {
        return $this->total_items;
    }


    /**
     * Get last page
     */
    public function get_last_page()
    {
        if ( $this->total_items === 0 || $this->page_size === 0 )
        {
            return 1;
        }

        return ceil($this->total_items / $this->page_size);
    }


    /**
     * Get pager URL
     */
    public function get_pager_url($list_url, $page = 1)
    {
        $vec_params = $this->vec_filters;
        
        // Order by
        if ( !empty($this->order_by) )
        {
            $vec_params['order'] = $this->order_by;
        }

        if ( $page < 1 )
        {
            $page = 1;
        }

        // Current page
        $vec_params['page'] = $page;

        return Url::to($list_url, $vec_params);
    }
}