<?php
/**
 * RestFilter class file
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2017 Fabián Ruiz
 */

namespace dz\filters;

use Yii;

/**
 * Filter that automatically checks the permissions for REST API
 */
class RestFilter extends \CFilter
{
	/**
	 * @var array name-value pairs that would be passed to business rules associated
	 * with the tasks and roles assigned to the user.
	 */
	public $params = [];


	/**
	 * Performs the pre-action filtering.
	 *
	 * @param CFilterChain $filterChain the filter chain that the filter is on.
	 * @return boolean whether the filtering process should continue and the action should be executed.
	 * @throws CHttpException if the user is denied access.
	 */
	protected function preFilter($filterChain)
	{
		$controller = $filterChain->controller;
		$vec_route = [
			'module' 		=> '',

			// Current controller
			'controller'	=> $controller->getId(),
			
			// Current action name
			'action'		=> $controller->action->getId(),
		];


		// Module
		if ( ($module = $controller->getModule()) !== null )
		{
			// Current module name
			$vec_route['module'] = $module->getId();

			// Public module has public access
			if ( $vec_route['module'] == 'api' )
			{
				return true;
			}
		}

		return false;
	}
}