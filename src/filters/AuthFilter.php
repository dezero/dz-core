<?php
/**
 * AuthFilter class file for Dz Framework
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2012 Dezero
 */

namespace dz\filters;

use Yii;

/**
 * Filter that automatically checks if the user has access to the current controller action.
 */
class AuthFilter extends \CFilter
{
    /**
     * @var array name-value pairs that would be passed to business rules associated
     * with the tasks and roles assigned to the user.
     */
    public $params = [];


    /**
     * Performs the pre-action filtering.
     *
     * @param CFilterChain $filterChain the filter chain that the filter is on.
     * @return boolean whether the filtering process should continue and the action should be executed.
     * @throws CHttpException if the user is denied access.
     */
    protected function preFilter($filterChain)
    {
        $controller = $filterChain->controller;
        $vec_route = [
            'module'        => '',

            // Current controller
            'controller'    => $controller->getId(),
            
            // Current action name
            'action'        => $controller->action->getId(),
        ];

        // Actions starting with "public" have public access
        if ( preg_match("/^public/", $vec_route['action']) )
        {
            return true;
        }

        // Module
        if ( ($module = $controller->getModule()) !== null )
        {
            // Current module name
            $vec_route['module'] = $module->getId();

            // Modules with public access? (Not logged in required)
            if ( Yii::app()->authManager->checkPublicAccess() )
            {
                return true;
            }
        }
        
        // $user CWebUser
        $user = Yii::app()->getUser();

        // Guest user must login
        if ($user->isGuest)
        {
            $user->loginRequired();
        }
        
        // Superadmin?
        if ( $user->isAdmin() ) 
        {
            return true;
        }
        
        
        /*
        |--------------------------------------------------------------------------
        | Check access using following rule: <MODULE>.<CONTROLLER>.<ACTION>
        |--------------------------------------------------------------------------
        */      
        $operation_name = '';
        
        // STEP #1 - MODULE access
        if ( !empty($vec_route['module']) )
        {
            $operation_name = $vec_route['module'] .".";
            
            // Access to full module? Rule: <MODULE>.*
            /*
            if ( $user->checkAccess($operation_name .'*', $this->params) )
            {       
                return true;
            }
            */
        }
        // STEP #2 - CONTROLLER access      
        // Current controller name
        $operation_name .= $vec_route['controller'] .".";

        // Access to full controller? Rule: <MODULE>.<CONTROLLER>.*
        if ( $user->checkAccess($operation_name .'*', $this->params) )
        {
            return true;
        }
        
        
        // STEP #3 - ACTION access
        
        // Get alias actions to check       
        $vec_alias_actions = $controller->checkAliasActions();
        if ( isset($vec_alias_actions[$vec_route['action']]) )
        {
            $operation_name = $vec_alias_actions[$vec_route['action']];
        }
        else
        {
            $operation_name .= $vec_route['action'];
        }
        
        // Registered user?
        if ( $operation_name === 'is_logged_in' )
        {
            return (Yii::app()->user->id > 0);
        }

        // Access to an action? Rule: <MODULE>.<CONTROLLER>.<ACTION>
        if ( $user->checkAccess($operation_name, $this->params) )
        {       
            return true;
        }

        throw new \CHttpException(401, Yii::t('app', 'Access denied'));
    }
}
