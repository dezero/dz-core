<?php
/**
 * IpLocationFilter class file for Dz Framework
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2020 Dezero
 */

namespace dz\filters;

use Dz;
use dz\helpers\Log;
use Yii;

/**
 * Check if current location (from IP address) has access
 */
class IpLocationFilter extends \CFilter
{
    /**
     * @var array IP addresses excluded for this filter
     */
    public $excluded_ip_addresses = [];


    /**
     * @var array ip addresses with permissions to access
     */
    public $allowed_ip_addresses = [];


    /**
     * @var array country codes with permissions to access
     */
    public $allowed_countries = [];


    /**
     * @var bool exclude the IP filter on TEST mode?
     */
    public $excluded_dev_mode = false;


    /**
     * Performs the pre-action filtering.
     *
     * @param CFilterChain $filterChain the filter chain that the filter is on.
     * @return boolean whether the filtering process should continue and the action should be executed.
     * @throws CHttpException if the user is denied access.
     */
    protected function preFilter($filterChain)
    {
        $ip_address = Yii::app()->request->getUserHostAddress();

        // Exclude "error/location" path
        $path_info = Yii::app()->getRequest()->getPathInfo();
        if ( preg_match("/error\/location/", $path_info) )
        {
            return true;
        }

        // Exclude addresses IP?
        $current_environment = Dz::get_environment();
        if ( !in_array($ip_address, $this->excluded_ip_addresses) && ( $current_environment !== 'dev' || ! $this->excluded_dev_mode ) )
        {
            // Allow access only for selected IP addresses
            if ( !empty($this->allowed_ip_addresses) )
            {
                // Get country code from IP address
                if ( !in_array($ip_address, $this->allowed_ip_addresses) )
                {
                    // Save access denied log message
                    Log::denied_location($ip_address .' blocked - IP address is not allowed');

                    // Custom redirect
                    Yii::app()->request->redirect(Yii::app()->createAbsoluteUrl('error/location'));
                    
                    // throw new CHttpException(403, Yii::t('app', 'Access denied. Your country is not allowed.'));
                    return false;
                }
            }

            // Allow access only for selected countries
            if ( !empty($this->allowed_countries) )
            {
                // Get country code from IP address
                $country_code = Yii::app()->ip2location->getCountryCode($ip_address);
                if ( !in_array($country_code, $this->allowed_countries) )
                {
                    // Save access denied log message
                    Log::denied_location($ip_address .' blocked - Country code: '. $country_code);

                    // Custom redirect
                    Yii::app()->request->redirect(Yii::app()->createAbsoluteUrl('error/location'));
                    
                    // throw new CHttpException(403, Yii::t('app', 'Access denied. Your country is not allowed.'));
                    return false;
                }
            }
        }

        return true;

    }
}