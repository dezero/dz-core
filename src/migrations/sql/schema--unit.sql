-- -----------------------------------------------------
-- Table `commerce_unit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `commerce_unit` ;

CREATE TABLE IF NOT EXISTS `commerce_unit` (
  `unit_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` CHAR(1) NOT NULL DEFAULT 'L'
    COMMENT "ENUM ('L' => 'Length', 'V' => 'Volume', 'W' => 'Weight',  'A' => 'Area',  'P' => 'Pressure',  'M' => 'Time',  'T' => 'Temperature')",
  `name` VARCHAR(32) NOT NULL,
  `description` VARCHAR(255) NULL,
  `unit_type` VARCHAR(8) NULL,
  PRIMARY KEY (`unit_id`),
  INDEX `unit_type_index` (`type`),
  INDEX `unit_name_index` (`name`),
  INDEX `unit_unit_type_index` (`unit_type`)
) ENGINE=MyISAM;


-- -----------------------------------------------------------------------------
-- Default unit data from https://www.drupal.org/project/unitsapi
-- -----------------------------------------------------------------------------
INSERT INTO `commerce_unit` (`type`, `name`, `description`, `unit_type`) VALUES
  ('L', 'height_mm', 'Altura en milímetros', 'mm'),
  ('L', 'height_cm', 'Altura en centímetros', 'cm'),
  ('L', 'height_m', 'Altura en metros', 'm'),
  ('L', 'width_mm', 'Anchura en milímetros', 'mm'),
  ('L', 'width_cm', 'Anchura en centímetros', 'cm'),
  ('L', 'width_m', 'Anchura en metros', 'm'),
  ('L', 'depth_mm', 'Profunidad en milímetros', 'mm'),
  ('L', 'depth_cm', 'Profunidad en centímetros', 'cm'),
  ('L', 'depth_m', 'Profunidad en metros', 'm'),

  ('W', 'weight_mg', 'Peso en miligramos', 'mg'),
  ('W', 'weight_g', 'Peso en gramos', 'g'),
  ('W', 'weight_kg', 'Peso en kilogramos', 'kg'),

  ('V', 'volume_L', 'Volumen en litros', 'L'),
  ('V', 'volume_mL', 'Volumen en mililitros', 'mL'),
  ('V', 'volume_mm3', 'Volumen en milímetros cúbicos', 'mm3'),
  ('V', 'volume_cm3', 'Volumen en centímetros cúbicos', 'cm3'),
  ('V', 'volume_m3', 'Volumen en metros cúbicos', 'm3'),

  ('A', 'area_m2', 'Área en metros cuadrados', 'm2'),

  ('P', 'pressure_bar', 'Presión en bares', 'bar'),
  ('P', 'pressure_mb', 'Presión en milibares', 'mb'),
  ('P', 'pressure_Pa', 'Presión en pascales', 'Pa'),

  ('M', 'time_s', 'Tiempo en segundos', 'sec'),
  ('M', 'time_d', 'Tiempo en días', 'día'),
  ('M', 'time_h', 'Tiempo en horas', 'horas'),
  ('M', 'time_min', 'Tiempo en minutos', 'min'),
  ('M', 'time_w', 'Tiempo en semanas', 'semanas'),
  ('M', 'time_mon', 'Tiempo en meses', 'meses'),
  ('M', 'time_y', 'Tiempo en años', 'años'),

  ('T', 'temp_celsius', 'Temperatura en Celsius', 'ºC'),
  ('T', 'temp_fahrenheit', 'Temperatura en Fahrenheit', 'ºF'),
  ('T', 'temp_kelvin', 'Temperatura en Kelvin', 'K');
  
