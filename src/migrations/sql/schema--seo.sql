-- -----------------------------------------------------
-- SEO VARIABLES
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `commerce_seo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `commerce_seo` ;

CREATE TABLE IF NOT EXISTS `commerce_seo` (
  `entity_id` INT UNSIGNED NOT NULL,
  `entity_type` VARCHAR(16) NOT NULL,
  `language_id` VARCHAR(4) NOT NULL
    COMMENT 'CONSTRAINT FOREIGN KEY (language_id) REFERENCES commerce_language(language_id)',
  `url_auto` VARCHAR(255) NULL,
  `url_counter` TINYINT(1) NOT NULL DEFAULT 0,
  `url_manual` VARCHAR(255) NULL,
  `meta_title` VARCHAR(128) NULL,
  `meta_description` VARCHAR(512) NULL,
  `created_date` INT UNSIGNED NOT NULL,
  `created_uid` INT UNSIGNED NOT NULL
    COMMENT "CONSTRAINT FOREIGN KEY (created_uid) REFERENCES user_users(uid)",
  `updated_date` INT UNSIGNED NOT NULL,
  `updated_uid` INT UNSIGNED NOT NULL
    COMMENT "CONSTRAINT FOREIGN KEY (updated_uid) REFERENCES user_users(uid)",
  PRIMARY KEY (`entity_id`, `entity_type`, `language_id`),
  INDEX `seo_entity_index` (`entity_id`, `entity_type`),
  INDEX `seo_language_index` (`language_id`)
) ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `commerce_seo_url_history`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `commerce_seo_url_history` ;

CREATE TABLE IF NOT EXISTS `commerce_seo_url_history` (
  `url_history_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `entity_id` INT UNSIGNED NOT NULL,
  `entity_type` VARCHAR(16) NOT NULL,
  `language_id` VARCHAR(4) NOT NULL
    COMMENT 'CONSTRAINT FOREIGN KEY (language_id) REFERENCES commerce_language(language_id)',
  `url` VARCHAR(255) NOT NULL,
  `created_date` INT UNSIGNED NOT NULL,
  `created_uid` INT UNSIGNED NOT NULL
    COMMENT "CONSTRAINT FOREIGN KEY (created_uid) REFERENCES user_users(uid)",
  `updated_date` INT UNSIGNED NOT NULL,
  `updated_uid` INT UNSIGNED NOT NULL
    COMMENT "CONSTRAINT FOREIGN KEY (updated_uid) REFERENCES user_users(uid)",
  PRIMARY KEY (`url_history_id`),
  INDEX `url_history_entity_index` (`entity_id`, `entity_type`),
  INDEX `url_history_language_index` (`language_id`),
  INDEX `url_history_url_index` (`url`)
) ENGINE = MyISAM;
