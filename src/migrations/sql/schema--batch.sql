-- -----------------------------------------------------
-- Table `batch_operation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `batch_operation` ;

CREATE TABLE IF NOT EXISTS `batch_operation` (
  `batch_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) NOT NULL,
  `last_operation` INT UNSIGNED NOT NULL DEFAULT 0,
  `total_operations` INT UNSIGNED NOT NULL DEFAULT 0,
  `entity_id` INT(10) UNSIGNED NOT NULL DEFAULT 0,
  `results` LONGTEXT NULL DEFAULT NULL,
  `created_date` INT UNSIGNED NOT NULL,
  `created_uid` INT UNSIGNED NOT NULL
    COMMENT "CONSTRAINT FOREIGN KEY (created_uid) REFERENCES user_users(uid)",
  `updated_date` INT UNSIGNED NOT NULL,
  `updated_uid` INT UNSIGNED NOT NULL
    COMMENT "CONSTRAINT FOREIGN KEY (updated_uid) REFERENCES user_users(uid)",
  PRIMARY KEY (`batch_id`),
  INDEX batch_operation_name (name)
) ENGINE=MyISAM;

-- Add new field "summary_results"
ALTER TABLE `batch_operation` ADD `summary_results` VARCHAR(255) NULL DEFAULT NULL AFTER `entity_id`;

-- Add new columns: "total_items", "item_starting_num", "item_ending_num"
ALTER TABLE `batch_operation`
  ADD `total_items` INT UNSIGNED NOT NULL DEFAULT '0' AFTER `entity_id`,
  ADD `item_starting_num` INT UNSIGNED NOT NULL DEFAULT '0' AFTER `total_items`,
  ADD `item_ending_num` INT UNSIGNED NOT NULL DEFAULT '0' AFTER `item_starting_num`;