-- -----------------------------------------------------
-- Table `dz_file`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dz_file` ;

CREATE TABLE IF NOT EXISTS `dz_file` (
  `file_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `file_name` VARCHAR(128) NOT NULL,
  `file_path` VARCHAR(255) NOT NULL,
  `file_type` VARCHAR(64) NOT NULL,
  `file_size` INT UNSIGNED NOT NULL DEFAULT 0,
  `file_options` VARCHAR(255) NULL,
  `entity_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `title` VARCHAR(255) NULL,
  `created_date` INT UNSIGNED NOT NULL,
  `created_uid` INT UNSIGNED NOT NULL
    COMMENT "CONSTRAINT FOREIGN KEY (created_uid) REFERENCES user_users(uid)",
  `updated_date` INT UNSIGNED NOT NULL,
  `updated_uid` INT UNSIGNED NOT NULL
    COMMENT "CONSTRAINT FOREIGN KEY (updated_uid) REFERENCES user_users(uid)",
  PRIMARY KEY (`file_id`),
  INDEX `dz_file_entity_index` (`entity_id`)
) ENGINE=MyISAM;