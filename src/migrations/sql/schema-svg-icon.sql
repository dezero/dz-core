-- -----------------------------------------------------
-- Table `dz_svg_icon`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dz_svg_icon` ;

CREATE TABLE IF NOT EXISTS `dz_svg_icon` (
  `svg_icon_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(128) NOT NULL,
  `xml_content` TEXT NULL,
  `height` INT UNSIGNED NOT NULL DEFAULT 0,
  `width` INT UNSIGNED NOT NULL DEFAULT 0,
  `created_date` INT UNSIGNED NOT NULL,
  `created_uid` INT UNSIGNED NOT NULL
    COMMENT "CONSTRAINT FOREIGN KEY (created_uid) REFERENCES user_users(uid)",
  `updated_date` INT UNSIGNED NOT NULL,
  `updated_uid` INT UNSIGNED NOT NULL
    COMMENT "CONSTRAINT FOREIGN KEY (updated_uid) REFERENCES user_users(uid)",
  PRIMARY KEY (`svg_icon_id`)
) ENGINE=MyISAM;


-- Add new column "email_image_id"
ALTER TABLE `dz_svg_icon` ADD `email_image_id` INT UNSIGNED NULL
  COMMENT 'CONSTRAINT FOREIGN KEY (email_image_id) REFERENCES web_image(image_id)' AFTER `width`;