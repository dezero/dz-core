-- -----------------------------------------------------
-- Table `dz_translate_source`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dz_translate_source` ;

CREATE TABLE IF NOT EXISTS `dz_translate_source` (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  category VARCHAR(32) DEFAULT NULL,
  message TEXT,
  PRIMARY KEY (id)
) ENGINE=MyISAM;

-- -----------------------------------------------------
-- Table `dz_translate_source`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dz_translate_message` ;

CREATE TABLE IF NOT EXISTS `dz_translate_message` (
  id INT UNSIGNED NOT NULL
    COMMENT "CONSTRAINT FOREIGN KEY (id) REFERENCES dz_translate_source(id)",
  language VARCHAR(16) NOT NULL default 'es',
  translation TEXT,
  PRIMARY KEY (id, language)
) ENGINE=MyISAM;