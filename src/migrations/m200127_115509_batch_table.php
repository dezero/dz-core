<?php
/**
 * Migration class m200127_115509_batch_table
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200127_115509_batch_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "my_table" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('batch', true);

        $this->createTable('batch', [
            'batch_id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull(),
            'last_operation_name' => $this->string(128),
            'last_operation_num' => $this->integer()->unsigned()->notNull()->defaultValue(0),
            'total_operations' => $this->integer()->unsigned()->notNull()->defaultValue(0),
            'operations_json' => $this->string(512),
            'total_items' => $this->integer()->unsigned()->notNull()->defaultValue(0),
            'item_starting_num' => $this->integer()->unsigned()->notNull()->defaultValue(0),
            'item_ending_num' => $this->integer()->unsigned()->notNull()->defaultValue(0),
            'entity_id' => $this->integer()->unsigned(),
            'entity_type' => $this->string(32),
            'entity_scenario' => $this->string(32),
            'summary_json' => $this->string(512),
            'results_json' => $this->longText(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create indexes
        $this->createIndex(null, 'batch', ['name'], false);
        $this->createIndex(null, 'batch', ['entity_id', 'entity_type'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'batch', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'batch', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('batch');
		return false;
	}
}

