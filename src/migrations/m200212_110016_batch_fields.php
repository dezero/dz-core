<?php
/**
 * Migration class m200212_110016_batch_fields
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200212_110016_batch_fields extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Rename "entity" columns
        $this->renameColumn('batch', 'entity_id', 'model_id');
        $this->renameColumn('batch', 'entity_type', 'model_class');
        $this->renameColumn('batch', 'entity_scenario', 'model_scenario');

        // Allow multiple id's for "model_id"
        $this->alterColumn('batch', 'model_id', $this->string(32));

        // Extend lenght of model_class column to 64 bytes
        $this->alterColumn('batch', 'model_class', $this->string(64));

		// Add new columns to "batch" table
		$this->addColumn('batch', 'batch_type', $this->string(32)->notNull()->after('batch_id'));
        $this->addColumn('batch', 'description', $this->string(255)->after('name'));
        $this->addColumn('batch', 'num_errors', $this->integer()->unsigned()->notNull()->defaultValue(0)->after('total_items'));
        $this->addColumn('batch', 'num_warnings', $this->integer()->unsigned()->notNull()->defaultValue(0)->after('num_errors'));
        $this->addColumn('batch', 'model_type', $this->string(32)->after('model_class'));
        $this->addColumn('batch', 'file_id', $this->integer()->unsigned()->after('model_scenario'));

        // Foreign keys
        $this->addForeignKey(null, 'batch', ['file_id'], 'asset_file', ['file_id'], 'SET NULL', null);

        // Create index for new columns
        $this->createIndex(null, 'batch', ['batch_type'], false);

        // Recreate old "enity" index
        $this->dropIndex('batch_entity_id_entity_type_idx', 'batch');
        $this->createIndex(null, 'batch', ['model_id', 'model_class'], false);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

