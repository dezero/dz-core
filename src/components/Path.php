<?php
/**
 * Class for getting server paths used by Dz Framework
 */

namespace dz\components;

use dz\base\ApplicationComponent;
use Yii;

class Path extends ApplicationComponent
{
    /**
     * Array with pathes
     */
    public $pathes;


    /**
     * @var string  Base path
     */
    private $_basePath;


    /**
     * Init function
     */
    public function init()
    {
        parent::init();

        if ( $this->pathes === null )
        {
            $this->pathes = Yii::app()->config->get('@dz.config.pathes');
        }
    }


    /**
     * Get a path by name
     */
    public function get($name, $is_full_path = false)
    {
        $relative_path = '';

        switch ( $name )
        {
            case 'base':
            case 'basePath':
                return $this->basePath();
                break;

            case 'log':
            case 'logPath':
                $relative_path = $this->logPath();
                break;

            case 'theme':
            case 'themePath':
                $relative_path = $this->themePath();
                break;

            default:
                if ( isset($this->pathes[$name]) )
                {
                    $relative_path = $this->pathes[$name];
                }
                else if ( isset($this->pathes[$name .'Path']) )
                {
                    $relative_path = $this->pathes[$name .'Path'];
                }
                break;
        }

        // Full directory path?
        if ( $is_full_path )
        {
            // Replace "www/../www/" with "www/";
            $full_path = $this->basePath() . DIRECTORY_SEPARATOR . $relative_path;
            $webroot_path = $this->webRootPath();
            return str_replace($webroot_path . DIRECTORY_SEPARATOR .'..'. DIRECTORY_SEPARATOR . $webroot_path, $webroot_path, $full_path);
        }

        return $relative_path;
    }


    /**
     * Returns the root path
     */
    public function basePath()
    {
        if ( ! $this->_basePath )
        {
            if ( isset(Yii::app()->params['basePath']) )
            {
                $this->_basePath = Yii::app()->params['basePath'];
            }

            if ( isset($this->pathes['basePath']) )
            {
                $this->_basePath = $this->pathes['basePath'];
            }

            $this->_basePath = DZ_BASE_PATH;
        }

        return $this->_basePath;
    }


    /**
     * Returns the theme path
     */
    public function themePath()
    {
        return Yii::app()->theme->getBasePath();
    }


    /**
     * Returns the log path (inside "storage" folder)
     */
    public function logPath()
    {
        return Yii::getAlias('@logs');
    }


    /**
     * Return relative path where CORE files are stored
     */
    public function corePath()
    {
        return Yii::getAlias('@core');
    }


    /**
     * Returns the backups path (inside "storage" folder)
     */
    public function backupPath($backup_subdirectory = '')
    {
        $backup_path = Yii::getAlias('@storage') . DIRECTORY_SEPARATOR . 'backups';
        if ( !empty($backup_subdirectory) )
        {
            $backup_path .= DIRECTORY_SEPARATOR . $backup_subdirectory;
        }

        return $backup_path;
    }


    /**
     * Return webroot relative path
     */
    public function webRootPath()
    {
        return $this->get('webRoot');
    }


    /**
     * Return relative path where files are stored
     */
    public function filesPath()
    {
        return $this->get('files');
    }


    /**
     * Return relative path where image files are stored
     */
    public function imagesPath()
    {
        return $this->get('images');
    }


    /**
     * Return relative path where temporary image files are stored
     */
    public function tempPath()
    {
        return $this->get('temp');
    }


    /**
     * Returns relative path where assets files are stored
     */
    public function assetsPath()
    {
        return $this->get('webRoot') . DIRECTORY_SEPARATOR . 'assets';
    }
}