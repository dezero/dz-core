<?php
/**
 * ClassMap is a MAPPER for classes names.
 * 
 * Based on Yii2 Container that implements a [dependency injection](http://en.wikipedia.org/wiki/Dependency_injection) container.
 */

namespace dz\components;

use dz\base\ApplicationComponent;
use ReflectionClass;
use Yii;

class ClassMap extends ApplicationComponent
{
    /**
     * @var array object definitions indexed by their types
     */
    private $_definitions = [];


    /**
     * Init function
     */
    public function init()
    {
        // Load configuration
        $vec_definitions = Yii::app()->config->get('components.class_map');
        if ( !empty($vec_definitions) )
        {
            foreach ( $vec_definitions as $class => $definition )
            {
                $this->set($class, $definition);
            }
        }

        parent::init();
    }


    /**
     * Returns correct namespace for a class name
     */
    public function get($class)
    {
        if ( isset($this->_definitions[$class]) )
        {
            return $this->_definitions[$class];
        }

        return $class;
    }


    /**
     * Registers a class definition with this container.
     */
    public function set($class, $definition)
    {
        $this->_definitions[$class] = $definition;
        return $this;
    }


    /**
     * Create a new instance of the requested class
     */
    public function create($class, $vec_params = [])
    {
        // User ReflectionClass
        try
        {
            $reflection = new ReflectionClass($this->get($class));
        }
        catch (\ReflectionException $e)
        {
            throw new \CException('Failed to instantiate component or class "' . $class . '".', 0, $e);
        }

        // Params is a string? If so, transform into an array
        if ( !empty($vec_params) && is_string($vec_params) )
        {
            $vec_params = [$vec_params];
        }

        if ( $reflection->isInstantiable() )
        {
            return $reflection->newInstanceArgs($vec_params);
        }

        $class_name = '\\'. $this->get($class_name);
        return new $class_name;
    }
}
 