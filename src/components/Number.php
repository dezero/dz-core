<?php
/**
 * Number component class file
 *
 * This component is used to format and unformat number fields
 * Based on http://www.yiiframework.com/wiki/360/custom-number-formatting-or-decimal-separators-and-i18n/
 *
 * @see http://www.yiiframework.com/wiki/360/custom-number-formatting-or-decimal-separators-and-i18n/
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2013 Dezero
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package extensions.DzFormatter
 */

namespace dz\components;

/**
 * @see http://www.yiiframework.com/wiki/360/custom-number-formatting-or-decimal-separators-and-i18n/
 */
class Number extends \CFormatter
{
    /**
     * Three elements may be specified: "decimals", "decimalSeparator" and 
     * "thousandSeparator". They correspond to the number of digits after 
     * the decimal point, the character displayed as the decimal point,
     * and the thousands separator character.
     * new: override default value: 2 decimals, a comma (,) before the decimals 
     * and no separator between groups of thousands
     *
     * @var array the format used to format a number with PHP number_format() function.
     */
    public $numberFormat = ['decimals' => 2, 'decimalSeparator' => ',', 'thousandSeparator' => ''];
 

    /**
     * Formats the value as a number using PHP number_format() function.
     * new: if the given $value is null/empty, return null/empty string
     *
     * @param mixed $value the value to be formatted
     * @return string the formatted result
     * @see numberFormat
     */
    public function formatNumber($value, $vec_number_format = [])
    {
        if ( $value !== null && $value !== '' )
        {
            $vec_format = [
                'decimals'          => isset($vec_number_format['decimals']) ? $vec_number_format['decimals'] : $this->numberFormat['decimals'],
                'decimalSeparator'  => isset($vec_number_format['decimalSeparator']) ? $vec_number_format['decimalSeparator'] : $this->numberFormat['decimalSeparator'],
                'thousandSeparator' => isset($vec_number_format['thousandSeparator']) ? $vec_number_format['thousandSeparator'] : $this->numberFormat['thousandSeparator'],
            ];
            if ( is_numeric($value) )
            {
                return number_format($value, $vec_format['decimals'], $vec_format['decimalSeparator'], $vec_format['thousandSeparator']);
            }

            return $value;
        }
        return null;
    }
        
 
    /**
     * Turns the given formatted number (string) into a float
     *
     * @param string $formatted_number A formatted number (usually formatted with the formatNumber() function)
     * @return float the 'unformatted' number
     */
    public function unformatNumber($formatted_number)
    {
        if ( $formatted_number !== null AND $formatted_number !== '' )
        {
            // Only 'unformat' if parameter is not float already
            if ( is_float($formatted_number) )
            {
                return $formatted_number;
            }
            
            // Make transformation
            $value = str_replace($this->numberFormat['thousandSeparator'], '', $formatted_number);
            $value = str_replace($this->numberFormat['decimalSeparator'], '.', $value);
            return (float) $value;
        }

        return null; 
    }


    /**
     * Round a number to the nearest nth
     *
     * @param   int  number to round
     * @param   int  number to round to
     * @return  int
     */
    public function round($number, $nearest = 5)
    {
        return round($number / $nearest) * $nearest;
    }
}
