<?php
/**
 * Mailer class file for Dz Framework
 *
 * Override some methods from YiiMailer extension
 *
 * @see http://www.yiiframework.com/extension/yiimailer/
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2019 Dezero
 */

namespace dz\components;

use Dz;
use dz\helpers\Json;
use Yii;

// Import YiiMailer class
Yii::import('@lib.mail.YiiMailer');

class Mailer extends \YiiMailer
{
    /**
     * Define the key of the Yii params for the config array
     */
    const CONFIG_PARAMS='YiiMailer';

	/**
	 * DEZERO custom properties
	 */

	// Save config params
	protected $config;


	// Module
	public $module;


	// Send type
	public $send_type = 'email';


	// Filename
	public $filename;


	/**
	 * Save "to address" (problems for getting this value form PHPMailer)
	 */
	public $to_address;


    /**
     * Frontend where layout and partials are stored
     */
    public $theme = 'backend';


    /**
     * If the email fails to send, retry via a "queue job"
     */
    public $is_retry_error = false;


	/**
	 * Default paths and private properties
	 */
	private $viewPath = '@theme.views.mail';
	
	private $layoutPath = '@theme.views.layouts';
	
	private $baseDirPath = '@theme.images.mail';

	private $testMode = false;

	private $savePath = '@www.assets.mail';
	
	private $layout = 'mail';
	
	private $view;
	
	private $data;


	/**
	 * Set and configure initial parameters
	 * 
	 * @param string $view View name
	 * @param array $data Data array
	 * @param string $layout Layout name
	 */
	public function __construct($view = '', $data = [], $layout = '')
	{
		// Initialize config
		if( isset(Yii::app()->params[self::CONFIG_PARAMS]) )
		{
			$config = Yii::app()->params[self::CONFIG_PARAMS];
		}
		else
		{
            $config = Yii::app()->config->get('components.mail');
		}

		$this->setConfig($config);
		$this->setView($view);
		$this->setData($data);
		$this->setLayout($layout);
	}


	/**
	 * Configure parameters
	 * @param array $config Config parameters
	 * @throws CException 
	 */
	// private function setConfig($config)
	protected function setConfig($config)
	{
		if ( !is_array($config) )
		{
			throw new \CException("Configuration options must be an array!");
		}

		$this->config = $config;

		if ( isset($config['fax']) )
		{
			unset($config['fax']);
		}
		foreach( $config as $key => $val )
		{
			$this->$key = $val;
		}
	}

/*
|--------------------------------------------------------------------------
| COPIED FROM YiiMailer.php class
|--------------------------------------------------------------------------
*/
	/**
	 * Set the view to be used
	 * @param string $view View file
	 * @throws CException 
	 */
	public function setView($view)
	{
		if ( $view != '' )
		{
			if ( ! is_file($this->getViewFile($this->viewPath.'.'.$view)) )
            {
				throw new \CException('View "'.$view.'" not found');
            }
			$this->view=$view;
		}
	}
	

	/**
	 * Get currently used view
	 * @return string View filename
	 */
	public function getView()
	{
		return $this->view;
	}
	

	/**
	 * Clear currently used view
	 */
	public function clearView()
	{
		$this->view = null;
	}


	/**
	 * Send data to be used in mail body
	 * @param array $data Data array
	 */
	public function setData($data)
	{
		$this->data = $data;
	}
	

	/**
	 * Get current data array
	 * @return array Data array
	 */
	public function getData()
	{
		return $this->data;
	}
	

	/**
	 * Clear current data array
	 */
	public function clearData()
	{
		$this->data = [];
	}
	

	/**
     * 
	 * Set layout file to be used
	 * @param string $layout Layout filename
	 * @throws CException 
	 */
	public function setLayout($layout)
	{
		if ( $layout != '' )
		{
			if ( !is_file($this->getViewFile($this->layoutPath.'.'.$layout)) )
            {
				throw new \CException('Layout "'.$layout.'" not found!');
            }
			$this->layout=$layout;
		}
	}
	

	/**
	 * Get current layout
	 * @return string Layout filename
	 */
	public function getLayout()
	{
		return $this->layout;
	}
	

	/**
	 * Clear current layout
	 */
	public function clearLayout()
	{
		$this->layout = null;
	}

	/**
	 * Set path for email views
	 * @param string $path Yii path
	 * @throws CException 
	 */
	public function setViewPath($path)
	{
		if ( !is_string($path) && !preg_match("/[a-z0-9\.]/i",$path) )
        {
			throw new \CException('Path "'.$path.'" not valid!');
        }
		$this->viewPath=$path;
	}
	

	/**
	 * Get path for email views
	 * @return string Yii path 
	 */
	public function getViewPath()
	{
		return $this->viewPath;
	}
	

	/**
	 * Set path for email layouts
	 * @param string $path Yii path
	 * @throws CException 
	 */
	public function setLayoutPath($path)
	{
		if ( !is_string($path) && !preg_match("/[a-z0-9\.]/i",$path) )
        {
			throw new \CException('Path "'.$path.'" not valid!');
        }
		$this->layoutPath=$path;
	}
	

	/**
	 * Get path for email layouts
	 * @return string Yii path 
	 */
	public function getLayoutPath()
	{
		return $this->layoutPath;
	}
	

	/**
	 * Set path for images to embed in email messages
	 * @param string $path Yii path
	 * @throws CException 
	 */
	public function setBaseDirPath($path)
	{
		if ( !is_string($path) && !preg_match("/[a-z0-9\.]/i",$path) )
        {
			throw new \CException('Path "'.$path.'" not valid!');
        }
		$this->baseDirPath=$path;
	}
	

	/**
	 * Get path for email images
	 * @return string Yii path 
	 */
	public function getBaseDirPath()
	{
		return $this->baseDirPath;
	}


	/**
	 * Generates HTML email, with or without layout
	 */
	public function render()
	{
		//render view as body if specified
		if ( isset($this->view) && $this->view != '' )
		{
			$this->setBody($this->renderView($this->viewPath.'.'.$this->view, $this->data));
		}
		
		//render with layout if given
		if($this->layout)
		{
			//has layout
			$this->MsgHTMLWithLayout($this->Body, Yii::getAlias($this->baseDirPath));
		}
		else
		{
			//no layout
			$this->MsgHTML($this->Body, Yii::getAlias($this->baseDirPath));
		}
	}

/*
|--------------------------------------------------------------------------
| COPIED FROM YiiMailer.php class
|--------------------------------------------------------------------------
*/


	/**
	 * Render HTML email message with layout
	 * @param string $message Email message
	 * @param string $basedir Path for images to embed in message
	 */
	protected function MsgHTMLWithLayout($message, $basedir = '')
	{
		// DEZERO - Preprocess CSS style
		$vec_params = [
			'content'    => $message,
			'data'       => $this->data,
			'style'      => ''
		];

		if ( preg_match("/\[style\]/", $message) )
		{
			$que_message = explode("[style]", $message);
			$vec_params['content'] = $que_message[2];
			$vec_params['style'] = $que_message[1]; 
		}

		$this->MsgHTML($this->renderView($this->layoutPath.'.'.$this->layout, $vec_params), $basedir);
	}


	/**
	 * Render the view file
	 * @param string $viewName Name of the view
	 * @param array $viewData Data for extraction
	 * @return string The rendered result
	 * @throws CException
	 */
	public function renderView($viewName, $viewData=null)
	{
		// DEZERO - Add subject to view template
		if ( isset($this->Subject) )
		{
			$this->data['subject'] = $this->Subject;
			if ( isset($viewData['data']) )
			{
				$viewData['data']['subject'] = $this->data['subject'];
			}
			else
			{
				$viewData['subject'] = $this->data['subject'];
			}
		}

        // Force theme
        if ( ! Dz::is_console() )
        {
            if ( isset($this->config['theme']) )
            {
                Yii::app()->theme = $this->config['theme'];
            }
            else
            {
                Yii::app()->theme = $this->theme;
            }
        }

        // Resolve the file name
        if( ( $viewFile = $this->getViewFile($viewName) ) !== false )
        {
            // Use controller instance if available or create dummy controller for console applications
            if ( isset(Yii::app()->controller) )
            {
                $controller = Yii::app()->controller;
            }

            // Use custom controller for CONSOLE applications
            else
            {
                $controller = new \dz\console\Controller(__CLASS__);
            }

            // 30/08/2021 - CONSOLE --> Change "viewPath" with correct theme
            if ( Dz::is_console() )
            {
                if ( isset($this->config['theme']) )
                {
                    $current_theme = $this->config['theme'];
                }
                else
                {
                    $current_theme = $this->theme;
                }

                if ( $current_theme !== 'backend' )
                {
                    $controller->viewPath = str_replace('/www/themes/backend', '/www/themes/'. $current_theme, $controller->getViewPath());
                }
            }

            //render and return the result
            return $controller->renderInternal($viewFile,$viewData,true);
        }

        return parent::renderView($viewName, $viewData);
	}


	/**
	 * Find the view file for the given view name
	 * @param string $viewName Name of the view
	 * @return string The file path or false if the file does not exist
	 */
	public function getViewFile($viewName)
	{
        // Use controller instance if available or create dummy controller for console applications
        if ( isset(Yii::app()->controller) )
        {
            $controller = Yii::app()->controller;
        }

        // Use custom controller for CONSOLE applications
        else
        {
            $controller = new \dz\console\Controller(__CLASS__);
        }

        // 30/08/2021 - CONSOLE --> Change "viewPath" with correct theme
        if ( Dz::is_console() )
        {
            if ( isset($this->config['theme']) )
            {
                $current_theme = $this->config['theme'];
            }
            else
            {
                $current_theme = $this->theme;
            }

            if ( $current_theme !== 'backend' )
            {
                $controller->viewPath = str_replace('/www/themes/backend', '/www/themes/'. $current_theme, $controller->getViewPath());
            }
        }

        return $controller->getViewFile($viewName);

        /*
        // In web application, use existing method
        if ( ! Dz::is_console() && isset(Yii::app()->controller) )
        {
            return Yii::app()->controller->getViewFile($viewName);
        }

		// Resolve the view file
		// @todo: support for themes in console applications
		if ( empty($viewName) )
		{
			return false;
		}
		
		$viewFile = Yii::getAlias($viewName);
		if ( is_file($viewFile.'.tpl.php') )
		{
			return Yii::app()->findLocalizedFile($viewFile.'.tpl.php');
		}

		return false;
        */
	}

	/**
	 * Set module name
	 * 
	 * @param string
	 * @throws CException 
	 */
	public function setModule($module)
	{
		$this->module = $module;
	}


    /**
     * Get path where EML file will be saved
     *
     * @return string
     */
    public function getSavePath()
    {
        return $this->savePath;
    }


    /**
     * Assign new value for test mode
     */
    public function setTestMode($test_mode)
    {
        $this->testMode = $test_mode;
    }


	/**
	 * Save message as eml file
	 * 
	 * @return boolean
	 */
	public function save()
	{
		// DEZERO - Create a folder if it doesn't already exist
		$dir = Yii::getAlias($this->savePath);
		if ( ! file_exists($dir) )
		{
			mkdir($dir, 0774, true);
			chmod($dir, 0774);
		}

		$this->filename = date('YmdHis') . '_' . uniqid() . '.eml';
		if ( !empty($this->module) )
		{
			$this->filename = $this->module .'_'. $this->filename;	
		}
		
		$dir = Yii::getAlias($this->savePath);
		
		// Check if dir exists and is writable
		if( !is_writable($dir) )
		{
			throw new \CException('Directory "'.$dir.'" does not exist or is not writable!');
		}

		try
		{
			$file = fopen($dir . DIRECTORY_SEPARATOR . $this->filename,'w+');
			fwrite($file, $this->GetSentMIMEMessage());
			fclose($file);
			
            return true;
		}
		catch (\Exception $e)
		{
			$this->SetError($e->getMessage());
			return false;
		}
	}


	/**
	 * Send an email
	 *
	 * @return	bool
	 */
	public function send_email()
	{
		$this->send_type = 'email';
		return $this->send();
	}


	/**
	 * Render message and send emails
	 * 
	 * @return boolean
	 */
	public function send($is_render = true, $is_save = true)
	{
		// Render message
		if ( $is_render )
		{
			$this->render();
		}
		
		// Send the message
		try
		{
			// TEST
			// $this->ClearAllRecipients();
			// $this->setTo('test@dezero.es');
			// TEST

			// Prepare the message
			if ( ! $this->PreSend() )
			{
				Yii::log("Error en el envío de ". $this->send_type ." a ". Json::encode($this->to)." - [Fichero: '". $this->filename ."'] - ERROR: Presend()", "profile", "mailer");
				return false;
			}

			// Always save email as an EML file
			if ( $is_save )
			{
				$this->save();
			}

			// In test mode, save message as a file
			if ( $this->testMode )
			{
				// return $this->save();
				return true;
			}
			else
			{
				$post_send_result = $this->PostSend();
				if ( ! $post_send_result )
				{
					Yii::log("Error en el envío de ". $this->send_type ." a ". Json::encode($this->to)." - [Fichero: '". $this->filename ."'] - ERROR: ". Json::encode($this->ErrorInfo), "profile", "mailer");
				}
				return $post_send_result;
			}
		}
		catch (\phpmailerException $e)
		{
			$this->mailHeader = '';
			$this->SetError($e->getMessage());
			if ($this->exceptions)
			{
				throw $e;
			}
			return false;
		}
	}

	/**
	 * Get to Address
	 */
	public function getToAddress()
	{
		return $this->to;
	}
}
