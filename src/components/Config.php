<?php
/**
 * Component class for working with several config files
 */

namespace dz\components;

use dz\base\ApplicationComponent;
use Yii;

class Config extends ApplicationComponent
{
    /**
     * Find a config file inside @app.config alias path and return the content
     * 
     * @param string Config file name or config file path
     * @return array
     */
    public function get($config_file, $config_key = '')
    {
        if ( ! preg_match("/^@/", $config_file) )
        {
            $config_file_path = Yii::getAlias('@app.config.'. $config_file) .'.php';
        }
        else
        {
            $config_file_path = Yii::getAlias($config_file) .'.php';   
        }

        if ( is_file($config_file_path) )
        {
            // Get configuration file content
            $vec_config = require($config_file_path);
            
            // Return just the content from a configuration key
            if ( !empty($config_key) && !empty($vec_config) && isset($vec_config[$config_key]) )
            {
                return $vec_config[$config_key];
            }

            return $vec_config;
        }

        return null;
    }


    /**
     * Get MySQL database configuration
     */
    public function getDb()
    {
        $vec_config = [];

        // If the connectionString (DSN) is already set, parse it
        if ( Yii::app()->db->connectionString && ($pos = strpos(Yii::app()->db->connectionString, ':')) !== false )
        {
            $vec_config['driver'] = substr(Yii::app()->db->connectionString, 0, $pos);
            $params = substr(Yii::app()->db->connectionString, $pos + 1);
            foreach ( explode(';', $params) as $param )
            {
                if ( ($pos = strpos($param, '=')) !== false )
                {
                    $param_name = substr($param, 0, $pos);
                    $param_value = substr($param, $pos + 1);
                    switch ($param_name)
                    {
                        case 'host':
                            $vec_config['server'] = $param_value;
                            break;
                        case 'port':
                            $vec_config['port'] = $param_value;
                            break;
                        case 'dbname':
                            $vec_config['database'] = $param_value;
                            break;
                        case 'unix_socket':
                            $vec_config['unixSocket'] = $param_value;
                            break;
                        case 'charset':
                            $vec_config['charset'] = $param_value;
                            break;
                        case 'user': // PG only
                            $vec_config['user'] = $param_value;
                            break;
                        case 'password': // PG only
                            $vec_config['password'] = $param_value;
                            break;
                    }
                }
            }

            // Set the port
            if ( !isset($vec_config['port']) && isset($vec_config['driver']) && $vec_config['driver'] == 'mysql' )
            {
                $vec_config['port'] = 3306;
            }
            else if ( isset($vec_config['port']) )
            {
                $vec_config['port'] = (int)$vec_config['port'];
            }

            // Username, password and charset
            if ( !isset($vec_config['username']) )
            {
                $vec_config['username'] = Yii::app()->db->username;
            }
            if ( !isset($vec_config['password']) )
            {
                $vec_config['password'] = Yii::app()->db->password;
            }
            if ( !isset($vec_config['charset']) )
            {
                $vec_config['charset'] = Yii::app()->db->charset;
            }
        }

        return $vec_config;
    }
}