<?php
/**
 * DbConnection class file
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2019 Dezero
 */

namespace dz\db;

use dz\errors\ShellCommandException;
use dz\helpers\StringHelper;
use dz\helpers\Transliteration;
use mikehaertl\shellcommand\Command as ShellCommand;
use Yii;

class DbConnection extends \CDbConnection
{
    /**
     * @var array Ignored tables on backups
     */
    public $backupIgnoredTables;

    /**
     * @inheritdoc
     */
    public function init()
    {
        // Custom Mysql Schema
        $this->driverMap['mysql'] = '\dz\db\MysqlSchema';
        $this->driverMap['mysqli'] = '\dz\db\MysqlSchema';

        // Default charset as UTF-8
        if ( $this->charset === null )
        {
            $this->charset = 'utf8';
        }

        parent::init();
    }


    /**
     * @inheritdoc
     */
    public function createCommand($query = null)
    {
        $this->setActive(true);
        return new DbCommand($this,$query);
    }


    /**
     * Returns a primary key name based on the table and column names.
     *
     * @param string $table
     * @param string|array $columns
     * @return string
     */
    public function getPrimaryKeyName($table, $columns)
    {
        $table = $this->_getTableNameWithoutPrefix($table);
        if ( is_string($columns) )
        {
            $columns = StringHelper::split($columns);
        }
        $name = $this->tablePrefix . $table . '_' . implode('_', $columns) . '_pk';

        return $this->trimObjectName($name);
    }

    /**
     * Returns a foreign key name based on the table and column names.
     *
     * @param string $table
     * @param string|array $columns
     * @return string
     */
    public function getForeignKeyName($table, $columns)
    {
        $table = $this->_getTableNameWithoutPrefix($table);
        if ( is_string($columns) )
        {
            $columns = StringHelper::split($columns);
        }
        $name = $this->tablePrefix . $table . '_' . implode('_', $columns) . '_fk';

        return $this->trimObjectName($name);
    }


    /**
     * Returns an index name based on the table, column names, and whether
     * it should be unique.
     *
     * @param string $table
     * @param string|array $columns
     * @param bool $unique
     * @param bool $foreignKey
     * @return string
     */
    public function getIndexName($table, $columns, $unique = false, $foreignKey = false)
    {
        $table = $this->_getTableNameWithoutPrefix($table);
        if ( is_string($columns) )
        {
            $columns = StringHelper::split($columns);
        }
        $name = $this->tablePrefix . $table . '_' . implode('_', $columns) . ($unique ? '_unq' : '') . ($foreignKey ? '_fk' : '_idx');

        return $this->trimObjectName($name);
    }


    /**
     * Ensures that an object name is within the schema's limit.
     *
     * @param string $name
     * @return string
     */
    public function trimObjectName($name)
    {
        $schema = $this->getSchema();

        if ( !isset($schema->maxObjectNameLength) )
        {
            return $name;
        }

        $name = trim($name, '_');
        $nameLength = StringHelper::strlen($name);

        if ($nameLength > $schema->maxObjectNameLength)
        {
            $parts = array_filter(explode('_', $name));
            $totalParts = count($parts);
            $totalLetters = $nameLength - ($totalParts - 1);
            $maxLetters = $schema->maxObjectNameLength - ($totalParts - 1);

            // Consecutive underscores could have put this name over the top
            if ($totalLetters > $maxLetters)
            {
                foreach ($parts as $i => $part)
                {
                    $newLength = round($maxLetters * StringHelper::strlen($part) / $totalLetters);
                    $parts[$i] = mb_substr($part, 0, $newLength);
                }
            }

            $name = implode('_', $parts);

            // Just to be safe
            if (StringHelper::strlen($name) > $schema->maxObjectNameLength) {
                $name = mb_substr($name, 0, $schema->maxObjectNameLength);
            }
        }

        return $name;
    }


    /**
     * Performs a backup operation. It will execute the default database schema specific backup
     * defined in `getDefaultBackupCommand()`, which uses `mysqldump` for MySQL.
     *
     * @return string The file path to the database backup
     * @throws Exception if the backupCommand config setting is false
     * @throws ShellCommandException in case of failure
     */
    public function backup($is_zip = false)
    {
        $file = $this->getBackupFilePath();
        return $this->backupTo($file, $is_zip);
    }


    /**
     * Performs a backup operation. It will execute the default database schema specific backup
     * defined in `getDefaultBackupCommand()`, which uses `mysqldump` for MySQL.
     *
     * @param string $filePath The file path the database backup should be saved at
     * @throws Exception if the backupCommand config setting is false
     * @throws ShellCommandException in case of failure
     */
    public function backupTo($file_path, $is_zip = false)
    {
        $schema = $this->getSchema();
        $backupCommand = $schema->getDefaultBackupCommand();

        if ( $backupCommand === false )
        {
            throw new Exception('Database not backed up because the backup command is false.');
        }

        // Create the shell command
        $backupCommand = $this->_parseCommandTokens($backupCommand, $file_path);
        $shellCommand = new ShellCommand();
        $shellCommand->setCommand($backupCommand);

        // If we don't have proc_open, maybe we've got exec
        if ( !function_exists('proc_open') && function_exists('exec') )
        {
            $shellCommand->useExec = true;
        }

        // Execute command
        $is_success = $shellCommand->execute();

        // Delete any temp connection files that might have been created.
        $is_deleted = $schema->deleteDumpConfigFile();

        if ( ! $is_success )
        {
            $execCommand = $shellCommand->getExecCommand();
            throw new ShellCommandException($execCommand, $shellCommand->getExitCode(), $shellCommand->getStdErr());
        }

        // Generate backup in a ZIP?
        else if ( $is_zip )
        {
            $backup_file = Yii::app()->file->set($file_path);
            if ( $backup_file && $backup_file->getExists() && $backup_file->zip(true) )
            {
                $file_path .= '.zip';
            }
        }

        return $file_path;
    }


    /**
     * Returns the path for a new backup file.
     *
     * @return string
     */
    public function getBackupFilePath()
    {
        // Determine the backup file path
        $filename = Transliteration::file(Yii::app()->name) . '_'. date('YmdHi') .'_'. strtolower(StringHelper::random_string(10)) .'.sql';
        return Yii::app()->path->backupPath('db') . DIRECTORY_SEPARATOR . mb_strtolower($filename);
    }


    /**
     * Returns the raw database table names that should be ignored by default.
     *
     * @return array
     */
    public function getIgnoredBackupTables()
    {
        $vec_tables = [
            'user_session'
        ];

        if ( !empty($this->backupIgnoredTables) )
        {
            $vec_tables = \CMap::mergeArray($vec_tables, $this->backupIgnoredTables);
        }

        return $vec_tables;
    }


    // Private Methods
    // =========================================================================

    /**
     * Returns a table name without the table prefix
     *
     * @param string $table
     * @return string
     */
    private function _getTableNameWithoutPrefix($table)
    {
        if ($this->tablePrefix)
        {
            if (strpos($table, $this->tablePrefix) === 0)
            {
                $table = substr($table, strlen($this->tablePrefix));
            }
        }

        return $table;
    }


    /**
     * Parses a database backup/restore command for config tokens
     *
     * @param string $command The command to parse tokens in
     * @param string $file The path to the backup file
     * @return string
     */
    private function _parseCommandTokens($command, $file)
    {
        $vec_config = Yii::app()->config->getDb();
        $tokens = [
            '{file}'        => $file,
            '{port}'        => $vec_config['port'],
            '{server}'      => $vec_config['server'],
            '{user}'        => $vec_config['username'],
            '{password}'    => addslashes(str_replace('$', '\\$', $vec_config['password'])),
            '{database}'    => $vec_config['database']
        ];

        return str_replace(array_keys($tokens), $tokens, $command);
    }
}
