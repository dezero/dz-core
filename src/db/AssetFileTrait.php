<?php
/**
 * AssetFileTrait contains shortcut methods to upload files (incluiding images)
 * 
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2019 Dezero
 */

namespace dz\db;

use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\asset\models\AssetFile;
use dz\modules\asset\models\AssetImage;

use Yii;

trait AssetFileTrait
{
    /**
     * @var AssetFile model
     */
    public $file_model;


    /**
     * @var bool. Is a temporary file?
     */
    public $is_temp = false;


    /**
     * Delete current image
     */
    public function delete_image($image_field = 'image_id')
    {
        return $this->_delete('image', $image_field);
    }


    /**
     * Delete current file
     */
    public function delete_file($file_field = 'file_id')
    {
        return $this->_delete('file', $file_field);
    }


    /**
     * Upload image
     */
    public function upload_image($image_field = 'image_id', $image_destination_path = '', $num_image = null, $is_temp = false)
    {
        // Recover previous image uploaded (single uploaded image)
        if ( $num_image === null && isset($_POST['AssetImage']) && isset($_POST['AssetImage']['file_id']) )
        {
            $image_model = AssetImage::findOne($_POST['AssetImage']['file_id']);

            // Delete current image
            if ( ! isset($_FILES['AssetImage']['name']['file_name']) && ! isset($_FILES['AssetFile']['name']['file_name']) && $image_model && $image_model->load_file() )
            {
                $image_model->delete();
            }
            else if ( isset($_FILES['AssetImage']['name']['file_name']) )
            {
                // No image has been changed
                if ( empty($_FILES['AssetImage']['name']['file_name']) )
                {
                    // Move file if file path has been changed too
                    if ( $image_model && $image_model->file_path !== $image_destination_path )
                    {
                        // Move & save AssetImage model
                        $image_model->setAttributes([
                            'entity_type'   => StringHelper::basename(get_class($this)),
                            'entity_id'     => !$this->isNewRecord ? $this->getPkValue() : null
                        ]);
                        if ( $image_model->move_image($image_destination_path) )
                        {
                            // Save "image_id" field
                            $this->{$image_field} = $image_model->file_id;
                            if ( !$this->isNewRecord )
                            {
                                $this->saveAttributes([$image_field => $image_model->file_id]);
                            }
                        }                        
                    }

                    return $image_model;
                }
                
                // New image has been uploaded (replaced)
                else
                {
                    // Delete previous image
                    if ( $image_model && $image_model->load_file() )
                    {
                        $image_model->delete();
                    }

                    return $this->save_image($image_field, $image_destination_path, $num_image, $is_temp);
                }
            }
        }


        // Recover previous image uploaded (multiple uploaded image)
        else if ( $num_image !== null && isset($_POST['AssetImage']) && isset($_POST['AssetImage'][$num_image]) && isset($_POST['AssetImage'][$num_image]['file_id']) )
        {
            $image_model = AssetImage::findOne($_POST['AssetImage'][$num_image]['file_id']);

            // Delete current image
            if ( ! isset($_FILES['AssetImage']['name'][$num_image]['file_name']) && ! isset($_FILES['AssetFile']['name'][$num_image]['file_name']) && $image_model && $image_model->load_file() )
            {
                $image_model->delete();
            }
            else if ( isset($_FILES['AssetImage']['name'][$num_image]['file_name']) )
            {
                // No image has been changed
                if ( empty($_FILES['AssetImage']['name'][$num_image]['file_name']) )
                {
                    // Move file if file path has been changed too
                    if ( $image_model && $image_model->file_path !== $image_destination_path )
                    {
                        // Move & save AssetImage model
                        $image_model->setAttributes([
                            'entity_type'   => StringHelper::basename(get_class($this)),
                            'entity_id'     => !$this->isNewRecord ? $this->getPkValue() : null
                        ]);
                        if ( $image_model->move_image($image_destination_path) )
                        {
                            // Save "image_id" field
                            $this->{$image_field} = $image_model->file_id;
                            if ( !$this->isNewRecord )
                            {
                                $this->saveAttributes([$image_field => $image_model->file_id]);
                            }
                        }                        
                    }

                    return $image_model;
                }
                
                // New image has been uploaded (replaced)
                else
                {
                    // Delete previous image
                    if ( $image_model && $image_model->load_file() )
                    {
                        $image_model->delete();
                    }

                    return $this->save_image($image_field, $image_destination_path, $num_image, $is_temp);
                }
            }
        }

        // Upload a new image
        else
        {
            return $this->save_image($image_field, $image_destination_path, $num_image, $is_temp);
        }
    }


    /**
     * Upload a file (not image)
     */
    public function upload_file($file_field = 'file_id', $file_destination_path = '', $num_file = null, $is_temp = false)
    {
        // Recover previous file uploaded (single uploaded file)
        if ( $num_file === null && isset($_POST['AssetFile']) && isset($_POST['AssetFile']['file_id']) )
        {
            $file_model = AssetFile::findOne($_POST['AssetFile']['file_id']);

            // Delete current file
            if ( ! isset($_FILES['AssetImage']['name']['file_name']) && ! isset($_FILES['AssetFile']['name']['file_name']) && $file_model && $file_model->load_file() )
            {
                $file_model->delete();
            }
            else if ( isset($_FILES['AssetFile']['name']['file_name']) )
            {
                // No file has been changed
                if ( empty($_FILES['AssetFile']['name']['file_name']) )
                {
                    // Move file if file path has been changed too
                    if ( $file_model && $file_model->file_path !== $file_destination_path )
                    {
                        // Move & save AssetFile model
                        $file_model->setAttributes([
                            'entity_type'   => StringHelper::basename(get_class($this)),
                            'entity_id'     => !$this->isNewRecord ? $this->getPkValue() : null
                        ]);
                        if ( $file_model->move($file_destination_path) && $file_field !== null && $this->hasAttribute($file_field) )
                        {
                            // Save "file_id" field
                            $this->{$file_field} = $file_model->file_id;
                            if ( !$this->isNewRecord )
                            {
                                $this->saveAttributes([$file_field => $file_model->file_id]);
                            }
                        }                  
                    }

                    return $file_model;
                }
                
                // New file has been uploaded (replaced)
                else
                {
                    // Delete previous file
                    if ( $file_model && $file_model->load_file() )
                    {
                        $file_model->delete();
                    }

                    return $this->save_file($file_field, $file_destination_path, $num_file, $is_temp);
                }
            }
        }

        // Recover previous file uploaded (multiple uploaded file)
        else if ( $num_file !== null && isset($_POST['AssetFile']) && isset($_POST['AssetFile'][$num_file]) && isset($_POST['AssetFile'][$num_file]['file_id']) )
        {
            $file_model = AssetFile::findOne($_POST['AssetFile'][$num_file]['file_id']);

            // Delete current file
            if ( ! isset($_FILES['AssetImage']['name'][$num_file]['file_name']) && ! isset($_FILES['AssetFile']['name'][$num_file]['file_name']) && $file_model && $file_model->load_file() )
            {
                $file_model->delete();
            }
            else if ( isset($_FILES['AssetFile']['name'][$num_file]['file_name']) )
            {
                // No file has been changed
                if ( empty($_FILES['AssetFile']['name'][$num_file]['file_name']) )
                {
                    // Move file if file path has been changed too
                    if ( $file_model && $file_model->file_path !== $file_destination_path )
                    {
                        // Move & save AssetFile model
                        $file_model->setAttributes([
                            'entity_type'   => StringHelper::basename(get_class($this)),
                            'entity_id'     => !$this->isNewRecord ? $this->getPkValue() : null
                        ]);
                        if ( $file_model->move($file_destination_path) && $file_field !== null && $this->hasAttribute($file_field) )
                        {
                            // Save "file_id" field
                            $this->{$file_field} = $file_model->file_id;
                            if ( !$this->isNewRecord )
                            {
                                $this->saveAttributes([$file_field => $file_model->file_id]);
                            }
                        }                        
                    }

                    return $file_model;
                }
                
                // New file has been uploaded (replaced)
                else
                {
                    // Delete previous file
                    if ( $file_model && $file_model->load_file() )
                    {
                        $file_model->delete();
                    }

                    return $this->save_file($file_field, $file_destination_path, $num_file, $is_temp);
                }
            }
        }

        // Upload a new file
        else
        {
            return $this->save_file($file_field, $file_destination_path, $num_file, $is_temp);
        }
    }


    /**
     * Create a temporary image for the first time.
     * Useful if a file has been uplodaded but model has some error
     */
    public function upload_temp_image($image_field = 'image_id', $image_destination_path = '', $num_image = null)
    {
        return $this->upload_image($image_field, $image_destination_path, $num_image, true);
    }


    /**
     * Create a temporary file for the first time.
     * Useful if a file has been uplodaded but model has some error
     */
    public function upload_temp_file($file_field = 'file_id', $file_destination_path = '', $num_file = null)
    {
        return $this->upload_file($file_field, $file_destination_path, $num_file, true);
    }


    /**
     * Save image via AssetImage model
     */
    public function save_image($image_field = 'image_id', $image_destination_path = '', $num_image = null, $is_temp = false)
    {
        // Build a new AssetImage model
        $this->file_model = Yii::createObject(AssetImage::class);
        $this->file_model->setAttributes([
            'entity_type'   => StringHelper::basename(get_class($this)),
            'entity_id'     => !$this->isNewRecord ? $this->getPkValue() : null
        ]);

        // Upload a file into TEMP directory
        if ( $is_temp )
        {
            $this->file_model->is_temp = true;
            $temp_path = $this->file_model->getTempPath() . DIRECTORY_SEPARATOR;
            if ( empty($image_destination_path) )
            {
                $image_destination_path = $temp_path;
            }
        }

        // By default, www/files/images/
        else if ( empty($image_destination_path) )
        {             
            $image_destination_path = $this->file_model->getImagesPath() . DIRECTORY_SEPARATOR;
        }

        if ( $this->file_model->save_asset_image($image_destination_path, '', '', $num_image) )
        {
            // Delete old image
            if ( $this->{$image_field} !== null )
            {
                $this->delete_image($image_field);
            }
            
            $this->{$image_field} = $this->file_model->file_id;
            if ( !$this->isNewRecord )
            {
                $this->saveAttributes([$image_field => $this->file_model->file_id]);
            }
            
            return $this->file_model;
        }

        return false;
    }


    /**
     * Save file via AssetFile model
     */
    public function save_file($file_field = 'file_id', $file_destination_path = '', $num_file = null, $is_temp = false)
    {
        // Build a new AssetFile model
        $this->file_model = Yii::createObject(AssetFile::class);
        $this->file_model->setAttributes([
            'entity_type'   => StringHelper::basename(get_class($this)),
            'entity_id'     => !$this->isNewRecord ? $this->getPkValue() : null
        ]);

        // Upload a file into TEMP directory
        if ( $is_temp )
        {
            $this->file_model->is_temp = true;
            $temp_path = $this->file_model->getTempPath() . DIRECTORY_SEPARATOR;
            if ( empty($file_destination_path) )
            {
                $file_destination_path = $temp_path;
            }
        }

        // By default, www/files/
        else if ( empty($file_destination_path) )
        {             
            $file_destination_path = $this->file_model->getFilesPath() . DIRECTORY_SEPARATOR;
        }

        if ( $this->file_model->save_asset_file($file_destination_path, '', '', $num_file) )
        {
            if ( $file_field !== null && $this->hasAttribute($file_field) )
            {
                // Delete old file
                if ( $this->{$file_field} !== null )
                {
                    $this->delete_file($file_field);
                }
                
                $this->{$file_field} = $this->file_model->file_id;
                if ( !$this->isNewRecord )
                {
                    $this->saveAttributes([$file_field => $this->file_model->file_id]);
                }
            }
            
            return $this->file_model;
        }

        return false;
    }


    /**
     * Save a AssetFile model given a destination file path
     */
    public function save_file_from_path($file_field, $file_destination_path)
    {
        // Build a new AssetFile model
        $this->file_model = Yii::createObject(AssetFile::class);
        $this->file_model->setAttributes([
            'entity_type'   => StringHelper::basename(get_class($this)),
            'entity_id'     => !$this->isNewRecord ? $this->getPkValue() : null
        ]);

        if ( $this->file_model->save_asset_file_from_path($file_destination_path) )
        {
            if ( $file_field !== null && $this->hasAttribute($file_field) )
            {
                // Delete old file
                if ( $this->{$file_field} !== null )
                {
                    $this->delete_file($file_field);
                }
                
                $this->{$file_field} = $this->file_model->file_id;
                if ( !$this->isNewRecord )
                {
                    $this->saveAttributes([$file_field => $this->file_model->file_id]);
                }
            }
            
            return $this->file_model;
        }

        return false;
    }


    /**
     * Save a AssetImage model given a destination file path
     */
    public function save_image_from_path($image_field, $image_destination_path)
    {
        // Build a new AssetImage model
        $this->file_model = Yii::createObject(AssetImage::class);
        $this->file_model->setAttributes([
            'entity_type'   => StringHelper::basename(get_class($this)),
            'entity_id'     => !$this->isNewRecord ? $this->getPkValue() : null
        ]);

        if ( $this->file_model->save_asset_image_from_path($image_destination_path) )
        {
            // Delete old image
            if ( $this->{$image_field} !== null )
            {
                $this->delete_image($image_field);
            }
            
            $this->{$image_field} = $this->file_model->file_id;
            if ( !$this->isNewRecord )
            {
                $this->saveAttributes([$image_field => $this->file_model->file_id]);
            }
            
            return $this->file_model;
        }

        return false;
    }


    /**
     * Check if some file has been uplodaded
     */
    public function is_uploaded_file()
    {
        // New uplodaded file?
        $que_file = null;
        if ( isset($_FILES['AssetFile']['name']['file_name']) )
        {
            $que_file = Yii::app()->file->set('AssetFile[file_name]');
        }
        $is_uploaded_file = !empty($que_file) && $que_file->isFile;

        // Uploaded file was added before (TEMP FILE)?
        if ( ! $is_uploaded_file && isset($_POST['AssetFile']) && isset($_POST['AssetFile']['file_id']) )
        {
            $file_model = AssetFile::findOne($_POST['AssetFile']['file_id']);

            // Delete file?
            if ( ! isset($_FILES['AssetFile']['name']['file_name']) && ! isset($_FILES['AssetImage']['name']['file_name']) && $file_model && $file_model->load_file() )
            {
                return false;
            }
            else if ( $file_model )
            {
                return true;
            }
        }

        return $is_uploaded_file;
    }


    /**
     * Check if some image file has been uplodaded
     */
    public function is_uploaded_image()
    {
        $que_file = null;
        if ( isset($_FILES['AssetImage']['name']['file_name']) )
        {
            $que_file = Yii::app()->file->set('AssetImage[file_name]');
        }
        $is_uploaded_image = !empty($que_file) && $que_file->isFile;

        // Uploaded image was added before (TEMP FILE)?
        if ( ! $is_uploaded_image && isset($_POST['AssetImage']) && isset($_POST['AssetImage']['file_id']) )
        {
            $image_model = AssetImage::findOne($_POST['AssetImage']['file_id']);

            // Delete file?
            if ( ! isset($_FILES['AssetFile']['name']['file_name']) && ! isset($_FILES['AssetImage']['name']['file_name']) && $image_model && $image_model->load_file() )
            {
                return false;
            }
            else if ( $image_model )
            {
                return true;
            }
        }

        return $is_uploaded_image;
    }


    /**
     * File mode
     */
    public function get_file_model()
    {
        return $this->file_model;
    }


    /**
     * File errors
     */
    public function get_file_errors()
    {
        return !empty($this->file_model) ? $this->file_model->getErrors() : [];
    }


    // Private Methods
    // =========================================================================

    /**
     * Detele file process
     */
    private function _delete($asset_type = 'image', $file_field = 'file_id')
    {
        if ( !empty($file_field) )
        {
            if ( $asset_type == 'image' )
            {
                $file_model = AssetImage::findOne($this->{$file_field});
            }
            else
            {
                $file_model = AssetFile::findOne($this->{$file_field});
            }

            if ( $file_model && $file_model->load_file() )
            {
                // Delete file
                $file_model->delete();

                return true;
            }
        }

        return false;
    }
}
