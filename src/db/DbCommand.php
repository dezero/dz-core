<?php
/**
 * DbConnection class file
 * 
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2019 Dezero
 */

namespace dz\db;

class DbCommand extends \CDbCommand
{
    /**
     * Creates and executes a SQL statement for dropping a DB table, if it exists.
     *
     * @param string $table The table to be dropped. The name will be properly quoted by the method.
     */
    public function dropTableIfExists($table)
    {
        return $this->setText($this->getConnection()->getSchema()->dropTableIfExists($table))->execute();
    }
}