<?php
/**
 * MsActiveRecord class file (Microsoft SQL Server)
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2016 Dezero
 */

namespace dz\db;

use Yii;

/**
 * MsActiveRecord is the base class for the generated AR (base) models
 * from Microsoft SQL Server database
 */
abstract class MsActiveRecord extends \GxActiveRecord
{
    /**
     * Attributes are saved here before they are changed
     *
     * @var array
     */
    private $_oldAttributes = array();


    /**
     * Attributos en raw (sin pasar por beforeFind/afterFind methods events)
     *
     * @var array
     */ 
    public $raw_attributes;


    /**
     * MSSQL DB CdbConnection
     */
	private static $mssql_db = null;

 
    protected static function getMssqlDbConnection()
    {
        if (self::$mssql_db !== null)
        {
            return self::$mssql_db;
        }

        self::$mssql_db = Yii::app()->mssql_db;
        if (self::$mssql_db instanceof CDbConnection)
        {
            Yii::app()->db->setActive(false);
            self::$mssql_db->setActive(true);
            return self::$mssql_db;
        }
        else
        {
            throw new \CDbException(Yii::t('yii','Active Record requires a "db" CDbConnection application component.'));
        }
    }


    /**
     * Initializes this model
     */
    public function init()
    {
        $this->attachEventHandler("onAfterFind", array($this, 'dzAfterFind'));
        return parent::init();
    }


    /**
     * Custom After Find event
     */
    protected function dzAfterFind($event)
    {
        $event->sender->oldAttributes = $event->sender->Attributes;
    }


    /**
     * Save old attributes values
     */
    public function setOldAttributes($value)
    {
        $this->_oldAttributes = $value;
        $this->raw_attributes = $value;
    }


    /**
     * Return old attributes values
     */
    public function getOldAttributes()
    {
        return $this->_oldAttributes;
    }


    /**
     * Número de fltros seleccionados de la búsqueda avanzada
     *
     * @return bool
     */
    public function selectedFilter()
    {
        return 0;
    }


    /**
     * Return search result items for an AJAX search request
     *
     * @param   mixed   $query              Query term to search in model fields
     * @param   array   $vec_columns        Columns where search will be applied
     * @param   array   $vec_options        Options
     * @return  array   Collection of models
     */
    public function ajaxSearchResult($query, $vec_columns, $vec_options = array())
    {
        $criteria = new \CDbCriteria();
        $pk_column_name = self::model(get_class($this))->getTableSchema()->primaryKey;
        
        // Condition defined in vec_options argument
        $criteria->condition = '';
        if ( isset($vec_options['condition']) )
        {
            $criteria->condition = '('. $vec_options['condition'] .') AND ';
        }
        
        // Search by ID, it means search by Primary Key "ID"
        if ( is_numeric($query) )
        {
            // $criteria->condition .= $pk_column_name ." LIKE '". $query ."%'";
            $criteria->condition .= $pk_column_name ." = ". $query;
            if ( !isset($vec_options['order']) )
            {
                $vec_options['order'] = $pk_column_name.' ASC';
            }
        }
        
        // Search by string in $vec_columns fields
        else
        {
            $criteria->order = '';
            $search_conditions = '';
            foreach ( $vec_columns as $que_column )
            {
                // $criteria->compare($que_column, $query, TRUE);
                if ( !empty($search_conditions) )
                {
                    $search_conditions .= ' OR ';
                }
                $search_conditions .= $que_column ." LIKE '". $query ."%' OR ". $que_column ." LIKE '%". $query ."%'";
                
                if ( !empty($criteria->order) )
                {
                    $criteria->order .= ', ';
                }
                $criteria->order .= "CASE WHEN ". $que_column ." LIKE '". $query ."%' THEN 0 ELSE 1 END, CASE WHEN ". $que_column ." LIKE '%". $query ."%' THEN 0 ELSE 1 END";
            }
            $criteria->condition .= '('. $search_conditions .')';
        }
        
        // Limit result
        if ( !isset($vec_options['limit']) )
        {
            $vec_options['limit'] = 10;
        }
        $criteria->limit = $vec_options['limit'];
        
        return $this->findAll($criteria);
    }
}