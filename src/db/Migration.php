<?php
/**
 * Migration class file for Dz Framework
 *
 * CDbMigration extension. ase class for representing a database migration
 * http://www.yiiframework.com/doc/api/1.1/CDbMigration
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2015 Fabián Ruiz
 */

namespace dz\db;

use Yii;

abstract class Migration extends \CDbMigration
{
    use SchemaBuilderTrait;

	/**
	 * Execute SQL file
	 */
	public function execute_sql_file($sql_file_path)
	{
		$sql_file = Yii::app()->file->set($sql_file_path);

		if ( $sql_file->getExists() )
		{
			$sql_content = $sql_file->getContents();
			return $this->execute($sql_content);
		}
	
		return FALSE;
	}


    /**
     * Creates and executes a SQL statement for dropping a DB table, if it exists.
     *
     * @param string $table The table to be dropped. The name will be properly quoted by the method.
     */
    public function dropTableIfExists($table, $disableCheckIntegrity = false)
    {
        // Disable "checkIntegrity" temporally
        if ( $disableCheckIntegrity )
        {
            $this->getDbConnection()->getSchema()->checkIntegrity(false);
        }

        echo "    > dropping $table if it exists ...";
        $time = microtime(true);
        $this->getDbConnection()->createCommand()->dropTableIfExists($table);
        echo ' done (time: ' . sprintf('%.3f', microtime(true) - $time) . "s)\n";

        // Enable "checkIntegrity" again
        if ( $disableCheckIntegrity )
        {
            $this->getDbConnection()->getSchema()->checkIntegrity(true);
        }
    }


    /**
     * @inheritdoc
     * 
     * @param string|null $name the name of the primary key constraint. If null, a name will be automatically generated.
     * @param string $table the table that the primary key constraint will be added to.
     * @param string|array $columns comma separated string or array of columns that the primary key will consist of.
     */
    public function addPrimaryKey($name, $table, $columns)
    {
        if ($name === null)
        {
            $name = $this->getDbConnection()->getPrimaryKeyName($table, $columns);
        }

        return parent::addPrimaryKey($name, $table, $columns);
    }


    /**
     * @inheritdoc
     * 
     * @param string|null $name the name of the foreign key constraint. If null, a name will be automatically generated.
     * @param string $table the table that the foreign key constraint will be added to.
     * @param string|array $columns the name of the column to that the constraint will be added on. If there are multiple columns, separate them with commas or use an array.
     * @param string $refTable the table that the foreign key references to.
     * @param string|array $refColumns the name of the column that the foreign key references to. If there are multiple columns, separate them with commas or use an array.
     * @param string $delete the ON DELETE option. Most DBMS support these options: RESTRICT, CASCADE, NO ACTION, SET DEFAULT, SET NULL
     * @param string $update the ON UPDATE option. Most DBMS support these options: RESTRICT, CASCADE, NO ACTION, SET DEFAULT, SET NULL
     */
    public function addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete = null, $update = null)
    {
        if ($name === null)
        {
            $name = $this->getDbConnection()->getForeignKeyName($table, $columns);
        }

        return parent::addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete, $update);
    }


    /**
     * @inheritdoc
     * 
     * @param string|null $name the name of the index. The name will be properly quoted by the method. If null, a name will be automatically generated.
     * @param string $table the table that the new index will be created for. The table name will be properly quoted by the method.
     * @param string|array $columns the column(s) that should be included in the index. If there are multiple columns, please separate them
     * by commas or use an array. Each column name will be properly quoted by the method. Quoting will be skipped for column names that
     * include a left parenthesis "(".
     * @param bool $unique whether to add UNIQUE constraint on the created index.
     */
    public function createIndex($name, $table, $columns, $unique = false)
    {
        if ($name === null)
        {
            $name = $this->getDbConnection()->getIndexName($table, $columns, $unique);
        }

        return parent::createIndex($name, $table, $columns, $unique);
    }
}