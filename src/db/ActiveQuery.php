<?php
/**
 * ActiveQuery class file for Dz Framework
 * 
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2019 Dezero
 */

namespace dz\db;

class ActiveQuery extends \CComponent
{
    /**
     * @var string the name of the ActiveRecord class.
     */
    public $modelClass;


    /**
     * @var dz\db\DbCriteria
     */
    public $criteria;


    /**
     * @var array the columns being selected. For example, `['id', 'name']`.
     * This is used to construct the SELECT clause in a SQL statement. If not set, it means selecting all columns.
     * @see select()
     */
    public $select;


    /**
     * @var bool whether to select distinct rows of data only. If this is set true,
     * the SELECT clause would be changed to SELECT DISTINCT.
     */
    public $distinct;


    /**
     * @var array how to join with other tables
     */
    public $join;


    /**
     * @var array a list of relations that this query should be performed with
     */
    public $with;


    /**
     * @var string|array query condition. This refers to the WHERE clause in a SQL statement.
     * For example, `['age' => 31, 'team' => 1]`.
     * 
     * @see where() for valid syntax on specifying this value.
     */
    public $where;


    /**
     * @var int maximum number of records to be returned
     * If not set or less than 0, it means no limit.
     */
    public $limit;
    

    /**
     * @var int zero-based offset from where the records are to be returned.
     * If not set or less than 0, it means starting from the beginning.
     */
    public $offset;


    /**
     * @var array how to sort the query results.
     * This is used to construct the ORDER BY clause in a SQL statement.
     */
    public $orderBy;


    /**
     * @var array list of query parameter values indexed by parameter placeholders.
     * For example, `[':name' => 'Dan', ':age' => 31]`.
     */
    public $params = [];


    /**
     * @var Return the query results in terms of arrays instead of Active Records.
     */
    public $asArray = false;


    /**
     * Constructor.
     * @param array $data criteria initial property values (indexed by property name)
     */
    public function __construct($modelClass)
    {
        $this->modelClass = $modelClass;
    }


    /**
     * Sets the SELECT part of the query.
     *
     * @param string|array $columns the columns to be selected.
     *
     * @param string|array $condition the conditions that should be put in the WHERE part.
     * @param array $params the parameters (name => value) to be bound to the query.
     * @return $this the query object itself
     * @see andWhere()
     * @see orWhere()
     */
    public function select($columns)
    {
        $this->select = $columns;
        return $this;
    }


    /**
     * Sets the value indicating whether to SELECT DISTINCT or not.
     * 
     * @param bool $value whether to SELECT DISTINCT or not.
     * @return $this the query object itself
     */
    public function distinct($value = true)
    {
        $this->distinct = $value;
        return $this;
    }


    /**
     * Specifies the relations with which this query should be performed.
     * 
     * @param string $relation relation name
     * @return $this the query object itself
     */
    public function with($relation)
    {
        if ( empty($this->with) )
        {
            $this->with = [];
        }

        $this->with[] = $relation;

        return $this;
    }


    /**
     * Appends a JOIN part to the query
     * 
     * @param string $join refers to the JOIN clause in an SQL statement
     * @return $this the query object itself
     */
    public function join($join)
    {
        if ( empty($this->join) )
        {
            $this->join = '';
        }
        else
        {
            $this->join .= ' ';
        }

        $this->join .= $join;

        return $this;
    }


    /**
     * Sets the WHERE part of the query.
     *
     * The method requires a `$condition` parameter, and optionally a `$params` parameter
     * specifying the values to be bound to the query.
     *
     * The `$condition` parameter should be either a string (e.g. `'id=1'`) or an array.
     *
     * {@inheritdoc}
     *
     * @param string|array $condition the conditions that should be put in the WHERE part.
     * @param array $params the parameters (name => value) to be bound to the query.
     * @return $this the query object itself
     * @see andWhere()
     * @see orWhere()
     */
    public function where($condition, $params = [])
    {
        $this->where = $condition;
        $this->addParams($params);
        return $this;
    }



    /**
     * Adds an additional WHERE condition to the existing one.
     * The new condition and the existing one will be joined using the `AND` operator.
     * 
     * @param string|array $condition the new WHERE condition. Please refer to [[where()]]
     * on how to specify this parameter.
     * @param array $params the parameters (name => value) to be bound to the query.
     * @return $this the query object itself
     * 
     * @see where()
     * @see orWhere()
     */
    public function andWhere($condition, $params = [])
    {
        if ( $this->where === null )
        {
            $this->where = $condition;
        }
        elseif ( is_array($this->where) && isset($this->where[0]) && strcasecmp($this->where[0], 'and') === 0 )
        {
            $this->where[] = $condition;
        }
        else
        {
            $this->where = ['and', $this->where, $condition];
        }
        $this->addParams($params);
        return $this;
    }



    /**
     * Adds an additional WHERE condition to the existing one.
     * The new condition and the existing one will be joined using the `OR` operator.
     * 
     * @param string|array $condition the new WHERE condition. Please refer to [[where()]]
     * on how to specify this parameter.
     * @param array $params the parameters (name => value) to be bound to the query.
     * @return $this the query object itself
     * 
     * @see where()
     * @see andWhere()
     */
    public function orWhere($condition, $params = [])
    {
        if ( $this->where === null )
        {
            $this->where = $condition;
        }
        else
        {
            $this->where = ['or', $this->where, $condition];
        }
        $this->addParams($params);
        return $this;
    }


    /**
     * Sets the parameters to be bound to the query.
     *
     * @param array $params list of query parameter values indexed by parameter placeholders.
     * For example, `[':name' => 'Dan', ':age' => 31]`.
     * @return $this the query object itself
     * @see addParams()
     */
    public function params($params)
    {
        $this->params = $params;
        return $this;
    }


    /**
     * Adds additional parameters to be bound to the query.
     * 
     * @param array $params list of query parameter values indexed by parameter placeholders.
     * For example, `[':name' => 'Dan', ':age' => 31]`.
     * @return $this the query object itself
     * @see params()
     */
    public function addParams($params)
    {
        if ( !empty($params) )
        {
            if ( empty($this->params) )
            {
                $this->params = $params;
            }
            else
            {
                foreach ( $params as $name => $value )
                {
                    if ( is_int($name) )
                    {
                        $this->params[] = $value;
                    }
                    else
                    {
                        $this->params[$name] = $value;
                    }
                }
            }
        }

        return $this;
    }


    /**
     * Sets the LIMIT part of the query.
     * @param int|null $limit the limit
     * @return $this the query object itself
     */
    public function limit($limit)
    {
        $this->limit = $limit;
        return $this;
    }


    /**
     * Sets the OFFSET part of the query.
     * @param int|null $offset the offset
     * @return $this the query object itself
     */
    public function offset($offset)
    {
        $this->offset = $offset;
        return $this;
    }


    /**
     * Sets the ORDER BY part of the query.
     * @param string|null $orderBy the orderBy
     * @return $this the query object itself
     */
    public function orderBy($orderBy)
    {
        $this->orderBy = $orderBy;
        return $this;
    }


    /**
     * Alias of orderBy method
     * @see orderBy()
     */
    public function order($orderBy)
    {
        return $this->orderBy($orderBy);
    }


    /**
     * Executes query and returns a single row of result.
     * 
     * @return ActiveRecord|array|null a single row of query result.
     */
    public function one()
    {
        // FORCE limit to 1
        $this->limit = 1;
        
        $modelClass = $this->modelClass;

         // Return results as ActiveRecord[]
        if ( ! $this->asArray )
        {
            return $modelClass::model()->find($this->buildCriteria());    
        }
        
        // Return results as array
        $tableSchema = $modelClass::model()->getTableSchema();
        $command = $modelClass::model()->getCommandBuilder()->createFindCommand($tableSchema, $this->buildCriteria());
        return $command->queryRow();
    }


    /**
     * Executes query and returns all results as an array
     * 
     * @return array|ActiveRecord[] the query results. If the query results in nothing, an empty array will be returned.
     */
    public function all()
    {
        $modelClass = $this->modelClass;

        // Return results as ActiveRecord[]
        if ( ! $this->asArray )
        {
            return $modelClass::model()->findAll($this->buildCriteria());
        }

        // Return results as array
        $tableSchema = $modelClass::model()->getTableSchema();
        $command = $modelClass::model()->getCommandBuilder()->createFindCommand($tableSchema, $this->buildCriteria());
        return $command->queryAll();
    }



    /**
     * Returns the query result as a scalar value.
     * The value returned will be the first column in the first row of the query results.
     * 
     * @param string|array $columns the columns to be selected. Optional
     *
     * @return string|null|false the value of the first column in the first row of the query result.
     * False is returned if the query result is empty.
     */
    public function scalar($columns = '')
    {
        $modelClass = $this->modelClass;

        if ( !empty($columns) )
        {
            $this->select($columns);
        }

        $tableSchema = $modelClass::model()->getTableSchema();
        $command = $modelClass::model()->getCommandBuilder()->createFindCommand($tableSchema, $this->buildCriteria());
        return $command->queryScalar();
    }


    /**
     * Executes the query and returns the first column of the result.
     * 
     * @param string|array $columns the columns to be selected. Optional
     *
     * @return array the first column of the query result. An empty array is returned if the query results in nothing.
     */
    public function column($columns = '')
    {
        $modelClass = $this->modelClass;

        if ( !empty($columns) )
        {
            $this->select($columns);
        }

        $tableSchema = $modelClass::model()->getTableSchema();
        $command = $modelClass::model()->getCommandBuilder()->createFindCommand($tableSchema, $this->buildCriteria());
        return $command->queryColumn();
    }


    /**
     * Returns the number of records.
     *
     * @return int|string number of records.
     */
    public function count()
    {
        $modelClass = $this->modelClass;
        return $modelClass::model()->count($this->buildCriteria());
    }


    /**
     * Returns a value indicating whether the query result contains any row of data.
     * 
     * @return bool whether the query result contains any row of data.
     */
    public function exists()
    {
        return (bool) $this->scalar();
    }


    /**
     * Sets the [[asArray]] property.
     * 
     * @param bool $value whether to return the query results in terms of arrays instead of Active Records.
     * @return $this the query object itself
     */
    public function asArray($value = true)
    {
        $this->asArray = $value;
        return $this;
    }


    /**
     * Build DbCriteria object
     */
    public function buildCriteria()
    {
        $this->criteria = new DbCriteria;

        // Build SELECT
        if ( $this->distinct !== null )
        {
            $this->criteria->distinct = $this->distinct;
        }
        if ( $this->select !== null )
        {
            $this->criteria->select = $this->select;
        }

        // Build WITH relations
        if ( $this->with !== null )
        {
            $this->criteria->with = $this->with;
        }

        // Build JOIN
        if ( $this->join !== null )
        {
            $this->criteria->join = $this->join;
        }

        // Build WHERE clause
        if ( $this->where !== null )
        {
            // ARRAY conditions -> AND
            if ( is_array($this->where) && isset($this->where[0]) && ( $this->where[0] === 'and' || $this->where[0] === 'or' ) )
            {
                foreach ( $this->where as $num_condition => $condition )
                {
                    if ( $num_condition > 0 )
                    {
                        $this->buildWhere($condition, strtoupper($this->where[0]));
                    }
                }
            }

            else
            {
                $this->buildWhere($this->where);
            }
        }

        // Build PARAMS
        if ( $this->params !== null )
        {
            $this->criteria->addParams($this->params);
        }

        // Built ORDER BY
        if ( $this->orderBy !== null )
        {
            $this->criteria->order = $this->orderBy;
        }

        // Build LIMIT
        if ( $this->limit !== null )
        {
            $this->criteria->limit = $this->limit;
        }

        // Build OFFSET
        if ( $this->offset !== null )
        {
            $this->criteria->offset = $this->offset;
        }

        return $this->criteria;
    }


    /**
     * @param string|array $condition
     * @return string the WHERE clause
     */
    public function buildWhere($condition, $condition_operator = 'AND')
    {
        if ( $condition )
        {
            // ARRAY conditions
            if ( is_array($condition) )
            {
                foreach ( $condition as $column => $value )
                {
                    if ( is_array($value) )
                    {
                        // Operator format: ['operator', 'column1', 'value1']
                        if ( count($value) == 3 AND preg_match('/(like|<>|>=|>|<=|<|=)/', $value[0]))
                        {
                            $current_operator = $value[0];

                            // LIKE condition
                            if ( $operator === 'like' OR $operator === '%')
                            {
                                $this->criteria->compare($value[1], $value[2], '%', false, $condition_operator);
                            }
                            else
                            {
                                $this->criteria->compare($value[1], $operator .' '. $value[2], false, $condition_operator);
                            }
                        }

                        // IN condition format: 'column1' => ['value1', 'value2', ...]
                        else if ( !is_int($column) )
                        {
                            $this->criteria->addInCondition($column, $value, $condition_operator);
                        }
                    }

                    // HASH format: 'column1' => 'value1', 'column2' => 'value2', ...
                    else
                    {
                        $this->criteria->compare($column, $value, false, $condition_operator);
                    }
                }
            }

            // Direct SQL condition
            else
            {
                $this->criteria->addCondition($condition, $condition_operator);
            }
        }
    }
}
