<?php
/**
 * ActiveRecord class file for Dz Framework
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2019 Dezero
 */

namespace dz\db;

use dz\db\ActiveQuery;
use dz\db\ActiveRecordMetaData;
use dz\db\AssetFileTrait;
use dz\db\DbCriteria;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use Yii;

/**
 * ActiveRecord is the base class for the generated AR (base) models.
 */
abstract class ActiveRecord extends \GxActiveRecord
{
    /**
     * Trait class with methods for working with files
     */
    use AssetFileTrait;


	/**
	 * @var array Attributes are saved here before they are changed
	 */
	private $_oldAttributes = [];


    /**
     * @var array ActiveRecordMetaData objects
     */
    private static $_md = [];


	/**
	 * @var array Raw attributes (without beforeFind/afterFind methods events)
	 */	
	public $raw_attributes;

	
	/**
	 * Filtro por "Mostrar Bajas" en CGridView
	 *
     *    0 => 'Mostrar TODOS'
	 *    1 => 'Mostrar solo BAJAS'
	 *	  2 => 'Mostrar solo ALTAS'
	 *
	 * @var int
	 */	
	public $disable_filter = 0;


	/**
	 * Filtro de búsqueda global en CGridView
	 *
	 * @var string
	 */	
	public $global_search_filter;


	/**
	 * Mensajes generados en los eventos afterSave, afterValidate y beforeValidate
	 *
	 * @var array
	 */
	public $event_message = [];


	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model(Yii::resolveClass($className));
	}
	
	
	/**
	 * Initializes this model
	 */
	public function init()
	{
		$this->attachEventHandler("onAfterFind", [$this, 'dzAfterFind']);
		return parent::init();
	}


	/**
	 * Return search result items for an AJAX search request
	 *
	 * @param 	mixed 	$query 				Query term to search in model fields
	 * @param 	array 	$vec_columns 		Columns where search will be applied
	 * @param 	array 	$vec_options 		Options
	 * @return 	array 	Collection of models
	 */
	public function ajaxSearchResult($query, $vec_columns, $vec_options = [])
	{
		$criteria = new DbCriteria();
		$pk_column_name = self::model(get_class($this))->getTableSchema()->primaryKey;
		
		// Condition defined in vec_options argument
		$criteria->condition = '';
		if ( isset($vec_options['condition']) )
		{
			$criteria->condition = '('. $vec_options['condition'] .') AND ';
		}
		
		// Search by ID, it means search by Primary Key "ID"
		if ( isset($vec_options['search_by_pk']) AND $vec_options['search_by_pk'] AND is_numeric($query) )
		{
			// $criteria->condition .= $pk_column_name ." LIKE '". $query ."%'";
			$criteria->condition .= $pk_column_name ." = ". $query;
			if ( !isset($vec_options['order']) )
			{
				$vec_options['order'] = $pk_column_name.' ASC';
			}
		}
		
		// Search by string in $vec_columns fields
		else
		{
			$criteria->order = '';
			$search_conditions = '';
			foreach ( $vec_columns as $que_column )
			{
				// $criteria->compare($que_column, $query, true);
				if ( !empty($search_conditions) )
				{
					$search_conditions .= ' OR ';
				}
				$search_conditions .= $que_column ." LIKE '". $query ."%' OR ". $que_column ." LIKE '%". $query ."%'";
				
				if ( !empty($criteria->order) )
				{
					$criteria->order .= ', ';
				}
				$criteria->order .= "CASE WHEN ". $que_column ." LIKE '". $query ."%' THEN 0 ELSE 1 END, CASE WHEN ". $que_column ." LIKE '%". $query ."%' THEN 0 ELSE 1 END";
			}
			$criteria->condition .= '('. $search_conditions .')';
		}
		
		// Limit result
		if ( !isset($vec_options['limit']) )
		{
			$vec_options['limit'] = 10;
		}
		$criteria->limit = $vec_options['limit'];
		
		return $this->findAll($criteria);
	}
	
	
	/**
	 * The specified column(s) is(are) the responsible for the string representation of the model instance.
	 *
	 * @return string|array The name of the representing column for the table (string) or
	 * 						the names of the representing columns (array).
	 * @see __toString
	 */
	public static function representingColumn()
	{
		return null;
	}


	/**
	 * Returns a string representation of the model instance, based on {@link representingColumn}.
	 *
	 * If the representing column is not set, the primary key will be used.
	 * If there is no primary key, the first field will be used.
	 *
	 * @return string The string representation for the model instance.
	 * @see representingColumn
	 */
	public function __toString()
	{
		$representingColumn = $this->representingColumn();

		if ( ($representingColumn === null) || ($representingColumn === []) )
		{
			if ( $this->getTableSchema()->primaryKey !== null )
			{
				$representingColumn = $this->getTableSchema()->primaryKey;
			}
			else
			{
				$columnNames = $this->getTableSchema()->getColumnNames();
				$representingColumn = $columnNames[0];
			}
		}
		
		if ( is_array($representingColumn) )
		{
			$part = '';
			foreach ($representingColumn as $representingColumn_item)
			{
				$part .= ( $this->$representingColumn_item === null ? '' : $this->$representingColumn_item) . ' - ';
			}
			return substr($part, 0, -3);
		}
		else
		{
			return $this->$representingColumn === null ? '' : (string) $this->$representingColumn;
		}
	}
	
	
	/**
	 * External behaviors
	 */
	public function behaviors()
	{
	    return [
			// Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior', // '@core.src.behaviors.DateBehavior',
            ],
				
			// Remeber filters --> http://www.yiiframework.com/extension/remember-filters-gridview/
           'ERememberFiltersBehavior' => [
               'class' => '@lib.ERememberFiltersBehavior',
               'defaults' => [],                // optional line
               'defaultStickOnClear' => false   // optional line
           ],

           // UUID behaviors
            'UuidBehavior' => [
                'class' => '\dz\behaviors\UuidBehavior',
            ],
		];
	}


    /**
     * Returns the meta-data for this AR
     * @return CActiveRecordMetaData the meta for this AR class.
     */
    public function getMetaData()
    {
        $className = get_class($this);
        if( !array_key_exists($className,self::$_md) )
        {
            self::$_md[$className]=null; // preventing recursive invokes of {@link getMetaData()} via {@link __get()}
            self::$_md[$className] = new ActiveRecordMetaData($this);
        }
        return self::$_md[$className];
    }


    /**
     * Refreshes the meta data for this AR class.
     * By calling this method, this AR class will regenerate the meta data needed.
     * This is useful if the table schema has been changed and you want to use the latest
     * available table schema. Make sure you have called {@link CDbSchema::refresh}
     * before you call this method. Otherwise, old table schema data will still be used.
     */
    public function refreshMetaData()
    {
        $className = get_class($this);
        if ( array_key_exists($className,self::$_md) )
        {
            unset(self::$_md[$className]);
        }
    }


	/*
	|--------------------------------------------------------------------------
	| CUSTOM FUNCTIONS
	|--------------------------------------------------------------------------
	*/
		
	/**
	 * Total selected filters - Used in advanced search
	 *
	 * @return integer
	 */
	public function selectedFilter()
	{
		return 0;
	}
	
	
	/**
	 * DISTINCT value of a column field
	 *
	 * @return array
	 */
	public function findDistinct($column_name)
	{
		// Check if column exists
		$que_column = self::getTableSchema()->getColumn($column_name);
		if ( empty($que_column) )
		{
			return false;
		}
		
		$vec_result = self::findAll([
			'select' => 't.'. $column_name,
			'group' => 't.'. $column_name,
			'distinct' => 'TRUE'			
		]);
		
		$vec_values = [];
		if ( !empty($vec_result) )
		{
			foreach ( $vec_result as $que_result )
			{
				$vec_values[] = $que_result->$column_name;
			}
		}
		return $vec_values;
	}
	
	
	/**
	 * Custom After Find event
	 */
	protected function dzAfterFind($event)
	{
		$event->sender->oldAttributes = $event->sender->Attributes;
	}


	/**
	 * Save old attributes values
	 */
    public function setOldAttributes($value)
    {
        $this->_oldAttributes = $value;
        $this->raw_attributes = $value;
    }


	/**
	 * Return old attributes values
	 */
    public function getOldAttributes()
    {
        return $this->_oldAttributes;
    }


    /**
	 * Save a log message
	 */
    public function log($log_message, $category = 'dev')
    {
    	if ( is_array($log_message) )
    	{
    		$log_message = Json::encode($log_message);
    	}
    	
        Yii::log($log_message, "profile", $category);
    	return true;
    }


    /**
     * Log errors
     */
    public function log_errors()
    {
        Log::save_model_error($this);
    }


	/*
	|------------------------------------------------------------------------------------
	| EVENT MESSAGES
	|
	| Messages detected on afterSave(), afterValidate() or beforeValidate() events
	|------------------------------------------------------------------------------------
	*/


	/**
	 * Añade un mensaje de error desde los eventos afterSave, afterValidate o beforeValidate
	 */
	public function addEventError($vec_errors = [], $custom_message = '')
	{
		if ( !empty($custom_message) )
		{
			$vec_errors = \CMap::mergeArray([$custom_message], $vec_errors);
		}

		if ( !isset($this->event_message['error']) )
		{
			$this->event_message['error'] = $vec_errors;
		}
		else
		{
			$this->event_message['error'] = \CMap::mergeArray($this->event_message['errors'], $vec_errors);
		}

		return true;
	}


	/**
	 * Añade un mensaje desde los eventos afterSave, afterValidate o beforeValidate
	 *
	 * @param string  	Tipo de mensaje: [success, warning, info]
	 * @param string  	Mensaje
	 */
	public function addEventMessage($type, $custom_message = '')
	{
		// Error -> Use specific function
		if ( $type == 'error' )
		{
			if ( !is_array($custom_message) )
			{
				$custom_message = [$custom_message];
			}
			return $this->addEventError($custom_message);
		}
		
		if ( !isset($this->event_message[$type]) )
		{
			$this->event_message[$type] = $message;
		}
		else
		{
			$this->event_message[$type] .= " <br/> ". $message;
		}
		
		return true;
	}


	/**
	 * Devuelve los mensajes de error registrados desde los eventos afterSave, afterValidate o beforeValidate
	 */
	public function getEventErrors()
	{
		if ( isset($this->event_message['error']) )
		{
			return $this->event_message['error'];
		}

		return false;
	}


	/**
	 * Devuelve los mensajes de $type registrados desde los eventos afterSave, afterValidate o beforeValidate
	 */
	public function getEventMessages($type)
	{
		if ( isset($this->event_message[$type]) )
		{
			return $this->event_message[$type];
		}

		return false;
	}


	/*
	|--------------------------------------------------------------------------
	| HELPERs METHODS
	|--------------------------------------------------------------------------
	*/

	/**
	 * Get last inserted record. Only useful for tables with AUTOINCREMENTAL primary key
	 */
	function lastRecord()
	{
		$pk_column_name = self::model(StringHelper::basename(get_class($this)))->getTableSchema()->primaryKey;
		if ( !empty($pk_column_name) )
		{
			$criteria = new DbCriteria();
			$criteria->order = $pk_column_name .' DESC';
			$criteria->limit = 1;
			return $this->find($criteria);
		}
		return false;
	}


    /**
     * Get value (single PK) or values (composite PK) from primary key/s of current ActiveRecord
     */
    public function getPkValue()
    {
        if ( $this->getTableSchema()->primaryKey !== null )
        {
            $pk_attribute = $this->getTableSchema()->primaryKey;

            if ( is_array($pk_attribute) )
            {
                $part = '';
                foreach ($pk_attribute as $pk_attribute_item)
                {
                    $part .= ( $this->$pk_attribute_item === null ? '' : $this->$pk_attribute_item) . '-';
                }
                return substr($part, 0, -1);
            }
            else
            {
                return $this->$pk_attribute === null ? '' : (string) $this->$pk_attribute;
            }
        }
    }


	/**
	 * Check if a model is enabled
	 */
	public function is_enabled()
	{
		return empty($this->disable_date);
	}


    /**
     * Check if a model is disabled
     */
    public function is_disabled()
    {
        return !empty($this->disable_date);
    }


	/**
	 * Disables a model
	 *
	 * @return bool
	 */
	public function disable()
	{
		$this->scenario = 'disable';
		$this->disable_date = time();
		$this->disable_uid = (Yii::app() instanceof \CConsoleApplication) ? 1 : Yii::app()->user->id;

        // Set "is_disabled" attribute
        if ( $this->hasAttribute('is_disabled') )
        {
            $this->is_disabled = 1;
        }

		return $this->save();
	}


	/**
	 * Enables a model
	 *
	 * @return bool
	 */
	public function enable()
	{
		$this->scenario = 'enable';
		$this->disable_date = null;
		$this->disable_uid = null;

        // Set "is_disabled" attribute
        if ( $this->hasAttribute('is_disabled') )
        {
            $this->is_disabled = 0;
        }

		return $this->save();
	}


    /**
     * DbCriteria (query criteria) for results showed in a <SELECT> list
     *
     * @return DbCriteria
     */
    public function enabledCriteria($vec_params = [])
    {
        $criteria = new DbCriteria;
        $criteria->addCondition('disable_date IS NULL OR disable_date = 0');
        return $criteria;
    }


    /**
     * Find a model by UUID
     */
    public function findByUuid($uuid)
    {
        return self::get()->where(['uuid' => $uuid])->one();
    }


    /**
     * Find a model by alias
     */
    public function findByAlias($alias, $language_id = null)
    {
        if ( $language_id !== null )
        {
            // $language_id = Yii::defaultLanguage();

            return self::get()
                ->where([
                    'alias'         => $alias,
                    'language_id'   => $language_id
                ])
                ->one();
        }

        return self::get()->where(['alias' => $alias])->one();
    }


    /**
     * Return an array with SAFE attributes
     */
    public function getSafeAttributes()
    {
        $vec_output = [];

        $vec_safe_attributes = $this->getSafeAttributeNames();
        if ( !empty($vec_safe_attributes) )
        {
            foreach ( $vec_safe_attributes as $safe_attribute )
            {
                $vec_output[$safe_attribute] = $this->getAttribute($safe_attribute);
            }
        }

        return $vec_output;
    }


    /*
    |--------------------------------------------------------------------------
    | MAIL METHODS
    |--------------------------------------------------------------------------
    */

    /**
     * Returns the mail tokens
     */
    public function mailTokens()
    {
        return [];
    }


    /**
     * Help text for the mail tokens
     */
    public function mailHelp()
    {
        return [];
    }


    /*
    |--------------------------------------------------------------------------
    | YII2 ACTIVE QUERY
    |--------------------------------------------------------------------------
    */

    /**
     * Short alias of ActiverRecord::model()->findByPk()
     */
    public static function findOne($pk, $condition = '', $params = [])
    {
        $class_name = Yii::resolveClass(get_called_class());
        return $class_name::model()->findByPk($pk, $condition, $params);
    }


    /**
     * Creates an ActiveQuery object to work with it
     */
    public static function get($columns = '')
    {
        return new ActiveQuery(Yii::resolveClass(get_called_class()));
    }
}
