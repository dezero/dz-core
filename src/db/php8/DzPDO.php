<?php
/**
 * Extends PDO to support nested transactions
 *
 * @link http://www.yiiframework.com/wiki/38/how-to-use-nested-db-transactions-mysql-5-postgresql/
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2012 Dezero
 */

namespace dz\db\php8;

use PDO;

class DzPDO extends PDO
{
    // Database drivers that support SAVEPOINTs.
    protected static $savepointTransactions = array("pgsql", "mysql");

    // The current transaction level.
    protected $transLevel = 0;

    protected function nestable()
    {
        return in_array($this->getAttribute(PDO::ATTR_DRIVER_NAME), self::$savepointTransactions);
    }


    public function beginTransaction() : bool
    {
        $result = true;

        if ($this->transLevel == 0 || !$this->nestable())
        {
            $result = parent::beginTransaction();
        }
        else
        {
            $this->exec("SAVEPOINT LEVEL{$this->transLevel}");
        }

        $this->transLevel++;
        return $result;
    }


    public function commit() : bool
    {
        $this->transLevel--;

        if ($this->transLevel == 0 || !$this->nestable())
        {
            $result = parent::commit();
        }
        else
        {
            $this->exec("RELEASE SAVEPOINT LEVEL{$this->transLevel}");
        }

        return $result;
    }


    public function rollBack() : bool
    {
        $result = true;

        $this->transLevel--;

        if ($this->transLevel == 0 || !$this->nestable())
        {
            $result = parent::rollBack();
        }
        else
        {
            $this->exec("ROLLBACK TO SAVEPOINT LEVEL{$this->transLevel}");
        }

        return $result;
    }
}
