<?php
/**
 * MysqlSchema class file
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2019 Dezero
 */

namespace dz\db;

use dz\db\ColumnSchemaBuilder;
use Yii;

class MysqlSchema extends \CMysqlSchema
{
    // Yii2 -> The following are the supported abstract column data types.
    const TYPE_PK = 'pk';
    const TYPE_UPK = 'upk';
    const TYPE_BIGPK = 'bigpk';
    const TYPE_UBIGPK = 'ubigpk';
    const TYPE_CHAR = 'char';
    const TYPE_STRING = 'varchar';
    const TYPE_TEXT = 'text';
    const TYPE_TINYINT = 'tinyint';
    const TYPE_SMALLINT = 'smallint';
    const TYPE_INTEGER = 'integer';
    const TYPE_BIGINT = 'bigint';
    const TYPE_FLOAT = 'float';
    const TYPE_DOUBLE = 'double';
    const TYPE_DECIMAL = 'decimal';
    const TYPE_DATETIME = 'datetime';
    const TYPE_TIMESTAMP = 'timestamp';
    const TYPE_TIME = 'time';
    const TYPE_DATE = 'date';
    const TYPE_DATE_RAW = 'dateraw';
    const TYPE_BINARY = 'binary';
    const TYPE_BOOLEAN = 'boolean';
    const TYPE_MONEY = 'money';
    const TYPE_JSON = 'json';

    const TYPE_TINYTEXT = 'tinytext';
    const TYPE_MEDIUMTEXT = 'mediumtext';
    const TYPE_LONGTEXT = 'longtext';
    const TYPE_ENUM = 'enum';


    // Column data types from Yii2
    public $columnTypes = [
        self::TYPE_PK => 'int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY',
        self::TYPE_UPK => 'int(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
        self::TYPE_BIGPK => 'bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY',
        self::TYPE_UBIGPK => 'bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
        self::TYPE_CHAR => 'char(1)',
        self::TYPE_STRING => 'varchar(255)',
        self::TYPE_TEXT => 'text',
        self::TYPE_TINYINT => 'tinyint(3)',
        self::TYPE_SMALLINT => 'smallint(6)',
        self::TYPE_INTEGER => 'int(11)',
        self::TYPE_BIGINT => 'bigint(20)',
        self::TYPE_FLOAT => 'float',
        self::TYPE_DOUBLE => 'double',
        self::TYPE_DECIMAL => 'decimal(10,0)',
        self::TYPE_DATETIME => 'datetime',
        self::TYPE_TIMESTAMP => 'timestamp',
        self::TYPE_TIME => 'time',
        self::TYPE_DATE => 'int(10) UNSIGNED', // 'date',
        self::TYPE_DATE_RAW => 'date',
        self::TYPE_BINARY => 'blob',
        self::TYPE_BOOLEAN => 'tinyint(1)',
        self::TYPE_MONEY => 'decimal(19,4)',
        self::TYPE_JSON => 'json',
        self::TYPE_TINYTEXT => 'tinytext',
        self::TYPE_MEDIUMTEXT => 'mediumtext',
        self::TYPE_LONGTEXT => 'longtext',
        self::TYPE_ENUM => 'enum'
    ];


    /**
     * @var int The maximum length that objects' names can be.
     */
    public $maxObjectNameLength = 64;


    /**
     * @inheritdoc
     */
    public function getColumnType($type)
    {
        $type = (string) $type;

        return parent::getColumnType($type);
    }


    /**
     * Builds a SQL statement for dropping a DB table if it exists.
     *
     * @param string $table The table to be dropped. The name will be properly quoted by the method.
     * @return string The SQL statement for dropping a DB table.
     */
    public function dropTableIfExists($table)
    {
        return "DROP TABLE IF EXISTS  ". $this->quoteTableName($table);
    }


    /**
     * @inheritdoc
     */
    public function createTable($table, $columns, $options = null)
    {
        // Default to InnoDb
        if ($options === null || strpos($options, 'ENGINE=') === false)
        {
            $options = ($options !== null ? $options . ' ' : '') . 'ENGINE=InnoDb';
        }

        // Use the default charset
        if (strpos($options, 'DEFAULT CHARSET=') === false)
        {
            $options .= ' DEFAULT CHARSET=' . $this->getDbConnection()->charset;
        }

        return parent::createTable($table, $columns, $options);
    }


    /**
     * {@inheritdoc}
     */
    public function createColumnSchemaBuilder($type, $length = null)
    {
        return new ColumnSchemaBuilder($type, $length, $this->getDbConnection());
    }


    /**
     * Returns the default backup command to execute.
     *
     * @return string The command to execute
     */
    public function getDefaultBackupCommand()
    {
        $default_args =
            ' --defaults-extra-file="' . $this->_createDumpConfigFile() . '"' .
            ' --add-drop-table' .
            ' --comments' .
            ' --create-options' .
            ' --dump-date' .
            ' --no-autocommit' .
            ' --routines' .
            ' --set-charset' .
            ' --triggers';

        $vec_ignore_table_args = [];
        foreach ( Yii::app()->db->getIgnoredBackupTables() as $table )
        {
            $vec_ignore_table_args[] = "--ignore-table={database}.{$table}";
        }

        // DEZERO - Custom backup command path
        $mysqldump_command_path = 'mysqldump';
        if ( isset(Yii::app()->params['backup_command']) )
        {
            $mysqldump_command_path = Yii::app()->params['backup_command'];
        }

        $schema_dump = $mysqldump_command_path .
            $default_args .
            ' --single-transaction' .
            ' --no-data' .
            ' --result-file="{file}"' .
            ' {database}';

        $data_dump = $mysqldump_command_path .
            $default_args .
            ' --no-create-info' .
            ' ' . implode(' ', $vec_ignore_table_args) .
            ' {database}' .
            ' >> "{file}"';

        return $schema_dump . ' && ' . $data_dump;
    }


    /**
     * Delete temporary my.cnf file based on the DB config settings.
     */
    public function deleteDumpConfigFile()
    {
        $file_path = Yii::app()->path->get('temp', true) . DIRECTORY_SEPARATOR . 'my.cnf';
        $destination_file = Yii::app()->file->set($file_path);
        if ( $destination_file->getExists() )
        {
            return $destination_file->delete();
        }

        return false;
    }


    // Private Methods
    // =========================================================================

    /**
     * Creates a temporary my.cnf file based on the DB config settings.
     *
     * @return string The path to the my.cnf file
     */
    private function _createDumpConfigFile()
    {
        // Get database configuration
        $vec_config = Yii::app()->config->getDb();
        $contents = '[client]' . PHP_EOL .
            'user=' . $vec_config['username'] . PHP_EOL .
            'password="' . addslashes($vec_config['password']) . '"' . PHP_EOL .
            'host=' . $vec_config['server'] . PHP_EOL .
            'port=' . $vec_config['port'];

        if ( isset($vec_config['unixSocket']) )
        {
            $contents .= PHP_EOL . 'socket=' . $vec_config['unixSocket'];
        }

        // Create a TEMP config file
        $file_path = Yii::app()->path->get('temp', true) . DIRECTORY_SEPARATOR . 'my.cnf';
        $destination_file = Yii::app()->file->set($file_path);
        if ( ! $destination_file->getExists() )
        {
            $destination_file->create();
        }
        $destination_file->setContents($contents);

        // Avoid a “world-writable config file 'my.cnf' is ignored” warning
        $destination_file->setPermissions(644);
        // chmod($filePath, 0644);

        return $file_path;
    }
}
