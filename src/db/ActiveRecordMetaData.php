<?php
/**
 * ActiveRecordMetaData class file for Dz Framework
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2020 Dezero
 */

namespace dz\db;

use Yii;

/**
 * ActiveRecordMetaData represents the meta-data for an Active Record class.
 */
class ActiveRecordMetaData extends \CActiveRecordMetaData
{
    private $_modelClassName;

    /**
     * Constructor.
     * @param CActiveRecord $model the model instance
     * @throws CDbException if specified table for active record class cannot be found in the database
     */
    public function __construct($model)
    {
        $this->_modelClassName=get_class($model);

        $tableName=$model->tableName();
        if(($table=$model->getDbConnection()->getSchema()->getTable($tableName))===null)
            throw new \CDbException(Yii::t('yii','The table "{table}" for active record class "{class}" cannot be found in the database.',
                array('{class}'=>$this->_modelClassName,'{table}'=>$tableName)));

        if(($modelPk=$model->primaryKey())!==null || $table->primaryKey===null)
        {
            $table->primaryKey=$modelPk;
            if(is_string($table->primaryKey) && isset($table->columns[$table->primaryKey]))
                $table->columns[$table->primaryKey]->isPrimaryKey=true;
            elseif(is_array($table->primaryKey))
            {
                foreach($table->primaryKey as $name)
                {
                    if(isset($table->columns[$name]))
                        $table->columns[$name]->isPrimaryKey=true;
                }
            }
        }
        $this->tableSchema=$table;
        $this->columns=$table->columns;

        foreach($table->columns as $name=>$column)
        {
            if(!$column->isPrimaryKey && $column->defaultValue!==null)
                $this->attributeDefaults[$name]=$column->defaultValue;
        }

        foreach($model->relations() as $name=>$config)
        {
            $this->addRelation($name,$config);
        }
    }


    /**
     * Adds a relation.
     *
     * $config is an array with three elements:
     * relation type, the related active record class and the foreign key.
     *
     * @throws CDbException
     * @param string $name $name Name of the relation.
     * @param array $config $config Relation parameters.
     * @return void
     * @since 1.1.2
     */
    public function addRelation($name,$config)
    {
        // relation class, AR class, FK
        if ( isset($config[0],$config[1],$config[2]) )
        {
            // <DEZERO> --> Resolve class name
            $class = Yii::resolveClass($config[1]);
            $this->relations[$name] = new $config[0]($name, $class, $config[2], array_slice($config,3));
        }

        else
        {
            throw new \CDbException(Yii::t('yii','Active record "{class}" has an invalid configuration for relation "{relation}". It must specify the relation type, the related active record class and the foreign key.', array('{class}'=>$this->_modelClassName,'{relation}'=>$name)));
        }
    }
}
