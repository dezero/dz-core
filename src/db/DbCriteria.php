<?php
/**
 * DbCriteria class file for Dz Framework
 * 
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2019 Dezero
 */

namespace dz\db;

use CDbCriteria;

class DbCriteria extends CDbCriteria
{
    /**
     * Adds additional parameters to be bound to the query.
     * 
     * This method CHECK if params have been added
     */
    public function addParams($params)
    {
        if ( !empty($params) )
        {
            if ( empty($this->params) )
            {
                $this->params = $params;
            }
            else
            {
                foreach ( $params as $name => $value )
                {
                    if ( is_int($name) )
                    {
                        $this->params[] = $value;
                    }
                    else
                    {
                        $this->params[$name] = $value;
                    }
                }
            }
        }
    }
}