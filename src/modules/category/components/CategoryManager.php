<?php
/**
 * CategoryManager
 * 
 * Component to manage categories
 */

namespace dz\modules\category\components;

use dz\base\ApplicationComponent;
use dz\db\DbCriteria;
use dz\helpers\App;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\helpers\Transliteration;
use dz\modules\asset\models\AssetImage;
use dz\modules\category\models\Category;
use dz\modules\category\models\TranslatedCategory;
use dz\modules\web\models\Seo;
use dzlab\commerce\models\Product;
use dzlab\commerce\models\ProductCategory;
use Yii;

class CategoryManager extends ApplicationComponent
{
    /**
     * Input data
     */
    protected $vec_input_data;


    /**
     * Some error detected?
     */
    public $is_error = false;


    /**
     * CREATE action for a Category model
     * 
     * @see BaseCategoryController::actionCreate()
     */
    public function create_category($category_model, $vec_input_data)
    {
        // Category configuration for category_type
        $vec_config = $category_model->get_config();

        // Save input data
        $this->vec_input_data = $vec_input_data;

        // Default language
        $default_language = Yii::defaultLanguage();

        // Category depth
        $category_depth = 0;
        if ( isset($_GET['parent_id']) )
        {
            $category_parent_model = Category::findOne($_GET['parent_id']);
            if ( $category_parent_model )
            {
                $category_model->category_parent_id = $category_parent_model->category_id;
                $category_depth = 1;

                if ( $category_parent_model->category_parent_id > 0 )
                {
                    $category_depth = 2;

                    if ( $category_parent_model->categoryParent->category_parent_id > 0 )
                    {
                        $category_depth = 3;
                    }
                }
            }
        }

        // Extra languages
        $vec_translated_models = [];
        $vec_extra_languages = Yii::app()->i18n->get_extra_languages();
        if ( !empty($vec_extra_languages) )
        {
            foreach ( $vec_extra_languages as $language_id )
            {
                $translated_category_model = Yii::createObject(TranslatedCategory::class);
                $translated_category_model->language_id = $language_id;
                $vec_translated_models[$language_id] = $translated_category_model;
            }
        }

        // Image
        $image_model = Yii::createObject(AssetImage::class);
        $image_model->setAttributes([
            'entity_type'   => 'Category'
        ]);

        // SEO fields
        $vec_seo_models = [];
        if ( isset($vec_config['is_seo_fields']) && $vec_config['is_seo_fields'] )
        {
            // First, add SEO for default language
            $seo_model = Yii::createObject(Seo::class);
            $seo_model->setAttributes([
                'entity_type'   => 'category',
                'language_id'   => $default_language
            ]);
            $vec_seo_models[$default_language] = $seo_model;

            // Add SEO for more languages
            /*
            if ( !empty($vec_extra_languages) )
            {
                foreach ( $vec_extra_languages as $language_id )
                {
                    $seo_model = Yii::createObject(Seo::class);
                    $seo_model->setAttributes([
                        'entity_type'   => 'category',
                        'language_id'   => $language_id
                    ]);
                    $vec_seo_models[$language_id] = $seo_model;
                }
            }
            */
        }

        // Create new Category?
        if ( isset($vec_input_data['Category']) )
        {
            $category_model->setAttributes($vec_input_data['Category']);

            // Custom trigger "before_save_category" method
            $category_model = $this->before_save_category($category_model, 'create', $vec_input_data);

            // Save category
            if ( $category_model->save() )
            {
                // Image
                $category_model = $this->save_category($category_model, 'image', $image_model, $vec_input_data);

                // Translations
                $category_model = $this->save_category($category_model, 'translation', $vec_translated_models, $vec_input_data);

                // SEO
                $category_model = $this->save_category($category_model, 'seo', $vec_seo_models, $vec_input_data);

                // Save data again & trigger custom "after_save_category" method
                if ( $category_model->save() )
                {
                    $this->after_save_category($category_model, 'create', $vec_input_data);
                }
            }
            else
            {
                // Set "error" detected
                $this->is_error = true;
            }

            // Some error(s) detected
            if ( $this->is_error )
            {
                $category_model->afterFind(new \CEvent($category_model));

                // IMAGE - Try to save file as TEMPORARY
                if ( isset($vec_config['is_image']) && $vec_config['is_image'] )
                {
                    $temp_destination_path = $image_model->getTempPath() . DIRECTORY_SEPARATOR . 'category' . DIRECTORY_SEPARATOR;
                    $image_model = $category_model->upload_temp_image('image_id', $temp_destination_path, 0);
                    if ( ! $image_model )
                    {
                        $image_model = Yii::createObject(AssetImage::class);
                        $image_model->setAttributes([
                            'entity_type'   => 'Category',
                            'entity_id'     => $category_model->category_id
                        ]);
                    }
                }

                // Recover SEO data
                if ( isset($vec_input_data['Seo']) )
                {
                    foreach ( $vec_input_data['Seo'] as $language_id => $seo_values )
                    {
                        if ( isset($vec_seo_models[$language_id]) && isset($seo_values['meta_title']) )
                        {
                            $vec_seo_models[$language_id]->setAttributes($seo_values);
                            if ( ! $vec_seo_models[$language_id]->save() )
                            {
                                Log::save_model_error($vec_seo_models[$language_id]);
                            }
                        }
                    }
                }
            }
        }

        return [
            'category_model'        => $category_model,
            'image_model'           => $image_model,
            'category_depth'        => $category_depth,
            'vec_config'            => $vec_config,
            'vec_translated_models' => $vec_translated_models,
            'vec_seo_models'        => $vec_seo_models,
            'default_language'      => $default_language,
            'vec_extra_languages'   => $vec_extra_languages,
            'form_id'               => $category_model->category_type .'-form'
        ];
    }


    /**
     * UPDATE action for a Category model
     * 
     * @see BaseCategoryController::actionUpdate()
     */
    public function update_category($category_model, $vec_input_data)
    {
        // Category configuration for category_type
        $vec_config = $category_model->get_config();

        // Save input data
        $this->vec_input_data = $vec_input_data;

        // Default language
        $default_language = Yii::defaultLanguage();

        // Image
        $image_model = $category_model->image;
        if ( ! $image_model )
        {
            $image_model = Yii::createObject(AssetImage::class);
            $image_model->setAttributes([
                'entity_type'   => 'Category',
                'entity_id'     => $category_model->category_id
            ]);
        }

        // Translations
        $vec_translated_models = [];
        $vec_extra_languages = Yii::app()->i18n->get_extra_languages();
        if ( !empty($vec_extra_languages) && isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] )
        {
            foreach ( $vec_extra_languages as $language_id )
            {
                $translated_category_model = TranslatedCategory::get()
                    ->where([
                        'language_id'   => $language_id,
                        'category_id'   => $category_model->category_id
                    ])
                    ->one();

                if ( empty($translated_category_model) )
                {
                    $translated_category_model = Yii::createObject(TranslatedCategory::class);
                    $translated_category_model->language_id = $language_id;
                    $translated_category_model->category_id = $category_model->category_id;
                }
                $vec_translated_models[$language_id] = $translated_category_model;
            }
        }

        // SEO fields
        $vec_seo_models = [];
        if ( isset($vec_config['is_seo_fields']) && $vec_config['is_seo_fields'] )
        {
            $vec_seo_models = $category_model->seo;
            if ( empty($vec_seo_models) ) 
            {
                // First, add SEO for default language
                $vec_seo_attributes = [
                    'entity_id'     => $category_model->category_id,
                    'entity_type'   => 'category',
                    'language_id'   => $default_language
                ];
                $seo_model = Yii::createObject(Seo::class);
                $seo_model->setAttributes($vec_seo_attributes);
                $vec_seo_models[$default_language] = $seo_model;

                // Add SEO for more languages
                if ( !empty($vec_extra_languages) )
                {
                    foreach ( $vec_extra_languages as $language_id )
                    {
                        $vec_seo_attributes['language_id'] = $language_id;
                        $seo_model = Yii::createObject(Seo::class);
                        $seo_model->setAttributes($vec_seo_attributes);
                        $vec_seo_models[$language_id] = $seo_model;
                    }
                }
            }
            else
            {
                // Group SEO models by language
                $vec_seo_language_models = [];
                foreach ( $vec_seo_models as $seo_model )
                {
                    $vec_seo_language_models[$seo_model->language_id] = $seo_model;
                }

                // Check if there's some language with no SEO model
                if ( !empty($vec_extra_languages) )
                {
                    foreach ( $vec_extra_languages as $language_id )
                    {
                        if ( !isset($vec_seo_language_models[$language_id]) )
                        {
                            $seo_model = Yii::createObject(Seo::class);
                            $seo_model->setAttributes([
                                'entity_id'     => $category_model->category_id,
                                'entity_type'   => 'category',
                                'language_id'   => $language_id
                            ]);
                            $vec_seo_language_models[$language_id] = $seo_model;
                        }
                    }
                }
                
                $vec_seo_models = $vec_seo_language_models;
            }
        }

        // Update model?
        if ( isset($vec_input_data['Category']) )
        {
            $category_model->setAttributes($vec_input_data['Category']);

            // Custom trigger "before_save_category" method
            $category_model = $this->before_save_category($category_model, 'update', $vec_input_data);

            if ( $category_model->save() )
            {
                // Image
                $category_model = $this->save_category($category_model, 'image', $image_model, $vec_input_data);

                // Translations
                $category_model = $this->save_category($category_model, 'translation', $vec_translated_models, $vec_input_data);

                // SEO
                $category_model = $this->save_category($category_model, 'seo', $vec_seo_models, $vec_input_data);

                // Save data again & trigger custom "after_save_category" method
                if ( ! $this->is_error && $category_model->save() )
                {
                    $this->after_save_category($category_model, 'update', $vec_input_data);
                }
                else
                {
                    // Set "error" detected
                    $this->is_error = true;       
                }
            }
            else
            {
                // Set "error" detected
                $this->is_error = true;
            }

            // Some error(s) detected
            if ( $this->is_error )
            {
                $category_model->afterFind(new \CEvent($category_model));

                // IMAGE - Try to save file as TEMPORARY
                if ( isset($vec_config['is_image']) && $vec_config['is_image'] )
                {
                    $temp_destination_path = $image_model->getTempPath() . DIRECTORY_SEPARATOR . 'category' . DIRECTORY_SEPARATOR;
                    $image_model = $category_model->upload_temp_image('image_id', $temp_destination_path, 0);
                    if ( ! $image_model )
                    {
                        $image_model = Yii::createObject(AssetImage::class);
                        $image_model->setAttributes([
                            'entity_type'   => 'Category',
                            'entity_id'     => $category_model->category_id
                        ]);
                    }
                }

                // Recover SEO data
                if ( isset($vec_input_data['Seo']) )
                {
                    foreach ( $vec_input_data['Seo'] as $language_id => $seo_values )
                    {
                        if ( isset($vec_seo_models[$language_id]) && isset($seo_values['meta_title']) )
                        {
                            $vec_seo_models[$language_id]->setAttributes($seo_values);
                            if ( ! $vec_seo_models[$language_id]->save() )
                            {
                                Log::save_model_error($vec_seo_models[$language_id]);
                            }
                        }
                    }
                }
            }
        }

        return [
            'category_model'        => $category_model,
            'image_model'           => $image_model,
            'category_depth'        => $category_model->get_category_depth(),
            'vec_config'            => $vec_config,
            'vec_seo_models'        => $vec_seo_models,
            'vec_translated_models' => $vec_translated_models,
            'default_language'      => $default_language,
            'vec_extra_languages'   => $vec_extra_languages,
            'form_id'               => $category_model->category_type .'-form'
        ];
    }


    /**
     * Save entities related with a Category: image, translations or seo
     */
    public function save_category($category_model, $entity_type, $entity_data = null, $vec_input_data = [])
    {
        $is_save_error = false;

        // Get input data
        if ( empty($vec_input_data) )
        {
            $vec_input_data = $this->vec_input_data;
        }

        // Category type configuration
        $vec_config = $category_model->get_config();

        switch ( $entity_type )
        {
            // Image
            case 'image':
                // if ( isset($vec_config['is_image']) && $vec_config['is_image'] && isset($_POST['AssetImage']) )
                if ( isset($vec_config['is_image']) && $vec_config['is_image'] )
                {
                    if ( ! $category_model->add_image($entity_data) )
                    {
                        $is_save_error = true;
                    }

                    /*
                    // Try to create image file
                    $image_destination_path = $image_model->getImagesPath() . DIRECTORY_SEPARATOR . 'category' . DIRECTORY_SEPARATOR;
                    if ( empty($category_model->image_id) )
                    {
                        if ( ! $category_model->upload_image('image_id', $image_destination_path, 0) )
                        {
                             $is_save_error = true;
                        }
                    }
                    */
                }
            break;

            // Translation
            case 'translation':
                $vec_translated_models = $entity_data;
                if ( isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] && !empty($vec_translated_models) && isset($vec_input_data['TranslatedCategory']) )
                {
                    foreach ( $vec_input_data['TranslatedCategory'] as $language_id => $vec_translated_category_attributes )
                    {
                        if ( isset($vec_translated_models[$language_id]) && isset($vec_translated_category_attributes['name']) )
                        {
                            $translated_category_model = $vec_translated_models[$language_id];
                            $vec_translated_category_attributes['language_id'] = $language_id;
                            $vec_translated_category_attributes['category_id'] = $category_model->category_id;
                            $translated_category_model->setAttributes($vec_translated_category_attributes);
                            if ( ! $category_model->save_translation_model($translated_category_model) )
                            {
                                $is_save_error = true;
                            }
                        }
                    }
                }

                /*
                // Translations
                if ( isset($_POST['TranslatedCategory']) )
                {
                    foreach ( $_POST['TranslatedCategory'] as $language_id => $translated_category_values )
                    {
                        if ( isset($vec_translated_models[$language_id]) && isset($translated_category_values['name']) )
                        {
                            $vec_translated_models[$language_id]->category_id = $category_model->category_id;
                            $vec_translated_models[$language_id]->name = $translated_category_values['name'];
                            if ( ! $vec_translated_models[$language_id]->save() )
                            {
                                // Log error message
                                Log::save_model_error($vec_translated_models[$language_id]);

                                // Show error message
                                $this->showErrors($vec_translated_models[$language_id]->getErrors());
                            }
                        }
                    }
                }
                */
            break;

            // SEO
            case 'seo':
                $vec_seo_models = $entity_data;
                if ( isset($vec_config['is_seo_fields']) && $vec_config['is_seo_fields'] && !empty($vec_seo_models) && isset($vec_input_data['Seo']) )
                {
                    foreach ( $vec_input_data['Seo'] as $language_id => $seo_values )
                    {
                        if ( isset($vec_seo_models[$language_id]) && isset($seo_values['meta_title']) )
                        {
                            $seo_model = $vec_seo_models[$language_id];
                            $seo_model->setAttributes($seo_values);
                            $seo_model->entity_id = $category_model->category_id;
                            if ( ! $category_model->save_seo_model($seo_model) )
                            {
                                $is_save_error = true;
                            }
                        }
                    }
                }

                /*
                // SEO
                if ( !empty($vec_seo_models) && isset($_POST['Seo']) )
                {
                    foreach ( $_POST['Seo'] as $language_id => $seo_values )
                    {
                        if ( isset($vec_seo_models[$language_id]) && isset($seo_values['meta_title']) )
                        {
                            $seo_values['entity_id'] = $category_model->category_id;
                            $vec_seo_models[$language_id]->setAttributes($seo_values);
                            if ( ! $vec_seo_models[$language_id]->save() )
                            {
                                // Log error message
                                Log::save_model_error($vec_seo_models[$language_id]);

                                // Show error message
                                $this->showErrors($vec_seo_models[$language_id]->getErrors());
                            }
                        }
                    }
                }
                */
            break;
        }

        // Error detected?
        if ( $is_save_error )
        {
            $this->is_error = true;
            $vec_errors = Log::errors_list();
            foreach ( $vec_errors as $que_error )
            {
                $category_model->addError('category_id', $que_error);
            }
        }

        return $category_model;
    }



    /**
     * This method is invoked before a Category is going to be saved via form submitting
     */
    public function before_save_category($category_model, $action = 'update', $vec_input_data)
    {
        return $category_model;
    }


    /**
     * This method is invoked after a Category is saved via form submitting
     */
    public function after_save_category($category_model, $action = 'update', $vec_input_data)
    {
        if ( ! App::is_console() )
        {
            // Category configuration for current category type
            $vec_config = $category_model->get_config();

            if ( $action == 'create' )
            {
                Yii::app()->user->addFlash('success', $category_model->text('created_success'));

                // Redirect to update page
                if ( isset($vec_config['max_levels']) && $vec_config['max_levels'] > 1 )
                {
                    Yii::app()->controller->redirect(['update', 'id' => $category_model->category_id]);
                }

                // Redirect to index page
                else
                {
                    Yii::app()->controller->redirect(['index']);
                }
            }

            else if ( $action == 'update' )
            {
                // Has an action been triggered?
                if ( isset($vec_input_data['StatusChange']) && !empty($vec_input_data['StatusChange']) )
                {
                    switch ( $vec_input_data['StatusChange'] )
                    {
                        case 'disable':
                            if ( $category_model->disable() )
                            {
                                Yii::app()->user->addFlash('success', $category_model->text('disable_success'));
                            }
                            else
                            {
                                Yii::app()->user->addFlash('error', $category_model->text('disable_error'));
                            }
                        break;

                        case 'enable':
                            if ( $category_model->enable() )
                            {
                                Yii::app()->user->addFlash('success', $category_model->text('enable_success'));
                            }
                            else
                            {
                                Yii::app()->user->addFlash('error', $category_model->text('enable_error'));
                            }
                        break;

                        case 'delete':
                            if ( $category_model->delete() )
                            {
                                Yii::app()->user->addFlash('success', $category_model->text('delete_success'));

                                // Redirect to category index page, because this category model no longer exists
                                Yii::app()->controller->redirect(['index']);
                            }
                            else
                            {
                                Yii::app()->user->addFlash('error', $category_model->text('delete_error'));
                            }
                        break;
                    }
                }
                else
                {
                    // Success message
                    Yii::app()->user->addFlash('success', $category_model->text('updated_success'));
                }

                // Redirect to update page
                if ( isset($vec_config['max_levels']) && $vec_config['max_levels'] > 1 )
                {
                    Yii::app()->controller->redirect(['update', 'id' => $category_model->category_id]);
                }

                // Redirect to index page
                else
                {
                    // Yii::app()->controller->redirect(['index']);
                    Yii::app()->controller->refresh();
                }
            }
        }
    }


    /**
     * Category list (usually used on a SELECT2 dropdown)
     * 
     * @return array
     */
    public function category_list($category_type, $vec_options = [])
    {
        //--> DEFAULT OPTION VALUES

        // Filter by a category_parent_id?
        $category_parent_id = isset($vec_options['parent_id']) ? $vec_options['parent_id'] : -1;
        
        // Return Yii models objects or category names?
        $is_return_model = isset($vec_options['is_return_model']) ? $vec_options['is_return_model'] : false;

        // Include category parent?
        $is_included_parent = isset($vec_options['is_included_parent']) ? $vec_options['is_included_parent'] : false;

        // Add suffix "--- TODOS ---" for parent categories?
        $is_suffix_all = isset($vec_options['is_suffix_all']) ? $vec_options['is_suffix_all'] : true;

        // Add prefix " -> Seleccionar "
        $is_select_prefix = isset($vec_options['is_select_prefix']) ? $vec_options['is_select_prefix'] : false;

        // Max depth? Default is 1
        $max_depth = isset($vec_options['max_depth']) ? $vec_options['max_depth'] : 1;
        $vec_category_config = Category::model()->get_config($category_type);
        if ( isset($vec_category_config) && isset($vec_category_config['max_levels']) && $max_depth > $vec_category_config['max_levels'] )
        {
            $max_depth = $vec_category_config['max_levels'];
        }

        // Order list
        $criteria_order = isset($vec_options['order']) ? $vec_options['order'] : 'weight ASC';

        // Language?
        $language_id = null;
        if ( isset($vec_options['language']) )
        {
            $language_id = $vec_options['language'];
        }

        // Are disabled categories excluded?
        $is_disable_excluded = isset($vec_options['is_disable_excluded']) ? $vec_options['is_disable_excluded'] : false;

        //--> SEARCH CATEGORIES WITH SELECTED CRITERIA

        $criteria = new DbCriteria;
        $criteria->compare('category_type', $category_type);
        if ( $category_parent_id > -1 )
        {
            $criteria->compare('category_parent_id', $category_parent_id);
        }
        $criteria->order = $criteria_order;

        // Exclude disabled?
        if ( $is_disable_excluded )
        {
            $criteria->addCondition('disable_date IS NULL');
        }

        $vec_category_list = [];
        $vec_category_models = Category::model()->findAllAttributes(['name', 'category_parent_id', 'weight', 'created_uid'], true, $criteria);
        if ( !empty($vec_category_models) )
        {
            // Just 1 level
            if ( $max_depth == 1 )
            {
                foreach ( $vec_category_models as $category_model )
                {
                    if ( !$is_return_model )
                    {
                        $vec_category_list[$category_model->category_id] = ! isset($vec_options['language']) ? $category_model->title() : $category_model->translated_title($vec_options['language']);
                    }
                    else
                    {
                        $vec_category_list[$category_model->category_id] = $category_model;
                    }
                }
            }

            else
            {
                // 1st level
                $vec_families = [];
                foreach ( $vec_category_models as $category_model )
                {
                    if ( $category_model->category_parent_id == 0 )
                    {
                        $vec_families[$category_model->category_id] = ! isset($vec_options['language']) ? $category_model->title() : $category_model->translated_title($vec_options['language']);
                        $vec_category_list[$category_model->title()] = [];
                        if ( $is_included_parent )
                        {
                            $vec_category_list[$category_model->name][$category_model->category_id] = ! isset($vec_options['language']) ? $category_model->title() : $category_model->translated_title($vec_options['language']);
                            if ( $is_suffix_all )
                            {
                                $vec_category_list[$category_model->name][$category_model->category_id] = '--- '. Yii::t('app', 'ALL') .' '. ! isset($vec_options['language']) ? StringHelper::strtoupper($category_model->title()) : StringHelper::strtoupper($category_model->translated_title($vec_options['language'])) .' ---';
                            }
                            else if ( $is_select_prefix )
                            {
                                $vec_category_list[$category_model->name][$category_model->category_id] = '-> '. Yii::t('app', 'Select') .' '. ! isset($vec_options['language']) ? $category_model->title() : $category_model->translated_title($vec_options['language']);
                            }
                        }
                    }
                }

                // 2nd level
                foreach ( $vec_category_models as $category_model )
                {
                    if ( $category_model->category_parent_id > 0 AND isset($vec_families[$category_model->category_parent_id]) )
                    {
                        $familia_name = $vec_families[$category_model->category_parent_id];
                        if ( ! $is_return_model )
                        {
                            $vec_category_list[$familia_name][$category_model->category_id] = ! isset($vec_options['language']) ? $category_model->title() : $category_model->translated_title($vec_options['language']);
                        }
                        else
                        {
                            $vec_category_list[$familia_name][$category_model->category_id] = $category_model;  
                        }

                        // 3rd level
                        if ( $max_depth > 2 )
                        {
                            foreach ( $vec_category_models as $sub_category_model )
                            {
                                if ( $sub_category_model->category_parent_id == $category_model->category_id )
                                {
                                    if ( !$is_return_model )
                                    {
                                        $vec_category_list[$familia_name][$category_model->category_id] = ! isset($vec_options['language']) ? $category_model->title() : $category_model->translated_title($vec_options['language']);
                                        if ( $is_suffix_all )
                                        {
                                            $vec_category_list[$familia_name][$category_model->category_id] .= ' --- '. Yii::t('app', 'ALL') .' ---';
                                        }
                                        $category_title = ! isset($vec_options['language']) ? $category_model->title() : $category_model->translated_title($vec_options['language']);
                                        $sub_category_title = ! isset($vec_options['language']) ? $sub_category_model->title() : $sub_category_model->translated_title($vec_options['language']);

                                        $vec_category_list[$familia_name][$sub_category_model->category_id] = $category_title .' - '. $sub_category_title;
                                    }
                                    else
                                    {
                                        $vec_category_list[$familia_name][$sub_category_model->category_id] = $sub_category_model;  
                                    }
                                    

                                    // 4th level
                                    if ( $max_depth > 3 )
                                    {
                                        foreach ( $vec_category_models as $sub_sub_category_model )
                                        {
                                            if ( $sub_sub_category_model->category_parent_id == $sub_category_model->category_id )
                                            {
                                                if ( ! $is_return_model )
                                                {
                                                    $category_title = ! isset($vec_options['language']) ? $category_model->title() : $category_model->translated_title($vec_options['language']);
                                                    $sub_category_title = ! isset($vec_options['language']) ? $sub_category_model->title() : $sub_category_model->translated_title($vec_options['language']);
                                                    $sub_sub_category_title = ! isset($vec_options['language']) ? $sub_sub_category_model->title() : $sub_sub_category_model->translated_title($vec_options['language']);

                                                    $vec_category_list[$familia_name][$sub_sub_category_model->category_id] = $category_title .' - '. $sub_category_title .' - '. $sub_sub_category_title;
                                                }
                                                else
                                                {
                                                    $vec_category_list[$familia_name][$sub_sub_category_model->category_id] = $sub_sub_category_model;  
                                                }   
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $vec_category_list;
    }


    /**
     * Get a category list (usually for SELECT2 dropdown)
     */
    public function category_tree($category_type, $is_html = true, $category_parent_id = 0)
    {
        $criteria = new DbCriteria;
        $criteria->compare('category_type', $category_type);
        $criteria->compare('category_parent_id', $category_parent_id);
        $criteria->order = 'name ASC';

        $vec_category_list = [];
        $vec_category_models = Category::model()->findAllAttributes(['name', 'category_parent_id'], true, $criteria);
        if ( !empty($vec_category_models) )
        {
            // 1st level
            $vec_families = [];
            foreach ( $vec_category_models as $category_model )
            {
                $vec_category_list[$category_model->name] = [];
                $category_title = $category_model->title();
                if ( $is_html )
                {
                    $category_title = '<span class="tree-level-1" data-text="'. $category_model->title() .'">Seleccionar '. $category_model->title() .'</span>';
                }
                $vec_category_list[$category_model->name][$category_model->category_id] = $category_title;
                
                // 2nd level
                $subcategory_relation = 'subCategories';
                
                if ( $category_model->$subcategory_relation )
                {
                    foreach ( $category_model->$subcategory_relation as $sub_category_model )
                    {
                        $category_title = $category_model->title() .' - '. $sub_category_model->title();
                        if ( $is_html )
                        {
                            $category_title = '<span class="tree-level-2" data-text="'. $category_model->title() .' - '. $sub_category_model->title() .'"><span class="fa fa-circle"></span> '. $sub_category_model->title() .'</span>';
                        }
                        $vec_category_list[$category_model->name][$sub_category_model->category_id] = $category_title;

                        // 3rd level
                        if ( $sub_category_model->$subcategory_relation )
                        {
                            foreach ( $sub_category_model->$subcategory_relation as $sub_sub_category_model )
                            {
                                $category_title = $category_model->title() .' - '. $sub_category_model->title() .' - '. $sub_sub_category_model->title();
                                if ( $is_html )
                                {
                                    $category_title = '<span class="tree-level-3" data-text="'. $category_model->title() .' - '. $sub_category_model->title() .' - '. $sub_sub_category_model->title() .'"><span class="fa fa-circle-o"></span> '. $sub_sub_category_model->title() .'</span>';
                                }
                                $vec_category_list[$category_model->name][$sub_sub_category_model->category_id] = $category_title;

                                // 4th level
                                if ( $sub_sub_category_model->$subcategory_relation )
                                {
                                    foreach ( $sub_sub_category_model->$subcategory_relation as $sub_sub_sub_category_model )
                                    {
                                        $category_title = $category_model->title() .' - '. $sub_category_model->title() .' - '. $sub_sub_category_model->title() .' - '. $sub_sub_sub_category_model->title();
                                        if ( $is_html )
                                        {
                                            $category_title = '<span class="tree-level-4" data-text="'. $category_model->title() .' - '. $sub_category_model->title() .' - '. $sub_sub_category_model->title() .' - '. $sub_sub_sub_category_model->title() .'"><span class="fa fa-square"></span> '. $sub_sub_sub_category_model->title() .'</span>';
                                        }
                                        $vec_category_list[$category_model->name][$sub_sub_sub_category_model->category_id] = $category_title;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $vec_category_list;
    }


    /**
     * Get Category models from LEVEL 1
     */
    public function getAllByDepth($category_type, $depth = 1)
    {
        $criteria = new DbCriteria;
        $criteria->compare('t.category_type', $category_type);
        if ( $depth == 1 )
        {
            $criteria->addCondition('t.category_parent_id IS NULL');
        }
        else
        {
            $criteria->compare('t.depth', $depth);
        }
        $criteria->order = 't.weight ASC';
        return Category::model()->findAll($criteria);
    }



    /**
     * Get the category_type by id
     */
    public function get_category_type($category_id)
    {
        return Category::get()->select('category_type')->where(['category_id' => $category_id])->scalar();
    }


    /**
     * Copy translations from "category" table to "translated_category" table
     * 
     * @return total copied translations
     */
    public function copy_translations($category_type, $language_id = 'en')
    {
        $num_copied_translations = 0;
        $vec_category_models = Category::get()->where(['category_type' => $category_type])->all();
        if ( !empty($vec_category_models) )
        {
            foreach ( $vec_category_models as $category_model )
            {
                if ( $category_model->copy_translation($language_id) )
                {
                    $num_copied_translations++;
                }
            }
        }

        return $num_copied_translations;
    }


    /**
     * Get products belonging to this category
     */
    public function products_list($category_id, $is_return_model = true, $limit = null)
    {
        $vec_output = [];

        $vec_product_category_models = ProductCategory::get()
            ->with('product')
            ->where(['t.category_id' => $category_id])
            ->orderBy('product.weight DESC')
            ->limit($limit)
            ->all();

        if ( !empty($vec_product_category_models) )
        {
            foreach ( $vec_product_category_models as $product_category_model )
            {
                $product_model = Product::findOne($product_category_model->product_id);
                if ( $product_model )
                {
                    if ( $is_return_model )
                    {
                        $vec_output[$product_category_model->product_id] = $product_model;
                    }
                    else
                    {
                        $vec_output[$product_category_model->product_id] = $product_model->title();
                    }
                }
            }
        }

        return $vec_output;
    }
}