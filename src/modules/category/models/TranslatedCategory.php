<?php
/**
 * @package dz\modules\category\models 
 */

namespace dz\modules\category\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dz\modules\asset\models\AssetImage;
use dz\modules\category\models\_base\TranslatedCategory as BaseTranslatedCategory;
use dz\modules\category\models\Category;
use dz\modules\settings\models\Language;
use user\models\User;
use Yii;

/**
 * TranslatedCategory model class for "translated_category" database table
 *
 * Columns in table "translated_category" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $category_id
 * @property string $language_id
 * @property string $name
 * @property string $description
 * @property integer $image_id
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class TranslatedCategory extends BaseTranslatedCategory
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['category_id, name, updated_date, updated_uid', 'required'],
			['category_id, image_id, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['language_id', 'length', 'max'=> 4],
			['name', 'length', 'max'=> 128],
			['uuid', 'length', 'max'=> 36],
			['language_id, description, image_id, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['description', 'safe'],
			['category_id, language_id, name, description, image_id, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'category' => [self::BELONGS_TO, Category::class, 'category_id'],
            'language' => [self::BELONGS_TO, Language::class, 'language_id'],
            'image' => [self::BELONGS_TO, AssetImage::class, 'image_id'],
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'category_id' => null,
			'language_id' => null,
			'name' => Yii::t('app', 'Name'),
			'description' => Yii::t('app', 'Description'),
			'image_id' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.description', $this->description, true);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['category_id' => true]]
        ]);
    }


    /**
     * TranslatedCategory models list
     * 
     * @return array
     */
    public function translatedcategory_list()
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['category_id', 'category_id', 'language_id'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = TranslatedCategory::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('category_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Get partial or full title
     */
    public function title($is_full_title = true)
    {
        $que_title = '';
        if ( $is_full_title && $this->category && $this->category->categoryParent )
        {
            $que_title = $this->category->categoryParent->translated_title($language_id);
        }

        if ( !empty($que_title) )
        {
            $que_title .= ' - ';
        }

        $que_title .= $this->name;

        return $que_title;
    }
}