<?php
/**
 * @package dz\modules\category\models 
 */

namespace dz\modules\category\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\helpers\Transliteration;
use dz\helpers\Url;
use dz\modules\asset\models\AssetImage;
use dz\modules\category\models\TranslatedCategory;
use dz\modules\category\models\_base\Category as BaseCategory;
use dz\modules\web\models\Seo;
use user\models\User;
use Yii;

/**
 * Category model class for "category" database table
 *
 * Columns in table "category" available as properties of the model,
 * followed by relations of table "category" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $category_id
 * @property string $category_type
 * @property integer $category_parent_id
 * @property string $name
 * @property string $description
 * @property integer $weight
 * @property integer $depth
 * @property integer $image_id
 * @property integer $disable_date
 * @property integer $disable_uid
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $categoryParent
 * @property mixed $categories
 * @property mixed $createdUser
 * @property mixed $disableUser
 * @property mixed $image
 * @property mixed $updatedUser
 */
class Category extends BaseCategory
{
    /**
     * SEO URL filter
     */
    public $seo_url_filter;


    /**
     * Categories configuration
     */
    protected $vec_config = [];


	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['category_type, name, created_date, created_uid, updated_date, updated_uid', 'required'],
			['category_parent_id, weight, depth, image_id, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['category_type', 'length', 'max'=> 32],
			['name', 'length', 'max'=> 128],
			['uuid', 'length', 'max'=> 36],
			['category_parent_id, description, weight, depth, image_id, disable_date, disable_uid, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['description', 'safe'],
			['category_id, category_type, category_parent_id, name, description, weight, depth, image_id, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid, uuid, disable_filter, seo_url_filter', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'image' => [self::BELONGS_TO, AssetImage::class, 'image_id'],
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'disableUser' => [self::BELONGS_TO, User::class, ['disable_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations - Between categories
            'categoryParent' => [self::BELONGS_TO, Category::class, 'category_parent_id'],
            'subCategories' => [self::HAS_MANY, Category::class, 'category_parent_id', 'order' => 'subCategories.weight ASC'],
            'countSubcategories' => [self::STAT, Category::class, 'category_parent_id'],

            // Custom relations - Others
            'translations' => [self::HAS_MANY, TranslatedCategory::class, 'category_id'],
            'seo' => [self::HAS_MANY, Seo::class, ['entity_id' => 'category_id'], 'condition' => 'seo.entity_type = "category"', 'order' => 'seo.updated_date DESC'],
		];
	}


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
            'category_id' => Yii::t('app', 'Category'),
            'category_type' => Yii::t('app', 'Category Type'),
            'category_parent_id' => Yii::t('app', 'Category Parent'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'weight' => Yii::t('app', 'Weight'),
            'depth' => Yii::t('app', 'Depth level'),
            'image_id' => Yii::t('app', 'Image'),
			'disable_date' => Yii::t('app', 'Disable Date'),
			'disable_uid' => null,
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'categoryParent' => null,
			'categories' => null,
			'createdUser' => null,
			'disableUser' => null,
			'image' => null,
			'updatedUser' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.category_id', $this->category_id);
        $criteria->compare('t.category_type', $this->category_type);
        $criteria->compare('t.depth', $this->depth);

        // $criteria->compare('t.name', $this->name, true);
        // $criteria->compare('t.description', $this->description, true);
        // $criteria->compare('t.weight', $this->weight);
        // $criteria->compare('t.depth', $this->depth);
        // $criteria->compare('t.disable_date', $this->disable_date);
        // $criteria->compare('t.created_date', $this->created_date);
        // $criteria->compare('t.updated_date', $this->updated_date);
        // $criteria->compare('t.uuid', $this->uuid, true);

        // Category parent
        if ( $this->category_parent_id === 0 )
        {
            $criteria->addCondition('t.category_parent_id IS NULL OR t.category_parent_id = "0"');
        }
        else
        {
            $criteria->compare('t.category_parent_id', $this->category_parent_id);
        }
        
        // Filter just DISABLED categories
        if ( $this->disable_filter == 1 )
        {
            $criteria->addCondition('t.disable_date IS NOT NULL AND t.disable_date > 0');
        }

        // Filter just ENABLED categories
        else if ( $this->disable_filter == 2 )
        {
            $criteria->addCondition('t.disable_date IS NULL OR t.disable_date = 0');
        }

        // Filter with translations
        if ( $this->name )
        {
            // $criteria->compare('t.name', $this->name, true);
            $criteria->distinct = true;
            $criteria->join .= 'LEFT JOIN translated_category category_translation ON (category_translation.category_id = t.category_id) ';
            $criteria->addCondition('t.name LIKE "%'. $this->name .'%" OR category_translation.name LIKE "%'. $this->name .'%"');
        }

        // Filter by SEO URL (default language)
        if ( $this->seo_url_filter )
        {
            $seo_table = Seo::model()->tableName();
            $criteria->distinct = true;
            $criteria->join .= "LEFT JOIN {$seo_table} seo ON (seo.entity_id = t.category_id AND seo.entity_type = 'category') ";
            $criteria->compare('seo.url_manual', $this->seo_url_filter, true);
            $criteria->compare('seo.language_id', Yii::defaultLanguage());
        }

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => false, // ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['weight' => false, 'name' => false]]
        ]);
    }


    /**
     * Category list
     */
    public function category_list($category_type = '', $vec_options = [])
    {
        if ( empty($category_type) )
        {
            $category_type = $this->category_type;
        }
        return Yii::app()->categoryManager->category_list($category_type, $vec_options);
    }


    /**
     * Event "beforeValidate"
     *
     * This method is invoked before validation starts
     *
     * @return bool
     */
    public function beforeValidate()
    {
        switch ( $this->scenario )
        {
            // Create new Category
            case 'insert':
                // Get last weight
                $criteria = new DbCriteria();
                $criteria->compare('category_parent_id', $this->category_parent_id);
                $criteria->compare('category_type', $this->category_type);
                $criteria->order = 'weight DESC';
                $criteria->limit = 1;
                $last_subcategory_model = Category::model()->find($criteria);
                if ( $last_subcategory_model )
                {
                    $this->weight = $last_subcategory_model->weight + 1;
                    if ( $this->weight > 255 )
                    {
                        $this->weight = 255;
                    }
                }

                // Get depth (tree level)
                $this->depth = $this->get_depth();
            break;

            case 'update':
                // Force checking category_parent_id (must be different from current category_id)
                if ( $this->category_parent_id == $this->category_id )
                {
                    $this->category_parent_id = 0;
                }

                // Get depth (tree level)
                $this->depth = $this->get_depth();
            break;
        }

        return parent::beforeValidate();
    }


    /**
     * Event "afterSave"
     *
     * This method is invoked after saving a record successfully
     */
    public function afterSave()
    {
        switch ( $this->scenario )
        {
            case 'insert':
            case 'update':
                if ( $this->is_multilanguage() && Yii::isMultilanguage() )
                {
                    // Copy translatable attributes from "category" table to "translated_category" table
                    $this->copy_translation();
                }
            break;
        }

        return parent::afterSave();
    }


    /**
     * Action "delete"
     *
     * Deletes the row corresponding to this active record
     */
    public function delete()
    {
        // STEP 1 - TRANSLATIONS
        if ( $this->translations )
        {
            foreach ( $this->translations as $translated_category_model )
            {
                $translated_category_model->delete();
            }
        }

        // STEP 2 - SEO
        if ( $this->seo )
        {
            foreach ( $this->seo as $seo_model )
            {
                $seo_model->delete();
            }
        }

        // STEP 3 - SUBCATEGORIES
        if ( $this->subCategories )
        {
            foreach ( $this->subCategories as $subcategory_model )
            {
                $subcategory_model->delete();
            }
        }

        // Going on with "delete" action
        return parent::delete();
    }


    /**
     * Disables a Category model
     */
    public function disable()
    {
        // Check if "disable" process is allowed for this category
        $vec_category_config = $this->get_config();
        if ( isset($vec_category_config['is_disable_allowed']) && $vec_category_config['is_disable_allowed'] )
        {
            // Disable process
            if ( parent::disable() )
            {
                // Disable subCategories
                if ( $this->subCategories )
                {
                    foreach ( $this->subCategories as $subcategory_model )
                    {
                        $subcategory_model->disable();
                    }
                }

                return true;
            }
            
            return false;
        }

        return true;
    }


    /**
     * Enables a Category model
     */
    public function enable()
    {
        // Check if "enable" process is allowed for this category
        $vec_category_config = $this->get_config();
        if ( isset($vec_category_config['is_disable_allowed']) && $vec_category_config['is_disable_allowed'] )
        {
            // Eanble subCategories
            if ( parent::enable() )
            {
                if ( $this->subCategories )
                {
                    foreach ( $this->subCategories as $subcategory_model )
                    {
                        $subcategory_model->enable();
                    }
                }

                return true;
            }
            
            return false;
        }

        return true;
    }


    /**
     * Get all Category chidren (subcategories)
     */
    public function get_all_subcategories($subcategory_relation = 'subCategories')
    {
        $vec_category_models = [];
        if ( $this->$subcategory_relation )
        {
            foreach ( $this->$subcategory_relation as $subcategory_model )
            {
                $vec_category_models[] = $subcategory_model;
                if ( $subcategory_model->countSubcategories > 0 )
                {
                    $vec_category_models = \CMap::mergeArray($vec_category_models, $subcategory_model->get_all_subcategories());
                }
            }
        }
        
        return $vec_category_models;
    }


    /**
     * Get all Category parents
     */
    public function get_all_parents()
    {
        $vec_category_models = [];
        if ( $this->categoryParent )
        {
            $vec_category_models = \CMap::mergeArray([$this->categoryParent], $this->categoryParent->get_all_parents());
        }
        
        return $vec_category_models;
    }


    /**
     * Get category depth number
     */
    public function get_category_depth()
    {
        // 1st level
        $category_depth = 0;

        // 2nd level
        if ( $this->categoryParent )
        {
            $category_depth = 1;

            if ( $this->categoryParent->categoryParent )
            {
                $category_depth = 2;

                if ( $this->categoryParent->categoryParent->categoryParent )
                {
                    $category_depth = 3;
                }
            }
        }

        return $category_depth;
    }


    /**
     * Load and returns schema configuration of categories
     */
    public function get_config($category_type = '')
    {
        if ( empty($this->vec_config) )
        {
            $this->vec_config = Yii::app()->config->get('components.categories');
        }

        if ( !empty($this->vec_config) )
        {
            // Return full configuration options
            if ( $category_type == 'all' )
            {
                return $this->vec_config;
            }

            // Current category type
            if ( empty($category_type) )
            {
                $category_type = $this->category_type;
            }

            // Return configuration given input params
            if ( !empty($category_type) && isset($this->vec_config[$category_type]) )
            {
                return $this->vec_config[$category_type];
            }
        }

        // Default values
        return [
            // Actions allowed
            'is_editable'               => true,
            'is_disable_allowed'        => true,
            'is_delete_allowed'         => true,
            'is_first_level_sortable'   => true,

            // Max levels (subcategories)
            'max_levels'                => 3,

            // Optional fields
            'is_multilanguage'          => true,
            'is_description'            => false,
            'is_image'                  => true,
            'is_seo_fields'             => true,

            // Custom path for images
            'images_path'               => 'www' . DIRECTORY_SEPARATOR .'files'. DIRECTORY_SEPARATOR .'images'. DIRECTORY_SEPARATOR .'category'. DIRECTORY_SEPARATOR,

            // View files path
            'views' => [
                'index'         => '//category/_base/index',
                'create'        => '//category/_base/create',    
                'update'        => '//category/_base/update',
                '_form'         => '//category/_base/_form',
                '_form_seo'     => '//category/_base/_form_seo',
                '_grid_column'  => '//category/_base/_grid_column',
                '_tree'         => '//category/_base/_tree',
                '_tree_main'    => '//category/_base/_tree_main',
            ],

            // Texts
            'texts' => [
                'entity_label'      => 'Category',
                'subentity_label'   => 'Subcategory',
                'entities_label'    => 'Categories',

                'index_title'       => 'Manage categories',
                'panel_title'       => 'Categories',
                'list_title'        => 'Categories list',
                'add_button'        => 'Add category',
                'create_title'      => 'Create category',
                'subcategory_title' => 'Create subcategory of "{subcategory}"',

                // Success messages
                'created_success'   => 'New category created successfully',
                'updated_success'   => 'Category updated successfully',

                // Disable
                'disable_success'   => 'Category DISABLED successfully',
                'disable_error'     => 'Category could not be DISABLED',
                'disable_confirm'   => '<h3>Are you sure you want to <span class=\'text-danger\'>DISABLE</span> this category?</h3>',

                // Enable
                'enable_success'    => 'Category ENABLED successfully',
                'enable_error'      => 'Category could not be ENABLED',
                'enable_confirm'    => '<h3>Are you sure you want to <span class=\'text-success\'>ENABLE</span> this category?</h3>',

                // Delete
                'delete_success'    => 'Category DELETED successfully',
                'delete_error'      => 'Category could not be DELETED',
                'delete_confirm'    => '<h3><i class="icon wb-alert-circle text-danger"></i> Are you sure you want to <span class=\'text-danger\'>DELETE</span> this category?</h3>',

                // Other
                'subcategories'     => 'subcategories',
                'empty_text'        => 'No categories found',
            ]
        ];
    }


    /**
     * Returns the view file path
     */
    public function get_view_path($view_name)
    {
        // Default view paths
        $vec_views = [
            'index'         => '//category/_base/index',
            'create'        => '//category/_base/create',    
            'update'        => '//category/_base/update',
            '_form'         => '//category/_base/_form',
            '_form_seo'     => '//category/_base/_form_seo',
            '_grid_column'  => '//category/_base/_grid_column',
            '_tree'         => '//category/_base/_tree',
            '_tree_main'    => '//category/_base/_tree_main',
        ];

        // Return view path from configuration file
        if ( !empty($this->category_type) )
        {
            $vec_config = $this->get_config($this->category_type);
            if ( !empty($vec_config) && isset($vec_config['views']) && isset($vec_config['views'][$view_name]) )
            {
                return $vec_config['views'][$view_name];
            }
        }

        // Return view path from default values
        if ( isset($vec_views[$view_name]) )
        {
            return $vec_views[$view_name];
        }

        // No view has been found
        return $view_name;
    }


    /**
     * Return the corresponding text
     */
    public function text($text_key, $category_type = null)
    {
        // Current category type
        if ( $category_type === null )
        {
            $category_type = $this->category_type;
        }

        // Load configuration and return text
        $vec_config = $this->get_config($category_type);
        if ( !empty($vec_config) && isset($vec_config['texts']) && isset($vec_config['texts'][$text_key]) )
        {
            return Yii::t('app', $vec_config['texts'][$text_key]);
        }

        return '';
    }


    /**
     * Get depth or tree level
     */
    public function get_depth()
    {
        $depth = $this->depth ;
        if ( $this->categoryParent )
        {
            $depth = $this->categoryParent->depth + 1;
        }

        if ( $depth > 255 )
        {
            $depth = 255;
        }

        return $depth;
    }


    /**
     * Add an AssetImage model to this category
     */
    public function add_image($image_model)
    {
        // Get image path
        $image_destination_path = $this->get_images_path(true);

        // Try to save image file
        if ( ! $this->upload_image('image_id', $image_destination_path, 0) )
        {
            $vec_file_errors = $this->get_file_errors();
            if ( !empty($vec_file_errors) )
            {
                Yii::app()->controller->showErrors($vec_file_errors);
                return false;
            }
        }

        return true;
    }


    /**
     * Get default images path for this category
     */
    public function get_images_path($is_check_exists = false)
    {
        // Category configuration for category_type
        $vec_config = $this->get_config();

        if ( isset($vec_config['images_path']) && !empty($vec_config['images_path']) )
        {
            $category_image_path = $vec_config['images_path'];
        }

        $category_image_path = Yii::app()->path->imagesPath() . DIRECTORY_SEPARATOR . 'category' . DIRECTORY_SEPARATOR;
        // $category_image_path AssertFile::model()->getImagesPath() . DIRECTORY_SEPARATOR . 'category' . DIRECTORY_SEPARATOR;

        // Check if directory exists, unless create it
        if ( $is_check_exists )
        {
            $category_image_dir = Yii::app()->file->set($category_image_path);
            if (  ! $category_image_dir->isDir )
            {
                // Default UNIX file permissions = 755
                $default_permissions = AssetImage::model()->get_default_permissions();

                $category_image_dir->createDir($default_permissions, $category_image_path);
                // $category_image_dir->setPermissions($default_permissions);
            }
        }

        return $category_image_path;
    }



    /**
     * Check if a preview image file exists and return it
     */
    public function preview_image($preset = 'small')
    {
        // Check if image us uploaded
        if ( $this->image && $this->image->load_file() )
        {
            return $this->image->image_url($preset) .'?id='. $this->image->file_id;
        }
        return false;
    }


    /**
     * Get TranslatedCategory model given a language
     */
    public function get_translation($language_id)
    {
        return TranslatedCategory::get()
            ->where([
                'category_id'   => $this->category_id,
                'language_id'   => $language_id
            ])
            ->one();
    }


    /**
     * Save translation
     */
    public function save_translation($vec_attributes, $language_id)
    {
        $vec_attributes['language_id'] = $language_id;
        $translated_model = $this->get_translation($language_id);

        if ( ! $translated_model )
        {
            $translated_model = Yii::createObject(TranslatedCategory::class);
            $translated_model->category_id = $this->category_id;
        }

        $translated_model->setAttributes($vec_attributes);
        return $translated_model->save();
    }


    /**
     * Save a TranslatedCategory model
     */
    public function save_translation_model($translated_category_model)
    {
        if ( empty($translated_category_model->category_id) || $translated_category_model->category_id == $this->category_id )
        {
            $translated_category_model->category_id = $this->category_id;

            if ( ! $translated_category_model->save() )
            {
                Log::save_model_error($translated_category_model);
                return false;
            }

            return true;
        }

        return false;
    }


    /**
     * Copy current translatable attributes to TranslatedCategory model
     * 
     * Copy from Category model to TranslatedCategory model
     */
    public function copy_translation($language_id = '')
    {
        // Copy translation with default language
        if ( empty($language_id) )
        {
            $language_id = Yii::defaultLanguage();
        }

        if ( !empty($language_id) )
        {
            $translated_category_model = $this->get_translation($language_id);
            if ( ! $translated_category_model )
            {
                $translated_category_model = Yii::createObject(TranslatedCategory::class);
                $translated_category_model->setAttributes([
                    'category_id'   => $this->category_id,
                    'language_id'   => $language_id,
                ]);
            }

            // Translatable attributes
            $translated_category_model->setAttributes([
                'name'          => $this->name,
                'description'   => $this->description,
                'image_id'      => $this->image_id,
            ]);

            if ( ! $translated_category_model->save() )
            {
                Log::save_model_error($translated_category_model);
                return false;
            }

            return true;
        }

        return false;
    }


    /**
     * Check if this Category type has multilanguage option enabled
     */
    public function is_multilanguage()
    {
        $vec_config = $this->get_config($this->category_type);
        return ( !empty($vec_config) && isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] );
    }


    /**
     * Get SEO model given a language
     */
    public function get_seo($language_id = null)
    {
        // Default language
        if ( $language_id === null )
        {
            $language_id = Yii::defaultLanguage();
        }

        return Seo::get()
            ->where([
                'entity_id'     => $this->category_id,
                'entity_type'   => 'category',
                'language_id'   => $language_id
            ])
            ->one();
    }


    /**
     * Save Seo attributes
     */
    public function save_seo($vec_attributes, $language_id = '')
    {
        // Default language
        if ( empty($language_id) )
        {
            $language_id = Yii::defaultLanguage();
        }
        $vec_attributes['language_id'] = $language_id;
        
        $seo_model = Seo::model()->findByAttributes([
            'entity_id'     => $this->category_id,
            'entity_type'   => 'category',
            'language_id'   => $vec_attributes['language_id']
        ]);

        if ( ! $seo_model )
        {
            $seo_model = Yii::createObject(Seo::class);
            $seo_model->entity_id = $this->category_id;
            $seo_model->entity_type = 'category';
        }

        $seo_model->setAttributes($vec_attributes);
        return $seo_model->save();
    }


    /**
     * Save a Seo model
     */
    public function save_seo_model($seo_model, $is_save_category = false, $language_id = null)
    {
        if ( empty($seo_model->entity_id) || $seo_model->entity_id == $this->category_id )
        {
            // Default language
            if ( $language_id === null )
            {
                $language_id = Yii::defaultLanguage();
            }

            $seo_model->entity_id = $this->category_id;
            $seo_model->entity_type = 'category';
            $seo_model->language_id = $language_id;

            if ( ! $seo_model->save() )
            {
                Log::save_model_error($seo_model);
                return false;
            }

            return true;
        }

        return false;
    }


    /**
     * Returns the SEO URL for this model
     * 
     * @see \dz\helper\Url::to() method
     */
    public function url($vec_params=[], $ampersand='&')
    {
        // Default route (NO SEO way)
        $route = 'category/'. $this->category_id;

        // Current language
        $language_id = Yii::currentLanguage();
        $is_multilanguage = $this->is_multilanguage();

        if ( ! $is_multilanguage )
        {
            if ( !empty($this->seo_url) )
            {
                $route = $this->seo_url;
            }

            else if ( !empty($this->seo) )
            {
                $route = $this->seo[0]->url_manual;
            }
        }
        else if ( $is_multilanguage )
        {
            if ( $language_id == Yii::defaultLanguage() && !empty($this->seo_url) )
            {
                $route = $this->seo_url;
            }
            else
            {
                $seo_model = $this->get_seo($language_id);
                if ( $seo_model )
                {
                    $route = $seo_model->url_manual;
                }
            }
        }

        return Url::to($route, $vec_params, '', $ampersand);
    }


    /**
     * Get SEO URL generated automatically
     */
    public function get_seo_url_auto($language_id = '')
    {
        if ( !empty($language_id) && $this->is_multilanguage() )
        {
            $translated_model = $this->get_translation($language_id);
            if ( $translated_model && !empty($translated_model->name) )
            {
                return Transliteration::url($translated_model->name);
            }
        }
        
        return Transliteration::url($this->name);
    }


    /**
     * Generate SEO data
     */
    public function generate_seo($language_id)
    {
        $translated_category_model = $this->get_translation($language_id);
        return [
            'url_auto'          => $this->get_seo_url_auto($language_id), 
            'meta_title'        => ($translated_category_model === null) ? $this->name : $translated_category_model->name,
            // 'meta_description'  => ($translated_category_model === null) ? $this->description : $translated_category_model->description,
        ];
    }


    /**
     * SEO - Return a meta tags array for a Category page
     */
    public function get_seo_metatags()
    {
        $vec_meta_tags = [
            'title'         => $this->title(),
            'description'   => $this->description,
            'image'         => $this->image,
            'type'          => 'website'
        ];

        return $vec_meta_tags;
    }


    /**
     * Returns the mail tokens
     */
    public function mailTokens()
    {
        return [
            '{category.id}'     => $this->category_id,
            '{category.name}'   => $this->name,
        ];
    }


    /**
     * Help text for the mail tokens
     */
    public function mailHelp()
    {
        return [
            '{category.id}'     => 'Category identificator (internal)',
            '{category.name}'   => 'Category name',
        ];
    }

    /**
     * Get translated title
     */
    public function translated_title($language_id = '', $is_full_title = false)
    {
        // Current language
        if ( empty($language_id) )
        {
            $language_id = Yii::currentLanguage();
        }

        $translated_model = $this->get_translation($language_id);
        if ( $translated_model )
        {
            return $translated_model->title($is_full_title);
        }

        return $this->title($is_full_title);
    }


    /**
     * Get partial or full title
     */
    public function title($is_full_title = false)
    {
        $que_title = '';
        if ( $is_full_title && $this->categoryParent )
        {
            $que_title = $this->categoryParent->title();
        }

        if ( !empty($que_title) )
        {
            $que_title .= ' - ';
        }

        $que_title .= $this->name;

        return $que_title;
    }


    /**
     * Get full title
     */
    public function full_title()
    {
        return $this->title(true);
    }
}