(function(document, window, $) {  
  $(document).ready(function() {
    // Disable/enable buttons
    $('a[data-plugin="dz-status-button"]').dzStatusButton();
  });
})(document, window, jQuery);