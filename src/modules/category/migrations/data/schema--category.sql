-- -----------------------------------------------------
-- CATEGORIES: Main and contextual categories
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `category` ;

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_type` VARCHAR(32) NOT NULL,
  `category_parent_id` INT UNSIGNED NOT NULL DEFAULT 0
    COMMENT 'CONSTRAINT FOREIGN KEY (category_parent_id) REFERENCES category(category_id)',
  `name` VARCHAR(128) NOT NULL,
  `description` TEXT NULL,
  `weight` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1,
  `depth` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `image_id` INT UNSIGNED NULL
    COMMENT 'CONSTRAINT FOREIGN KEY (image_id) REFERENCES asset_file(image_id)',
  `disable_date` INT UNSIGNED NULL,
  `disable_uid` INT UNSIGNED NULL
    COMMENT 'CONSTRAINT FOREIGN KEY (disable_uid) REFERENCES user_users(uid)',
  `created_date` INT UNSIGNED NOT NULL,
  `created_uid` INT UNSIGNED NOT NULL
    COMMENT "CONSTRAINT FOREIGN KEY (created_uid) REFERENCES user_users(uid)",
  `updated_date` INT UNSIGNED NOT NULL,
  `updated_uid` INT UNSIGNED NOT NULL
    COMMENT "CONSTRAINT FOREIGN KEY (updated_uid) REFERENCES user_users(uid)",
  PRIMARY KEY (`category_id`) ,
  INDEX `category_type_index` (`category_type`),
  INDEX `category_parent_index` (`category_parent_id`)
) ENGINE=MyISAM;


-- -----------------------------------------------------
-- Table `translated_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `translated_category` ;

CREATE TABLE IF NOT EXISTS `translated_category` (
  `category_id` INT UNSIGNED NOT NULL
    COMMENT 'CONSTRAINT FOREIGN KEY (category_id) REFERENCES category(category_id)',
  `language_id` VARCHAR(4) NOT NULL
    COMMENT 'CONSTRAINT FOREIGN KEY (language_id) REFERENCES language(language_id)',
  `name` VARCHAR(128) NOT NULL,
  `description` TEXT NULL,
  `image_id` INT UNSIGNED NULL
    COMMENT 'CONSTRAINT FOREIGN KEY (image_id) REFERENCES asset_file(image_id)',
  PRIMARY KEY (`category_id`, `language_id`),
  INDEX `translated_category_index` (`category_id`),
  INDEX `translated_language_index` (`language_id`)
) ENGINE = MyISAM;



-- ------------------------------------------------------------
-- Auth items
-- ------------------------------------------------------------
DELETE FROM `user_auth_item` WHERE `name` LIKE "category.%";
DELETE FROM `user_auth_item` WHERE `name` LIKE "category_%";
DELETE FROM `user_auth_item_child` WHERE `child` LIKE "category.%";
DELETE FROM `user_auth_item_child` WHERE `child` LIKE "category_%";

INSERT INTO `user_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
  ('category_manage', 1, 'Manage categories (full access)', NULL, 'N;'),
  ('category.category.*',0,'Categories - Categories - Full access to categories',NULL,'N;'),
  ('category.category.create',0,'Categories - Categories - Create new categories',NULL,'N;'),
  ('category.category.update',0,'Categories - Categories - Edit categories',NULL,'N;'),
  ('category.category.view',0,'Categories - Categories - View categories',NULL,'N;');

INSERT INTO `user_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
  ('category_edit', 1, 'Edit category',NULL,'N;'),
  ('category_view', 1, 'View category',NULL,'N;');

INSERT INTO `user_auth_item_child` (`parent`, `child`) VALUES
  ('category_edit','category.category.update'),
  ('category_edit','category.category.view'),
  ('category_view','category.category.view'),
  ('admin','category_manage'),
  ('editor','category_view'),
  ('editor','category_edit');