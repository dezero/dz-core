<?php
/**
 * Migration class m190203_160000_category_tables
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m190203_160000_category_tables extends Migration
{
    /**
     * This method contains the logic to be executed when applying this migration.
     */
    public function up()
    {
        // Create "category" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('category', true);

         $this->createTable('category', [
            'category_id' => $this->primaryKey(),
            'category_type' => $this->string(32)->notNull(),
            'category_parent_id' => $this->integer()->unsigned(),
            'name' => $this->string(128)->notNull(),
            'description' => $this->text(),
            'weight' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(1),
            'depth' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(0),
            'image_id' => $this->integer()->unsigned(),
            'disable_date' => $this->date(),
            'disable_uid' => $this->integer()->unsigned(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create indexes
        $this->createIndex(null, 'category', ['category_type'], false);

        // Foreign keys
        $this->addForeignKey(null, 'category', ['category_parent_id'], 'category', ['category_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'category', ['image_id'], 'asset_file', ['file_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'category', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'category', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'category', ['disable_uid'], 'user_users', ['id'], 'CASCADE', null);


        // Create "translated_category" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('translated_category', true);

         $this->createTable('translated_category', [
            'category_id' => $this->integer()->unsigned()->notNull(),
            'language_id' => $this->string(4)->notNull()->defaultValue('es'),
            'name' => $this->string(128)->notNull(),
            'description' => $this->text(),
            'image_id' => $this->integer()->unsigned(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Primary key
        $this->addPrimaryKey(null, 'translated_category', ['category_id', 'language_id']);

        // Foreign keys
        $this->addForeignKey(null, 'translated_category', ['category_id'], 'category', ['category_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'translated_category', ['language_id'], 'language', ['language_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'translated_category', ['image_id'], 'asset_file', ['file_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'translated_category', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);


        // Permissions
        // -------------------------------------------------------------------------
        $this->insertMultiple('user_auth_item', [
            [
                'name'          => 'category.category.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Categories - Category - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'category.category.create',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Categories - Category - Create new categories',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'category.category.update',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Categories - Category - Edit categories',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'category.category.view',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Categories - Category - View categories',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'category_manage',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Categories - Full access to categories',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'category_edit',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Categories - Edit categories',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'category_view',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Categories - View categories',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
        ]);

        // Set items relationships
        $this->insertMultiple('user_auth_item_child', [
            [
                'parent'    => 'category_manage',
                'child'     => 'category.category.*'
            ],
            [
                'parent'    => 'category_edit',
                'child'     => 'category.category.update'
            ],
            [
                'parent'    => 'category_edit',
                'child'     => 'category.category.view'
            ],
            [
                'parent'    => 'category_view',
                'child'     => 'category.category.view'
            ],
            [
                'parent'    => 'admin',
                'child'     => 'category_manage'
            ],
            [
                'parent'    => 'editor',
                'child'     => 'category_edit'
            ],
        ]);

        return true;
    }


    /**
     * This method contains the logic to be executed when removing this migration.
     */
    public function down()
    {
        // $this->dropTable('my_table');
        return false;
    }


    /**
     * Insert all countries into "country" database table
     */
    private function _insert_countries()
    {
        $vec_countries = array(
            'ac' => 'Isla de la Ascensión',
            'ad' => 'Andorra',
            'ae' => 'Emiratos Árabes Unidos',
            'af' => 'Afganistán',
            'ag' => 'Antigua y Barbuda',
            'ai' => 'Anguila',
            'al' => 'Albania',
            'am' => 'Armenia',
            'an' => 'Antillas Neerlandesas',
            'ao' => 'Angola',
            'aq' => 'Antártida',
            'ar' => 'Argentina',
            'as' => 'Samoa Americana',
            'at' => 'Austria',
            'au' => 'Australia',
            'aw' => 'Aruba',
            'ax' => 'Islas Åland',
            'az' => 'Azerbaiyán',
            'ba' => 'Bosnia-Herzegovina',
            'bb' => 'Barbados',
            'bd' => 'Bangladesh',
            'be' => 'Bélgica',
            'bf' => 'Burkina Faso',
            'bg' => 'Bulgaria',
            'bh' => 'Bahréin',
            'bi' => 'Burundi',
            'bj' => 'Benín',
            'bl' => 'San Bartolomé',
            'bm' => 'Bermudas',
            'bn' => 'Brunéi',
            'bo' => 'Bolivia',
            'bq' => 'Caribe neerlandés',
            'br' => 'Brasil',
            'bs' => 'Bahamas',
            'bt' => 'Bután',
            'bv' => 'Isla Bouvet',
            'bw' => 'Botsuana',
            'by' => 'Bielorrusia',
            'bz' => 'Belice',
            'ca' => 'Canadá',
            'cc' => 'Islas Cocos',
            'cd' => 'Congo [República Democrática del Congo]',
            'cf' => 'República Centroafricana',
            'cg' => 'Congo [República]',
            'ch' => 'Suiza',
            'ci' => 'Costa de Marfil',
            'ck' => 'Islas Cook',
            'cl' => 'Chile',
            'cm' => 'Camerún',
            'cn' => 'China',
            'co' => 'Colombia',
            'cp' => 'Isla Clipperton',
            'cr' => 'Costa Rica',
            'cu' => 'Cuba',
            'cv' => 'Cabo Verde',
            'cw' => 'Curazao',
            'cx' => 'Isla Christmas',
            'cy' => 'Chipre',
            'cz' => 'República Checa',
            'de' => 'Alemania',
            'dg' => 'Diego García',
            'dj' => 'Yibuti',
            'dk' => 'Dinamarca',
            'dm' => 'Dominica',
            'do' => 'República Dominicana',
            'dz' => 'Argelia',
            'ea' => 'Ceuta y Melilla',
            'ec' => 'Ecuador',
            'ee' => 'Estonia',
            'eg' => 'Egipto',
            'eh' => 'Sáhara Occidental',
            'er' => 'Eritrea',
            'es' => 'España',
            'et' => 'Etiopía',
            'eu' => 'Unión Europea',
            'fi' => 'Finlandia',
            'fj' => 'Fiyi',
            'fk' => 'Islas Malvinas [Islas Falkland]',
            'fm' => 'Micronesia',
            'fo' => 'Islas Feroe',
            'fr' => 'Francia',
            'ga' => 'Gabón',
            'gb' => 'Reino Unido',
            'gd' => 'Granada',
            'ge' => 'Georgia',
            'gf' => 'Guayana Francesa',
            'gg' => 'Guernsey',
            'gh' => 'Ghana',
            'gi' => 'Gibraltar',
            'gl' => 'Groenlandia',
            'gm' => 'Gambia',
            'gn' => 'Guinea',
            'gp' => 'Guadalupe',
            'gq' => 'Guinea Ecuatorial',
            'gr' => 'Grecia',
            'gs' => 'Islas Georgia del Sur y Sandwich del Sur',
            'gt' => 'Guatemala',
            'gu' => 'Guam',
            'gw' => 'Guinea-Bissau',
            'gy' => 'Guyana',
            'hk' => 'Hong Kong',
            'hm' => 'Islas Heard y McDonald',
            'hn' => 'Honduras',
            'hr' => 'Croacia',
            'ht' => 'Haití',
            'hu' => 'Hungría',
            'ic' => 'Islas Canarias',
            'id' => 'Indonesia',
            'ie' => 'Irlanda',
            'il' => 'Israel',
            'im' => 'Isla de Man',
            'in' => 'India',
            'io' => 'Territorio Británico del Océano Índico',
            'iq' => 'Iraq',
            'ir' => 'Irán',
            'is' => 'Islandia',
            'it' => 'Italia',
            'je' => 'Jersey',
            'jm' => 'Jamaica',
            'jo' => 'Jordania',
            'jp' => 'Japón',
            'ke' => 'Kenia',
            'kg' => 'Kirguistán',
            'kh' => 'Camboya',
            'ki' => 'Kiribati',
            'km' => 'Comoras',
            'kn' => 'San Cristóbal y Nieves',
            'kp' => 'Corea del Norte',
            'kr' => 'Corea del Sur',
            'kw' => 'Kuwait',
            'ky' => 'Islas Caimán',
            'kz' => 'Kazajistán',
            'la' => 'Laos',
            'lb' => 'Líbano',
            'lc' => 'Santa Lucía',
            'li' => 'Liechtenstein',
            'lk' => 'Sri Lanka',
            'lr' => 'Liberia',
            'ls' => 'Lesoto',
            'lt' => 'Lituania',
            'lu' => 'Luxemburgo',
            'lv' => 'Letonia',
            'ly' => 'Libia',
            'ma' => 'Marruecos',
            'mc' => 'Mónaco',
            'md' => 'Moldavia',
            'me' => 'Montenegro',
            'mf' => 'San Martín',
            'mg' => 'Madagascar',
            'mh' => 'Islas Marshall',
            'mk' => 'Macedonia [ERYM]',
            'ml' => 'Mali',
            'mm' => 'Myanmar [Birmania]',
            'mn' => 'Mongolia',
            'mo' => 'Macao',
            'mp' => 'Islas Marianas del Norte',
            'mq' => 'Martinica',
            'mr' => 'Mauritania',
            'ms' => 'Montserrat',
            'mt' => 'Malta',
            'mu' => 'Mauricio',
            'mv' => 'Maldivas',
            'mw' => 'Malaui',
            'mx' => 'México',
            'my' => 'Malasia',
            'mz' => 'Mozambique',
            'na' => 'Namibia',
            'nc' => 'Nueva Caledonia',
            'ne' => 'Níger',
            'nf' => 'Isla Norfolk',
            'ng' => 'Nigeria',
            'ni' => 'Nicaragua',
            'nl' => 'Países Bajos',
            'no' => 'Noruega',
            'np' => 'Nepal',
            'nr' => 'Nauru',
            'nu' => 'Isla Niue',
            'nz' => 'Nueva Zelanda',
            'om' => 'Omán',
            'pa' => 'Panamá',
            'pe' => 'Perú',
            'pf' => 'Polinesia Francesa',
            'pg' => 'Papúa Nueva Guinea',
            'ph' => 'Filipinas',
            'pk' => 'Pakistán',
            'pl' => 'Polonia',
            'pm' => 'San Pedro y Miquelón',
            'pn' => 'Islas Pitcairn',
            'pr' => 'Puerto Rico',
            'ps' => 'Palestina',
            'pt' => 'Portugal',
            'pw' => 'Palau',
            'py' => 'Paraguay',
            'qa' => 'Qatar',
            'qo' => 'Territorios alejados de Oceanía',
            're' => 'Reunión',
            'ro' => 'Rumanía',
            'rs' => 'Serbia',
            'ru' => 'Rusia',
            'rw' => 'Ruanda',
            'sa' => 'Arabia Saudí',
            'sb' => 'Islas Salomón',
            'sc' => 'Seychelles',
            'sd' => 'Sudán',
            'se' => 'Suecia',
            'sg' => 'Singapur',
            'sh' => 'Santa Elena',
            'si' => 'Eslovenia',
            'sj' => 'Svalbard y Jan Mayen',
            'sk' => 'Eslovaquia',
            'sl' => 'Sierra Leona',
            'sm' => 'San Marino',
            'sn' => 'Senegal',
            'so' => 'Somalia',
            'sr' => 'Surinam',
            'ss' => 'Sudán del Sur',
            'st' => 'Santo Tomé y Príncipe',
            'sv' => 'El Salvador',
            'sx' => 'Sint Maarten',
            'sy' => 'Siria',
            'sz' => 'Suazilandia',
            'ta' => 'Tristán da Cunha',
            'tc' => 'Islas Turcas y Caicos',
            'td' => 'Chad',
            'tf' => 'Territorios Australes Franceses',
            'tg' => 'Togo',
            'th' => 'Tailandia',
            'tj' => 'Tayikistán',
            'tk' => 'Tokelau',
            'tl' => 'Timor Oriental',
            'tm' => 'Turkmenistán',
            'tn' => 'Túnez',
            'to' => 'Tonga',
            'tr' => 'Turquía',
            'tt' => 'Trinidad y Tobago',
            'tv' => 'Tuvalu',
            'tw' => 'Taiwán',
            'tz' => 'Tanzania',
            'ua' => 'Ucrania',
            'ug' => 'Uganda',
            'um' => 'Islas menores alejadas de los Estados Unidos',
            'us' => 'EE. UU.',
            'uy' => 'Uruguay',
            'uz' => 'Uzbekistán',
            'va' => 'Ciudad del Vaticano',
            'vc' => 'San Vicente y las Granadinas',
            've' => 'Venezuela',
            'vg' => 'Islas Vírgenes Británicas',
            'vi' => 'Islas Vírgenes de los Estados Unidos',
            'vn' => 'Vietnam',
            'vu' => 'Vanuatu',
            'wf' => 'Wallis y Futuna',
            'ws' => 'Samoa',
            'xk' => 'Kosovo',
            'ye' => 'Yemen',
            'yt' => 'Mayotte',
            'za' => 'Sudáfrica',
            'zm' => 'Zambia',
            'zw' => 'Zimbabue',
        );
        asort($vec_countries);

        foreach ( $vec_countries as $country_code => $name )
        {
            $this->insert('country', array(
                'country_code'  => $country_code,
                'name'          => $name,
                'created_uid'   => 1,
                'created_date'  => time(),
                'updated_uid'   => 1,
                'updated_date'  => time(),
                'uuid' => $this->uuid(),
            ));
        }
    }
}

