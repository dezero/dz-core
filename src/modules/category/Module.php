<?php
/**
 * Module to manage category entities
 */

namespace dz\modules\category;

class Module extends \dz\web\Module
{
    /**
     * @var array mapping from controller ID to controller configurations.
     */
    public $controllerMap = [
        'category' => [
            'class' => 'dz\modules\category\controllers\CategoryController',
        ],
        'tag' => [
            'class' => 'dz\modules\category\controllers\TagController',
        ],
    ];

    
    /**
     * Default controller
     */
    public $defaultController = 'category';


    /**
     * Load specific CSS or JS files for this module
     */
    public $cssFiles = null; // ['category.css'];
    public $jsFiles = null; // ['category.js'];


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        parent::init();
    }
}
