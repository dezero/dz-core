<?php
/*
|--------------------------------------------------------------------------
| Controller class for Category model (category_type = "category")
|--------------------------------------------------------------------------
*/

namespace dz\modules\category\controllers;

use dz\helpers\Html;
use dz\modules\category\models\Category;
use dz\modules\category\controllers\_base\BaseCategoryController;
use dz\web\Controller;
use Yii;

class CategoryController extends BaseCategoryController
{   
    /**
     * Category type = CATEGORY
     */
    protected $category_type = 'category';
}