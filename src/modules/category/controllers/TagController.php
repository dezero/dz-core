<?php
/*
|--------------------------------------------------------------------------
| Controller class for Category model (category_type = "tag")
|--------------------------------------------------------------------------
*/

namespace dz\modules\category\controllers;

use dz\helpers\Html;
use dz\modules\category\models\Category;
use dz\modules\category\controllers\_base\BaseCategoryController;
use dz\web\Controller;
use Yii;

class TagController extends BaseCategoryController
{   
    /**
     * Category type = TAG
     */
    protected $category_type = 'tag';
}