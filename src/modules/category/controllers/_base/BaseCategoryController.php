<?php
/*
|--------------------------------------------------------------------------
| Base controller class for Category model
|
| Common class methods used in different controllers of category module
|--------------------------------------------------------------------------
*/

namespace dz\modules\category\controllers\_base;

use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\Transliteration;
use dz\modules\asset\models\AssetImage;
use dz\modules\category\models\Category;
use dz\modules\category\models\TranslatedCategory;
use dz\modules\web\models\Seo;
use dz\web\Controller;
use Yii;

abstract class BaseCategoryController extends Controller
{
    /**
     * Category type
     */
    protected $category_type;


    /**
     * List action for Category models
     */
    public function actionIndex()
    {
        $category_model = Yii::createObject(Category::class, 'search');
        $category_model->unsetAttributes();
        $category_model->category_type = $this->category_type;
        $category_model->category_parent_id = 0;
        
        if ( isset($_GET['Category']) )
        {
            $category_model->setAttributes($_GET['Category']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }

        $this->render($category_model->get_view_path('index'), [
            'category_model'    => $category_model,
            'vec_config'        => $category_model->get_config()
        ]);
    }


    /**
     * Create action for Category model
     */
    public function actionCreate()
    {
        // Category model
        $category_model = Yii::createObject(Category::class);
        $category_model->category_type = $this->category_type;

        // Subcategory - Get channel_id from parent
        /*
        if ( isset($_GET['parent_id']) )
        {
            $parent_category_model = Category::findOne($_GET['parent_id']);
            if ( $parent_category_model && $parent_category_model->category_type == $this->category_type )
            {
                $category_model->channel_id = $parent_category_model->channel_id;
            }
        }
        */

        // Create action
        $vec_data = Yii::app()->categoryManager->create_category($category_model, $_POST);

        // Render create template
        $this->render($category_model->get_view_path('create'), $vec_data);
    }


    /**
     * Update action for Category model
     */
    public function actionUpdate($id)
    {
        // Category model
        $category_model = $this->loadModel($id, Category::class);

        // Update action
        $vec_data = Yii::app()->categoryManager->update_category($category_model, $_POST);

        // Render update template
        $this->render($category_model->get_view_path('update'), $vec_data);
    }


    /**
     * Update category weights
     */
    public function actionUpdateWeight($category_id)
    {
        // Delete action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsPostRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        if ( isset($_POST['nestable']) && !empty($_POST['nestable']) && is_array($_POST['nestable']) )
        {
            $vec_nestable = $_POST['nestable'];

            // 1st level (no parents)
            if ( $category_id == 0 )
            {
                foreach ( $vec_nestable as $num_category => $que_category )
                {
                    if ( isset($que_category['id']) )
                    {
                        // UPDATE menu weight
                        Yii::app()->db->createCommand()->update('category',
                            ['weight' => $num_category+1],
                            'category_id = :category_id',
                            [':category_id' => $que_category['id']]);
                    }
                }
            }

            // 2nd LEVEL
            else if ( isset($vec_nestable[0]['id']) && $vec_nestable[0]['id'] == $category_id )
            {
                // Check if current category has subactegories (children)
                if ( isset($vec_nestable[0]['children']) && !empty($vec_nestable[0]['children']) )
                {
                    $this->_update_children_weights($vec_nestable[0]['children']);
                }
            }

            echo Json::encode(['result' => 1]);
        }
        else
        {
            echo Json::encode(['result' => -1]);
        }

        // End application
        Yii::app()->end();
    }


    /**
     * Disable a Category model
     */
    public function actionDisable($id)
    {
        // Baja action only allowed by POST requests
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $category_model = $this->loadModel($id, Category::class);
            if ( $category_model->disable() )
            {
                if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
                {
                    Yii::app()->user->addFlash('success', Yii::t('app', $category_model->text('disable_success')) );
                }
                else
                {
                    echo Yii::t('app', $category_model->text('disable_success'));
                }
            }
            else
            {
                $this->showErrors($category_model->getErrors());
            }           
        }
        else
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }
    }
    
    
    
    /**
     * Enable a Category model
     */
    public function actionEnable($id)
    {
        // Alta action only allowed by POST requests
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $category_model = $this->loadModel($id, Category::class);
            if ( $category_model->enable() )
            {
                if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
                {
                    Yii::app()->user->addFlash('success', Yii::t('app', $category_model->text('enable_success')) );
                }
                else
                {
                    echo Yii::t('app', $category_model->text('enable_success'));
                }
            }
            else
            {
                $this->showErrors($category_model->getErrors());
            }           
        }
        else
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }
    }


    /**
     * Delete action for a Category model
     */
    public function actionDelete($id)
    {
        // Delete action only allowed by POST requests
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $category_model = $this->loadModel($id, Category::class);
            
            if ( $category_model->delete() )
            {
                if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
                {
                    Yii::app()->user->addFlash('success', Yii::t('app', $category_model->text('delete_success')) );
                }
                else
                {
                    echo Yii::t('app', $category_model->text('delete_success'));
                }
            }
            else
            {
                $this->showErrors($category_model->getErrors());
            }
            
            if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
            {
                $this->redirect(['index']);
            }
        }
        else
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }
    }

    
    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index'         => 'category.category.view',
            'create'        => 'category.category.create',
            'update'        => 'category.category.update',
            'updateWeight'  => 'category.category.update',
            'tree'          => 'category.category.view',
            'enable'        => 'category.category.update',
            'disable'       => 'category.category.update',
            'delete'        => 'category.category.update',
        ];
    }


    /**
     * Custom texts for the category type
     * 
     * Inherited method from an OLD VERSION when $category_model->text() method didn't exist
     */
    protected function text($text_key)
    {
        $category_model = Yii::createObject(Category::class);
        $category_model->category_type = $this->category_type;
        return $category_model->text($text_key);
    }


    /**
     * Update children weights in a recursivelly way
     */
    private function _update_children_weights($vec_children = [])
    {
        if ( !empty($vec_children) )
        {
            foreach ( $vec_children as $num_category => $que_category )
            {
                if ( isset($que_category['id']) )
                {
                    $subcategory_id = substr($que_category['id'], 2);

                    // UPDATE category weight
                    Yii::app()->db->createCommand()->update('category',
                        ['weight' => $num_category+1],
                        'category_id = :category_id',
                        [':category_id' => $subcategory_id]);
                }

                if ( isset($que_category['children']) && !empty($que_category['children']) )
                {
                    $this->_update_children_weights($que_category['children']);
                }
            }
        }
    }
}