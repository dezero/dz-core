<?php
/**
 * Notifications Manager
 * 
 * Helper classes to work with Notification models
 */

namespace dz\modules\notification\components;

use Dz;
use dz\base\ApplicationComponent;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\MarkdownHelper;
use dz\helpers\StringHelper;
use dz\modules\notification\models\Notification;
use dz\modules\notification\models\NotificationTemplate;
use user\models\User;
use Yii;

class NotificationManager extends ApplicationComponent
{
    /**
     * @var object. NotificationTemplate model
     */
    public $notificationTemplate;


    /**
     * @var string. Language code
     */
    public $language_id;


    /**
     * @var array. User's destionation
     */
    public $vec_users;


    /**
     * @var array. Array with data for notification body
     */
    public $vec_data;


    /**
     * @var array. Array with tokens
     */
    public $vec_tokens;


    /**
     * Init function
     */
    public function init()
    {
        $this->vec_data = [];
        $this->vec_tokens = [];
        $this->vec_users = [];

        parent::init();
    }


    /**
     * Set one or more users
     */
    public function setUsers($vec_users)
    {
        $this->vec_users = $vec_users;

        return $this;
    }


    /**
     * Set language
     */
    public function setLanguage($language_id)
    {
        if ( $language_id !== null && Yii::app()->i18n->is_enabled_language($language_id) )
        {
            $this->language_id = $language_id;
            $this->addData([
                'language_id' => $language_id
            ]);
        }
        return $this;
    }


    /**
     * Set a NotificationTemplate model
     */
    public function setTemplate($template)
    {
        // Template ALIAS is given as input param
        if ( is_string($template) )
        {
            $this->notificationTemplate = NotificationTemplate::model()->findByAlias($template, $this->language_id);
        }

        // Template MODEL is given as input param
        else
        {
            $this->notificationTemplate = $template;
        }
        
        // Set values from mail template
        if ( $this->notificationTemplate )
        {
            // 07/02/2022 - Add "mail_alias" into mail data
            if ( !empty($this->notificationTemplate->alias) )
            {
                $this->addData([
                    'template_alias' => $this->notificationTemplate->alias
                ]);
            }

            // Notification type
            if ( !empty($this->notificationTemplate->notification_type) )
            {
                $this->addData([
                    'notification_type' => $this->notificationTemplate->notification_type
                ]);
            }

            // Body
            if ( !empty($this->notificationTemplate->body) )
            {
                // Parse tokens
                $notification_body = $this->parseTokens($this->notificationTemplate->body);

                // Parse markdown
                $this->addData([
                    'body' => MarkdownHelper::convert($notification_body)
                ]);
            }

            // Add current user_model, by default
            if ( ! Dz::is_console() && Yii::app()->user->id > 0 && ! isset($this->vec_data['user']) )
            {
                $user_model = Yii::app()->user->model();
                $this->addModel($user_model);
            }
        }
        return $this;
    }


    /**
     * Creates a new notification message and composes its body content
     */
    public function compose($template = null, $vec_data = [])
    {
        // Add default tokens
        $this->addTokens(Yii::app()->mail->defaultTokens());

        // Set template
        if ( $template !== null )
        {
            $this->setTemplate($template);
        }

        // Set data
        if ( !empty($vec_data) )
        {
            $this->addData($vec_data);
        }

        return $this;
    }


    /**
     * Sends the notification
     */
    public function send()
    {
        // Destination users must be an array
        if ( !is_array($this->vec_users) )
        {
            $this->vec_users = [$this->vec_users];
        }

        // Get notification body
        $notification_body = $this->getBody();

        if ( !empty($this->vec_users) && !empty($notification_body) )
        {
            foreach ( $this->vec_users as $user_model )
            {
                // $user_model is an integer
                if ( ! is_object($user_model) && ( is_integer($user_model) || ctype_digit($user_model) ) )
                {
                    $user_model = User::findOne($user_model);
                }

                if ( $user_model && ! $user_model->is_disabled() && ! $user_model->is_banned() )
                {
                    $notification_model = Yii::createObject(Notification::class);
                    $notification_model->setAttributes([
                        'user_id'           => $user_model->id,
                        'notification_type' => isset($this->vec_data['notification_type']) ? $this->vec_data['notification_type'] : 'default',
                        'content'           => $notification_body,
                        'language_id'       => isset($this->vec_data['language_id']) ? $this->vec_data['language_id'] : Yii::currentLanguage(),
                        'template_id'       => $this->notificationTemplate ? $this->notificationTemplate->template_id : null,
                        'template_alias'    => $this->notificationTemplate ? $this->notificationTemplate->alias : null,
                        'entity_id'         => isset($this->vec_data['entity_id']) ? $this->vec_data['entity_id'] : null,
                        'entity_type'       => isset($this->vec_data['entity_type']) ? $this->vec_data['entity_type'] : null,
                    ]);

                    // Custom event "beforeSend"
                    $notification_model = $this->beforeSend($notification_model);

                    if ( ! $notification_model->save() )
                    {
                        Log::save_model_error($notification_model);
                    }

                    // Custom event "afterSend"
                    $this->afterSend($notification_model);
                }
            }

            return true;
        }

        return false;
    }



    /**
     * Event triggered before a notification is sent
     */
    public function beforeSend($notification_model)
    {
        return $notification_model;
    }


    /**
     * Event triggered after a notification is sent
     */
    public function afterSend($notification_model)
    {
    }


    /**
     * Set text body of the notification directly
     */
    public function setBody($body)
    {
        // Parse tokens
        $notification_body = $this->parseTokens($body);

        // Parse markdown
        $this->addData([
            'body' => MarkdownHelper::convert($notification_body)
        ]);

        return $this;
    }


    /**
     * Return rendered body
     */
    public function getBody()
    {
        return isset($this->vec_data['body']) ? $this->vec_data['body'] : '';
    }


    /**
     * Set notification type
     */
    public function setType($notification_type)
    {
        $this->addData([
            'notification_type' => $notification_type
        ]);

        return $this;
    }


    /**
     * Add data to be used in mail body
     */
    public function addData($vec_data)
    {
        if ( is_array($vec_data) )
        {
            if ( !empty($vec_data) )
            {
                $this->vec_data = \CMap::mergeArray($this->vec_data, $vec_data);
            }
            else
            {
                $this->vec_data = $vec_data;
            }
        }
        return $this;
    }


    /**
     * Set entity information
     */
    public function setEntityInfo($entity_id, $entity_type)
    {
        $this->addData([
            'entity_id' => $entity_id,
            'entity_type' => $entity_type,
        ]);

        return $this;
    }


    /*
    |--------------------------------------------------------------------------------------
    | TOKENS
    |--------------------------------------------------------------------------------------
    */


    /**
     * Add new tokens
     */
    public function addTokens($vec_tokens)
    {
        if ( is_array($vec_tokens) && !empty($vec_tokens) )
        {
            $this->vec_tokens = \CMap::mergeArray($this->vec_tokens, $vec_tokens);
        }

        return $this;
    }


    /**
     * Add new model class
     * 
     * It also adds the tokens returned by the mailTokens method (it it exists)
     */
    public function addModel($model, $data_name = '')
    {
        // Add model
        if ( is_object($model) )
        {
            // Use model class name in lowercase as data for the email body
            if ( empty($data_name) )
            {
                $data_name = StringHelper::strtolower(StringHelper::basename(get_class($model)));
            }

            // Add new data for the email body
            $this->addData([
                $data_name => $model
            ]);

            // Add tokens?
            if ( method_exists($model, 'mailTokens') )
            {
                $this->addTokens($model->mailTokens());
            }
        }

        return $this;
    }


    /**
     * Parse tokens in a text
     */
    public function parseTokens($text)
    {
        if ( !empty($this->vec_tokens) )
        {
            return str_replace(array_keys($this->vec_tokens), $this->vec_tokens, $text);
        }

        return $text;
    }



    /*
    |--------------------------------------------------------------------------------------
    | TOTALS
    |--------------------------------------------------------------------------------------
    */


    /**
     * Return total unread messages for an user
     */
    public function total_unread($user_id = null)
    {
        if ( empty($user_id) )
        {
            $user_id = Yii::app()->user->id;
        }

        if ( !empty($user_id) )
        {
            $notification_table_name = Notification::model()->tableName();
            return Yii::app()->db->createCommand("SELECT COUNT(*) FROM `{$notification_table_name}` t WHERE t.user_id = :user_id AND t.is_read = 0")->bindValue(':user_id', $user_id)->queryScalar();
        }

        return 0;
    }


    /**
     * Mark all notifications as READ for an user
     */
    public function mark_all_as_read($user_id = null)
    {
        if ( empty($user_id) )
        {
            $user_id = Yii::app()->user->id;
        }

        if ( !empty($user_id) )
        {
            $notification_table_name = Notification::model()->tableName();
            return Yii::app()->db->createCommand("UPDATE `{$notification_table_name}` t SET t.is_read = 1, t.read_date = :now WHERE t.user_id = :user_id AND t.is_read = 0")->bindValue(':now', time())->bindValue(':user_id', $user_id)->execute();
        }

        return false;
    }
}