<?php
/**
 * @package dz\modules\notification\models 
 */

namespace dz\modules\notification\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\notification\models\_base\NotificationTemplate as BaseNotificationTemplate;
use dz\modules\notification\models\Notification;
use user\models\User;
use Yii;

/**
 * NotificationTemplate model class for "notification_template" database table
 *
 * Columns in table "notification_template" available as properties of the model,
 * followed by relations of table "notification_template" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $template_id
 * @property string $alias
 * @property string $language_id
 * @property string $name
 * @property string $description
 * @property string $notification_type
 * @property string $body
 * @property integer $disable_date
 * @property integer $disable_uid
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $notifications
 * @property mixed $createdUser
 * @property mixed $disableUser
 * @property mixed $language
 * @property mixed $updatedUser
 */
class NotificationTemplate extends BaseNotificationTemplate
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['alias, name, created_date, created_uid, updated_date, updated_uid', 'required'],
			['disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['alias', 'length', 'max'=> 64],
			['language_id', 'length', 'max'=> 4],
			['name', 'length', 'max'=> 128],
			['notification_type', 'length', 'max'=> 32],
			['uuid', 'length', 'max'=> 36],
			['language_id, description, notification_type, body, disable_date, disable_uid, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['description, body', 'safe'],
			['template_id, alias, language_id, name, description, notification_type, body, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],

            // Custom rules
            ['alias', 'validate_alias', 'on' => 'insert'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'notifications' => [self::HAS_MANY, Notification::class, 'template_id'],
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'disableUser' => [self::BELONGS_TO, User::class, ['disable_uid' => 'id']],
			'language' => [self::BELONGS_TO, Language::class, 'language_id'],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
            'translations' => [self::HAS_MANY, NotificationTemplate::class, ['alias' => 'alias'], 'condition' => 'translations.language_id <> t.language_id'],
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'template_id' => Yii::t('app', 'Template'),
			'alias' => Yii::t('app', 'Alias'),
			'language_id' => null,
			'name' => Yii::t('app', 'Name'),
			'description' => Yii::t('app', 'Description'),
			'notification_type' => Yii::t('app', 'Notification Type'),
			'body' => Yii::t('app', 'Body'),
			'disable_date' => Yii::t('app', 'Disable Date'),
			'disable_uid' => null,
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'notifications' => null,
			'createdUser' => null,
			'disableUser' => null,
			'language' => null,
			'updatedUser' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.template_id', $this->template_id);
        $criteria->compare('t.language_id', $this->language_id);
        $criteria->compare('t.alias', $this->alias, true);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.description', $this->description, true);
        $criteria->compare('t.notification_type', $this->notification_type, true);
        $criteria->compare('t.body', $this->body, true);
        $criteria->compare('t.disable_date', $this->disable_date);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['template_id' => true]]
        ]);
    }


    /**
     * NotificationTemplate models list
     * 
     * @return array
     */
    public function notificationtemplate_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['template_id', 'name'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = NotificationTemplate::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('template_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Disables NotificationTemplate model
     */
    public function disable()
    {
        if ( parent::disable() )
        {
            if ( $this->language_id === Yii::app()->i18n->get_default_language() && $this->translations )
            {
                foreach ( $this->translations as $translated_notification_model )
                {
                    $translated_notification_model->disable();
                }
            }

            return true;
        }

        return false;
    }


    /**
     * Enables NotificationTemplate model
     */
    public function enable()
    {
        if ( parent::enable() )
        {
            if ( $this->language_id === Yii::app()->i18n->get_default_language() && $this->translations )
            {
                foreach ( $this->translations as $translated_notification_model )
                {
                    $translated_notification_model->enable();
                }
            }

            return true;
        }

        return false;
    }


    /**
     * Deletes the model
     */
    public function delete()
    {
        // TRANSLATIONS
        if ( $this->language_id === Yii::app()->i18n->get_default_language() && $this->translations )
        {
            foreach ( $this->translations as $translated_notification_model )
            {
                $translated_notification_model->delete();
            }
        }

        // Going on with "delete" action
        return parent::delete();
    }


    /**
     * Get the translated NotificationTemplate model given a language
     */
    public function get_translation($language_id)
    {
        return NotificationTemplate::get()
            ->where([
                'alias'         => $this->alias,
                'language_id'   => $language_id
            ])
            ->one();
    }


    /**
     * Get same template translated into another language
     */
    public function get_translated_model($language_id)
    {
        if ( $language_id != $this->language_id )
        {
            $translated_notification_model = $this->findByAlias($this->alias, $language_id);
            if ( !empty($translated_notification_model) )
            {
                // Assign same values from original language
                // $translated_notification_model->vec_extra_data = $this->vec_extra_data;
                return $translated_notification_model;
            }
        }

        return $this;
    }


    /**
     * Save translation
     */
    public function save_translation($vec_attributes, $language_id)
    {
        $vec_attributes['language_id'] = $language_id;
        $translated_model = $this->get_translation($language_id);

        if ( ! $translated_model )
        {
            $translated_model = Yii::createObject(NotificationTemplate::class);
            $translated_model->alias = $this->alias;
        }

        $translated_model->setAttributes($vec_attributes);
        return $translated_model->save();
    }


     /**
     * Save a translation model
     */
    public function save_translation_model($translated_notification_model)
    {
        if ( empty($translated_notification_model->template_id) || $translated_notification_model->alias == $this->alias )
        {
            $translated_notification_model->alias = $this->alias;
            $translated_notification_model->name = $this->name;
            $translated_notification_model->description = $this->description;
            $translated_notification_model->notification_type = $this->notification_type;

            if ( ! $translated_notification_model->save() )
            {
                Log::save_model_error($translated_notification_model);
                return false;
            }

            return true;
        }

        return false;
    }


    /**
     * Validate alias attribute
     */
    public function validate_alias($attribute, $params)
    {
        $alias_mail_model = self::findByAlias($this->alias, $this->language_id);
        if ( $alias_mail_model )
        {
            $this->addError('alias',  Yii::t('app', 'It already exists a notification template with alias <em>'. $this->alias .'</em>.'));
        }
    }
}