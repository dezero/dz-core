<?php
/**
 * @package dz\modules\notification\models 
 */

namespace dz\modules\notification\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\notification\models\_base\Notification as BaseNotification;
use dz\modules\notification\models\NotificationTemplate;
use user\models\User;
use Yii;

/**
 * Notification model class for "notification" database table
 *
 * Columns in table "notification" available as properties of the model,
 * followed by relations of table "notification" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $notification_id
 * @property integer $user_id
 * @property string $notification_type
 * @property string $content
 * @property integer $read_date
 * @property integer $is_read
 * @property string $language_id
 * @property integer $template_id
 * @property string $template_alias
 * @property string $entity_id
 * @property string $entity_type
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $language
 * @property mixed $template
 * @property mixed $updatedUser
 * @property mixed $user
 */
class Notification extends BaseNotification
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['user_id, created_date, created_uid, updated_date, updated_uid', 'required'],
			['user_id, read_date, is_read, template_id, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['notification_type, entity_type', 'length', 'max'=> 32],
			['language_id', 'length', 'max'=> 4],
			['template_alias, entity_id', 'length', 'max'=> 64],
			['uuid', 'length', 'max'=> 36],
			['notification_type, content, read_date, is_read, language_id, template_id, template_alias, entity_id, entity_type, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['content', 'safe'],
			['notification_id, user_id, notification_type, content, read_date, is_read, language_id, template_id, template_alias, entity_id, entity_type, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'user' => [self::BELONGS_TO, User::class, 'user_id'],
            'language' => [self::BELONGS_TO, Language::class, 'language_id'],
            'template' => [self::BELONGS_TO, NotificationTemplate::class, 'template_id'],
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'read_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'notification_id' => Yii::t('app', 'Notification'),
			'user_id' => Yii::t('app', 'User'),
			'notification_type' => Yii::t('app', 'Notification Type'),
			'content' => Yii::t('app', 'Content'),
			'read_date' => Yii::t('app', 'Read Date'),
			'is_read' => Yii::t('app', 'Is Read'),
			'language_id' => Yii::t('app', 'Language'),
            'template_id' => Yii::t('app', 'Template'),
            'template_alias' => Yii::t('app', 'Template Alias'),
			'entity_id' => Yii::t('app', 'Entity'),
			'entity_type' => Yii::t('app', 'Entity Type'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'createdUser' => null,
			'language' => Yii::t('app', 'Language'),
            'template' => Yii::t('app', 'Template'),
			'updatedUser' => null,
			'user' => Yii::t('app', 'User'),
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.notification_id', $this->notification_id);
        $criteria->compare('t.notification_type', $this->notification_type);
        $criteria->compare('t.content', $this->content, true);
        $criteria->compare('t.read_date', $this->read_date);
        $criteria->compare('t.is_read', $this->is_read);
        $criteria->compare('t.language_id', $this->language_id);
        $criteria->compare('t.template_id', $this->template_id);
        $criteria->compare('t.template_alias', $this->template_alias, true);
        $criteria->compare('t.entity_id', $this->entity_id);
        $criteria->compare('t.entity_type', $this->entity_type);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['notification_id' => true]]
        ]);
    }


    /**
     * Notification models list
     * 
     * @return array
     */
    public function notification_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['notification_id', 'notification_type'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = Notification::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('notification_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Event "beforeValidate"
     *
     * This method is invoked before validation starts
     *
     * @return bool
     */
    public function beforeValidate()
    {
        switch ( $this->scenario )
        {
            // Author user id. For example, when an user creates a new Comment
            case 'insert':
                if ( empty($this->user_id) )
                {
                    $this->user_id = (Yii::app() instanceof \CConsoleApplication) ? 1 : Yii::app()->user->id;
                }
            break;
        }

        return parent::beforeValidate();
    }


    /**
     * Return content. Decodes from JSON to array, if it's needed
     */
    public function get_content()
    {
        if ( StringHelper::is_json($this->content) )
        {
            return Json::decode($this->content);
        }

        return $this->content;
    }


    /**
     * Mark this Notification as read
     */
    public function mark_as_read($is_save = false)
    {
        if ( $this->is_read == 0 )
        {
            $this->read_date = time();
            $this->is_read = 1;

            if ( $is_save )
            {
                if ( ! $this->save() )
                {
                    Log::save_model_error($this);
                    return false;
                }
            }
        }

        return true;
    }


    /**
     * Mark this Notification as unread
     */
    public function mark_as_unread($is_save = false)
    {
        if ( $this->is_read == 1 )
        {
            $this->read_date = null;
            $this->is_read = 0;

            if ( $is_save )
            {
                if ( ! $this->save() )
                {
                    Log::save_model_error($this);
                    return false;
                }
            }
        }

        return true;
    }


    /**
     * Return notification view URL
     */
    public function url()
    {
        return Url::to('/notification/notification/view', ['id' => $this->notification_id]);
    }
}