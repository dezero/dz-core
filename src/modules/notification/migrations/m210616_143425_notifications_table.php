<?php
/**
 * Migration class m210616_143425_notifications_table
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210616_143425_notifications_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "notification" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('notification', true);

        $this->createTable('notification', [
            'notification_id' => $this->primaryKey(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'notification_type' => $this->string(32)->notNull()->defaultValue('default'),
            'content' => $this->text(),
            'read_date' => $this->date(),
            'is_read' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'language_id' => $this->string(4)->notNull()->defaultValue('es'),
            'entity_id' => $this->string(64),
            'entity_type' => $this->string(32),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);
    
        // Create indexes
        $this->createIndex(null, 'notification', ['notification_type'], false);
        $this->createIndex(null, 'notification', ['is_read'], false);
        $this->createIndex(null, 'notification', ['entity_id', 'entity_type'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'notification', ['user_id'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'notification', ['language_id'], 'language', ['language_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'notification', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'notification', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

        
        // Permissions
        // -------------------------------------------------------------------------
        $this->insertMultiple('user_auth_item', [
            [
                'name'          => 'notification.notification.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Notifications - Notification - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'notification.notification.update',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Notifications - Notification - Edit notifications',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'notification_manage',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Notifications - Full access to notifications',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'notification_edit',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Notifications - Edit notifications',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'notification_view',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Notifications - View notifications',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
        ]);

        // Set items relationships
        $this->insertMultiple('user_auth_item_child', [
            [
                'parent'    => 'notification_manage',
                'child'     => 'notification.notification.*'
            ],
            [
                'parent'    => 'notification_edit',
                'child'     => 'notification.notification.update'
            ],
            [
                'parent'    => 'admin',
                'child'     => 'notification_manage'
            ],
            [
                'parent'    => 'editor',
                'child'     => 'notification_edit'
            ],
        ]);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

