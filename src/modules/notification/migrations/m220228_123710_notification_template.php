<?php
/**
 * Migration class m220228_123710_notification_template
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m220228_123710_notification_template extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Create "notification_template" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('notification_template', true);

        $this->createTable('notification_template', [
            'template_id' => $this->primaryKey(),
            'alias' => $this->string(64)->notNull(),
            'language_id' => $this->string(4)->notNull()->defaultValue('es'),
            'name' => $this->string(128)->notNull(),
            'description' => $this->text(),
            'notification_type' => $this->string(32)->notNull()->defaultValue('default'),
            'body' => $this->text(),
            'disable_date' => $this->date(),
            'disable_uid' => $this->integer()->unsigned(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);
    
        // Create indexes
        $this->createIndex(null, 'notification_template', ['alias'], false);
        $this->createIndex(null, 'notification_template', ['alias', 'language_id'], false);
        $this->createIndex(null, 'notification_template', ['notification_type'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'notification_template', ['language_id'], 'language', ['language_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'notification_template', ['disable_uid'], 'user_users', ['id'], 'SET NULL', null);
        $this->addForeignKey(null, 'notification_template', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'notification_template', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

		
        // Add new columns "template_id" and "template_alias" into "notification" table
        $this->addColumn('notification', 'template_id', $this->integer()->unsigned()->after('language_id'));
        $this->addColumn('notification', 'template_alias', $this->string(64)->after('template_id'));
        
        // Now, add index and foreign key
        $this->createIndex(null, 'notification', ['template_alias', 'language_id'], false);
        $this->addForeignKey(null, 'notification', ['template_id'], 'notification_template', ['template_id'], 'SET NULL', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

