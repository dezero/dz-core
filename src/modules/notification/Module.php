<?php
/**
 * Module to manage notifications
 */

namespace dz\modules\notification;

class Module extends \dz\web\Module
{
    /**
     * @var array mapping from controller ID to controller configurations.
     */
    public $controllerMap = [
        'notification' => [
            'class' => 'dz\modules\notification\controllers\NotificationController',
        ],
        'template' => [
            'class' => 'dz\modules\notification\controllers\TemplateController',
        ],
    ];

    /**
     * Default controller
     */
    public $defaultController = 'notification';


    /**
     * Load specific CSS or JS files for this module
     */
    public $cssFiles = null; // ['notification.css'];
    public $jsFiles = null; // ['notification.js'];


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        parent::init();
    }
}
