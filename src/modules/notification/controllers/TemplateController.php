<?php
/*
|--------------------------------------------------------------------------
| Controller class for NotificationTemplate model
|--------------------------------------------------------------------------
*/

namespace dz\modules\notification\controllers;

use dz\helpers\Html;
use dz\helpers\Log;
use dz\modules\notification\models\NotificationTemplate;
use dz\web\Controller;
use Yii;

class TemplateController extends Controller
{   
    /**
     * List action for NotificationTemplate models
     */
    public function actionIndex()
    {
        $model = Yii::createObject(NotificationTemplate::class, 'search');
        $model->unsetAttributes();
        $model->language_id = Yii::app()->language;
        
        if ( isset($_GET['NotificationTemplate']) )
        {
            $model->setAttributes($_GET['NotificationTemplate']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }
        
        // Render page
        $this->render('//notification/template/index', [
            'model' => $model,
        ]);
    }


    /**
     * Create action for NotificationTemplate model
     */
    public function actionCreate()
    {
        /*
        $community_message_model = $this->loadModel(80, \community\models\CommunityMessage::class);
        
        Yii::app()->notificationManager
            ->setCommunity(75)
            ->addModel($community_message_model, 'communityMessage')
            ->setEntityInfo($community_message_model->message_id, 'CommunityMessage')
            ->compose('community_doubt_new')
            ->send();
        die;
        */

        // NotificationTemplate model
        $notification_model = Yii::createObject(NotificationTemplate::class);
        $notification_model->language_id = Yii::app()->language;

        // Insert new model?
        if ( isset($_POST['NotificationTemplate']) )
        {
            $notification_model->setAttributes($_POST['NotificationTemplate']);

            if ( $notification_model->save() )
            {
                Yii::app()->user->addFlash('success', 'New notification template created successfully');

                // Redirect to update page
                $this->redirect(['update', 'id' => $notification_model->template_id]);
            }
        }

        $this->render('//notification/template/create', [
            'model'     => $notification_model,
            'form_id'   => 'notification-form'
        ]);
    }


    /**
     * Update action for NotificationTemplate model
     */
    public function actionUpdate($id)
    {   
        // NotificationTemplate model
        $notification_model = $this->loadModel($id, NotificationTemplate::class);

        // Default language
        $default_language = Yii::defaultLanguage();

        // Translated messages
        $vec_translated_models = [];
        $vec_extra_languages = Yii::app()->i18n->get_extra_languages();
        if ( !empty($vec_extra_languages) )
        {
            foreach ( $vec_extra_languages as $language_id )
            {
                $translated_notification_model = NotificationTemplate::get()
                    ->where([
                        'alias'         => $notification_model->alias,
                        'language_id'   => $language_id,
                    ])
                    ->one();
                    
                if ( ! $translated_notification_model )
                {
                    $translated_notification_model = Yii::createObject(NotificationTemplate::class);
                    $translated_notification_model->setAttributes($notification_model->raw_attributes);
                    $translated_notification_model->language_id = $language_id;
                    
                    // Attributes with different values from original
                    $translated_notification_model->body = null;
                    $translated_notification_model->created_date = null;
                    $translated_notification_model->created_uid = null;
                    $translated_notification_model->updated_date = null;
                    $translated_notification_model->updated_uid = null;
                    
                }
                $vec_translated_models[$language_id] = $translated_notification_model;
            }
        }

        // Insert new model?
        if ( isset($_POST['NotificationTemplate']) )
        {
            $notification_model->setAttributes($_POST['NotificationTemplate']);

            if ( $notification_model->save() )
            {
                $is_save_error = false;

                // Save translations
                if ( !empty($vec_translated_models) && isset($_POST['TranslatedNotification']) )
                {
                    foreach ( $_POST['TranslatedNotification'] as $language_id => $vec_translated_attributes )
                    {
                        if ( isset($vec_translated_models[$language_id]) && isset($vec_translated_attributes['body']) && !empty($vec_translated_attributes['body']) )
                        {
                            $translated_notification_model = $vec_translated_models[$language_id];
                            $vec_translated_attributes['language_id'] = $language_id;
                            $vec_translated_attributes['alias'] = $notification_model->alias;
                            $translated_notification_model->setAttributes($vec_translated_attributes);
                            if ( ! $translated_notification_model->save_translation_model($vec_translated_models[$language_id]) )
                            {
                                $is_save_error = true;
                            }
                        }
                    }
                }

                // Is error here?
                if ( $is_save_error )
                {
                    $vec_errors = Log::errors_list();
                    foreach ( $vec_errors as $que_error )
                    {
                        $notification_model->addError('template_id', $que_error);
                    }
                }
                else
                {
                    Yii::app()->user->addFlash('success', 'Notification template updated successfully');

                    // Redirect to update page
                    $this->redirect(['update', 'id' => $notification_model->template_id]);
                }
            }
        }

        $this->render('//notification/template/update', [
            'model'                     => $notification_model,
            'vec_translated_models'     => $vec_translated_models,
            'default_language'          => $default_language,
            'vec_extra_languages'       => $vec_extra_languages,
            'form_id'                   => 'notification-form',
        ]);
    }
}
