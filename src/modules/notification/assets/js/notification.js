/**
 * Scripts for NOTIFICATION module
 */
(function(document, window, $) {

  $(document).ready(function() {

    // NOTIFICATION TEMPLATES
    // ---------------------------------------------------------

    // PredefinedDocument form
    if ( $('#notification-form').size() > 0 ) {
      $.markdownLanguage('.notification-form-wrapper', 'TranslatedNotification', 'body');
    }
  });

})(document, window, jQuery);