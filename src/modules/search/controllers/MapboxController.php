<?php
/*
|--------------------------------------------------------------------------
| Controller class for Mapbox
|--------------------------------------------------------------------------
*/

namespace dz\modules\search\controllers;

use dz\db\DbCriteria;
use dz\helpers\Html;
use dz\helpers\Json;
use dz\web\Controller;
use Yii;

class MapboxController extends Controller
{
    /**
     * Reverse geocoding via Mapbox API v4
     *
     * It converts geographic coordinates into a text description.
     * For example, turning -77.050,38.889 into 2 Lincoln Memorial Circle NW
     *
     * https://api.mapbox.com/geocoding/v5/mapbox.places/3.21594%2C%2041.948.json?access_token=<YOUR_TOKEN>
     *
     * @see https://docs.mapbox.com/help/getting-started/geocoding/
     */
    public function actionReverseGeocode()
    {
        // Action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            throw new \CHttpException(400, Yii::t('backend', 'Your request is invalid.'));
        }

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        // Submitted form?
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $vec_ajax_output = [
                'error_msg'     => '',
                'error_code'    => 0,
            ];

            $vec_input = $this->jsonInput();

            // #1 - JSON input params are correct?
            if ( !empty($vec_input) && isset($vec_input['longitude']) && isset($vec_input['latitude']) && isset($vec_input['access_token']) )
            {
                $vec_ajax_output['longitude'] = $vec_input['longitude'];
                $vec_ajax_output['latitude'] = $vec_input['latitude'];
                $vec_ajax_output['response'] = Yii::app()->mapbox->reverse_geocode($vec_input['longitude'], $vec_input['latitude'], $vec_input['access_token']);
                $vec_ajax_output['address'] = Yii::app()->mapbox->decode_reverse_respone($vec_ajax_output['response']);
            }
            else
            {
                $vec_ajax_output['error_code'] = 201;
                $vec_ajax_output['error_msg'] = 'Access denied - JSON input params are incorrect';
            }

            // Return JSON and end application
            $this->jsonOutput(200, Json::encode($vec_ajax_output));
        }
    }
}
