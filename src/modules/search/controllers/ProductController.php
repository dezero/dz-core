<?php
/*
|--------------------------------------------------------------------------
| Controller class for SearchResult models
|--------------------------------------------------------------------------
*/

namespace dz\modules\search\controllers;

use dz\db\DbCriteria;
use dz\helpers\Html;
use dz\helpers\Json;
use dzlab\commerce\models\Product;
use dz\modules\search\models\SearchResult;
use dz\web\Controller;
use Yii;

class ProductController extends Controller
{   
    /**
     * List action for Product models
     */
    public function actionIndex()
    {
        $vec_output = [
            'results' => []
        ];

        $search_query = isset($_GET['q']) ? Yii::app()->request->get('q') : '';
        if ( !empty($search_query) )
        {
            $criteria = new DbCriteria;
            $criteria->compare('t.is_disabled', 0);
            $criteria->addCondition("t.name LIKE '%{$search_query}%' OR t.default_sku = '{$search_query}'");
            $criteria->order = 't.default_sku ASC, t.name ASC';
            $criteria->limit = 50;
            $vec_product_models = Product::model()->findAll($criteria);
            if ( $vec_product_models )
            {
                foreach ( $vec_product_models as $product_model )
                {
                    $vec_output['results'][] = [
                        'id'    => $product_model->product_id,
                        'text'  => !empty($product_model->default_sku) ? $product_model->default_sku .' - '. $product_model->name : $product_model->name
                    ];
                }
            }
        }

        // Return JSON and end application
        $this->jsonOutput(200, Json::encode($vec_output));
    }
}