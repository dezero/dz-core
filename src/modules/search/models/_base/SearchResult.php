<?php
/**
 * @package application\core\src\modules\search\models\_base
 */

namespace application\core\src\modules\search\models\_base;

use dz\db\ActiveRecord;
use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use user\models\User;
use Yii;

/**
 * DO NOT MODIFY THIS FILE! It is automatically generated by Gii.
 * If any changes are necessary, you must set or override the required
 * property or method in class "application\core\src\modules\search\models\SearchResult".
 *
 * This is the model base class for the table "search_result".
 * Columns in table "search_result" available as properties of the model,
 * followed by relations of table "search_result" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $result_id
 * @property integer $entity_id
 * @property string $entity_type
 * @property string $entity_code
 * @property string $name
 * @property string $description
 * @property integer $is_disabled
 * @property integer $updated_date
 * @property integer $updated_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $updatedUser
 */
abstract class SearchResult extends ActiveRecord
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * Returns the name of the associated database table
	 */
	public function tableName()
	{
		return 'search_result';
	}


	/**
	 * Label with translation support (from GIIX)
	 */
	public static function label($n = 1)
	{
		return Yii::t('app', 'SearchResult|SearchResults', $n);
	}


    /**
     * Returns the validation rules for attributes
     */
    public function rules()
    {
        return [
            ['updated_date, updated_uid', 'required'],
            ['entity_id, is_disabled, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
            ['entity_type, entity_code', 'length', 'max'=> 32],
            ['name', 'length', 'max'=> 128],
            ['description', 'length', 'max'=> 512],
            ['entity_id, entity_type, entity_code, name, description, is_disabled', 'default', 'setOnEmpty' => true, 'value' => null],
            ['result_id, entity_id, entity_type, entity_code, name, description, is_disabled, updated_date, updated_uid', 'safe', 'on' => 'search'],
        ];
    }


    /**
     * Define relations with other objects
     *
     * There are four types of relations that may exist between two active record objects:
     *   - BELONGS_TO: e.g. a member belongs to a team;
     *   - HAS_ONE: e.g. a member has at most one profile;
     *   - HAS_MANY: e.g. a team has many members;
     *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
     */
    public function relations()
    {
        return [
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],
        ];
    }


    /**
     * Returns the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'result_id' => Yii::t('app', 'Result'),
            'entity_id' => Yii::t('app', 'Entity'),
            'entity_type' => Yii::t('app', 'Entity Type'),
            'entity_code' => Yii::t('app', 'Entity Code'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'is_disabled' => Yii::t('app', 'Is Disabled'),
            'updated_date' => Yii::t('app', 'Updated Date'),
            'updated_uid' => null,
            'updatedUser' => null,
        ];
    }


    /**
     * SearchResult models list
     * 
     * @return array
     */
    public function searchresult_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['result_id', 'entity_type'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = SearchResult::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('result_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


	/**
	 * Generate an ActiveDataProvider for search form of this model
	 *
	 * Used in CGridView
	 */
	public function search()
	{
		$criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

		$criteria->compare('t.result_id', $this->result_id);
		$criteria->compare('t.entity_id', $this->entity_id);
		$criteria->compare('t.entity_type', $this->entity_type, true);
		$criteria->compare('t.entity_code', $this->entity_code, true);
		$criteria->compare('t.name', $this->name, true);
		$criteria->compare('t.description', $this->description, true);
		$criteria->compare('t.is_disabled', $this->is_disabled);
		$criteria->compare('t.updated_date', $this->updated_date);

		return new \CActiveDataProvider($this, [
			'criteria' => $criteria,
			'pagination' => ['pageSize' => 30],
			'sort' => ['defaultOrder' => ['result_id' => true]]
		]);
	}
	
	
	/**
	 * Returns fields not showed in create/update forms
	 */
	public function excludedFormFields()
	{
		return [
			'updated_date' => true, 
			'updated_uid' => true, 
		];
	}


    /**
     * Automatic string representation for a model (from GIIX)
     */
    public static function representingColumn()
    {
        return 'entity_type';
    }


    /**
     * Title used for this model
     */
    public function title()
    {
        return $this->entity_type;
    }
}