<?php
/**
 * Module to manage search
 */

namespace dz\modules\search;

class Module extends \dz\web\Module
{
    /**
     * @var array mapping from controller ID to controller configurations.
     */
    public $controllerMap = [
        'mapbox' => [
            'class' => 'dz\modules\search\controllers\MapboxController',
        ],

        'product' => [
            'class' => 'dz\modules\search\controllers\ProductController',
        ],

        'search' => [
            'class' => 'dz\modules\search\controllers\SearchController',
        ],
    ];

    /**
     * Default controller
     */
    public $defaultController = 'search';


    /**
     * Load specific CSS or JS files for this module
     */
    public $cssFiles = null; // ['search.css'];
    public $jsFiles = null; // ['search.js'];


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        parent::init();
    }
}
