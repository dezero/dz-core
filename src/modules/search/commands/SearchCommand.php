<?php
/**
 * SearchCommand class file
 *
 * This component is used to manage search results
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link https://www.dezero.es
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package commands
 */

namespace dz\modules\search\commands;

use dz\console\CronCommand;
use Yii;

class SearchCommand extends CronCommand
{
    /**
     * Load or reload search results for an entity type
     * 
     * ./yiic search reload --type=<entity_type>
     */
    public function actionReload($type)
    {
        
    }
}