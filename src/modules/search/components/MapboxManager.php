<?php

namespace dz\modules\search\components;

use dz\base\ApplicationComponent;
use dz\helpers\Json;
use dz\helpers\Log;
use Yii;

/**
 * Component to manage with Mapbox API
 */
class MapboxManager extends ApplicationComponent
{
    /**
     * Reverse geocoding via Mapbox API v4
     *
     * It converts geographic coordinates into a text description.
     * For example, turning -77.050,38.889 into 2 Lincoln Memorial Circle NW
     *
     * https://api.mapbox.com/geocoding/v5/mapbox.places/3.21594%2C%2041.948.json?access_token=<YOUR_TOKEN>
     *
     * @see https://docs.mapbox.com/help/getting-started/geocoding/
     */
    public function reverse_geocode($longitude, $latitude, $access_token)
    {
        // Import EHttpClient library (Yii port of Zend_Http_Client)
        Yii::import("@lib.EHttpClient.*");

        // Send GET request to Mapbox API
        $mapbox_url = "https://api.mapbox.com/geocoding/v5/mapbox.places/{$longitude}%2C%20{$latitude}.json";
        $client = new \EHttpClient($mapbox_url);
        $client->setMethod('GET');
        $client->setParameterGet([
            'access_token'  => $access_token
        ]);
        $response = $client->request();
        if ( $response && $response->isSuccessful() )
        {
            $vec_response = Json::decode($response->getBody());
            return $vec_response;
        }

        return [];
    }


    /**
     * Decode reverse geocoding response
     */
    public function decode_reverse_respone($vec_response)
    {
        $vec_output = [
            'address_line'      => '',
            'address_number'    => '',
            'postal_code'       => '',
            'city'              => '',
            'neighborhood'      => '',
            'province_name'     => '',
            'province_code'     => '',
            'country_name'      => '',
            'country_code'      => ''
        ];

        if ( !empty($vec_response) && isset($vec_response['features']) && is_array($vec_response['features']) )
        {
            foreach ( $vec_response['features'] as $vec_context_info )
            {
                if ( is_array($vec_context_info) && isset($vec_context_info['type']) && $vec_context_info['type'] === 'Feature' && isset($vec_context_info['place_type']) && isset($vec_context_info['place_type'][0]) && isset($vec_context_info['text']) )
                {
                    switch ( $vec_context_info['place_type'][0] )
                    {
                        case 'address':
                            $vec_output['address_line'] = $vec_context_info['text'];
                            if ( isset($vec_context_info['address']) && !empty($vec_context_info['address']) )
                            {
                                $vec_output['address_number'] = $vec_context_info['address'];
                            }
                        break;

                        case 'country':
                            $vec_output['country_name'] = $vec_context_info['text'];
                            if ( isset($vec_context_info['properties']) && !empty($vec_context_info['properties']) && isset($vec_context_info['properties']['short_code']) )
                            {
                                $vec_output['province_code'] = $vec_context_info['properties']['short_code'];
                            }
                        break;

                        case 'neighborhood':
                            $vec_output['neighborhood'] = $vec_context_info['text'];
                        break;

                        case 'postcode':
                            $vec_output['postal_code'] = $vec_context_info['text'];
                        break;

                        case 'place':
                            $vec_output['city'] = $vec_context_info['text'];
                        break;

                        case 'region':
                            $vec_output['province_name'] = $vec_context_info['text'];
                            if ( isset($vec_context_info['properties']) && !empty($vec_context_info['properties']) && isset($vec_context_info['properties']['short_code']) )
                            {
                                $vec_output['country_code'] = $vec_context_info['properties']['short_code'];
                            }
                        break;
                    }
                }
            }
        }

        return $vec_output;
    }


    /**
     * Forward geocoding with search text input via Mapbox Geocoding API v6
     *
     * The forward geocoding query type allows you to look up a location using a string
     * of search text and returns its standardized address, geographic context, and coordinates.
     *
     * @see https://docs.mapbox.com/api/search/geocoding/#forward-geocoding-with-search-text-input
     */
    public function search($query, $access_token, $vec_options = [])
    {
        // Default options
        if ( empty($vec_options) )
        {
            $vec_options = [
                'limit' => 5,
                'types' => 'street,address',
                // 'proximity' => 'ip',
                'proximity' => '2.1734,41.3851',    // Barcelona coordinates
                'autocomplete' => true,
                'fuzzyMatch' => true,
                'language' => 'es',
            ];
        }

        // Build all the input GET paremeters
        $vec_parameters = \CMap::mergeArray([
            'q' => $query,
            'access_token'  => $access_token
        ], $vec_options);

        // Import EHttpClient library (Yii port of Zend_Http_Client)
        Yii::import("@lib.EHttpClient.*");

        // Send GET request to Mapbox API
        $mapbox_url = "https://api.mapbox.com/search/geocode/v6/forward";
        $client = new \EHttpClient($mapbox_url);
        $client->setMethod('GET');
        $client->setParameterGet($vec_parameters);
        $response = $client->request();
        if ( $response && $response->isSuccessful() )
        {
            $vec_response = Json::decode($response->getBody());
            return $vec_response;
        }

        return [];
    }


    /**
     * Decode forward geocoding response from Mapbox Geocoding API v6
     *
     * @see https://docs.mapbox.com/api/search/geocoding/#geocoding-response-object
     */
    public function decode_response_v6($vec_response)
    {
        $vec_output = [];

        if ( !empty($vec_response) && isset($vec_response['features']) && is_array($vec_response['features']) )
        {
            foreach ( $vec_response['features'] as $num_place => $vec_place )
            {
                if ( isset($vec_place['type']) && $vec_place['type'] === 'Feature' && isset($vec_place['properties']) && isset($vec_place['properties']['context']) )
                {
                    $vec_output_place = [
                        'address_line'      => isset($vec_place['properties']['name_preferred']) ? $vec_place['properties']['name_preferred'] : '',
                        'address_number'    => '',
                        'street_name'       => '',
                        'postal_code'       => '',
                        'city'              => '',
                        'province_name'     => '',
                        'province_code'     => '',
                        'country_name'      => '',
                        'country_code'      => ''
                    ];
                    foreach ( $vec_place['properties']['context'] as $context_type => $vec_context_info )
                    {
                        switch ( $context_type )
                        {
                            case 'address':
                                // $vec_output_place['address_line'] = $vec_context_info['name'];
                                $vec_output_place['address_number'] = isset($vec_context_info['address_number']) ? $vec_context_info['address_number'] : '';
                                $vec_output_place['street_name'] = isset($vec_context_info['street_name']) ? $vec_context_info['street_name'] : '';
                            break;

                            case 'street':
                                $vec_output_place['street_name'] = $vec_context_info['name'];
                                // if ( empty($vec_context_info['address_line']) )
                                // {
                                //     $vec_output_place['address_line'] = $vec_context_info['name'];
                                // }
                            break;

                            case 'country':
                                $vec_output_place['country_name'] = $vec_context_info['name'];
                                $vec_output_place['country_code'] = isset($vec_context_info['country_code']) ? $vec_context_info['country_code'] : '';
                            break;

                            case 'postcode':
                                $vec_output_place['postal_code'] = $vec_context_info['name'];
                            break;

                            case 'place':
                                $vec_output_place['city'] = $vec_context_info['name'];
                            break;

                            case 'region':
                                $vec_output_place['province_name'] = $vec_context_info['name'];
                                $vec_output_place['province_code'] = isset($vec_context_info['region_code']) ? $vec_context_info['region_code'] : '';

                                // Remove prefix "provincia de" or "estado de"
                                if ( !empty($vec_output_place['province_name']) )
                                {
                                    $vec_output_place['province_name'] = strtr($vec_output_place['province_name'], [
                                        'provincia de ' => '',
                                        'Provincia de ' => '',
                                        'estado de ' => '',
                                        'Estado de ' => '',
                                    ]);
                                }
                            break;
                        }
                    }

                    $vec_output[] = $vec_output_place;
                }
            }
        }

        return $vec_output;
    }
}
