<?php

namespace dz\modules\search\components;

use dz\base\ApplicationComponent;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\helpers\Url;
use dzlab\commerce\models\Product;
use dzlab\commerce\models\SearchProduct;
use dz\modules\search\models\SearchResult;
use user\models\User;
use Yii;

/**
 * Search component to manage search results
 */
class SearchManager extends ApplicationComponent
{
    /**
     * Execute a database query
     */
    public function query($entity_type, $query, $limit = 10)
    {
        $vec_output = '';
        $sql  = 'SELECT t.entity_id, t.name, t.description FROM {{search_result}} t WHERE t.entity_type = :entity_type AND t.is_disabled = 0 AND (t.name LIKE "%:query%" OR t.description "%:query%") ';
        $sql .= 'ORDER BY CASE WHEN name LIKE ":query%" THEN 0 ELSE 1 END, CASE WHEN description LIKE "%:query%" THEN 0 ELSE 1 END ';
        $sql .= 'LIMIT 0, :limit';
        $vec_results = Yii::app()->db
            ->createCommand($sql)
            ->bindValue(':entity_type', $entity_type)
            ->bindValue(':query', $query)
            ->bindValue(':limit', $limit)
            ->queryAll();

        if ( !empty($vec_results) )
        {
            foreach ( $vec_results as $que_result )
            {
                $que_title = !empty($que_result['description']) ? $que_result['name'] .' '. $que_result['description'] : $que_result['name'];

                $vec_output[] = [
                    'id'        => $que_result['entity_id'],
                    'title'     => $que_title,
                ];
            }
        }

        return $vec_output;
    }


    /**
     * Reload all the search results caches
     */
    public function reload_caches_all()
    {
        Yii::app()->searchManager->reload_caches_product();
    }


    /**
     * Reload product cache search results
     */
    public function reload_caches_product()
    {
        $total_cache_products = 0;
        Yii::app()->db->createCommand()->truncateTable(SearchProduct::model()->tableName());

        $vec_product_models = Product::model()->findAll();
        if ( !empty($vec_product_models) )
        {
            foreach ( $vec_product_models as $product_model )
            {
                if ( $product_model->update_search_product() )
                {
                    $total_cache_products++;
                }
            }
        }

        return $total_cache_products;
    }
}