<?php
/**
 * Migration class m200529_093049_search_results_table
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200529_093049_search_results_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "search_result" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('search_result', true);

        $this->createTable('search_result', [
            'result_id' => $this->primaryKey(),
            'entity_id' => $this->integer()->unsigned(),
            'entity_type' => $this->string(32),
            'entity_code' => $this->string(32),
            'name' => $this->string(128),
            'description' => $this->string(512),
            'is_disabled' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
        ]);
    
        // Create indexes
        $this->createIndex(null, 'search_result', ['entity_id', 'entity_type'], false);
        $this->createIndex(null, 'search_result', ['entity_type', 'is_disabled'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'search_result', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('search_result');
		return false;
	}
}

