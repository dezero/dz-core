<?php
/*
|--------------------------------------------------------------------------
| Users Resource class for REST API
|--------------------------------------------------------------------------
*/

namespace dz\modules\api\resources;

use dz\db\DbCriteria;
use dz\rest\RestResource;
use Yii;

class UsersResource extends RestResource
{
    /**
     * Validate input params
     */
    public function validate($action = 'index', $id = '')
    {
        if ( ! $this->is_validated )
        {
            // Cliente list - GET /api/v1/users/{session_id}/session
            if ( $action === 'session' && $this->request_type === 'GET' )
            {
                $this->_validate_session();
            }
        }

        return $this->is_validated;
    }


    /**
     * Return session status
     */
    public function session_status($session_id)
    {
        // Prepare common REST API output
        $this->vec_response = [
            'status_code'   => 1,
            'errors'        => [],
            'session_id'    => null,
            'is_logged_in'  => false,
            'expire_date'   => null
        ];

        $vec_session_data = Yii::app()->db->createCommand('SELECT id, user_id, expire, data FROM user_session WHERE id = :session_id')->bindValue(':session_id', $this->vec_input['id'])->queryRow();
        if ( ! empty($vec_session_data) && isset($vec_session_data['expire']) )
        {
            $now = time();
            $session_decoded_data = Yii::app()->session->readSession($vec_session_data['id']);
            if ( !empty($session_decoded_data) && preg_match("/\_\_id/", $session_decoded_data) && $vec_session_data['expire'] > $now )
            {
                $this->vec_response['session_id'] = $vec_session_data['id'];
                $this->vec_response['is_logged_in'] = true;
                $this->vec_response['expire_date'] = (int)$vec_session_data['expire'];
            }
        }
        
        return true;
    }


    /**
     * Validate input params for GET /api/v1/users/{session_id}/session endpoint
     */
    private function _validate_session()
    {
        if ( ! $this->is_validated )
        {
            $this->is_validated = true;

            // Check if Session exists
            /*
            if ( isset($this->vec_input['id']) )
            {
                $session_id = Yii::app()->db->createCommand('SELECT id FROM user_session WHERE id = :session_id')->bindValue(':session_id', $this->vec_input['id'])->queryColumn();
                if ( empty($session_id) )
                {
                    $this->send_error(102, 'Session not found: #'. $this->vec_input['id']);
                    return false;
                }

                // No input? So validated it
                $this->is_validated = true;
            }
            */
        }

        return $this->is_validated;
    }
}
