<?php
/*
|--------------------------------------------------------------------------
| Sending contact messages resource class for REST API
|--------------------------------------------------------------------------
*/

namespace dz\modules\api\resources;

use dz\db\DbCriteria;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\web\models\WebContact;
use dz\rest\RestResource;
use Yii;

class ContactResource extends RestResource
{
    /**
     * Validate input params
     */
    public function validate($action = 'index')
    {
        if ( ! $this->is_validated )
        {
            if ( $action === 'index' )
            {
                switch ( $this->request_type )
                {
                    // Send contact message via POST method: api/v1/contact
                    case 'POST':
                        $this->_validate_post();
                    break;
                }
            }
        }

        return $this->is_validated;
    }


    /**
     * Send contact message
     */
    public function post_contact()
    {
        if ( ! $this->request_type == 'POST' )
        {
            return false;
        }

        // Prepare common REST API output
        $this->vec_response = [
            'status_code'   => 1,
            'errors'        => [],
            'contact_id'    => null
        ];

        // WebContact model
        $contact_model = Yii::createObject(WebContact::class);
        $contact_model->setAttributes([
            'name'          => $this->vec_input['name'],
            'message'       => $this->vec_input['message'],
            'email'         => isset($this->vec_input['email']) ? $this->vec_input['email'] : null,
            'phone'         => isset($this->vec_input['phone']) ? $this->vec_input['phone'] : null,
            'language_id'   => isset($this->vec_input['language_id']) ? $this->vec_input['language_id'] : Yii::defaultLanguage(),
        ]);

        // Save WebContact model and send the email
        if ( ! $contact_model->save() )
        {
            Log::save_model_error($contact_model);

            $this->send_error(103, $contact_model->getErrors());
        }

        // Send attributes to REST response
        $this->vec_response['contact_id'] = (int)$contact_model->contact_id;

        return true;
    }


    /**
     * Validate input params for POST /api/v1/mailchimp endpoint
     */
    private function _validate_post()
    {
        if ( ! $this->is_validated )
        {
            // Required parameter
            $vec_required_errors = [];
            $vec_required_params = ['name', 'message'];
            foreach ( $vec_required_params as $que_param )
            {
                if ( !isset($this->vec_input[$que_param]) )
                {
                    $vec_required_errors[] = 'Missing required parameter \''. $que_param .'\'';
                }
            }

            if ( !empty($vec_required_errors) )
            {
                $this->send_error(102, $vec_required_errors);
                return false;
            }

            // Email validation
            if ( isset($this->vec_input['email']) && !empty($this->vec_input['email']) )
            {
                $email = StringHelper::trim($this->vec_input['email']);
                if ( empty($email) || ! StringHelper::validate_email($email) )
                {
                    $this->send_error(102, 'Email is incorrect');
                    return false;
                }
            }

            // Set input as validated
            $this->is_validated = true;
        }

        return $this->is_validated;
    }
}
