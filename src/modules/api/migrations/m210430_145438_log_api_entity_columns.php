<?php
/**
 * Migration class m210430_145438_log_api_entity_columns
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210430_145438_log_api_entity_columns extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Add new entity columns
		$this->addColumn('log_api', 'entity_id', $this->string(64)->after('request_hostname'));
        $this->addColumn('log_api', 'entity_type', $this->string(32)->after('entity_id'));
        $this->createIndex(null, 'log_api', ['entity_id', 'entity_type'], false);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

