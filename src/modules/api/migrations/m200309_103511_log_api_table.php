<?php
/**
 * Migration class m200309_103511_log_api_table
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200309_103511_log_api_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "log_api" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('log_api', true);

        $this->createTable('log_api', [
            'log_api_id' => $this->primaryKey(),
            'api_name' => $this->string(32)->notNull()->defaultValue('dz'),
            'request_type' => $this->enum('request_type', ['GET', 'POST', 'PUT', 'DELETE'])->notNull()->defaultValue('GET'),
            'request_url' => $this->string(255)->notNull(),
            'request_endpoint' => $this->string(128)->notNull(),
            'request_input_json' => $this->longtext(),
            'request_hostname' => $this->string(128),
            'response_http_code' => $this->smallInteger(4)->unsigned()->notNull()->defaultValue(200),
            'response_json' => $this->longtext(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
        ]);
    
        // Create indexes
        $this->createIndex(null, 'log_api', ['api_name'], false);
        $this->createIndex(null, 'log_api', ['api_name', 'request_endpoint'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'log_api', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		$this->dropTable('log_api');
		return false;
	}
}

