<?php
/**
 * @package dz\modules\models 
 */

namespace dz\modules\api\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Json;
use dz\helpers\StringHelper;
use dz\modules\api\models\_base\LogApi as BaseLogApi;
use user\models\User;
use Yii;

/**
 * LogApi model class for "log_api" database table
 *
 * Columns in table "log_api" available as properties of the model,
 * followed by relations of table "log_api" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $log_api_id
 * @property string $api_name
 * @property string $request_type
 * @property string $request_url
 * @property string $request_endpoint
 * @property string $request_input_json
 * @property string $request_hostname
 * @property string $entity_id
 * @property string $entity_type
 * @property integer $response_http_code
 * @property string $response_json
 * @property integer $created_date
 * @property integer $created_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 */
class LogApi extends BaseLogApi
{
    /**
     * Search filter by entity
     */
    public $entity_filter;

	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['request_url, request_endpoint, created_date, created_uid', 'required'],
			['response_http_code, created_date, created_uid', 'numerical', 'integerOnly' => true],
			['api_name, entity_type', 'length', 'max'=> 32],
			['request_url', 'length', 'max'=> 255],
			['request_endpoint, request_hostname', 'length', 'max'=> 128],
            ['entity_id', 'length', 'max'=> 64],
			['request_type', 'in', 'range' => ['GET', 'POST', 'PUT', 'DELETE']],
			['api_name, request_type, request_input_json, request_hostname, entity_id, entity_type, response_http_code, response_json', 'default', 'setOnEmpty' => true, 'value' => null],
			['request_input_json, response_json', 'safe'],
			['log_api_id, api_name, request_type, request_url, request_endpoint, request_input_json, request_hostname, entity_id, entity_type, response_http_code, response_json, created_date, created_uid, entity_filter', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'log_api_id' => Yii::t('app', 'Log API'),
			'api_name' => Yii::t('app', 'API Name'),
			'request_type' => Yii::t('app', 'Request Type'),
			'request_url' => Yii::t('app', 'Request Url'),
			'request_endpoint' => Yii::t('app', 'Request Endpoint'),
			'request_input_json' => Yii::t('app', 'Request Input Json'),
			'request_hostname' => Yii::t('app', 'Request Hostname'),
            'entity_id' => Yii::t('app', 'Entity ID'),
            'entity_type' => Yii::t('app', 'Entity Type'),
			'response_http_code' => Yii::t('app', 'Response Http Code'),
			'response_json' => Yii::t('app', 'Response Json'),
			'created_date' => Yii::t('app', 'Register Date'),
			'created_uid' => null,
			'createdUser' => null,
		];
	}


    /**
     * Get "request_type" labels
     */
    public function request_type_labels()
    {
        return [
            'GET' => Yii::t('app', 'GET'),
            'POST' => Yii::t('app', 'POST'),
            'PUT' => Yii::t('app', 'PUT'),
            'DELETE' => Yii::t('app', 'DELETE'),
        ];
    }


    /**
     * Get "request_type" specific label
     */
    public function request_type_label($request_type)
    {
        $vec_labels = $this->request_type_labels();
        return isset($vec_labels[$request_type]) ? $vec_labels[$request_type] : '';
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.api_name', $this->api_name, true);
        $criteria->compare('t.request_endpoint', $this->request_endpoint);

        if ( $this->entity_filter )
        {
            $criteria->addCondition('t.entity_type LIKE :entity_type OR t.entity_id = :entity_id');
            $criteria->addParams([
                ':entity_type'  => '%'. $this->entity_filter .'%',
                ':entity_id'    => $this->entity_filter
            ]);
        }
        else
        {
            $criteria->compare('t.entity_type', $this->entity_type);
            $criteria->compare('t.entity_id', $this->entity_id);
        }

        /*
        $criteria->compare('t.log_api_id', $this->log_api_id);
        $criteria->compare('t.request_type', $this->request_type, true);
        $criteria->compare('t.request_url', $this->request_url, true);
        $criteria->compare('t.request_input_json', $this->request_input_json, true);
        $criteria->compare('t.request_hostname', $this->request_hostname, true);
        $criteria->compare('t.entity_id', $this->entity_id);
        $criteria->compare('t.entity_type', $this->entity_type);
        $criteria->compare('t.response_http_code', $this->response_http_code);
        $criteria->compare('t.response_json', $this->response_json, true);
        $criteria->compare('t.created_date', $this->created_date);
        */

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['log_api_id' => true]]
        ]);
    }


    /**
     * Endpoint list
     * 
     * @return array
     */
     public function endpoint_list()
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['request_endpoint'];
        $criteria->group = 't.request_endpoint';
        
        $vec_models = LogApi::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('request_endpoint')] = $que_model->get_endpoint();
            }
        }

        return $vec_output;
    }


    /**
     * Entity type list
     * 
     * @return array
     */
     public function entity_list()
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['entity_type'];
        $criteria->group = 't.entity_type';
        
        $vec_models = LogApi::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('entity_type')] = $que_model->entity_type;
            }
        }

        return $vec_output;
    }



    /**
     * API name list
     * 
     * @return array
     */
     public function api_name_list()
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['api_name'];
        $criteria->group = 't.api_name';
        
        $vec_models = LogApi::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('api_name')] = $que_model->api_name;
            }
        }

        return $vec_output;
    }


    /**
     * Return formatted endpoint
     */
    public function get_endpoint($endpoint = null)
    {
        if ( $endpoint === null )
        {
            $endpoint = $this->request_endpoint;
        }

        if ( preg_match("/\_\_\_/", $endpoint) )
        {
            $vec_endpoint = explode("___", $endpoint);
            return $vec_endpoint[0] .' '. $vec_endpoint[1];
        }

        return $endpoint;
    }


    /**
     * Return the response into an array
     */
    public function get_input()
    {
        if ( !empty($this->request_input_json) )
        {
            $vec_input = Json::decode($this->request_input_json);
            if ( !is_array($vec_input) )
            {
                $vec_input = Json::decode($vec_input);
            }

            return $vec_input;
        }

        return [];
    }


    /**
     * Return the response into an array
     */
    public function get_response()
    {
        return !empty($this->response_json) ? Json::decode($this->response_json) : [];
    }


    /**
     * Title used for this model
     */
    public function title()
    {
        return $this->get_endpoint();
    }
}