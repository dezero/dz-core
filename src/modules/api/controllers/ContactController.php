<?php
/*
|--------------------------------------------------------------------------
| Controller class for sending contact messages via REST API endpoint
|--------------------------------------------------------------------------
*/

namespace dz\modules\api\controllers;

use dz\modules\api\resources\ContactResource;
use dz\rest\RestController;
use Yii;

class ContactController extends RestController
{
    /**
     * Main action for contact sendings
     *
     *   + POST method: api/v1/contact
     *
     * @return JSON
     */
    public function actionIndex()
    {
        $contact_resource = Yii::createObject(ContactResource::class);

        // #1 - VALIDATE INPUT PARAMS
        if ( $contact_resource->validate('index') )
        {
            // #2  - PROCESS REQUEST
            switch ( $contact_resource->request_type )
            {
                // Send contact message via POST method: api/v1/contact
                case 'POST':
                    $contact_resource->post_contact();
                break;
            }
        }

        // #3 - SEND REST response
        $contact_resource->send_response();
    }
}
