<?php
/*
|--------------------------------------------------------------------------
| Controller class for Users models - REST API endpoint
|--------------------------------------------------------------------------
*/

namespace dz\modules\api\controllers;

use dz\modules\api\resources\UsersResource;
use dz\rest\RestController;
use Yii;

class UsersController extends RestController
{
    /**
     * Main action for User model
     * 
     *   + GET method: api/v1/users
     *
     * @return JSON
     */
    /*
    public function actionIndex()
    {
        $users_resource = Yii::createObject(UsersResource::class);

        // #1 - VALIDATE INPUT PARAMS
        if ( $users_resource->validate('index') )
        {
            // #2  - PROCESS REQUEST
            switch ( $users_resource->request_type )
            {
                // List of accepted users - GET method: api/v1/users
                case 'GET':
                    $users_resource->user_list();
                break;
            }
        }

        // #3 - SEND REST response
        $users_resource->send_response();
    }
    */


    /**
     * Action to get session information
     * 
     *   + GET method: /api/v1/users/{session_id}/session
     *
     * @return JSON
     */
    public function actionSession($id)
    {
        $users_resource = Yii::createObject(UsersResource::class);

        // #1 - VALIDATE INPUT PARAMS
       if ( $users_resource->validate('session', $id) )
        {
            // #2  - PROCESS REQUEST
            switch ( $users_resource->request_type )
            {
                // Check session status - GET method: /api/v1/users/{session_id}/session
                case 'GET':
                    $users_resource->session_status($id);
                break;
            }
        }

        // #3 - SEND REST response
        $users_resource->send_response();
    }
}
