<?php
/**
 * ApiLogCommand class file
 *
 * Clean old log entries from API
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2020 Fabián Ruiz
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package commands
 */

namespace dz\modules\api\commands;

use dz\console\CronCommand;
use dz\helpers\DateHelper;
use Yii;

class ApiLogCommand extends CronCommand
{
    /**
     * Clean old logs from API (5 days)
     *
     * ./yiic apiLog clear
     * ./yiic apiLog clear --days=5
     */
    public function actionClear($days = 5)
    {
        // Get expired date 5 days
        $expired_date = time() - (86400 * $days);

        // Log
        $log_message = "Removing API logs that are before ". DateHelper::unix_to_date($expired_date);
        \DzLog::cron($log_message);
        echo $log_message ."\n";

        // Delete
        Yii::app()->db->createCommand("DELETE FROM log_api WHERE created_date <= ". $expired_date)->execute();
    }
}
