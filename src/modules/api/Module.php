<?php
/**
 * Module for REST API
 */

namespace dz\modules\api;

class Module extends \dz\web\Module
{
    /**
     * Default controller
     */
    public $defaultController = 'users';


    /**
     * @var array mapping from controller ID to controller configurations.
     */
    public $controllerMap = [
        'contact' => [
            'class' => 'dz\modules\api\controllers\ContactController',
        ],
        'users' => [
            'class' => 'dz\modules\api\controllers\UsersController',
        ],
    ];


    /**
     * Load specific CSS or JS files for this module
     */
    public $cssFiles = null; // ['api.css'];
    public $jsFiles = null; // ['api.js'];

    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        parent::init();
    }
}
