<?php
/**
 * Module to manage asset files
 */

namespace dz\modules\asset;

class Module extends \dz\web\Module
{
    /**
     * @var array mapping from controller ID to controller configurations.
     */
    public $controllerMap = [
        'download' => [
            'class' => 'dz\modules\asset\controllers\DownloadController',
        ],
        'filepond' => [
            'class' => 'dz\modules\asset\controllers\FilepondController',
        ],
        'status' => [
            'class' => 'dz\modules\asset\controllers\StatusController',
        ],
    ];

    /**
     * Default controller
     */
    public $defaultController = 'asset';


    /**
     * Load specific CSS or JS files for this module
     */
    public $cssFiles = null; // ['asset.css'];
    public $jsFiles = null; // ['asset.js'];


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        parent::init();
    }
}
