<?php
/**
 * ImageInspectorCommand class file
 *
 * This commmand is used to check data consistency between databases and image files
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link https://www.dezero.es
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package commands
 */

namespace dz\modules\asset\commands;

use dz\console\CronCommand;
use Yii;

class ImageInspectorCommand extends CronCommand
{
	/**
	 * Review Asset models exists
	 * 
	 * PROBLEM: ProductImage model exists but AssetImage model relation NOT
	 * 
	 * ./yiic imageInspector assetModel
	 * ./yiic imageInspector assetModel --autofix=1
	 */
	public function actionAssetModel($autofix = 0)
	{
		$vec_error_products = array();
		echo "Getting products with images...\n";
		$vec_product_results = Yii::app()->db->createCommand("SELECT product_id, COUNT(*) AS num_images FROM product_image GROUP BY product_id HAVING num_images > 0 ORDER BY product_id")->queryAll();
		if ( !empty($vec_product_results) )
		{
			echo "Detected ". count($vec_product_results) ." products with images. Processing...\n";
			foreach ( $vec_product_results as $que_result )
			{
				$product_sku = $this->_get_sku($que_result['product_id']);
				
				$vec_product_image_results = Yii::app()->db->createCommand("SELECT file_id, product_id FROM product_image WHERE product_id = ". $que_result['product_id'])->queryAll();
				echo "\n\n-> Product ". $que_result['product_id'] ." - SKU ". $product_sku .": checking ". count($vec_product_image_results) ." images...";
				foreach ( $vec_product_image_results as $que_product_image_result )
				{
					$asset_image_model = AssetImage::model()->findByPk($que_product_image_result['file_id']);

					// Asset model does not exist 
					if ( ! $asset_image_model )
					{
						$vec_error_products[$product_sku] = $product_sku;
						echo "\n  - ERROR: Image of product ". $product_sku ." - Asset model does not exist with file_id = ". $que_product_image_result['file_id'];
						if ( $autofix == 1 )
						{
							Yii::app()->db->createCommand("DELETE FROM product_image WHERE product_id = ". $que_result['product_id'] ." AND file_id = ". $que_product_image_result['file_id'])->execute();	
						}
					}
				}
			}
		}

		// Show final results
		echo "\n\n--------------------------------------------";
		echo "\n FINAL RESULTS";
		echo "\n - ". count($vec_error_products) . " products without Asset models: ". implode(", ", $vec_error_products);
		echo "\n--------------------------------------------\n";
	}


	/**
	 * Review that all files have an existing value in database
	 * 
	 * PROBLEM: ProductImage & AssetImage models exist but image file NOT
	 * 
	 * ./yiic imageInspector assetFile
	 * ./yiic imageInspector assetFile --autofix=1
	 */
	public function actionAssetFile($autofix = 0)
	{
		$vec_error_products = array();
		echo "Getting products with images...\n";
		$vec_product_results = Yii::app()->db->createCommand("SELECT product_id, COUNT(*) AS num_images FROM product_image GROUP BY product_id HAVING num_images > 0 ORDER BY product_id")->queryAll();
		if ( !empty($vec_product_results) )
		{
			echo "Detected ". count($vec_product_results) ." products with images. Processing...\n";
			foreach ( $vec_product_results as $que_result )
			{
				$product_sku = $this->_get_sku($que_result['product_id']);
				
				$vec_product_image_results = Yii::app()->db->createCommand("SELECT file_id, product_id FROM product_image WHERE product_id = ". $que_result['product_id'])->queryAll();
				echo "\n\n-> Product ". $que_result['product_id'] ." - SKU ". $product_sku .": checking ". count($vec_product_image_results) ." images...";
				foreach ( $vec_product_image_results as $que_product_image_result )
				{
					$asset_image_model = AssetImage::model()->findByPk($que_product_image_result['file_id']);

					// Asset model exist, but not the image file
					if ( $asset_image_model AND ! $asset_image_model->load_file() )
					{
						$vec_error_products[$product_sku] = $product_sku;
						echo "\n  - ERROR: Image file of product ". $product_sku ." does not exist: ". $asset_image_model->file_path . $asset_image_model->file_name ." (file_id = ". $que_product_image_result['file_id'] .')';
						if ( $autofix == 1 )
						{
							$asset_image_model->delete();
						}
					}
				}
			}
		}

		// Show final results
		echo "\n\n--------------------------------------------";
		echo "\n FINAL RESULTS";
		echo "\n - ". count($vec_error_products) . " products with error: ". implode(", ", $vec_error_products);
		echo "\n--------------------------------------------\n";
	}


	/**
	 * Review that the image values existing in database has a filename in OS file system
	 * 
	 * PROBLEM: Image file exist but it has not an AssetImage
	 * 
	 * ./yiic imageInspector orphanFile
	 * ./yiic imageInspector orphanFile --autofix=1
	 */
	public function actionOrphanFile($autofix = 0)
	{
		$vec_output = array(
			'total_images' => 0,
			'total_products' => 0,
			'error_products' => array(),
			'error_images' => array(
				'not_existing' => array(),
				'deleted' => array()
			)
		);

        // Relative path where images are saved
        $images_path = AssetImage::model()->getImagesPath() . DIRECTORY_SEPARATOR;    // Yii::app()->params['images']['destination_path'];

		// Get all files from upload directory
        $images_directory = Yii::app()->file->set(Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . $images_path);
		$vec_directories = $images_directory->getContents();
		if ( !empty($vec_directories) )
		{
			// Get presets prefixes and build pattern to exclude these files
    		$preset_pattern = "^L\_|^M\_|^S\_";
    		$vec_presets_prefixes = AssetImage::model()->get_presets_prefixes();
    		if ( !empty($vec_presets_prefixes) )
    		{
    			$preset_pattern = "";
    			foreach ( $vec_presets_prefixes as $preset_id => $preset_prefix )
    			{
    				if ( empty($preset_pattern) )
    				{
    					$preset_pattern = "^". str_replace("_", "\_", $preset_prefix);
    				}
    				else
    				{
    					$preset_pattern .= "|^". str_replace("_", "\_", $preset_prefix);
    				}
    			}
    		}

            echo "Found ". count($vec_directories) ." products inside ". $images_path .". Comparing file images with Asset models in database...\n";
			foreach ( $vec_directories as $num_directory => $image_product_directory_path )
			{
				$image_product_directory = Yii::app()->file->set($image_product_directory_path);
				if ( $image_product_directory->isDir )
				{
					// Log / output
					$vec_output['total_products']++;

					$product_id = $image_product_directory->getBasename();
					$vec_image_files = $image_product_directory->getContents();
					if ( !empty($vec_image_files) )
					{
						foreach ( $vec_image_files as $image_file_path )
						{
							$image_file = Yii::app()->file->set($image_file_path);
							if ( $image_file->isFile AND ! preg_match("/". $preset_pattern ."/", $image_file->filename) )
							{
								// Log / output
								$vec_output['total_images']++;

								// Does Asset model exist?
								$image_model = AssetImage::model()->findByAttributes(array(
									'file_name' => $image_file->getBasename(),
                                    'file_path' => $images_path . $product_id . DIRECTORY_SEPARATOR
                                    
								));

								if ( ! $image_model )
								{
									$vec_output['error_products'][$product_id] = $product_id;
									$vec_output['error_images']['not_existing'][] = $images_path . $product_id . DIRECTORY_SEPARATOR . $image_file->getBasename();

									// Autofix -> Delete image 
									if ( $autofix == 1 )
									{
										// Delete original filename & presets
										$vec_prefixes = array('', 'L_', 'M_', 'S_');
										$vec_presets_prefixes = AssetImage::model()->get_presets_prefixes();
							    		if ( !empty($vec_presets_prefixes) )
							    		{
							    			$vec_prefixes = array_values($vec_presets_prefixes);
							    			$vec_prefixes[] = '';
							    		}
										
										foreach ( $vec_prefixes as $que_prefix )
										{
											$que_file_path = Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . $images_path . $product_id . DIRECTORY_SEPARATOR;
											$que_file_name = $que_prefix . $image_file->getBasename();
											$old_file = Yii::app()->file->set($que_file_path . $que_file_name);
											if ( $old_file->getExists() )
											{
												$old_file->delete();
											}
										}

										// Delete original file
										$image_file->delete();

										// Log deleted file
										$vec_output['error_images']['deleted'][] = $images_path . $product_id . DIRECTORY_SEPARATOR . $image_file->getBasename();
									}
								}
							}
						}
					}
				}
			}
		}

		// Show final results;
		echo "\n\n--------------------------------------------";
		echo "\n FINAL RESULTS";
		echo "\n - ". $vec_output['total_images'] . " images have been processed from ". $vec_output['total_products'] ." products";
		echo "\n - ". count($vec_output['error_products']) . " products with errors: ". implode("\n   + ", $vec_output['error_products']);
		if ( !empty($vec_output['error_images']['not_existing']) )
		{
			echo "\n - ". count($vec_output['error_images']['not_existing']) . " images with ERROR 'image model does not exist in database': ". "\n   + " . implode("\n   + ", $vec_output['error_images']['not_existing']);
		}
		if ( !empty($vec_output['error_images']['deleted']) )
		{
			echo "\n - ". count($vec_output['error_images']['deleted']) . " delete images (and their presets): ". "\n   + " .  implode("\n   + ", $vec_output['error_images']['deleted']);
		}
		echo "\n--------------------------------------------\n";
	}


	/**
	 * Return just SKU field for a product_id
	 */
	private function _get_sku($product_id)
	{
		return Yii::app()->db->createCommand()
				->select('sku')
				->from('product')
				->where('product_id = :id', array(':id'=> $product_id) )
				->queryScalar();
	}
}