<?php
/**
 * ImageCommand class file
 *
 * This component is used to work with images
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link https://www.dezero.es
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package commands
 */

namespace dz\modules\asset\commands;

use dz\console\CronCommand;
use dz\db\DbCriteria;
use dz\modules\asset\models\AssetImage;
use dzlab\commerce\models\Product;
use dz\utils\FtpClient;
use Yii;

class ImageCommand extends CronCommand
{
	/**
	 * Regenerate the image presets of a product
	 * 
	 * ./yiic image presets --product_id=10290
	 */
	public function actionPresets($product_id)
	{
		// Enable "log" in real time
		$this->set_log_realtime(TRUE);

		// Import needed classes
		Yii::import('@lib.iwi.Iwi');

		// Get all image prefixes
		$vec_prefixes = ['L_', 'M_', 'S_'];
		$vec_presets_prefixes = AssetImage::model()->get_presets_prefixes();
		if ( !empty($vec_presets_prefixes) )
		{
			$vec_prefixes = array_values($vec_presets_prefixes);
		}

		// Get product image
		$product_model = Product::findOne($product_id);
		if ( $product_model )
		{
			echo "Detectado producto ". $product_model->product_id ." (". $product_model->title() .")\n";
			if ( ! $product_model->images )
			{
				echo " - NO existen imágenes";
			}
			else
			{
                // Default file permissions (755)
                $file_permissions = AssetImage::model()->get_default_permissions();

				foreach ( $product_model->images as $product_image_model )
				{
					if ( $product_image_model->image && $product_image_model->image->load_file() )
					{
						echo "Procesando imagen ". $product_image_model->image->file_name ."\n";
						$product_image_model->image->generate_thumbnails(Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . $product_image_model->image->file_path);

						// Change permissions to 755 to all the images
						$current_dir = Yii::app()->file->set(Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . $product_image_model->image->file_path);
						if ( $current_dir->isDir && $current_dir->getExists() )
						{
							$vec_files = $current_dir->getContents();
							if ( !empty($vec_files) )
							{
								foreach ( $vec_files as $que_file_path )
								{
									$que_file = Yii::app()->file->set($que_file_path);
									if ( $que_file && $que_file->getExists() )
									{
										$que_file->setPermissions($file_permissions);
									}
								}
							}
						}
					}
				}
			}
		}
		else
		{
			echo "ERROR - No existe ningún producto con id. ". $product_id ."\n";
		}
	}


	/**
	 * Regenerate all the image presets
	 * 
	 * ./yiic image presetsAll
	 * ./yiic image presetsAll --offset=8000
	 */
	public function actionPresetsAll($offset = 0)
	{
		// Import needed classes
		Yii::import('@lib.iwi.Iwi');

		// Get all image prefixes
		$vec_prefixes = array('L_', 'M_', 'S_');
		$vec_presets_prefixes = AssetImage::model()->get_presets_prefixes();
		if ( !empty($vec_presets_prefixes) )
		{
			$vec_prefixes = array_values($vec_presets_prefixes);
		}

		// Get all product images
		$criteria = new DbCriteria;
		$criteria->compare('entity_type', 'product');
		$criteria->compare('asset_type', 'I');

		// Create separated jobs with 1000 max items per job
	    $num_jobs = 1;
	    $items_limit = 1000;
	    $total_asset_images = AssetImage::model()->count();
	    if ( $total_asset_images > $items_limit ) 
	    {
	    	$num_jobs = ceil($total_asset_images / $items_limit);
	    }
	    echo "Detectadas ". $total_asset_images ." imágenes de producto\n";

	    $total_updated = 0;
	    for ( $current_job = 0; $current_job < $num_jobs; $current_job++ )
	    {
	    	echo "Procesando imágenes de producto desde ". ($offset + ($items_limit * $current_job)) ." hasta ". ($offset + (($current_job + 1) * $items_limit)) ."...\n";

	    	$criteria = new DbCriteria;
	    	$criteria->compare('entity_type', 'product');
			$criteria->compare('asset_type', 'I');
	    	$criteria->limit = $items_limit;
	    	$criteria->offset = $offset + ($items_limit * $current_job);
	    	$criteria->order = 'entity_id ASC';
			$vec_asset_models = AssetImage::model()->findAll($criteria);
			if ( !empty($vec_asset_models) )
			{
				foreach ( $vec_asset_models as $asset_model )
				{
					if ( $asset_model->load_file() )
					{
						echo "Procesando imagen ". $asset_model->file_name ."\n";
						$asset_model->generate_thumbnails(Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . $asset_model->file_path);
					}
				}
			}
		}
	}


	/**
	 * Upload product images via FTP
	 * 
	 * ./yiic image upload --product_id=20404
	 * ./yiic image upload --product_id=20404 --ftp_site=logicommerce_images
	 */
	public function actionUpload($product_id, $ftp_site='')
	{
		$product_model = Product::model()->findByPk($product_id);
		if ( $product_model )
		{
			// Use FtpClient library
			if ( !empty($ftp_site) )
			{
				if ( isset(Yii::app()->params['ftp'][$ftp_site .'_host']) AND isset(Yii::app()->params['ftp'][$ftp_site .'_user']) AND isset(Yii::app()->params['ftp'][$ftp_site .'_pass']) )
				{
					$ftp = new FtpClient();
					$ftp->connect(Yii::app()->params['ftp'][$ftp_site .'_host']);
					$ftp->login(Yii::app()->params['ftp'][$ftp_site .'_user'], Yii::app()->params['ftp'][$ftp_site .'_pass']);
					// $ftp->pasv(TRUE);
					// $ftp->set_option(FTP_USEPASVADDRESS, FALSE);
					$ftp->set_option(FTP_TIMEOUT_SEC, 10);
					// $ftp->set_filter_pattern("^L\_|^M\_|^S\_");

					// $source_directory = Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . Yii::app()->params['images']['destination_path'] . AssetImage::model()->get_product_directory($product_model);
                    $source_directory = Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR .  AssetImage::model()->getImagesPath() . DIRECTORY_SEPARATOR . AssetImage::model()->get_product_directory($product_model);
					$destination_directory = 'productos' . DIRECTORY_SEPARATOR . AssetImage::model()->get_product_directory($product_model);
					
					echo "Subiendo imágenes vía FTP a ". $destination_directory ."...\n";
					$ftp->putAll($source_directory, $destination_directory); 
				}
				else
				{
					echo "ERROR - No se ha definido el host y las credenciales del sitio FTP ". $ftp_site ."\n";
				}
			}

			// Use LFTP script
			else
			{
				$command_path = Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'scripts' . DIRECTORY_SEPARATOR . 'local' . DIRECTORY_SEPARATOR . 'upload_single_product.sh '. AssetImage::model()->get_product_directory($product_model);
				echo $command_path; die;
				exec("/bin/bash ". $command_path ." 2>&1", $output, $exit_code);

				if ((int)$exit_code !== 0)
				{
					echo implode(' ', $output);
					exit(1);
				}
			}
		}
		else
		{
			echo "ERROR - No existe un producto con identificador ". $product_id ."\n";
		}
	}


	/**
	 * Clean (delete) unused presets
	 * 
	 * ./yiic image cleanPresets --product_id=10290
	 */
	public function actionCleanPresets($product_id)
	{
		// Get product image
		$product_model = Product::model()->findByPk($product_id);
		if ( $product_model )
		{
			echo "Detectado producto ". $product_model->product_id ." (". $product_model->title() .")\n";
			if ( ! $product_model->images )
			{
				echo " - NO existen imágenes\n";
			}
			else
			{
				foreach ( $product_model->images as $product_image_model )
				{
					if ( $product_image_model->image AND $product_image_model->image->load_file() )
					{
						echo "Limpiando thumbnails de imagen ". $product_image_model->image->file_name ."\n";
						$vec_deleted_files = $product_image_model->image->clean_thumbnails(Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . $product_image_model->image->file_path);
						if ( !empty($vec_deleted_files) )
						{
							foreach ( $vec_deleted_files as $que_deleted_file )
							{
								echo " - Imagen ". $que_deleted_file ." ha sido eliminada\n";
							}
						}
					}
				}
			}
		}
		else
		{
			echo "ERROR - No existe ningún producto con id. ". $product_id ."\n";
		}
	}


	/**
	 * Clean (delete) all unsued image presets
	 * 
	 * ./yiic image cleanPresetsAll
	 * ./yiic image cleanPresetsAll --offset=8000
	 */
	public function actionCleanPresetsAll($offset = 0)
	{
		// Get all product images
		$criteria = new DbCriteria;
		$criteria->compare('entity_type', 'product');
		$criteria->compare('asset_type', 'I');

		// Create separated jobs with 1000 max items per job
	    $num_jobs = 1;
	    $items_limit = 1000;
	    $total_asset_images = AssetImage::model()->count();
	    if ( $total_asset_images > $items_limit ) 
	    {
	    	$num_jobs = ceil($total_asset_images / $items_limit);
	    }
	    echo "Detectadas ". $total_asset_images ." imágenes de producto\n";

	    $total_updated = 0;
	    for ( $current_job = 0; $current_job < $num_jobs; $current_job++ )
	    {
	    	echo "Procesando imágenes de producto desde ". ($offset + ($items_limit * $current_job)) ." hasta ". ($offset + (($current_job + 1) * $items_limit)) ."...\n";

	    	$criteria = new DbCriteria;
	    	$criteria->compare('entity_type', 'product');
			$criteria->compare('asset_type', 'I');
	    	$criteria->limit = $items_limit;
	    	$criteria->offset = $offset + ($items_limit * $current_job);
	    	$criteria->order = 'entity_id ASC';
			$vec_asset_models = AssetImage::model()->findAll($criteria);
			if ( !empty($vec_asset_models) )
			{
				foreach ( $vec_asset_models as $asset_model )
				{
					if ( $asset_model->load_file() )
					{
						echo "Limpiando thumbnails de imagen ". $asset_model->file_name ."\n";
						$vec_deleted_files = $asset_model->clean_thumbnails(Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . $asset_model->file_path);
						if ( !empty($vec_deleted_files) )
						{
							foreach ( $vec_deleted_files as $que_deleted_file )
							{
								echo " - Imagen ". $que_deleted_file ." ha sido eliminada\n";
							}
						}
					}
				}
			}
		}
	}


	/**
	 * Reorder product images
	 * 
	 * ./yiic image reorder
	 * ./yiic image reorder --product_id=8028
	 * ./yiic image reorder --product_id=gt3763
	 */
	public function actionReorder($product_id = '')
	{
		echo "Getting products with images...\n";
		if ( empty($product_id) )
		{
			$vec_product_results = Yii::app()->db->createCommand("SELECT product_id, COUNT(*) AS num_images FROM product_image GROUP BY product_id HAVING num_images > 0 ORDER BY product_id")->queryAll();
		}
		else
		{
			// Greater than?
			if ( preg_match("/^gt/", $product_id) )
			{
				$product_id = substr($product_id, 2);
				$vec_product_results = Yii::app()->db->createCommand("SELECT product_id, COUNT(*) AS num_images FROM product_image WHERE product_id > ". $product_id ." GROUP BY product_id HAVING num_images > 0 ORDER BY product_id")->queryAll();	
			}
			else
			{
				$vec_product_results = Yii::app()->db->createCommand("SELECT product_id, COUNT(*) AS num_images FROM product_image WHERE product_id = ". $product_id ." GROUP BY product_id HAVING num_images > 0 ORDER BY product_id")->queryAll();	
			}
		}

		if ( !empty($vec_product_results) )
		{
			echo "Detected ". count($vec_product_results) ." products with images. Processing...\n";
			foreach ( $vec_product_results as $que_result )
			{
				$product_model = Product::model()->findByPk($que_result['product_id']);
				if ( $product_model )
				{
					echo "\n - Product ". $product_model->product_id ." - SKU ". $product_model->sku ." - Reordering images...";
					$product_model->reorder_images();
				}
			}
		}

		echo "\n";
	}


	/**
	 * Regenerate ONE preset for ALL images
	 * 
	 * ./yiic image regeneratePreset --prefix=M_
	 * ./yiic image regeneratePreset --prefix=M --offset=8000
	 */
	public function actionRegeneratePreset($prefix, $offset = 0)
	{
		// Enable "log" in real time
		$this->set_log_realtime(TRUE);

		// Get all image prefixes
		$vec_prefixes = array('L_', 'M_', 'S_');
		$vec_presets_prefixes = AssetImage::model()->get_presets_prefixes();
		if ( !empty($vec_presets_prefixes) )
		{
			$vec_prefixes = array_values($vec_presets_prefixes);
		}

		if ( ! in_array($prefix, $vec_prefixes) )
		{
			echo "ERROR - Prefix ". $prefix ." is not defined\n";
		}
		else
		{
			// Import needed classes
			Yii::import('@lib.iwi.Iwi');

			// Get all product images
			$criteria = new DbCriteria;
			$criteria->compare('entity_type', 'product');
			$criteria->compare('asset_type', 'I');

			// Create separated jobs with 1000 max items per job
		    $num_jobs = 1;
		    $items_limit = 1000;
		    $total_asset_images = AssetImage::model()->count();
		    if ( $total_asset_images > $items_limit ) 
		    {
		    	$num_jobs = ceil($total_asset_images / $items_limit);
		    }
		    echo $total_asset_images ." product images detected\n";

		    $total_updated = 0;
		    for ( $current_job = 0; $current_job < $num_jobs; $current_job++ )
		    {
		    	echo "Processing product images from ". ($offset + ($items_limit * $current_job)) ." to ". ($offset + (($current_job + 1) * $items_limit)) ."...\n";

		    	$criteria = new DbCriteria;
		    	$criteria->compare('entity_type', 'product');
				$criteria->compare('asset_type', 'I');
		    	$criteria->limit = $items_limit;
		    	$criteria->offset = $offset + ($items_limit * $current_job);
		    	$criteria->order = 'entity_id ASC';
				$vec_asset_models = AssetImage::model()->findAll($criteria);
				if ( !empty($vec_asset_models) )
				{
					foreach ( $vec_asset_models as $asset_model )
					{
						if ( $asset_model->load_file() )
						{
							echo "Image ". $asset_model->file_name ." - Generating thumbnail with prefix $prefix\n";
							$asset_model->generate_thumbnails(Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . $asset_model->file_path, array($prefix));
						}
					}
				}
			}
		}
	}
}