<?php
/**
 * FileManager component
 * 
 * Component to manage AssetFile models
 */

namespace dz\modules\asset\components;

use dz\base\ApplicationComponent;
use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\helpers\Transliteration;
use dz\modules\asset\models\AssetFile;
use dz\modules\asset\models\AssetFileStatus;
use dz\modules\asset\services\CompressAssetFileService;
use user\models\User;
use Yii;

class FileManager extends ApplicationComponent
{
    /**
     * Get "status_type" labels
     */
    public function status_labels()
    {
        return AssetFileStatus::model()->status_type_labels();
    }


    /**
     * Get "status_type" colors
     */
    public function status_colors()
    {
        return [
            'pending'       => 'blue-800',
            'accepted'      => 'green-800',
            'rejected'      => 'red-800',
            'incomplete'    => 'purple-800',
        ];
    }


    /**
     * Create a new AssetFile model from a File object
     */
    public function create_model_from_file($file, $vec_attributes = [])
    {
        $vec_attributes['file_name'] = Transliteration::file($file->basename);
        $vec_attributes['file_mime'] = $file->mimeType;
        $vec_attributes['file_size'] = $file->getSize(false);

        $asset_file_model = Yii::createObject(AssetFile::class);
        $asset_file_model->setAttributes($vec_attributes);
        $asset_file_model->scenario = 'upload_document';
        $asset_file_model->file = $file;

        if ( ! $asset_file_model->save() )
        {
            Log::save_model_error($asset_file_model);
            return null;
        }

        return $asset_file_model;
    }


    /**
     * Create a directory if it does not exist
     */
    public function create_directory($directory_path)
    {
        $file_dir = Yii::app()->file->set($directory_path);
        if ( ! $file_dir->isDir )
        {
            $default_permissions = AssetFile::model()->get_default_permissions();
            $file_dir->createDir($default_permissions, $directory_path);
        }

        return $directory_path;
    }


    /**
     * Compress a file with ZIP loaded from a AssetFile model
     *
     * @return AssetFile model
     */
    public function compress_asset_file($asset_file_model)
    {
        $compress_asset_file_service = Yii::createObject(CompressAssetFileService::class, [$asset_file_model]);
        if ( ! $compress_asset_file_service->run() )
        {
            $vec_errors = $compress_asset_file_service->get_errors();
            if ( !empty($vec_errors) )
            {
                foreach ( $vec_errors as $que_error )
                {
                    Log::error($que_error);
                }
            }

            return false;
        }

        return $asset_file_model;
    }
}
