<?php
/*
|--------------------------------------------------------------------------
| Controller class for AssetFile models using Filepond widget
|--------------------------------------------------------------------------
*/

namespace dz\modules\asset\controllers;

use dz\helpers\Log;
use dz\helpers\Transliteration;
use dz\modules\asset\models\AssetFile;
use dz\web\Controller;
use Yii;

class FilepondController extends Controller
{
    /**
     * TEMP destination path where files will be uploaded via Filepond
     */
    public $temp_destination_path;


    /**
     * Main action
     */
    public function actionIndex()
    {
        $this->_log('index');

        if ( $request_method == 'DELETE' )
        {
            echo '';
        }
        else
        {
            echo mt_rand(0,9999999999);
        }
    }


    /**
     * Upload action (Filepond method "process")
     */
    public function actionUpload()
    {
        $this->_log('upload');

        if ( isset($_POST['Filepond']) && isset($_FILES['Filepond']['name']) )
        {
            $filepond_post = $_POST['Filepond'];
            reset($filepond_post);
            $filepond_key = key($filepond_post);

            if ( isset($_FILES['Filepond']['name'][$filepond_key]) )
            {
                $que_file = Yii::app()->file->set('Filepond['. $filepond_key .'][0]');
                if ( empty($que_file) || ! $que_file->isFile )
                {
                    $que_file = Yii::app()->file->set('Filepond['. $filepond_key .']');
                }

                if ( !empty($que_file) && $que_file->isFile )
                {
                    $file_model = new AssetFile;
                    $file_model->setAttributes([
                        'file_name'     => Transliteration::file($que_file->basename),
                        'file_mime'     => $que_file->mimeType,
                        'file_size'     => $que_file->getSize(false),
                        'file_path'     => $this->get_temp_destination_path(),
                        'file_options'  => 'filepond',
                    ]);
                    $file_model->file = $que_file;

                    // Change save scenario
                    $file_model->scenario = 'upload_document';

                    if ( ! $file_model->save() )
                    {
                        Log::save_model_error($file_model);
                        echo '';
                    }
                    else
                    {
                        // Filepond needs the file_id to be returned
                        echo $file_model->file_id;
                    }

                    // Stop execution
                    Yii::app()->end();
                }
            }
        }

        // Filepond needs a file_id to be returned. We use 0 for empty input values
        echo 0;
    }


    /**
     * Delete action (Filepond method "revert")
     */
    public function actionDelete()
    {
        $this->_log('delete');

        // Get file_id
        $file_id = Yii::app()->request->getRawBody();

        // Delete the model and the file
        $file_model = AssetFile::findOne($file_id);
        if ( $file_model )
        {
            $file_model->delete();
        }

        // Filepond needs an empty response to be returned
        echo '';
    }


    /**
     * Load action (Filepond method "load")
     */
    public function actionLoad($id)
    {
        $this->_log('load');

        $file_model = AssetFile::findOne($id);
        if ( $file_model && $file_model->load_file() )
        {
            header('Access-Control-Expose-Headers: Content-Disposition, Content-Length, X-Content-Transfer-Id');
            header('Content-Type: '. $file_model->file_mime);
            header('Content-Length: '. $file_model->file_size);
            header('Content-Disposition: inline; filename="'. $file_model->file_name .'"');
            header('X-Content-Transfer-Id: '. $file_model->file_id);

            /*
            Log::dev([
                'Content-Type: '. $file_model->file_mime,
                'Content-Length: '. $file_model->file_size,
                'Content-Disposition: inline; filename="'. $file_model->file_name .'"',
                'X-Content-Transfer-Id: '. $file_model->file_id
            ]);
            */

            // Return file content
            echo $file_model->file->getContents();
        }

        Yii::app()->end();
    }


    /**
     * Return TEMP destination path where files will be uploaded via Filepond
     */
    public function get_temp_destination_path()
    {
        if ( empty($this->temp_destination_path) )
        {
            // $this->temp_destination_path = AssetFile::model()->getTempPath();
            $this->temp_destination_path = Yii::app()->path->get('privateTempPath') . DIRECTORY_SEPARATOR;
        }

        return $this->temp_destination_path;
    }


    /**
     * Add a TEMP destination path
     */
    public function set_temp_destination_path($temp_destination_path)
    {
        $this->temp_destination_path = $temp_destination_path;
    }


    /**
     * Log input requests
     */
    private function _log($action)
    {
        $request_method = Yii::app()->request->getRequestType();

        Log::dev('----- ACTION: '. $action .' -----');
        Log::dev('- REQUEST METHOD ---> '. Yii::app()->request->getRequestType());
        Log::dev('- GET: '. print_r($_GET, true));
        Log::dev('- POST: '. print_r($_POST, true));
        Log::dev('- FILES: '. print_r($_FILES, true));
        Log::dev('- php://input="'. Yii::app()->request->getRawBody() .'"');
    }
}
