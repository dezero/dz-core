<?php
/*
|--------------------------------------------------------------------------
| Controller class to manage AssetFileStatus models
|--------------------------------------------------------------------------
*/

namespace dz\modules\asset\controllers;

use dz\modules\asset\models\AssetFile;
use dz\modules\asset\models\AssetFileStatus;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\Url;
use dz\helpers\StringHelper;
use dz\web\Controller;
use Yii;

class StatusController extends Controller
{
    /**
     * Action to reload AssetFileStatus models
     */
    public function actionTable($validation_type, $table_id)
    {
        // Action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        // Get AssetFile models
        $vec_file_models = [];
        $vec_file_status_models = AssetFileStatus::get()->where(['validation_type' => $validation_type])->all();
        if ( !empty($vec_file_status_models) )
        {
            foreach ( $vec_file_status_models as $file_status_model )
            {
                if ( $file_status_model->file )
                {
                    $vec_file_models[] = $file_status_model->file;
                }
            }
        }

        // Render partial view
        $vec_data = [
            'validation_type'   => $validation_type,
            'table_id'          => $table_id,
            'table_reload_url'  => Url::to('/asset/status/table', ['validation_type' => $validation_type, 'table_id' => $table_id]),
            'vec_file_models'   => $vec_file_models
        ];
        echo $this->renderPartial('//asset/status/_table', $vec_data, true, true);
        Yii::app()->end();
    }


    /**
     * Update Status action for the SlidePanel widget
     */
    public function actionUpdate($validation_type, $file_id)
    {
        // Action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        // First of all, check if AssetFile model exist or we need to create it
        $asset_file_model = $this->loadModel($file_id, AssetFile::class);

        // Now, get or create AssetFileStatus model
        $asset_file_status_model = $asset_file_model->get_status_model($validation_type, true);

        // Submitted form?
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $vec_ajax_output = [
                'error_msg'         => '',
                'error_code'        => 0
            ];

            // Get all status types
            $vec_status_types = Yii::app()->fileManager->status_labels();

            // Get input data
            $vec_input = $this->jsonInput();

            // #1 - JSON input params are correct?
            if ( !empty($vec_input) && isset($vec_input['file_id']) && isset($vec_input['new_status']) )
            {
                // #2 - AssetFile matches?
                if ( $vec_input['file_id'] != $file_id )
                {
                    $vec_ajax_output['error_code'] = 101;
                    $vec_ajax_output['error_msg'] = 'Access denied - File '. $vec_input['file_id'] .' is invalid';
                }

                // #3 - Status type exists?
                else if ( !isset($vec_status_types[$vec_input['new_status']]) )
                {
                    $vec_ajax_output['error_code'] = 102;
                    $vec_ajax_output['error_msg'] = 'Access denied - Status given '. $vec_input['new_status'] .' is invalid';
                }

                else
                {
                    // #4 - Save new status
                    $comments = isset($vec_input['new_comments']) ? $vec_input['new_comments'] : null;
                    if ( ! $asset_file_status_model->change_status($vec_input['new_status'], $comments) )
                    {
                        $vec_ajax_output['error_code'] = 103;
                        $vec_ajax_output['error_msg'] = 'Error - File status could not be changed.';
                    }
                }
            }
            else
            {
                $vec_ajax_output['error_code'] = 201;
                $vec_ajax_output['error_msg'] = 'Access denied - JSON input params are incorrect';
            }

            // Return JSON and end application
            $this->jsonOutput(200, Json::encode($vec_ajax_output));
        }

        // If we arrive here (not POST params), render partial view
        $this->renderPartial('//asset/status/_slidepanel_status', [
            'asset_file_model'           => $asset_file_model,
            'asset_file_status_model'    => $asset_file_status_model,
        ]);
    }


    /**
     * Upload File action for the SlidePanel widget
     */
    public function actionUpload($validation_type, $file_field = null)
    {
        // Action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        $vec_ajax_output = [
            'error_msg'         => '',
            'error_code'        => 0
        ];

        // New uploaded files
        if ( isset($_FILES['AssetFiles']) )
        {
            if ( isset($_FILES['AssetFiles']['name'][0]) )
            {
                $vec_ajax_output['files'] = $_FILES['AssetFiles'];
                foreach ( $_FILES['AssetFiles']['name'] as $num_file => $file_name )
                {
                    $que_file = Yii::app()->file->set('AssetFiles['. $num_file .']');
                    if ( $que_file->isFile )
                    {
                        $asset_file_model = Yii::app()->fileManager->create_model_from_file($que_file, [
                            // 'file_path'     => $account_model->get_document_path(true),
                            // 'file_options'  => 'krajee',
                            // 'entity_id'     => $identity_document_model->identity_document_id,
                            // 'entity_type'   => 'IdentityDocument',
                            // 'title'         => $identity_document_model->getAttributeLabel($file_field),
                            // 'description'   => 'Identity document for User #'. $account_model->user_id .' ('. $account_model->title().')',
                        ]);
                        
                        if ( $asset_file_model )
                        {
                            // Remove OLD AssetFile
                            if ( isset($_POST['file_id']) )
                            {
                                $old_file_model = AssetFile::findOne($_POST['file_id']);
                                if ( $old_file_model )
                                {
                                    $old_file_model->delete();
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                $vec_ajax_output['error_code'] = 102;
                $vec_ajax_output['error_msg'] = 'Uploaded file is not found';
            }

            // Return JSON and end application
            $this->jsonOutput(200, Json::encode($vec_ajax_output));
        }

        // If we arrive here (not $_FILES params), render partial view
        $this->renderPartial('//asset/status/_slidepanel_dropzone', [
            'validation_type'   => $validation_type,
            'upload_url'        => Url::to('/asset/status/upload', ['validation_type' => $validation_type, 'file_field' => $file_field]),
        ]);
    }


    /**
     * Delete action for AssetFile model
     */
    public function actionDelete($validation_type, $file_id)
    {
        // Delete action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsPostRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // First of all, check if AssetFile model and AssetFileStatus model exist
        $asset_file_model = $this->loadModel($file_id, AssetFile::class);
        $asset_file_status_model = $asset_file_model->get_status_model($validation_type);
        if ( ! $asset_file_status_model )
        {
            throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }
        
        $vec_ajax_output = [
            'error_msg'     => '',
            'error_code'    => 0,
        ];

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $vec_input = $this->jsonInput();

            // #1 - JSON input params are correct?
            if ( !empty($vec_input) && isset($vec_input['file_id']) && isset($vec_input['validation_type']) )
            {
                // #2 - Files matches?
                if ( $vec_input['file_id'] != $file_id )
                {
                    $vec_ajax_output['error_code'] = 101;
                    $vec_ajax_output['error_msg'] = 'Access denied - File #'. $vec_input['file_id'] .' is invalid';
                }

                // #3 - Validations matches?
                else if ( $vec_input['validation_type'] != $validation_type )
                {
                    $vec_ajax_output['error_code'] = 102;
                    $vec_ajax_output['error_msg'] = 'Access denied - Validation type #'. $vec_input['validation_type'] .' is invalid';
                }

                // #4 - Delete the model
                else
                {   
                    if ( ! $asset_file_model->delete() )
                    {
                        $vec_ajax_output['error_code'] = 103;
                        $vec_ajax_output['error_msg'] = 'Error - File #'. $vec_input['file_id'] .' could not be deleted';
                    }
                }
            }
            else
            {
                $vec_ajax_output['error_code'] = 201;
                $vec_ajax_output['error_msg'] = 'Access denied - JSON input params are incorrect';
            }
        }
        else
        {
            $vec_ajax_output['error_code'] = 202;
            $vec_ajax_output['error_msg'] = 'Access denied - Your request is invalid. Request is not POST type';
        }

        // Return JSON and end application
        $this->jsonOutput(200, Json::encode($vec_ajax_output));
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'table'     => 'admin',
            'update'    => 'admin',
            'delete'    => 'admin',
        ];
    }
}