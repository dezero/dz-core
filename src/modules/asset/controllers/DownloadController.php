<?php
/*
|--------------------------------------------------------------------------
| Controller class used to download AssetFile models
|--------------------------------------------------------------------------
*/

namespace dz\modules\asset\controllers;

use dz\modules\asset\models\AssetFile;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\web\Controller;
use Yii;

class DownloadController extends Controller
{
    /**
     * Private download action
     */
    public function actionIndex($id)
    {
         // Do not allow anonymous access
        if ( Yii::app()->user->id == 0  )
        {
            throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }

        // Load current user
        $user_model = Yii::app()->user->model();

        // Load file
        $file_model = AssetFile::model()->findByUuid($id);
        if ( ! $file_model )
        {
            Log::download_error('ERROR 404 - File does not exists: User '. $user_model->id . ' - '. $user_model->username .' ('. $user_model->email .') - Error downloading file (uuid = '. $id .') from IP '. Yii::app()->request->getUserIP());
            throw new \CHttpException(404, Yii::t('app', 'The requested file does not exist.'));
        }

        // Only allow access if file belongs to the user or if it's an admin or user has "download_full_access" permissions
        if ( $file_model->created_uid !== Yii::app()->user->id && ! Yii::app()->user->isAdmin() && ! Yii::app()->user->checkAccess('download_full_access') )
        {
            Log::download_error('ERROR 403 - File does not belong to user: User '. $user_model->id . ' - '. $user_model->username .' ('. $user_model->email .') - Error downloading file (file_id = '. $file_model->file_id .' - file_path = '. $file_model->file_path . $file_model->file_name .') from IP '. Yii::app()->request->getUserIP());
            throw new \CHttpException(403, Yii::t('app', 'Access denied.'));
        }

        // Check if physical file exists
        if ( ! $file_model->load_file() || ! $file_model->file->getExists() )
        {
            Log::download_error('ERROR 404 - File does not exist: User '. $user_model->id . ' - '. $user_model->username .' ('. $user_model->email .') - Error downloading file (file_id = '. $file_model->file_id .' - file_path = '. $file_model->file_path . $file_model->file_name .') from IP '. Yii::app()->request->getUserIP());
            throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }

        // ---------------------------------------------------------------------
        // Download file from PUBLIC path
        // ---------------------------------------------------------------------
        $files_path = Yii::app()->path->get('filesPath');
        if ( preg_match("/^". str_replace("/", "\/", $files_path) . "/", $file_model->file_path) )
        {
            // Register download success
            // Log::download_info('User '. $user_model->id . ' - '. $user_model->username .' ('. $user_model->email .') - Trying to download file "'. $file_model->file_name .'" (file_id = '. $file_model->file_id .' - file_path = '. $file_model->file_path . $file_model->file_name .') from IP '. Yii::app()->request->getUserIP());

            if ( ! $file_model->file->download($file_model->file_name) )
            {
                Log::download_error('ERROR 403 PUBLIC - File "'. $file_model->file_name .'" cannot be downloaded: User '. $user_model->id . ' - '. $user_model->username .' ('. $user_model->email .') - Error downloading file (file_id = '. $file_model->file_id .' - file_path = '. $file_model->file_path . $file_model->file_name .') from IP '. Yii::app()->request->getUserIP());
                throw new \CHttpException(403, Yii::t('app', 'File "'. $file_model->file_name .'" cannot be downloaded.'));
            }
        }

        // ---------------------------------------------------------------------
        // Download file from PRIVATE path
        // ---------------------------------------------------------------------
        else
        {
            // Copy file to temp directory
            $temp_path = Yii::app()->path->get('temp') . DIRECTORY_SEPARATOR . 'download' . DIRECTORY_SEPARATOR;
            $temp_dir = Yii::app()->file->set($temp_path);
            if ( ! $temp_dir->isDir )
            {
                $temp_dir->createDir(Yii::app()->file->default_permissions, $temp_path);
            }
            $destination_file_path = $temp_path . StringHelper::random_timestamp('suffix', 5) .'_'. $file_model->file_name;
            $destination_file = $file_model->file->copy($destination_file_path);

            // Download file
            if ( $destination_file && $destination_file->getExists() )
            {
                // Register download success
                Log::download_info('User '. $user_model->id . ' - '. $user_model->username .' ('. $user_model->email .') - Trying to download file "'. $file_model->file_name .'" (file_id = '. $file_model->file_id .') from IP '. Yii::app()->request->getUserIP());

                if ( ! $destination_file->download($file_model->file_name) )
                {
                    Log::download_error('ERROR 403A PRIVATE - File "'. $file_model->file_name .'" cannot be downloaded: User '. $user_model->id . ' - '. $user_model->username .' ('. $user_model->email .') - Error downloading file (file_id = '. $file_model->file_id .' - file_path = '. $file_model->file_path . $file_model->file_name .') from IP '. Yii::app()->request->getUserIP());
                    throw new \CHttpException(403, Yii::t('app', 'File "'. $file_model->file_name .'" cannot be downloaded.'));
                }
            }
            else
            {
                Log::download_error('ERROR 403B PRIVATE - File cannot be downloaded: User '. $user_model->id . ' - '. $user_model->username .' ('. $user_model->email .') - Error downloading file "'. $file_model->file_name .'" (file_id = '. $file_model->file_id .' - file_path = '. $file_model->file_path . $file_model->file_name .') from IP '. Yii::app()->request->getUserIP());
                throw new \CHttpException(403, Yii::t('app', 'File cannot be downloaded.'));
            }
        }

        // If we arrive here, throws a 404 exception
        throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            // Access to any logged_in user
            'index'    => 'is_logged_in',
        ];
    }
}
