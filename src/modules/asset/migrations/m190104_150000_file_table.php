<?php
/**
 * Migration class m190104_150000_file_table
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;

class m190104_150000_file_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		/// Create "asset_file" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('asset_file', true);

        $this->createTable('asset_file', [
            'file_id' => $this->primaryKey(),
            'file_name' => $this->string(128)->notNull(),
            'file_path' => $this->string(255)->notNull(),
            'file_mime' => $this->string(128)->notNull(),
            'file_size' => $this->integer()->notNull()->defaultValue(0),
            'file_options' => $this->string(255),
            'entity_id' => $this->string(64),
            'entity_type' => $this->string(32),
            'asset_type' => $this->enum('asset_type', ['image', 'document', 'video', 'other']),
            'title' => $this->string(255),
            'description' => $this->text(),
            'original_file_name' => $this->string(128),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create normal INDEX
        $this->createIndex(null, 'asset_file', ['entity_id', 'entity_type'], false);
        $this->createIndex(null, 'asset_file', ['asset_type'], false);

        // Foreign keys
        $this->addForeignKey(null, 'asset_file', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'asset_file', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('asset_file');
		return false;
	}
}

