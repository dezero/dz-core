<?php
/**
 * Migration class m210514_140549_file_status_table
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210514_140549_file_status_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "asset_file_status" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('asset_file_status', true);

        $this->createTable('asset_file_status', [
            'file_id' => $this->integer()->unsigned()->notNull(),
            'validation_type' => $this->string(32)->notNull()->defaultValue('default'),
            'status_type' => $this->enum('status_type', ['pending', 'accepted', 'rejected', 'incomplete']),
            'user_id' => $this->integer()->unsigned(),
            'accepted_date' => $this->date(),
            'accepted_uid' => $this->integer()->unsigned(),
            'rejected_date' => $this->date(),
            'rejected_uid' => $this->integer()->unsigned(),
            'comments' => $this->string(512),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);
    
        // Primary key (alternative method)
        $this->addPrimaryKey(null, 'asset_file_status', ['file_id', 'validation_type']);

        // Create indexes
        $this->createIndex(null, 'asset_file_status', ['validation_type'], false);
        $this->createIndex(null, 'asset_file_status', ['status_type'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'asset_file_status', ['file_id'], 'asset_file', ['file_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'asset_file_status', ['user_id'], 'user_users', ['id'], 'SET NULL', null);
        $this->addForeignKey(null, 'asset_file_status', ['accepted_uid'], 'user_users', ['id'], 'SET NULL', null);
        $this->addForeignKey(null, 'asset_file_status', ['rejected_uid'], 'user_users', ['id'], 'SET NULL', null);
        $this->addForeignKey(null, 'asset_file_status', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'asset_file_status', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('asset_file_status');
		return false;
	}
}

