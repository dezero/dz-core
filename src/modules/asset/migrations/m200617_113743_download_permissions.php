<?php
/**
 * Migration class m200617_113743_download_permissions
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200617_113743_download_permissions extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Permissions
        $this->insertMultiple('user_auth_item', [
            [
                'name'          => 'asset.download.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Asset - Download file - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'download_full_access',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Download asset files - Full access to download any file',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
        ]);

        // Set items relationships
        $this->insertMultiple('user_auth_item_child', [
            [
                'parent'    => 'download_full_access',
                'child'     => 'asset.download.*'
            ],
            [
                'parent'    => 'admin',
                'child'     => 'download_full_access'
            ],
        ]);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

