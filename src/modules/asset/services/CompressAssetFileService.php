<?php
/*
|--------------------------------------------------------------------------
| Service for compressing a file with ZIP loaded from a AssetFile model
|--------------------------------------------------------------------------
*/

namespace dz\modules\asset\services;

use dz\contracts\ServiceInterface;
use dz\helpers\Log;
use dz\modules\asset\models\AssetFile;
use dz\traits\ErrorTrait;
use Yii;

class CompressAssetFileService implements ServiceInterface
{
    use ErrorTrait;


    /**
     * Constructor
     */
    public function __construct(AssetFile $asset_file_model)
    {
        $this->asset_file_model = $asset_file_model;
    }


    /**
     * @return bool
     */
    public function run()
    {
        // Load File instance
        if ( ! $this->load_file() )
        {
            return false;
        }

        // Compress file with ZIP encoding
        if ( ! $this->compress_file() )
        {
            return false;
        }

        // Update AssetFile model
        if ( ! $this->update_asset_file_model() )
        {
            return false;
        }

        return true;
    }


    /**
     * Load File instance
     */
    public function load_file()
    {
        if ( ! $this->asset_file_model->load_file() )
        {
            $this->add_error('File cannot be loaded');

            return false;
        }

        return true;
    }


    /**
     * Compress file with ZIP encoding
     */
    public function compress_file()
    {
        if ( ! $this->asset_file_model->file->zip(true) )
        {
            $this->add_error('File cannot be compressed with zip');

            return false;
        }

        // Update new file name
        $this->asset_file_model->file_name .= '.zip';

        return true;
    }


    /**
     * Update AssetFile model
     */
    private function update_asset_file_model()
    {
        $file_destination_path = $this->asset_file_model->file_path . $this->asset_file_model->file_name;
        if ( ! $this->asset_file_model->save_asset_file_from_path($file_destination_path) )
        {
            $this->add_error($this->asset_file_model->getErrors());

            return false;
        }

        return true;
    }
}
