<?php
/**
 * @package dz\modules\asset\models 
 */

namespace dz\modules\asset\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\helpers\Transliteration;
use dz\helpers\Url;
use dz\modules\asset\models\_base\AssetFile as BaseAssetFile;
use dz\modules\asset\models\AssetFileStatus;
use product\models\Product;
use product\models\ProductImage;
use product\models\ProductDocument;
use user\models\User;
use Yii;

/**
 * AssetFile model class for "asset_file" database table
 *
 * Columns in table "asset_file" available as properties of the model,
 * followed by relations of table "asset_file" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $file_id
 * @property string $file_name
 * @property string $file_path
 * @property string $file_mime
 * @property integer $file_size
 * @property string $file_options
 * @property integer $entity_id
 * @property string $entity_type
 * @property string $asset_type
 * @property string $title
 * @property string $description
 * @property string $original_file_name
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property User $createdUser
 * @property User $updatedUser
 *
 * -------------------------------------------------------------------------
 * LINUX FTP COMMANDS
 * -------------------------------------------------------------------------
 * Upload all subdirectories over FTP to productos folder
 *  >  ncftpput -R -v -u "981" ftpclient.logicommerce.net /productos .
 *
 * 
 */
class AssetFile extends BaseAssetFile
{
    const ASSET_TYPE_IMAGE = 'image';
    const ASSET_TYPE_DOCUMENT = 'document';
    const ASSET_TYPE_VIDEO = 'video';
    const ASSET_TYPE_OTHER = 'other';

    /**
     * @var File
     */
    public $file;


    /**
     * @var string  Webroot relative path
     */
    private $_webRootPath;


    /**
     * @var string  Relative path where files are stored
     */
    private $_filesPath;


    /**
     * @var string  Relative path where iamges files are stored
     */
    private $_imagesPath;


    /**
     * @var string  Relative path where temporary files are stored
     */
    private $_tempPath;


    /**
     * Constructor
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    

    /**
     * Label with translation support (from GIIX)
     */
    public static function label($n = 1)
    {
        return Yii::t('app', 'File|Files', $n);
    }

    
    /**
     * Returns the validation rules for attributes
     */
    public function rules()
    {
        return [
            ['file_name, file_path, file_mime, created_date, created_uid, updated_date, updated_uid', 'required'],
            ['file_size, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
            ['file_name, file_mime, original_file_name', 'length', 'max'=> 128],
            ['file_path, file_options, title', 'length', 'max'=> 255],
            ['entity_id', 'length', 'max'=> 64],
            ['entity_type', 'length', 'max'=> 32],
            ['uuid', 'length', 'max'=> 36],
            ['asset_type', 'in', 'range' => [self::ASSET_TYPE_IMAGE, self::ASSET_TYPE_DOCUMENT, self::ASSET_TYPE_VIDEO, self::ASSET_TYPE_OTHER]],
            ['file_size, file_options, entity_id, entity_type, asset_type, title, description, original_file_name, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
            ['description', 'safe'],
            ['file_id, file_name, file_path, file_mime, file_size, file_options, entity_id, entity_type, asset_type, title, description, original_file_name, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on'=>'search'],
        ];
    }
    

    /**
     * Define relations with other objects
     *
     * There are four types of relations that may exist between two active record objects:
     *   - BELONGS_TO: e.g. a member belongs to a team;
     *   - HAS_ONE: e.g. a member has at most one profile;
     *   - HAS_MANY: e.g. a team has many members;
     *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
     */
    public function relations()
    {
        return [
            'createdUser' => [self::BELONGS_TO, User::class,['created_uid' => 'id']],
            'updatedUser' => [self::BELONGS_TO, User::class,['updated_uid' => 'id']],

            // Custom relations
            'status' => [self::BELONGS_TO, AssetFileStatus::class, ['file_id' => 'file_id']],
            'statuses' => [self::HAS_MANY, AssetFileStatus::class, ['file_id' => 'file_id']],
        ];
    }
    
    /**
     * Returns the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'file_id' => Yii::t('app', 'Fichero'),
            'file_name' => Yii::t('app', 'Nombre fichero'),
            'file_path' => Yii::t('app', 'Ruta fichero'),
            'file_mime' => Yii::t('app', 'Tipo fichero'),
            'file_size' => Yii::t('app', 'Tamaño'),
            'file_options' => Yii::t('app', 'Opciones fichero'),
            'entity_id' => Yii::t('app', 'Entity'),
            'entity_type' => Yii::t('app', 'Entity Type'),
            'asset_type' => Yii::t('app', 'Asset Type'),
            'title' => Yii::t('app', 'Título'),
            'description' => Yii::t('app', 'Descripción'),
            'original_file_name' => Yii::t('app', 'Nombre Fichero Original'),
            'created_date' => Yii::t('app', 'Created Date'),
            'created_uid' => null,
            'updated_date' => Yii::t('app', 'Updated Date'),
            'updated_uid' => null,
            'uuid' => Yii::t('app', 'Uuid'),
            'createdUser' => null,
            'updatedUser' => null,
            'pimProducts' => null,
        ];
    }
    
    
    /**
     * Get "asset_type" labels
     */
    public function asset_type_labels()
    {
        return [
            self::ASSET_TYPE_IMAGE => Yii::t('app', 'image'),
            self::ASSET_TYPE_DOCUMENT => Yii::t('app', 'document'),
            self::ASSET_TYPE_VIDEO => Yii::t('app', 'video'),
            self::ASSET_TYPE_OTHER => Yii::t('app', 'other'),
        ];
    }


    /**
     * Get "asset_type" specific label
     */
    public function asset_type_label($asset_type)
    {
        $vec_labels = $this->asset_type_labels();
        return isset($vec_labels[$asset_type]) ? $vec_labels[$asset_type] : '';
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.file_id', $this->file_id);
        $criteria->compare('t.file_name', $this->file_name, true);
        $criteria->compare('t.file_path', $this->file_path, true);
        $criteria->compare('t.file_mime', $this->file_mime, true);
        $criteria->compare('t.file_size', $this->file_size);
        $criteria->compare('t.file_options', $this->file_options, true);
        $criteria->compare('t.entity_id', $this->entity_id);
        $criteria->compare('t.entity_type', $this->entity_type, true);
        $criteria->compare('t.asset_type', $this->asset_type, true);
        $criteria->compare('t.title', $this->title, true);
        $criteria->compare('t.description', $this->description, true);
        $criteria->compare('t.original_file_name', $this->original_file_name, true);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['file_id' => true]]
        ]);
    }


    /*
    |--------------------------------------------------------------------------
    | CUSTOM FUNCTIONS
    |--------------------------------------------------------------------------
    */


    /**
     * Return webroot relative path
     */
    public function getWebRootPath()
    {
        if ( ! $this->_webRootPath )
        {
            $this->_webRootPath = Yii::app()->path->webRootPath();
        }
        
        return $this->_webRootPath;
    }


    /**
     * Return relative path where files are stored
     */
    public function getFilesPath()
    {
        if ( ! $this->_filesPath )
        {
            $this->_filesPath = Yii::app()->path->filesPath();
        }
        
        return $this->_filesPath;
    }


    /**
     * Return relative path where image files are stored
     */
    public function getImagesPath()
    {
        if ( ! $this->_imagesPath )
        {
            $this->_imagesPath = Yii::app()->path->imagesPath();
        }

        return $this->_imagesPath;
    }


    /**
     * Return relative path where temporary files are stored
     */
    public function getTempPath()
    {
        if ( ! $this->_tempPath )
        {
            $this->_tempPath = Yii::app()->path->tempPath();
        }

        return $this->_tempPath;
    }


    /**
     * Event "afterValidate"
     *
     * This method is invoked after validation end
     */
    public function afterValidate()
    {
        // File information
        if ( $this->isNewRecord && $this->scenario == 'upload_document' )
        {
            $this->_save_file();
        }

        // Remove "www" (webRoot)
        /*
        $webRootPath = $this->getWebRootPath();
        if ( preg_match("/^". $webRootPath ."\//", $this->file_path) )
        {
            $this->file_path = str_replace($webRootPath ."/", "/", $this->file_path);
        }
        */

        return parent::afterValidate();
    }


    /**
     * Set file information
     */
    public function set_file_info()
    {
        if ( $this->file && $this->file->getExists() )
        {
            $this->setAttributes([
                'file_name'     => Transliteration::file($this->file->basename, FALSE),
                'file_mime'     => $this->file->mimeType,
                'file_size'     => $this->file->getSize(FALSE)
            ]);

            return true;
        }

        return false;
    }


    /**
     * Load $this->file value with CFile library
     */
    public function load_file()
    {
        if ( empty($this->file) )
        {
            $this->file = Yii::app()->file->set(Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . $this->file_path . $this->file_name);
        }

        if ( $this->file && $this->file->getExists() )
        {
            return true;
        }
        return false;
    }


    /**
     * Deletes the model
     */
    public function delete()
    {
        // Delete file
        $this->_delete_file();

        // Delete statuses
        if ( $this->statuses )
        {
            foreach ( $this->statuses as $asset_file_status_model )
            {
                $asset_file_status_model->delete();
            }
        }
        
        return parent::delete();
    }


    /**
     * Check if the file is an image
     */
    public function is_image($image_path)
    {
        if ( !is_file($image_path) )
        {
            return false;
        }

        // Disable error reporting, to prevent PHP warnings
        $ER = error_reporting(0);

        // Fetch the image size and mime type
        $vec_image_info = getimagesize($image_path);

        // Turn on error reporting again
        error_reporting($ER);

        // Make sure that the image is readable and valid
        if ( !is_array($vec_image_info) || count($vec_image_info) < 3)
        {
            return false;
        }

        return true;
    }


    /**
     * Get default UNIX permissions
     */
    public function get_default_permissions()
    {
        // Permissions in NUMERIC format and without a 0 at the beginning
        // Example: 755
        return 755;
    }


    /**
     * Return file URL
     */
    public function file_url($external_file_type = '')
    {
        $file_path = Yii::app()->params['baseUrl'] . DIRECTORY_SEPARATOR . $this->file_path;

        // Remove relative webroot path ("www")
        $file_path = $this->remove_webroot_path($file_path);
        
        // Remove last "/"
        if ( preg_match("/\/$/", $file_path) )
        {
            $file_path = substr($file_path, 0, -1);
        }

        // Use external URL for files?
        if ( !empty($external_file_type) && isset(Yii::app()->params[$external_file_type]['external_url']) )
        {
            $file_path = str_replace(Yii::app()->params['baseUrl'], Yii::app()->params[$external_file_type]['external_url'], $file_path);
        }

        // Return full path
        return $file_path . DIRECTORY_SEPARATOR . $this->file_name;
    }


    /**
     * Return file URL
     */
    public function get_file_url()
    {
        return $this->file_url();
    }


    /**
     * Return file URL
     */
    public function external_file_url($external_file_type = '')
    {
        return $this->file_url($external_file_type);
    }



    /**
     * Get file path
     */
    public function get_file_path()
    {
        return Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . $this->file_path . $this->file_name;
    }


    /**
     * Remove relative webroot path ("www")
     */
    public function remove_webroot_path($file_path)
    {
        $webroot_path = $this->getWebRootPath();
        if ( preg_match("/\/". $webroot_path ."\//", $file_path) )
        {
            return str_replace('/'. $webroot_path .'/', '/', $file_path);
        }

        return $file_path;
    }


    /**
     * Generate Product directory where save files of this product
     *
     * By default will be "<sku>"
     */
    public function get_product_directory($product_model)
    {
        return $this->get_cleaned_sku($product_model->sku);
        // return trim($product_model->sku);
    }


    /**
     * Get cleaned SKU
     * 
     * Replace special characters
     */
    public function get_cleaned_sku($product_sku)
    {
        $product_sku = StringHelper::clean_text($product_sku);
        $product_sku = str_replace("/", "++", $product_sku);
        $product_sku = str_replace("#", "__", $product_sku);
        $product_sku = str_replace("?", "--", $product_sku);
        $product_sku = str_replace("&", "_and_", $product_sku);
        $product_sku = str_replace("=", "_eq_", $product_sku);
        $product_sku = str_replace("@", "_at_", $product_sku);
        $product_sku = str_replace("$", "_S_", $product_sku);
        $product_sku = str_replace("%", "_p_", $product_sku);
        // $product_sku = Transliteration::file($product_sku, FALSE);
        return $product_sku;
    }


    /**
     * Return file extension
     */
    public function get_file_extension()
    {
        if ( $this->load_file() )
        {
            return $this->file->getExtension();
        }

        return '';
    }


    /**
     * Return formatted file size
     * 
     * @see \dz\components\File::formatFileSize()
     */
    public function get_file_size($format = '0.00')
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');

        $bytes = max($this->file_size, 0);
        $expo = floor(($bytes ? log($bytes) : 0) / log(1024));
        $expo = min($expo, count($units)-1);

        $bytes /= pow(1024, $expo);

        return Yii::app()->numberFormatter->format($format, $bytes) .' '. $units[$expo];
    }


    /**
     * Return file_options attributes
     */
    public function get_file_options()
    {
        if ( !empty($this->file_options) && StringHelper::is_json($this->file_options) )
        {
            return Json::decode($this->file_options);
        }

        return $this->file_options;
    }


    /**
     * Save file process
     */
    public function save_asset_file($file_path = '', $entity_id = '', $entity_type = '', $num_file = null)
    {
        // Exclude file_name in $_POST because it's an attribute value loaded from $_FILES
        if ( $num_file === null && isset($_POST['AssetFile']['file_name']) && empty($_POST['AssetFile']['file_name']) )
        {
            unset($_POST['AssetFile']['file_name']);
        }
        else if ( $num_file !== null && isset($_POST['AssetFile'][$num_file]['file_name']) && empty($_POST['AssetFile'][$num_file]['file_name']) )
        {
            unset($_POST['AssetFile'][$num_file]['file_name']);
        }

        // Single file uploaded (by default)
        $que_file = null;
        if ( $num_file === null )
        {
            if ( isset($_FILES['AssetFile']['name']['file_name']) )
            {
                $que_file = Yii::app()->file->set('AssetFile[file_name]');
            }
            else if ( isset($_FILES['AssetImage']['name']['file_name']) )
            {
                $que_file = Yii::app()->file->set('AssetImage[file_name]');
            }
        }

        // Multiple files uploaded
        else
        {
            if ( isset($_FILES['AssetFile']['name'][$num_file]['file_name']) )
            {
                $que_file = Yii::app()->file->set('AssetFile['. $num_file .'][file_name]');
            }
            else if ( isset($_FILES['AssetImage']['name'][$num_file]['file_name']) )
            {
                $que_file = Yii::app()->file->set('AssetImage['. $num_file .'][file_name]');
            }   
        }

        if ( !empty($que_file) && $que_file->isFile )
        {
            $this->entity_id = !empty($entity_id) ? $entity_id : $this->entity_id;
            $this->entity_type = !empty($entity_type) ? $entity_type : $this->entity_type;
            $this->file_name = Transliteration::file($que_file->basename);
            $this->file_mime = $que_file->mimeType;
            $this->file_size = $que_file->getSize(false);
            $this->file_path = !empty($file_path) ? $file_path : $this->file_path;
            $this->file = $que_file;

            // Change save scenario
            $this->scenario = 'upload_document';

            if ( ! $this->save() )
            {
                Log::save_model_error($this);
                return false;
            }

            return true;
        }

        return false;
    }


    /**
     * Save an AssetFile model from a given directory path
     */
    public function save_asset_file_from_path($file_destination_path)
    {
        // Try to get file from the given destination
        $que_file = Yii::app()->file->set(Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . $file_destination_path);
        
        // Save AssetFile
        if ( !empty($que_file) && $que_file->isFile )
        {
            $file_path = str_replace($que_file->basename, '', $file_destination_path);

            $this->entity_id = !empty($entity_id) ? $entity_id : $this->entity_id;
            $this->entity_type = !empty($entity_type) ? $entity_type : $this->entity_type;
            $this->file_name = Transliteration::file($que_file->basename);
            $this->file_mime = !empty($que_file->mimeType) ? $que_file->mimeType : '-';
            $this->file_size = $que_file->getSize(false);
            $this->file_path = !empty($file_path) ? $file_path : $this->file_path;
            $this->file = $que_file;

            if ( ! $this->save() )
            {
                Log::save_model_error($this);
                return false;
            }

            return true;
        }

        return false;
    }


    /**
     * Move a file to a new destination path
     */
    public function move($destination_path)
    {
        // Check if file exists and if destination path is different from current path
        if ( $this->load_file() && $this->file_path !== $destination_path && $this->file->move($destination_path . $this->file_name) )
        {
            $this->file_path = $destination_path;
            $this->saveAttributes(['file_path' => $this->file_path]);
            return true;
        }

        return false;
    }


    /**
     * Clone the AssetFile model
     * 
     * Create a new AssetFile model into a new destination path
     */
    public function clone($destination_path, $destination_file_name = null)
    {
        // Check if file exists and if destination path is different from current path
        if ( $this->load_file() && $this->file_path !== $destination_path )
        {
            // Ensure destination path exists
            $destination_dir = Yii::app()->file->set($destination_path);
            if ( ! $destination_dir->isDir )
            {
                $default_permissions = 755;
                $destination_dir->createDir($default_permissions, $destination_path);
            }

            // Destination file name
            if ( $destination_file_name === null )
            {
                $destination_file_name = $this->file_name;
            }

            // Copy file into new destination path
            if ( $this->file->copy($destination_path . $destination_file_name) )
            {
                $clone_file_model = Yii::createObject(AssetFile::class);
                $clone_file_model->setAttributes([
                    'file_name'             => $destination_file_name,
                    'file_path'             => $destination_path,
                    'file_mime'             => $this->file_mime,
                    'file_size'             => $this->file_size,
                    'file_options'          => $this->file_options,
                    'entity_id'             => $this->entity_id,
                    'entity_type'           => $this->entity_type,
                    'asset_type'            => $this->asset_type,
                    'title'                 => $this->title,
                    'description'           => $this->description,
                    'original_file_name'    => $this->original_file_name
                ]);
                if ( ! $clone_file_model->save() )
                {
                    Log::save_model_error($clone_file_model);
                    return null;
                }

                return $clone_file_model;
            }
        }

        return null;
    }


    /**
     * Get private or public download URL
     */
    public function download_url()
    {
        if ( !empty($this->uuid) )
        {
            return Url::to('/asset/download', ['id' => $this->uuid]);
        }

        return Url::base();
    }


    /**
     * Get AssetFileStatus model for a "validation_type"
     */
    public function get_status_model($validation_type = 'default', $is_create_if_empty = false)
    {
        $asset_file_status_model = null;
        if ( !empty($this->file_id) )
        {
            $asset_file_status_model = AssetFileStatus::get()
                ->where([
                    'file_id'           => $this->file_id,
                    'validation_type'   => $validation_type
                ])
                ->one();
        }

        if ( $is_create_if_empty && ! $asset_file_status_model )
        {
            $asset_file_status_model = Yii::createObject(AssetFileStatus::class);
            $asset_file_status_model->setAttributes([
                'validation_type'   => $validation_type
            ]);
        }

        return $asset_file_status_model;
    }


    /**
     * Get status_type attribute value from AssetFileStatus model
     */
    public function get_status($validation_type = 'default')
    {
        $asset_file_status_model = $this->get_status_model($validation_type);
        if ( $asset_file_status_model )
        {
            return $asset_file_status_model->status_type;
        }

        return null;
    }


    /**
     * Change AssetFile status
     */
    public function change_status($new_status, $validation_type = 'default', $comments = null, $user_id = null)
    {
        $this->scenario = 'change_status';

        // Get AssetFileStatus model
        $asset_file_status_model = $this->get_status_model($validation_type);

        // First time "status" is saved
        if ( ! $asset_file_status_model )
        {
            $asset_file_status_model = Yii::createObject(AssetFileStatus::class);
            $asset_file_status_model->setAttributes([
                'file_id'           => $this->file_id,
                'validation_type'   => $validation_type,
                'status_type'       => $new_status,
                'comments'          => $comments,
                'user_id'           => $user_id
            ]);

            if ( ! $asset_file_status_model->save() )
            {
                Log::save_model_error($asset_file_status_model);
                return false;
            }
        }

        // Change status of existing AssetFileStatus model
        else
        {
            if ( $user_id !== null )
            {
                $asset_file_status_model->user_id = $user_id;
            }
            return $asset_file_status_model->change_status($new_status, $validation_type, $comments);
        }

        return true;
    }


    /**
     * Return file encoded in base64
     */
    public function base64_encode()
    {
        if ( $this->load_file() )
        {
            return base64_encode($this->file->getContents());
        }
        
        return null;
    }


    /**
     * Save an file
     */
    private function _save_file()
    {
        // Delete previous file?
        if ( !$this->isNewRecord && !empty($this->raw_attributes['file_name']) )
        {
            $old_file = Yii::app()->file->set($this->raw_attributes['file_path'] . DIRECTORY_SEPARATOR . $this->raw_attributes['file_name']);
            if ( $old_file->getExists() )
            {
                $old_file->delete();
            }
        }

        // Default UNIX file permissions = 755
        $default_permissions = $this->get_default_permissions();

        // Directory destination exists?
        $destination_dir = Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . $this->file_path;
        $new_dir = Yii::app()->file->set($destination_dir);
        if ( !$new_dir->isDir )
        {
            $new_dir->createDir($default_permissions, $destination_dir);
        }
        $destination_path = $destination_dir . DIRECTORY_SEPARATOR . $this->file_name;

        // Save file on /destination_dir/<sku>/
        if ( $this->entity_type == 'product' && $this->entity_id > 0 )
        {
            $product_model = Product::model()->findByPk($this->entity_id);
            if ( $product_model )
            {
                // Get cleaned SKU (with Transliteration)
                $cleaned_sku = $this->get_cleaned_sku($product_model->sku);

                // $destination_dir = $destination_dir . DIRECTORY_SEPARATOR . trim($product_model->sku);
                $destination_dir = $destination_dir . DIRECTORY_SEPARATOR . $cleaned_sku;
                $new_dir = Yii::app()->file->set($destination_dir);
                if ( !$new_dir->isDir )
                {
                    $new_dir->createDir($default_permissions, $destination_dir);
                }
                $destination_path = $destination_dir . DIRECTORY_SEPARATOR . $this->file_name;

                // Save new file_path
                $this->file_path .= $cleaned_sku . DIRECTORY_SEPARATOR;
            }
        }

        // Existing file?
        $new_file = Yii::app()->file->set($destination_path);
        if ( $new_file->getExists() )
        {
            $this->file_name = $new_file->filename .'_'. time() .'.'. $new_file->extension;
            $destination_path = $destination_dir . DIRECTORY_SEPARATOR . $this->file_name ;
        }

        // Copy new file into destination path
        $destination_path = str_replace("//", "/", $destination_path);
        if ( ! $this->file->copy($destination_path) )
        {
            $error_message = "Couldn't save file '". $this->file->basename ."' into ". $destination_path;
            Yii::log($error_message, "profile", "error");
            throw new \CException($error_message);
        }

        // Try to change permissions
        $this->file->setPermissions($default_permissions);

        return true;
    }


    /**
     * Delete a file
     */
    private function _delete_file()
    {
        if ( !empty($this->raw_attributes['file_name']) )
        {
            $old_file = Yii::app()->file->set(Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . $this->raw_attributes['file_path'] . DIRECTORY_SEPARATOR . $this->raw_attributes['file_name']);
            if ( $old_file->getExists() )
            {
                $old_file->delete();
            }
        }

        // Reset values
        $this->file_options = null;
        $this->file_size = 0;
        $this->file_name = null;
        $this->file_path = null;
        $this->file_mime = null;
    }
}
