<?php
/**
 * @package dz\modules\asset\models 
 */

namespace dz\modules\asset\models;

use dz\db\DbCriteria;
use dz\modules\asset\models\AssetFile;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\Transliteration;
use dzlab\commerce\models\Product;
use dzlab\commerce\models\ProductImage;
use user\models\User;
use Yii;

/**
 * AssetImage submodel class from AssetFile model class ("asset_file" database table)
 */
class AssetImage extends AssetFile
{
    /**
     * @var string Asset Type
     */
    public $asset_type = parent::ASSET_TYPE_IMAGE;


	/**
     * Non identified images
     */
    public $presets_dir = '_';


	/**
     * Images pending to identify
     */
    public $pending_dir = '_pending';


    /**
     * Override image file?
     */
    public $is_override = true;


    /**
     * Original file name
     */
    public $original_file_name;


    /**
     * File name information
     */
    public $vec_file_name = [];


	/**
	 * Presets configuration
	 */
	protected $vec_config = [];


    /**
     * Exists product image with same file?
     */
    private $_existing_product_image;


	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	

	/**
	 * This method is invoked after each record is instantiated by a find method
	 *
	 * @return void
	 */	
	public function afterFind()
	{
		parent::afterFind();
	}


	/*
	|--------------------------------------------------------------------------
	| CUSTOM FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	/**
	 * Event "afterValidate"
	 *
	 * This method is invoked after validation end
	 */
	public function afterValidate()
    {
    	$que_image = '';

    	// Load file image
    	if ( empty($this->file) )
    	{
    		$this->load_file();
    	}

        // Default UNIX file permissions = 755
        $default_permissions = $this->get_default_permissions();

        // Check if file is readable and is an image
    	if ( $this->file->getReadable() && $this->is_image($this->file->realPath) )
    	{
	    	// Load Iwi object to make image resizes
            Yii::import('@lib.iwi.Iwi');
			$que_image = new \Iwi($this->file->realPath);
			// if ( !empty($que_image) && isset($que_image->width) )
			if ( !empty($que_image) && property_exists($que_image, 'image') )
			{
				if ( property_exists($que_image, 'width') && property_exists($que_image, 'height') && property_exists($que_image, 'ext') )
                {
                    $this->file_options = Json::encode([
                        'width'     => $que_image->width,
                        'height'    => $que_image->height,
                        'ext'       => $que_image->ext
                    ]);
                }
                else if ( is_array($que_image) && isset($que_image['width']) && isset($que_image['height']) && isset($que_image['ext']) )
                {
                    $this->file_options = Json::encode([
                        'width'     => $que_image['width'],
                        'height'    => $que_image['height'],
                        'ext'       => $que_image['ext']
                    ]);
                }
			}
			else
			{
				// Log error message
				$error_message = 'ERROR con Iwi - No se puede acceder al fichero o no es una imagen: '. $this->file->realPath .' - '. print_r($que_image, true);
				Yii::log($error_message, "profile", "error");
				return false;
			}
		}
		else
		{
			// Log error message
			$error_message = 'ERROR - No se puede acceder al fichero o no es una imagen: '. $this->file->realPath;
			Yii::log($error_message, "profile", "error");
			throw new \CException($error_message);
		}

    	// Upload image
		if ( !empty($que_image) && $this->scenario != 'not_upload' && ( $this->isNewRecord OR $this->scenario == 'upload_image' OR $this->scenario == 'upload_manual' ) )
		{
			// Product image with correct format in file name

			//--> Save the file in filesystem with CFile extension

			// Directory destination exists?
			$destination_dir = Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . $this->file_path;
			$new_dir = Yii::app()->file->set($destination_dir);

			if ( !$new_dir->isDir )
			{
				$new_dir->createDir($default_permissions, $destination_dir);
				// $new_dir->setPermissions($default_permissions);
			}
			$destination_path = $destination_dir . DIRECTORY_SEPARATOR . $this->file_name;

			// It's a product image? If so, check if subfolder exists
			// Save image on /destination_dir/<sku>/
			if ( $this->entity_type == 'product' && $this->entity_id > 0 )
			{
				$product_model = Product::model()->findByPk($this->entity_id);
				if ( $product_model )
				{
					$destination_dir = $destination_dir . DIRECTORY_SEPARATOR . $this->get_product_directory($product_model);
					$new_dir = Yii::app()->file->set($destination_dir);
					if ( !$new_dir->isDir )
					{
						$new_dir->createDir($default_permissions, $destination_dir);
						// $new_dir->setPermissions($default_permissions);
					}
					$destination_path = $destination_dir . DIRECTORY_SEPARATOR . $this->file_name;

					// Save new file_path. Add backslash at the end
                    if ( ! preg_match("/\/$/", $this->file_path) )
                    {
                        $this->file_path .= '/';
                    }
					$this->file_path .= $this->get_product_directory($product_model) . DIRECTORY_SEPARATOR;
				}

				$is_copy_file = true;
				
				// Upload via FORM upload -> Generate file name automatically (next number)
				if ( $product_model && $this->scenario == 'upload_manual' )
				{
					if ( $this->isNewRecord )
					{
						// $next_image_number = $product_model->countImages + 1;
						
						// Get next image number based on last weight
						$next_image_number = 1;
						$criteria = new DbCriteria;
						$criteria->compare('product_id', $product_model->product_id);
						$criteria->order = 'weight DESC';
						$product_image_model = ProductImage::model()->find($criteria);
						if ( $product_image_model )
						{
							$next_image_number = $product_image_model->weight + 2;
						}

						$this->file_name = AssetImage::model()->get_product_filename($product_model, $next_image_number, false) .'.'. $this->file->getExtension();
					}
					$destination_path = $destination_dir . DIRECTORY_SEPARATOR . $this->file_name;
				}

				// Upload via BATCH
				else
				{
					// Check if image file exists
					$destination_file = Yii::app()->file->set($destination_path);
					if ( $destination_file->getExists() )
					{
						// $this->_is_existing_image = true;
						$this->_existing_product_image = AssetImage::model()->findByAttributes([
							'file_name'	=> $this->file_name,
							'file_path'	=> $this->file_path,
							'entity_id'	=> $this->entity_id
						]);

						// Image file exits?
						if ( $this->_existing_product_image )
						{
							// Override mode is ON ---> Delete previous image
							if ( $this->is_override )
							{
								$this->_existing_product_image->delete();
							}

							// Override mode is OFF ---> Don't copy new image
							else
							{
								$is_copy_file = false;
							}
						}
					}
				}
				
				// Copy new file into destination path
				$destination_path = str_replace("//", "/", $destination_path);
				if ( $is_copy_file && $this->file->copy($destination_path) )
				{
					// Generate thumbnails
					$this->generate_thumbnails($destination_dir);

					// Move image file to "loaded" folder
					if ( $this->scenario != 'upload_manual' )
					{
						$this->file->move(Yii::app()->params['images']['import_path'] . DIRECTORY_SEPARATOR . 'loaded' . DIRECTORY_SEPARATOR . $this->original_file_name);
					}

					// Try to change permissions of /destination_dir/<sku>/
					$destination_dir_file = Yii::app()->file->set($destination_dir);
					if ( $destination_dir_file->getExists() && $destination_dir_file->isDir )
					{
						$vec_files = $destination_dir_file->getContents();
						if ( !empty($vec_files) )
						{
							foreach ( $vec_files as $que_dir_file_path )
							{
								$que_dir_file = Yii::app()->file->set($que_dir_file_path);
								if ( $que_dir_file && $que_dir_file->getExists() )
								{
									$que_dir_file->setPermissions($default_permissions);
								}
							}
						}
					}
				}
				else
				{
					$error_message = "Couldn't save image file '". $this->file->basename ."' into ". $destination_path;
					Yii::log($error_message, "profile", "error");
					throw new \CException($error_message);
				}
			}
			
			// Upload NON product image
			else if ( $this->isNewRecord OR $this->scenario == 'upload_image' )
			{
				// Delete previous image file
				if ( !$this->isNewRecord && !empty($this->raw_attributes['file_name']) )
				{
					$old_file = Yii::app()->file->set($this->raw_attributes['file_path'] . $this->raw_attributes['file_name']);
					if ( $old_file->getExists() )
					{
						$old_file->delete();
					}
				}

				// Existing image?
				$new_file = Yii::app()->file->set($destination_path);
				if ( $new_file->getExists() )
				{
					$this->file_name = $new_file->filename .'_'. time() .'.'. $new_file->extension;
					$destination_path = $destination_dir . DIRECTORY_SEPARATOR . $this->file_name ;
				}

				// Copy image file to destination path
                $destination_path = str_replace("//", "/", $destination_path);
				if ( ! $this->file->copy($destination_path) )
				{
					$error_message = "Couldn't save image file '". $this->file->basename ."' into ". $destination_path;
					Yii::log($error_message, "profile", "error");
					throw new \CException($error_message);
				}
				else if ( ! $this->is_temp )
				{
					// Try to change permissions
					$this->file->setPermissions($default_permissions);

                    // Generate thumbnails
                    $this->generate_thumbnails($destination_dir . DIRECTORY_SEPARATOR . $this->presets_dir);
				}
			}
		}

		return parent::afterValidate();
    }


    /**
     * Set file information
     */
    public function set_file_info()
    {
    	$this->asset_type = parent::ASSET_TYPE_IMAGE;
    	return parent::set_file_info();
	}


    /**
	 * Deletes the model
	 */
	public function delete()
	{
		// Delete image file
		$this->_delete_image_file();

		// Delete/update related tables model
		if ( $this->entity_type == 'product' )
		{
            $product_image_table = ProductImage::model()->tableName();
			Yii::app()->db->createCommand("DELETE FROM {$product_image_table} WHERE image_id = ". $this->file_id)->execute();
		}
		
		return parent::delete();
	}


	/**
     * Try to identify product (SKU and order) by filename
     * 
     * Generate an array from file_name separated by "_" or "-"
     *
     * Allowed formats:
     * 	1. <sku>_<num>.jpg
     * 	2. <sku>-<num>.jpg
     */
    public function identify_by_filename($file_name)
    {
    	$vec_output = [
    		'type'		=> '',
    		'sku'		=> '',
    		'num'		=> '',
    		'error_id'	=> '',
    		'error_log'	=> ''
    	];

    	$vec_file_name = [$file_name];

		// Nombre de fichero separado por guiones bajos "_"
		$is_guion_bajo = false;
		if ( preg_match ("/\_/", $file_name ) )
		{
			$is_guion_bajo = true;
			$vec_file_name = explode("_", $file_name);
		}

		// Nombre de fichero separado por guiones normales "-"
		else if ( preg_match ("/\-/", $file_name ) )
		{
			$vec_file_name = explode("-", $file_name);
		}


		// Wrong format? "<sku>_<num>.jpg"
		if ( count($vec_file_name) != 2 )
		{
			// Formato de fichero incorrecto
			$vec_output['error_id'] = 'wrong_format';
			$vec_output['error_log'] = " --> Formato de fichero incorrecto\n";
		}

		// If no errors, get sku and number
		if ( empty($vec_output['error_id']) )
		{
			// Check if <sku> has a "-" in its name. For example "RAM3102-00"
			if ( $is_guion_bajo && preg_match ("/\-/", $vec_file_name[0]) )
			{
				$vec_sku_name = explode("-", $vec_file_name[0]);
				$vec_output['sku'] = $vec_sku_name[0];
			}
			else
			{
				$vec_output['sku'] = $vec_file_name[0];
			}


			// Modelo format? "<sku>_<num>".jpg
			
			// Product type
			$vec_output['type'] = 'product';

			// Image number or image version
			$vec_output['num'] = $vec_file_name[1];
		}
		// Yii::log(print_r($vec_output, true), 'profile', 'dev');

		return $vec_output;
    }


    /**
     * Try to identify product (SKU and order) by filename renamed or generated from application
     *
     * Only 1 allowed formats "<sku>-<num>.jpg"
     */
    public function identify_by_renamed_filename($file_name)
    {
    	$vec_output = [
    		'type'		=> '',
    		'sku'		=> '',
    		'num'		=> '',
    		'error_id'	=> '',
    		'error_log'	=> ''
    	];

    	$vec_file_name = [$file_name];

		// Nombre de fichero separado por guiones normales "-"
		if ( preg_match ("/\-/", $file_name ) )
		{
			$vec_file_name = explode("-", $file_name);
		}

		// Wrong format? "<sku>_<num>.jpg"
		if ( count($vec_file_name) != 2 )
		{
			// Formato de fichero incorrecto
			$vec_output['error_id'] = 'wrong_format';
			$vec_output['error_log'] = " --> Formato de fichero incorrecto\n";
		}

		// If no errors, get sku and number
		if ( empty($vec_output['error_id']) )
		{
			// SKU
			$vec_output['sku'] = $vec_file_name[0];
			
			// Product type
			$vec_output['type'] = 'product';

			// Image number or image version
			$vec_output['num'] = $vec_file_name[1];
		}
		// Yii::log(print_r($vec_output, true), 'profile', 'dev');

		return $vec_output;
    }


    /**
	 * Load and returns presets / thumbnails configuration
	 */
	public function get_config($preset_name = '')
	{
		if ( empty($this->vec_config) )
		{
            $this->vec_config = Yii::app()->config->get('components.images');
		}

		// Default value
		if ( empty($this->vec_config) )
		{
			// Presets or thumbnails
			$this->vec_config = [
				'presets' => [
					'original'	=> [
						'name' => 'Tamaño original',
					],
					'large' => [
						'name' 			=> 'Tamaño grande - 1200x1200 (prefijo "L_")',
						'prefix' 		=> 'L_',
						'width'			=> 1200,
						'height'		=> 1200,
						'is_upscale'	=> true
					],
					'medium'=> [
						'name' 			=> 'Tamaño mediano - 656x656 (prefijo "M_")',
						'prefix' 		=> 'M_',
						'width'			=> 656,
						'height'		=> 656,
						'is_upscale'	=> true
					],
					'small' => [
						'name' 			=> 'Tamaño pequeño - 400x400 (prefijo "S_")',
						'prefix' 		=> 'S_',
						'width'			=> 400,
						'height'		=> 400,
						'is_upscale'	=> true
					],
				],
			];
		}

		if ( !empty($preset_name) && isset($this->vec_config['presets'][$preset_name]) )
		{
			return $this->vec_config['presets'][$preset_name];
		}

		return $this->vec_config;
	}


    /**
     * Get default UNIX permissions
     */
    public function get_default_permissions()
    {
        if ( empty($this->vec_config) )
        {
            $this->vec_config = Yii::app()->config->get('components.images');
        }

        // Permissions in NUMERIC format and without a 0 at the beginning
        // Example: 755
        if ( isset($this->vec_config['permissions']) )
        {
            return $this->vec_config['permissions'];
        }

        // By default, return 755
        return 755;
    }


    /**
     * Return image URL
     *
     * $preset can be a string (small, medium, large) or an array with height and width coordinates
     */
    public function image_url($preset = '', $is_shorten_url = false, $is_external_url = false)
    {
    	$image_path = Yii::app()->params['baseUrl'] . DIRECTORY_SEPARATOR . $this->file_path;
    	
    	// Remove last "/"
    	if ( preg_match("/\/$/", $image_path) )
    	{
    		$image_path = substr($image_path, 0, -1);
    	}

    	if ( $is_shorten_url && isset(Yii::app()->params['images']['shorten_path']) )
    	{
    		$image_path = str_replace($this->getImagesPath(), Yii::app()->params['images']['shorten_path'], $image_path);
    	}

    	// Remove relative webroot path ("www")
        // $image_path = str_replace("www/files/", "files/", $image_path);
        $image_path = $this->remove_webroot_path($image_path);

    	// Use external URL for images?
    	if ( $is_external_url && isset(Yii::app()->params['images']['external_url']) )
    	{
    		$image_path = str_replace(Yii::app()->params['baseUrl'], Yii::app()->params['images']['external_url'], $image_path);
    	}

    	// Presets configuration
    	$vec_presets_config = $this->get_config();

    	$preset_prefix = '';
    	if ( !empty($preset) )
    	{
    		// PREDEFINED PRESET: small, medium or large
    		if ( ! is_array($preset) )
    		{
    			$preset = strtoupper($preset);
    			foreach ( $vec_presets_config['presets'] as $preset_id => $que_preset )
    			{
    				// Accepted input PRESET parameter accepted
    				//	- SMALL (by preset_id in uppercase)
    				//	- S_ 	(by prefix with "_" character)
    				//	- S 	(by prefix without "_" character)
    				if ( isset($que_preset['prefix']) && (strtoupper($preset_id) == $preset || $que_preset['prefix'] == $preset OR $que_preset['prefix'] == $preset .'_' ) )
    				{
    					$preset_prefix = $que_preset['prefix'];
    				}
    			}

                // Check if image preset file exists. If not, try to regenerate thumbnails
		    	if ( !empty($preset_prefix) )
		    	{
                    // Build image URL
		    		$image_preset_url = $image_path . DIRECTORY_SEPARATOR . $this->presets_dir . DIRECTORY_SEPARATOR . $preset_prefix . $this->file_name;
                    
                    // Check image path
                    $image_preset_file_path = Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . $this->file_path . $this->presets_dir . DIRECTORY_SEPARATOR . $preset_prefix . $this->file_name;
                    $image_preset_file = Yii::app()->file->set($image_preset_file_path);
                    if ( ! $image_preset_file || ! $image_preset_file->getExists() )
                    {
                        Log::images('WARNING - AssetImage::image_url() - Trying to load a non-existing preset file: "'. $image_preset_file_path .'"');

                        // Try to regenerate thumbnails
                        //$this->regenerate_thumbnails();
                    }

                    return $image_preset_url;
		    	}
		    }

		    // DYNAMIC PRESET GENERATION: Array with width and height
		    else if ( count($preset) == 2 )
		    {
		    	if ( $this->load_file() )
		    	{
		    		return Yii::app()->iwi->load($this->file->realPath)->adaptive($preset[0],$preset[1])->cache();
		    	}
		    }
	    }
	    
	    // No preset. Return original image
	    return $image_path . DIRECTORY_SEPARATOR . $this->file_name;
    }



    /**
     * Return external image URL
     *
     * $preset can be a string (small, medium, large) or an array with height and width coordinates
     */
    public function external_image_url($preset_name = '')
    {
    	return $this->image_url($preset_name, false, true);
    }


    /**
     * Upload an image file and try to associate it with PIM products
     *
     * Correct filename formats:
     * 	- For a model: <sku>_<num>.jpg
     */
    public function import_image_file($que_image_file, $entity_type = 'product')
    {
		$image_file = Yii::app()->file->set($que_image_file);
		$file_name = trim($image_file->getFilename());
		
		$output_log = "Procesando imagen ". $image_file->getBasename() ."\n";
		$product_model = '';
		$error_id = '';

		if ( !empty($file_name) )
		{
			// Create Image model (save image in database)
			$this->setAttributes([
				'file_name'		=> Transliteration::file($image_file->basename, false),
				'file_mime'		=> $image_file->mimeType,
				'file_size'		=> $image_file->getSize(false),
				'file_path'		=> $this->getImagesPath(),
				'entity_type'	=> $entity_type
			]);
			$this->file = $image_file;
			$this->scenario = 'upload_import';

			// Save original name
			$this->original_file_name = $this->file_name;
			// $this->setImage($image_file->getTempName());

			// If file it isn't readable, try to change file permissions
			if ( !$this->file->getReadable() )
			{
				$this->file->setPermissions($this->get_default_permissions()); // 755
				$this->file = Yii::app()->file->set($que_image_file);
			}
				
			// Check if file is readable and is an image
			if ( $this->file->getReadable() && $this->is_image($this->file->realPath) )
    		{
				// Get filename structure
				$this->vec_file_name = $this->identify_by_filename($file_name);

				// Some error on identification? Let's try with "identify_by_renamed_filename" method
				if ( !empty($this->vec_file_name['error_id']) )
				{
					$vec_file_name_by_renamed = $this->identify_by_renamed_filename($file_name);
					if ( empty($vec_file_name_by_renamed['error_id']) && !empty($vec_file_name_by_renamed['sku']) )
					{
						$this->vec_file_name = $vec_file_name_by_renamed;
					}
				}
			}
			else
			{
				$this->vec_file_name['error_id'] = 'wrong_file';
				$this->vec_file_name['error_log'] = " --> Fichero de imagen no accesible o no es una imagen\n";
			}

			// Some error on identification?
			if ( !empty($this->vec_file_name['error_id']) )
			{
				$error_id = $this->vec_file_name['error_id'];
				if ( !empty($this->vec_file_name['error_log']) )
				{
					$output_log .= $this->vec_file_name['error_log'];
				}
			}
			else if ( !empty($this->vec_file_name['sku']) )
			{
				// Identify the product by file name
				$product_model = $this->get_model_by_filename($this->vec_file_name);

				if ( $product_model )
				{
					$this->entity_id = $product_model->product_id;

					// Save AssetImage model, if there is no error
					if ( empty($error_id) )
					{
						// Ensure we have set the correct file name
						$this->file_name = $this->get_product_filename($product_model, $this->vec_file_name['num'], false, $this->vec_file_name['sku']) .'.'. $this->file->getExtension();

						// This function executes "afterValidate" method that save image file
						if ( $this->save() )
						{
							// Exist image file and Override Mode is OFF
							if ( $this->_existing_product_image && !$this->is_override )
							{
								$error_id = 'override';
								$output_log .= "Image file exists and cannot be loaded because override mode is OFF (type: ". $this->vec_file_name['type'] .")";
							}
							else
							{
								// If we arrive here, it means Product has been identified correctly

								// Create ProductImage model
								$product_image_model = Yii::createObject(ProductImage::class);
								$product_image_model->scenario = 'upload_import';
								$product_image_model->setAttributes([
									'file_id'		=> $this->file_id,
									'product_id'	=> $product_model->product_id,
								]);

								$output_log .= " --> Producto identificado: ". $product_model->sku;

								// Weight has been set on file name?
								if ( isset($this->vec_file_name['num']) )
								{
									$product_image_model->weight = $this->vec_file_name['num'];
									$output_log .= " con weight ". $this->vec_file_name['num'];
								}

								
								$output_log .= "\n";

								if ( ! $product_image_model->save() )
								{
									$error_id = 'image_product_save_error';
									$output_log .= "Error saving ProductImage model in database: ". print_r([$product_image_model->getAttributes(), $product_image_model->getErrors()], true) ."\n";
								}
							}
						}
						else
						{
							$error_id = 'product_save_error';
							$output_log .= "Error saving Image model in database: ". print_r([$this->getAttributes(), $this->getErrors()], true) ."\n";
						}
					}				
				}

				// Product not found
				else
				{
					$error_id = 'product_not_found';

					// $this->save();
					$output_log .= " --> Producto NO encontrado\n";
				}
			}

			$output_log .= "\n" ;
		}

		return [
			'log' => $output_log,
			'error_id' => $error_id,
			'product_id' => $product_model ? $product_model->product_id : ''
		];
    }


    /**
     * Get Product model by filename
     */
    public function get_model_by_filename($vec_file_name)
    {
    	return Product::model()->findByAttributes(['sku' => $vec_file_name['sku']]);
    }


    /**
     * Generate Product filename (without extension)
     *
     * <SKU>-NN where NN is the number with 2 digits
     */
    public function get_product_filename($product_model, $num_image, $is_add_extension = true, $sku = '')
    {
    	if ( empty($sku) )
    	{
    		$sku = $product_model->sku;
    	}
    	$sku = $this->get_cleaned_sku($sku);
    	$product_filename = $sku .'-'. sprintf("%02d", $num_image);

    	// Add extension filename?
    	if ( $is_add_extension )
    	{
    		$product_filename .= '.jpg';
    	}
    	return $product_filename;
    }


    /**
     * Get directory where product images will be sabed
     * 
     * By default will be "<sku>"
     */
    public function get_product_directory($product_model)
    {
    	return $this->get_cleaned_sku($product_model->sku);
    }



    /**
     * Get image size
     */
    public function get_image_size($image_path = null)
    {
        if ( $image_path === null && $this->load_file() )
        {
            $image_path = $this->file->realPath;
        }

        // Disable error reporting, to prevent PHP warnings
        $ER = error_reporting(0);

        // Fetch the image size and mime type
        $vec_image_info = getimagesize($image_path);

        // Turn on error reporting again
        error_reporting($ER);

        return $vec_image_info;
    }


    /**
     * Generate thumbnail images
     * 
     * @array 	$vec_prefixes 	(Optional) Generate thumbnails only for these prefixes
     */
    public function generate_thumbnails($destination_dir, $vec_prefixes = [])
    {
        // Default UNIX file permissions = 0755
        $default_permissions = $this->get_default_permissions();

    	// Directory for presets exists?
		// Save presets on /destination_dir/<sku>/_/
		// $destination_presets_dir = Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . $this->file_path . DIRECTORY_SEPARATOR . $this->presets_dir;
		// $destination_presets_dir = $destination_dir . DIRECTORY_SEPARATOR . $this->presets_dir;
		$destination_presets_dir = $destination_dir;
		$new_dir = Yii::app()->file->set($destination_presets_dir);
		if ( ! $new_dir->isDir )
		{
			$new_dir->createDir($default_permissions, $destination_presets_dir);
		}
        else
        {
            $new_dir->setPermissions($default_permissions);
        }

		// Presets configuration
    	$vec_presets_config = $this->get_config();

    	// Make sure, file exists
    	if ( empty($this->file) )
		{
			$this->load_file();
		}

    	// Load Iwi object to make image resizes
        Yii::import('@lib.iwi.Iwi');
    	$que_image = new \Iwi($this->file->realPath);

		// Generate 3 presets: large, medium, small
		// $que_image->adaptive(100,100)->save($destination_presets_dir . DIRECTORY_SEPARATOR . 'S_'. $this->file_name);
		// $que_image->adaptive(300,300)->save($destination_presets_dir . DIRECTORY_SEPARATOR . 'M_'. $this->file_name);
		// $que_image->adaptive(1200,1200)->save($destination_presets_dir . DIRECTORY_SEPARATOR . 'L_'. $this->file_name);

		// Resize image based on width
		// $que_image->resize(400, null)->save($destination_presets_dir . DIRECTORY_SEPARATOR . 'S_'. $this->file_name);
		// $que_image->resize(656, null)->save($destination_presets_dir . DIRECTORY_SEPARATOR . 'M_'. $this->file_name);
		// $que_image->resize(1200,null)->save($destination_presets_dir . DIRECTORY_SEPARATOR . 'L_'. $this->file_name);
		foreach ( $vec_presets_config['presets'] as $preset_name => $que_preset )
		{
			if ( isset($que_preset['prefix']) && isset($que_preset['width']) && isset($que_preset['height']) )
			{
				// Check if there's some filtered prefix ($vec_prefixes)
				if ( empty($vec_prefixes) OR in_array($que_preset['prefix'], $vec_prefixes) )
				{
					// Square dimensions. Generate thumbnail with a proporcional height
					if ( $que_preset['width'] == $que_preset['height'] )
					{
                        // Use HEIGHT as resize reference
                        if ( isset($que_preset['resize_reference']) && $que_preset['resize_reference'] === 'height' )
                        {
    						// Upscale is not allowed
    						if ( isset($que_preset['is_upscale']) && ! $que_preset['is_upscale'] )
    						{
    							if ( $que_preset['height'] > $que_image->height )
    							{
    			                    $que_preset['height'] = $que_image->height;
    							}
    						}

    						$que_image->resize(null, $que_preset['height'])->save($destination_presets_dir . DIRECTORY_SEPARATOR . $que_preset['prefix'] . $this->file_name);
                        }

                        // Use WIDTH as resize reference
                        else
                        {
                            // Upscale is not allowed
                            if ( isset($que_preset['is_upscale']) && ! $que_preset['is_upscale'] )
                            {
                                if ( $que_preset['width'] > $que_image->width )
                                {
                                    $que_preset['width'] = $que_image->width;
                                }
                            }

                            $que_image->resize($que_preset['width'], null)->save($destination_presets_dir . DIRECTORY_SEPARATOR . $que_preset['prefix'] . $this->file_name);
                        }
					}

					// Resize with exact dimensions
					else
					{
						// Upscale is not allowed
						if ( isset($que_preset['is_upscale']) && ! $que_preset['is_upscale'] )
						{
							if ( $que_preset['width'] > $que_image->width )
							{
			                    $que_preset['width'] = $que_image->width;
							}

							if ( $que_preset['height'] > $que_image->height )
							{
			                    $que_preset['height'] = $que_image->height;
							}
						}

						$que_image->adaptive($que_preset['width'], $que_preset['height'])->save($destination_presets_dir . DIRECTORY_SEPARATOR . $que_preset['prefix'] . $this->file_name);
					}
				}
			}
		}
    }


    /**
     * Alias of "generatte_thumbnails" method
     */
    public function generate_presets($destination_dir)
    {
    	return $this->generate_thumbnails($destination_dir);
    }


    /**
     * Regenerate all the thumbnails
     */
    public function regenerate_thumbnails($destination_dir = '')
    {
        // Get preset directory
        if ( empty($destination_dir) )
        {
            $destination_dir = Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . $this->file_path . DIRECTORY_SEPARATOR . $this->presets_dir;
        }

        // Check if it exists and delete all the thumbnails
        $preset_dir = Yii::app()->file->set($destination_dir);
        if ( $preset_dir && $preset_dir->getExists() && $preset_dir->isDir )
        {
            $preset_dir->purge();
        }
        
        return $this->generate_thumbnails($destination_dir);
    }


    /**
     * Get thumbnails options
     */
    public function get_presets_options()
    {
    	// Presets configuration
    	$vec_presets_config = $this->get_config();

    	$vec_presets_options = [];
    	foreach ( $vec_presets_config['presets'] as $preset_name => $que_preset )
		{
			if ( isset($que_preset['name']) )
			{
				$vec_presets_options[$preset_name] = $que_preset['name'];
			}
		}

    	return $vec_presets_options;
    }


    /**
     * Return File object from a preset image
     */
    public function get_preset_file($preset_name)
    {
        $vec_config = $this->get_config($preset_name);
        if ( !empty($vec_config) )
        {
            $preset_file_path = Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . $this->file_path . $this->presets_dir . DIRECTORY_SEPARATOR . $vec_config['prefix'] . $this->file_name;
            return Yii::app()->file->set($preset_file_path);
        }

        return null;
    }

    /**
     * Return File object from a preset image
     * 
     * @see AssetImage::get_preset_file()
     */
    public function load_preset_file($preset_name)
    {
        return $this->get_preset_file($preset_name);
    }


    /**
     * Get preset prefixes
     */
    public function get_presets_prefixes()
    {
    	// Presets configuration
    	$vec_presets_config = $this->get_config();

    	$vec_presets_prefixes = [];
        if ( isset($vec_presets_config['presets']) )
        {
        	foreach ( $vec_presets_config['presets'] as $preset_name => $que_preset )
    		{
    			if ( isset($que_preset['prefix']) )
    			{
    				$vec_presets_prefixes[$preset_name] = $que_preset['prefix'];
    			}
    		}
        }
        else
        {
            $vec_presets_prefixes = ['B_', 'S_', 'T_'];
        }

    	return $vec_presets_prefixes;
    }


    /**
     * Clean (delete) unused thumbnails
     */
    public function clean_thumbnails($destination_path)
    {
    	// Presets configuration
    	$vec_presets_prefixes = $this->get_presets_prefixes();
    	if ( !empty($vec_presets_prefixes) )
    	{
    		$vec_presets_prefixes = array_values($vec_presets_prefixes);
    	}

    	// Make sure, file exists
    	if ( empty($this->file) )
		{
			$this->load_file();
		}

		$vec_deleted_files = [];
		$destination_dir = Yii::app()->file->set($destination_path);
		if ( $destination_dir->isDir )
		{
			/**
			 * Get all files ending with current filename to get presets or thumbnail images
			 * For example, for image "WDS200T2B0A-01.jpg" will get "WDS200T2B0A-01.jpg" (original image)
			 * "S_WDS200T2B0A-01.jpg", "M_WDS200T2B0A-01.jpg" and "L_WDS200T2B0A-01.jpg" (presets images)
			 */
			$vec_preset_files = $destination_dir->getContents(false, "/". $this->file_name ."$/");
			if ( !empty($vec_preset_files) )
			{
				foreach ( $vec_preset_files as $que_preset_file )
				{
					$que_preset_file = str_replace($destination_path, "", $que_preset_file);
					$que_prefix = str_replace($this->file_name, "", $que_preset_file);

					if ( !empty($que_prefix) && !in_array($que_prefix, $vec_presets_prefixes) )
					{
						$deleting_file = Yii::app()->file->set($destination_path . $que_preset_file);
						if ( $deleting_file && $deleting_file->getExists() && $deleting_file->delete() )
						{
							$vec_deleted_files[] = $que_preset_file;
						}
					}
				}
			}
		}

		return $vec_deleted_files;
    }


    /**
     * Save image process
     */
    public function save_asset_image($file_path = '', $entity_id = '', $entity_type = '', $num_image = null)
    {
        // Exclude file_name in $_POST because it's an attribute value loaded from $_FILES
        if ( $num_image == null && isset($_POST['AssetImage']['file_name']) && empty($_POST['AssetImage']['file_name']) )
        {
            unset($_POST['AssetImage']['file_name']);
        }
        else if ( $num_image !== null && isset($_POST['AssetImage'][$num_image]['file_name']) && empty($_POST['AssetImage'][$num_image]['file_name']) )
        {
            unset($_POST['AssetImage'][$num_image]['file_name']);
        }

        // Single image uploaded (by default)
        $image_file = null;
        if ( $num_image === null )
        {
            if ( isset($_FILES['AssetImage']['name']['file_name']) )
            {
                $image_file = Yii::app()->file->set('AssetImage[file_name]');
            }
            else if ( isset($_FILES['AssetFile']['name']['file_name']) )
            {
                $image_file = Yii::app()->file->set('AssetFile[file_name]');
            }
        }

        // Multiple files uploaded
        else
        {
            if ( isset($_FILES['AssetImage']['name'][$num_image]['file_name']) )
            {
                $image_file = Yii::app()->file->set('AssetImage['. $num_image .'][file_name]');
            }
            else if ( isset($_FILES['AssetFile']['name'][$num_image]['file_name']) )
            {
                $image_file = Yii::app()->file->set('AssetFile['. $num_image .'][file_name]');
            }   
        }
        
        if ( !empty($image_file) && $image_file->isFile )
        {
            $this->entity_id = !empty($entity_id) ? $entity_id : $this->entity_id;
            $this->entity_type = !empty($entity_type) ? $entity_type : $this->entity_type;
            $this->file_name = Transliteration::file($image_file->basename);
            $this->file_mime = $image_file->mimeType;
            $this->file_size = $image_file->getSize(false);
            $this->file_path = !empty($file_path) ? $file_path : $this->file_path;
            $this->file = $image_file;

            // Change save scenario
            $this->scenario = 'upload_image';

            if ( ! $this->save() )
            {
                Log::save_model_error($this);
                return false;
            }

            return true;
        }
    }


    /**
     * Save an AssetImage model from a given directory path
     */
    public function save_asset_image_from_path($file_destination_path)
    {
        return $this->save_asset_file_from_path($file_destination_path);
    }


    /**
     * Move image files
     */
    public function move_image($new_destination_dir)
    {
        // Move image file
        if ( $this->load_file() )
        {
            // Default UNIX file permissions = 0755
            $default_permissions = $this->get_default_permissions();

            // Create directory if it does not exist
            $new_dir = Yii::app()->file->set($new_destination_dir);
            if ( ! $new_dir->isDir )
            {
                $new_dir->createDir($default_permissions, $new_destination_dir);
                // $new_dir->setPermissions($default_permissions);
            }

            if ( $this->file->move($new_destination_dir . $this->file_name) )
            {
                // Update AssetFile model
                $old_file_path = $this->file_path;
                $this->file_path = $new_destination_dir;
                if ( ! $this->save() )
                {
                    Log::save_model_error($this);
                    return false;
                }

                // Move all the presets
                // Load "prefixes" from preset images configuration
                $vec_presets_config = $this->get_config();
                $vec_prefixes = [];
                // $vec_prefixes = ['L_', 'M_', 'S_');
                foreach ( $vec_presets_config['presets'] as $preset_name => $que_preset )
                {
                    if ( isset($que_preset['prefix']) )
                    {
                        $vec_prefixes[] = $que_preset['prefix'];
                    }
                }

                // Create preset directory if it does not exist
                if ( $this->entity_type != 'product' )
                {
                    $new_dir = Yii::app()->file->set($new_destination_dir . $this->presets_dir);
                    if ( ! $new_dir->isDir )
                    {
                        $new_dir->createDir($default_permissions, $new_destination_dir . $this->presets_dir);
                        // $new_dir->setPermissions($default_permissions);
                    }
                }

                foreach ( $vec_prefixes as $que_prefix )
                {
                    if ( !empty($que_prefix) )
                    {
                        $current_file_path = $old_file_path . $que_prefix . $this->file_name;
                        $preset_destination_path = $new_destination_dir . $que_prefix . $this->file_name;
                        if ( $this->entity_type != 'product' )
                        {
                           $current_file_path = $old_file_path . $this->presets_dir . DIRECTORY_SEPARATOR . $que_prefix . $this->file_name;
                           $preset_destination_path = $new_destination_dir . $this->presets_dir . DIRECTORY_SEPARATOR . $que_prefix . $this->file_name;
                        }

                        $old_file = Yii::app()->file->set($current_file_path);
                        if ( $old_file->getExists() )
                        {
                            $old_file->move($preset_destination_path);
                        }
                    }
                }

                return true;
            }
        }

        return false;
    }



    /**
	 * Delete an image file
	 */
	private function _delete_image_file()
	{
		if ( !empty($this->raw_attributes['file_name']) )
		{
			// Load "prefixes" from preset images configuration
			$vec_presets_config = $this->get_config();
			$vec_prefixes = [''];
			// $vec_prefixes = ['', 'L_', 'M_', 'S_');
			foreach ( $vec_presets_config['presets'] as $preset_name => $que_preset )
			{
				if ( isset($que_preset['prefix']) )
				{
					$vec_prefixes[] = $que_preset['prefix'];
				}
			}

			foreach ( $vec_prefixes as $que_prefix )
			{
                $que_file_path = $this->raw_attributes['file_path'] . $que_prefix . $this->raw_attributes['file_name'];
                if ( !empty($que_prefix) && $this->entity_type != 'product' )
                {
				   $que_file_path = $this->raw_attributes['file_path'] . $this->presets_dir . DIRECTORY_SEPARATOR . $que_prefix . $this->raw_attributes['file_name'];
                }

				$old_file = Yii::app()->file->set($que_file_path);
				if ( $old_file->getExists() )
				{
					$old_file->delete();
				}
			}
		}

		// Reset values
		$this->file_options = null;
		$this->file_size = 0;
		$this->file_name = null;
		$this->file_path = null;
		$this->file_mime = null;
	}
}