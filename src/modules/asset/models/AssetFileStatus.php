<?php
/**
 * @package dz\modules\asset\models 
 */

namespace dz\modules\asset\models;

use dz\db\DbCriteria;
use dz\helpers\App;
use dz\helpers\DateHelper;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\asset\models\_base\AssetFileStatus as BaseAssetFileStatus;
use dz\modules\asset\models\AssetFile;
use dz\modules\asset\models\AssetImage;
use dz\modules\settings\models\StatusHistory;
use user\models\User;
use Yii;

/**
 * AssetFileStatus model class for "asset_file_status" database table
 *
 * Columns in table "asset_file_status" available as properties of the model,
 * followed by relations of table "asset_file_status" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $file_id
 * @property string $validation_type
 * @property string $status_type
 * @property integer $user_id
 * @property integer $accepted_date
 * @property integer $accepted_uid
 * @property integer $rejected_date
 * @property integer $rejected_uid
 * @property string $comments
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $acceptedUser
 * @property mixed $createdUser
 * @property mixed $file
 * @property mixed $rejectedUser
 * @property mixed $updatedUser
 * @property mixed $user
 */
class AssetFileStatus extends BaseAssetFileStatus
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    /**
     * Label with translation support (from GIIX)
     */
    public static function label($n = 1)
    {
        return Yii::t('app', 'File Status|File Statuses', $n);
    }
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
            ['file_id, created_date, created_uid, updated_date, updated_uid', 'required'],
            ['file_id, user_id, accepted_date, accepted_uid, rejected_date, rejected_uid, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
            ['validation_type', 'length', 'max'=> 32],
            ['comments', 'length', 'max'=> 512],
            ['uuid', 'length', 'max'=> 36],
            ['status_type', 'in', 'range' => ['pending', 'accepted', 'rejected', 'incomplete']],
            ['validation_type, status_type, user_id, accepted_date, accepted_uid, rejected_date, rejected_uid, comments, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
            ['file_id, validation_type, status_type, user_id, accepted_date, accepted_uid, rejected_date, rejected_uid, comments, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'file' => [self::BELONGS_TO, AssetFile::class, 'file_id'],
            'image' => [self::BELONGS_TO, AssetImage::class, 'file_id'],
            'user' => [self::BELONGS_TO, User::class, 'user_id'],
            'acceptedUser' => [self::BELONGS_TO, User::class, ['accepted_uid' => 'id']],
            'rejectedUser' => [self::BELONGS_TO, User::class, ['rejected_uid' => 'id']],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],
			
            // Custom relations
            'statusHistory' => [self::HAS_MANY, StatusHistory::class, ['entity_id' => 'file_id'], 'condition' => 'statusHistory.entity_type = "AssetFile"', 'order' => 'statusHistory.history_id DESC'],
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'accepted_date' => 'd/m/Y - H:i',
                    'rejected_date' => 'd/m/Y - H:i',
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'file_id' => Yii::t('app', 'File'),
			'validation_type' => Yii::t('app', 'Validation Type'),
			'status_type' => Yii::t('app', 'Status Type'),
			'user_id' => Yii::t('app', 'User Owner'),
			'accepted_date' => Yii::t('app', 'Accepted Date'),
            'accepted_uid' => null,
            'rejected_date' => Yii::t('app', 'Rejected Date'),
            'rejected_uid' => null,
            'comments' => Yii::t('app', 'Comments'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'file' => null,
            'user' => null,
            'acceptedUser' => null,
            'rejectedUser' => null,
            'createdUser' => null,
			'updatedUser' => null,
			
		];
	}


    /**
     * Get "status_type" labels
     */
    public function status_type_labels()
    {
        return [
            'pending' => Yii::t('app', 'Pending'),
            'accepted' => Yii::t('app', 'Accepted'),
            'rejected' => Yii::t('app', 'Rejected'),
            'incomplete' => Yii::t('app', 'Incomplete'),
        ];
    }


    /**
     * Get "status_type" specific label
     */
    public function status_type_label($status_type = null)
    {
        if ( $status_type === null )
        {
            $status_type = $this->status_type;
        }
        $vec_labels = $this->status_type_labels();
        return isset($vec_labels[$status_type]) ? $vec_labels[$status_type] : '';
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.file_id', $this->file_id);
        $criteria->compare('t.validation_type', $this->validation_type);
        $criteria->compare('t.status_type', $this->status_type);
        $criteria->compare('t.user_id', $this->user_id);
        $criteria->compare('t.accepted_date', $this->accepted_date);
        $criteria->compare('t.rejected_date', $this->rejected_date);
        $criteria->compare('t.comments', $this->comments, true);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['file_id' => true]]
        ]);
    }


    /**
     * AssetFileStatus models list
     * 
     * @return array
     */
    public function assetfilestatus_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['file_id', 'uuid'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = AssetFileStatus::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('file_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /*
    |--------------------------------------------------------------------------
    | CUSTOM FUNCTIONS
    |--------------------------------------------------------------------------
    */


    /**
     * Event "afterSave"
     *
     * This method is invoked after saving a record successfully
     */
    public function afterSave()
    {
        switch ( $this->scenario )
        {
            // Add new promotor
            case 'insert':
                // Save STATUS history
               $this->save_status_history($this->status_type, $this->validation_type);
            break;
        }

        return parent::afterSave();
    }


    /**
     * Deletes the model
     */
    public function delete()
    {
        // Delete history
        if ( $this->statusHistory )
        {
            foreach ( $this->statusHistory as $status_history_model )
            {
                $status_history_model->delete();
            }
        }
        
        return parent::delete();
    }


    /**
     * Change AssetFile status
     */
    public function change_status($new_status, $comments = null)
    {
        $this->scenario = 'change_status';

        // Save old status
        $old_status = $this->status_type;

        // Set new status
        $this->status_type = $new_status;

        // Status has been changed?
        if ( $old_status !== $this->status_type )
        {
            // Reset "date" values
            $this->accepted_date = null;
            $this->accepted_uid = null;
            $this->rejected_date = null;
            $this->rejected_uid = null;

            // Comments
            $this->comments = $comments;

            if ( $this->status_type === 'accepted' )
            {
                $this->accepted_date = time();
                $this->accepted_uid = ! App::is_console() ? Yii::app()->user->id : 1;
            }
            else if ( $this->status_type === 'rejected' )
            {
                $this->rejecetd_date = time();
                $this->rejecetd_uid = ! App::is_console() ? Yii::app()->user->id : 1;
            }

            if ( ! $this->save() )
            {
                Log::save_model_error($this);
                return false;
            }

            // Save status history
            $this->save_status_history($new_status, $comments);
        }

        // Save comments only
        else
        {
            $this->comments = $comments;

            if ( ! $this->save() )
            {
                Log::save_model_error($this);
                return false;
            }
        }

        return true;
    }


        /**
     * Save new status into status history
     */
    public function save_status_history($new_status, $comments = null)
    {
        // Get last StatusHistory model
        $last_status_history_model = StatusHistory::get()
            ->where([
                'entity_id'     => $this->file_id,
                'entity_type'   => 'AssetFile'
            ])
            ->order('history_id DESC')
            ->one();

        if ( ! $last_status_history_model || $last_status_history_model->status_type !== $new_status )
        {
            $status_history_model = Yii::createObject(StatusHistory::class);
            $status_history_model->setAttributes([
                'entity_id'         => $this->file_id,
                'entity_type'       => 'AssetFile',
                'entity_scenario'   => $this->validation_type,
                'status_type'       => $new_status,
                'comments'          => $comments,
            ]);

            if ( ! $status_history_model->save() )
            {
                Log::save_model_error($status_history_model);
                return false;
            }
        }

        return true;
    }


    /**
     * Get private or public download URL
     */
    public function download_url()
    {
        if ( $this->file )
        {
            return $this->file->download_url();
        }

        return '#';
    }


    /**
     * Title used for this model
     */
    public function title()
    {
        if ( $this->file )
        {
            return $this->file->title();
        }
        
        return '';
    }
}