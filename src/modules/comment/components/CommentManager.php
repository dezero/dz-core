<?php
/**
 * Comments Manager
 * 
 * Helper classes to work with Comment models
 */

namespace dz\modules\comment\components;

use dz\base\ApplicationComponent;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\comment\models\Comment;
use Yii;

class CommentManager extends ApplicationComponent
{
    /**
     * Send a new comment
     */
    public function send_comment($comment_model, $is_sending_mail = false, $is_save_log = true)
    {
        if ( $comment_model->save() )
        {
            if ( $is_sending_mail )
            {
                $sending_result = Yii::app()->mail
                    ->addModel($comment_model, 'comment')
                    ->setEntityInfo($comment_model->entity_id, $comment_model->entity_type)
                    ->compose('new_comment')
                    ->send();

                // Save error into LOG
                if ( ! $sending_result )
                {
                    Yii::app()->mail->logError();
                }
            }

            return true;
        }
        else if ( $is_save_log )
        {
            Log::save_model_error($comment_model);
        }

        return false;
    }


    /**
     * Get total comments
     */
    public function get_total_comments($entity_type, $entity_id)
    {
        $comment_table_name = Comment::model()->tableName();
        return Yii::app()->db->createCommand("SELECT COUNT(*) FROM `{$comment_table_name}` t WHERE t.entity_type = :entity_type AND t.entity_id = :entity_id AND t.disable_date IS NULL AND t.status_type IN ('pending', 'approved')")->bindValue(':entity_type', $entity_type)->bindValue(':entity_id', $entity_id)->queryScalar();
    }


     /**
     * Returns entity information as label, URL or model
     */
    public function get_entity_info($comment_model)
    {
        /**
         * @todo: Load an entity model by "entity_type" and "entity_id"
         */
        return [
            'model' => null,
            'label' => $comment_model->entity_type,
            'title' => '',
            'url'   => '',
        ];
    }
}
