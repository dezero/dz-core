<?php
/**
 * CommentList component class file
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2021 Dezero
 */

namespace dz\modules\comment\components;

use dz\db\DbCriteria;
use dz\entities\EntityList;
use dz\modules\comment\models\Comment;
use dz\helpers\Url;
use Yii;

/**
 * CommentList component
 */
class CommentList extends EntityList
{

    /**
     * Return a list with Comment models
     */
    public function get_comments($type, $id)
    {
        $this->criteria = new DbCriteria;
        $this->criteria->compare('t.entity_type', $type);
        $this->criteria->compare('t.entity_id', $id);

        // Total items
        $this->criteria_count = $this->criteria;
        $this->total_items = Comment::model()->count($this->criteria_count);

        // Show only "first level" comments
        $this->criteria->addCondition('comment_parent_id IS NULL');

        // Order by
        $this->criteria->order = 't.created_date DESC';
        
        return Comment::model()->findAll($this->criteria);
    }
}