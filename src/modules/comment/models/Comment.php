<?php
/**
 * @package dz\modules\comment\models
 */

namespace dz\modules\comment\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\helpers\Url;
use dz\modules\comment\models\_base\Comment as BaseComment;
use dz\modules\settings\models\Language;
use user\models\User;
use Yii;

/**
 * Comment model class for "comment" database table
 *
 * Columns in table "comment" available as properties of the model,
 * followed by relations of table "comment" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $comment_id
 * @property integer $user_id
 * @property string $comment_type
 * @property integer $comment_parent_id
 * @property string $status_type
 * @property string $comment
 * @property string $language_id
 * @property string $entity_id
 * @property string $entity_type
 * @property integer $weight
 * @property integer $disable_date
 * @property integer $disable_uid
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $commentParent
 * @property mixed $comments
 * @property mixed $createdUser
 * @property mixed $disableUser
 * @property mixed $language
 * @property mixed $updatedUser
 * @property mixed $user
 */
class Comment extends BaseComment
{
    /**
     * Send mail?
     */
    public $is_sending_mail = false;


    /**
     * Search filters
     */
    public $user_filter;
    public $entity_filter;


	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['user_id, created_date, created_uid, updated_date, updated_uid', 'required'],
			['user_id, comment_parent_id, weight, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['comment_type, entity_type', 'length', 'max'=> 32],
			['language_id', 'length', 'max'=> 4],
			['entity_id', 'length', 'max'=> 64],
			['uuid', 'length', 'max'=> 36],
			['status_type', 'in', 'range' => ['approved', 'pending', 'spam', 'disabled']],
			['comment_type, comment_parent_id, status_type, comment, language_id, entity_id, entity_type, weight, disable_date, disable_uid, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['comment', 'safe'],
			['comment_id, user_id, comment_type, comment_parent_id, status_type, comment, language_id, entity_id, entity_type, weight, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid, uuid, user_filter, entity_filter', 'safe', 'on' => 'search'],
		];
	}


	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'author' => [self::BELONGS_TO, User::class, ['user_id' => 'id']],
            'user' => [self::BELONGS_TO, User::class, ['user_id' => 'id']],
            'language' => [self::BELONGS_TO, Language::class, 'language_id'],
			'disableUser' => [self::BELONGS_TO, User::class, ['disable_uid' => 'id']],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations - Between comments
            'commentParent' => [self::BELONGS_TO, Comment::class, 'comment_parent_id'],
            'subComments' => [self::HAS_MANY, Comment::class, 'comment_parent_id', 'order' => 'subComments.created_date ASC'],
            // 'comments' => [self::HAS_MANY, Comment::class, 'comment_parent_id'],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }


	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'comment_id' => Yii::t('app', 'Comment'),
			'user_id' => Yii::t('app', 'Author'),
			'comment_type' => Yii::t('app', 'Comment Type'),
			'comment_parent_id' => null,
			'status_type' => Yii::t('app', 'Status Type'),
			'comment' => Yii::t('app', 'Comment'),
			'language_id' => null,
			'entity_id' => Yii::t('app', 'Entity'),
			'entity_type' => Yii::t('app', 'Entity Type'),
			'weight' => Yii::t('app', 'Weight'),
			'disable_date' => Yii::t('app', 'Disable Date'),
			'disable_uid' => null,
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'commentParent' => null,
			'comments' => null,
			'createdUser' => null,
			'disableUser' => null,
			'language' => null,
			'updatedUser' => null,
            'author' => Yii::t('app', 'Author'),
			'user' => Yii::t('app', 'Author'),
		];
	}


    /**
     * Get "status_type" labels
     */
    public function status_type_labels()
    {
        return [
            'approved' => Yii::t('app', 'Approved'),
            'pending' => Yii::t('app', 'Pending'),
            'spam' => Yii::t('app', 'SPAM'),
            'disabled' => Yii::t('app', 'Disabled'),
        ];
    }


    /**
     * Get "status_type" specific label
     */
    public function status_type_label($status_type = null)
    {
        if ( $status_type === null )
        {
            $status_type = $this->status_type;
        }
        $vec_labels = $this->status_type_labels();
        return isset($vec_labels[$status_type]) ? $vec_labels[$status_type] : '';
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search($search_id = '')
    {
        $criteria = new DbCriteria;

        $criteria->with = [];
        $criteria->together = true;

        $criteria->compare('t.comment_id', $this->comment_id);
        $criteria->compare('t.comment_type', $this->comment_type);
        $criteria->compare('t.status_type', $this->status_type);
        $criteria->compare('t.comment', $this->comment, true);
        $criteria->compare('t.entity_id', $this->entity_id);
        $criteria->compare('t.entity_type', $this->entity_type);

        // Search by email, firstname and/or lastname
        if ( $this->user_filter )
        {
            $criteria->with[] = 'user';
            $criteria->addCondition('user.email LIKE CONCAT(:user_filter, "%") OR user.firstname LIKE CONCAT("%", :user_filter, "%") OR user.lastname LIKE CONCAT("%", :user_filter, "%") OR CONCAT(user.firstname, " ", user.lastname) LIKE CONCAT(:user_filter, "%")');
            $criteria->addParams([':user_filter' => $this->user_filter]);
        }

        // Search by entity
        if ( $this->entity_filter )
        {
            $criteria->addCondition('t.entity_id = :entity_filter OR t.entity_type = :entity_filter OR CONCAT(t.entity_type, " ", t.entity_id) LIKE CONCAT(:entity_filter, "%")');
            $criteria->addParams([':entity_filter' => $this->entity_filter]);
        }

        // $criteria->compare('t.weight', $this->weight);
        // $criteria->compare('t.disable_date', $this->disable_date);
        // $criteria->compare('t.created_date', $this->created_date);
        // $criteria->compare('t.updated_date', $this->updated_date);
        // $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 20],
            'sort' => ['defaultOrder' => ['created_date' => false]]
        ]);
    }


    /**
     * Comment models list
     *
     * @return array
     */
    public function comment_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['comment_id', 'comment_type'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';

        $vec_models = Comment::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('comment_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Event "beforeValidate"
     *
     * This method is invoked before validation starts
     *
     * @return bool
     */
    public function beforeValidate()
    {
        switch ( $this->scenario )
        {
            // Create new Comment
            case 'insert':
                if ( empty($this->user_id) )
                {
                    $this->user_id = (Yii::app() instanceof \CConsoleApplication) ? 1 : Yii::app()->user->id;
                }
            break;
        }

        return parent::beforeValidate();
    }


    /**
     * Return author's name
     */
    public function author_name()
    {
        if ( $this->author )
        {
            return $this->author->fullname();
        }

        return '';
    }


    /**
     * Return comment view URL
     */
    public function url()
    {
        return Url::to('/comment/comment/view', ['id' => $this->comment_id]);
    }


    /**
     * Returns the mail tokens
     */
    public function mailTokens()
    {
        $vec_entity_info = Yii::app()->commentManager->get_entity_info($this);

        return [
            '{comment.date}'            => is_numeric($this->created_date) ? DateHelper::unix_to_date($this->created_date) : $this->created_date,
            '{comment.comment}'         => nl2br($this->comment),
            '{comment.reference}'       => StringHelper::uppercase($this->entity_type .' '. $this->entity_id),
            '{comment.author}'          => $this->createdUser ? $this->createdUser->fullname() : '',
            '{comment.url}'             => $this->url(),
            '{comment.entity_type}'     => $vec_entity_info['label'],
            '{comment.entity_title}'    => $vec_entity_info['title'],
            '{comment.entity_url}'      => $vec_entity_info['url'],
            // '{contact.language}'    => $this->language_id,
        ];
    }


    /**
     * Help text for the mail tokens
     */
    public function mailHelp()
    {

        return [
            '{comment.date}'            => 'Comment sent date',
            '{comment.comment}'         => 'Comment content',
            '{comment.reference}'       => 'Comment reference',
            '{comment.author}'          => 'Comment fullname author',
            '{comment.url}'             => 'Comment URL',
            '{comment.entity_type}'     => 'Entity type. For example, file, user or product',
            '{comment.entity_title}'    => 'Entity title',
            '{comment.entity_url}'      => 'Direct link to entity view page',
            // '{contact.language}'    => 'Contact selected language',
        ];
    }
}
