<?php
/**
 * Module to manage comments
 */

namespace dz\modules\comment;

class Module extends \dz\web\Module
{
    /**
     * @var array mapping from controller ID to controller configurations.
     */
    public $controllerMap = [
        'comment' => [
            'class' => 'dz\modules\comment\controllers\CommentController',
        ],
    ];

    /**
     * Default controller
     */
    public $defaultController = 'comment';


    /**
     * Load specific CSS or JS files for this module
     */
    public $cssFiles = null; // ['comment.css'];
    public $jsFiles = null; // ['comment.js'];


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        parent::init();
    }
}
