<?php
/*
|--------------------------------------------------------------------------
| Comment controller class
|--------------------------------------------------------------------------
*/

namespace dz\modules\comment\controllers;

use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\comment\models\Comment;
use dz\web\Controller;
use user\models\User;
use Yii;

class CommentController extends Controller
{
    /**
     * Main action
     */
    public function actionIndex()
    {
        // Comment model
        $comment_model = Yii::createObject(Comment::class, 'search');
        $comment_model->unsetAttributes();
        
        // Filters?
        if ( isset($_GET['Comment']) )
        {
            $comment_model->setAttributes($_GET['Comment']);
        }

        // Render comment index page
        $this->render('//comment/index', [
            'comment_model'     => $comment_model
        ]);
    }


    /**
     * Delete action for Comment model
     */
    public function actionDelete($id)
    {
        // Delete action only allowed by POST requests
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $model = $this->loadModel($id, Comment::class);
            if ( $model->delete() )
            {
                if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
                {
                    Yii::app()->user->addFlash('success', 'Comment removed successfully');
                }
                else
                {
                    echo Yii::t('app', 'Comment removed successfully');
                }
            }
            else
            {
                $this->showErrors($model->getErrors());
            }
            
            if ( !Yii::app()->getRequest()->getIsAjaxRequest() )
            {
                $this->redirect(['index']);
            }
        }
        else
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }
    }
}
