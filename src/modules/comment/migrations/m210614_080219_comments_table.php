<?php
/**
 * Migration class m210614_080219_comments_table
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210614_080219_comments_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "comment" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('comment', true);

        $this->createTable('comment', [
            'comment_id' => $this->primaryKey(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'comment_type' => $this->string(32)->notNull()->defaultValue('default'),
            'comment_parent_id' => $this->integer()->unsigned(),
            'status_type' => $this->enum('status_type', ['pending', 'approved', 'disabled', 'spam'])->notNull()->defaultValue('pending'),
            'comment' => $this->text(),
            'language_id' => $this->string(4)->notNull()->defaultValue('es'),
            'entity_id' => $this->string(64),
            'entity_type' => $this->string(32),
            'weight' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(1),
            'disable_date' => $this->date(),
            'disable_uid' => $this->integer()->unsigned(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);
    
        // Create indexes
        $this->createIndex(null, 'comment', ['comment_type'], false);
        $this->createIndex(null, 'comment', ['status_type'], false);
        $this->createIndex(null, 'comment', ['entity_id', 'entity_type'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'comment', ['user_id'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'comment', ['comment_parent_id'], 'comment', ['comment_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'comment', ['language_id'], 'language', ['language_id'], 'RESTRICT', null);
        $this->addForeignKey(null, 'comment', ['disable_uid'], 'user_users', ['id'], 'SET NULL', null);
        $this->addForeignKey(null, 'comment', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'comment', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

        
        // Permissions
        // -------------------------------------------------------------------------
        $this->insertMultiple('user_auth_item', [
            [
                'name'          => 'comment.comment.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Comments - Comment - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'comment.comment.update',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Comments - Comment - Edit comments',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'comment_manage',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Comments - Full access to comments',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'comment_edit',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Comments - Edit comments',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'comment_view',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Comments - View comments',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
        ]);

        // Set items relationships
        $this->insertMultiple('user_auth_item_child', [
            [
                'parent'    => 'comment_manage',
                'child'     => 'comment.comment.*'
            ],
            [
                'parent'    => 'comment_edit',
                'child'     => 'comment.comment.update'
            ],
            [
                'parent'    => 'admin',
                'child'     => 'comment_manage'
            ],
            // [
            //     'parent'    => 'editor',
            //     'child'     => 'comment_edit'
            // ],
        ]);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('comment');
		return false;
	}
}

