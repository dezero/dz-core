<?php
/**
 * OperationController class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2012-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package auth.controllers
 */

namespace dz\modules\auth\controllers;

use dz\modules\auth\controllers\_base\AuthItemController;

/**
 * Controller for operation related actions.
 */
class OperationController extends AuthItemController
{
	/**
	 * @var integer the item type (0=operation, 1=task, 2=role).
	 */
	public $type = \CAuthItem::TYPE_OPERATION;
}
