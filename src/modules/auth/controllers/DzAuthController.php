<?php
/*
|--------------------------------------------------------------------------
| Controller class for Auth Items Assignments
|--------------------------------------------------------------------------
*/

namespace dz\modules\auth\controllers;

use dz\modules\auth\controllers\_base\AuthController;
use user\models\User;
use Yii;

class DzAuthController extends AuthController
{
	/**
	 * Default action
	 *
	 * @var string
	 */
	public $defaultAction = 'roles';
	
	
	/**
	 * Displays a checbox table list with users as rows and roles as columns
	 */
	public function actionRoles()
	{
		// Auth Manager
		$am = Yii::app()->getAuthManager();
		
		// Item types (0=operation, 1=task, 2=role).
		$vec_roles = $am->getAuthItems(2);
		$vec_users = User::model()->findAll();
		$vec_assignments = [];
		
		foreach ( $vec_users as $que_user )
		{
			// Assigments: user -> roles
			$vec_assignments[$que_user->username] = array_keys($am->getAuthItems(2, $que_user->id));

			if ( !empty($_POST) )
			{
				// Save new assignments: user -> new role
				if ( isset($_POST[$que_user->username]) )
				{
					$vec_user_roles = $_POST[$que_user->username];
					foreach ( $vec_user_roles as $que_user_role )
					{
						if ( !$am->isAssigned($que_user_role, $que_user->id) )
						{
							$am->assign($que_user_role, $que_user->id);
							if ($am instanceof CPhpAuthManager)
							{
								$am->save();
							}
							if ($am instanceof ICachedAuthManager)
							{
								$am->flushAccess($que_user_role, $que_user->id);
							}
						
							// Add new role to user
							$vec_assignments[$que_user->username][] = $que_user_role;
						}
					}
				}
				
				// Revoke assignments
				foreach ( $vec_assignments[$que_user->username] as $que_user_role )
				{
					if ( ( !isset($_POST[$que_user->username]) || !in_array($que_user_role, $_POST[$que_user->username]) ) && $am->isAssigned($que_user_role, $que_user->id) )
					{
						// d("Eliminar '". $que_user_role ."' para usuario '". $que_user->username ."'");
						$am->revoke($que_user_role, $que_user->id);
						if ($am instanceof CPhpAuthManager)
						{
							$am->save();
						}
						if ($am instanceof ICachedAuthManager)
						{
							$am->flushAccess($que_user_role, $que_user->id);
						}
						
						// Remove role from user assigments
						$vec_assignments[$que_user->username] = array_diff($vec_assignments[$que_user->username], [$que_user_role]);
					}
				}
			}			
		}
			
		if ( !empty($_POST) )
		{
			Yii::app()->user->addFlash('success', 'Cambios guardados correctamente');
		}
		
		$this->render('roles', [
			'roles' => $vec_roles,
			'users' => $vec_users,
			'assignments' => $vec_assignments
		]);
	}
	
	
	/**
	 * Displays a checbox table list with tasks as rows and roles as columns
	 */
	public function actionTasks()
	{
		// Auth Manager
		$am = Yii::app()->getAuthManager();

		// Item types (0=operation, 1=task, 2=role).
		$vec_roles = $am->getAuthItems(2);
		$vec_tasks = $am->getAuthItems(1);
		$vec_assignments = [];
		
		foreach ( $vec_tasks as $que_task )
		{
			// Assigments: task -> roles
			$vec_assignments[$que_task->name] = array_keys($am->getAncestors($que_task->name));
			
			if ( !empty($_POST) )
			{
				// Save new assignments: role -> new task
				if ( isset($_POST[$que_task->name]) )
				{
					$vec_task_roles = $_POST[$que_task->name];
					foreach ( $vec_task_roles as $que_task_role )
					{
						if ( !$am->hasItemChild($que_task_role, $que_task->name) )
						{
							$am->addItemChild($que_task_role, $que_task->name);
							if ($am instanceof CPhpAuthManager)
							{
								$am->save();
							}
							
							// Add new role (parent) to task (child)
							$vec_assignments[$que_task->name][] = $que_task_role;
						}
					}
				}
				
				// Revoke assignments
				foreach ( $vec_assignments[$que_task->name] as $que_task_role )
				{
					if ( ( !isset($_POST[$que_task->name]) || !in_array($que_task_role, $_POST[$que_task->name]) ) && $am->hasItemChild($que_task_role, $que_task->name) )
					{
						// d("Eliminar '". $que_task_role ."' para tarea '". $que_task->name ."'");
						$am->removeItemChild($que_task_role, $que_task->name);
						if ($am instanceof CPhpAuthManager)
						{
							$am->save();
						}
						
						// Remove role (parent) from task (child)
						$vec_assignments[$que_task->name] = array_diff($vec_assignments[$que_task->name], [$que_task_role]);
					}
				}
			}			
		}
		
		// d($vec_assignments);
			
		if ( !empty($_POST) )
		{
			Yii::app()->user->addFlash('success', 'Cambios guardados correctamente');
		}
		
		$this->render('//auth/dzAuth/tasks', [
			'roles' => $vec_roles,
			'tasks' => $vec_tasks,
			'assignments' => $vec_assignments
		]);
	}
	
	
	/**
	 * Displays a checbox table list with operations as rows and the task as single column
	 */
	public function actionTask($task_name)
	{	
		// Auth Manager
		$am = Yii::app()->getAuthManager();
		
		// Item types (0=operation, 1=task, 2=role).
		$vec_operations = $am->getAuthItems(0);
		$vec_assignments = [];

		if ( !empty($_POST) )
		{
			$input_post = [];
			foreach ( $_POST as $post_key => $post_value )
			{
				$input_post[str_replace("_", ".", $post_key)] = $post_value;
			}
		}
		
		foreach ( $vec_operations as $que_operation )
		{
			// Operations assigments
			$vec_assignments = array_keys($am->getDescendants($task_name));
			
			if ( !empty($input_post) )
			{
				// Save new operations assignments
				if ( isset($input_post[$que_operation->name]) )
				{
					if ( !$am->hasItemChild($task_name, $que_operation->name) )
					{
						$am->addItemChild($task_name, $que_operation->name);
						if ($am instanceof CPhpAuthManager)
						{
							$am->save();
						}
						
						// Add new operation (child) to task (parent)
						$vec_assignments[] = $que_operation->name;
					}
				}
				
				// Revoke operation assignment
				if ( ( !isset($input_post[$que_operation->name]) || !in_array($que_operation->name, $vec_assignments) ) && $am->hasItemChild($task_name, $que_operation->name) )
				{
					// d("Eliminar '". $que_operation->name ."' para tarea '". $task_name ."'");
					$am->removeItemChild($task_name, $que_operation->name);
					if ($am instanceof CPhpAuthManager)
					{
						$am->save();
					}
					
					// Remove role (parent) from task (child)
					$vec_assignments = array_diff($vec_assignments, [$que_operation->name]);
				}
			}			
		}
		

		if ( !empty($input_post) )
		{
			Yii::app()->user->addFlash('success', 'Cambios guardados correctamente');
		}
		
		$this->render('task_view', [
			'task' => $am->getAuthItem($task_name),
			'operations' => $vec_operations,
			'assignments' => $vec_assignments
		]);
	}
}