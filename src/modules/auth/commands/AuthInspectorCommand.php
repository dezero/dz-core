<?php
/**
 * AuthInspectorCommand class file
 *
 * This component is used to check data consistency in auth module
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2018 Fabián Ruiz
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package commands
 */

namespace dz\modules\auth\commands;

use dz\console\CronCommand;
use Yii;

class AuthInspectorCommand extends CronCommand
{
	/**
	 * Remove duplicate rows in "auth" MySQL tables
	 *
	 * ./yiic authInspector removeDuplicates
	 */
	public function actionRemoveDuplicates()
	{
		// Table "user_auth_assignment"
		$vec_rows = Yii::app()->db->createCommand()
			->select()
			->from('user_auth_assignment')
			->queryAll();

		$vec_assignment = array();
		if ( !empty($vec_rows) )
		{
			foreach ( $vec_rows as $que_row )
			{
				if ( isset($vec_assignment[$que_row['itemname'] .'---'. $que_row['userid']]) )
				{
					Yii::app()->db->createCommand()->delete('user_auth_assignment', 'itemname=:name AND userid=:id', array(':name' => $que_row['itemname'], ':id' => $que_row['userid']));
					Yii::app()->db->createCommand()->insert('user_auth_assignment', array(
						'itemname' 	=> $que_row['itemname'],
						'userid'	=> $que_row['userid']
					));
				}
				$vec_assignment[$que_row['itemname'] .'---'. $que_row['userid']] = 1;
			}
		}

		// Table "user_auth_item"
		$vec_rows = Yii::app()->db->createCommand()
			->select()
			->from('user_auth_item')
			->queryAll();

		$vec_item_child = array();
		if ( !empty($vec_rows) )
		{
			foreach ( $vec_rows as $que_row )
			{
				if ( isset($vec_item_child[$que_row['name']]) )
				{
					Yii::app()->db->createCommand()->delete('user_auth_item', 'name=:name', array(':name' => $que_row['name']));
					Yii::app()->db->createCommand()->insert('user_auth_item', array(
						'name' 			=> $que_row['name'],
						'type'			=> $que_row['type'],
						'description'	=> $que_row['description'],
					));
				}
				$vec_item_child[$que_row['name']] = 1;
			}
		}


		// Table "user_auth_item_child"
		$vec_rows = Yii::app()->db->createCommand()
			->select()
			->from('user_auth_item_child')
			->queryAll();

		$vec_item_child = array();
		if ( !empty($vec_rows) )
		{
			foreach ( $vec_rows as $que_row )
			{
				if ( isset($vec_item_child[$que_row['parent'] .'---'. $que_row['child']]) )
				{
					Yii::app()->db->createCommand()->delete('user_auth_item_child', 'parent=:parent AND child=:child', array(':parent' => $que_row['parent'], ':child' => $que_row['child']));
					Yii::app()->db->createCommand()->insert('user_auth_item_child', array(
						'parent' 	=> $que_row['parent'],
						'child'		=> $que_row['child']
					));
				}
				$vec_item_child[$que_row['parent'] .'---'. $que_row['child']] = 1;
			}
		}
	}
}
