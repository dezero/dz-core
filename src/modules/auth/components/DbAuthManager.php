<?php
/**
 * CDbAuthManager represents an authorization manager that stores authorization information in database.
 */

namespace dz\modules\auth\components;

use Yii;

class DbAuthManager extends \CDbAuthManager
{
    const TYPE_ROLE = 'role';
    const TYPE_TASK = 'operation';
    const TYPE_OPERATION = 'operation';

    /**
     * Assigns an authorization item to a user.
     * 
     * @return CAuthAssignment the authorization assignment information.
     */
    public function assign($itemName, $userId, $bizRule = null, $data = null)
    {
        if( $this->usingSqlite() && $this->getAuthItem($itemName) === null )
        {
            throw new \CException(Yii::t('yii','The item "{name}" does not exist.',array('{name}'=>$itemName)));
        }

        $auth_item_row = $this->db->createCommand()
            ->select()
            ->from($this->itemTable)
            ->where('name=:name', array(':name' => $itemName))
            ->queryRow();

        $this->db->createCommand()
            ->insert($this->assignmentTable, [
                'itemname'  => $itemName,
                'item_type' => $auth_item_row['item_type'],
                'userid'    => $userId,
                'bizrule'   => $bizRule,
                'data'      => serialize($data),

            ]);
        return new \CAuthAssignment($this, $itemName, $userId, $bizRule, $data);
    }


    /**
     * Creates an authorization item.
     *
     * @return CAuthItem the authorization item
     */
    public function createAuthItem($name,$type,$description='',$bizRule=null,$data=null)
    {
        $this->db->createCommand()
            ->insert($this->itemTable, array(
                'name' => $name,
                'type' => $type,
                'item_type' => $this->getTypeLabel($type),
                'description' => $description,
                'bizrule' => $bizRule,
                'data' => serialize($data)
            ));
        return new \CAuthItem($this, $name, $type, $description, $bizRule, $data);
    }


    /**
     * Saves an authorization item to persistent storage.
     * @param CAuthItem $item the item to be saved.
     */
    public function saveAuthItem($item,$oldName=null)
    {
        if ( $this->usingSqlite() && $oldName !== null && $item->getName() !== $oldName )
        {
            $this->db->createCommand()
                ->update($this->itemChildTable, array(
                    'parent'=>$item->getName(),
                ), 'parent=:whereName', array(
                    ':whereName'=>$oldName,
                ));
            $this->db->createCommand()
                ->update($this->itemChildTable, array(
                    'child'=>$item->getName(),
                ), 'child=:whereName', array(
                    ':whereName'=>$oldName,
                ));
            $this->db->createCommand()
                ->update($this->assignmentTable, array(
                    'itemname'=>$item->getName(),
                ), 'itemname=:whereName', array(
                    ':whereName'=>$oldName,
                ));
        }

        $this->db->createCommand()
            ->update($this->itemTable, array(
                'name' => $item->getName(),
                'type' => $item->getType(),
                'item_type' => $this->getTypeLabel($item->getType()),
                'description' => $item->getDescription(),
                'bizrule' => $item->getBizRule(),
                'data' => serialize($item->getData()),
            ), 'name=:whereName', array(
                ':whereName' => $oldName === null ? $item->getName() : $oldName,
            ));
    }


    /**
     * Get item type label
     */
    public function getTypeLabel($type)
    {
        switch ( $type )
        {
            case 2:
                return self::TYPE_ROLE;
                break;

            case 1:
                return self::TYPE_TASK;
                break;

            default:
                return self::TYPE_OPERATION;
                break;
        }
    }


    /**
     * Check if current action has public access (not logged in required)
     * 
     * By default, these rules will be applied:
     *  - Actions starting with "public". For example, a controller with an action called "actionPublicDownload"
     *  - Modules "public" and "frontend"
     *  - User modules specific actions: user/login, user/logout, user/activation, user/recovery
     */
    public function checkPublicAccess()
    {
        $vec_route = [
            'module'        => '',

            // Current controller
            'controller'    => strtolower(Yii::app()->controller->getId()),
            
            // Current action name
            'action'        => 'index'
        ];

        // Action
        if ( Yii::app()->controller->getAction() )
        {
            $vec_route['action'] = strtolower(Yii::app()->controller->getAction()->getId());
        }

        // Actions with public access? Actions starting with "public" have public access
        if ( preg_match("/^public/", $vec_route['action']) )
        {
            return true;
        }

        // Module with public access?
        if ( ($module = Yii::app()->controller->getModule()) !== null )
        {
            // Current module name
            $vec_route['module'] = strtolower($module->getId());

            // Public module has public access
            if ( $vec_route['module'] == 'public' || $vec_route['module'] == 'frontend' )
            {
                return true;
            }

            // User login
            if ( $vec_route['module'] == 'user' && ( in_array($vec_route['action'], ['login', 'logout', 'recovery', 'reset']) || $vec_route['controller'] === 'email' ) )
            {
                return true;
            }
        }

        // Not public access allowed
        return false;
    }
}