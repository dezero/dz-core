<?php
/**
 * AuthItemForm class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2012-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package auth.models
 */

namespace dz\modules\auth\models;

use Yii;

/**
 * Form model for updating an authorization item.
 */
class AuthItemForm extends \CFormModel
{
	/**
	 * @var string item name.
	 */
	public $name;
	/**
	 * @var string item description.
	 */
	public $description;
	/**
	 * @var string business rule associated with the item.
	 */
	public $bizrule;
	/**
	 * @var string additional data for the item.
	 */
	public $data;
	/**
	 * @var string the item type (0=operation, 1=task, 2=role).
	 */
	public $type;

	/**
	 * Returns the attribute labels.
	 * @return array attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'name' => Yii::t('app', 'System name'),
			'description' => Yii::t('app', 'Description'),
			'bizrule' => Yii::t('app', 'Business rule'),
			'data' => Yii::t('app', 'Data'),
			'type' => Yii::t('app', 'Type'),
		);
	}

	/**
	 * Returns the validation rules for attributes.
	 * @return array validation rules.
	 */
	public function rules()
	{
		return array(
			array('description, type', 'required'),
			array('name', 'required', 'on' => 'create'),
			array('name', 'length', 'max' => 64),
		);
	}
}
