<?php
/**
 * Migration class m190103_150000_auth_tables
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m190103_150000_auth_tables extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        $this->dropTableIfExists('user_auth_assignment', true);
        $this->dropTableIfExists('user_auth_item_child', true);
        $this->dropTableIfExists('user_auth_item', true);

        // Create "user_auth_item" table
        // -------------------------------------------------------------------------
        $this->createTable('user_auth_item', [
            'name' => $this->string(64)->notNull(),
            'type' => $this->tinyInteger(1)->unsigned()->notNull(),
            'item_type' => $this->enum('item_type', ['operation', 'task', 'role']),
            'description' => $this->string(),
            'bizrule' => $this->string(32),
            'data' => $this->string(32)->notNull()->defaultValue('N;'),
            'created_date' => $this->date()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        $this->addPrimaryKey(null, 'user_auth_item', 'name');
        $this->createIndex(null, 'user_auth_item', ['type'], false);
        $this->createIndex(null, 'user_auth_item', ['item_type'], false);
        $this->createIndex(null, 'user_auth_item', ['uuid'], false);


        // Create "user_auth_item_child" table
        // -------------------------------------------------------------------------
        $this->createTable('user_auth_item_child', [
            'parent' => $this->string(64)->notNull(),
            'child' => $this->string(64)->notNull(),
        ]);

        $this->addPrimaryKey(null, 'user_auth_item_child', ['parent', 'child']);
        $this->addForeignKey(null, 'user_auth_item_child', ['parent'], 'user_auth_item', ['name'], 'CASCADE', null);
        $this->addForeignKey(null, 'user_auth_item_child', ['child'], 'user_auth_item', ['name'], 'CASCADE', null);


        // Create "user_auth_assignment" table
        // -------------------------------------------------------------------------
        $this->createTable('user_auth_assignment', [
            'itemname' => $this->string(64)->notNull(),
            'item_type' => $this->enum('item_type', ['operation', 'task', 'role']),
            'userid' => $this->integer()->unsigned()->notNull(),
            'bizrule' => $this->string(32),
            'data' => $this->string(32)->notNull()->defaultValue('N;'),
        ]);

        $this->addPrimaryKey(null, 'user_auth_assignment', ['itemname', 'userid']);
        $this->addForeignKey(null, 'user_auth_assignment', ['itemname'], 'user_auth_item', ['name'], 'CASCADE', null);
        $this->addForeignKey(null, 'user_auth_assignment', ['userid'], 'user_users', ['id'], 'CASCADE', null);
        $this->createIndex(null, 'user_auth_assignment', ['userid', 'item_type'], false);


        // Insert default roles and permissions
        // -------------------------------------------------------------------------
        $this->insertMultiple('user_auth_item', [
            [
                'name'          => 'admin',
                'type'          => 2,
                'item_type'     => 'role',
                'description'   => 'Administrator',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'editor',
                'type'          => 2,
                'item_type'     => 'role',
                'description'   => 'Editor',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'user_manage',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Administer users',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'user.admin.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'User - Administer users',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
        ]);


        // Set auth items relationships
        $this->insert('user_auth_item_child', [
            'parent'    => 'user_manage',
            'child'     => 'user.admin.*'
        ]);
        $this->insert('user_auth_item_child', [
            'parent'    => 'admin',
            'child'     => 'user_manage'
        ]);

        // Finally assign role "admin" to user 1
        $this->insert('user_auth_assignment', [
            'itemname'  => 'admin',
            'item_type' => 'role',
            'userid'    => 1,
        ]);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

