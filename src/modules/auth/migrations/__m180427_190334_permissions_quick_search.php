<?php
/**
 * Migration class m180427_190334_permissions_quick_search
 *
 * @link http://www.dezero.es
 */

use dz\db\Migration;

class m180427_190334_permissions_quick_search extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create permission "search.search" to allow users use QUICK SEARCH
		$this->insert('user_auth_item', array(
			'name'			=> 'search.search',
			'type'			=> 0,
			'description'	=> 'Products - Products - Quick search',
			'bizrule'		=> NULL,
			'data'			=> 'N;'
		));

		// Insert 
		$this->insertMultiple('user_auth_item_child', array(
			array(
				'parent'	=> 'product_manage',
				'child'		=> 'search.search',
			),
			array(
				'parent'	=> 'product_edit',
				'child'		=> 'search.search',
			),
			array(
				'parent'	=> 'product_view',
				'child'		=> 'search.search',
			),
		));

		return TRUE;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return FALSE;
	}
}

