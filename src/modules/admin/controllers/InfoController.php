<?php
/*
|--------------------------------------------------------------------------
| Controller class to view system information
|--------------------------------------------------------------------------
*/

namespace dz\modules\admin\controllers;

use Dz;
use dz\helpers\App;
use dz\helpers\PhpInfo;
use dz\web\Controller;
use Yii;

class InfoController extends Controller
{ 
    /**
     * View PHP information
     */
    public function actionIndex()
    {
        $this->render('index', [
            // Application
            'app_version'       => App::appVersion(),
            'app_commit'        => App::appLastCommit(),
            'app_env'           => Dz::get_environment(),

            // Server
            'php_version'       => App::phpVersion(),
            'apache_version'    => App::apacheVersion(),
            'db_driver'         => App::dbDriver(),
            'image_driver'      => App::imageDriver(),
            'yii_version'       => App::yiiVersion(),
            'dz_version'        => App::dzVersion(),
            'dz_commit'         => App::dzLastCommit(),

            // PHP information
            'memory_limit'          => PhpInfo::memory_limit(),
            'max_execution_time'    => PhpInfo::max_execution_time(),
            'post_max_size'         => PhpInfo::post_max_size(),
            'upload_max_filesize'   => PhpInfo::upload_max_filesize(),
        ]);
    }
}