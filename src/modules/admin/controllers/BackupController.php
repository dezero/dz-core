<?php
/*
|--------------------------------------------------------------------------
| Controller class to make DB backups
|--------------------------------------------------------------------------
*/

namespace dz\modules\admin\controllers;

use dz\web\Controller;
use Yii;

class BackupController extends Controller
{
    /**
     * Index action for DB backup files
     */
    public function actionIndex()
    {
        if ( isset($_GET['delete']) )
        {
            Yii::app()->user->addFlash('success', Yii::t('app', 'Fichero base datos eliminado correctamente: <em>'. $_GET['delete'] .'</em>'));
            $this->redirect(['index']);
        }

        // Delete TEMP SQL and ZIP files
        Yii::app()->backupManager->delete_temp_files(['sql', 'zip']);

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }
        
        $this->render('index', [
            'vec_backup_files' => Yii::app()->backupManager->get_files('db'),
        ]);
    }


    /**
     * Performs a backup operation.
     */
    public function actionCreate()
    {
        $file_path = Yii::app()->db->backup(true);

        Yii::app()->user->addFlash('success', Yii::t('app', 'Backup de base datos generada correctamente en <em>'. $file_path .'</em>'));

        $this->redirect(['index']);
    }


    /**
     * Download a backup file
     */
    public function actionDownload($file)
    {
        $file_path = Yii::app()->path->backupPath('db') . DIRECTORY_SEPARATOR . $file;
        $que_file = Yii::app()->file->set($file_path);

        if ( $que_file && $que_file->getExists() )
        {
            // Copy SQL file to temp directory
            $destination_file_path = Yii::app()->path->get('temp') . DIRECTORY_SEPARATOR . $file;
            $destination_file = $que_file->copy($destination_file_path);

            // Download & delete
            if ( $destination_file && $destination_file->getExists() )
            {
                if ( ! $destination_file->download() )
                {
                    $this->showErrors(['No se puede descargar el fichero de backup <em>'. $file .'</em>']);
                }
            }
            else
            {
                $this->showErrors(['No se puede copiar el fichero de backup en el directorio temporal']);
            }
        }
        else
        {
            $this->showErrors(['No se encuentra ningún fichero de backup con nombre <em>'. $file .'</em>']);
        }

        $this->redirect(['index']);
    }


    /**
     * Delete a backup file
     */
    public function actionDelete($file)
    {
        // Delete action only allowed by POST requests
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $file_path = Yii::app()->path->backupPath('db') . DIRECTORY_SEPARATOR . $file;
            $que_file = Yii::app()->file->set($file_path);

            if ( $que_file && $que_file->getExists() )
            {
                if ( $que_file->delete() )
                {
                    if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
                    {
                        Yii::app()->user->addFlash('success', Yii::t('app', 'Backup eliminado correctamente') );
                    }
                    else
                    {
                        echo Yii::t('app', 'Backup eliminado correctamente');
                    }
                }
                else
                {
                    $this->showErrors(['No se ha podido eliminar el fichero de backup <em>'. $file .'</em>']);
                }
            }
            else
            {
                $this->showErrors(['No se encuentra ningún fichero de backup con nombre <em>'. $file .'</em>']);
            }
        }
        else
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }
    }
}