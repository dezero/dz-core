<?php
/*
|--------------------------------------------------------------------------
| Controller class to view logs
|--------------------------------------------------------------------------
*/

namespace dz\modules\admin\controllers;

use dz\helpers\Html;
use dz\helpers\StringHelper;
use dz\web\Controller;
use Dz;
use Yii;

class LogController extends Controller
{   
    /**
     * Index action for Log files
     */
    public function actionIndex()
    {
        // Get all files from log directory
        $logs_path = Yii::app()->path->logPath();
        $logs_directory = Yii::app()->file->set($logs_path);
        $vec_files = $logs_directory->getContents();
        
        // Filter log files
        $vec_log_files = [];
        if ( !empty($vec_files) )
        {
            foreach ( $vec_files as $que_file_path )
            {
                // Accept only LOG files
                $que_filename = trim($que_file_path);
                $que_filename = str_replace($logs_path . DIRECTORY_SEPARATOR, '', $que_filename);
                $que_filename = str_replace($logs_path, '', $que_filename);
                
                if ( preg_match("/\.log/", $que_filename) AND !preg_match("/^\./", $que_filename) )
                {
                    $que_file = Yii::app()->file->set($que_file_path);
                    if ( $que_file AND $que_file->getExists() )
                    {
                        $modified_time = $que_file->getTimeModified();
                        if ( isset($vec_log_files[$modified_time]) )
                        {
                            while ( isset($vec_log_files[$modified_time]) )
                            {
                                $modified_time++;
                            }
                        }
                        $vec_log_files[$modified_time] = $que_file;
                    }
                }
            }

            if ( !empty($vec_log_files) )
            {
                krsort($vec_log_files);
            }
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }
        
        $this->render('index', [
            'vec_log_files' => $vec_log_files,
        ]);
    }


    /**
     * View log files
     */
    public function actionView($file)
    {
        // Check if log file exists
        $log_file_path = Dz::logPath() . DIRECTORY_SEPARATOR . $file;
        $log_file = Yii::app()->file->set($log_file_path);
        if ( $log_file AND $log_file->getExists() )
        {
            // Parse log file
            $content_log = nl2br($log_file->getContents());
            $content_log = str_replace("\t", "&nbsp;", $content_log);
            // $content_log = StringHelper::clean_text($content_log);
            $content_log = StringHelper::remove_invisible_characters($content_log);
            $encoded_log = Html::encode($content_log);
            if ( !empty($encoded_log) )
            {
                $content_log = $encoded_log;
            }
            $content_log = str_replace("&lt;br /&gt;", "<br />", $content_log);

            $this->render('view', [
                'log_file'      => $log_file,
                'content_log'   => $content_log
            ]);
        }
        else
        {
            throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }
    }
}