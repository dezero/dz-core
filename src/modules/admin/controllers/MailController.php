<?php
/*
|--------------------------------------------------------------------------
| Controller class for MailHistory model
|--------------------------------------------------------------------------
*/

namespace dz\modules\admin\controllers;

use dz\helpers\Html;
use dz\modules\settings\models\MailHistory;
use dz\modules\settings\models\MailTemplate;
use dz\web\Controller;
use user\models\User;
use Yii;

class MailController extends Controller
{   
    /**
     * Sent emails list
     */
    public function actionIndex()
    {
        $model = Yii::createObject(MailHistory::class, 'search');
        $model->unsetAttributes();
        $model->status_type = 'sent';
        
        if ( isset($_GET['MailHistory']) )
        {
            $model->setAttributes($_GET['MailHistory']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }
        
        // Render page
        $this->render('index', [
            'model'                 => $model,
            'vec_template_filter'   => MailTemplate::model()->mailtemplate_list()
        ]);
    }


    /**
     * Pending email list
     */
    public function actionPending()
    {
        $model = Yii::createObject(MailHistory::class, 'search');
        $model->unsetAttributes();
        $model->status_type = 'pending';
        
        if ( isset($_GET['MailHistory']) )
        {
            $model->setAttributes($_GET['MailHistory']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }
        
        // Render page
        $this->render('index', [
            'model'                 => $model,
            'vec_template_filter'   => MailTemplate::model()->mailtemplate_list()
        ]);
    }


    /**
     * Scheduled email list
     */
    public function actionScheduled()
    {
        $model = Yii::createObject(MailHistory::class, 'search');
        $model->unsetAttributes();
        $model->status_type = 'scheduled';
        
        if ( isset($_GET['MailHistory']) )
        {
            $model->setAttributes($_GET['MailHistory']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }
        
        // Render page
        $this->render('index', [
            'model'                 => $model,
            'vec_template_filter'   => MailTemplate::model()->mailtemplate_list()
        ]);
    }


    /**
     * View action for MailHistory model
     */
    public function actionView($id)
    {
        $mail_history_model = $this->loadModel($id, MailHistory::class);

        // Send the email
        if ( isset($_POST['MailHistory']) && isset($_POST['MailHistory']['history_id']) && $_POST['MailHistory']['history_id'] === $mail_history_model->history_id )
        {
            $result_sending = $mail_history_model->send_email(true);
            if ( $result_sending )
            {
                Yii::app()->user->addFlash('success', Yii::t('app', 'Email has been sent successfully to') .' <em>'. $mail_history_model->recipient_emails .'</em>');
            }
            else
            {
                Yii::app()->user->addFlash('error', Yii::t('app', 'Email could not be sent') );
            }

            $this->redirect(['view', 'id' => $id]);
        }

        // Render
        $this->render('view', [
            'mail_history_model'    => $mail_history_model,
        ]);
    }


    /**
     * Send email action
     */
    public function actionSend($user_id)
    {
        // User model
        $user_model = $this->loadModel($user_id, User::class);

        // MailTemplate model
        $mail_template_model = null;
        if ( isset($_GET['template_id']) )
        {
            $mail_template_model = MailTemplate::model()->findByAlias($_GET['template_id']);
            if ( ! $mail_template_model )
            {
                throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
            }
        }

        // Submit form?
        if ( isset($_POST['MailHistory']) )
        {
            // Mail template validation
            $mail_template_model = null;
            if ( isset($_POST['MailHistory']['template_id']) )
            {
                // Mail template selected
                if ( empty($_POST['MailHistory']['template_id']) || $_POST['MailHistory']['template_id'] === '_free_' )
                {
                    $mail_template_model = Yii::app()->mail->get_free_template();
                }
                else
                {
                    $mail_template_model = MailTemplate::findOne($_POST['MailHistory']['template_id']);
                }

                if ( ! $mail_template_model )
                {
                    $mail_template_model->addError('Email template does not exist');
                }
                
                // Subject & body submitted
                else if ( isset($_POST['MailTemplate']['subject']) && isset($_POST['MailTemplate']['body']) )
                {
                    $mail_template_model->subject = $_POST['MailTemplate']['subject'];
                    $mail_template_model->body = $_POST['MailTemplate']['body'];

                    // Entity information?
                    $vec_entity_info = [];
                    if ( isset($_POST['MailHistory']['entity_type']) && isset($_POST['MailHistory']['entity_id']) )
                    {
                        $vec_entity_info = [
                            'entity_type'   => $_POST['MailHistory']['entity_type'],
                            'entity_id'     => $_POST['MailHistory']['entity_id'],
                        ];
                    }

                    $sending_result = $user_model->send_email($mail_template_model, null, $vec_entity_info);
                    if ( $sending_result )
                    {
                        $success_email_message = Yii::t('app', 'Email <em>{template}</em> has been sent to {email}', [
                            '{template}' => $mail_template_model->alias,
                            '{email}' => $user_model->get_email()
                        ]);
                        Yii::app()->user->addFlash('success', $success_email_message);

                        // Redirect to SENT emails page
                        $this->redirect(['/admin/mail']);
                    }
                    else
                    {
                        $error_email_message = Yii::t('app', 'Email <em>{template}</em> could not been sent to {email}', [
                            '{template}' => $mail_template_model->alias,
                            '{email}' => $user_model->get_email()
                        ]);
                        Yii::app()->user->addFlash('error', $error_email_message);

                        // Redirect to same page
                        $this->redirect(['/admin/mail/send', 'user_id' => $user_model->id]);
                    }
                }
            }
        }

        // MailHistory model
        $mail_history_model = Yii::createObject(MailHistory::class);
        $mail_history_model->unsetAttributes();
        $mail_history_model->entity_type = 'User';
        $mail_history_model->entity_id = $user_id;
        $mail_history_model->template_id = $mail_template_model ? $mail_template_model->template_id : null;

        // Render page
        $this->render('send', [
            'user_model'            => $user_model,
            'mail_template_model'   => $mail_template_model,
            'mail_history_model'    => $mail_history_model,
            'vec_template_list'     => ['_free_' => '--- Free email ---'] + MailTemplate::model()->mailtemplate_list()
        ]);
    }



    /**
     * Delete action for MailHistory model
     */
    public function actionDelete($id)
    {
        // Delete action only allowed by POST requests
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $model = $this->loadModel($id, MailHistory::class);
            if ( $model->delete() )
            {
                if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
                {
                    Yii::app()->user->addFlash('success', 'Email removed successfully');
                }
                else
                {
                    echo Yii::t('app', 'Email removed successfully');
                }
            }
            else
            {
                $this->showErrors($model->getErrors());
            }

            if ( !Yii::app()->getRequest()->getIsAjaxRequest() )
            {
                $this->redirect(['index']);
            }
        }
        else
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index'     => 'admin.mail.view',
            'pending'   => 'admin.mail.view',
            'scheduled' => 'admin.mail.view',
            'send'      => 'admin.mail.view',
            'delete'    => 'admin.mail.update',
        ];
    }
}
