<?php
/*
|--------------------------------------------------------------------------
| LogApi controller class
|--------------------------------------------------------------------------
*/

namespace dz\modules\admin\controllers;

use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\api\models\LogApi;
use dz\web\Controller;
use user\models\User;
use Yii;

class ApiLogController extends Controller
{
    /**
     * Main action
     */
    public function actionIndex()
    {
        // LogApi model
        $log_api_model = Yii::createObject(LogApi::class, 'search');
        $log_api_model->unsetAttributes();
        
        // Filters?
        if ( isset($_GET['LogApi']) )
        {
            $log_api_model->setAttributes($_GET['LogApi']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }

        // Render index page
        $this->render('//admin/apiLog/index', [
            'log_api_model'     => $log_api_model
        ]);
    }


    /**
     * View action for LogApi model
     */
    public function actionView($id)
    {
        $log_api_model = $this->loadModel($id, LogApi::class);

        // Render view page
        $this->render('//admin/apiLog/view', [
            'log_api_model'    => $log_api_model,
        ]);
    }
}