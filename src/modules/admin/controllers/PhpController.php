<?php
/*
|--------------------------------------------------------------------------
| Controller class to view PHP Information
|--------------------------------------------------------------------------
*/

namespace dz\modules\admin\controllers;

use dz\helpers\PhpInfo;
use dz\web\Controller;
use Yii;

class PhpController extends Controller
{ 
    /**
     * View PHP information
     */
    public function actionIndex()
    {
        // Get PHP Information
        $vec_php_info = PhpInfo::info();

        $this->render('index', [
            'vec_php_info'      => $vec_php_info,
        ]);
    }
}