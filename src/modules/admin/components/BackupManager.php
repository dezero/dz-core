<?php
/**
 * Component class for working with backups
 */

namespace dz\modules\admin\components;

use dz\base\ApplicationComponent;
use Yii;

class BackupManager extends ApplicationComponent
{
    /**
     * Get all backup files from backup directory
     */
    public function get_files($backups_directory = 'db')
    {
        $backups_path = Yii::app()->path->backupPath($backups_directory);
        $backups_directory = Yii::app()->file->set($backups_path);
        $vec_files = $backups_directory->getContents();
        
        // Filter backup files
        $vec_backup_files = [];
        if ( !empty($vec_files) )
        {
            foreach ( $vec_files as $que_file_path )
            {
                // Accept only SQL files
                $que_filename = trim($que_file_path);
                $que_filename = str_replace($backups_path . DIRECTORY_SEPARATOR, '', $que_filename);
                $que_filename = str_replace($backups_path, '', $que_filename);
                
                if ( preg_match("/\.sql/", $que_filename) AND !preg_match("/^\./", $que_filename) )
                {
                    $que_file = Yii::app()->file->set($que_file_path);
                    if ( $que_file AND $que_file->getExists() )
                    {
                        $modified_time = $que_file->getTimeModified();
                        if ( isset($vec_backup_files[$modified_time]) )
                        {
                            while ( isset($vec_backup_files[$modified_time]) )
                            {
                                $modified_time++;
                            }
                        }
                        $vec_backup_files[$modified_time] = $que_file;
                    }
                }
            }

            if ( !empty($vec_backup_files) )
            {
                krsort($vec_backup_files);
            }
        }

        return $vec_backup_files;
    }

    /**
     * Delete files filtered by extension on temp folder
     */
    public function delete_temp_files($vec_extensions = ['sql', 'zip'])
    {
        // Delete all SQL files in TEMP
        $temp_path = Yii::app()->path->get('temp');
        $temp_directory = Yii::app()->file->set($temp_path);
        $vec_files = $temp_directory->getContents();
        if ( !empty($vec_files) )
        {
            foreach ( $vec_files as $que_file_path )
            {
                // Accept only SQL files
                $que_filename = trim($que_file_path);
                $que_filename = str_replace($temp_path . DIRECTORY_SEPARATOR, '', $que_filename);
                $que_filename = str_replace($temp_path, '', $que_filename);
                
                // Create regexp pattern. Example "/\.sql|\.zip/"
                $pattern = "";
                foreach ( $vec_extensions as $num_extension => $que_extension )
                {
                    if ( $num_extension > 0 )
                    {
                        $pattern .= "|";
                    }
                    $pattern .= "\.". $que_extension;
                }
                    
                if ( !empty($pattern) && preg_match("/". $pattern ."/", $que_filename) && !preg_match("/^\./", $que_filename) )
                {
                    $que_file = Yii::app()->file->set($que_file_path);
                    if ( $que_file AND $que_file->getExists() )
                    {
                        $que_file->delete();
                    }
                }
            }
        }
    }
}