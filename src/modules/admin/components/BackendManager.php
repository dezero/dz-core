<?php
/**
 * Backend Manager
 * 
 * Helper classes collection for backend theme
 */

namespace dz\modules\admin\components;

use dz\base\ApplicationComponent;
use dz\helpers\StringHelper;
use Yii;

class BackendManager extends ApplicationComponent
{
    /**
     * @var bool. Unify all CSS files?
     */
    public $is_unify_css = true;


    /**
     * @var bool. Unify all Javascript files?
     */
    public $is_unify_js = true;


    /**
     * @var string. Current controller name
     */
    public $current_controller;


    /**
     * @var string. Current action name
     */
    public $current_action;


    /**
     * @var string. Current module name
     */
    public $current_module;


    /**
     * Init function. Required!
     */
    public function init()
    {
        parent::init();
        
        if ( !empty(Yii::app()->theme) && Yii::app()->theme->name == 'backend' )
        {
            $this->current_controller = Yii::currentController(true);
            $this->current_action = Yii::currentAction(true);
            $this->current_module = Yii::currentModule(true);
        }
    }


    /**
     * Return params used on main layout and partials "_header.tpl.php" and "_footer.tpl.php"
     */
    public function get_params()
    {
        return [
            'current_module'      => $this->current_module,
            'current_controller'  => $this->current_controller,
            'current_action'      => $this->current_action,
            'language_id'         => Yii::app()->language,
            'is_logged_in'        => ( Yii::app()->user->id > 0 ),
            'path_info'           => Yii::app()->getRequest()->getPathInfo(),
        ];
    }


    /**
     * Get body classes
     */
    public function body_classes()
    {
        $vec_classes = [
            // Current action
            $this->current_action .'-page',

            // Current controller and action
            $this->current_controller .'-'. $this->current_action .'-page',

            // Logged in?
            ( Yii::app()->user->isGuest ? 'not-logged-in' : 'logged-in')
        ];

        // Login & password page (different layout)
        if ( Yii::app()->user->isGuest || ($this->current_module == 'user' && $this->current_controller == 'password') )
        {
            $vec_classes[] = 'page-login';
            $vec_classes[] = 'layout-full';
        }

        // Logged-in pages -> Enable main menu
        else
        {
            $vec_classes[] = 'site-navbar-small';

            // Left sidebar column
            if ( $this->current_module == 'admin' && ( $this->current_controller == 'mail' && $this->current_action !== 'view' ) )
            {
                $vec_classes[] = 'page-aside-static';
                $vec_classes[] = 'page-aside-left';
            }
        }

        return $vec_classes;
    }
}