<?php
/**
 * MailCommand class file
 *
 * This command is used to send emails
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2021 Fabián Ruiz
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package commands
 */

namespace dz\modules\admin\commands;

use dz\console\CronCommand;
use dz\db\DbCriteria;
use dz\helpers\Log;
use dz\modules\settings\models\MailHistory;
use Yii;

class MailCommand extends CronCommand
{
    /**
     * Check and send emails mark as "pending" or "scheduled"
     *
     * ./yiic mail pending
     * ./yiic mail pending --limit=30
     */
    public function actionPending($limit = 50)
    {
        // #1 - Send PENDING emails
        $vec_mail_history_models = MailHistory::get()
            ->where(['status_type' => 'pending'])
            ->orderBy('scheduled_date ASC, history_id ASC')
            ->limit($limit)
            ->all();

        $total_sendings = 0;
        $total_success = 0;
        $total_errors = 0;
        $log_message = '';

        if ( !empty($vec_mail_history_models) )
        {
            $log_message = "Detected ". count($vec_mail_history_models) ." mails PENDING to send...\n";
            foreach ( $vec_mail_history_models as $mail_history_model )
            {
                $sending_result = $mail_history_model->send_email();
                $total_sendings++;

                if ( $sending_result )
                {
                    $log_message .= " - Mail send OK to ". $mail_history_model->recipient_emails ." (". $mail_history_model->template_alias .")\n";
                    $total_success++;
                }
                else
                {
                    $log_message .= " - ERROR to send an email to ". $mail_history_model->recipient_emails ." (". $mail_history_model->template_alias .")\n";
                    $total_errors++;
                }
            }
        }

        // #2 - Send SCHEDULED emails
        $limit = $limit - $total_sendings;
        if ( $limit > 0 )
        {
            $now = time();
            $vec_mail_history_models = MailHistory::get()
                ->where(['status_type' => 'scheduled'])
                ->andWhere('scheduled_date <= '. $now)
                ->orderBy('scheduled_date ASC, history_id ASC')
                ->limit($limit)
                ->all();

            if ( !empty($vec_mail_history_models) )
            {
                $log_message = "\n\nDetected ". count($vec_mail_history_models) ." mails SCHEDULED to be send now...\n";
                foreach ( $vec_mail_history_models as $mail_history_model )
                {
                    $sending_result = $mail_history_model->send_email();
                    $total_sendings++;

                    if ( $sending_result )
                    {
                        $log_message .= " - Mail send OK to ". $mail_history_model->recipient_emails ." (". $mail_history_model->template_alias .") - Scheduled date ". $mail_history_model->scheduled_date ." \n";
                        $total_success++;
                    }
                    else
                    {
                        $log_message .= " - ERROR to send an email to ". $mail_history_model->recipient_emails ." (". $mail_history_model->template_alias .") - Scheduled date ". $mail_history_model->scheduled_date ."\n";
                        $total_errors++;
                    }
                }
            }
        }
        
        // Final results
        if ( !empty($log_message) )
        {
            $log_message .= "\n---------------\n";
            $log_message .= "TOTAL SENT EMAILS: ". $total_success ."\n";
            $log_message .= "TOTAL ERRORS: ". $total_errors ."\n";

            // Save results to log
            Log::mail_cron($log_message);
        }
    }
}
