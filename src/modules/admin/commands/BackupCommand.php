<?php
/**
 * BackupCommand class file
 *
 * Performs a backup operation
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2019 Fabián Ruiz
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package commands
 */

namespace dz\modules\admin\commands;

use dz\console\CronCommand;
use Yii;

class BackupCommand extends CronCommand
{
    /**
     * Performs a database backup operation and compress it with zip
     *
     * ./yiic backup db
     */
    public function actionDb()
    {
         $file_path = Yii::app()->db->backup(true);
         if ( !empty($file_path) )
         {
            echo 'Database backup performed successfully at '. $file_path ;
         }
         else
         {
            echo 'Database backup cannot be performed';
         }
    }
}
