/**
 * Scripts for ADMIN module
 */
(function(document, window, $) {
  $(document).ready(function() {

    // Resend an email (from MailHistory)
    $('#mail-history-send-btn').on('click', function(e){
      e.preventDefault();

      var msg_alert = '<h3>Are you sure you want to RESEND this email to <em>'+ $(this).data('recipient') +'</em>?</h3>';
      bootbox.confirm(
        msg_alert,
        function(confirmed){
          if ( confirmed ) {
            $('#mail-history-send-form').submit();
          }
        }
      );
    });

    // Send an email
    $('#mail-send-form').find('.btn-back').on('click', function(e){
      e.preventDefault();
      var $this = $(this);
      var msg_alert = '<h3>Are you sure you want to GO BACK?</h3>';
      msg_alert += '<p><u>WARNING</u>: the data you have entered will be lost.</p>';
      bootbox.confirm(
        msg_alert,
        function(confirmed){
          if ( confirmed ) {
            window.location.href = $this.attr('href');
          }
        }
      );
    });
  });
})(document, window, jQuery);