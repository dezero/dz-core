<?php
/**
 * Migration class m200525_160013_mail_permissions
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200525_160013_mail_permissions extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Permissions
        // -------------------------------------------------------------------------
        $this->insertMultiple('user_auth_item', [
            [
                'name'          => 'admin.mail.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Administration - Sent Emails - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'admin.mail.view',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Administration - Sent Emails - View sent email',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'sent_email_manage',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Administration - Full access to sent emails',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'sent_email_view',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Administration - View sent emails',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
        ]);

        // Set items relationships
        $this->insertMultiple('user_auth_item_child', [
            [
                'parent'    => 'sent_email_manage',
                'child'     => 'admin.mail.*'
            ],
            [
                'parent'    => 'sent_email_view',
                'child'     => 'admin.mail.view'
            ],
            [
                'parent'    => 'admin',
                'child'     => 'sent_email_manage'
            ],
        ]);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

