<?php
/**
 * Module for administration purposes
 */

namespace dz\modules\admin;

class Module extends \dz\web\Module
{
    /**
     * @var array mapping from controller ID to controller configurations.
     */
    public $controllerMap = [
        'php' => [
            'class' => 'dz\modules\admin\controllers\PhpController',
        ],
        'info' => [
            'class' => 'dz\modules\admin\controllers\InfoController',
        ],
        'log' => [
            'class' => 'dz\modules\admin\controllers\LogController',
        ],
        'backup' => [
            'class' => 'dz\modules\admin\controllers\BackupController',
        ],
        'mail' => [
            'class' => 'dz\modules\admin\controllers\MailController',
        ],
        'apiLog' => [
            'class' => 'dz\modules\admin\controllers\ApiLogController',
        ],
    ];

    /**
     * Default controller
     */
    public $defaultController = 'log';


    /**
     * Load specific CSS or JS files for this module
     */
    public $cssFiles = null; // ['admin.css'];
    public $jsFiles = null; // ['admin.js'];


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        parent::init();
    }
}
