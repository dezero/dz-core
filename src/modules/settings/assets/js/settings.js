/**
 * Scripts for SETTINGS module
 */
(function(document, window, $) {

  $(document).ready(function() {

    // LANGUAGES
    // ---------------------------------------------------------

    // Show all currencies
    $('#all-currencies-link').on('click', function(e){
      e.preventDefault();
      $('#famous-currencies-wrapper').removeClass('hide').addClass('hide');
      $('#all-currencies-wrapper').removeClass('hide');
    });

    // Show just famouse currencies
    $('#famous-currencies-link').on('click', function(e){
      e.preventDefault();
      $('#all-currencies-wrapper').removeClass('hide').addClass('hide');
      $('#famous-currencies-wrapper').removeClass('hide');
    });

    // TRANSLATIONS
    // ---------------------------------------------------------

    // Add translation
    $('#add-text-btn').on('click', function(e){
      e.preventDefault();
      $('#row-new-'+ $(this).data('id')).removeClass('hide');
      $(this).data('id', $(this).data('id')+1);
    });

    $('#translation-table').children('tbody').children('tr').children('.remove-col').children('a').on('click', function(e){
      e.preventDefault();
      var $parent_row = $(this).parent().parent();
      $parent_row.find('input').val('');
      $parent_row.addClass('hide');
    });

    $('#translation-categories').on('change', function(e){
      e.preventDefault();
      $(this).prop('disabled', true);
      $('#category-destination').val($(this).val());
      $('#translation-form').submit();
      // window.location.href = js_globals.baseUrl + '/settings/translation?category='+ $(this).val();
    });

    // MAIL TEMPLATES
    // ---------------------------------------------------------

    // PredefinedDocument form
    if ( $('#mail-form').size() > 0 ) {
      $.markdownLanguage('.mail-form-wrapper', 'TranslatedMail', 'body');
    }
  });

})(document, window, jQuery);