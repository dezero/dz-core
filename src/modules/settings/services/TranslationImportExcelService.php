<?php
/*
|--------------------------------------------------------------------------
| Service for import Excel file to upload translations
|--------------------------------------------------------------------------
*/

namespace dz\modules\settings\services;

use dz\contracts\ServiceInterface;
use dz\helpers\Log;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dz\modules\asset\models\AssetFile;
use dz\modules\settings\models\Language;
use dz\modules\settings\models\TranslationImportExcel;
use dz\traits\ErrorTrait;
use user\models\User;
use Yii;

class TranslationImportExcelService implements ServiceInterface
{
    use ErrorTrait;


    /**
     * Constructor
     */
    public function __construct(Language $language_model, AssetFile $upload_file_model, TranslationImportExcel $batch_import_model)
    {
        $this->language_model = $language_model;
        $this->upload_file_model = $upload_file_model;
        $this->batch_import_model = $batch_import_model;
    }


    /**
     * @return bool
     */
    public function run()
    {
        // Language model needed for batch import process
        $this->batch_import_model->set_language($this->language_model);

        // Upload file
        if ( ! $this->upload_file() )
        {
            $this->add_error($this->batch_import_model->get_file_errors());

            return false;
        }

        // Save --> Import all the rows from the Excel file
        if ( ! $this->batch_import_model->save() )
        {
            $this->add_error($this->batch_import_model->getErrors());

            return false;
        }

        return true;
    }


    /**
     * Upload Excel file
     */
    private function upload_file()
    {
        // Upload file
        $this->upload_file_model->setAttributes([
            'entity_type'   => 'ImportExcel'
        ]);

        // Try to save imported file
        $file_destination_path = $this->batch_import_model->get_import_path() . DIRECTORY_SEPARATOR;
        $this->batch_import_model->file = $this->batch_import_model->upload_file('file_id', $file_destination_path);
        if ( ! $this->batch_import_model->file )
        {
            return false;
        }

        return true;
    }
}
