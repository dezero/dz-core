<?php
/*
|--------------------------------------------------------------------------
| Service for exporting translation messages to an Excel file
|--------------------------------------------------------------------------
*/

namespace dz\modules\settings\services;

use dz\batch\ExportExcel;
use dz\contracts\ServiceInterface;
use dz\helpers\Log;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dz\modules\settings\models\Language;
use dz\modules\settings\models\TranslationSource;
use dz\traits\ErrorTrait;
use user\models\User;
use Yii;

class TranslationExportExcelService implements ServiceInterface
{
    use ErrorTrait;

    /**
     * @var array. Excel sheets
     */
    private $vec_excel_sheets;

    /**
     * Constructor
     */
    public function __construct(Language $language_model, string $translation_category)
    {
        $this->language_model = $language_model;
        $this->translation_category = $translation_category;
        $this->vec_excel_sheets = [];
    }


    /**
     * @return bool
     */
    public function run()
    {
        // Validate language
        if ( ! $this->validate_language() )
        {
            return false;
        }

        // Validate translation category
        if ( ! $this->validate_translation_category() )
        {
            return false;
        }

        // Prepare export to an Excel file
        if ( ! $this->prepare_excel() )
        {
            return false;
        }

        return true;
    }


    /**
     * Return Excel sheets
     */
    public function get_excel_sheets()
    {
        return $this->vec_excel_sheets;
    }


    /**
     * Check if Language is enabled and is not the default language
     */
    private function validate_language()
    {
        // Check if email address is valid
        if ( $this->language_model->is_disabled() )
        {
            $this->add_error(Yii::t('backend', 'The language :language is disabled', [':language' => $this->language_model->title()]));

            return false;
        }

        // Check if language is NOT de default language
        if ( $this->language_model->language_id === Yii::app()->i18n->get_default_language() )
        {
            $this->add_error(Yii::t('backend', 'The exported language :language cannot be the default language', [':language' => $this->language_model->title()]));

            return false;
        }

        return true;
    }


    /**
     * Check if translation category exists
     */
    private function validate_translation_category()
    {
        $vec_translation_categories = Yii::app()->i18n->get_translation_categories();
        if ( !isset($vec_translation_categories[$this->translation_category]) )
        {
            $this->add_error(Yii::t('backend', 'The translation category :translation_category does not exist', [':translation_category' => $this->translation_category]));

            return false;
        }

        return true;
    }


    /**
     * Prepare export to an Excel file
     */
    private function prepare_excel()
    {
        // TranslationSource model
        $translation_source_model = Yii::createObject(TranslationSource::class, 'search');
        $translation_source_model->unsetAttributes();
        $translation_source_model->category = $this->translation_category;

        // Excel sheet
        $this->vec_excel_sheets = [
            [
                'title'     => 'Translations',
                'data'      => $translation_source_model->search('excel'),
                'columns'   => $this->get_excel_columns($translation_source_model)
            ]
        ];

        return true;
    }


    /**
     * Get translation columns for Excel exported file
     */
    private function get_excel_columns()
    {
        return [
            [
                'header' => 'Default language message',
                'name' => 'message',
            ],
            [
                'header' => 'Translated message',
                'value' => 'trim($this->getOwner()->renderPartial("//settings/translationExport/_excel_column", ["column" => "translation", "language_id" => "'. $this->language_model->language_id .'", "model" => $data], true))',
            ],
            [
                'header' => 'Category (optional)',
                'name' => 'category',
            ],
        ];
    }
}
