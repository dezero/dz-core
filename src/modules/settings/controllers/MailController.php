<?php
/*
|--------------------------------------------------------------------------
| Controller class for MailTemplate model
|--------------------------------------------------------------------------
*/

namespace dz\modules\settings\controllers;

use dz\helpers\Html;
use dz\helpers\Log;
use dz\modules\settings\models\MailTemplate;
use dz\web\Controller;
use Yii;

class MailController extends Controller
{   
    /**
     * List action for MailTemplate models
     */
    public function actionIndex()
    {
        $model = Yii::createObject(MailTemplate::class, 'search');
        $model->unsetAttributes();
        $model->language_id = Yii::app()->language;
        
        if ( isset($_GET['MailTemplate']) )
        {
            $model->setAttributes($_GET['MailTemplate']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }
        
        // Render page
        $this->render('index', [
            'model' => $model,
        ]);
    }


    /**
     * Create action for MailTemplate model
     */
    public function actionCreate()
    {
        // MailTemplate model
        $mail_model = Yii::createObject(MailTemplate::class);
        $mail_model->language_id = Yii::app()->language;

        // Mail configuration settings
        $vec_mail_config = Yii::app()->mail->getConfig();

        // Insert new model?
        if ( isset($_POST['MailTemplate']) )
        {
            $mail_model->setAttributes($_POST['MailTemplate']);

            if ( $mail_model->save() )
            {
                Yii::app()->user->addFlash('success', 'New mail template created successfully');

                // Redirect to update page
                $this->redirect(['update', 'id' => $mail_model->template_id]);
            }
        }
        else
        {
            $mail_model->sender_name = $vec_mail_config['FromName'];
            $mail_model->sender_email = $vec_mail_config['From'];
        }

        $this->render('create', [
            'model'                 => $mail_model,
            'vec_template_files'    => $mail_model->get_template_files(),
            'vec_mail_config'       => $vec_mail_config,
            'form_id'               => 'mail-form'
        ]);
    }


    /**
     * Update action for MailTemplate model
     */
    public function actionUpdate($id)
    {
        // MailTemplate model
        $mail_model = $this->loadModel($id, MailTemplate::class);

        // Mail configuration settings
        $vec_mail_config = Yii::app()->mail->getConfig();

        // Default language
        $default_language = Yii::defaultLanguage();

        // Translated messages
        $vec_translated_models = [];
        $vec_extra_languages = Yii::app()->i18n->get_extra_languages();
        if ( !empty($vec_extra_languages) )
        {
            foreach ( $vec_extra_languages as $language_id )
            {
                $translated_mail_model = MailTemplate::get()
                    ->where([
                        'alias'         => $mail_model->alias,
                        'language_id'   => $language_id,
                    ])
                    ->one();
                    
                if ( ! $translated_mail_model )
                {
                    // Get attributes from base language
                    $vec_attributes = $mail_model->raw_attributes;
                    unset($vec_attributes['template_id']);
                    unset($vec_attributes['created_date']);
                    unset($vec_attributes['created_uid']);
                    unset($vec_attributes['updated_date']);
                    unset($vec_attributes['updated_uid']);
                    unset($vec_attributes['uuid']);

                    $translated_mail_model = Yii::createObject(MailTemplate::class);
                    $translated_mail_model->setAttributes($mail_model->raw_attributes);
                    $translated_mail_model->language_id = $language_id;
                    
                    // Attributes with different values from original
                    $translated_mail_model->subject = null;
                    $translated_mail_model->body = null;
                    $translated_mail_model->created_date = null;
                    $translated_mail_model->created_uid = null;
                    $translated_mail_model->updated_date = null;
                    $translated_mail_model->updated_uid = null;
                    
                }
                $vec_translated_models[$language_id] = $translated_mail_model;
            }
        }

        // Insert new model?
        if ( isset($_POST['MailTemplate']) )
        {
            $mail_model->setAttributes($_POST['MailTemplate']);

            if ( $mail_model->save() )
            {
                $is_save_error = false;

                // Save translations
                if ( !empty($vec_translated_models) && isset($_POST['TranslatedMail']) )
                {
                    foreach ( $_POST['TranslatedMail'] as $language_id => $vec_translated_attributes )
                    {
                        if ( isset($vec_translated_models[$language_id]) && isset($vec_translated_attributes['body']) && !empty($vec_translated_attributes['body']) )
                        {
                            $translated_mail_model = $vec_translated_models[$language_id];
                            $vec_translated_attributes['language_id'] = $language_id;
                            $vec_translated_attributes['alias'] = $mail_model->alias;
                            $translated_mail_model->setAttributes($vec_translated_attributes);
                            if ( ! $translated_mail_model->save_translation_model($vec_translated_models[$language_id]) )
                            {
                                $is_save_error = true;
                            }
                        }
                    }
                }

                // Is error here?
                if ( $is_save_error )
                {
                    $vec_errors = Log::errors_list();
                    foreach ( $vec_errors as $que_error )
                    {
                        $mail_model->addError('template_id', $que_error);
                    }
                }
                else
                {
                    Yii::app()->user->addFlash('success', 'Mail template updated successfully');

                    // Redirect to update page
                    $this->redirect(['update', 'id' => $mail_model->template_id]);
                }
            }
        }

        $this->render('update', [
            'model'                     => $mail_model,
            'vec_template_files'        => $mail_model->get_template_files(),
            'vec_mail_config'           => $vec_mail_config,
            'vec_translated_models'     => $vec_translated_models,
            'default_language'          => $default_language,
            'vec_extra_languages'       => $vec_extra_languages,
            'form_id'                   => 'mail-form',
        ]);
    }
}
