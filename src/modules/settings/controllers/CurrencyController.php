<?php
/*
|--------------------------------------------------------------------------
| Controller class for Currency model
|--------------------------------------------------------------------------
*/

namespace dz\modules\settings\controllers;

use dz\helpers\Html;
use dz\modules\settings\models\Currency;
use dz\web\Controller;
use Yii;

class CurrencyController extends Controller
{	
	/**
	 * List action for Currency models
	 */
	public function actionIndex()
	{
		$model = Yii::createObject(Currency::class, 'search');
		$model->unsetAttributes();
		$model->disable_filter = 2;
		
		if ( isset($_GET['Currency']) )
		{
			$model->setAttributes($_GET['Currency']);
		}

		// Update GRID with AJAX
		if ( Yii::app()->getRequest()->getIsAjaxRequest() )
		{
			// Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
			Yii::app()->clientScript->reset();
		}
		
		$this->render('index', [
			'model' => $model,
		]);
	}


	/**
	 * Create action for Currency model
	 */
	public function actionCreate()
	{
		// Currency model
        $currency_model = Yii::createObject(Currency::class);

		// Insert new model?
		if ( isset($_POST['Currency']) AND ( isset($_POST['Currency']['currency_id']) OR isset($_POST['Currency']['numeric_code']) ) )
		{
			if ( isset($_POST['Currency']['currency_id']) AND !empty($_POST['Currency']['currency_id']) AND ! $currency_model->add_currency($_POST['Currency']['currency_id']) )
			{
				Yii::app()->user->addFlash('error', 'La moneda con código "'. $_POST['Currency']['currency_id'] .'" no existe');
			}
			else if ( isset($_POST['Currency']['numeric_code']) AND !empty($_POST['Currency']['numeric_code']) AND ! $currency_model->add_currency($_POST['Currency']['numeric_code']) )
			{
				Yii::app()->user->addFlash('error', 'La moneda con código "'. $_POST['Currency']['numeric_code'] .'" no existe');
			}
			else if ( $currency_model->save() )
			{				
				Yii::app()->user->addFlash('success', 'Nueva moneda añadida correctamente');

				// Redirect to index page
				$this->redirect(['index']);
			}
		}

		$this->render('create', [
			'model' 	=> $currency_model,
			'form_id' 	=> 'currency-form'
		]);
	}


	/**
	 * Update action for Currency model
	 */
	public function actionUpdate($id)
	{
		// Currency model
        $currency_model = Currency::findOne($id);
        if ( ! $currency_model )
        {
            throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }
		
		// Update model?
		if ( isset($_POST['Currency']) )
		{
			$currency_model->setAttributes($_POST['Currency']);
			if ( $currency_model->save() )
			{
				Yii::app()->user->addFlash('success', 'Moneda actualizada correctamente');

				// Redirect to index page
				$this->redirect(['index']);
			}
		}

		$this->render('update', [
			'model' 	=> $currency_model,
			'form_id'	=> 'currency-form'
		]);
	}



	/**
	 * Disable a Currency
	 */
	public function actionDisable($id)
	{
		// Baja action only allowed by POST requests
		if ( Yii::app()->getRequest()->getIsPostRequest() )
		{
			$currency_model = Currency::findOne($id);
            if ( ! $currency_model )
            {
                throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
            }

			if ( $currency_model->disable() )
			{
				if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
				{
					Yii::app()->user->addFlash('success', Yii::t('app', 'Moneda desactivada correctamente') );
				}
				else
				{
					echo Yii::t('app', 'Moneda desactivada correctamente');
				}
			}
			else
			{
				$this->showErrors($currency_model->getErrors());
			}			
		}
		else
		{
			throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}
	
	
	
	/**
	 * Enable a Currency
	 */
	public function actionEnable($id)
	{
		// Alta action only allowed by POST requests
		if ( Yii::app()->getRequest()->getIsPostRequest() )
		{
			$currency_model = Currency::findOne($id);
            if ( ! $currency_model )
            {
                throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
            }

			if ( $currency_model->enable() )
			{
				if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
				{
					Yii::app()->user->addFlash('success', Yii::t('app', 'Moneda activada correctamente') );
				}
				else
				{
					echo Yii::t('app', 'Moneda activada correctamente');
				}
			}
			else
			{
				$this->showErrors($currency_model->getErrors());
			}			
		}
		else
		{
			throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}


    /**
     * Delete action for Currency model
     */
    public function actionDelete($id)
    {
        // Delete action only allowed by POST requests
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $currency_model = $this->loadModel($id, Currency::class);

            if ( $currency_model->delete() )
            {
                if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
                {
                    Yii::app()->user->addFlash('success', Yii::t('app', 'Moneda eliminada correctamente') );
                }
                else
                {
                    echo Yii::t('app', 'Moneda eliminada correctamente');
                }
            }
            else
            {
                $this->showErrors($currency_model->getErrors());
            }
        }
        else
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }
    }
	
	
	/**
	 * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
	 *
	 * array('<defined_action>' => '<operation_name_to_check>')
	 *
	 * @return array
	 */
	static public function checkAliasActions()
	{
		return [
			'index'	=> 'settings.currency.view'
		];
	}
}