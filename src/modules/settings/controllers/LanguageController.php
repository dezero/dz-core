<?php
/*
|--------------------------------------------------------------------------
| Controller class for Language model
|--------------------------------------------------------------------------
*/

namespace dz\modules\settings\controllers;

use dz\helpers\Html;
use dz\modules\settings\models\Language;
use dz\web\Controller;
use Yii;

class LanguageController extends Controller
{	
	/**
	 * List action for Language models
	 */
	public function actionIndex()
	{
        $model = Yii::createObject(Language::class, 'search');
		$model->unsetAttributes();
		$model->disable_filter = 2;
		
		if ( isset($_GET['Language']) )
		{
			$model->setAttributes($_GET['Language']);
		}

		// Update GRID with AJAX
		if ( Yii::app()->getRequest()->getIsAjaxRequest() )
		{
			// Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
			Yii::app()->clientScript->reset();
		}
		
		$this->render('index', [
			'model' => $model,
		]);
	}


	/**
	 * Create action for Language model
	 */
	public function actionCreate()
	{
		// Language model
        $language_model = Yii::createObject(Language::class);

		// Insert new model?
		if ( isset($_POST['Language']) AND isset($_POST['Language']['language_id']) )
		{
			if ( ! $language_model->add_language($_POST['Language']['language_id']) )
			{
				Yii::app()->user->addFlash('error', Yii::t('app', 'Language with code "{code}" does not exist', array('{code}' => $_POST['Language']['language_id'])));
			}
			else if ( $language_model->save() )
			{				
				Yii::app()->user->addFlash('success',  Yii::t('app', 'Language created successfully'));

				// Redirect to index page
				$this->redirect(['index']);
			}
		}

		$this->render('create', [
			'model' 	=> $language_model,
			'form_id' 	=> 'language-form'
		]);
	}


	/**
	 * Update action for Language model
	 */
	public function actionUpdate($id)
	{
		// Language model
		$language_model = $this->loadModel($id, Language::class);
		
		// Update model?
		if ( isset($_POST['Language']) )
		{
			$language_model->setAttributes($_POST['Language']);
			if ( $language_model->save() )
			{
				Yii::app()->user->addFlash('success', Yii::t('app', 'Language updated successfully'));

				// Redirect to index page
				$this->redirect(['index']);
			}
		}

		$this->render('update', [
			'model' 	=> $language_model,
			'form_id'	=> 'language-form'
		]);
	}



	/**
	 * Disable a Language
	 */
	public function actionDisable($id)
	{
		// Baja action only allowed by POST requests
		if ( Yii::app()->getRequest()->getIsPostRequest() )
		{
			$model = $this->loadModel($id, Language::class);
			if ( $model->disable() )
			{
				if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
				{
					Yii::app()->user->addFlash('success', Yii::t('app', 'Language disabled successfully'));
				}
				else
				{
					echo Yii::t('app', 'Language disabled successfully');
				}
			}
			else
			{
				$this->showErrors($model->getErrors());
			}			
		}
		else
		{
			throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}
	
	
	
	/**
	 * Enable a Language
	 */
	public function actionEnable($id)
	{
		// Alta action only allowed by POST requests
		if ( Yii::app()->getRequest()->getIsPostRequest() )
		{
			$model = $this->loadModel($id, Language::class);
			if ( $model->enable() )
			{
				if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
				{
					Yii::app()->user->addFlash('success', Yii::t('app', 'Language enabled successfully'));
				}
				else
				{
					echo Yii::t('app', 'Language enabled successfully');
				}
			}
			else
			{
				$this->showErrors($model->getErrors());
			}			
		}
		else
		{
			throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}


    /**
     * Delete action for Language model
     */
    public function actionDelete($id)
    {
        // Delete action only allowed by POST requests
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $language_model = $this->loadModel($id, Language::class);

            if ( $language_model->delete() )
            {
                if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
                {
                    Yii::app()->user->addFlash('success', Yii::t('app', 'Language deleted successfully') );
                }
                else
                {
                    echo Yii::t('app', 'Language deleted successfully');
                }
            }
            else
            {
                $this->showErrors($language_model->getErrors());
            }
        }
        else
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }
    }
	
	
	/**
	 * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
	 *
	 * array('<defined_action>' => '<operation_name_to_check>')
	 *
	 * @return array
	 */
	static public function checkAliasActions()
	{
		return [
			'index'	=> 'settings.language.view'
		];
	}
}
