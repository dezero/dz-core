<?php
/*
|--------------------------------------------------------------------------
| Controller class to manage Setting models (for shipping)
|--------------------------------------------------------------------------
*/

use dz\web\Controller;

class ShippingController extends Controller
{
	/**
	 * Admin action for Setting models
	 *
	 * @return HTML
	 */
	public function actionIndex()
	{
		if ( isset($_POST['Setting']) AND !empty($_POST['Setting']) )
		{
			foreach ( $_POST['Setting'] as $setting_name => $setting_value )
			{
				Yii::app()->settings->set($setting_name, $setting_value);
			}

			Yii::app()->user->addFlash('success', 'Parámetros actualizados correctamente');

			// Redirect to index page
			$this->redirect(array('index'));
		}

		$criteria = new CDbCriteria;
		$criteria->compare('type', 'shipping');
		$vec_models = Setting::model()->findAll($criteria);

		$this->render('index', array(
			'vec_models' => $vec_models,
		));
	}
}