<?php
/*
|--------------------------------------------------------------------------
| TEMP controller class
|--------------------------------------------------------------------------
*/

use dz\web\Controller;

class TempController extends Controller
{
	/**
	 * Admin action for Setting models
	 *
	 * @return HTML
	 */
	public function actionIndex($title)
	{
		$this->render('index', array(
			'title' => $title,
		));
	}
}