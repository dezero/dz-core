<?php
/*
|--------------------------------------------------------------------------
| Controller class for MailHistory model
|--------------------------------------------------------------------------
|
| Available variables:
| 	$data: MailHistory model class
|
*/

use dz\web\Controller;

class MailHistoryController extends Controller
{
	/**
	 * List action for MailHistory models
	 *
	 * @return HTML
	 */
	public function actionIndex()
	{
		$model = new MailHistory('search');
		$model->unsetAttributes();
		
		if ( isset($_GET['MailHistory']) )
		{
			$model->setAttributes($_GET['MailHistory']);
		}

		// Update GRID with AJAX
		if ( Yii::app()->getRequest()->getIsAjaxRequest() )
		{
			// Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
			Yii::app()->clientScript->reset();
		}

		$vec_templates_filter = array('all' => '- Todos -');
		$vec_mail_template_models = MailTemplate::model()->findAllByAttributes(array('language_id' => 'es'));
		foreach ( $vec_mail_template_models as $mail_template_model )
		{
			$que_name = $mail_template_model->name;
			if ( strlen($que_name) > 50 )
			{
				$que_name = substr($que_name, 0, 50) .'...';
			}
			$vec_templates_filter[$mail_template_model->template_id] = $que_name;
		}
		
		$this->render('index', array(
			'model' 				=> $model,
			'vec_templates_filter'	=> $vec_templates_filter
		));
	}


	/**
	 * View action for MailHistory model
	 */
	public function actionView($id)
	{
		$mail_history_model = $this->loadModel($id, 'MailHistory');

		// Render
		$this->render('view', array(
			'model' 	=> $mail_history_model,
		));
	}
}