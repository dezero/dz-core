<?php
/*
|--------------------------------------------------------------------------
| Controller class for MailTemplate model
|--------------------------------------------------------------------------
|
| Available variables:
| 	$data: MailTemplate model class
|
*/

use dz\helpers\Html;
use dz\web\Controller;

class MailTemplateController extends Controller
{
	/**
	 * List action for MailTemplate models
	 *
	 * @return HTML
	 */
	public function actionIndex()
	{
		$model = new MailTemplate('search');
		$model->unsetAttributes();
		$model->language_id = 'es';
		
		if ( isset($_GET['MailTemplate']) )
		{
			$model->setAttributes($_GET['MailTemplate']);
		}

		// Update GRID with AJAX
		if ( Yii::app()->getRequest()->getIsAjaxRequest() )
		{
			// Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
			Yii::app()->clientScript->reset();
		}
		
		$this->render('index', array(
			'model' => $model,
		));
	}


	/**
	 * Create action for MailTemplate model
	 *
	 * @return HTML
	 */
	public function actionCreate()
	{
		// MailTemplate model
		$mail_model = new MailTemplate;

		// Insert new model?
		if ( isset($_POST['MailTemplate']) )
		{
			$mail_model->setAttributes($_POST['MailTemplate']);

			// Custom validation for ALIAS
			$is_error = FALSE;
			if ( !empty($mail_model->alias) )
			{
				$alias_mail_model = MailTemplate::model()->findByAttributes(array('alias' => $mail_model->alias));
				if ( $alias_mail_model )
				{
					$is_error = TRUE;
					Yii::app()->user->addFlash('error', 'Ya existe una plantilla de mail con el alias <em>'. $mail_model->alias .'</em>.');
				}
			}

			if ( !$is_error AND $mail_model->save() )
			{
				Yii::app()->user->addFlash('success', 'Nueva plantilla de mail creada correctamente');

				// Redirect to index page
				$this->redirect(array('index'));
			}
		}

		$this->render('create', array(
			'model' 	=> $mail_model,
			'form_id' 	=> 'mail-template-form'
		));
	}


	/**
	 * Update action for MailTemplate model
	 *
	 * @return HTML
	 */
	public function actionUpdate($id)
	{
		// MailTemplate model
		$mail_model = $this->loadModel($id, 'MailTemplate');

		// Extra languages
		$vec_translated_models = array();
		$vec_extra_languages = array();
		/*
		$vec_extra_languages = Yii::app()->params['extra_languages'];
		if ( !empty($vec_extra_languages) )
		{
			foreach ( $vec_extra_languages as $language_id => $language_name )
			{
				$translated_mail_model = MailTemplate::model()->findByAttributes(array('language_id' => $language_id, 'alias' => $mail_model->alias));
				if ( empty($translated_mail_model) )
				{
					$translated_mail_model = new MailTemplate;
					$translated_mail_model->alias = $mail_model->alias;
					$translated_mail_model->language_id = $language_id;
				}
				$vec_translated_models[$language_id] = $translated_mail_model;
			}
		}
		*/

		// Update model?
		$is_error = FALSE;
		if ( isset($_POST['MailTemplate']) )
		{	
			// Translations
			if ( isset($_POST['TranslatedMailTemplate']) )
			{
				foreach ( $_POST['TranslatedMailTemplate'] as $language_id => $translated_content_values )
				{
					if ( isset($vec_translated_models[$language_id]) AND isset($translated_content_values['name']) AND !empty($translated_content_values['name']) )
					{
						$vec_translated_models[$language_id]->setAttributes($translated_content_values);
						if ( ! $vec_translated_models[$language_id]->save() )
						{
							$is_error = TRUE;
							Yii::app()->user->addFlash('error', 'Error al guardar idioma  <em>'. $vec_extra_languages[$language_id] .'</em>');
							// $vec_errors = $this->showErrors($vec_translated_models[$language_id]->getErrors(), TRUE);
							// Yii::app()->user->addFlash('error', 'Error al guardar idioma  <em>'. $vec_extra_languages[$language_id] .'</em>: '. Html::ul($vec_errors));
						}
					}
				}
			}

			$mail_model->setAttributes($_POST['MailTemplate']);
			if ( !$is_error AND $mail_model->save() )
			{
				Yii::app()->user->addFlash('success', 'Plantilla de mail actualizada correctamente');

				// Redirect to update page
				$this->redirect(array('update', 'id' => $mail_model->template_id));

				// Redirect to index page
				// $this->redirect(array('index'));
			}
		}
		
		// Render form page
		$this->render('update', array(
			'model' 				=> $mail_model,
			'vec_translated_models' => $vec_translated_models,
			'vec_extra_languages'	=> $vec_extra_languages,
			'form_id' 				=> 'mail-template-form'
		));
	}


	/**
	 * Test email templates
	 */
	public function actionTest()
	{
		// User MailHistory model for sending mails. BUT we don't save anything in database
		$mail_history_model = new MailHistory;

		// Mail template selected?
		if ( isset($_GET['template_id']) )
		{
			$mail_history_model->template_id = $_GET['template_id'];
		}

		if ( isset($_POST['MailHistory']) )
		{
			$mail_history_model->setAttributes($_POST['MailHistory']);

			if ( ! empty($_POST['MailHistory']['recipient_mails']) )
			{
				$vec_attributes = $_POST['MailHistory'];
				$mail_model = MailTemplate::model()->findByPk($vec_attributes['template_id']);
				if ( $mail_model )
				{
					$vec_mail_data = array();
					switch ( $mail_model->alias )
					{
						case 'new_order':
							$order_model = Order::model()->findByPk($vec_attributes['order_id']);
							if ( $order_model )
							{
								$mail_model->set_order($order_model);
								$mail_model->set_customer($order_model->customer);
							}
						break;

						// Emails sent to CUSTOMER
						default:
							$customer_model = Customer::model()->findByPk($vec_attributes['recipient_uid']);
							if ( $customer_model )
							{
								$mail_model->set_customer($customer_model);
							}
						break;
					}

					$mail_model->set_extra_data($vec_mail_data);
					if ( $mail_model->send_email($vec_attributes['recipient_mails'], $vec_attributes['language_id'], FALSE) )
					{
						Yii::app()->user->addFlash('success', 'Mail <em>'. $mail_model->alias .'</em> enviado a <em>'. $vec_attributes['recipient_mails'] .'</em>');
					}
					// return $mail_model->send_email();
				}
			}
			else
			{
				$mail_history_model->addError('recipient_mails', 'No se ha introducido ningún <em>Email destino</em>.');
			}
		}

		$vec_mail_templates = array();
		$vec_mail_template_models = MailTemplate::model()->findAllByAttributes(array('language_id' => 'es'));
		foreach ( $vec_mail_template_models as $mail_template_model )
		{
			$vec_mail_templates[$mail_template_model->template_id] = '['. $mail_template_model->alias .']' .' --> '. $mail_template_model->name;
		}

		// Languages
		$vec_languages = array('es' => 'Castellano');
		$vec_extra_languages = array();
		/*
		$vec_extra_languages = Yii::app()->params['extra_languages'];
		if ( !empty($vec_extra_languages) )
		{
			foreach ( $vec_extra_languages as $language_id => $language_name )
			{
				$vec_languages[$language_id] = $language_name;
			}
		}
		*/

		$this->render('test', array(
			'model'					=> $mail_history_model,
			'vec_mail_templates'	=> $vec_mail_templates,
			'vec_languages'			=> $vec_languages,
			'vec_dummy_customers'	=> $this->_test_dummy_data('customer'),
			'vec_dummy_orders'		=> $this->_test_dummy_data('order'),
		));
	}


	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model, $form = null)
	{
		if( isset($_POST['ajax']) && $_POST['ajax']==='mail-template-form' )
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	
	/**
 	 * Menu with action options for a MailTemplate model
	 *
	 * @return array
	 */
	public function menuAction($id)
	{
		if ( empty($this->_menu_action[$id]) )
		{
			$model = MailTemplate::model()->findByPk($id);
			if ( !empty($model->disable_date) )
			{
				$disable_enable_button = array(
					'label' => '<span class="text-green">'. Yii::t('app', 'Enable') .'</span>',
					'icon' => 'arrow-up',
					'url' => array('enable', 'id' => $id),
					'visible' => 'Yii::app()->user->checkAccess("config.currency.create")',
					'confirm' => '<h3>¿Estás seguro de ACTIVAR esta moneda?</h3>',
					'htmlOptions' => array(
						'id' => 'enable-currency-'.$id,
						// 'data-redirect' => Html::normalizeUrl(array('/config/currency'))
						'data-gridview' => 'currency-grid',
						'data-loading' => 'currency-loading',
					)
				);
			}
			else
			{
				$disable_enable_button = array(
					'label' => '<span class="text-danger">'. Yii::t('app', 'Disable') .'</span>',
					'icon' => 'arrow-down',
					'url' => array('disable', 'id' => $id),
					'visible' => 'Yii::app()->user->checkAccess("config.currency.create")',
					'confirm' => '<h3>¿Estás seguro de <span class=\'text-danger\'>DESACTIVAR</span> esta moneda?</h3><p>Las tarifas almacenados hasta el momento no se eliminarán, por si la moneda se vuelve a activar de nuevo.</p>',
					'htmlOptions' => array(
						'id' => 'disable-currency-'.$id,
						// 'data-redirect' => Html::normalizeUrl(array('/config/currency'))
						'data-gridview' => 'currency-grid',
						'data-loading' => 'currency-loading',
					)
				);
			}

			$this->_menu_action[$id] = array(
				$disable_enable_button,
				// '---',
				// array(
				// 	'label' => Yii::t('app', 'Ver productos temporada '. $id),
				// 	// 'icon' => 'trash',
				// 	'url' => self::createUrl('product/product') .'?M3Model[currency_filter]='. $id,
				// 	'visible' => 'Yii::app()->user->checkAccess("product.product.view")',
				// 	'htmlOptions' => array(
				// 		'target' => '_blank',
				// 	)
				// ),
			);
		}
		return $this->_menu_action[$id];
	}		
	
	
	/**
	 * Menu with action options (more options) for a MailTemplate model
	 *
	 * @return array
	 */
	static public function staticMenuAction($id)
	{
		$controller = new MailTemplateController('mail');
		return $controller->menuAction($id);
	}
	
	
	/**
	 * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
	 *
	 * array('<defined_action>' => '<operation_name_to_check>')
	 *
	 * @return array
	 */
	static public function checkAliasActions()
	{
		return array(
			'index'		=> 'config.mail.view',
			'test'		=> 'config.mail.update',
		);
	}


	/**
	 * Generate dummy data for dropdowns
	 */
	private function _test_dummy_data($data_type, $data_id = '')
	{
		$vec_dummy_data = array();

		switch ( $data_type )
		{
			case 'customer':
				$criteria = new CDbCriteria;
				$criteria->limit = 5;
				$vec_customer_models = Customer::model()->findAll($criteria);
				foreach ( $vec_customer_models as $customer_model )
				{
					$vec_dummy_data[$customer_model->user_id]  = $customer_model->user_id .' - '. $customer_model->get_fullname() .' - '. $customer_model->user->email;
				}
			break;

			// Order
			case 'order':
				$criteria = new CDbCriteria;
				$criteria->compare('t.order_id', $data_id);
				$criteria->compare('t.is_cart_completed', 1);
				$criteria->addCondition('t.status_type <> "N" AND t.status_type <> "O"');
				$criteria->limit = 5;
				$criteria->order = 't.updated_date DESC';
				$vec_order_models = Order::model()->findAll($criteria);
				foreach ( $vec_order_models as $order_model )
				{
					$vec_dummy_data[$order_model->order_id]  = $order_model->order_reference .' - '. $order_model->itemsTotal .' productos - '. $order_model->total_price .' €';
				}
			break;
		}

		return $vec_dummy_data;
	}
}