<?php
/*
|--------------------------------------------------------------------------
| Controller class for managing translations
|--------------------------------------------------------------------------
*/

namespace dz\modules\settings\controllers;

use dz\helpers\Html;
use dz\modules\settings\models\Language;
use dz\modules\settings\models\TranslationMessage;
use dz\modules\settings\models\TranslationSource;
use dz\web\Controller;
use Yii;

class TranslationController extends Controller
{
    /**
     * Translate form page
     */
    public function actionIndex($category = null)
    {
        // Enabled languages
        $vec_languages = Yii::app()->i18n->language_list('enabled');

        // Default language
        $default_language = Yii::app()->i18n->get_default_language();

        // Default category
        if ( $category === null )
        {
            $category = Yii::app()->i18n->get_default_translation_category();
        }

        // Remove default language (origin)
        if ( $category === 'app' && isset($vec_languages['en']) )
        {
            unset($vec_languages['en']);
        }
        else if ( $category !== 'app' && isset($vec_languages[$default_language]) )
        {
            unset($vec_languages[$default_language]);
        }
    
        // Get all source messages for this category
        $vec_translation_source_models = TranslationSource::get()
            ->where(['category' => $category])
            ->order('message ASC')
            ->all();

        if ( isset($_POST['TranslationSource']) && isset($_POST['TranslationMessage']) )
        {
            foreach ( $_POST['TranslationSource'] as $message_id => $que_message )
            {
                if ( !empty($que_message) )
                {
                    // Create new message
                    if ( preg_match("/new/", $message_id) )
                    {
                        $translation_source_model = Yii::createObject(TranslationSource::class);
                        $translation_source_model->category = $category;
                        $translation_source_model->message = $que_message;
                        if ( $translation_source_model->save() )
                        {
                            foreach ( $vec_languages as $language_id => $language_name )
                            {
                                if ( isset($_POST['TranslationMessage'][$language_id]) && isset($_POST['TranslationMessage'][$language_id][$message_id]) && !empty($_POST['TranslationMessage'][$language_id][$message_id]) )
                                {
                                    $translation_source_model->save_translation($language_id, $_POST['TranslationMessage'][$language_id][$message_id]);
                                }
                            }
                        }
                        else
                        {
                            Log::save_model_error($translation_source_model);
                        }
                    }

                    // Update existing message
                    else
                    {
                        $translation_source_model = TranslationSource::findOne($message_id);
                        $translation_source_model->message = $que_message;
                        if ( $translation_source_model->save() )
                        {
                            foreach ( $vec_languages as $language_id => $language_name )
                            {
                                if ( isset($_POST['TranslationMessage'][$language_id]) && isset($_POST['TranslationMessage'][$language_id][$message_id]) && !empty($_POST['TranslationMessage'][$language_id][$message_id]) )
                                {
                                    $translation_source_model->save_translation($language_id, $_POST['TranslationMessage'][$language_id][$message_id]);
                                }
                            }
                        }
                        else
                        {
                            Log::save_model_error($translation_source_model);
                        }
                    }
                }

                // Remove existing message
                else if ( ! preg_match("/new/", $message_id) )
                {
                    $translation_source_model = TranslationSource::findOne($message_id);
                    if ( $translation_source_model )
                    {
                        $translation_source_model->delete();
                    }

                }
            }

            // Category redirect
            if ( isset($_POST['Category']) && !empty($_POST['Category']) )
            {
                $this->redirect(['index', 'category' => $_POST['Category']]);
            }

            // Success message
            Yii::app()->user->addFlash('success', Yii::t('app', 'Translations updated successfully'));
            $this->redirect(['index', 'category' => $category]);
            
        }

        // New messages
        $vec_new_translation_models = [];
        for ( $i = 0; $i < 30; $i++ )
        {
            $translation_source_model =  Yii::createObject(TranslationSource::class);
            $translation_source_model->category = $category;
            $vec_new_translation_models[$i] = $translation_source_model;
        }

        // Translation category types
        $vec_translation_categories = Yii::app()->i18n->get_translation_categories();

        // Page title
        $page_title = 'Translations';
        if ( $category !== 'app' )
        {
            if ( !empty($vec_translation_categories) && isset($vec_translation_categories[$category]) )
            {
                $page_title .= ' - '. $vec_translation_categories[$category];
            }
            else
            {
                $page_title .= ' - '. $category;
            }
        }
        $page_title = Yii::t('app', $page_title);

        $this->render('index', [
            'category'                      => $category,
            'vec_languages'                 => $vec_languages,
            'default_language'              => $default_language,
            'vec_translation_source_models' => $vec_translation_source_models,
            'vec_new_translation_models'    => $vec_new_translation_models,
            'vec_translation_categories'    => $vec_translation_categories,
            'page_title'                    => $page_title,
            'form_id'                       => 'translation-form'
        ]);
    }
}
