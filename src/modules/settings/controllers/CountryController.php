<?php
/*
|--------------------------------------------------------------------------
| Controller class for Country model
|--------------------------------------------------------------------------
*/

namespace dz\modules\settings\controllers;

use dz\helpers\Html;
use dz\modules\settings\models\Country;
use dz\web\Controller;
use Yii;

class CountryController extends Controller
{   
    /**
     * List action for Country models
     */
    public function actionIndex()
    {
        $model = Yii::createObject(Country::class, 'search');
        $model->unsetAttributes();
        
        if ( isset($_GET['Country']) )
        {
            $model->setAttributes($_GET['Country']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }
        
        $this->render('index', [
            'model' => $model,
        ]);
    }


    /**
     * Update action for Country model
     */
    public function actionUpdate($id)
    {
        // Country model
        $country_model = Country::findOne($id);
        if ( ! $country_model )
        {
            throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }
        
        // Update model?
        if ( isset($_POST['Country']) )
        {
            $country_model->setAttributes($_POST['Country']);
            if ( $country_model->save() )
            {
                Yii::app()->user->addFlash('success', 'Country updated successfully');

                // Redirect to index page
                $this->redirect(['index']);
            }
        }

        $this->render('update', [
            'model'     => $country_model,
            'form_id'   => 'country-form'
        ]);
    }

    
    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index' => 'settings.country.view'
        ];
    }
}