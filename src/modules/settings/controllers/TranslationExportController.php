<?php
/*
|---------------------------------------------------------------
| Controller class used for exporting translations messages
|---------------------------------------------------------------
*/

namespace dz\modules\settings\controllers;

use dz\helpers\Html;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\utils\excel\ExcelTrait;
use dz\modules\asset\models\AssetFile;
use dz\modules\settings\models\Language;
use dz\modules\settings\services\TranslationExportExcelService;
use dz\web\Controller;
use Yii;

class TranslationExportController extends Controller
{
    /**
     * Excel exclusive actions
     */
    use ExcelTrait;


    /**
     * Export translation messages
     */
    public function actionIndex()
    {
        // Default language
        $default_language = Yii::app()->i18n->get_default_language();

        // Extra languages
        $vec_extra_languages = Yii::app()->i18n->language_list('extras');

        // Default translation category
        $translation_category = Yii::app()->i18n->get_default_translation_category();

        // Export translation(s) to an Excel file
        if ( isset($_POST['ExportLanguageId']) && isset($_POST['TranslationCategory']) )
        {
            // Language model is required
            $language_model = $this->loadModel($_POST['ExportLanguageId'], Language::class);

            // Translation category
            $translation_category = $_POST['TranslationCategory'];

            // Export translations(s) to an Excel file via TranslationExportExcelService class
            $translation_export_excel_service = Yii::createObject(TranslationExportExcelService::class, [$language_model, $translation_category]);
            if ( $translation_export_excel_service->run() )
            {
                $vec_excel_sheets = $translation_export_excel_service->get_excel_sheets();

                // Export to excel action
                $export_filename = 'export_translations_'. time();
                $vec_document_options = [
                    'creator'   => Yii::app()->name,
                    'subject'   => Yii::app()->name,
                    // 'autoWidth'  =>  false,
                ];

                // ExcelBehavior::toExcel() calls to widget ExcelView
                $this->toExcel($vec_excel_sheets, $export_filename, $vec_document_options, 'Excel5');



                // Yii::app()->user->addFlash('success', 'Excel file exported successfully');

                // $this->redirect(['/settings/translationExport']);
            }
            else
            {
                $this->showErrors($translation_export_excel_service->get_errors());
            }
        }

        // Render form
        $this->render('//settings/translationExport/index', [
            'default_language'              => $default_language,
            'default_language_model'        => Yii::app()->i18n->get_default_language(true),
            'vec_extra_languages'           => $vec_extra_languages,
            'vec_translation_categories'    => Yii::app()->i18n->get_translation_categories(),
            'translation_category'          => $translation_category,
            'form_id'                       => 'translation-export-form'
        ]);
    }


    /**
     * Behaviors for Controller
     */
    public function behaviors()
    {
        return array(
            'eexcelview' => [
                'class'     => '\dz\behaviors\ExcelBehavior',
                // 'excelView' => '@dz.utils.excel.ExcelView'
                'excelView' => '@dz.modules.settings.components.TranslationExcelView'
            ],
        );
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index'     => 'settings.translation.create',
        ];
    }
}
