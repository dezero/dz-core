<?php
/*
|---------------------------------------------------------------
| Controller class used for importing translations messages
|---------------------------------------------------------------
*/

namespace dz\modules\settings\controllers;

use dz\helpers\Html;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\asset\models\AssetFile;
use dz\modules\settings\models\TranslationImportExcel;
use dz\modules\settings\models\Language;
use dz\modules\settings\services\TranslationImportExcelService;
use dz\web\Controller;
use Yii;

class TranslationImportController extends Controller
{
    /**
     * List action for TranslationImportExcel models
     */
    public function actionIndex()
    {
        // Batch model
        $batch_import_model = Yii::createObject(TranslationImportExcel::class);

        if ( isset($_GET['Batch']) )
        {
            $batch_import_model->setAttributes($_GET['Batch']);
        }

        // AssetFile model to save imported Excel file
        $upload_file_model = Yii::createObject(AssetFile::class);

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        }

        $this->render('//settings/translationImport/index', [
            'batch_import_model'    => $batch_import_model,
            'upload_file_model'     => $upload_file_model
        ]);
    }


    /**
     * Create a new TranslationImportExcel model
     */
    public function actionCreate()
    {
        // Batch model
        $batch_import_model = Yii::createObject(TranslationImportExcel::class);

        // AssetFile model to save imported Excel file
        $upload_file_model = Yii::createObject(AssetFile::class);
        $upload_file_model->setAttributes([
            'entity_type'   => 'TranslationMessage'
        ]);

        // Default language
        $default_language = Yii::app()->i18n->get_default_language();

        // Extra languages
        $vec_extra_languages = Yii::app()->i18n->language_list('extras');

        // Create translation(s) via EXCEL file?
        if ( isset($_POST['AssetFile']) )
        {
            // Language model is required
            $language_model = $this->loadModel($_POST['ImportLanguageId'], Language::class);

            // Import Excel file via TranslationImportExcelService class
            $translation_import_excel_service = Yii::createObject(TranslationImportExcelService::class, [$language_model, $upload_file_model, $batch_import_model]);
            if ( $translation_import_excel_service->run() )
            {
                Yii::app()->user->addFlash('success', 'Excel file imported successfully');

                if ( !empty($batch_import_model->batch_id) )
                {
                    $this->redirect(['/settings/translationImport/view', 'import_id' => $batch_import_model->batch_id]);
                }
                else
                {
                    $this->redirect(['/settings/translationImport']);
                }
            }
            else
            {
                $this->showErrors($certificate_import_excel_service->get_errors());
            }
        }

        // Render form
        $this->render('//settings/translationImport/create', [
            'upload_file_model'     => $upload_file_model,
            'batch_import_model'    => $batch_import_model,
            'default_language'      => $default_language,
            'vec_extra_languages'   => $vec_extra_languages,
            'form_id'               => 'translation-import-form'
        ]);
    }


    /**
     * View action for TranslationImportExcel models
     */
    public function actionView($import_id)
    {
        // TranslationImportExcel model
        $batch_import_model = $this->loadModel($import_id, TranslationImportExcel::class);
        if ( ! $batch_import_model->file )
        {
            throw new \CHttpException(400, Yii::t('app', 'Import file does not exist.'));
        }

        $this->render('//settings/translationImport/view', [
            'batch_import_model'    => $batch_import_model,
            'file_model'            => $batch_import_model->file,
            'total_imported'        => $batch_import_model->get_totals('inserted') + $batch_import_model->get_totals('updated')
        ]);
    }


    /**
     * Download Excel template file
     */
    public function actionDownload()
    {
        $excel_template_file = Yii::app()->file->set(Yii::app()->path->corePath() . DIRECTORY_SEPARATOR .'views' . DIRECTORY_SEPARATOR .'settings'. DIRECTORY_SEPARATOR .'translationImport'. DIRECTORY_SEPARATOR .'import_translations.xls');
        if ( $excel_template_file && $excel_template_file->getExists() )
        {
            $file_name = $excel_template_file->getFilename();

            // Copy file to temp directory
            $temp_path = Yii::app()->path->get('temp') . DIRECTORY_SEPARATOR . 'download' . DIRECTORY_SEPARATOR;
            $temp_dir = Yii::app()->file->set($temp_path);
            if ( ! $temp_dir->isDir )
            {
                $temp_dir->createDir(Yii::app()->file->default_permissions, $temp_path);
            }
            $destination_file_path = $temp_path . StringHelper::random_timestamp('suffix', 5) .'_'. $file_name .'.xls';
            $destination_file = $excel_template_file->copy($destination_file_path);

            // Download file
            if ( $destination_file && $destination_file->getExists() )
            {
                if ( ! $destination_file->download($file_name .'.xls') )
                {
                    throw new \CHttpException(403, Yii::t('app', 'File "'. $file_name .'" cannot be downloaded.'));
                }
            }
        }

        throw new \CHttpException(404, Yii::t('app', 'Template file does not exist.'));
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index'     => 'settings.translation.view',
            'view'      => 'settings.translation.view',
            'create'    => 'settings.translation.create',
            'download'  => 'settings.translation.create',
        ];
    }
}
