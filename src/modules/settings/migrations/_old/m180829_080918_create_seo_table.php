<?php
/**
 * Migration class m180829_080918_create_seo_table
 *
 * @link http://www.dezero.es
 */

use dz\db\Migration;

class m180829_080918_create_seo_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Load & execute SQL file
		$sql_file_path = Yii::getPathOfAlias('app.modules.config.data') . DIRECTORY_SEPARATOR . 'schema--seo.sql';
		$this->execute_sql_file($sql_file_path);
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// Remove seo tables
		$this->dropTable('seo_url_history');
		$this->dropTable('seo');

		// Delete permissions
		$this->execute(
			"DELETE FROM `user_auth_item` WHERE `name` LIKE \"config.seo.*\";
			 DELETE FROM `user_auth_item_child` WHERE `child` LIKE \"config.seo.*\";"
		);
		return TRUE;
	}
}

