<?php
/**
 * Migration class m190207_192444_setting_table
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;

class m190207_192444_setting_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Execute SQL code
		$this->execute(
			"DROP TABLE IF EXISTS `setting`;

			CREATE TABLE IF NOT EXISTS `setting` (
				`name` VARCHAR(32) NOT NULL,
				`value` VARCHAR(255) NOT NULL,
				`type` VARCHAR(32) NOT NULL,
				`title` VARCHAR(255) NULL,
				`description` VARCHAR(512) NULL,
				`created_date` INT UNSIGNED NOT NULL,
				`created_uid` INT UNSIGNED NOT NULL
					COMMENT 'CONSTRAINT FOREIGN KEY (created_uid) REFERENCES user_users(id)',
				`updated_date` INT UNSIGNED NOT NULL,
				`updated_uid` INT UNSIGNED NOT NULL
					COMMENT 'CONSTRAINT FOREIGN KEY (updated_uid) REFERENCES user_users(id)',
				PRIMARY KEY (`name`),
				INDEX `settings_type_index` (`type`)
			) ENGINE=MyISAM;"
		);

		// Insert shipping params
		$this->insert('setting', array(
			'name'	=> 'shipping_amount',
			'value' => '6.00',
			'type' => 'shipping',
			'title' => 'Gastos fijos',
			'description' => 'Gastos de transporte a toda España, excepto Canarias',
			'created_date' => time(),
			'created_uid' => 1,
			'updated_date' => time(),
			'updated_uid' => 1,
		));
		$this->insert('setting', array(
			'name'	=> 'shipping_canarias_amount',
			'value' => '14.00',
			'type' => 'shipping',
			'title' => 'Gastos a Canarias',
			'description' => 'Gastos de transporte a la comunidad de Canarias',
			'created_date' => time(),
			'created_uid' => 1,
			'updated_date' => time(),
			'updated_uid' => 1,
		));
		$this->insert('setting', array(
			'name'	=> 'shipping_free_from_amount',
			'value' => '150.00',
			'type' => 'shipping',
			'title' => 'Gratis a partir de',
			'description' => 'Importe total del pedido en EUR a partir del cual los gastos de transporte son gratis',
			'created_date' => time(),
			'created_uid' => 1,
			'updated_date' => time(),
			'updated_uid' => 1,
		));
		return TRUE;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		$this->dropTable('settings');
		return FALSE;
	}
}

