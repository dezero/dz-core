<?php
/**
 * Migration class m190726_144717_mail_template_tables
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;

class m190726_144717_mail_template_tables extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "mail_template" table
		$this->execute(
			'DROP TABLE IF EXISTS `mail_template`;

			CREATE TABLE IF NOT EXISTS `mail_template` (
				`template_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`alias` VARCHAR(64) NOT NULL,
				`language_id` VARCHAR(4) NOT NULL DEFAULT "es"
					COMMENT "CONSTRAINT FOREIGN KEY (language_id) REFERENCES language(language_id)",
				`name` VARCHAR(128) NOT NULL,
				`description` TEXT NULL,
				`subject` VARCHAR(255) NOT NULL,
				`body` TEXT NOT NULL,
				`header` TEXT NULL,
				`footer` TEXT NULL,
				`disable_date` INT UNSIGNED NULL,
				`disable_uid` INT UNSIGNED NULL
					COMMENT "CONSTRAINT FOREIGN KEY (disable_uid) REFERENCES user_users(id)",
				`created_date` INT UNSIGNED NOT NULL,
				`created_uid` INT UNSIGNED NOT NULL
					COMMENT "CONSTRAINT FOREIGN KEY (created_uid) REFERENCES user_users(id)",
				`updated_date` INT UNSIGNED NOT NULL,
				`updated_uid` INT UNSIGNED NOT NULL
					COMMENT "CONSTRAINT FOREIGN KEY (updated_uid) REFERENCES user_users(id)",
				PRIMARY KEY (`template_id`),
				INDEX `template_alias_index` (`alias`)
			) ENGINE=MyISAM;'
		);

		// Create "mail_history" table
		$this->execute(
			'DROP TABLE IF EXISTS `mail_history`;

			CREATE TABLE IF NOT EXISTS `mail_history` (
				`mail_history_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`subject` VARCHAR(255) NOT NULL,
				`body` TEXT NOT NULL,
				`sender_mail` VARCHAR(100) NOT NULL,
				`recipient_mails` VARCHAR(255) NOT NULL,
				`recipient_uid` INT UNSIGNED NULL
					COMMENT "CONSTRAINT FOREIGN KEY (recipient_uid) REFERENCES user_users(id)",
				`language_id` VARCHAR(4) NOT NULL DEFAULT "es"
					COMMENT "CONSTRAINT FOREIGN KEY (language_id) REFERENCES language(language_id)",
				`file_name_eml` VARCHAR(64) NULL,
				`is_pending` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
				`is_error` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
				`error_description` VARCHAR(255) NULL,
				`template_id` INT UNSIGNED NULL
					COMMENT "CONSTRAINT FOREIGN KEY (template_id) REFERENCES mail_template(template_id)",
				`template_alias` VARCHAR(64) NULL,
				`order_id` INT UNSIGNED NULL DEFAULT NULL
					COMMENT "CONSTRAINT FOREIGN KEY (order_id) REFERENCES order(order_id)",
				`created_date` INT UNSIGNED NOT NULL,
				`created_uid` INT UNSIGNED NOT NULL
					COMMENT "CONSTRAINT FOREIGN KEY (created_uid) REFERENCES user_users(id)",
				PRIMARY KEY (`mail_history_id`),
				INDEX `mail_recipient_index` (`recipient_uid`),
				INDEX `mail_recipient_error_index` (`recipient_uid`, `is_error`),
				INDEX `mail_pending_index` (`is_pending`),
				INDEX `mail_error_index` (`is_error`),
				INDEX `mail_order_index` (`order_id`)
			) ENGINE=MyISAM;'
		);

		// Create permissions
		$this->execute(
			"DELETE FROM `user_auth_item` WHERE `name` LIKE \"config.mailHistory.*\";
			 DELETE FROM `user_auth_item_child` WHERE `child` LIKE \"config.mailHistory.*\";
			 DELETE FROM `user_auth_item` WHERE `name` LIKE \"config.mailTemplate.*\";
			 DELETE FROM `user_auth_item_child` WHERE `child` LIKE \"config.mailTemplate.*\";

			 -- Operations (level 0)
			 INSERT INTO `user_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
			  ('config.mailHistory.*',0,'Configuration - Mail History - Full access',NULL,'N;'),
			  ('config.mailTemplate.*',0,'Configuration - Mail Templates - Full access to manage templates',NULL,'N;');
			  
			INSERT INTO `user_auth_item_child` (`parent`, `child`) VALUES
			  -- Relations tasks and operations
			  ('config_manage','config.mailHistory.*'),
			  ('config_manage','config.mailTemplate.*');"
		);

		return TRUE;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// Remove mails tables
		$this->dropTable('mail_template');
		$this->dropTable('mail_history');

		// Delete permissions
		$this->execute(
			"DELETE FROM `user_auth_item` WHERE `name` LIKE \"config.mailHistory.*\";
			 DELETE FROM `user_auth_item_child` WHERE `child` LIKE \"config.mailHistory.*\";
			 DELETE FROM `user_auth_item` WHERE `name` LIKE \"config.mailTemplate.*\";
			 DELETE FROM `user_auth_item_child` WHERE `child` LIKE \"config.mailTemplate.*\";"
		);

		return FALSE;
	}
}

