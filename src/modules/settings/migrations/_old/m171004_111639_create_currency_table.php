<?php
/**
 * Migration class m171004_111639_create_currency_table
 *
 * @link http://www.dezero.es
 */

use dz\db\Migration;

class m171004_111639_create_currency_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Load & execute SQL file
		$sql_file_path = Yii::getPathOfAlias('app.modules.config.data') . DIRECTORY_SEPARATOR . 'schema--currency.sql';
		$this->execute_sql_file($sql_file_path);
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// Remove currency table
		$this->dropTable('currency');

		// Delete permissions
		$this->execute(
			"DELETE FROM `user_auth_item` WHERE `name` LIKE \"config.currency.*\";
			 DELETE FROM `user_auth_item_child` WHERE `child` LIKE \"config.currency.*\";"
		);
		return TRUE;
	}
}

