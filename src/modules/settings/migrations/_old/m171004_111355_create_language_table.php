<?php
/**
 * Migration class m171004_111355_create_language_table
 *
 * @link http://www.dezero.es
 */

use dz\db\Migration;

class m171004_111355_create_language_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Load & execute SQL file
		$sql_file_path = Yii::getPathOfAlias('app.modules.config.data') . DIRECTORY_SEPARATOR . 'schema--language.sql';
		$this->execute_sql_file($sql_file_path);
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// Remove language table
		$this->dropTable('language');

		// Delete permissions
		$this->execute(
			"DELETE FROM `user_auth_item` WHERE `name` LIKE \"config.language.*\";
			 DELETE FROM `user_auth_item_child` WHERE `child` LIKE \"config.language.*\";"
		);

		return TRUE;
	}
}

