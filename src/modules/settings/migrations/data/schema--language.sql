-- -----------------------------------------------------
-- Table `language`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `language` ;

CREATE TABLE IF NOT EXISTS `language` (
  `language_id` VARCHAR(4) NOT NULL,
  `name` VARCHAR(64) NOT NULL,
  `native` VARCHAR(64) NULL,
  `prefix` VARCHAR(16) NOT NULL,
  `is_ltr_direction` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1
    COMMENT 'Direction of language (Left-to-Right = 1, Right-to-Left = 0).',
  `is_default` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `weight` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `disable_date` INT UNSIGNED NULL,
  `disable_uid` INT UNSIGNED NULL
    COMMENT 'CONSTRAINT FOREIGN KEY (disable_uid) REFERENCES user_users(uid)',
  `created_date` INT UNSIGNED NOT NULL,
  `created_uid` INT UNSIGNED NOT NULL
    COMMENT "CONSTRAINT FOREIGN KEY (created_uid) REFERENCES user_users(uid)",
  `updated_date` INT UNSIGNED NOT NULL,
  `updated_uid` INT UNSIGNED NOT NULL
    COMMENT "CONSTRAINT FOREIGN KEY (updated_uid) REFERENCES user_users(uid)",
  PRIMARY KEY (`language_id`)
) ENGINE=MyISAM;

-- Add default languages
INSERT INTO `language` (`language_id`, `name`, `native`, `prefix`, `is_ltr_direction`, `is_default`, `weight`, `disable_date`, `disable_uid`, `created_date`, `created_uid`, `updated_date`, `updated_uid`) VALUES
('en', 'English', 'English', 'en', 1, 0, 2, NULL, NULL, 1489347780, 1, 1489349046, 1),
('es', 'Spanish', 'Español', 'es', 1, 1, 1, NULL, NULL, 1489347240, 1, 1489772812, 1);



-- ------------------------------------------------------------
-- Auth items (Roles and permissions)
-- ------------------------------------------------------------
DELETE FROM `user_auth_item` WHERE `name` LIKE "config_manage";
DELETE FROM `user_auth_item` WHERE `name` LIKE "config.language.*";
DELETE FROM `user_auth_item_child` WHERE `child` LIKE "config_manage";
DELETE FROM `user_auth_item_child` WHERE `child` LIKE "config.language.*";

INSERT INTO `user_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('config_manage', 1, 'Manage configuration (full access)', NULL, 'N;'),
('config.language.*', 0, 'Configuration - Manage languages', NULL, 'N;');

INSERT INTO `user_auth_item_child` (`parent`, `child`) VALUES
('admin', 'config_manage'),
('config_manage', 'config.language.*');
