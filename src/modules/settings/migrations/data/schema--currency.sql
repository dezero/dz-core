-- -----------------------------------------------------
-- Table `currency`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `currency` ;

CREATE TABLE IF NOT EXISTS `currency` (
  `currency_id` VARCHAR(4) NOT NULL,
  `name` VARCHAR(64) NOT NULL,
  `symbol` VARCHAR(16) NOT NULL,
  `numeric_code` SMALLINT(4) NOT NULL,
  `symbol_placement` CHAR(1) NULL
    COMMENT "ENUM ('A' => 'After', 'B' => 'Before')",
  `minor_unit` VARCHAR(16) NULL,
  `major_unit` VARCHAR(16) NULL,
  `thousands_separator` VARCHAR(16) NULL,
  `decimal_separator` VARCHAR(16) NULL,
  `decimals_number` TINYINT(1) NOT NULL DEFAULT 2,
  `weight` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `disable_date` INT UNSIGNED NULL,
  `disable_uid` INT UNSIGNED NULL
    COMMENT 'CONSTRAINT FOREIGN KEY (disable_uid) REFERENCES user_users(uid)',
  `created_date` INT UNSIGNED NOT NULL,
  `created_uid` INT UNSIGNED NOT NULL
    COMMENT "CONSTRAINT FOREIGN KEY (created_uid) REFERENCES user_users(uid)",
  `updated_date` INT UNSIGNED NOT NULL,
  `updated_uid` INT UNSIGNED NOT NULL
    COMMENT "CONSTRAINT FOREIGN KEY (updated_uid) REFERENCES user_users(uid)",
  PRIMARY KEY (`currency_id`)
) ENGINE=MyISAM;

-- ------------------------------------------------------------
-- Auth items (Roles and permissions)
-- ------------------------------------------------------------
DELETE FROM `user_auth_item` WHERE `name` LIKE "config.currency.*";
DELETE FROM `user_auth_item_child` WHERE `child` LIKE "config.currency.*";

INSERT INTO `user_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('config.currency.*', 0, 'Configuration - Manage currencies', NULL, 'N;');

INSERT INTO `user_auth_item_child` (`parent`, `child`) VALUES
('config_manage', 'config.currency.*');