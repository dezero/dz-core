<?php
/**
 * Migration class m200218_125738_country_eu_field
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

use dz\modules\settings\models\Country;

class m200218_125738_country_eu_field extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Add new column to check if country belongs to European Union
		$this->addColumn('country', 'is_eu', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0)->after('name'));

        // Create new index for the new column
        $this->createIndex(null, 'country', ['is_eu'], false);

        /**
         * Insert default UE countries
         * 
         * @see https://gist.github.com/henrik/1688572
         */
        $countries_list = "'AT', 'BE', 'BG', 'CY', 'CZ', 'DK', 'EE', 'FI', 'FR', 'DE', 'GR', 'HU', 'HR', 'IE', 'IT', 'LV', 'LT', 'LU', 'MT', 'NL', 'PL', 'PT', 'RO', 'SK', 'SI', 'ES', 'SE', 'GB'";
        $this->update('country', ['is_eu' => 1], 'country_code IN ('. $countries_list .')');

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

