<?php
/**
 * Migration class m201015_142143_translation_tables
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m201015_142143_translation_tables extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "translation_source" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('translation_source', true);

        $this->createTable('translation_source', [
            'id' => $this->primaryKey(),
            'category' => $this->string(64),
            'message' => $this->text(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull()
        ]);
    
        // Create indexes
        $this->createIndex(null, 'translation_source', ['category'], false);
        $this->addForeignKey(null, 'translation_source', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);


        // Create "translation_message" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('translation_message', true);

        $this->createTable('translation_message', [
            'id' => $this->integer(10)->unsigned()->notNull(),
            'language_id' => $this->string(4)->notNull()->defaultValue('es'),
            'translation' => $this->text(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull()
        ]);

        // Primary key
        $this->addPrimaryKey(null, 'translation_message', ['id', 'language_id']);
    
        // Foreign keys
        $this->addForeignKey(null, 'translation_message', ['id'], 'translation_source', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'translation_message', ['language_id'], 'language', ['language_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'translation_message', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

