<?php
/**
 * Migration class m190204_170000_insert_mail_templates
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m190204_170000_insert_mail_templates extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Mail templates for this project
        // -------------------------------------------------------------------------
        $this->insertMultiple('mail_template', [

            // NEW CONTACT MESSAGE
            [
                'alias'                 => 'contact_message',
                'language_id'           => 'en',
                'name'                  => 'Contact message received',
                'description'           => 'An user has submitted the contact form. An email is sent to administrators to notify it.',
                'sender_name'           => Yii::app()->name,
                'sender_email'          => Yii::app()->params['adminEmail'],
                'recipient_emails'      => Yii::app()->params['adminEmail'],
                'source_type'           => 'database',
                'subject'               => Yii::app()->name .' - New contact message received',
                'template_file_path'    => 'notification.tpl.php',
                'body'                  => $this->_get_body('contact_message', 'en'),
                'created_date'          => time(),
                'created_uid'           => 1,
                'updated_date'          => time(),
                'updated_uid'           => 1,
                'uuid'                  => StringHelper::UUID()
            ],
            [
                'alias'                 => 'contact_message',
                'language_id'           => 'es',
                'name'                  => 'Mensaje de contacto recibido',
                'description'           => 'Un usuario ha enviado un mensaje a través del formulario de contacto. Se envía un email al administrador para notificarlo.',
                'sender_name'           => Yii::app()->name,
                'sender_email'          => Yii::app()->params['adminEmail'],
                'recipient_emails'      => Yii::app()->params['adminEmail'],
                'source_type'           => 'database',
                'subject'               => Yii::app()->name .' - Nuevo mensaje de contacto',
                'template_file_path'    => 'notification.tpl.php',
                'body'                  => $this->_get_body('contact_message', 'es'),
                'created_date'          => time(),
                'created_uid'           => 1,
                'updated_date'          => time(),
                'updated_uid'           => 1,
                'uuid'                  => StringHelper::UUID()
            ],

            // RESET PASSWORD
            [
                'alias'                 => 'reset_password',
                'language_id'           => 'en',
                'name'                  => 'Reset password request',
                'description'           => 'User has forgotten the password and requested a new one. This email contains all the information to reset the password.',
                'sender_name'           => Yii::app()->name,
                'sender_email'          => Yii::app()->params['adminEmail'],
                'source_type'           => 'database',
                'subject'               => Yii::app()->name .' - You have requested the password recovery',
                'template_file_path'    => 'notification.tpl.php',
                'body'                  => $this->_get_body('reset_password', 'en'),
                'created_date'          => time(),
                'created_uid'           => 1,
                'updated_date'          => time(),
                'updated_uid'           => 1,
                'uuid'                  => StringHelper::UUID()
            ],
            [
                'alias'                 => 'reset_password',
                'language_id'           => 'es',
                'name'                  => 'Solicitud para restablecer contraseña',
                'description'           => 'Un usuario ha olvidado su contraseña y solicita una nueva. Este email contiene toda la información para restablecer la contraseña.',
                'sender_name'           => Yii::app()->name,
                'sender_email'          => Yii::app()->params['adminEmail'],
                'source_type'           => 'database',
                'subject'               => Yii::app()->name .' - Restablecer contraseña',
                'template_file_path'    => 'notification.tpl.php',
                'body'                  => $this->_get_body('reset_password', 'es'),
                'created_date'          => time(),
                'created_uid'           => 1,
                'updated_date'          => time(),
                'updated_uid'           => 1,
                'uuid'                  => StringHelper::UUID()
            ],

            // WELCOME MAIL
            [
                'alias'                 => 'welcome',
                'language_id'           => 'en',
                'name'                  => 'Welcome message - Sent after new user registration',
                'description'           => 'User has been submitted successfully. This email the welcome and the first email user receives.',
                'sender_name'           => Yii::app()->name,
                'sender_email'          => Yii::app()->params['adminEmail'],
                'source_type'           => 'database',
                'subject'               => 'Welcome to '. Yii::app()->name,
                'template_file_path'    => 'notification.tpl.php',
                'body'                  => $this->_get_body('welcome', 'en'),
                'created_date'          => time(),
                'created_uid'           => 1,
                'updated_date'          => time(),
                'updated_uid'           => 1,
                'uuid'                  => StringHelper::UUID()
            ],
            [
                'alias'                 => 'welcome',
                'language_id'           => 'es',
                'name'                  => 'Mensaje de bienvenida - Se envía después del registro de un nuevo usuario',
                'description'           => 'Un usuario se ha registrado correctamente. Este es un mail de bienvenida y el primero que recibirá el usuario.',
                'sender_name'           => Yii::app()->name,
                'sender_email'          => Yii::app()->params['adminEmail'],
                'source_type'           => 'database',
                'subject'               => 'Bienvenido a '. Yii::app()->name,
                'template_file_path'    => 'notification.tpl.php',
                'body'                  => $this->_get_body('welcome', 'es'),
                'created_date'          => time(),
                'created_uid'           => 1,
                'updated_date'          => time(),
                'updated_uid'           => 1,
                'uuid'                  => StringHelper::UUID()
            ],
        ]);

        return true;
    }


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}


    /**
     * Predefined body for mail templates
     */
    private function _get_body($alias, $language_id)
    {
        switch ( $alias )
        {
            case 'contact_message':
                if ( $language_id === 'es' )
                {
                    return "Nuevo mensaje recibido desde el formulario de contacto ({site_url}):

* Fecha: {contact.date}
* Nombre {contact.name}
* Email: {contact.email}
* Teléfono: {contact.phone}

Mensaje:
> {contact.message}

-- Este mensaje ha sido enviado automáticamente desde [{site_name}]({site_url})";
            break;
                }

                return "New message received from the contact form ({site_url}):

* Date: {contact.date}
* Name {contact.name}
* Mail: {contact.mail}
* Phone: {contact.phone}

Content message:
> {contact.message}

-- This message has been sent automatically from the website [{site_name}]({site_url})";
            break;

            case 'reset_password';
                if ( $language_id === 'es' )
                {
                    return "{user.fullname}.

Hemos recibido una solicitud para cambiar tu contraseña de {site_name}.

Puedes cambiar la contraseña entrando en [{password_recovery_url}]({password_recovery_url}).";
                }

                return "{user.fullname}.

You have requested the password recovery at site {site_name}.

To receive a new password, go to [{password_recovery_url}]({password_recovery_url}).";
            break;

            case 'welcome':
                if ( $language_id === 'es' )
                {
                    return "Hola {user.fullname}.

Tu cuenta ha sido creada correctamente. Puedes acceder introduciendo tu correo electrónico y contraseña en [{site_url}]({site_url}).

Thank you.";
                }

                return "Hello {user.fullname}.

Your account has been created. You can log in by using your email address and password by visiting our website [{site_url}]({site_url}).

Thank you.";
            break;
        }
    }
}

