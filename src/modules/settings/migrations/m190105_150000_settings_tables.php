<?php
/**
 * Migration class m190105_150000_settings_tables
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dz\modules\settings\models\Country;

class m190105_150000_settings_tables extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Create "language" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('language', true);

         $this->createTable('language', [
            'language_id' => $this->string(4)->notNull(),
            'name' => $this->string(64)->notNull(),
            'native' => $this->string(64),
            'prefix' => $this->string(16)->notNull(),
            'is_ltr_direction' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1),
            'is_default' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'weight' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(1),
            'disable_date' => $this->date(),
            'disable_uid' => $this->integer()->unsigned(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Primary key
        $this->addPrimaryKey(null, 'language', 'language_id');

        // Foreign keys
        $this->addForeignKey(null, 'language', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'language', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'language', ['disable_uid'], 'user_users', ['id'], 'CASCADE', null);

        // Insert default languages: English and Spanish
        $this->insertMultiple('language', [
            [
                'language_id'       => 'en',
                'name'              => 'English',
                'native'            => 'English',
                'prefix'            => 'en',
                'is_ltr_direction'  => 1,
                'is_default'        => 0,
                'weight'            => 2,
                'created_date'      => time(),
                'created_uid'       => 1,
                'updated_date'      => time(),
                'updated_uid'       => 1,
                'uuid'              => StringHelper::UUID()
            ],
            [
                'language_id'       => 'es',
                'name'              => 'Spanish',
                'native'            => 'Español',
                'prefix'            => 'es',
                'is_ltr_direction'  => 1,
                'is_default'        => 1,
                'weight'            => 1,
                'created_date'      => time(),
                'created_uid'       => 1,
                'updated_date'      => time(),
                'updated_uid'       => 1,
                'uuid'              => StringHelper::UUID()
            ],
        ]);


        // Create "currency" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('currency', true);

         $this->createTable('currency', [
            'currency_id' => $this->string(4)->notNull(),
            'name' => $this->string(64)->notNull(),
            'symbol' => $this->string(16)->notNull(),
            'numeric_code' => $this->smallInteger(4)->unsigned()->notNull(),
            'symbol_placement' => $this->enum('symbol_placement', ['after', 'before', 'hidden']),
            'minor_unit' => $this->string(16),
            'major_unit' => $this->string(16),
            'thousands_separator' => $this->string(16),
            'decimal_separator' => $this->string(16),
            'decimals_number' => $this->tinyInteger(2)->unsigned()->notNull()->defaultValue(2),
            'weight' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(1),
            'disable_date' => $this->date(),
            'disable_uid' => $this->integer()->unsigned(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Primary key
        $this->addPrimaryKey(null, 'currency', 'currency_id');

        // Foreign keys
        $this->addForeignKey(null, 'currency', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'currency', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'currency', ['disable_uid'], 'user_users', ['id'], 'CASCADE', null);

        // Insert default currency: Euro
        $this->insert('currency', [
            'currency_id'           => 'EUR',
            'name'                  => 'Euro',
            'symbol'                => '€',
            'numeric_code'          => 978,
            'symbol_placement'      => 'after',
            'minor_unit'            => 'Cent',
            'major_unit'            => NULL,
            'thousands_separator'   => '.',
            'decimal_separator'     => ',',
            'decimals_number'       => 2,
            'weight'                => 1,
            'created_date'          => time(),
            'created_uid'           => 1,
            'updated_date'          => time(),
            'updated_uid'           => 1,
            'uuid'                  => StringHelper::UUID()
        ]);


        // Create "country" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('country', true);

         $this->createTable('country', [
            'country_code' => $this->string(4)->notNull(),
            'name' => $this->string(64)->notNull(),
            'disable_date' => $this->date(),
            'disable_uid' => $this->integer()->unsigned(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

         // Primary key
        $this->addPrimaryKey(null, 'country', 'country_code');

        // Foreign keys
        $this->addForeignKey(null, 'country', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'country', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'country', ['disable_uid'], 'user_users', ['id'], 'CASCADE', null);

        // Insert all countries
        $this->_insert_countries();


        // Create "setting" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('setting', true);

         $this->createTable('setting', [
            'name' => $this->string(32)->notNull(),
            'value' => $this->string(255)->notNull(),
            'type' => $this->string(32)->notNull(),
            'title' => $this->string(255),
            'description' => $this->string(512),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

         // Primary key
        $this->addPrimaryKey(null, 'setting', 'name');

        // Foreign keys
        $this->addForeignKey(null, 'setting', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'setting', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);


        // Permissions
        // -------------------------------------------------------------------------
        $this->insertMultiple('user_auth_item', [
            [
                'name'          => 'settings_manage',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Settings - Full access to settings',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'settings.language.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Settings - Languages - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'settings.currency.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Settings - Currencies - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'settings.country.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Settings - Countries - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'settings.setting.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Settings - Setting Options - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
        ]);

        // Set items relationships
        $this->insertMultiple('user_auth_item_child', [
            [
                'parent'    => 'admin',
                'child'     => 'settings_manage'
            ],
            [
                'parent'    => 'settings_manage',
                'child'     => 'settings.language.*'
            ],
            [
                'parent'    => 'settings_manage',
                'child'     => 'settings.currency.*'
            ],
            [
                'parent'    => 'settings_manage',
                'child'     => 'settings.country.*'
            ],
            [
                'parent'    => 'settings_manage',
                'child'     => 'settings.setting.*'
            ],
        ]);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}


    /**
     * Insert all countries into "country" database table
     */
    private function _insert_countries()
    {
        $vec_countries = Country::model()->country_codes();
        asort($vec_countries);

        foreach ( $vec_countries as $country_code => $country_name )
        {
            $this->insert('country', [
                'country_code'  => $country_code,
                'name'          => $country_name,
                'created_uid'   => 1,
                'created_date'  => time(),
                'updated_uid'   => 1,
                'updated_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ]);
        }
    }
}

