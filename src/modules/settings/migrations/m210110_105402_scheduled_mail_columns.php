<?php
/**
 * Migration class m210110_105402_scheduled_mail_columns
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210110_105402_scheduled_mail_columns extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Add new columns to schedule an email
        $this->addColumn('mail_history', 'scheduled_date', $this->date()->after('template_alias'));
        $this->addColumn('mail_history', 'is_scheduled', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0)->after('scheduled_date'));

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

