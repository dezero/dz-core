<?php
/**
 * Migration class m190204_160000_mail_tables
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m190204_160000_mail_tables extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "mail_template" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('mail_template', true);

        $this->createTable('mail_template', [
            'template_id' => $this->primaryKey(),
            'alias' => $this->string(64)->notNull(),
            'language_id' => $this->string(4)->notNull()->defaultValue('es'),
            'name' => $this->string(128)->notNull(),
            'description' => $this->text(),
            'sender_name' => $this->string(255)->notNull(),
            'sender_email' => $this->string(255)->notNull(),
            'reply_email' => $this->string(255),
            'recipient_emails' => $this->string(255),
            'cc_emails' => $this->string(255),
            'bcc_emails' => $this->string(255),
            'source_type' => $this->enum('source_type', ['database', 'file'])->notNull()->defaultValue('database'),
            'subject' => $this->string(255)->notNull(),
            'template_file_path' => $this->string(255),
            'body' => $this->text(),
            'help_text' => $this->text(),
            'disable_date' => $this->date(),
            'disable_uid' => $this->integer()->unsigned(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);
    
        // Create indexes
        $this->createIndex(null, 'mail_template', ['alias'], false);
        $this->createIndex(null, 'mail_template', ['alias', 'language_id'], false);
        $this->createIndex(null, 'mail_template', ['source_type'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'mail_template', ['language_id'], 'language', ['language_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'mail_template', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'mail_template', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'mail_template', ['disable_uid'], 'user_users', ['id'], 'CASCADE', null);


        // Create "mail_history" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('mail_history', true);

        $this->createTable('mail_history', [
            'history_id' => $this->primaryKey(),
            'subject' => $this->string(255)->notNull(),
            'body' => $this->text(),
            'sender_name' => $this->string(255)->notNull(),
            'sender_email' => $this->string(255)->notNull(),
            'reply_email' => $this->string(255),
            'recipient_emails' => $this->string(255)->notNull(),
            'cc_emails' => $this->string(255),
            'bcc_emails' => $this->string(255),
            'recipient_uid' => $this->integer()->unsigned(),
            'eml_file_path' => $this->string(255),
            'language_id' => $this->string(4)->notNull()->defaultValue('es'),
            'template_id' => $this->integer()->unsigned(),
            'template_alias' => $this->string(64),
            'is_test' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'is_pending' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'is_error' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'error_description' => $this->string(255),
            'entity_id' => $this->integer()->unsigned(),
            'entity_type' => $this->string(32),
            'entity_scenario' => $this->string(32),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create indexes
        $this->createIndex(null, 'mail_history', ['is_pending'], false);
        $this->createIndex(null, 'mail_history', ['is_error'], false);
        $this->createIndex(null, 'mail_history', ['entity_id', 'entity_type'], false);
        $this->createIndex(null, 'mail_history', ['template_alias', 'language_id'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'mail_history', ['recipient_uid'], 'user_users', ['id'], 'SET NULL', null);
        $this->addForeignKey(null, 'mail_history', ['language_id'], 'language', ['language_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'mail_history', ['template_id'], 'mail_template', ['template_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'mail_history', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);

        
        // Permissions
        // -------------------------------------------------------------------------
        $this->insertMultiple('user_auth_item', [
            [
                'name'          => 'settings.mail.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Settings - Mail - Full access to email templates',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'settings.mail.create',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Settings - Mail - Create new email templates',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'settings.mail.update',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Settings - Mail - Edit email templates',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'settings.mail.view',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Settings - Mail - View email templates',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'mail_manage',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Settings - Full access to email templates',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'mail_edit',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Settings - Edit email templates',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'mail_view',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Settings - View email templates',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
        ]);

        // Set items relationships
        $this->insertMultiple('user_auth_item_child', [
            [
                'parent'    => 'mail_manage',
                'child'     => 'settings.mail.*'
            ],
            [
                'parent'    => 'mail_edit',
                'child'     => 'settings.mail.update'
            ],
            [
                'parent'    => 'mail_edit',
                'child'     => 'settings.mail.view'
            ],
            [
                'parent'    => 'mail_view',
                'child'     => 'settings.mail.view'
            ],
            [
                'parent'    => 'admin',
                'child'     => 'mail_manage'
            ]
        ]);


		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

