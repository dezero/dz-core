<?php
/**
 * Migration class m210421_130511_setting_value_null
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210421_130511_setting_value_null extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Allow "value" column to recieve NULL values
        $this->alterColumn('setting', 'value', $this->string(255));

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

