<?php
/**
 * Migration class m210305_161616_status_history_table
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210305_161616_status_history_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Create "status_history" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('status_history', true);

        $this->createTable('status_history', [
            'history_id' => $this->primaryKey(),
            'entity_id' => $this->integer()->unsigned()->notNull(),
            'entity_type' => $this->string(32)->notNull(),
            'entity_scenario' => $this->string(32),
            'status_type' => $this->string(32)->notNull(),
            'mail_template_id' => $this->integer()->unsigned(),
            'mail_alias' => $this->string(64),
            'comments' => $this->text(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull()
        ]);

        // Create indexes
        $this->createIndex(null, 'status_history', ['entity_id', 'entity_type'], false);
        $this->createIndex(null, 'status_history', ['entity_type'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'status_history', ['mail_template_id'], 'mail_template', ['template_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'status_history', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('status_history');
		return false;
	}
}

