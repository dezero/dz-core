<?php
/**
 * Migration class m210129_150907_new_status_mail_columns
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210129_150907_new_status_mail_columns extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Add new "status" columns
        $this->addColumn('mail_history', 'status_type', $this->enum('content_type', ['pending', 'scheduled', 'sent', 'error'])->defaultValue('sent')->after('template_alias'));
        $this->addColumn('mail_history', 'send_date', $this->date()->after('status_type'));

        // Add "updated" information columns
        $this->addColumn('mail_history', 'updated_date', $this->date()->notNull()->after('created_uid'));
        $this->addColumn('mail_history', 'updated_uid', $this->integer()->unsigned()->notNull()->after('updated_date'));

        // Remove unnecessary columns
        $this->dropColumn('mail_history', 'is_scheduled');
        $this->dropColumn('mail_history', 'is_pending');
        $this->dropColumn('mail_history', 'is_error');

        // Create index
        $this->createIndex(null, 'mail_history', ['status_type'], false);

        // Foreign key
        $this->addForeignKey(null, 'mail_history', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

        // Use "created_date" as "send_date" and "updated_date" for all the existing emails
        Yii::app()->db->createCommand('UPDATE mail_history SET send_date = created_date')->execute();
        Yii::app()->db->createCommand('UPDATE mail_history SET updated_date = created_date')->execute();
        Yii::app()->db->createCommand('UPDATE mail_history SET updated_uid = created_uid')->execute();

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

