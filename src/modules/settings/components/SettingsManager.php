<?php

namespace dz\modules\settings\components;

use dz\base\ApplicationComponent;
use dz\modules\settings\models\Setting;
use Yii;

/**
 * SettingsManager to manage Settings models
 */
class SettingsManager extends ApplicationComponent
{
	 /**
     * Init function
     */
    public function init()
    {
        parent::init();
    }
    

    /**
     * Get a setting value by name
     */
    public function get($name)
    {
    	$setting_model = Setting::model()->findByPk($name);
    	if ( $setting_model )
    	{
    		return $setting_model->value;
    	}

    	return '';
    }


    /**
     * Set a setting value by name
     */
    public function set($name, $value)
    {
    	$setting_model = Setting::model()->findByPk($name);
    	if ( $setting_model )
    	{
    		$setting_model->value = $value;
    		return $setting_model->save();
    	}

    	return false;
    }


    /**
     * Save multiple Setting models
     */
    public function save_settings($vec_input, $vec_settings_models = [])
    {
        $is_error = false;

        if ( !empty($vec_settings_models) )
        {
            foreach ( $vec_input as $settings_name => $vec_attributes )
            {
                if ( isset($vec_settings_models[$settings_name]) )
                {
                    $vec_settings_models[$settings_name]->setAttributes($vec_attributes);
                    if ( ! $vec_settings_models[$settings_name]->save() )
                    {
                        $is_error = true;
                    }
                }
            }
        }

        return ! $is_error;
    }
}