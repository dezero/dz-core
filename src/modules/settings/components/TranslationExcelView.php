<?php
/**
 * Special override of CGridView for EExcelView
 */

// namespace dz\modules\settings\components;

use dz\batch\Batch;
// use Yii;

Yii::import('@dz.utils.excel.ExcelView');

class TranslationExcelView extends ExcelView
{
    /**
     * Excel sheets - DEZERO extension
     */
    public $sheets = [];


    /**
     * Custom AfterRender event
     */
    public $afterRender = 'afterRender'; // '$this->afterRender';


    /**
     * Init callback
     */
    public function init()
    {
        parent::init();
    }


    /**
     * After render callback
     */
    public function afterRender($total_rows, $num_sheet)
    {
        foreach( $this->sheets as $num_sheet => $que_sheet )
        {
            // Activate current sheet
            $this->objPHPExcel->setActiveSheetIndex($num_sheet);

            $last_row = $this->objPHPExcel->getActiveSheet()->getHighestRow();
            $last_column = $this->objPHPExcel->getActiveSheet()->getHighestColumn();
            $full_worksheet_dimension = $this->objPHPExcel->getActiveSheet()->calculateWorksheetDimension();

            // Header in bold
            $this->objPHPExcel->getActiveSheet()->getStyle('A1:'. $last_column .'1')->getFont()->setBold(TRUE);

            // Vertical align
            $this->objPHPExcel->getActiveSheet()->getStyle($full_worksheet_dimension)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

            // Freeze columns "A2" and "C1" - Header and first column fixed
            // $this->objPHPExcel->getActiveSheet()->freezePane('B2');

            // Apply column formats
            $this->apply_column_formats();

            // Row height
            $this->objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);

            // Text Wrapping on header
            $this->objPHPExcel->getActiveSheet()->getStyle('A1:'. $last_column .'1')->getAlignment()->setWrapText(TRUE);
        }

        // We want first sheet selected when user opens Excel for the first time
        $this->objPHPExcel->setActiveSheetIndex(0);
    }
}
