<?php
/**
 * I18nCommand class file
 *
 * This command is used to work with translations
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2020 Fabián Ruiz
 * @package commands
 */

namespace dz\modules\settings\commands;

use dz\console\CronCommand;
use dz\helpers\Log;
use dz\modules\settings\models\TranslationMessage;
use dz\modules\settings\models\TranslationSource;
use Yii;

class I18nCommand extends CronCommand
{
    /**
     * Load all translations from a FILE to DATABASE
     *
     * ./yiic i18n loadTranslationFile --file_path=/app/messages/es/app.php
     */
    public function actionLoadTranslationFile($file_path)
    {
        // Ensure file path starts with "/"
        if ( !preg_match("/^\//", $file_path) )
        {
            $file_path = "/". $file_path;
        }

        $full_file_path = Yii::app()->path->basePath() . $file_path;
        if ( ! file_exists($full_file_path) )
        {
            echo "ERROR - Given file path does not exist: ". $full_file_path ."\n";
        }

        // Get language and category from the file path
        $vec_file_path = explode("/", $file_path);

        $language_id = $vec_file_path[count($vec_file_path) - 2];
        echo " -> Language detected: {$language_id}\n";

        $translation_category = str_replace(".php", "", $vec_file_path[count($vec_file_path) - 1]);
        echo  " -> Translation category detected: {$translation_category}\n";

        // Get PHP file
        $vec_translations = require_once $full_file_path;

        if ( !empty($vec_translations) )
        {
            $total_detected_translations = count($vec_translations);

            echo " -> Loading {$total_detected_translations} translation messages...\n\n";

            $total_saved_translations = 0;
            foreach ( $vec_translations as $source_message => $translation_message )
            {
                // TranslationSource
                $translation_source_model = TranslationSource::get()
                    ->where([
                        'category'  => $translation_category,
                        'message'   => $source_message
                    ])
                    ->one();
                if ( ! $translation_source_model )
                {
                    $translation_source_model = Yii::createObject(TranslationSource::class);
                    $translation_source_model->category = $translation_category;
                    $translation_source_model->message = $source_message;
                    if ( ! $translation_source_model->save() )
                    {
                        Log::save_model_error($translation_source_model);

                        // Skip this "foreach" iteration and go to next one
                        continue;
                    }
                }


                $translation_id = $translation_source_model->id;
                if ( !empty($translation_id) && $translation_source_model )
                {
                    if ( ! $translation_source_model->save_translation($language_id, $translation_message) )
                    {
                        echo "* Error saving translation for '{$source_message}' message to '{$translation_message}'\n";
                    }
                    else
                    {
                        $total_saved_translations++;
                    }
                }
            }

            echo " -> Saved {$total_saved_translations} translation messages into database\n\n";
        }
    }


    /**
     * Copy data from a "default" table to a "translated" table
     *
     * ./yiic i18n copyTranslations --model_class=category --language_id=en
     * ./yiic i18n copyTranslations --model_class=product --language_id=en
     * ./yiic i18n copyTranslations --model_class=productOption --language_id=en
     */
    public function actionCopyTranslations($model_class, $language_id = 'en')
    {
        switch ( $model_class )
        {
            // From Category model to TranslatedCategory model
            case 'category':
                $vec_category_types = Yii::app()->config->get('components.categories');
                if ( !empty($vec_category_types) )
                {
                    foreach ( $vec_category_types as $category_type => $vec_category_options )
                    {
                        if ( isset($vec_category_options['is_multilanguage']) && $vec_category_options['is_multilanguage'] )
                        {
                            echo "Category type '". $category_type ."' has multilanguage option enabled. Copying translations...\n";
                            $num_copied_translations = Yii::app()->categoryManager->copy_translations($category_type, $language_id);
                            if ( $num_copied_translations > 0 )
                            {
                                echo "--> Copied ". $num_copied_translations ." translations from {category} table to {translated_category_model} table\n\n";
                            }
                            else
                            {
                                echo "--> NO translations have been found to be copied\n\n";
                            }
                        }
                    }
                }
            break;

            // From Product model to TranslatedProduct model
            case 'product':
                $vec_product_types = Yii::app()->config->get('components.products');
                if ( !empty($vec_product_types) )
                {
                    foreach ( $vec_product_types as $product_type => $vec_product_options )
                    {
                        if ( isset($vec_product_options['is_multilanguage']) && $vec_product_options['is_multilanguage'] )
                        {
                            echo "Product type '". $product_type ."' has multilanguage option enabled. Copying translations...\n";
                            $num_copied_translations = Yii::app()->productManager->copy_translations($product_type, $language_id);
                            if ( $num_copied_translations > 0 )
                            {
                                echo "--> Copied ". $num_copied_translations ." translations from {commerce_product} table to {commerce_translated_product} table\n\n";
                            }
                            else
                            {
                                echo "--> NO translations have been found to be copied\n\n";
                            }
                        }
                    }
                }
            break;

            // From ProductOption model to TranslatedProductOption model
            case 'productOption':
                $vec_option_types = Yii::app()->config->get('components.product_options');
                if ( !empty($vec_option_types) )
                {
                    foreach ( $vec_option_types as $option_type => $vec_product_options )
                    {
                        if ( isset($vec_product_options['is_multilanguage']) && $vec_product_options['is_multilanguage'] )
                        {
                            echo "Product option type '". $option_type ."' has multilanguage option enabled. Copying translations...\n";
                            $num_copied_translations = Yii::app()->productOptionManager->copy_translations($option_type, $language_id);
                            if ( $num_copied_translations > 0 )
                            {
                                echo "--> Copied ". $num_copied_translations ." translations from {commerce_product_option} table to {commerce_translated_product_option} table\n\n";
                            }
                            else
                            {
                                echo "--> NO translations have been found to be copied\n\n";
                            }
                        }
                    }
                }
            break;
        }
    }
}
