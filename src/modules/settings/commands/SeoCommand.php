<?php
/**
 * SeoCommand class file
 *
 * This command is used to work with SEO models
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2021 Fabián Ruiz
 * @package commands
 */

namespace dz\modules\settings\commands;

use dz\console\CronCommand;
use dz\db\DbCriteria;
use dz\modules\category\models\Category;
use dz\modules\settings\models\Language;
use dz\modules\web\models\Seo;
use dz\modules\web\models\WebContent;
use dzlab\commerce\models\Product;
use Yii;

class SeoCommand extends CronCommand
{
    /**
     * Check data consistency
     *
     * ./yiic seo inspector --model_class=category
     */
    public function actionInspector($model_class)
    {
        switch ( $model_class )
        {
            // Check data consistency on Category models
            case 'category':
                // Filter by categories with "is_seo_fields" option enabled
                $vec_category_types = Yii::app()->config->get('components.categories');
                if ( !empty($vec_category_types) )
                {
                    foreach ( $vec_category_types as $category_type => $vec_category_config )
                    {
                        if ( !isset($vec_category_config['is_seo_fields']) || $vec_category_config['is_seo_fields'] === false )
                        {
                            unset($vec_category_types[$category_type]);
                        }
                    }
                }

                $criteria = new DbCriteria;
                $criteria->compare('t.entity_type', 'category');
                $vec_seo_models = Seo::model()->findAll($criteria);
                if ( !empty($vec_seo_models) )
                {
                    foreach ( $vec_seo_models as $seo_model )
                    {
                        $category_model = Category::findOne($seo_model->entity_id);
                        if ( ! $category_model )
                        {
                            echo "ERROR - Category #{$category_model->category_id} - It doesn't exist but it has a SEO model related with it.\n";
                        }
                        else if ( !isset($vec_category_types[$category_model->category_type]) )
                        {
                            echo "ERROR - Category #{$category_model->category_id} - It exists a SEO model for category type ". $category_model->category_type ." but category_type has not enabled 'is_seo_fields' option.\n";
                        }
                    }
                }
            break;
        }
    }
}