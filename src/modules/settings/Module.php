<?php
/**
 * Module to manage settings options
 */

namespace dz\modules\settings;

class Module extends \dz\web\Module
{
    /**
     * @var array mapping from controller ID to controller configurations.
     */
    public $controllerMap = [
        'country' => [
            'class' => 'dz\modules\settings\controllers\CountryController',
        ],
        'currency' => [
            'class' => 'dz\modules\settings\controllers\CurrencyController',
        ],
        'language' => [
            'class' => 'dz\modules\settings\controllers\LanguageController',
        ],
        'mail' => [
            'class' => 'dz\modules\settings\controllers\MailController',
        ],
        'setting' => [
            'class' => 'dz\modules\settings\controllers\SettingController',
        ],
        'translation' => [
            'class' => 'dz\modules\settings\controllers\TranslationController',
        ],
        'translationExport' => [
            'class' => 'dz\modules\settings\controllers\TranslationExportController',
        ],
        'translationImport' => [
            'class' => 'dz\modules\settings\controllers\TranslationImportController',
        ],
    ];

    /**
     * Default controller
     */
    public $defaultController = 'language';


    /**
     * Load specific CSS or JS files for this module
     */
    public $cssFiles = null; // ['settings.css'];
    public $jsFiles = null; // ['settings.js'];


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        parent::init();
    }
}
