<?php
/**
 * @package dz\modules\settings\models 
 */

namespace dz\modules\settings\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dz\modules\settings\models\_base\Currency as BaseCurrency;
use dz\modules\settings\helpers\CurrencyHelper;
use user\models\User;
use Yii;

/**
 * Currency model class for "currency" database table
 *
 * Columns in table "currency" available as properties of the model,
 * followed by relations of table "currency" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property string $currency_id
 * @property string $name
 * @property string $symbol
 * @property integer $numeric_code
 * @property string $symbol_placement
 * @property string $minor_unit
 * @property string $major_unit
 * @property string $thousands_separator
 * @property string $decimal_separator
 * @property integer $decimals_number
 * @property integer $weight
 * @property integer $disable_date
 * @property integer $disable_uid
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $disableUser
 * @property mixed $updatedUser
 */
class Currency extends BaseCurrency
{
    const SYMBOL_AFTER = 'after';
    const SYMBOL_BEFORE = 'before';
    const SYMBOL_HIDDEN = 'hidden';

	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['currency_id, name, symbol, numeric_code, created_date, created_uid, updated_date, updated_uid', 'required'],
			['numeric_code, decimals_number, weight, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['currency_id', 'length', 'max'=> 4],
			['name', 'length', 'max'=> 64],
			['symbol, minor_unit, major_unit, thousands_separator, decimal_separator', 'length', 'max'=> 16],
			['uuid', 'length', 'max'=> 36],
			['symbol_placement', 'in', 'range' => [self::SYMBOL_AFTER, self::SYMBOL_BEFORE, self::SYMBOL_HIDDEN]],
			['symbol_placement, minor_unit, major_unit, thousands_separator, decimal_separator, decimals_number, weight, disable_date, disable_uid, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['currency_id, name, symbol, numeric_code, symbol_placement, minor_unit, major_unit, thousands_separator, decimal_separator, decimals_number, weight, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid, uuid, disable_filter', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'disableUser' => [self::BELONGS_TO, User::class, ['disable_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],
        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'currency_id' => Yii::t('app', 'Moneda'),
			'name' => Yii::t('app', 'Nombre'),
            'symbol' => Yii::t('app', 'Símbolo'),
            'numeric_code' => Yii::t('app', 'Código numérico'),
            'symbol_placement' => Yii::t('app', 'Posición del símbolo'),
            'minor_unit' => Yii::t('app', 'Nombre unidad mayor'),
            'major_unit' => Yii::t('app', 'Nombre unidad menor'),
            'thousands_separator' => Yii::t('app', 'Separador de miles'),
            'decimal_separator' => Yii::t('app', 'Separador de decimales'),
            'decimals_number' => Yii::t('app', 'Número de decimales'),
			'weight' => Yii::t('app', 'Weight'),
			'disable_date' => Yii::t('app', 'Disable Date'),
			'disable_uid' => null,
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'createdUser' => null,
			'disableUser' => null,
			'updatedUser' => null,
		];
	}


    /**
     * Get "symbol_placement" labels
     */
    public function symbol_placement_labels()
    {
        return [
            self::SYMBOL_AFTER => Yii::t('app', 'Después'),
            self::SYMBOL_BEFORE => Yii::t('app', 'Antes'),
            self::SYMBOL_HIDDEN => Yii::t('app', 'Oculto'),
        ];
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.currency_id', $this->currency_id, true);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.numeric_code', $this->numeric_code);

        // $criteria->compare('t.symbol', $this->symbol, true);
        // $criteria->compare('t.symbol_placement', $this->symbol_placement, true);
        // $criteria->compare('t.minor_unit', $this->minor_unit, true);
        // $criteria->compare('t.major_unit', $this->major_unit, true);
        // $criteria->compare('t.thousands_separator', $this->thousands_separator, true);
        // $criteria->compare('t.decimal_separator', $this->decimal_separator, true);
        // $criteria->compare('t.decimals_number', $this->decimals_number);
        // $criteria->compare('t.weight', $this->weight);
        // $criteria->compare('t.disable_date', $this->disable_date);
        // $criteria->compare('t.created_date', $this->created_date);
        // $criteria->compare('t.updated_date', $this->updated_date);
        // $criteria->compare('t.uuid', $this->uuid, true);

        // Filter just DISABLED currencies
        if ( $this->disable_filter == 1 )
        {
            $criteria->addCondition('t.disable_date IS NOT NULL AND t.disable_date > 0');
        }

        // Filter just ENABLED currencies
        else if ( $this->disable_filter == 2 )
        {
            $criteria->addCondition('t.disable_date IS NULL OR t.disable_date = 0');
        }

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            // 'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['weight' => false]]
        ]);
    }


    /**
     * Currency models list
     * 
     * @return array
     */
     public function currency_list()
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['currency_id', 'name'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = Currency::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('currency_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Add a new currency to platform
     */
    public function add_currency($currency_id)
    {
        $vec_currencys = CurrencyHelper::currency_list(false);
        if ( isset($vec_currencys[$currency_id]) )
        {
            // Currency attributes
            $this->currency_id = $currency_id;
            $this->weight = 1;

            // More attributes
            $que_currency = $vec_currencys[$currency_id];
            $this->name = $que_currency['name'];
            $this->symbol = isset($que_currency['symbol']) ? $que_currency['symbol'] : '';
            $this->numeric_code = $que_currency['numeric_code'];
            $this->minor_unit = isset($que_currency['minor_unit']) ? $que_currency['minor_unit'] : NULL;
            $this->thousands_separator = isset($que_currency['thousands_separator']) ? $que_currency['thousands_separator'] : NULL;
            $this->decimal_separator = isset($que_currency['decimal_separator']) ? $que_currency['decimal_separator'] : NULL;
            $this->decimals_number = isset($que_currency['decimals_number']) ? $que_currency['decimals_number'] : 2;
            $this->symbol_placement = isset($que_currency['symbol_placement']) ? $que_currency['symbol_placement'] : NULL;

            return true;
        }
        return false;
    }



    /**
     * Returns a simple array with currency information
     */
    public function currency_full_list($is_most_important = true)
    {
        // Get all currencys
        $vec_currencies = CurrencyHelper::currency_list($is_most_important);

        // Remove existing currencys
        $vec_currency_models = Currency::model()->findAll();
        if ( !empty($vec_currency_models) )
        {
            foreach ( $vec_currency_models as $currency_model )
            {
                if ( isset($vec_currencies[$currency_model->currency_id]) )
                {
                    unset($vec_currencies[$currency_model->currency_id]);
                }
            }
        }

        foreach ( $vec_currencies as $currency_code => $que_currency )
        {
            $vec_currencies[$currency_code] = $currency_code .' - '. $que_currency['name'];
            if ( isset($que_currency['symbol']) AND !empty($que_currency['symbol']) )
            {
                $vec_currencies[$currency_code] .= ' - '. $que_currency['symbol'];
            }
        }
        return $vec_currencies;
    }


    /**
     * Title for this model
     */
    public function title()
    {
        $title = $this->currency_id .' - '. $this->name;
        if ( $this->symbol )
        {
            $title .= ' - '. $this->symbol;
        }
        return $title;
    }
}