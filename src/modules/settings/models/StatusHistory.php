<?php
/**
 * @package dz\modules\settings\models 
 */

namespace dz\modules\settings\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\settings\models\_base\StatusHistory as BaseStatusHistory;
use dz\modules\settings\models\MailTemplate;
use user\models\User;
use Yii;

/**
 * StatusHistory model class for "status_history" database table
 *
 * Columns in table "status_history" available as properties of the model,
 * followed by relations of table "status_history" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $history_id
 * @property integer $entity_id
 * @property string $entity_type
 * @property string $entity_scenario
 * @property string $status_type
 * @property integer $mail_template_id
 * @property string $mail_alias
 * @property string $comments
 * @property integer $created_date
 * @property integer $created_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $mailTemplate
 */
class StatusHistory extends BaseStatusHistory
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['entity_id, entity_type, status_type, created_date, created_uid', 'required'],
			['entity_id, mail_template_id, created_date, created_uid', 'numerical', 'integerOnly' => true],
			['entity_type, entity_scenario, status_type', 'length', 'max'=> 32],
			['mail_alias', 'length', 'max'=> 64],
			['entity_scenario, mail_template_id, mail_alias, comments', 'default', 'setOnEmpty' => true, 'value' => null],
			['comments', 'safe'],
			['history_id, entity_id, entity_type, entity_scenario, status_type, mail_template_id, mail_alias, comments, created_date, created_uid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'mailTemplate' => [self::BELONGS_TO, MailTemplate::class, 'mail_template_id'],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'history_id' => Yii::t('app', 'History'),
			'entity_id' => Yii::t('app', 'Entity'),
			'entity_type' => Yii::t('app', 'Entity Type'),
			'entity_scenario' => Yii::t('app', 'Entity Scenario'),
			'status_type' => Yii::t('app', 'Status Type'),
			'mail_template_id' => null,
			'mail_alias' => Yii::t('app', 'Mail Alias'),
			'comments' => Yii::t('app', 'Comments'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'createdUser' => null,
			'mailTemplate' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.history_id', $this->history_id);
        $criteria->compare('t.entity_id', $this->entity_id);
        $criteria->compare('t.entity_type', $this->entity_type, true);
        $criteria->compare('t.entity_scenario', $this->entity_scenario, true);
        $criteria->compare('t.status_type', $this->status_type, true);
        $criteria->compare('t.mail_alias', $this->mail_alias, true);
        $criteria->compare('t.comments', $this->comments, true);
        $criteria->compare('t.created_date', $this->created_date);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['history_id' => true]]
        ]);
    }


    /**
     * StatusHistory models list
     * 
     * @return array
     */
    public function statushistory_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['history_id', 'entity_type'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = StatusHistory::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('history_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}