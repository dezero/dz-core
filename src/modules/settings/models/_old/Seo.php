<?php
/**
 * @file models/Seo.php
 * Seo model class for "seo" database table
 *
 * Columns in table "seo" available as properties of the model,
 * followed by relations of table "seo" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property string $entity_id
 * @property string $entity_type
 * @property string $language_id
 * @property string $url_auto
 * @property integer $url_counter
 * @property string $url_manual
 * @property string $meta_title
 * @property string $meta_description
 * @property string $created_date
 * @property string $created_uid
 * @property string $updated_date
 * @property string $updated_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property Language $language
 * @property User $createdUser
 * @property User $updatedUser
 */

use product\models\Product;
use product\models\TranslatedProduct;

// Import model base class for the table "seo"
Yii::import('config.models._base.BaseSeo');

class Seo extends BaseSeo
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return array(
			array('entity_id, entity_type, language_id, created_date, created_uid, updated_date, updated_uid', 'required'),
			array('url_counter', 'numerical', 'integerOnly'=>true),
			array('entity_id, created_date, created_uid, updated_date, updated_uid', 'length', 'max'=>10),
			array('entity_type', 'length', 'max'=>16),
			array('language_id', 'length', 'max'=>4),
			array('url_auto, url_manual', 'length', 'max'=>255),
			array('meta_title', 'length', 'max'=>128),
			array('meta_description', 'length', 'max'=>512),
			array('url_auto, url_counter, url_manual, meta_title, meta_description', 'default', 'setOnEmpty' => true, 'value' => null),
			array('entity_id, entity_type, language_id, url_auto, url_counter, url_manual, meta_title, meta_description, created_date, created_uid, updated_date, updated_uid', 'safe', 'on'=>'search'),
		);
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return array(
			'language' => array(self::BELONGS_TO, 'Language', 'language_id'),
			'createdUser' => array(self::BELONGS_TO, 'User', array('created_uid' => 'id')),
			'updatedUser' => array(self::BELONGS_TO, 'User', array('updated_uid' => 'id')),
		);
	}
	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return array(
			'entity_id' => Yii::t('app', 'Entity'),
			'entity_type' => Yii::t('app', 'Entity Type'),
			'language_id' => null,
			'url_auto' => Yii::t('app', 'Url Auto'),
			'url_counter' => Yii::t('app', 'Url Counter'),
			'url_manual' => Yii::t('app', 'URL'),
			'meta_title' => Yii::t('app', 'Meta Título'),
			'meta_description' => Yii::t('app', 'Meta Descripción'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'language' => null,
			'createdUser' => null,
			'updatedUser' => null,
		);
	}
	
	
	/*
	|--------------------------------------------------------------------------
	| CUSTOM FUNCTIONS
	|--------------------------------------------------------------------------
	*/

	/**
	 * Event "afterValidate"
	 *
	 * This method is invoked after validation end
	 */
	public function afterValidate()
    {
    	if ( !empty($this->entity_id) )
    	{
	    	// Generate AUTO URL and meta-title
	    	switch ( $this->entity_type )
	    	{
	    	
	    		case 'category':
		    		$category_model = Category::model()->findByPk($this->entity_id);
		    		if ( $category_model )
		    		{
		    			// URL
		    			$this->url_auto = $category_model->get_seo_url_auto($this->language_id);
		    			if ( empty($this->url_manual) )
		    			{
		    				$this->url_manual = $this->url_auto;
		    			}

		    			// Meta-title
		    			if ( empty($this->meta_title) )
		    			{
		    				$this->meta_title = $category_model->name;
		    				if ( $this->language_id != 'es' )
		    				{
		    					$this->meta_title = NULL;
		    					$translated_category_model = TranslatedCategory::model()->findByAttributes(array(
									'category_id'	=> $this->entity_id,
									'language_id'	=> $this->language_id
								));
								if ( $translated_category_model )
								{
									$this->meta_title = $translated_category_model->name;	
								}
		    				}
		    			}

		    			// Meta-description
		    			if ( empty($this->meta_description) )
		    			{
		    				$this->meta_description = $category_model->description;
		    				if ( $this->language_id != 'es' )
		    				{
		    					$this->meta_title = NULL;
		    					$translated_category_model = TranslatedCategory::model()->findByAttributes(array(
									'category_id'	=> $this->entity_id,
									'language_id'	=> $this->language_id
								));
								if ( $translated_category_model )
								{
									$this->meta_description = $translated_category_model->description;	
								}
		    				}
		    			}
		    		}
	    		break;

	    		// ECOMMERCE PRODUCTS - Generate AUTO URL and meta-title
	    		case 'product':
		    		$product_model = Product::model()->findByPk($this->entity_id);
		    		if ( $product_model )
		    		{
		    			// URL
		    			$this->url_auto = $product_model->get_seo_url_auto($this->language_id);
		    			if ( empty($this->url_manual) )
		    			{
		    				$this->url_manual = $this->url_auto;
		    			}

		    			// Meta-title
		    			if ( empty($this->meta_title) )
		    			{
		    				$this->meta_title = $product_model->name;
		    				if ( $this->language_id != 'es' )
		    				{
		    					$this->meta_title = NULL;
		    					$translated_product_model = TranslatedProduct::model()->findByAttributes(array(
									'product_id'	=> $this->entity_id,
									'language_id'	=> $this->language_id
								));
								if ( $translated_product_model )
								{
									$this->meta_title = $translated_product_model->name;	
								}
		    				}
		    			}
		    		}
		    	break;

		    	// Web content: Pages
		    	case 'content_page':
		    		$web_content_model = WebContent::model()->findByPk($this->entity_id);
		    		if ( $web_content_model )
		    		{
		    			// URL
		    			$this->url_auto = $web_content_model->get_seo_url_auto();
		    			if ( empty($this->url_manual) )
		    			{
		    				$this->url_manual = $this->url_auto;
		    			}

		    			// Meta-title
		    			if ( empty($this->meta_title) )
		    			{
		    				$this->meta_title = $web_content_model->title;
		    			}
		    		}
		    	break;
	    	}
	    }

		return parent::afterValidate();
    }
}