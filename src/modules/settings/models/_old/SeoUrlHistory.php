<?php
/**
 * @file models/SeoUrlHistory.php
 * SeoUrlHistory model class for "seo_url_history" database table
 *
 * Columns in table "seo_url_history" available as properties of the model,
 * followed by relations of table "seo_url_history" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property string $url_history_id
 * @property string $entity_id
 * @property string $entity_type
 * @property string $language_id
 * @property string $url
 * @property string $created_date
 * @property string $created_uid
 * @property string $updated_date
 * @property string $updated_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property Language $language
 * @property User $createdUser
 * @property User $updatedUser
 */

// Import model base class for the table "seo_url_history"
Yii::import('config.models._base.BaseSeoUrlHistory');

class SeoUrlHistory extends BaseSeoUrlHistory
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return array(
			array('entity_id, entity_type, language_id, url, created_date, created_uid, updated_date, updated_uid', 'required'),
			array('entity_id, created_date, created_uid, updated_date, updated_uid', 'length', 'max'=>10),
			array('entity_type', 'length', 'max'=>16),
			array('language_id', 'length', 'max'=>4),
			array('url', 'length', 'max'=>255),
			array('url_history_id, entity_id, entity_type, language_id, url, created_date, created_uid, updated_date, updated_uid', 'safe', 'on'=>'search'),
		);
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return array(
			'language' => array(self::BELONGS_TO, 'Language', 'language_id'),
			'createdUser' => array(self::BELONGS_TO, 'User', array('created_uid' => 'id')),
			'updatedUser' => array(self::BELONGS_TO, 'User', array('updated_uid' => 'id')),
		);
	}
	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return array(
			'url_history_id' => Yii::t('app', 'Url History'),
			'entity_id' => Yii::t('app', 'Entity'),
			'entity_type' => Yii::t('app', 'Entity Type'),
			'language_id' => null,
			'url' => Yii::t('app', 'Url'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'language' => null,
			'createdUser' => null,
			'updatedUser' => null,
		);
	}
	
	
	/*
	|--------------------------------------------------------------------------
	| CUSTOM FUNCTIONS
	|--------------------------------------------------------------------------
	*/
}