<?php
/**
 * @file models/MailHistory.php
 * MailHistory model class for "mail_history" database table
 *
 * Columns in table "mail_history" available as properties of the model,
 * followed by relations of table "mail_history" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property string $mail_history_id
 * @property string $subject
 * @property string $body
 * @property string $sender_mail
 * @property string $recipient_mails
 * @property string $recipient_uid
 * @property string $language_id
 * @property string $file_name_eml
 * @property integer $is_pending
 * @property integer $is_error
 * @property string $error_description
 * @property string $template_id
 * @property string $template_alias
 * @property string $order_id
 * @property string $created_date
 * @property string $created_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property User $recipientUser
 * @property Language $language
 * @property MailTemplate $template
 * @property Order $order
 * @property User $createdUser
 */

use dz\helpers\Log;
use dz\components\Mailer;

// Import model base class for the table "mail_history"
Yii::import('config.models._base.BaseMailHistory');

class MailHistory extends BaseMailHistory
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return array(
			array('subject, body, sender_mail, recipient_mails, created_date, created_uid', 'required'),
			array('is_pending, is_error', 'numerical', 'integerOnly'=>true),
			array('subject, recipient_mails, error_description', 'length', 'max'=>255),
			array('sender_mail', 'length', 'max'=>100),
			array('recipient_uid, template_id, order_id, created_date, created_uid', 'length', 'max'=>10),
			array('language_id', 'length', 'max'=>4),
			array('file_name_eml, template_alias', 'length', 'max'=>64),
			array('recipient_uid, language_id, file_name_eml, is_pending, is_error, error_description, template_id, template_alias, order_id', 'default', 'setOnEmpty' => true, 'value' => null),
			array('mail_history_id, subject, body, sender_mail, recipient_mails, recipient_uid, language_id, file_name_eml, is_pending, is_error, error_description, template_id, template_alias, order_id, created_date, created_uid', 'safe', 'on'=>'search'),
		);
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return array(
			'recipientUser' => array(self::BELONGS_TO, 'User', array('recipient_uid' => 'id')),
			'language' => array(self::BELONGS_TO, 'Language', 'language_id'),
			'template' => array(self::BELONGS_TO, 'MailTemplate', 'template_id'),
			'order' => array(self::BELONGS_TO, 'Order', 'order_id'),
			'createdUser' => array(self::BELONGS_TO, 'User', array('created_uid' => 'id')),
		);
	}
	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return array(
			'mail_history_id' => Yii::t('app', 'Mail History'),
			'subject' => Yii::t('app', 'Subject'),
			'body' => Yii::t('app', 'Body'),
			'sender_mail' => Yii::t('app', 'Sender Mail'),
			'recipient_mails' => Yii::t('app', 'Recipient Mails'),
			'recipient_uid' => null,
			'language_id' => null,
			'file_name_eml' => Yii::t('app', 'File Name Eml'),
			'is_pending' => Yii::t('app', 'Is Pending'),
			'is_error' => Yii::t('app', 'Is Error'),
			'error_description' => Yii::t('app', 'Error Description'),
			'template_id' => null,
			'template_alias' => Yii::t('app', 'Template Alias'),
			'order_id' => null,
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'recipientUser' => null,
			'language' => null,
			'template' => null,
			'order' => null,
			'createdUser' => null,
		);
	}
	
	
	/*
	|--------------------------------------------------------------------------
	| CUSTOM FUNCTIONS
	|--------------------------------------------------------------------------
	*/


	/**
	 * Sends the email
	 */
	public function send_email()
	{
		if ( !empty($this->recipient_mails) AND !empty($this->subject) AND !empty($this->body) )
		{
			$mailer = new Mailer();
			$mailer->setTo($this->recipient_mails);
			$mailer->setView('commerce--notification');
			$mailer->setSubject($this->subject);

			// Recover "vec_mail_attributes" data. It's enconded in JSON format on body field
			$vec_mail_attributes = CJSON::decode($this->body);

			// Recover user_model
			if ( isset($vec_mail_attributes['user_model']) AND isset($vec_mail_attributes['user_model']['id']) )
			{
				$user_model = User::model()->findByPk($vec_mail_attributes['user_model']['id']);
				if ( $user_model )
				{
					$vec_mail_attributes['user_model'] = $user_model;
				}
			}

			// Recover customer_model
			if ( isset($vec_mail_attributes['customer_model']) AND isset($vec_mail_attributes['customer_model']['user_id']) )
			{
				$customer_model = Customer::model()->findByPk($vec_mail_attributes['customer_model']['user_id']);
				if ( $customer_model )
				{
					$vec_mail_attributes['customer_model'] = $customer_model;
				}
			}

			// Recover order_model
			if ( isset($vec_mail_attributes['order_model']) AND isset($vec_mail_attributes['order_model']['order_id']) )
			{
				$order_model = Order::model()->findByPk($vec_mail_attributes['order_model']['order_id']);
				if ( $order_model )
				{
					$vec_mail_attributes['order_model'] = $order_model;
				}
			}			


			$mailer->setData($vec_mail_attributes);

			// Send email and not render template (first param) and not generate EML file (second param)
			// $send_email_result = $mailer->send_email(FALSE, FALSE);
			$send_email_result = $mailer->send_email();
			
			// Error
			if ( $send_email_result === FALSE )
			{
				$this->is_error = 1;
				$this->error_description = $mailer->getError();
			}

			// Mark as "sent"
			else
			{
				$this->is_pending = 0;
				$this->body = $mailer->Body;
			}

			// Save the model
			if ( ! $this->save() )
			{
				Log::save_model_error($this);
				return FALSE;
			}

			return $send_email_result;
		}

		return FALSE;
	}
}