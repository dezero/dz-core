<?php
/**
 * @file models/MailTemplate.php
 * MailTemplate model class for "mail_template" database table
 *
 * Columns in table "mail_template" available as properties of the model,
 * followed by relations of table "mail_template" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property string $template_id
 * @property string $alias
 * @property string $language_id
 * @property string $name
 * @property string $description
 * @property string $subject
 * @property string $body
 * @property string $header
 * @property string $footer
 * @property string $disable_date
 * @property string $disable_uid
 * @property string $created_date
 * @property string $created_uid
 * @property string $updated_date
 * @property string $updated_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property MailHistory[] $mailHistories
 * @property Language $language
 * @property User $disableUser
 * @property User $createdUser
 * @property User $updatedUser
 */

use dz\helpers\Log;
use dz\components\Mailer;

// Include Textile
Yii::import('@lib.php-textile.*', true);

// Import model base class for the table "mail_template"
Yii::import('config.models._base.BaseMailTemplate');

class MailTemplate extends BaseMailTemplate
{
	/**
	 * Mailer class (using PHPMailer)
	 */
	public $mailer;


	/**
	 * User model
	 */
	public $user_model;


	/**
	 * Customer model
	 */
	public $customer_model;


	/**
	 * Order model
	 */
	public $order_model;


	/**
	 * Extra data for email (sending process)
	 */
	public $vec_extra_data = array();


	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return array(
			array('alias, name, subject, body, created_date, created_uid, updated_date, updated_uid', 'required'),
			array('alias', 'length', 'max'=>64),
			array('language_id', 'length', 'max'=>4),
			array('name', 'length', 'max'=>128),
			array('subject', 'length', 'max'=>255),
			array('disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'length', 'max'=>10),
			array('description, header, footer', 'safe'),
			array('language_id, description, header, footer, disable_date, disable_uid', 'default', 'setOnEmpty' => true, 'value' => null),
			array('template_id, alias, language_id, name, description, subject, body, header, footer, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'safe', 'on'=>'search'),
		);
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return array(
			'mailHistories' => array(self::HAS_MANY, 'MailHistory', 'template_id'),
			'language' => array(self::BELONGS_TO, 'Language', 'language_id'),
			'disableUser' => array(self::BELONGS_TO, 'User', array('disable_uid' => 'id')),
			'createdUser' => array(self::BELONGS_TO, 'User', array('created_uid' => 'id')),
			'updatedUser' => array(self::BELONGS_TO, 'User', array('updated_uid' => 'id')),
		);
	}
	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return array(
			'template_id' => Yii::t('app', 'Template'),
			'alias' => Yii::t('app', 'Alias'),
			'language_id' => null,
			'name' => Yii::t('app', 'Name'),
			'description' => Yii::t('app', 'Description'),
			'subject' => Yii::t('app', 'Asunto'),
			'body' => Yii::t('app', 'Contenido'),
			'header' => Yii::t('app', 'Header'),
			'footer' => Yii::t('app', 'Footer'),
			'disable_date' => Yii::t('app', 'Disable Date'),
			'disable_uid' => null,
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'mailHistories' => null,
			'language' => null,
			'disableUser' => null,
			'createdUser' => null,
			'updatedUser' => null,
		);
	}
	
	
	/*
	|--------------------------------------------------------------------------
	| CUSTOM FUNCTIONS
	|--------------------------------------------------------------------------
	*/


	/**
	 * Generate an ActiveDataProvider for search form of this model
	 *
	 * Used in CGridView
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('language_id', $this->language_id);

		$criteria->compare('template_id', $this->template_id);
		$criteria->compare('alias', $this->alias);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('subject', $this->subject, true);
		$criteria->compare('body', $this->body, true);
		$criteria->compare('header', $this->header, true);
		$criteria->compare('footer', $this->footer, true);
		$criteria->compare('disable_date', $this->disable_date, true);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_date', $this->updated_date, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => FALSE,
			// 'pagination' => array('pageSize' => 25,),
			'sort' => array(
				'defaultOrder' => array('alias' => FALSE)
			)
		));
	}


	/**
	 * Find template by alias
	 */
	public function findByAlias($alias, $language_id = 'es')
	{
		return MailTemplate::model()->findByAttributes(array(
			'alias' 		=> $alias,
			'language_id'	=> $language_id
		));
	}


	/**
	 * Get same template translated into another language
	 */
	public function get_translated_model($language_id)
	{
		if ( $language_id != $this->language_id )
		{
			$translated_mail_model = $this->findByAlias($this->alias, $language_id);
			if ( !empty($translated_mail_model) )
			{
				// Assign same values from orignal language
				$translated_mail_model->vec_extra_data = $this->vec_extra_data;
				$translated_mail_model->user_model = $this->user_model;
				$translated_mail_model->customer_model = $this->customer_model;
				$translated_mail_model->order_model = $this->order_model;
				return $translated_mail_model;
			}
		}

		return $this;
	}



	/**
	 * Set a Customer model
	 */
	public function set_customer($customer_model = '')
	{
		if ( empty($this->customer_model) )
		{
			// Try to get current customer model
			if ( empty($customer_model) AND Yii::app()->user->id > 0 )
			{
				$customer_model = Customer::model()->findByPk(Yii::app()->user->id);
			}
			$this->customer_model = $customer_model;

			// User model
			if ( $this->customer_model AND empty($this->user_model) )
			{
				$this->user_model = User::model()->findbyPk($this->customer_model->user_id);
			}

			// Default language
			/*
			if ( $this->customer_model AND $this->customer_model->language_id )
			{
				$this->language_id = $this->customer_model->language_id;
			}
			*/
		}
	}


	/**
	 * Set an User model
	 */
	public function set_user($user_model = '')
	{
		if ( empty($this->user_model) )
		{
			// Try to get current user
			if ( empty($user_model) AND Yii::app()->user->id > 0 )
			{
				$user_model = User::model()->findbyPk($this->user_id);
			}
			$this->user_model = $user_model;
		}

		return TRUE;
	}


	/**
	 * Set an Order model
	 */
	public function set_order($order_model)
	{
		$this->order_model = $order_model;
	}


	/**
	 * Set extra data
	 */
	public function set_extra_data($vec_data)
	{
		$this->vec_extra_data = $vec_data;
	}


	/**
	 * Render text mail content
	 */
	public function render_text($section)
	{
		// We need a destination (Customer model) to send the email
		if ( empty($this->customer_model) )
		{
			// Try to get current customer
			$this->set_customer();
			if ( empty($this->customer_model) )
			{
				return '';
			}
		}

		// Include Textile
		Yii::import('@app.lib.php-textile.*', true);

		$output = '';
		switch ( $section )
		{
			case 'subject':
				$output .= $this->_replace_tokens($this->subject);
			break;

			case 'header':
				$output .= $this->_format_text($this->header);
			break;

			case 'body':
				$output .= $this->_format_text($this->body);
			break;

			case 'footer':
				$output .= $this->_format_text($this->footer);
			break;

			// Full: header + body + footer
			default:
				$output .= $this->_format_text($this->header);
				$output .= $this->_format_text($this->body);
				$output .= $this->_format_text($this->footer);
			break;
		}

		// Clean HTML code
		$output = strtr($output, array(
			'<p><tr>' => '<tr>',
			'</tr></p>' => '</tr>'
		));

		return $output;
	}



	/**
	 * Add an email to sending queue for sending it after
	 */
	public function enqueue_email($to_address = '', $language_id = '')
	{
		// Add to queue mail in another language?
		if ( !empty($language_id) AND $this->language_id != $language_id )
		{
			$translated_mail_model = $this->get_translated_model($language_id);
			return $translated_mail_model->add_to_queue_mail($to_address, '');
		}

		// Load Mailer and render email template
		if ( $this->_load_mailer($to_address, $language_id) AND !empty($this->mailer) )
		{
			// Save sending mail to history and add to queue
			$this->_save_to_history(TRUE);

			// Render email
			/*
			$this->mailer->render();

			// Save EML file
			if ( $this->mailer->PreSend() AND $this->mailer->save() )
			{
				// Save sending mail to history
				$this->_save_to_history(TRUE);
			}
			*/

			return TRUE;
		}

		return FALSE;
	}


		/**
	 * Sends an email
	 */
	public function send_email($to_address = '', $language_id = '', $is_save_history = TRUE)
	{
		// Send mail in another language?
		if ( !empty($language_id) AND $this->language_id != $language_id )
		{
			Yii::app()->language = $language_id;
			$translated_mail_model = $this->get_translated_model($language_id);
			return $translated_mail_model->send_email($to_address, '', $is_save_history);
		}

		// Load Mailer and render email template
		if ( $this->_load_mailer($to_address, $language_id) AND !empty($this->mailer) )
		{
			// Check if send email is enabled
			if ( Yii::app()->params['mail']['is_enabled'] )
			{
				$send_email_result = $this->mailer->send_email();
			}
			else
			{
				$send_email_result = TRUE;
			}

			// Atributes for DzMailHistory
			if ( $is_save_history )
			{
				$is_error = ($send_email_result === FALSE);

				// Save sending mail to history
				$this->_save_to_history(FALSE, $is_error);
			}

			return $send_email_result;
		}

		return FALSE;
	}


	/**
	 * Load Mailer and render email template
	 */
	private function _load_mailer($to_address = '', $language_id = '')
	{
		// Try to get current member model
		if ( empty($to_address) )
		{
			$this->set_user();
			if ( !empty($this->user_model) )
			{
				$to_address = $this->user_model->email;
			}
		}

		// Destination email address is needed
		if ( empty($to_address) )
		{
			return FALSE;	
		}

		/**
		 * Attributes for email template
		 */
		$vec_mail_attributes = array(
			'header' 			=> $this->render_text('header'),
			'body' 				=> $this->render_text('body'),
			'footer' 			=> $this->render_text('footer'),
			// 'template'		=> $this
		);

		if ( !empty($this->user_model) )
		{
			$vec_mail_attributes['user_model'] = $this->user_model;
		}
		if ( !empty($this->customer_model) )
		{
			$vec_mail_attributes['customer_model'] = $this->customer_model;
		}
		if ( !empty($this->order_model) )
		{
			$vec_mail_attributes['order_model'] = $this->order_model;
		}

		$this->mailer = new Mailer();
		if ( Yii::app()->params['mail']['is_test'] )
		{
			$to_address = Yii::app()->params['mail']['test_email'];
		}
		$this->mailer->setTo($to_address);
		$this->mailer->setView('commerce--notification');
		$this->mailer->setSubject($this->render_text('subject'));
		$this->mailer->setData($vec_mail_attributes);

		// CC address
		if ( !empty($this->cc_address) )
		{
			$this->mailer->setCc($this->cc_address);
		}

		return TRUE;
	}


	/**
	 * Format the text following Textile reference
	 * 
	 * @see https://txstyle.org/
	 */
	private function _format_text($que_text)
	{
		$parser = new TextileParser();
		return $parser->parse($this->_replace_tokens($que_text));
	}


	/**
	 * Replace tokens
	 */
	private function _replace_tokens($que_text)
	{
		// Load user model
		if ( !empty($this->customer_model) AND empty($this->user_model) )
		{
			$this->user_model = User::model()->findbyPk($this->customer_model->user_id);
		}

		else if ( empty($this->user_model) )
		{
			$this->set_user();
		}

		// Tokens for USERS
		if ( !empty($this->customer_model) AND !empty($this->user_model) )
		{
			// Console - Get URL manually
			if ( get_class(Yii::app())=='CConsoleApplication' )
			{
				$url_login = Yii::app()->params['baseUrl'] . '/account/login?u='. $this->user_model->username;
				$url_register = Yii::app()->params['baseUrl'] . '/account/register';
				$url_recovery_password = Yii::app()->params['baseUrl'] . '/account/password' .'?verification_code='. $this->user_model->verification_code .'&email='. $this->user_model->email;
			}

			// Web - Get URL via createAbsoluteUrl
			else
			{
				$url_login = Yii::app()->createAbsoluteUrl('/account/login', array('u' => $this->user_model->username));
				$url_register = Yii::app()->createAbsoluteUrl('/account/register');
				$url_recovery_password = Yii::app()->createAbsoluteUrl('/account/password', array("verification_code" => $this->user_model->verification_code, "email" => $this->user_model->email));
			}

			$vec_text = array(
				'[name]'	 				=> $this->user_model->profile ? $this->user_model->profile->name : $this->user_model->email,
				'[full_name]'	 			=> $this->user_model->profile ? $this->user_model->profile->getFullName() : $this->user_model->email,
				'[email]'	 				=> $this->user_model->email,
				'[url_login]' 				=> $url_login,
				'[url_register]' 			=> $url_register,
				'[url_recovery_password]' 	=> $url_recovery_password,
			);

			if ( !empty($this->order_model) )
			{
				// Order details
				$template_file = '@frontend_theme.views.mail.zone--order';

				// Load controller and template file system from Console (@source YiiMailer)
				if ( isset(Yii::app()->controller) )
				{
					$controller = Yii::app()->controller;
					$template_file = Yii::app()->controller->getViewFile($template_file);
				}
				else
				{
					$controller = new CController('YiiMailer');
					$template_file = Yii::getPathOfAlias($template_file);
					if ( is_file($template_file.'.tpl.php') )
					{
						$template_file = Yii::app()->findLocalizedFile($template_file .'.tpl.php');
					}
				}

				$order_content = $controller->renderInternal($template_file, array(
					'order_model' 	=> $this->order_model
				), TRUE);

				$vec_text['[order_id]'] = $this->order_model->order_id;
				$vec_text['[order_reference]'] = $this->order_model->order_reference;
				$vec_text['[order_date]'] = $this->order_model->paid_date;
				$vec_text['[order_details]'] = $order_content;
			}

			return strtr($que_text, $vec_text);
		}

		return $que_text;
	}


	/**
	 * Save this email to history
	 */
	private function _save_to_history($is_pending = TRUE, $is_error = FALSE)
	{
		$to_address = $this->mailer->getToAddress();
		if ( is_array($to_address) )
		{
			if ( isset($to_address[0]) AND is_array($to_address[0]) )
			{
				$to_address = $to_address[0];
			}

			// Get first element of array
			if ( isset($to_address[0]) )
			{
				$to_address = $to_address[0];
			}
			else
			{
				$to_address = reset($to_address);
			}

			/*
			if ( count($to_address) > 1 )
			{
				$to_address = implode(",", $to_address);
			}
			else
			{
				$to_address = $to_address[0];
			}
			*/
		}

		if ( ! $is_pending )
		{
			$vec_mail_history_attributes = array(
				'subject'			=> $this->mailer->Subject,
				'body'				=> $this->mailer->Body,
				'sender_mail'		=> $this->mailer->From,
				'recipient_mails'	=> $to_address,
				'recipient_uid'		=> ( $this->customer_model ) ? $this->customer_model->user_id : NULL,
				'language_id'		=> $this->language_id,
				'template_id'		=> $this->template_id,
				'template_alias'	=> $this->alias,
				'file_name_eml'		=> $this->mailer->filename,
				'is_error'			=> ( $is_error ) ? 1 : 0,
				'is_pending'		=> ( $is_pending ) ? 1 : 0
			);
		}
		else
		{
			$vec_mail_history_attributes = array(
				'subject'			=> $this->mailer->Subject,
				'body'				=> CJSON::encode($this->mailer->getData()),
				'sender_mail'		=> $this->mailer->From,
				'recipient_mails'	=> $to_address,
				'recipient_uid'		=> ( $this->customer_model ) ? $this->customer_model->user_id : NULL,
				'language_id'		=> $this->language_id,
				'template_id'		=> $this->template_id,
				'template_alias'	=> $this->alias,
				'file_name_eml'		=> '',
				'is_error'			=> ( $is_error ) ? 1 : 0,
				'is_pending'		=> ( $is_pending ) ? 1 : 0
			);
		}

		// Extra data?
		if ( !empty($this->vec_extra_data) )
		{
			if ( isset($this->vec_extra_data['order_id']) )
			{
				$vec_mail_history_attributes['order_id'] = $this->vec_extra_data['order_id'];
			}
		}

		// Some error detected?
		if ( $is_error )
		{
			$vec_mail_history_attributes['error_description'] = $this->mailer->getError();
		}

		// Finally, save the model
		$mail_history_model = new MailHistory;
		$mail_history_model->setAttributes($vec_mail_history_attributes);
		if ( ! $mail_history_model->save() )
		{
			Log::save_model_error($mail_history_model);
			return FALSE;
		}
		return TRUE;
	}
}