<?php
/**
 * @package dz\modules\settings\models\_base
 */

namespace dz\modules\settings\models\_base;

use dz\db\ActiveRecord;
use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use Yii;

/**
 * DO NOT MODIFY THIS FILE! It is automatically generated by Gii.
 * If any changes are necessary, you must set or override the required
 * property or method in class "dz\modules\settings\models\MailHistory".
 *
 * This is the model base class for the table "mail_history".
 * Columns in table "mail_history" available as properties of the model,
 * followed by relations of table "mail_history" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $history_id
 * @property string $subject
 * @property string $body
 * @property string $sender_name
 * @property string $sender_email
 * @property string $reply_email
 * @property string $recipient_emails
 * @property string $cc_emails
 * @property string $bcc_emails
 * @property integer $recipient_uid
 * @property string $eml_file_path
 * @property string $language_id
 * @property integer $template_id
 * @property string $template_alias
 * @property string $status_type
 * @property integer $send_date
 * @property integer $scheduled_date
 * @property integer $is_test
 * @property string $error_description
 * @property integer $entity_id
 * @property string $entity_type
 * @property string $entity_scenario
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $language
 * @property mixed $recipientUser
 * @property mixed $template
 * @property mixed $updatedUser
 */
abstract class MailHistory extends ActiveRecord
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * Returns the name of the associated database table
	 */
	public function tableName()
	{
		return 'mail_history';
	}


	/**
	 * Label with translation support (from GIIX)
	 */
	public static function label($n = 1)
	{
		return Yii::t('app', 'MailHistory|MailHistories', $n);
	}


    /**
     * Returns the validation rules for attributes
     */
    public function rules()
    {
        return [
            ['subject, sender_name, sender_email, recipient_emails, created_date, created_uid, updated_date, updated_uid', 'required'],
            ['recipient_uid, template_id, send_date, scheduled_date, is_test, entity_id, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
            ['subject, sender_name, sender_email, reply_email, recipient_emails, cc_emails, bcc_emails, eml_file_path, error_description', 'length', 'max'=> 255],
            ['language_id', 'length', 'max'=> 4],
            ['template_alias', 'length', 'max'=> 64],
            ['entity_type, entity_scenario', 'length', 'max'=> 32],
            ['uuid', 'length', 'max'=> 36],
            ['status_type', 'in', 'range' => ['pending', 'scheduled', 'sent', 'error']],
            ['body, reply_email, cc_emails, bcc_emails, recipient_uid, eml_file_path, language_id, template_id, template_alias, status_type, send_date, scheduled_date, is_test, error_description, entity_id, entity_type, entity_scenario, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
            ['body', 'safe'],
            ['history_id, subject, body, sender_name, sender_email, reply_email, recipient_emails, cc_emails, bcc_emails, recipient_uid, eml_file_path, language_id, template_id, template_alias, status_type, send_date, scheduled_date, is_test, error_description, entity_id, entity_type, entity_scenario, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
        ];
    }


    /**
     * Define relations with other objects
     *
     * There are four types of relations that may exist between two active record objects:
     *   - BELONGS_TO: e.g. a member belongs to a team;
     *   - HAS_ONE: e.g. a member has at most one profile;
     *   - HAS_MANY: e.g. a team has many members;
     *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
     */
    public function relations()
    {
        return [
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
            'language' => [self::BELONGS_TO, Language::class, 'language_id'],
            'recipientUser' => [self::BELONGS_TO, User::class, ['recipient_uid' => 'id']],
            'template' => [self::BELONGS_TO, MailTemplate::class, 'template_id'],
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],
        ];
    }


    /**
     * Returns the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'history_id' => Yii::t('app', 'History'),
            'subject' => Yii::t('app', 'Subject'),
            'body' => Yii::t('app', 'Body'),
            'sender_name' => Yii::t('app', 'Sender Name'),
            'sender_email' => Yii::t('app', 'Sender Email'),
            'reply_email' => Yii::t('app', 'Reply Email'),
            'recipient_emails' => Yii::t('app', 'Recipient Emails'),
            'cc_emails' => Yii::t('app', 'Cc Emails'),
            'bcc_emails' => Yii::t('app', 'Bcc Emails'),
            'recipient_uid' => null,
            'eml_file_path' => Yii::t('app', 'Eml File Path'),
            'language_id' => null,
            'template_id' => null,
            'template_alias' => Yii::t('app', 'Template Alias'),
            'status_type' => Yii::t('app', 'Status Type'),
            'send_date' => Yii::t('app', 'Send Date'),
            'scheduled_date' => Yii::t('app', 'Scheduled Date'),
            'is_test' => Yii::t('app', 'Is Test'),
            'error_description' => Yii::t('app', 'Error Description'),
            'entity_id' => Yii::t('app', 'Entity'),
            'entity_type' => Yii::t('app', 'Entity Type'),
            'entity_scenario' => Yii::t('app', 'Entity Scenario'),
            'created_date' => Yii::t('app', 'Created Date'),
            'created_uid' => null,
            'uuid' => Yii::t('app', 'Uuid'),
            'createdUser' => null,
            'language' => null,
            'recipientUser' => null,
            'template' => null,
        ];
    }

    /**
     * Get "status_type" labels
     */
    public function status_type_labels()
    {
        return [
            'pending' => Yii::t('app', 'pending'),
            'scheduled' => Yii::t('app', 'scheduled'),
            'sent' => Yii::t('app', 'sent'),
            'error' => Yii::t('app', 'error'),
        ];
    }


    /**
     * Get "status_type" specific label
     */
    public function status_type_label($status_type)
    {
        $vec_labels = $this->status_type_labels();
        return isset($vec_labels[$status_type]) ? $vec_labels[$status_type] : '';
    }


    /**
     * MailHistory models list
     * 
     * @return array
     */
     public function mailhistory_list()
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['history_id', 'subject'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = MailHistory::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('history_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


	/**
	 * Generate an ActiveDataProvider for search form of this model
	 *
	 * Used in CGridView
	 */
	public function search()
	{
		$criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

		$criteria->compare('t.history_id', $this->history_id);
		$criteria->compare('t.subject', $this->subject, true);
		$criteria->compare('t.body', $this->body, true);
		$criteria->compare('t.sender_name', $this->sender_name, true);
		$criteria->compare('t.sender_email', $this->sender_email, true);
		$criteria->compare('t.reply_email', $this->reply_email, true);
		$criteria->compare('t.recipient_emails', $this->recipient_emails, true);
		$criteria->compare('t.cc_emails', $this->cc_emails, true);
		$criteria->compare('t.bcc_emails', $this->bcc_emails, true);
		$criteria->compare('t.eml_file_path', $this->eml_file_path, true);
		$criteria->compare('t.template_alias', $this->template_alias, true);
        $criteria->compare('t.status_type', $this->status_type);
        $criteria->compare('t.send_date', $this->send_date);
        $criteria->compare('t.scheduled_date', $this->scheduled_date);
		$criteria->compare('t.is_test', $this->is_test);
		$criteria->compare('t.error_description', $this->error_description, true);
		$criteria->compare('t.entity_id', $this->entity_id);
		$criteria->compare('t.entity_type', $this->entity_type, true);
		$criteria->compare('t.entity_scenario', $this->entity_scenario, true);
		$criteria->compare('t.created_date', $this->created_date);
		$criteria->compare('t.uuid', $this->uuid, true);

		return new \CActiveDataProvider($this, [
			'criteria' => $criteria,
			'pagination' => ['pageSize' => 30],
			'sort' => ['defaultOrder' => ['history_id' => true]]
		]);
	}
	
	
	/**
	 * Returns fields not showed in create/update forms
	 */
	public function excludedFormFields()
	{
		return [
			'created_date' => true, 
			'created_uid' => true, 
		];
	}


    /**
     * Automatic string representation for a model (from GIIX)
     */
    public static function representingColumn()
    {
        return 'subject';
    }


    /**
     * Title used for this model
     */
    public function title()
    {
        return $this->subject;
    }
}