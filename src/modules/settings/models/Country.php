<?php
/**
 * @package dz\modules\settings\models 
 */

namespace dz\modules\settings\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dz\modules\settings\models\_base\Country as BaseCountry;
use dz\modules\settings\helpers\CountryHelper;
use user\models\User;
use Yii;

/**
 * Country model class for "country" database table
 *
 * Columns in table "country" available as properties of the model,
 * followed by relations of table "country" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property string $country_code
 * @property string $name
 * @property integer $is_eu
 * @property integer $disable_date
 * @property integer $disable_uid
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $disableUser
 * @property mixed $updatedUser
 */
class Country extends BaseCountry
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['country_code, name, created_date, created_uid, updated_date, updated_uid', 'required'],
			['is_eu, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['country_code', 'length', 'max'=> 4],
			['name', 'length', 'max'=> 64],
			['uuid', 'length', 'max'=> 36],
			['is_eu, disable_date,, disable_uid, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['country_code, name, is_eu, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid, uuid, disable_filter', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'disableUser' => [self::BELONGS_TO, User::class, ['disable_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],
        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'country_code' => Yii::t('app', 'Country Code'),
			'name' => Yii::t('app', 'Name'),
            'is_eu' => Yii::t('app', 'European Union?'),
			'disable_date' => Yii::t('app', 'Disable Date'),
			'disable_uid' => null,
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'createdUser' => null,
			'disableUser' => null,
			'updatedUser' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.country_code', $this->country_code);
        $criteria->compare('t.name', $this->name, true);
        if ( $this->is_eu !== 'all' )
        {
            $criteria->compare('t.is_eu', $this->is_eu);
        }

        // $criteria->compare('t.disable_date', $this->disable_date);
        // $criteria->compare('t.created_date', $this->created_date);
        // $criteria->compare('t.updated_date', $this->updated_date);
        // $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 100],
            'sort' => ['defaultOrder' => ['country_code' => false]]
        ]);
    }


    /**
     * Country models list
     * 
     * @return array
     */
    public function country_list()
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['country_code', 'name'];
        $criteria->order = 't.name ASC';
        // $criteria->condition = '';
        
        $vec_models = Country::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('country_code')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Return a country code list following ISO 3166-1 alpha2 format
     * 
     * @see https://gist.github.com/djaiss/2938259
     */
    public function country_codes($language_id = '')
    {
        if ( empty($language_id) )
        {
            $language_id = Yii::currentLanguage();
        }
        return CountryHelper::code_list($language_id);
    }


    /**
     * Mapping arrays for countries ISO 3166-1 from alpha-2 code to alpha-3
     * 
     * @see https://gist.github.com/diegogurpegui/0476f17d3f01a7f79246c454876bdb9e
     */ 
    public function alpha3_code()
    {
        return CountryHelper::alpha3_code($this->country_code);
    }
}