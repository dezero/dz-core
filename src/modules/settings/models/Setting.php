<?php
/**
 * @package dz\modules\settings\models 
 */

namespace dz\modules\settings\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Html;
use dz\helpers\StringHelper;
use dz\modules\settings\models\_base\Setting as BaseSetting;
use user\models\User;
use Yii;

/**
 * Setting model class for "setting" database table
 *
 * Columns in table "setting" available as properties of the model,
 * followed by relations of table "setting" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property string $name
 * @property string $value
 * @property string $type
 * @property string $title
 * @property string $description
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $updatedUser
 */
class Setting extends BaseSetting
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['name, type, created_date, created_uid, updated_date, updated_uid', 'required'],
			['created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['name, type', 'length', 'max'=> 32],
			['value, title', 'length', 'max'=> 255],
			['description', 'length', 'max'=> 512],
			['uuid', 'length', 'max'=> 36],
			['value, title, description, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['name, value, type, title, description, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'name' => Yii::t('app', 'Name'),
			'value' => Yii::t('app', 'Value'),
			'type' => Yii::t('app', 'Type'),
			'title' => Yii::t('app', 'Title'),
			'description' => Yii::t('app', 'Description'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'createdUser' => null,
			'updatedUser' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.value', $this->value, true);
        $criteria->compare('t.type', $this->type, true);
        $criteria->compare('t.title', $this->title, true);
        $criteria->compare('t.description', $this->description, true);
        // $criteria->compare('t.created_date', $this->created_date);
        // $criteria->compare('t.updated_date', $this->updated_date);
        // $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['name' => true]]
        ]);
    }


    /**
     * Setting models list
     * 
     * @return array
     */
     public function setting_list()
    {
        $vec_output = array();

        $criteria = new DbCriteria;
        $criteria->select = ['name', 'value'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = Setting::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('name')] = $que_model->title();
            }
        }

        return $vec_output;
    }



    /*
    |--------------------------------------------------------------------------
    | CUSTOM FUNCTIONS
    |--------------------------------------------------------------------------
    */


    /**
     * Get the input ID for a model attribute.
     */
    public function resolve_id($attribute)
    {
        return Html::getIdByName($this->resolve_name($attribute));
    }


    /**
     * Get the input name for a model attribute.
     */
    public function resolve_name($attribute)
    {
        return 'Setting['. $this->name .']['. $attribute .']';
    }
}