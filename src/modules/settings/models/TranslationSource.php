<?php
/**
 * @package dz\modules\settings\models 
 */

namespace dz\modules\settings\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\settings\models\_base\TranslationSource as BaseTranslationSource;
use user\models\User;
use Yii;

/**
 * TranslationSource model class for "translation_source" database table
 *
 * Columns in table "translation_source" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $id
 * @property string $category
 * @property string $message
 * @property integer $updated_date
 * @property integer $updated_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class TranslationSource extends BaseTranslationSource
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['updated_date, updated_uid', 'required'],
			['updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['category', 'length', 'max'=> 64],
			['category, message', 'default', 'setOnEmpty' => true, 'value' => null],
			['message', 'safe'],
			['id, category, message, updated_date, updated_uid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'category' => Yii::t('app', 'Category'),
			'message' => Yii::t('app', 'Message'),
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => Yii::t('app', 'Updated Uid'),
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search($search_id = '')
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.category', $this->category);
        $criteria->compare('t.message', $this->message, true);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.updated_uid', $this->updated_uid);

        // Limit 50 rows per page by default
        $page_size = 50;

        // "Unlimited" paging for export excel action
        if ( $search_id == 'excel' )
        {
            $page_size = 100000;
        }

        // Order ASC by "message date"
        $criteria->order = 't.message ASC';

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => $page_size],
            // 'sort' => ['defaultOrder' => ['id' => true]]
        ]);
    }


    /**
     * Get translated model
     */
    public function get_translation_model($language_id)
    {
        return TranslationMessage::get()
            ->where([
                'id'            => $this->id,
                'language_id'   => $language_id,
            ])
            ->one();
    }


    /**
     * Save translated message
     */
    public function save_translation($language_id, $message)
    {
        $translation_message_model = $this->get_translation_model($language_id);
        if ( ! $translation_message_model )
        {
            $translation_message_model = Yii::createObject(TranslationMessage::class);
            $translation_message_model->setAttributes([
                'id'            => $this->id,
                'language_id'   => $language_id
            ]);
        }
        $translation_message_model->translation = $message;
        if ( ! $translation_message_model->save() )
        {
            Log::save_model_error($translation_message_model);
            return false;
        }

        return true;
    }


    /**
     * Automatic string representation for a model (from GIIX)
     */
    public static function representingColumn()
    {
        return 'message';
    }


    /**
     * Title used for this model
     */
    public function title()
    {
        return $this->message;
    }
}
