<?php
/**
 * @package dz\modules\settings\models 
 */

namespace dz\modules\settings\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dz\modules\settings\models\_base\Language as BaseLanguage;
use user\models\User;
use Yii;

/**
 * Language model class for "language" database table
 *
 * Columns in table "language" available as properties of the model,
 * followed by relations of table "language" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property string $language_id
 * @property string $name
 * @property string $native
 * @property string $prefix
 * @property integer $is_ltr_direction
 * @property integer $is_default
 * @property integer $weight
 * @property integer $disable_date
 * @property integer $disable_uid
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $disableUser
 * @property mixed $updatedUser
 */
class Language extends BaseLanguage
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['language_id, name, prefix, created_date, created_uid, updated_date, updated_uid', 'required'],
			['is_ltr_direction, is_default, weight, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['language_id', 'length', 'max'=> 4],
			['name, native', 'length', 'max'=> 64],
			['prefix', 'length', 'max'=> 16],
			['uuid', 'length', 'max'=> 36],
			['native, is_ltr_direction, is_default, weight, disable_date, disable_uid, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['language_id, name, native, prefix, is_ltr_direction, is_default, weight, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid, uuid, disable_filter', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'disableUser' => [self::BELONGS_TO, User::class, ['disable_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],
        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'language_id' => Yii::t('app', 'Language code'),
            'name' => Yii::t('app', 'Public name'),
            'native' => Yii::t('app', 'Native name'),
            'prefix' => Yii::t('app', 'URL Prefix / Core'),
            'is_ltr_direction' => Yii::t('app', 'LTR language?'),
            'is_default' => Yii::t('app', 'Default language?'),
			'weight' => Yii::t('app', 'Weight'),
			'disable_date' => Yii::t('app', 'Disable Date'),
			'disable_uid' => null,
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'createdUser' => null,
			'disableUser' => null,
			'updatedUser' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.language_id', $this->language_id, true);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.native', $this->native, true);

        // $criteria->compare('t.prefix', $this->prefix, true);
        // $criteria->compare('t.is_ltr_direction', $this->is_ltr_direction);
        // $criteria->compare('t.is_default', $this->is_default);
        // $criteria->compare('t.weight', $this->weight);
        // $criteria->compare('t.disable_date', $this->disable_date);
        // $criteria->compare('t.created_date', $this->created_date);
        // $criteria->compare('t.updated_date', $this->updated_date);
        // $criteria->compare('t.uuid', $this->uuid, true);

        // Filter just DISABLED languages
        if ( $this->disable_filter == 1 )
        {
            $criteria->addCondition('t.disable_date IS NOT NULL AND t.disable_date > 0');
        }

        // Filter just ENABLED languages
        else if ( $this->disable_filter == 2 )
        {
            $criteria->addCondition('t.disable_date IS NULL OR t.disable_date = 0');
        }

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            // 'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['weight' => false]]
        ]);
    }


    /**
     * Language models list
     * 
     * @return array
     */
     public function language_list()
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['language_id', 'name'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = Language::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('language_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Event "beforeValidate"
     *
     * This method is invoked before validation starts
     *
     * @return bool
     */
    public function beforeValidate()
    {
        switch ( $this->scenario )
        {
            // Create new Language
            case 'insert':
                $criteria = new DbCriteria();
                $criteria->order = 'weight DESC';
                $criteria->limit = 1;
                $last_language_model = Language::model()->find($criteria);
                if ( $last_language_model )
                {
                    $this->weight = $last_language_model->weight + 1;
                    $this->is_default = 0;
                }

                // First language added to database
                else
                {
                    $this->weight = 1;
                    $this->is_default = 1;
                }
            break;
        }

        return parent::beforeValidate();
    }


    /**
     * Event "afterSave"
     *
     * This method is invoked after saving a record successfully
     */
    public function afterSave()
    {
        switch ( $this->scenario )
        {
            // Update language
            case 'update':
                // Ensure all other languages have "is_default = 0" value
                if ( $this->is_default == 1 )
                {
                    Yii::app()->db->createCommand("UPDATE language SET is_default = 0 WHERE language_id <> :language_id")->bindValue(':language_id', $this->language_id)->execute();
                }
            break;
        }

        return parent::afterSave();
    }


    /**
     * Disables a language
     */
    public function disable()
    {
        $this->scenario = 'disable';
        if ( $this->is_default == 1 )
        {
            $this->addError('is_default', Yii::t('app', 'The DEFAULT language cannot be disabled'));

            return false;
        }

        return parent::disable();
    }


    /**
     * Deletes the model
     */
    public function delete()
    {
        if ( $this->is_default == 1 )
        {
            $this->addError('is_default', Yii::t('app', 'The DEFAULT language cannot be deleted'));

            return false;
        }

        return parent::delete();
    }


    /**
     * Add a new language to platform
     */
    public function add_language($language_id)
    {
        $vec_languages = $this->get_predefined_list();
        if ( isset($vec_languages[$language_id]) )
        {
            // Language attributes
            $this->language_id = $language_id;
            $this->prefix = $language_id;
            $this->weight = 1;

            // English name and native name
            $que_language = $vec_languages[$language_id];
            $this->name = $que_language[0];
            if ( isset($que_language[1]) )
            {
                $this->native = $que_language[1];
            }

            return true;
        }
        return false;
    }


    /**
     * Some of the common languages with their English and native names
     *
     * Based on ISO 639 and http://people.w3.org/rishida/names/languages.html
     *
     * Return an array with following structure
     *  
     */
    public function get_predefined_list()
    {
        // Returns an array with the following structure
        //  array('language_id' => (
        //      'name',
        //      'native_name',
        //      'is_RTL_language
        //  ));
        return [
            'aa' => array('Afar'),
            'ab' => array('Abkhazian', 'аҧсуа бызшәа'),
            'ae' => array('Avestan'),
            'af' => array('Afrikaans'),
            'ak' => array('Akan'),
            'am' => array('Amharic', 'አማርኛ'),
            // 'ar' => array('Arabic', /* Left-to-right marker "‭" */ 'العربية', TRUE),
            'as' => array('Assamese'),
            'ast' => array('Asturian'),
            'av' => array('Avar'),
            'ay' => array('Aymara'),
            'az' => array('Azerbaijani', 'azərbaycan'),
            'ba' => array('Bashkir'),
            'be' => array('Belarusian', 'Беларуская'),
            'bg' => array('Bulgarian', 'Български'),
            'bh' => array('Bihari'),
            'bi' => array('Bislama'),
            'bm' => array('Bambara', 'Bamanankan'),
            'bn' => array('Bengali'),
            'bo' => array('Tibetan'),
            'br' => array('Breton'),
            'bs' => array('Bosnian', 'Bosanski'),
            'ca' => array('Catalan', 'Català'),
            'ce' => array('Chechen'),
            'ch' => array('Chamorro'),
            'co' => array('Corsican'),
            'cr' => array('Cree'),
            'cs' => array('Czech', 'Čeština'),
            'cu' => array('Old Slavonic'),
            'cv' => array('Chuvash'),
            'cy' => array('Welsh', 'Cymraeg'),
            'da' => array('Danish', 'Dansk'),
            'de' => array('German', 'Deutsch'),
            'dv' => array('Maldivian'),
            'dz' => array('Bhutani'),
            'ee' => array('Ewe', 'Ɛʋɛ'),
            'el' => array('Greek', 'Ελληνικά'),
            'en' => array('English', 'English'),
            'en-gb' => array('English, British', 'English, British'),
            'eo' => array('Esperanto'),
            'es' => array('Spanish', 'Español'),
            'et' => array('Estonian', 'Eesti'),
            'eu' => array('Basque', 'Euskera'),
            // 'fa' => array('Persian', /* Left-to-right marker "‭" */ 'فارسی', TRUE),
            'ff' => array('Fulah', 'Fulfulde'),
            'fi' => array('Finnish', 'Suomi'),
            'fil' => array('Filipino'),
            'fj' => array('Fiji'),
            'fo' => array('Faeroese'),
            'fr' => array('French', 'Français'),
            'fy' => array('Frisian', 'Frysk'),
            'ga' => array('Irish', 'Gaeilge'),
            'gd' => array('Scots Gaelic'),
            'gl' => array('Galician', 'Galego'),
            'gn' => array('Guarani'),
            'gsw-berne' => array('Swiss German'),
            'gu' => array('Gujarati'),
            'gv' => array('Manx'),
            'ha' => array('Hausa'),
            // 'he' => array('Hebrew', /* Left-to-right marker "‭" */ 'עברית', TRUE),
            'hi' => array('Hindi', 'हिन्दी'),
            'ho' => array('Hiri Motu'),
            'hr' => array('Croatian', 'Hrvatski'),
            'ht' => array('Haitian Creole'),
            'hu' => array('Hungarian', 'Magyar'),
            'hy' => array('Armenian', 'Հայերեն'),
            'hz' => array('Herero'),
            'ia' => array('Interlingua'),
            'id' => array('Indonesian', 'Bahasa Indonesia'),
            'ie' => array('Interlingue'),
            'ig' => array('Igbo'),
            'ik' => array('Inupiak'),
            'is' => array('Icelandic', 'Íslenska'),
            'it' => array('Italian', 'Italiano'),
            'iu' => array('Inuktitut'),
            'ja' => array('Japanese', '日本語'),
            'jv' => array('Javanese'),
            'ka' => array('Georgian'),
            'kg' => array('Kongo'),
            'ki' => array('Kikuyu'),
            'kj' => array('Kwanyama'),
            'kk' => array('Kazakh', 'Қазақ'),
            'kl' => array('Greenlandic'),
            'km' => array('Cambodian'),
            'kn' => array('Kannada', 'ಕನ್ನಡ'),
            'ko' => array('Korean', '한국어'),
            'kr' => array('Kanuri'),
            'ks' => array('Kashmiri'),
            'ku' => array('Kurdish', 'Kurdî'),
            'kv' => array('Komi'),
            'kw' => array('Cornish'),
            'ky' => array('Kyrgyz', 'Кыргызча'),
            'la' => array('Latin', 'Latina'),
            'lb' => array('Luxembourgish'),
            'lg' => array('Luganda'),
            'ln' => array('Lingala'),
            'lo' => array('Laothian'),
            'lt' => array('Lithuanian', 'Lietuvių'),
            'lv' => array('Latvian', 'Latviešu'),
            'mg' => array('Malagasy'),
            'mh' => array('Marshallese'),
            'mi' => array('Māori'),
            'mk' => array('Macedonian', 'Македонски'),
            'ml' => array('Malayalam', 'മലയാളം'),
            'mn' => array('Mongolian'),
            'mo' => array('Moldavian'),
            'mr' => array('Marathi'),
            'ms' => array('Malay', 'Bahasa Melayu'),
            'mt' => array('Maltese', 'Malti'),
            'my' => array('Burmese'),
            'na' => array('Nauru'),
            'nd' => array('North Ndebele'),
            'ne' => array('Nepali'),
            'ng' => array('Ndonga'),
            'nl' => array('Dutch', 'Nederlands'),
            'nb' => array('Norwegian Bokmål', 'Bokmål'),
            'nn' => array('Norwegian Nynorsk', 'Nynorsk'),
            'nr' => array('South Ndebele'),
            'nv' => array('Navajo'),
            'ny' => array('Chichewa'),
            'oc' => array('Occitan'),
            'om' => array('Oromo'),
            'or' => array('Oriya'),
            'os' => array('Ossetian'),
            'pa' => array('Punjabi'),
            'pi' => array('Pali'),
            'pl' => array('Polish', 'Polski'),
            // 'ps' => array('Pashto', /* Left-to-right marker "‭" */ 'پښتو', TRUE),
            'pt' => array('Portuguese, International'),
            'pt-pt' => array('Portuguese, Portugal', 'Português'),
            'pt-br' => array('Portuguese, Brazil', 'Português'),
            'qu' => array('Quechua'),
            'rm' => array('Rhaeto-Romance'),
            'rn' => array('Kirundi'),
            'ro' => array('Romanian', 'Română'),
            'ru' => array('Russian', 'Русский'),
            'rw' => array('Kinyarwanda'),
            'sa' => array('Sanskrit'),
            'sc' => array('Sardinian'),
            'sco' => array('Scots'),
            'sd' => array('Sindhi'),
            'se' => array('Northern Sami'),
            'sg' => array('Sango'),
            'sh' => array('Serbo-Croatian'),
            'si' => array('Sinhala', 'සිංහල'),
            'sk' => array('Slovak', 'Slovenčina'),
            'sl' => array('Slovenian', 'Slovenščina'),
            'sm' => array('Samoan'),
            'sn' => array('Shona'),
            'so' => array('Somali'),
            'sq' => array('Albanian', 'Shqip'),
            'sr' => array('Serbian', 'Српски'),
            'ss' => array('Siswati'),
            'st' => array('Sesotho'),
            'su' => array('Sudanese'),
            'sv' => array('Swedish', 'Svenska'),
            'sw' => array('Swahili', 'Kiswahili'),
            'ta' => array('Tamil', 'தமிழ்'),
            'te' => array('Telugu', 'తెలుగు'),
            'tg' => array('Tajik'),
            'th' => array('Thai', 'ภาษาไทย'),
            'ti' => array('Tigrinya'),
            'tk' => array('Turkmen'),
            'tl' => array('Tagalog'),
            'tn' => array('Setswana'),
            'to' => array('Tonga'),
            'tr' => array('Turkish', 'Türkçe'),
            'ts' => array('Tsonga'),
            'tt' => array('Tatar', 'Tatarça'),
            'tw' => array('Twi'),
            'ty' => array('Tahitian'),
            'ug' => array('Uyghur'),
            'uk' => array('Ukrainian', 'Українська'),
            // 'ur' => array('Urdu', /* Left-to-right marker "‭" */ 'اردو', TRUE),
            'uz' => array('Uzbek', "o'zbek"),
            've' => array('Venda'),
            'vi' => array('Vietnamese', 'Tiếng Việt'),
            'wo' => array('Wolof'),
            'xh' => array('Xhosa', 'isiXhosa'),
            'xx-lolspeak' => array('Lolspeak'),
            'yi' => array('Yiddish'),
            'yo' => array('Yoruba', 'Yorùbá'),
            'za' => array('Zhuang'),
            'zh-hans' => array('Chinese, Simplified', '简体中文'),
            'zh-hant' => array('Chinese, Traditional', '繁體中文'),
            'zu' => array('Zulu', 'isiZulu'),
        ];
    }


    /**
     * Returns a simple array with language information
     */
    public function language_full_list()
    {
        // Get all languages
        $vec_languages = $this->get_predefined_list();

        // Remove existing languages
        $vec_language_models = Language::model()->findAll();
        if ( !empty($vec_language_models) )
        {
            foreach ( $vec_language_models as $language_model )
            {
                if ( isset($vec_languages[$language_model->language_id]) )
                {
                    unset($vec_languages[$language_model->language_id]);
                }
            }
        }

        foreach ( $vec_languages as $language_id => $que_language )
        {
            $vec_languages[$language_id] = $que_language[0];
            if ( isset($que_language[1]) )
            {
                $vec_languages[$language_id] .= ' ('. $que_language[1] .')';
            }
        }
        return $vec_languages;
    }


    /**
     * Automatic string representation for a model (from GIIX)
     */
    public static function representingColumn()
    {
        return 'name';
    }


    /**
     * Title used for this model
     */
    public function title()
    {
        return $this->name;
    }
}
