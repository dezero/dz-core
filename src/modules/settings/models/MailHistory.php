<?php
/**
 * @package dz\modules\settings\models 
 */

namespace dz\modules\settings\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\asset\models\AssetFile;
use dz\modules\settings\models\_base\MailHistory as BaseMailHistory;
use dz\modules\settings\models\MailTemplate;
use user\models\User;
use Yii;

/**
 * MailHistory model class for "mail_history" database table
 *
 * Columns in table "mail_history" available as properties of the model,
 * followed by relations of table "mail_history" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $history_id
 * @property string $subject
 * @property string $body
 * @property string $sender_name
 * @property string $sender_email
 * @property string $reply_email
 * @property string $recipient_emails
 * @property string $cc_emails
 * @property string $bcc_emails
 * @property integer $recipient_uid
 * @property string $eml_file_path
 * @property string $language_id
 * @property integer $template_id
 * @property string $template_alias
 * @property string $status_type
 * @property integer $send_date
 * @property integer $scheduled_date
 * @property integer $is_test
 * @property string $error_description
 * @property integer $entity_id
 * @property string $entity_type
 * @property string $entity_scenario
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $language
 * @property mixed $recipientUser
 * @property mixed $template
 * @property mixed $updatedUser
 */
class MailHistory extends BaseMailHistory
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
            ['subject, sender_name, sender_email, recipient_emails, created_date, created_uid, updated_date, updated_uid', 'required'],
            ['recipient_uid, template_id, send_date, scheduled_date, is_test, entity_id, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
            ['subject, sender_name, sender_email, reply_email, recipient_emails, cc_emails, bcc_emails, eml_file_path, error_description', 'length', 'max'=> 255],
            ['language_id', 'length', 'max'=> 4],
            ['template_alias', 'length', 'max'=> 64],
            ['entity_type, entity_scenario', 'length', 'max'=> 32],
            ['uuid', 'length', 'max'=> 36],
            ['status_type', 'in', 'range' => ['pending', 'scheduled', 'sent', 'error']],
            ['body, reply_email, cc_emails, bcc_emails, recipient_uid, eml_file_path, language_id, template_id, template_alias, status_type, send_date, scheduled_date, is_test, error_description, entity_id, entity_type, entity_scenario, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
            ['body', 'safe'],
            ['history_id, subject, body, sender_name, sender_email, reply_email, recipient_emails, cc_emails, bcc_emails, recipient_uid, eml_file_path, language_id, template_id, template_alias, status_type, send_date, scheduled_date, is_test, error_description, entity_id, entity_type, entity_scenario, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'language' => [self::BELONGS_TO, Language::class, 'language_id'],
			'recipientUser' => [self::BELONGS_TO, User::class, ['recipient_uid' => 'id']],
			'template' => [self::BELONGS_TO, MailTemplate::class, 'template_id'],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'send_date' => 'd/m/Y - H:i',
                    'scheduled_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }


	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'history_id' => Yii::t('app', 'History'),
			'subject' => Yii::t('app', 'Subject'),
			'body' => Yii::t('app', 'Body'),
            'sender_name' => Yii::t('app', 'From'),
            'sender_email' => Yii::t('app', 'From Email'),
            'reply_email' => Yii::t('app', 'Reply-To Email'),
            'recipient_emails' => Yii::t('app', 'To - Email Recipient(s)'),
            'cc_emails' => Yii::t('app', 'CC'),
            'bcc_emails' => Yii::t('app', 'BCC'),
			'recipient_uid' => Yii::t('app', 'User Recipient'),
			'eml_file_path' => Yii::t('app', 'Eml File Path'),
			'language_id' => Yii::t('app', 'Language'),
			'template_id' => Yii::t('app', 'Template'),
			'template_alias' => Yii::t('app', 'Template Alias'),
            'status_type' => Yii::t('app', 'Status'),
            'send_date' => Yii::t('app', 'Sending Date'),
            'scheduled_date' => Yii::t('app', 'Scheduled Date'),
            'is_test' => Yii::t('app', 'Test?'),
            'error_description' => Yii::t('app', 'Error Description'),
			'entity_id' => Yii::t('app', 'Entity'),
			'entity_type' => Yii::t('app', 'Entity Type'),
			'entity_scenario' => Yii::t('app', 'Entity Scenario'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'createdUser' => null,
			'language' => null,
			'recipientUser' => null,
			'template' => null,
		];
	}


    /**
     * Get "status_type" labels
     */
    public function status_type_labels()
    {
        return [
            'sent' => Yii::t('app', 'Sent'),
            'pending' => Yii::t('app', 'Pending'),
            'scheduled' => Yii::t('app', 'Scheduled'),
            'error' => Yii::t('app', 'Error'),
        ];
    }


    /**
     * Get "status_type" specific label
     */
    public function status_type_label($status_type)
    {
        $vec_labels = $this->status_type_labels();
        return isset($vec_labels[$status_type]) ? $vec_labels[$status_type] : '';
    }


    /**
     * Get "status_type" colors
     */
    public function status_colors()
    {
        return [
            'sent'      => 'green-800',
            'error'     => 'red-800',
            'pending'   => 'blue-800',
            'scheduled' => 'purple-800',
        ];
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        /*
        $criteria->compare('t.history_id', $this->history_id);
        $criteria->compare('t.subject', $this->subject, true);
        $criteria->compare('t.body', $this->body, true);
        $criteria->compare('t.sender_name', $this->sender_name, true);
        $criteria->compare('t.sender_email', $this->sender_email, true);
        $criteria->compare('t.reply_email', $this->reply_email, true);
        $criteria->compare('t.cc_emails', $this->cc_emails, true);
        $criteria->compare('t.bcc_emails', $this->bcc_emails, true);
        $criteria->compare('t.eml_file_path', $this->eml_file_path, true);
        $criteria->compare('t.template_alias', $this->template_alias, true);
        $criteria->compare('t.send_date', $this->send_date);
        $criteria->compare('t.scheduled_date', $this->scheduled_date);
        $criteria->compare('t.is_test', $this->is_test);
        $criteria->compare('t.error_description', $this->error_description, true);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.uuid', $this->uuid, true);
        */

        $criteria->compare('t.template_id', $this->template_id);
        $criteria->compare('t.recipient_emails', $this->recipient_emails, true);
        $criteria->compare('t.entity_id', $this->entity_id);
        $criteria->compare('t.entity_type', $this->entity_type);
        $criteria->compare('t.entity_scenario', $this->entity_scenario);
        
        // Status type filter
        if ( $this->status_type && $this->status_type !== 'all' )
        {
            $criteria->compare('t.status_type', $this->status_type);
        }

        // Order by default
        $criteria->order = 't.history_id DESC';
        if ( $this->status_type === 'scheduled' )
        {
            $criteria->order = 't.scheduled_date ASC';
        }
        else if ( $this->status_type === 'sent' )
        {
            $criteria->order = 't.send_date DESC';
        }

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            // 'sort' => ['defaultOrder' => ['history_id' => true]]
        ]);
    }


    /**
     * MailHistory models list
     * 
     * @return array
     */
    public function mailhistory_list()
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['history_id', 'subject'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = MailHistory::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('history_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Status list values
     */
    public function status_list()
    {
        return [
            'pending'   => 'Pending',
            'scheduled' => 'Scheduled',
            'sent'      => 'Sent',
            'error'     => 'Error'
        ];
    }


    /**
     * Get body (convert from JSON)
     */
    public function get_body()
    {
        $vec_data_json = $this->get_data();
        if ( !empty($vec_data_json) && isset($vec_data_json['body']) )
        {
            return $vec_data_json['body'];
        }

        return $this->body;
    }


    /**
     * Return Json data
     */
    public function get_data()
    {
        return Json::decode($this->body);
    }


    /**
     * Get content from EML file
     * 
     * @see https://github.com/php-mime-mail-parser/php-mime-mail-parser
     */
    public function get_eml_content()
    {
        $content = '';

        if ( !empty($this->eml_file_path) )
        {
            $vec_mail_config = Yii::app()->mail->getConfig();
            if ( !empty($vec_mail_config) && isset($vec_mail_config['savePath']) )
            {
                // Get path where eml files are saved
                $view_path = Yii::getAlias($vec_mail_config['savePath']);
                $eml_file = Yii::app()->file->set($view_path . DIRECTORY_SEPARATOR . $this->eml_file_path);
                if ( $eml_file && $eml_file->getExists() )
                {
                    $mail_parser = new \PhpMimeMailParser\Parser();
                    $mail_parser->setText($eml_file->getContents());
                    return $mail_parser->getMessageBody('htmlEmbedded');
                }
            }
        }

        return $content;
    }


    /**
     * Sends the saved email
     */
    public function send_email($is_save_history = false)
    {
        // Recover data. It's enconded in JSON format on body field
        $vec_data = $this->get_data();
        if ( !empty($vec_data) && isset($vec_data['body']) )
        {
            // Entity information
            if ( !empty($this->entity_id) && !empty($this->entity_type) )
            {
                Yii::app()->mail->setEntityInfo($this->entity_id, $this->entity_type, $this->entity_scenario);
            }

            // MailManager component
            $mail = Yii::app()->mail
                ->setTo($this->recipient_emails)
                ->addData($vec_data)
                ->compose($this->template_alias)
                ->setFrom($this->sender_email, $this->sender_name)
                ->setSubject($this->subject, false)
                ->setBody($vec_data['body'], false);

            // Add reply email
            if ( !empty($this->reply_email) )
            {
                $mail->setReplyTo($this->reply_email);
            }

            // Send email
            $sending_result = $mail->send($is_save_history);

            // Email is sent OK -> Update MailHistory model attributes
            if ( $sending_result )
            {
                $this->eml_file_path = Yii::app()->mail->get_eml_filepath();
                $this->status_type = 'sent';
                $this->send_date = time();
            }

            // Error sending email -> Update MailHistory model attributes
            else
            {
                $this->status_type = 'error';
                $this->error_description = Yii::app()->mail->getError();

                // Error description has a maximum length of 255 characvteres
                if ( StringHelper::strlen($this->error_description) > 255 )
                {
                    $this->error_description = StringHelper::substr($this->error_description, 0, 254);
                }
            }

            // Save model
            if ( ! $this->save() )
            {
                Log::save_model_error($this);
            }

            return $sending_result;
        }

        return false;
    }
}
