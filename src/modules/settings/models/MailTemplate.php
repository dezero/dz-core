<?php
/**
 * @package dz\modules\settings\models 
 */

namespace dz\modules\settings\models;

use dz\components\Mailer;
use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\MarkdownHelper;
use dz\helpers\StringHelper;
use dz\modules\settings\models\_base\MailTemplate as BaseMailTemplate;
use dz\modules\settings\models\Language;
use dz\modules\settings\models\MailHistory;
use user\models\User;
use Yii;

/**
 * MailTemplate model class for "mail_template" database table
 *
 * Columns in table "mail_template" available as properties of the model,
 * followed by relations of table "mail_template" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $template_id
 * @property string $alias
 * @property string $language_id
 * @property string $name
 * @property string $description
 * @property string $sender_name
 * @property string $sender_email
 * @property string $reply_email
 * @property string $recipient_emails
 * @property string $cc_emails
 * @property string $bcc_emails
 * @property string $source_type
 * @property string $subject
 * @property string $template_file_path
 * @property string $body
 * @property string $help_text
 * @property integer $disable_date
 * @property integer $disable_uid
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $mailHistories
 * @property mixed $createdUser
 * @property mixed $disableUser
 * @property mixed $language
 * @property mixed $updatedUser
 */
class MailTemplate extends BaseMailTemplate
{
    /**
     * @var string. View file for mail template
     */
    public $template_file_path = 'notification.tpl.php';


	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['alias, name, sender_name, sender_email, subject, created_date, created_uid, updated_date, updated_uid', 'required'],
			['disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['alias', 'length', 'max'=> 64],
			['language_id', 'length', 'max'=> 4],
			['name', 'length', 'max'=> 128],
			['sender_name, sender_email, reply_email, recipient_emails, cc_emails, bcc_emails, subject, template_file_path', 'length', 'max'=> 255],
			['uuid', 'length', 'max'=> 36],
			['source_type', 'in', 'range' => ['database', 'file']],
			['language_id, description, reply_email, recipient_emails, cc_emails, bcc_emails, source_type, template_file_path, body, help_text, disable_date, disable_uid, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['description, body, help_text', 'safe'],
			['template_id, alias, language_id, name, description, sender_name, sender_email, reply_email, recipient_emails, cc_emails, bcc_emails, source_type, subject, template_file_path, body, help_text, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],

            // Custom rules
            ['alias', 'validate_alias', 'on' => 'insert'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'mailHistories' => [self::HAS_MANY, MailHistory::class, 'template_id'],
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'disableUser' => [self::BELONGS_TO, User::class, ['disable_uid' => 'id']],
			'language' => [self::BELONGS_TO, Language::class, 'language_id'],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
            'translations' => [self::HAS_MANY, MailTemplate::class, ['alias' => 'alias'], 'condition' => 'translations.language_id <> t.language_id'],
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'template_id' => Yii::t('app', 'Template'),
			'alias' => Yii::t('app', 'Alias'),
			'language_id' => Yii::t('app', 'Language'),
			'name' => Yii::t('app', 'Title'),
			'description' => Yii::t('app', 'Description'),
			'sender_name' => Yii::t('app', 'From Name'),
			'sender_email' => Yii::t('app', 'From Email'),
			'reply_email' => Yii::t('app', 'Reply-To Email'),
			'recipient_emails' => Yii::t('app', 'Email Recipient(s)'),
			'cc_emails' => Yii::t('app', 'CC'),
			'bcc_emails' => Yii::t('app', 'BCC'),
			'source_type' => Yii::t('app', 'Source Type'),
			'subject' => Yii::t('app', 'Subject'),
			'template_file_path' => Yii::t('app', 'Template File'),
			'body' => Yii::t('app', 'Email Body (HTML)'),
			'help_text' => Yii::t('app', 'Help Text'),
			'disable_date' => Yii::t('app', 'Disable Date'),
			'disable_uid' => null,
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'mailHistories' => null,
			'createdUser' => null,
			'disableUser' => null,
			'language' => null,
			'updatedUser' => null,
		];
	}


    /**
     * Get "source_type" labels
     */
    public function source_type_labels()
    {
        return [
            'database' => Yii::t('app', 'database'),
            'file' => Yii::t('app', 'file'),
        ];
    }


    /**
     * Get "source_type" specific label
     */
    public function source_type_label($source_type)
    {
        $vec_labels = $this->source_type_labels();
        return isset($vec_labels[$source_type]) ? $vec_labels[$source_type] : '';
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.template_id', $this->template_id);
        $criteria->compare('t.alias', $this->alias, true);
        $criteria->compare('t.language_id', $this->language_id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.description', $this->description, true);
        $criteria->compare('t.sender_name', $this->sender_name, true);
        $criteria->compare('t.sender_email', $this->sender_email, true);
        $criteria->compare('t.reply_email', $this->reply_email, true);
        $criteria->compare('t.recipient_emails', $this->recipient_emails, true);
        $criteria->compare('t.cc_emails', $this->cc_emails, true);
        $criteria->compare('t.bcc_emails', $this->bcc_emails, true);
        $criteria->compare('t.source_type', $this->source_type, true);
        $criteria->compare('t.subject', $this->subject, true);
        $criteria->compare('t.template_file_path', $this->template_file_path, true);
        $criteria->compare('t.body', $this->body, true);
        $criteria->compare('t.help_text', $this->help_text, true);
        $criteria->compare('t.disable_date', $this->disable_date);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['alias' => false]]
        ]);
    }


    /**
     * MailTemplate models list
     * 
     * @return array
     */
    public function mailtemplate_list()
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['template_id', 'name'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = MailTemplate::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('template_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Disables MailTemplate model
     */
    public function disable()
    {
        if ( parent::disable() )
        {
            if ( $this->language_id === Yii::app()->i18n->get_default_language() && $this->translations )
            {
                foreach ( $this->translations as $translated_mail_model )
                {
                    $translated_mail_model->disable();
                }
            }

            return true;
        }

        return false;
    }


    /**
     * Enables MailTemplate model
     */
    public function enable()
    {
        if ( parent::enable() )
        {
            if ( $this->language_id === Yii::app()->i18n->get_default_language() && $this->translations )
            {
                foreach ( $this->translations as $translated_mail_model )
                {
                    $translated_mail_model->enable();
                }
            }

            return true;
        }

        return false;
    }


    /**
     * Deletes the model
     */
    public function delete()
    {
        // TRANSLATIONS
        if ( $this->language_id === Yii::app()->i18n->get_default_language() && $this->translations )
        {
            foreach ( $this->translations as $translated_mail_model )
            {
                $translated_mail_model->delete();
            }
        }

        // Going on with "delete" action
        return parent::delete();
    }


    /**
     * Get the translated MailTemplate model given a language
     */
    public function get_translation($language_id)
    {
        return MailTemplate::get()
            ->where([
                'alias'         => $this->alias,
                'language_id'   => $language_id
            ])
            ->one();
    }


    /**
     * Get same template translated into another language
     */
    public function get_translated_model($language_id)
    {
        if ( $language_id != $this->language_id )
        {
            $translated_mail_model = $this->findByAlias($this->alias, $language_id);
            if ( !empty($translated_mail_model) )
            {
                // Assign same values from original language
                // $translated_mail_model->vec_extra_data = $this->vec_extra_data;
                return $translated_mail_model;
            }
        }

        return $this;
    }


    /**
     * Save translation
     */
    public function save_translation($vec_attributes, $language_id)
    {
        $vec_attributes['language_id'] = $language_id;
        $translated_model = $this->get_translation($language_id);

        if ( ! $translated_model )
        {
            $translated_model = Yii::createObject(MailTemplate::class);
            $translated_model->alias = $this->alias;
        }

        $translated_model->setAttributes($vec_attributes);
        return $translated_model->save();
    }


     /**
     * Save a translation model
     */
    public function save_translation_model($translated_mail_model)
    {
        if ( empty($translated_mail_model->template_id) || $translated_mail_model->alias == $this->alias )
        {
            $translated_mail_model->alias = $this->alias;
            $translated_mail_model->name = $this->name;
            $translated_mail_model->description = $this->description;
            $translated_mail_model->sender_name = $this->sender_name;
            $translated_mail_model->sender_email = $this->sender_email;
            $translated_mail_model->reply_email = $this->reply_email;
            $translated_mail_model->recipient_emails = $this->recipient_emails;
            $translated_mail_model->cc_emails = $this->cc_emails;
            $translated_mail_model->bcc_emails = $this->bcc_emails;
            $translated_mail_model->source_type = $this->source_type;
            $translated_mail_model->template_file_path = $this->template_file_path;
            $translated_mail_model->help_text = $this->help_text;

            if ( ! $translated_mail_model->save() )
            {
                Log::save_model_error($translated_mail_model);
                return false;
            }

            return true;
        }

        return false;
    }


    /**
     * Sends an email
     */
    public function send_email($to_address = '', $language_id = '', $is_save_history = true)
    {
        // Send mail in another language?
        if ( !empty($language_id) && $this->language_id !== $language_id )
        {
            Yii::app()->language = $language_id;
            $translated_mail_model = $this->get_translated_model($language_id);
            return $translated_mail_model->send_email($to_address, '', $is_save_history);
        }

        $vec_mail_attributes = [];
        $mailer = new Mailer();
        $mailer->setTo($to_address);
        $mailer->setSubject($this->subject);
        $mailer->setView('notification');
        $mailer->setData($vec_mail_attributes);
        $send_email_result = $mailer->send_email();
    }


    /**
     * Get mail template files
     */
    public function get_template_files()
    {
        $vec_template_files = [];

        // Get mail path from mail.php configuration file
        $vec_mail_config = Yii::app()->mail->getConfig();
        if ( !empty($vec_mail_config) && isset($vec_mail_config['viewPath']) )
        {
            // Get all the files inside "viewPath"
            $view_path = Yii::getAlias($vec_mail_config['viewPath']);
            $view_dir = Yii::app()->file->set($view_path);
            if ( $view_dir && $view_dir->getExists() && $view_dir->getIsDir() )
            {
                $vec_files = $view_dir->getContents();
                if ( !empty($vec_files) )
                {
                    foreach ( $vec_files as $que_file )
                    {
                        // Only .tpl.php files are allowed.
                        if ( preg_match("/\.tpl\.php$/", $que_file) )
                        {
                            // Remove full path and remove "/" at the beginning
                            $file_name = substr(str_replace($view_path, '', $que_file), 1);
                            
                            // Exclude partials (starting with "_")
                            if ( ! preg_match("/^\_/", $file_name) )
                            {
                                $vec_template_files[$file_name] = $file_name;
                            }
                        }
                    }
                }
            }
        }
        
        return $vec_template_files;
    }


    /**
     * Get rendered subject with parsered tokens
     */
    public function get_rendered_subject()
    {
        $rendered_subject = Yii::app()->mail->getSubject();
        if ( empty($rendered_subject) )
        {
            $rendered_subject = Yii::app()->mail
                ->compose($this->alias)
                ->render()
                ->getSubject();
        }

        return $rendered_subject;
    }


    /**
     * Get body content rendered with full HTML layout and parsered tokens
     */
    public function get_rendered_body()
    {
        $rendered_body = Yii::app()->mail->getBody();
        if ( empty($rendered_body) )
        {
            $rendered_body = Yii::app()->mail
                ->compose($this->alias)
                ->render()
                ->getBody();
        }

        return $rendered_body;
    }


    /**
     * Validate alias attribute
     */
    public function validate_alias($attribute, $params)
    {
        $alias_mail_model = self::findByAlias($this->alias, $this->language_id);
        if ( $alias_mail_model )
        {
            $this->addError('alias',  Yii::t('app', 'It already exists a mail template with alias <em>'. $this->alias .'</em>.'));
        }
    }
}