<?php
/*
|--------------------------------------------------------------------------
| Batch job model to import users via Excel
|--------------------------------------------------------------------------
*/

namespace dz\modules\settings\models;

use dz\batch\ImportExcel as BaseImportExcel;
use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Json;
use dz\helpers\StringHelper;
use dz\helpers\Url;
use dz\batch\Batch as Batch;
use dz\modules\asset\models\AssetFile;
use dz\modules\settings\models\Language;
use dz\modules\settings\models\TranslationMessage;
use dz\modules\settings\models\TranslationSource;
use user\models\User;
use Yii;

/**
 * Batch model class for "batch" database table
 *
 * Columns in table "batch" available as properties of the model,
 * followed by relations of table "batch" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $batch_id
 * @property string $batch_type
 * @property string $name
 * @property string $description
 * @property string $last_operation_name
 * @property integer $last_operation_num
 * @property integer $total_operations
 * @property string $operations_json
 * @property integer $total_items
 * @property integer $num_errors
 * @property integer $num_warnings
 * @property integer $item_starting_num
 * @property integer $item_ending_num
 * @property string $model_id
 * @property string $model_class
 * @property string $model_type
 * @property string $model_scenario
 * @property integer $file_id
 * @property string $summary_json
 * @property string $results_json
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $file
 * @property mixed $createdUser
 * @property mixed $updatedUser
 */
class TranslationImportExcel extends BaseImportExcel
{
    /**
     * @var string. Batch job name
     */
    public $name = 'translations';


    /**
     * @var string. User model class
     */
    public $model_class = 'TranslationMessage';


    /**
     * @var Language. Current Language model
     */
    private $language_model;


    /**
     * @var array. Array with translation categories
     */
    private $vec_translation_categories;


    /**
     * @var int. Current row number
     */
    private $num_row;


    /**
     * Constructor
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;

        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.name', $this->name);
        $criteria->compare('t.batch_type', $this->batch_type);
        // $criteria->compare('t.model_id', $this->model_id);
        // $criteria->compare('t.model_class', $this->model_class);
        // $criteria->compare('t.model_scenario', $this->model_scenario);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 10],
            'sort' => ['defaultOrder' => ['batch_id' => true]]
        ]);
    }


    /**
     * Import rows from Excel file
     */
    public function import_rows($vec_rows)
    {
        // Init vars
        $this->vec_results_json = [];
        $this->vec_summary_json = [
            'language_id'   => $this->language_model->language_id,
            'inserted'      => 0,
            'updated'       => 0,
            'errors'        => 0
        ];
        $this->vec_translation_categories = Yii::app()->i18n->get_translation_categories();

        return parent::import_rows($vec_rows);
    }


    /**
     * Import a single row from Excel file
     */
    public function import_row($vec_row, $num_row)
    {
        // Save current row
        $this->num_row = $num_row;

        // Exclude "EMPTY" rows. Check first column "Holder Name" is not empty
        if ( !empty($vec_row) && isset($vec_row['A']) && !empty(StringHelper::trim($vec_row['A'])) )
        {
            // Update total_items
            $this->total_items++;

            /*
            |--------------------------------------------------------------------------
            | Row information:
            |
            |   [A] => source_message
            |   [B] => translated_message
            |   [C] => category (optional)
            |
            */

            // Validate row
            $this->validate_row($vec_row);

            // Save translation
            if ( ! $this->is_error($this->num_row) )
            {
                $this->save_translation($vec_row);
            }

            // Save import result (totals and errors)
            $this->save_import_results($vec_row);
        }

        // Update total num_errors
        $this->num_errors = $this->vec_summary_json['errors'];

        return true;
    }


    /**
     * Validate current row
     */
    public function validate_row($vec_row)
    {
        // #0 - Columns number is incorrect
        if ( count($vec_row) < 2 )
        {
            $this->add_error($this->num_row, 'wrong_columns');
        }

        else
        {
            // "Translated message" column is required
            if ( empty($vec_row['B']) )
            {
                $this->add_error($this->num_row, 'translation_required');
            }

            // "Category" column validation
            if ( !empty($vec_row['C']) && !isset($this->vec_translation_categories[$vec_row['C']]) )
            {
                $this->add_error($this->num_row, 'category_not_found');
            }
        }
    }


    /**
     * Save translation
     */
    public function save_translation($vec_row)
    {
        // Trim values
        $vec_row['A'] = StringHelper::trim($vec_row['A']);
        $vec_row['B'] = StringHelper::trim($vec_row['B']);
        $vec_row['C'] = StringHelper::trim($vec_row['C']);

        // Category
        $category = !empty($vec_row['C']) ? $vec_row['C'] : 'app';

        // Existing TranslationSource model?
        $translation_source_model = TranslationSource::get()
            ->where([
                'category'  => $category,
                'message'   => $vec_row['A']
            ])
            ->one();

        // Create new TranslationSource model
        $is_error = false;
        if ( ! $translation_source_model )
        {
            $translation_source_model = Yii::createObject(TranslationSource::class);
            $translation_source_model->category = $category;
            $translation_source_model->message = $vec_row['A'];
            if ( ! $translation_source_model->save() )
            {
                $is_error = true;
                $this->add_error($this->num_row, 'validate_error', 'Translation source could not be created');
                $this->add_error($this->num_row, 'validate_error', $translation_source_model->getErrors());
            }
        }

        if ( ! $is_error )
        {
            if ( ! $translation_source_model->save_translation($this->language_model->language_id, $vec_row['B']) )
            {
                $this->add_error($this->num_row, 'validate_error', 'Translation message could not be saved');
            }
        }
    }


    /**
     * Get error description
     */
    public function get_error_description($error_code, $error_content = '')
    {
        switch ( $error_code )
        {
            case 'wrong_columns':
                return Yii::t('app', 'Excel file - Incorrect number of columns');
            break;

            case 'translation_required':
                return Yii::t('app', 'Translation message is required');
            break;

            case 'category_not_found':
                return Yii::t('app', 'Translation category does not exist. Allowed values: '. Json::encode(array_keys(Yii::app()->i18n->get_translation_categories())));
            break;

            case 'validate_error':
                if ( ! empty($error_content) )
                {
                    if ( is_array($error_content) )
                    {
                        $vec_errors = [];
                        foreach ( $error_content as $attribute_name => $vec_attribute_errors )
                        {
                            foreach ( $vec_attribute_errors as $error_msg )
                            {
                                // $vec_errors[] = $attribute_name .' - '. $error_msg;
                                $vec_errors[] = $error_msg;
                            }
                        }
                        return $vec_errors;
                    }

                    return $error_content;
                }

                return Yii::t('app', 'Translation could not be imported');
            break;
        }

        return '';
    }


    /**
     * Save import result (totals and errors)
     */
    private function save_import_results($vec_row)
    {
        if ( ! $this->is_error($this->num_row) )
        {
            $this->vec_summary_json['inserted']++;
        }

        // Save ERROR information
        if ( $this->is_error($this->num_row) )
        {
            $this->vec_summary_json['errors']++;
            $this->vec_results_json[$this->num_row] = [
                'errors'    => $this->get_errors($this->num_row),
                'data'      => $vec_row
            ];
            if ( empty($this->vec_results_json[$this->num_row]['errors']) )
            {
                unset($this->vec_results_json[$this->num_row]['errors']);
            }
        }

        // Save SUCCESS information
        else
        {
            $this->vec_results_json[$this->num_row] = [
                'data'  => $vec_row
            ];
        }
    }


    /**
     * Set Language model
     */
    public function set_language($language_model)
    {
        $this->language_model = $language_model;
    }


    /**
     * Return Language model
     */
    public function get_language()
    {
        if ( empty($this->language_model) && !empty($this->summary_json) )
        {
            if ( isset($this->summary_json['language_id']) )
            {
                $this->language_model = Language::findOne($this->summary_json['language_id']);
            }
        }

        return $this->language_model;
    }
}
