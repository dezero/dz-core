<?php
/**
 * @package dz\modules\settings\models 
 */

namespace dz\modules\settings\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\settings\models\_base\TranslationMessage as BaseTranslationMessage;
use dz\modules\settings\models\Language;
use dz\modules\settings\models\TranslationSource;
use user\models\User;
use Yii;

/**
 * TranslationMessage model class for "translation_message" database table
 *
 * Columns in table "translation_message" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $id
 * @property string $language_id
 * @property string $translation
 * @property integer $updated_date
 * @property integer $updated_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class TranslationMessage extends BaseTranslationMessage
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['id, updated_date, updated_uid', 'required'],
			['id, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['language_id', 'length', 'max'=> 4],
			['language_id, translation', 'default', 'setOnEmpty' => true, 'value' => null],
			['translation', 'safe'],
			['id, language_id, translation, updated_date, updated_uid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'source' => [self::BELONGS_TO, TranslationSource::class, 'id'],
            'language' => [self::BELONGS_TO, Language::class, 'language_id'],
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'id' => null,
			'language_id' => null,
			'translation' => Yii::t('app', 'Translation'),
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.translation', $this->translation, true);
        $criteria->compare('t.updated_date', $this->updated_date);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['id' => true]]
        ]);
    }


    /**
     * Automatic string representation for a model (from GIIX)
     */
    public static function representingColumn()
    {
        return 'translation';
    }


    /**
     * Title used for this model
     */
    public function title()
    {
        return $this->translation;
    }
}