<?php
/*
|--------------------------------------------------------------------------
| Controller class for updating profile
|--------------------------------------------------------------------------
*/

namespace dz\modules\user\controllers;

use dz\helpers\Log;
use dz\helpers\Json;
use dz\helpers\StringHelper;
use dz\modules\user\services\ProfileUpdateService;
use dz\web\Controller;
use user\models\User;
use Yii;

class ProfileController extends Controller
{
    /**
     * Update profile
     */
    public function actionIndex()
    {
        // User model
        $user_model = Yii::app()->user->model();;

        // Data submitted?
        if ( isset($_POST['User']) )
        {
            // Password has changed?
            $is_password_changed = false;
            if ( ! $user_model->isNewRecord )
            {
                $is_password_changed = true;
                if ( !isset($_POST['is-password-changed']) || $_POST['is-password-changed'] != 1 )
                {
                    $is_password_changed = false;
                    if ( isset($_POST['User']['password']) )
                    {
                        unset($_POST['User']['password']);
                    }
                    if ( isset($_POST['User']['verify_password']) )
                    {
                        unset($_POST['User']['verify_password']);
                    }
                }
            }
            $user_model->setAttributes($_POST['User']);

            // Update user via ProfileUpdateService class
            $profile_update_service = Yii::createObject(ProfileUpdateService::class, [$user_model, $is_password_changed]);
            if ( $profile_update_service->run() )
            {
                // Show ERROR messages
                $vec_errors = $profile_update_service->get_errors();
                if ( !empty($vec_errors) )
                {
                    Yii::app()->user->addFlash('error', $vec_errors);
                }

                // Show SUCCESS messages
                $vec_success_messages = $profile_update_service->get_flash_messages('success');
                if ( !empty($vec_success_messages) )
                {
                    Yii::app()->user->addFlash('success', $vec_success_messages);
                }

                // Success message & redirect
                Yii::app()->user->addFlash('success', Yii::t('backend', 'Profile updated successfully'));
                $this->redirect(['/user/profile']);
            }
            else
            {
                $vec_errors = $profile_update_service->get_errors();
                if ( !empty($vec_errors) )
                {
                    Yii::app()->user->addFlash('error', $vec_errors);
                }
            }
        }

        // Password
        $user_model->password = '';
        $user_model->verify_password = '';

        // Render form
        $this->render('//user/profile/index', [
            'user_model'    => $user_model,
            'form_id'       => 'profile-form',
        ]);
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            // Access to any logged_in user
            'index'    => 'is_logged_in',
        ];
    }
}
