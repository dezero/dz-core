<?php
/*
|--------------------------------------------------------------------------
| Controller class for user Logout
|--------------------------------------------------------------------------
*/

namespace dz\modules\user\controllers;

use dz\web\Controller;
use user\models\User;
use Yii;

class LogoutController extends Controller
{
	/**
	 * Default action
	 *
	 * @var string
	 */
	public $defaultAction = 'logout';
	
	
	/**
	 * Logout the current user and redirect to returnLogoutUrl
	 */
	public function actionLogout()
	{
        // Get current user
        $user_model = Yii::app()->user->model();

        // Make "logout" process
		Yii::app()->user->logout();

        // After logout redirect
        Yii::app()->userManager->afterLogout($user_model);
	}


    /**
     * Logout and login as another user
     * 
     * This function is exclusive for SUPERADMIN
     */
    public function actionLoginAs()
    {
        if ( ! Yii::app()->user->isSuperadmin() )
        {
            throw new \CHttpException(401, Yii::t('app', 'Access denied'));
        }

        // Get "token" needed for "Login As..." function
        $user_model = User::findOne(Yii::app()->user->id);
        if ( ! $user_model )
        {
            throw new \CHttpException(401, Yii::t('app', 'Access denied'));
        }        

        // Make "logout" process
        Yii::app()->user->logout();

        // Redirect to login page with a special param
        $this->redirect(['/user/login', 'token' => $user_model->token()]);
    }
}