<?php
/**
 * @package user\controllers 
 */

namespace dz\modules\user\controllers;

use dz\helpers\Log;
use dz\web\Controller;
use user\models\User;
use Yii;

/**
 * Controller class for email verification
*/
class EmailController extends Controller
{
    /**
     * Default action
     *
     * @var string
     */
    public $defaultAction = 'verify';


    /**
     * Email verification page
     */
    public function actionVerify($id, $code)
    {
        if ( Yii::app()->user->isGuest )
        {
            $user_model = User::findOne($id);
            $user_model->scenario = 'email';

            // Check verification code
            if ( $user_model && $user_model->verify_email($code) )
            {
                if ( $user_model->save() )
                {
                    // Success message
                    Yii::app()->user->addFlash('loginMessage', Yii::t('app', 'Email has been verified successfully.'));

                    Yii::app()->userManager->afterVerifyEmail($user_model);
                }
                else
                {
                    Log::save_model_error($user_model);
                }
            }
        }
        
        throw new \CHttpException(403, Yii::t('app', 'Access denied.'));
    }
}
