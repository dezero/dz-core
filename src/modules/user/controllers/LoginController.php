<?php
/**
 * @package user\controllers 
 */

namespace dz\modules\user\controllers;

use dz\helpers\Log;
use dz\web\Controller;
use user\models\User;
// use user\models\UserLogin;
use Yii;

/**
 * Controller class for user Login 
*/
class LoginController extends Controller
{
	/**
	 * Default action
	 *
	 * @var string
	 */
	public $defaultAction = 'login';


	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if ( Yii::app()->user->isGuest )
		{
            $user_model = Yii::createObject(User::class);
            $user_model->scenario = 'login';

			// Collect user input data
			if ( isset($_POST['User']) )
			{
				$user_model->attributes = $_POST['User'];

				// Validate user input and redirect to previous page if valid
                if ( $user_model->validate() )
                {
                    Yii::app()->userManager->afterLogin($user_model);
                }
                else
                {
                    Yii::app()->userManager->afterErrorLogin($user_model);
                }
			}

			// Get USER from $_GET
			if ( isset($_GET['u']) && !empty($_GET['u']) && empty($user_model->username) )
			{
				$user_model->username = $_GET['u'];
			}

            // Check if "token" exists and is valid to do "loginAs" method
            $loginas_token = '';
            if ( isset($_GET['token']) && !empty($_GET['token']) )
            {
                $loginas_user_model = User::model()->findByToken($_GET['token']);
                if ( $loginas_user_model )
                {
                    $vec_roles = $loginas_user_model->roles();
                    if ( !empty($vec_roles) && isset($vec_roles['admin']) )
                    {
                        $loginas_token = $_GET['token'];
                    }
                }
            }
			
			// Display login form
			$this->render('//user/user/login', [
                'model'         => $user_model,
                'loginas_token' => $loginas_token
            ]);
		}

         // Already logged in, redirect user
        else
        {
            $user_model = Yii::app()->user->model();
            Yii::app()->userManager->afterLogin($user_model);
            // $this->redirect(Yii::app()->user->returnUrl);
        }
	}
}