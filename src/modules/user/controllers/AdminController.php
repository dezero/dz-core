<?php
/*
|--------------------------------------------------------------------------
| Controller class for admin users
|--------------------------------------------------------------------------
*/

namespace dz\modules\user\controllers;

use dz\helpers\AuthHelper;
use dz\helpers\Html;
use dz\helpers\StringHelper;
use dz\web\Controller;
use user\models\User;
use Yii;

class AdminController extends Controller
{
	/**
	 * Default action
	 *
	 * @var string
	 */
	public $defaultAction = 'admin';
	
	
	/**
	 * List action for User models
	 */
	public function actionAdmin()
	{
        $model = Yii::createObject(User::class, 'search');
		$model->unsetAttributes();
		
		if ( isset($_GET['User']) )
		{
			$model->setAttributes($_GET['User']);
		}

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }
		
		$this->render('index', [
            'model'             => $model,
            'vec_list_roles'    => AuthHelper::role_list()
		]);
	}
	

	/**
	 * Create action for User model
	 */
	public function actionCreate()
	{
		$user_model = Yii::createObject(User::class);
		
		// Auth Manager
		$am = Yii::app()->getAuthManager();
		
		if ( isset($_POST['User']) )
		{
			$user_model->attributes = $_POST['User'];
			$user_model->verification_code = StringHelper::encrypt(microtime() . $user_model->password);
			
			if ( $user_model->validate() )
			{
				// Password validation
				$user_model->password = StringHelper::encrypt($user_model->password);
				$user_model->save(false);
				
				// Roles assignment
				$vec_post_roles = [];
				if ( isset($_POST['user_roles']) )
				{
					$vec_post_roles = $_POST['user_roles'];
					foreach ( $vec_post_roles as $que_role )
					{
						if ( !$am->isAssigned($que_role, $user_model->id) )
						{
							$am->assign($que_role, $user_model->id);
							if ($am instanceof \CPhpAuthManager)
							{
								$am->save();
							}
							if ($am instanceof \ICachedAuthManager)
							{
								$am->flushAccess($que_role, $user_model->id);
							}
						}
					}
				}

				Yii::app()->user->addFlash('success', Yii::t('app', 'Usuario creado correctamente'));

				$this->redirect(['update','id' => $user_model->id]);
			}
			else
			{
				$vec_errors = $user_model->getErrors();
				if ( !empty($vec_errors) )
				{
					foreach ( $vec_errors as $que_error )
					{
						Yii::app()->user->addFlash('error', $que_error);
					}
				}				
			}
		}

		// Roles list
		$vec_roles = $am->getAuthItems(2);
		$vec_list_roles = [];
		if ( !empty($vec_roles) )
		{
			foreach ( $vec_roles as $que_role )
			{
                // Only users with "user_full_manage" can create ADMIN users
                if ( $que_role->name != 'admin' || Yii::app()->user->checkAccess('user_full_manage') )
                {
				    $vec_list_roles[$que_role->name] = $que_role->description;
                }
			}
		}

		$this->render('create', [
			'model' => $user_model,
			'roles' => $vec_list_roles,
		]);
	}


	/**
	 * Update action for User model
	 */
	public function actionUpdate($id)
	{
		$user_model = User::findOne($id);
        if ( ! $user_model )
        {
            throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }
	
		// Auth Manager
		$am = Yii::app()->getAuthManager();
		$vec_assigned_roles = $am->getAuthItems(2, $user_model->id);

        // Check if current user has full permissions to update an ADMIN users
        if ( ! Yii::app()->user->checkAccess('user_full_manage') && isset($vec_assigned_roles['admin']) && Yii::app()->user->id !== $id )
        {
            throw new \CHttpException(401, Yii::t('app', 'Access denied'));
        }

		if ( isset($_POST['User']) )
		{
			$user_model->attributes = $_POST['User'];
			
			if ( $user_model->validate() )
			{
				// Roles assignment
				$vec_post_roles = [];
				if ( isset($_POST['user_roles']) )
				{
					$vec_post_roles = $_POST['user_roles'];
					foreach ( $vec_post_roles as $que_role )
					{
						if ( !$am->isAssigned($que_role, $user_model->id) )
						{
							$am->assign($que_role, $user_model->id);
							if ($am instanceof \CPhpAuthManager)
							{
								$am->save();
							}
							if ($am instanceof \ICachedAuthManager)
							{
								$am->flushAccess($que_role, $user_model->id);
							}
						}
					}
				}

				// Revoke assignments
				foreach ( $vec_assigned_roles as $role_item )
				{
					if ( !in_array($role_item->name, $vec_post_roles) AND $am->isAssigned($role_item->name, $user_model->id) )
					{
						$am->revoke($role_item->name, $user_model->id);
						if ($am instanceof \CPhpAuthManager)
						{
							$am->save();
						}
						if ($am instanceof \ICachedAuthManager)
						{
							$am->flushAccess($role_item->name, $user_model->id);
						}
					}
				}
	
				// Change password?
				$old_password = User::findOne($user_model->id);
				if ( $old_password->password !== $user_model->password )
				{
                    $user_model->change_password($user_model->password, false);
				}
				$user_model->save(false);
				
				Yii::app()->user->addFlash('success', Yii::t('app', 'Usuario actualizado correctamente'));

				$this->redirect(['/user/admin/update', 'id' => $user_model->id]);
			}
			else
			{
				$vec_errors = $user_model->getErrors();
				if ( !empty($vec_errors) )
				{
					foreach ( $vec_errors as $que_error )
					{
						Yii::app()->user->addFlash('error', $que_error);
					}
				}					
			}
		}
	
		$this->render('update', [
			'model' => $user_model,
			'roles' => AuthHelper::roles_tasks($id),
    		'tasks' => [
    			'assigned' => AuthHelper::manual_tasks($id)
    		]
		]);
	}


    /**
     * View action for User model
     */
    public function actionView($id)
    {
        $user_model = User::findOne($id);
        if ( ! $user_model )
        {
            throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }
        
        $this->render('view', [
            'model' => $user_model,
            'roles' => AuthHelper::roles_tasks($id, true),
            'tasks' => [
                'assigned' => AuthHelper::manual_tasks($id)
            ]
        ]);
    }


	/**
	 * Permissions action for User model
	 *
	 * @return HTML
	 */
	public function actionPermission($id)
	{
		$user_model = User::findOne($id);
        if ( ! $user_model )
        {
            throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }

		// Auth Manager
		$am = Yii::app()->getAuthManager();
		$vec_roles = AuthHelper::roles_tasks($id, TRUE);
		$vec_tasks_assigned = $am->getAuthItems(1, $id);
        $vec_assigned_roles = $am->getAuthItems(2, $user_model->id);
    
        // Check if current user has full permissions to update an ADMIN users
        if ( ! Yii::app()->user->checkAccess('user_full_manage') && isset($vec_assigned_roles['admin']) && Yii::app()->user->id !== $id )
        {
            throw new \CHttpException(401, Yii::t('app', 'Access denied'));
        }
		
		if ( !empty($_POST) )
		{
			$vec_post_tasks = $_POST;
			foreach ( $vec_post_tasks as $que_task => $val_task )
			{
				if ( !empty($val_task) )
				{
					if ( !$am->isAssigned($que_task, $user_model->id) )
					{
						$am->assign($que_task, $user_model->id);
						if ($am instanceof \CPhpAuthManager)
						{
							$am->save();
						}
						if ($am instanceof \ICachedAuthManager)
						{
							$am->flushAccess($que_task, $user_model->id);
						}
						$task_item = $am->getAuthItem($que_task);
						Yii::app()->user->addFlash('success', Yii::t('app', 'Asignado permiso <em>'. $task_item->description .'</em>'));
					}
				}
			}

			// Revoke assignments
			$vec_post_tasks = array_keys($vec_post_tasks);
			foreach ( $vec_tasks_assigned as $task_item )
			{
				if ( !in_array($task_item->name, $vec_roles['tasks']) AND !in_array($task_item->name, $vec_post_tasks) AND $am->isAssigned($task_item->name, $user_model->id) )
				{
					$am->revoke($task_item->name, $user_model->id);
					if ($am instanceof \CPhpAuthManager)
					{
						$am->save();
					}
					if ($am instanceof \ICachedAuthManager)
					{
						$am->flushAccess($task_item->name, $user_model->id);
					}
					Yii::app()->user->addFlash('success', Yii::t('app', 'Eliminado permiso <em>'. $task_item->description .'</em>'));
				}
			}
		}
		
		$this->render('permission', [
			'model' => $user_model,
			'roles' => $vec_roles,
			'tasks' => [
				'list' => $am->getAuthItems(1),
				'assigned' => array_keys($am->getAuthItems(1, $id))
			],
		]);
	}
	

	/**
	 * Disables an user
	 */
	public function actionDisable($id)
	{
		// Disable action only allowed via POST requests
		if ( Yii::app()->getRequest()->getIsPostRequest() )
		{
            $user_model = $this->loadModel($id, User::class);

            // Auth Manager
            $am = Yii::app()->getAuthManager();
            $vec_assigned_roles = $am->getAuthItems(2, $user_model->id);
        
            // Check if current user has full permissions to disable an ADMIN users
            if ( ! Yii::app()->user->checkAccess('user_full_manage') && isset($vec_assigned_roles['admin']) && Yii::app()->user->id !== $id )
            {
                throw new \CHttpException(401, Yii::t('app', 'Access denied'));
            }

            if ( $user_model->disable() )
            {
                if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
                {
                    Yii::app()->user->addFlash('success', Yii::t('app', 'Usuario dado de baja correctamente'));
                }
                else
                {
                    echo Yii::t('app', 'Usuario dado de baja correctamente');
                }
            }
            else
            {
                $this->showErrors($user_model->getErrors());
            }
		}
		else
		{
			throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}
	
	
	
	/**
	 * Enables an user
	 */
	public function actionEnable($id)
	{
		// Disable action only allowed via POST requests
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $user_model = $this->loadModel($id, User::class);

            // Auth Manager
            $am = Yii::app()->getAuthManager();
            $vec_assigned_roles = $am->getAuthItems(2, $user_model->id);
        
            // Check if current user has full permissions to enable an ADMIN users
            if ( ! Yii::app()->user->checkAccess('user_full_manage') && isset($vec_assigned_roles['admin']) && Yii::app()->user->id !== $id )
            {
                throw new \CHttpException(401, Yii::t('app', 'Access denied'));
            }

            if ( $user_model->enable() )
            {
                if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
                {
                    Yii::app()->user->addFlash('success', Yii::t('app', 'Usuario dado de alta correctamente'));
                }
                else
                {
                    echo Yii::t('app', 'Usuario dado de alta correctamente');
                }
            }
            else
            {
                $this->showErrors($user_model->getErrors());
            }
        }
        else
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }
	}
	
	
	/**
	 * Delete action for User model
	 */
	public function actionDelete($id)
	{
        // Delete action only allowed by POST requests
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $user_model = $this->loadModel($id, User::class);

            // Auth Manager
            $am = Yii::app()->getAuthManager();
            $vec_assigned_roles = $am->getAuthItems(2, $user_model->id);

            // Check if current user has full permissions to DELETE an ADMIN users
            if ( ! Yii::app()->user->checkAccess('user_full_manage') && isset($vec_assigned_roles['admin']) && Yii::app()->user->id !== $id )
            {
                throw new \CHttpException(401, Yii::t('app', 'Access denied'));
            }

            if ( $user_model->delete() )
            {
                if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
                {
                    Yii::app()->user->addFlash('success', Yii::t('app', 'Usuario eliminado correctamente') );
                }
                else
                {
                    echo Yii::t('app', 'Usuario eliminado correctamente');
                }
            }
            else
            {
                $this->showErrors($model->getErrors());
            }
        }
        else
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }
	}
}
