<?php
/*
|--------------------------------------------------------------------------
| Controller class for change password users
|--------------------------------------------------------------------------
*/

namespace dz\modules\user\controllers;

use dz\helpers\StringHelper;
use dz\web\Controller;
use user\models\User;
use Yii;

class PasswordController extends Controller
{  
    /**
     * Default action
     *
     * @var string
     */
    public $defaultAction = 'recovery';


    /**
     * Recovery password action
     */
    public function actionRecovery()
    {
        if ( Yii::app()->user->isGuest )
        {
            $user_model = Yii::createObject(User::class);
            $user_model->scenario = 'password_recovery';

            // Collect user input data
            if ( isset($_POST['User']) )
            {
                $user_model->attributes = $_POST['User'];

                // Validate user input and redirect to previous page if valid
                if ( $user_model->validate() )
                {
                    // Reload user model & send password reset email
                    $user_model = User::model()->cache(0)->findByEmail($user_model->email);
                    Yii::app()->userManager->send_reset_password_email($user_model);

                    // Success message
                    Yii::app()->user->addFlash('loginMessage', Yii::t('app', 'You should receive an email soon in order to reset your password. Please make sure to check your spam and trash folders if you cannot see this email.'));

                    // Redirect to main page (after login)
                    $this->redirect(['/user/login']);
                }
            }

            // Get USER from $_GET
            if ( isset($_GET['u']) AND !empty($_GET['u']) AND empty($user_model->email) )
            {
                $user_model->email = $_GET['u'];
            }

            // Display password recovery form
            $this->render('//user/user/password_recovery', [
                'user_model' => $user_model,
            ]);
        }

        // Already logged in, redirect user to change password page
        else
        {
            $this->redirect(['/user/password/change']);
        }
    }


    /**
     * Reset password action. Usually, user arrives here from a link into the "reset_password" mail
     */
    public function actionReset($id, $code)
    {
        if ( Yii::app()->user->isGuest )
        {
            $user_model = User::findOne($id);
            $user_model->scenario = 'change_password';

            // Clean password fields
            $user_model->password = '';
            $user_model->verify_password = '';

            // Check verification code
            if ( $user_model && $user_model->verification_code === $code)
            {
                // Change password?
                if ( isset($_POST['User']) )
                {
                    $user_model->setAttributes($_POST['User']);
                    if ( $user_model->validate() )
                    {
                        // Change password & update "is_force_change_password" attribute
                        $user_model->change_password($user_model->password, true);

                        if ( $user_model->save(false) )
                        {
                            // Success message
                            Yii::app()->user->addFlash('loginMessage', Yii::t('app', 'Your password has been changed successfully. Log in now with your new password.'));

                            Yii::app()->userManager->afterChangePassword($user_model);
                        }
                    }

                    // If error, clean password fields
                    else
                    {
                        $user_model->password = '';
                        $user_model->verify_password = '';
                    }
                }

                // Display password reset form
                $this->render('//user/user/password_reset', [
                    'user_model' => $user_model
                ]);
            }
            else
            {
                throw new \CHttpException(403, Yii::t('app', 'Access denied.'));
            }
        }

        // Already logged in, redirect user to change password page
        else
        {
            $this->redirect(['/user/password/change']);
        }
    }


    /**
     * Change password action
     * 
     * This action is only available if "is_force_change_password" attribute is enabled.
     */
    public function actionChange()
    {
        // Get current user model
        $user_model = Yii::app()->user->model();
        $user_model->scenario = 'change_password';
        
        if ( Yii::app()->user->id > 0 && $user_model && $user_model->is_force_change_password == 1 )
        {
            // Clean password fields
            $user_model->password = '';
            $user_model->verify_password = '';

            // Change password?
            if ( isset($_POST['User']) )
            {
                $user_model->setAttributes($_POST['User']);
                if ( $user_model->validate() )
                {
                    // Change password & update "is_force_change_password" attribute
                    $user_model->change_password($user_model->password, true);

                    if ( $user_model->save(false) )
                    {
                        Yii::app()->userManager->afterChangePassword($user_model);
                    }
                }

                // If error, clean password fields
                else
                {
                    $user_model->password = '';
                    $user_model->verify_password = '';
                }
            }

            // Display change password form
            $this->render('//user/user/password_change', [
                'user_model' => $user_model
            ]);
        }
        else
        {
            throw new \CHttpException(403, Yii::t('app', 'Access denied.'));
        }
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            // Access to any logged_in user
            'change'    => 'is_logged_in',
        ];
    }
}
