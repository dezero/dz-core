<?php

namespace dz\modules\user;

use dz\components\Mailer;
use dz\modules\user\models\User;
use Yii;

/**
 * Yii-User module
 * 
 * @author Mikhail Mangushev <mishamx@gmail.com> 
 * @link http://yii-user.2mx.org/
 * @license http://www.opensource.org/licenses/bsd-license.php
 */
class Module extends \dz\web\Module
{
    /**
     * @var array mapping from controller ID to controller configurations.
     */
    public $controllerMap = [
        'activation' => [
            'class' => 'dz\modules\user\controllers\ActivationController',
        ],
        'admin' => [
            'class' => 'dz\modules\user\controllers\AdminController',
        ],
        'confirm' => [
            'class' => 'dz\modules\user\controllers\ConfirmController',
        ],
        'email' => [
            'class' => 'dz\modules\user\controllers\EmailController',
        ],
        'login' => [
            'class' => 'dz\modules\user\controllers\LoginController',
        ],
        'logout' => [
            'class' => 'dz\modules\user\controllers\LogoutController',
        ],
        'password' => [
            'class' => 'dz\modules\user\controllers\PasswordController',
        ],
        'profile' => [
            'class' => 'dz\modules\user\controllers\ProfileController',
        ],
        'recovery' => [
            'class' => 'dz\modules\user\controllers\RecoveryController',
        ],
    ];


    /**
     * Load specific CSS or JS files for this module
     */
    public $cssFiles = null; // ['user.css'];
    public $jsFiles = null; // ['user.js'];

	/*
	|--------------------------------------------------------------------------
	| CUSTOM FUNCTIONS
	|--------------------------------------------------------------------------
	*/
	
	
	/**
	 * Send an mail
	 *
	 * @return bool
	 */
	public static function sendMail($email,$subject,$message)
	{
    	$adminEmail = Yii::app()->params['adminEmail'];
	    $headers = "MIME-Version: 1.0\r\nFrom: $adminEmail\r\nReply-To: $adminEmail\r\nContent-Type: text/html; charset=utf-8";
	    $message = wordwrap($message, 70);
	    $message = str_replace("\n.", "\n..", $message);
	    // return mail($email,'=?UTF-8?B?'.base64_encode($subject).'?=',$message,$headers);
	
		// --------------------------------------------------------------
		// Dezero - Added YiiMailer
		// --------------------------------------------------------------
		$mail = new Mailer('recover-password', array('message' => $message));
		$mail->render();
		$mail->Subject = $subject;
		$mail->AddAddress($email);
		$sending_result = $mail->Send();
		if ( $sending_result )
		{
		    $mail->ClearAddresses();
		    // Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
		}
		else
		{
		    Yii::app()->user->setFlash('error', Yii::t('app', 'Error while sending email') .': '. $mail->ErrorInfo);
		}
		return $sending_result;
		// --------------------------------------------------------------
		// Dezero - Added YiiMailer
		// --------------------------------------------------------------
	}
}
