<?php
/**
 * @package dz\modules\user\models
 */

namespace dz\modules\user\models;

use dz\db\DbCriteria;
use dz\helpers\AuthHelper;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dz\helpers\Log;
use dz\helpers\Url;
use dz\modules\user\components\UserIdentity;
use dz\modules\user\models\_base\User as BaseUser;
use Yii;

/**
 * User model class for "user_users" database table
 *
 * Columns in table "user_users" available as properties of the model,
 * followed by relations of table "user_users" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $firstname
 * @property string $lastname
 * @property string $verification_code
 * @property integer $last_login_date
 * @property string $last_login_ip
 * @property string $status_type
 * @property integer $is_verified_email
 * @property integer $last_verification_date
 * @property integer $is_force_change_password
 * @property integer $last_change_password_date
 * @property string $default_role
 * @property integer $is_superadmin
 * @property integer $disable_date
 * @property integer $disable_uid
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $sessions
 * @property mixed $createdUser
 * @property mixed $users
 * @property mixed $disableUser
 * @property mixed $users1
 * @property mixed $updatedUser
 * @property mixed $users2
 */
class User extends BaseUser
{
    const STATUS_ACTIVE = 'active';
    const STATUS_DISABLED = 'disabled';
    const STATUS_BANNED = 'banned';
    const STATUS_PENDING = 'pending';

    /**
     * @var enum Default value for status type
     */
    public $status_type = 'active';


    /**
     * @var int Role filter for search
     */
    public $role_filter;


    /**
     * @var Verify password
     */
    public $verify_password;


    /**
     * @var string. Token used for "LOGIN AS" process
     */
    public $loginas_token;


    /**
     * @var array. Caching roles
     */
    private $_roles = [];


    /**
     * Constructor
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     * Returns the validation rules for attributes
     */
    public function rules()
    {
        return [
            ['username, password, email, created_date, created_uid, updated_date, updated_uid', 'required'],
            ['last_login_date, is_verified_email, last_verification_date, is_force_change_password, last_change_password_date, is_superadmin, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
            ['username', 'length', 'min' => 5, 'max'=>100, 'message' => Yii::t('user', "Incorrect username (length between 5 and 50 characters).")],
            ['username', 'unique', 'message' => Yii::t('user', "This username has already been taken"), 'on' => 'insert'],
            // ['username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u', 'message' => Yii::t('user', "Username must contain only letters and numbers.")],
            ['password', 'length', 'max' => 128, 'min' => 5, 'message' => Yii::t('user', "Incorrect password (minimal length 5 characters).")],
            ['firstname, lastname', 'length', 'max'=> 100],
            ['email, verification_code, last_login_ip', 'length', 'max'=> 255],
            ['email', 'email'],
            ['email', 'unique', 'message' => Yii::t('user', "This email address has already been taken"), 'on' => 'insert'],
            ['default_role', 'length', 'max'=> 64],
            ['uuid', 'length', 'max' => 36],
            ['status_type', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DISABLED, self::STATUS_BANNED, self::STATUS_PENDING]],
            ['loginas_token', 'safe'],
            ['firstname, lastname, verification_code, last_login_date, last_login_ip, status_type, is_verified_email, last_verification_date, is_force_change_password, last_change_password_date, is_superadmin, default_role, disable_date, disable_uid, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
            ['id, username, password, email, firstname, lastname, verification_code, last_login_date, last_login_ip, status_type, is_verified_email, last_verification_date, is_force_change_password, last_change_password_date, is_superadmin, default_role, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid, uuid, role_filter', 'safe', 'on' => 'search'],

            // Custom validation rules
            ['password', 'validate_login', 'on' => 'login'],
            ['verify_password', 'validate_verify_password', 'on' => 'insert, change_password'],
            ['email', 'validate_verify_email', 'on' => 'password_recovery' ]
        ];
    }


    /**
     * Define relations with other objects
     *
     * There are four types of relations that may exist between two active record objects:
     *   - BELONGS_TO: e.g. a member belongs to a team;
     *   - HAS_ONE: e.g. a member has at most one profile;
     *   - HAS_MANY: e.g. a team has many members;
     *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
     */
    public function relations()
    {
        return [
            'sessions' => [self::HAS_MANY, UserSession::class, 'user_id'],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
            'disableUser' => [self::BELONGS_TO, User::class, ['disable_uid' => 'id']],
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
        ];
    }


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'last_verification_date' => 'd/m/Y - H:i',
                    'disable_date' => 'd/m/Y - H:i',
                ],
            ],
        ], parent::behaviors());
    }


    /**
     * Returns the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'email' => Yii::t('app', 'Email'),
            'firstname' => Yii::t('app', 'First name'),
            'lastname' => Yii::t('app', 'Last name'),
            'verification_code' => Yii::t('app', 'Verification Code'),
            'last_login_date' => Yii::t('app', 'Last Login'),
            'last_login_ip' => Yii::t('app', 'Last Login Ip'),
            'status_type' => Yii::t('app', 'Status Type'),
            'is_verified_email' => Yii::t('app', 'Verified Email?'),
            'last_verification_date' => Yii::t('app', 'Last Email Verification Date'),
            'is_force_change_password' => Yii::t('app', 'Force Password Change?'),
            'last_change_password_date' => Yii::t('app', 'Last Change Password'),
            'default_role' => Yii::t('app', 'Default Role'),
            'is_superadmin' => Yii::t('app', 'Is Superadmin'),
            'disable_date' => Yii::t('app', 'Disable Date'),
            'disable_uid' => null,
            'created_date' => Yii::t('app', 'User Since'),
            'created_uid' => null,
            'updated_date' => Yii::t('app', 'Updated Date'),
            'updated_uid' => null,
            'uuid' => Yii::t('app', 'Uuid'),
            'userSessions' => null,
            'createdUser' => null,
            'userUsers' => null,
            'disableUser' => null,
            'userUsers1' => null,
            'updatedUser' => null,
            'userUsers2' => null,

            // Custom labels
            'verify_password' => Yii::t('app', 'Repeat Password'),
        ];
    }


    /**
     * Get "status_type" labels
     */
    public function status_type_labels()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
            self::STATUS_DISABLED => Yii::t('app', 'Disabled'),
            self::STATUS_BANNED => Yii::t('app', 'Banned'),
            self::STATUS_PENDING => Yii::t('app', 'Pending'),
        ];
    }


    /**
     * Get "status_type" specific label
     */
    public function status_type_label($status_type)
    {
        $vec_labels = $this->status_type_labels();
        return isset($vec_labels[$status_type]) ? $vec_labels[$status_type] : '';
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;

        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.username', $this->username, true);
        $criteria->compare('t.email', $this->email, true);

        // Find by firstname, lastname or firstname together with lastname
        if ( $this->firstname )
        {
            $criteria->addCondition('t.firstname LIKE CONCAT("%", :name, "%") OR t.lastname LIKE CONCAT("%", :name, "%") OR CONCAT(t.firstname, " ", t.lastname) LIKE CONCAT("%", :name, "%")');
            $criteria->addParams([':name' => $this->firstname]);
        }

        // Exclude "Anonymous" for non-superadmin user
        if ( Yii::app()->user->id > 1 )
        {
            $criteria->addCondition('t.id > 0');

            // Exclude SUPERADMIN?
            if ( ! Yii::app()->user->checkAccess('user_full_manage') )
            {
                $criteria->compare('t.is_superadmin', 0);
            }
        }

        // Filter by role
        if ( $this->role_filter )
        {
            $criteria->join = "INNER JOIN user_auth_assignment u ON u.userid = t.id";
            $criteria->addCondition('u.itemname = "'. $this->role_filter.'"');
        }

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['id' => true]]
        ]);
    }


    /**
     * Event "beforeValidate"
     *
     * This method is invoked before validation starts
     */
    public function beforeValidate()
    {
        // Email is not required on LOGIN scenario
        if ( $this->scenario == 'login' )
        {
            if ( empty($this->email) )
            {
                $this->email = $this->username .'@login.dz';
                if ( preg_match("/\@/", $this->username) )
                {
                    $this->email = $this->username;
                }
            }
        }

        // Username and password are not required on PASSWORD_RECOVERY scenario
        else if ( $this->scenario == 'password_recovery')
        {
            $this->username = 'temp@recovery.dz';
            $this->password = 'temp@recovery.dz';
        }

        return parent::beforeValidate();
    }


    /**
     * Event "afterValidate"
     *
     * This method is invoked after validation end
     */
    public function afterValidate()
    {
        // Disable/enable an user indirectly
        if ( $this->scenario !== 'disable' && $this->scenario !== 'enable' )
        {
            if ( $this->status_type === self::STATUS_DISABLED && empty($this->disable_date) )
            {
                $this->disable_date = time();
                $this->disable_uid = (Yii::app() instanceof \CConsoleApplication) ? 1 : Yii::app()->user->id;
            }
            elseif ( $this->status_type !== self::STATUS_DISABLED && !empty($this->disable_date) )
            {
                $this->disable_date = null;
                $this->disable_uid = null;
            }
        }

        return parent::afterValidate();
    }


    /**
     * User models list
     *
     * @return array
     */
     public function user_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['id', 'username'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';

        $vec_models = self::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


     /**
     * Action "delete"
     *
     * Deletes the row corresponding to this active record
     */
    public function delete()
    {
        // STEP 1 - Revoke permissions
        Yii::app()->db->createCommand("DELETE FROM user_auth_assignment WHERE userid = ". $this->id)->execute();

        // Going on with "delete" action
        return parent::delete();
    }


    /**
     * Disables an user
     */
    public function disable()
    {
        $this->status_type = self::STATUS_DISABLED;
        return parent::disable();
    }


    /**
     * Enables an user
     */
    public function enable()
    {
        $this->status_type = self::STATUS_ACTIVE;
        return parent::enable();
    }


    /**
     * Check if a user is enabled
     */
    public function is_enabled()
    {
        return $this->status_type === self::STATUS_ACTIVE && parent::is_enabled();
    }


    /**
     * Check if a model is disabled
     */
    public function is_disabled()
    {
        return $this->status_type === self::STATUS_DISABLED && parent::is_disabled();
    }


    /**
     * Check if a user is banned
     */
    public function is_banned()
    {
        return $this->status_type === self::STATUS_BANNED;
    }


    /**
     * Authenticates the password.
     *
     * This is the 'authenticate' validator as declared in rules().
     */
    public function validate_login($attribute, $params)
    {
        // We only want to authenticate when no input errors
        if ( ! $this->hasErrors() )
        {
            $identity = new UserIdentity($this->username, $this->password);

            // Default login process
            if ( empty($this->loginas_token) )
            {
                $identity->authenticate();
            }

            // LoginAs process
            else
            {
                $identity = $identity->loginAs($this->loginas_token);
            }

            switch ( $identity->errorCode )
            {
                case UserIdentity::ERROR_NONE:
                    // $duration = $this->is_remember ? Yii::app()->user->rememberMeTime : 0;
                    Yii::app()->user->login($identity, 0);
                break;

                case UserIdentity::ERROR_EMAIL_INVALID:
                    $this->addError('username', Yii::t('app', 'Email is not valid'));
                break;

                case UserIdentity::ERROR_USERNAME_INVALID:
                    $this->addError('username', Yii::t('app', 'Username is not valid'));
                break;

                case UserIdentity::ERROR_STATUS_NOTACTIV:
                    $this->addError('username', Yii::t('app', 'You account is not activated'));
                break;

                case UserIdentity::ERROR_STATUS_BAN:
                    $this->addError('username', Yii::t('app', 'Your account is blocked'));
                break;

                case UserIdentity::ERROR_PASSWORD_INVALID:
                    $this->addError('username', Yii::t('app', 'Invalid password'));
                break;

                case UserIdentity::ERROR_LOGINAS:
                    $this->addError('username', Yii::t('app', 'No es posible iniciar sesión con otro usuario'));
                break;
            }
        }
    }


    /**
     * Validate a new email
     */
    public function validate_email()
    {
        // #1 - Check email empty?
        if ( empty($this->email) )
        {
            $this->addError('email', Yii::t('app', 'You must type an email address'));
            return false;
        }

        // #2 - Check email is correct
        else
        {
            $email_validator = new \CEmailValidator;

            if ( ! $email_validator->validateValue($this->email) )
            {
               $this->addError('email', Yii::t('app', 'Invalid email'));
               return false;
           }
        }

        return true;
    }


    /**
     * Authenticates an user using UserIdentity
     */
    public function authenticate($source_password)
    {
        $identity = new UserIdentity($this->username, $source_password);
        $identity->authenticate();
        return $identity;
    }


    /**
     * Get token
     */
    public function token()
    {
        return $this->verification_code;
    }


    /**
     * Find an User model by token (API)
     */
    public function findByToken($token)
    {
        $criteria = new DbCriteria;
        $criteria->compare('verification_code', $token);
        $vec_user_models = static::model()->findAll($criteria);
        if ( !empty($vec_user_models) )
        {
            // Strange case - We have 2 users with same "verification_code" (token) value
            if ( count($vec_user_models) > 1 )
            {
                Log::error("WARNING - There are ". count($vec_user_models) ." users with same verification_code: ". $token);
            }

            return $vec_user_models[0];
        }

        return null;
    }


    /**
     * Find an User model by username (or by email)
     */
    public function findByUsername($username)
    {
        if ( preg_match("/\@/", $username) )
        {
            return self::findByEmail($username);
        }

        return self::get()->where(['username' => $username])->one();
    }


    /**
     * Find an User model by email
     */
    public function findByEmail($email)
    {
        return self::get()->where(['email' => $email])->one();
    }


    /**
     * Check if given password is valid
     */
    public function check_password($password)
    {
        return StringHelper::encrypt($password) === $this->raw_attributes['password'];
    }


    /**
     * Change password
     */
    public function change_password($new_password, $is_update_force_change_password = true)
    {
        $this->password = StringHelper::encrypt($new_password);
        $this->verification_code = StringHelper::encrypt(microtime() . $this->password);
        $this->last_change_password_date = time();

        // Update force change password?
        if ( $is_update_force_change_password && $this->is_force_change_password == 1 )
        {
            $this->is_force_change_password = 0;
        }

        return true;
    }


    /**
     * Add a new role to this user
     */
    public function add_role($role_name, $is_default = false)
    {
        $am = Yii::app()->getComponent('authManager');
        if ( ! $am->isAssigned($role_name, $this->id) )
        {
            $am->assign($role_name, $this->id);
            if ($am instanceof \CPhpAuthManager)
            {
                $am->save();
            }
            if ($am instanceof \ICachedAuthManager)
            {
                $am->flushAccess($role_name, $this->id);
            }

            // 18/03/2021 - Add default role
            if ( empty($this->default_role) || $is_default )
            {
                $this->default_role = $role_name;
                $this->raw_attributes['default_role'] = $role_name;
                $this->saveAttributes([
                    'default_role'  => $role_name
                ]);
            }

            return true;
        }

        return false;
    }


    /**
     * Add a new role to this user and set this role as DEFAULT
     */
    public function add_default_role($role_name)
    {
        return $this->add_role($role_name, true);
    }

    /**
     * Remove a role from this user
     */
    public function remove_role($role_name)
    {
        $am = Yii::app()->getComponent('authManager');

        // Role is not assigned, so it cannot be removed
        if ( ! $am->isAssigned($role_name, $this->id) )
        {
            return false;
        }

        // Revoke / remove role
        $am->revoke($role_name, $this->id);
        if ( $am instanceof CPhpAuthManager )
        {
            $am->save();
        }

        if ( $am instanceof ICachedAuthManager )
        {
            $am->flushAccess($role_name, $this->id);
        }

        // Clean default role
        if ( $this->default_role === $role_name )
        {
            $this->default_role = null;
            $this->raw_attributes['default_role'] = null;

            // Check if user has another role
            $vec_roles = $this->roles();
            if ( !empty($vec_roles) )
            {
                foreach ( $vec_roles as $item_name => $item )
                {
                    if ( $this->default_role === null && $item_name !== $role_name )
                    {
                        $this->default_role = $item_name;
                        $this->raw_attributes['default_role'] = $item_name;
                        break;
                    }
                }
            }

            // Finally, save it into database
            $this->saveAttributes([
                'default_role'  => $this->default_role
            ]);
        }

        return true;
    }


    /**
     * Get all roles assigned to an user
     */
    public function roles()
    {
        if ( empty($this->_roles) )
        {
            $this->_roles = Yii::app()->getComponent('authManager')->getAuthItems(2, $this->id);
        }
        return $this->_roles;
    }


    /**
     * Check if User model is assigned to $role_name
     */
    public function has_role($role_name)
    {
        $vec_roles = $this->roles();

        return isset($vec_roles[$role_name]);
    }


    /**
     * Verify email
     */
    public function verify_email($code)
    {
        if ( $this->verification_code === $code )
        {
            // Verify email
            $this->is_verified_email = 1;
            $this->last_verification_date = time();

            return true;
        }

        return false;
    }


    /**
     * Check if an user is Admin
     */
    public function isAdmin()
    {
        // return AuthHelper::isAdmin();
        $vec_roles = $this->roles();
        return isset($vec_roles['admin']);
    }


    /**
     * Alias of isAdmin()
     */
    public function isSuperadmin()
    {
        // return AuthHelper::isAdmin();
        return self::isAdmin();
    }


    /**
     * Check if an user has "Editor" role
     */
    public function isEditor()
    {
        $vec_roles = $this->roles();
        return isset($vec_roles['editor']);
    }


    /**
     * Check if an user has "Customer" role
     */
    public function isCustomer()
    {
        $vec_roles = $this->roles();
        return isset($vec_roles['customer']);
    }


    /**
     * Validate verify password
     */
    public function validate_verify_password($attribute, $params)
    {
        if ( strlen($this->password) < 6 )
        {
            $this->addError('password', Yii::t('app', 'Incorrect password (minimal length 6 characters)'));
        }

        else if ( $this->verify_password !== null && $this->password !== $this->verify_password )
        {
            $this->addError('password',  Yii::t('app', 'Passwords do not match'));
        }
    }


    /**
     * Validate verify email
     */
    public function validate_verify_email($attribute, $params)
    {
        $user_model = $this->findByEmail($this->email);
        if ( ! $user_model )
        {
            $this->addError('email', Yii::t('app', 'Invalid email'));
        }
    }


    /**
     * Get email address
     */
    public function email()
    {
        // Guest email
        if ( preg_match("/^". $this->username ."\-\-\-/", $this->email) )
        {
            return str_replace($this->username .'---', "", $this->email);
        }
        return $this->email;
    }


    /**
     * Get email address. Alias of email()
     */
    public function get_email()
    {
        return $this->email();
    }


    /**
     * Get full name: first name + last name
     */
    public function fullname()
    {
        $fullname = $this->firstname;
        if ( $this->lastname )
        {
            $fullname .= ' '. $this->lastname;
        }

        return $fullname;
    }


    /**
     * Title used for this model
     */
    public function title()
    {
        $fullname = $this->fullname();

        return !empty($fullname) ? $fullname : $this->username;
    }


    /**
     * Send email using a mailing template
     */
    public function send_email($template_mail, $recipient_email = null, $vec_entity_info = [])
    {
        if ( $recipient_email === null )
        {
            $recipient_email = $this->get_email();
        }

        // Composer email via MailManager component
        $language_id = isset($vec_entity_info['language_id']) ? $vec_entity_info['language_id'] : null;
        $mailer = Yii::app()->mail
            ->setTo($recipient_email)
            ->setLanguage($language_id)
            ->addModel($this, 'user')
            ->compose($template_mail);

        // Entity information?
        if ( !empty($vec_entity_info) && isset($vec_entity_info['entity_id']) && isset($vec_entity_info['entity_type']) )
        {
            $entity_scenario = isset($vec_entity_info['entity_scenario']) ? $vec_entity_info['entity_scenario'] : null;
            $mailer->setEntityInfo($vec_entity_info['entity_id'], $vec_entity_info['entity_type'], $entity_scenario);
        }

        // Send email
        $sending_result = $mailer->send();

        // Save error into LOG
        if ( ! $sending_result )
        {
            Yii::app()->mail->logError();
        }

        return $sending_result;
    }


    /**
     * Returns verify email URL
     */
    public function verify_email_url()
    {
        $verify_email_url = Yii::app()->user->verify_email_url;
        if ( is_array($verify_email_url) )
        {
            $verify_email_url = reset($verify_email_url);
        }

        return Url::to($verify_email_url, ['id' => $this->id, 'code' => $this->verification_code]);
    }


    /**
     * Returns the mail tokens
     */
    public function mailTokens()
    {
        return [
            '{user.id}'                 => $this->id,
            '{user.username}'           => $this->username,
            '{user.email}'              => $this->email,
            '{user.firstname}'          => $this->firstname,
            '{user.latname}'            => $this->lastname,
            '{user.fullname}'           => $this->fullname(),
            '{user.verify_email_url}'   => $this->verify_email_url()
        ];
    }


    /**
     * Help text for the mail tokens
     */
    public function mailHelp()
    {
        return [
            '{user.id}'                 => 'User number identification (internal)',
            '{user.username}'           => 'Username',
            '{user.email}'              => 'Email address',
            '{user.firstname}'          => 'First name',
            '{user.latname}'            => 'Last name',
            '{user.fullname}'           => 'Full name',
            '{user.verify_email_url}'   => 'URL to verify the user\'s email'
        ];
    }
}
