/**
 * Scripts for USER module
 */
 (function(document, window, $) {

  // profileForm Form - Global object
  // -------------------------------------------------------------------------------------------
  $.profileForm = {
    init: function() {
      // Change password button on update user form page
      $('#change-password-btn').on('click', function(e){
        e.preventDefault();

        var $this = $(this);
        $('#is-password-changed').val(1);
        $('#profile-form').find('.password-row').removeClass('hide');
        $this.parent().parent().parent().parent().addClass('hide');
      });
    }
  };

  // DOCUMENT READY
  // -------------------------------------------------------------------------------------------
  $(document).ready(function() {
    // Profile form
    if ( $('#profile-form').size() > 0  ) {
      $.profileForm.init();
    }
  });

})(document, window, jQuery);
