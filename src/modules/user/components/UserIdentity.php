<?php
/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */

namespace dz\modules\user\components;

use user\models\User;
use dz\helpers\StringHelper;
use Yii;

class UserIdentity extends \CUserIdentity
{
    const ERROR_EMAIL_INVALID = 3;
    const ERROR_STATUS_NOTACTIV = 4;
    const ERROR_STATUS_BAN = 5;
    const ERROR_LOGINAS = 66;

    /**
     * User identificator
     */
	public $user_id;


    /**
     * Authenticates a user.
     * 
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        $user_model = User::model()->findByUsername($this->username);

        if ( $user_model === null)
        {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            
            if ( strpos($this->username,"@") )
            {
                $this->errorCode = self::ERROR_EMAIL_INVALID;
            }
        }
        else if ( StringHelper::encrypt($this->password) !== $user_model->password )
        {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        }
        else if ( $user_model->status_type === User::STATUS_DISABLED && Yii::app()->user->allow_login_disabled === false )
        {
            $this->errorCode = self::ERROR_STATUS_NOTACTIV;
        }
        else if ( $user_model->status_type === User::STATUS_BANNED )
        {
            $this->errorCode = self::ERROR_STATUS_BAN;
        }
        else
        {
            $this->user_id = $user_model->id;
            $this->username = $user_model->username;
            $this->errorCode = self::ERROR_NONE;

            // Update session user_id and other attributes
            Yii::app()->session->afterAuthenticate($user_model->id);
        }

        return ! $this->errorCode;
    }


    /**
     * LoginAs another user
     * 
     * return UserIdentity
     */
    public function loginAs($loginas_token)
    {
        $valid_loginas_token = '';
        $loginas_user_model = User::model()->findByToken($loginas_token);
        if ( $loginas_user_model )
        {
            $vec_roles = $loginas_user_model->roles();
            if ( !empty($vec_roles) AND isset($vec_roles['admin']) )
            {
                $valid_loginas_token = $loginas_token;
            }
        }

        if ( empty($valid_loginas_token) )
        {
            $this->errorCode = self::ERROR_LOGINAS;
        }
        else
        {
            $identity = new UserIdentity($loginas_user_model->username, $this->password);
            if ( $identity->authenticate() )
            {
                $user_destination = User::model()->findByAttributes(['username' => $this->username]);
                if ( ! $user_destination )
                {
                    $this->errorCode = UserIdentity::ERROR_USERNAME_INVALID;    
                }
                else
                {
                    $this->user_id = $user_destination->id;
                    $this->username = $user_destination->username;
                    $this->errorCode = self::ERROR_NONE;

                    // Update session user_id and other attributes
                    Yii::app()->session->afterAuthenticate($user_destination->id);
                }
            }
            else
            {
                return $identity;
            }
        }

        return $this;
    }

    
    /**
    * @return integer the ID of the user record
    */
    public function getId()
    {
        return $this->user_id;
    }
}