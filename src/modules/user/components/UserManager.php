<?php
/**
 * UserManager
 * 
 * Component to manage users
 */

namespace dz\modules\user\components;

use dz\base\ApplicationComponent;
use dz\db\DbCriteria;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\user\components\UserIdentity;
use user\models\User;
use Yii;

class UserManager extends ApplicationComponent
{
    /**
     * @var boolean whether to register login access in log files
     */
    public $registerLoginAccess = false;


    /**
     * @var string welcome message
     */
    public $welcomeMessage;


    /**
     * After login process
     * 
     * Called from LoginController::actionLogin() method
     * 
     * Redirects the user after a successful login attempt
     * or if they visited the Login page while they were already logged in
     */
    public function afterLogin($user_model, $redirect_url = '')
    {        
        $user_model = User::model()->findByUsername($user_model->username);
        if ( $user_model )
        {
            $user_model->saveAttributes([
                'last_login_date' => time(),
                'last_login_ip' => Yii::app()->request->getUserIP()
            ]);

            // Register login access in log files
            if ( $this->registerLoginAccess )
            {
                Log::login_access('User '. $user_model->id . ' - '. $user_model->username .' ('. $user_model->email .') logged in from IP '. Yii::app()->request->getUserIP());
            }

            // Force change password?
            if ( $user_model->is_force_change_password == 1 )
            {
                Yii::app()->controller->redirect(['/user/password/change']);
            }
        }

        // Welcome message
        if ( !empty($this->welcomeMessage) )
        {
            Yii::app()->user->addFlash('success', $this->welcomeMessage);
        }

        // Custom redirect URL?
        if ( empty($redirect_url) )
        {
            $redirect_url = Yii::app()->user->returnUrl;
        }
        else if ( !is_array($redirect_url) )
        {
            $redirect_url = [$redirect_url];
        }

        // Redirect after login
        if ( Yii::app()->user->returnUrl == '/index.php' )
        {
            Yii::app()->controller->redirect($redirect_url);
        }
        else
        {
            $vec_roles = Yii::app()->user->roles();
            if ( !isset($vec_roles['admin']) && ( isset($vec_roles['editor']) ) )
            {
                Yii::app()->controller->redirect(['/']);
            }
        }

        Yii::app()->controller->redirect($redirect_url);
    }


    /**
     * After logout process
     * 
     * Called from LogoutController::actionLogout() method
     * 
     * Redirects the user after the logout action
     */
    public function afterLogout($user_model, $redirect_url = '')
    {
        // Custom redirect URL?
        if ( empty($redirect_url) )
        {
            $redirect_url = Yii::app()->user->returnLogoutUrl;
        }
        else if ( !is_array($redirect_url) )
        {
            $redirect_url = [$redirect_url];
        }

        // Redirect after logout
        Yii::app()->controller->redirect($redirect_url);
    }


    /**
     * After password change
     * 
     * Called from PasswordController::actionIndex() method
     * 
     * Redirect the user after password is changed
     */
    public function afterChangePassword($user_model)
    {
        // Redirect after change password
        if ( Yii::app()->user->returnUrl == '/index.php' )
        {
            Yii::app()->controller->redirect(Yii::app()->user->returnUrl);
        }
        else
        {
            $vec_roles = Yii::app()->user->roles();
            if ( !isset($vec_roles['admin']) && ( isset($vec_roles['editor']) ) )
            {
                Yii::app()->controller->redirect(['/']);
            }
        }

        Yii::app()->controller->redirect(Yii::app()->user->returnUrl);
    }


    /**
     * After error login
     */
    public function afterErrorLogin($user_model)
    {
        $error_msg = 'Failed login attempt: username "'. $user_model->username .'" - IP '. Yii::app()->request->getUserIP();
        
        // Add error message
        $vec_errors = $user_model->getErrors();
        if ( !empty($vec_errors) )
        {
            // Clean error message
            $que_error = print_r($vec_errors, true);
            $que_error = StringHelper::clean_text($que_error);
            $que_error = str_replace("    ", "", $que_error);
            $que_error = str_replace(")[", ") --[", $que_error);
            $que_error = str_replace("[", " [", $que_error);
            $error_msg .= ' - Errors: '. $que_error;
        }

        // Register failed attempt login
        Log::login_error($error_msg);
    }


    /**
     * After verify email
     * 
     * Called from EmailController::actionVerify() method
     * 
     * Redirect the user after email has been verified
     */
    public function afterVerifyEmail($user_model, $is_auto_login = true)
    {
        // Make login automatically
        if ( $is_auto_login )
        {
            if ( $this->auto_login($user_model->id) )
            {
                return $this->afterLogin($user_model);
            }
        }

        // Redirect to LOGIN page
        Yii::app()->controller->redirect(Yii::app()->user->login_url);
    }


    /**
     * Sent a password reset email
     */
    public function send_reset_password_email($user_model)
    {
        return $user_model->send_email('reset_password');
    }


    /**
     * Makes an automatic login authentication
     */
    public function auto_login($user_id)
    {
        // Create a "fake" UserIdentity class
        $user_identity = new UserIdentity($user_id, '');
        $user_identity->user_id = $user_id;
        $user_identity->errorCode = UserIdentity::ERROR_NONE;
        
        Yii::app()->user->login($user_identity, 0);

        return true;
    }


    /**
     * Create a cookie
     */
    public function create_cookie($cookie_name, $cookie_value, $vec_options = null)
    {
        $cookie = new \CHttpCookie($cookie_name, $cookie_value);
        if ( $vec_options !== null )
        {
            $cookie->configure($vec_options);
        }
        Yii::app()->request->cookies[$cookie_name] = $cookie;

        return true;
    }


    /**
     * Removes a cookie
     */
    public function remove_cookie($cookie_name)
    {
        // Remove "JE_COMMUNITY" cookie (used by Wordpress)
        if ( isset(Yii::app()->request->cookies[$cookie_name]) )
        {
            unset(Yii::app()->request->cookies[$cookie_name]);

            return true;
        }

        return false;
    }
}