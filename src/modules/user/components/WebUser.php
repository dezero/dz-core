<?php
/**
 * WebUser class file for Dz Framework
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2019 Dezero
 */

namespace dz\modules\user\components;

use dz\helpers\AuthHelper; 
use dz\helpers\Html; 
use dz\modules\auth\components\AuthWebUser;
use user\models\User;
use Yii;

class WebUser extends AuthWebUser
{
    /**
     * @var string Hash method to encrypt strings
     * 
     * @see http://www.php.net/manual/en/function.hash.php
     */
    public $hash_method = 'md5';


    /**
     * @var bool allow auth for is not active user
     */
    public $allow_login_disabled = false;


    /**
     * @var string User register URL
     */
    public $register_url = ['/user/register'];


    /**
     * @var string Reset password URL
     */
    public $reset_password_url = ['/user/password/reset'];


    /**
     * @var string Verify email URL
     */
    public $verify_email_url = ['/user/email/verify'];


    /**
     * @var string Login page URL
     */
    public $login_url = ['/user/login'];


    /**
     * @var string page after login
     */
    public $returnUrl = ['/'];


    /**
     * @var string page after logout
     */
    public $returnLogoutUrl = ['/user/login'];


    /**
     * @var array Caching user access checkings
     */
    private $_access = [];


    /**
     * @var array Caching user roles
     */
    private $_roles = [];


    /**
     * Initializes the component.
     */
    public function init()
    {
        parent::init();
        if ( isset($this->username) )
        {
            $this->setAdmin(in_array($this->username, Yii::app()->authManager->admins));
        }

        // Update CWebUser attributes
        if ( !empty($this->login_url) )
        {
            $this->loginUrl = $this->login_url;
        }
    }


    /**
     * Returns a flash message
     * A flash message is available only in the current and the next requests
     *
     * @param string $key key identifying the flash message
     * @param mixed $defaultValue value to be returned if the flash message is not available.
     * @param boolean $delete whether to delete this flash message after accessing it. Defaults to true.
     * @return mixed the message message
     */
    public function getFlash($key, $defaultValue=null, $delete=true)
    {
        $flash_message = parent::getFlash($key, $defaultValue, $delete);
        
        // If delete "message", it means it will be printed
        if ( $delete && is_array($flash_message) )
        {
            if ( count($flash_message) > 1 )
            {
                return Html::ul($flash_message);
            }
            return $flash_message[0];
        }
        return $flash_message;
    }


    /**
     * Return one or more flash messages
     */
    public function getRawFlash($key, $defaultValue=null, $delete=true)
    {
        return parent::getFlash($key, $defaultValue, $delete);
    }
    
    
    /**
     * Add a flash message to the stack
     *
     * @param string $key key identifying the flash message
     * @param mixed $value flash message
     */
    public function addFlash($key, $value)
    {
        $vec_flashes = $this->getFlash($key, null, false);
        if ( empty($vec_flashes) )
        {
            if ( is_array($value) )
            {
                $vec_flashes = $value;
            }
            else
            {
                $vec_flashes = [$value];    
            }
        }
        else if ( is_array($vec_flashes) )
        {
            if ( is_array($value) )
            {
                $vec_flashes = \CMap::mergeArray($vec_flashes, $value);
            }
            else
            {
                $vec_flashes[] = $value;
            }
        }
        $this->setFlash($key, $vec_flashes);
    }
    
    
    /**
     * Performs access check for this user.
     * 
     * @param string $operation the name of the operation that need access check.
     * @param array $params name-value pairs that would be passed to business rules associated
     * with the tasks and roles assigned to the user.
     * @param boolean $allowCaching whether to allow caching the result of access check.
     * @return boolean whether the operations can be performed by this user.
     */
    public function checkAccess($operation, $params = [], $allowCaching = true)
    {
        if ( $allowCaching && $params === [] && isset($this->_access[$operation]) )
        {
            return $this->_access[$operation];
        }

        if ( $this->getIsAdmin() )
        {
            return true;
        }
        
        // If operation has this format "<module>.<controller>.<action>", let's check if user has access to "<module>.<controller>.*"
        if ( preg_match("/\./",$operation) )
        {
            $vec_operation = explode(".", $operation);
            $vec_operation[(count($vec_operation) -1)] = '*';
            $new_operation = implode(".", $vec_operation);
            if ( parent::checkAccess($new_operation, $params, $allowCaching) )
            {
                return true;
            }           
        }

        return parent::checkAccess($operation, $params, $allowCaching);
    }


    /**
     * Alias of checkAccess function
     * 
     * @see checkAccess
     */
    public function can($operation, $params = [], $allowCaching = true)
    {
        return parent::checkAccess($operation, $params, $allowCaching);
    }
    
    
    /*
    |--------------------------------------------------------------------------
    | Methods from modules/auth/components/AuthWebUser.php
    |--------------------------------------------------------------------------
    */

    /**
     * Returns whether the logged in user is an administrator.
     * 
     * @return boolean the result.
     */
    public function getIsAdmin()
    {
        $is_admin = $this->getState('__isAdmin', false);
        if ( !$is_admin )
        {
            return $this->isAdmin();
        }
        return $is_admin;
    }

    /**
     * Sets the logged in user as an administrator.
     * 
     * @param boolean $value whether the user is an administrator.
     */
    public function setAdmin($value)
    {
        $this->setState('__isAdmin', $value);
    }
    
    
    /*
    |--------------------------------------------------------------------------
    | Methods from modules/user/components/WebUser.php
    |--------------------------------------------------------------------------
    */
    public function getRole()
    {
        return $this->getState('__role');
    }

    public function getId()
    {
        return $this->getState('__id') ? $this->getState('__id') : 0;
    }

    protected function afterLogin($fromCookie)
    {
        parent::afterLogin($fromCookie);
        $this->updateSession();
    }

    public function updateSession()
    {
        $user = User::findOne($this->id);
        $vec_user_attributes = [
            'email' => $user->email,
            'username' => $user->username,
            'created_date' => $user->created_date,
            'last_login_date' => $user->last_login_date,
        ];
        
        foreach ($vec_user_attributes as $attribute_name => $attribute_value)
        {
            $this->setState($attribute_name, $attribute_value);
        }
    }

    /**
     * Returns an User model
     */
    public function model($id = 0)
    {
        if ( $id === 0 )
        {
            $id = Yii::app()->user->id;
        }
        return User::findOne($id);
    }


    public function isAdmin()
    {
        return AuthHelper::isAdmin();
    }


    public function isSuperadmin()
    {
        return AuthHelper::isAdmin();
    }


    /*
    |--------------------------------------------------------------------------
    | New DEZERO custom functions
    |--------------------------------------------------------------------------
    */

    /**
     * Get all roles assigned to an user
     */
    public function roles()
    {
        if ( empty($this->_roles) )
        {
            $this->_roles = Yii::app()->getAuthManager()->getAuthItems(2, $this->id);
        }
        return $this->_roles;
    }


    /**
     * Check if an user has "Editor" role
     */
    public function isEditor()
    {
        $vec_roles = $this->roles();
        return isset($vec_roles['editor']);
    }


    /**
     * Check if an user has "Admin" role
     */
    public function isAdministrator()
    {
        $vec_roles = $this->roles();
        return isset($vec_roles['admin']);
    }


    /**
     * Check if an user has "Customer" role
     */
    public function isCustomer()
    {
        $vec_roles = $this->roles();
        return isset($vec_roles['customer']);
    }


    /**
     * Return user email
     */
    public function email()
    {
        $user_model = $this->model();
        return $user_model ? $user_model->email() : null;
    }
}
