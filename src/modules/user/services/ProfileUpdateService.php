<?php
/*
|--------------------------------------------------------------------------
| Service for updating profile
|--------------------------------------------------------------------------
*/

namespace dz\modules\user\services;

use dz\contracts\ServiceInterface;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\traits\ErrorTrait;
use dz\traits\FlashMessageTrait;
use user\models\User;
use Yii;

class ProfileUpdateService implements ServiceInterface
{
    use ErrorTrait;
    use FlashMessageTrait;


    /**
     * Password without encryption
     */
    private $original_password;


    /**
     * Constructor
     */
    public function __construct(User $user_model, bool $is_password_changed)
    {
        $this->user_model = $user_model;
        $this->is_password_changed = $is_password_changed;
    }


    /**
     * @return bool
     */
    public function run()
    {
        // Validate email address
        if ( ! $this->validate_email() )
        {
            return false;
        }

        // Update User model
        if ( ! $this->update_user() )
        {
            return false;
        }

        return true;
    }


    /**
     * Email address validation
     */
    private function validate_email()
    {
        // Check if email address is valid
        if ( ! $this->user_model->validate_email() )
        {
            return false;
        }

        // Check if email belongs to another user or it is valid
        $existing_user_model = User::model()->findByEmail($this->user_model->email);
        if ( $existing_user_model && ( $this->user_model->isNewRecord || $existing_user_model->id !== $this->user_model->id ) )
        {
            $this->user_model->addError('email', 'This email address belongs to another user');

            return false;
        }

        return true;
    }


    /**
     * Update User model
     */
    private function update_user()
    {
        // Assign attributes
        $this->user_model->setAttributes([
            'firstname'         => $this->user_model->firstname,
            'lastname'          => $this->user_model->lastname,
        ]);

        // Validate model's attributes
        if ( ! $this->user_model->validate() )
        {
            return false;
        }

        // Has password been changed? --> If yes, save original password and encrypt it
        if ( $this->is_password_changed )
        {
            $this->original_password = $this->user_model->password;
            // $this->user_model->password = StringHelper::encrypt($this->user_model->password);
            $this->user_model->change_password($this->user_model->password, false);
        }
        $this->user_model->save(false);

        return true;
    }
}
