<?php
/**
 * Migration class m190101_150000_user_table
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m190101_150000_user_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "user_users" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('user_users', true);

        $this->createTable('user_users', [
            'id' => $this->primaryKey(),
            'username' => $this->string(100)->notNull(),
            'password' => $this->string(128)->notNull(),
            'email' => $this->string()->notNull(),
            'firstname' => $this->string(100),
            'lastname' => $this->string(100),
            'verification_code' => $this->string()->notNull()->defaultValue(''),
            'last_login_date' => $this->date(),
            'last_login_ip' => $this->string(),
            'status_type' => $this->enum('status_type', ['active', 'disabled', 'banned', 'pending']),
            'is_superadmin' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'disable_date' => $this->date(),
            'disable_uid' => $this->integer()->unsigned(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create UNQUE indexes for "username" and "email" columns
        $this->createIndex(null, 'user_users', ['username'], true);
        $this->createIndex(null, 'user_users', ['email'], true);
        $this->createIndex(null, 'user_users', ['verification_code'], true);

        // Create normal INDEX for "superadmin" and "status" columns
        $this->createIndex(null, 'user_users', ['is_superadmin'], false);
        $this->createIndex(null, 'user_users', ['status_type'], false);
        $this->createIndex(null, 'user_users', ['uuid'], false);

        // Insert new data
        $this->insert('user_users', [
            'id' => 1,
            'username' => 'superadmin', 
            'password' => '21232f297a57a5a743894a0e4a801fc3',
            'email' => 'fabian@dezero.es',
            'firstname' => 'Admin',
            'lastname' => 'Super',
            'verification_code' => '9a24eff8c15a6a141ece27eb6947da0f',
            'is_superadmin' => 1,
            'status_type' => 'active',
            'created_date' => time(),
            'created_uid' => 1,
            'updated_date' => time(),
            'updated_uid' => 1,
            'uuid' => StringHelper::UUID()
        ]);

        // Foreign keys
        $this->addForeignKey(null, 'user_users', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'user_users', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'user_users', ['disable_uid'], 'user_users', ['id'], 'CASCADE', null);

        return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('user_users');
		return false;
	}
}

