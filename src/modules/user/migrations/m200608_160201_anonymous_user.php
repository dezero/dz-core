<?php
/**
 * Migration class m200608_160201_anonymous_user
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use user\models\User;

class m200608_160201_anonymous_user extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Check if anonymous user exists
        $user_model = User::get()->where(['id' => 0])->one();
        if ( ! $user_model )
        {
    		// Insert new anonymous user
            $this->insert('user_users', [
                'username' => 'anonymous', 
                'password' => '',
                'email' => 'anonymous@anonymous.com',
                'firstname' => 'Anonymous',
                'lastname' => 'Anonymous',
                'verification_code' => '',
                'is_superadmin' => 0,
                'status_type' => 'active',
                'created_date' => time(),
                'created_uid' => 1,
                'updated_date' => time(),
                'updated_uid' => 1,
                'uuid' => StringHelper::UUID()
            ]);

            // Force "anonymous" to be uid = 0
            $this->update('user_users', ['id' => 0], 'username = :username', [':username' => 'anonymous']);
        }
        else
        {
            echo "Anonymous user already exists in database with id=0\n";
        }

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

