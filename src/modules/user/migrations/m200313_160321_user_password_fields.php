<?php
/**
 * Migration class m200313_160321_user_password_fields
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200313_160321_user_password_fields extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Add new column via $this->addColumn
		$this->addColumn('user_users', 'is_force_change_password', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0)->after('status_type'));
        $this->addColumn('user_users', 'last_change_password_date', $this->date()->after('is_force_change_password'));

        // Update new column "last_change_password_date" with "created_date"
        $this->execute('UPDATE user_users users SET users.last_change_password_date = users.created_date');

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

