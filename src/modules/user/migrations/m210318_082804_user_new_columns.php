<?php
/**
 * Migration class m210318_082804_user_new_columns
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210318_082804_user_new_columns extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Add new "email verification" columns
        $this->addColumn('user_users', 'is_verified_email', $this->tinyInteger()->unsigned()->notNull()->defaultValue(0)->after('status_type'));
        $this->addColumn('user_users', 'last_verification_date', $this->date()->after('is_verified_email'));
        
        // Add new column "default role"
        $this->addColumn('user_users', 'default_role', $this->string(64)->after('last_change_password_date'));
        $this->createIndex(null, 'user_users', ['default_role'], false);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

