<?php
/**
 * Migration class m190102_150000_session_table
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;

class m190102_150000_session_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        $this->dropTableIfExists('user_session');

        $this->createTable('user_session', [
            'id' => $this->char(32),
            'user_id' => $this->integer()->unsigned(),
            'expire' => $this->date()->notNull(),
            'data' => $this->binary(),
            'created_date' => $this->date()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Indexes
        $this->addPrimaryKey(null, 'user_session', 'id');
        $this->addForeignKey(null, 'user_session', ['user_id'], 'user_users', ['id'], 'CASCADE', null);
        $this->createIndex(null, 'user_session', ['uuid'], false);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('user_session');
		return false;
	}
}

