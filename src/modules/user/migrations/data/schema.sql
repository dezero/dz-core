-- ------------------------------------------------------------
-- Users
-- ------------------------------------------------------------
DROP TABLE IF EXISTS `user_users`;

CREATE TABLE IF NOT EXISTS `user_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `verification_code` varchar(128) NOT NULL DEFAULT '',
  `create_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  `superadmin` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `status` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`),
  INDEX user_users_superadmin_index(superadmin)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;


INSERT INTO `user_users` (`id`, `username`, `password`, `email`, `verification_code`, `superadmin`, `status`) VALUES
(1, 'superadmin', '21232f297a57a5a743894a0e4a801fc3', 'fabian@dezero.es', '9a24eff8c15a6a141ece27eb6947da0f', 1, 1);
-- (2, 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 'demo@example.com', '099f825543f7850cc038b90aaff39fac', 0, 1);



-- ------------------------------------------------------------
-- User profiles
-- ------------------------------------------------------------
DROP TABLE IF EXISTS `user_profile`;

CREATE TABLE IF NOT EXISTS `user_profile` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT
	COMMENT "CONSTRAINT user_profile_id FOREIGN KEY (user_id) REFERENCES user_users(id)",
  `lastname` varchar(50) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ;

ALTER TABLE `user_profile` CHANGE `name` `name` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '';

INSERT INTO `user_profile` (`user_id`, `lastname`, `name`) VALUES
(1, 'Admin', 'Superadmin');
-- (2, 'Demo', 'Demo');


-- ------------------------------------------------------------
-- Auth items (Roles and permissions)
-- ------------------------------------------------------------
INSERT INTO `user_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, 'Administrator', NULL, 'N;'),
('editor', 2, 'Editor', NULL, 'N;'),
('user_manage', 1, 'Administer users', NULL, 'N;'),
('user.admin.*', 0, 'User - Administer users', NULL, 'N;');


INSERT INTO `user_auth_item_child` (`parent`, `child`) VALUES
('user_manage', 'user.admin.*'),
('admin', 'user_manage');

