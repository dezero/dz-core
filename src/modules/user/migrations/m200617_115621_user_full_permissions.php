<?php
/**
 * Migration class m200617_115621_user_full_permissions
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200617_115621_user_full_permissions extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{        
        // Permissions
        // -------------------------------------------------------------------------
        $this->insertMultiple('user_auth_item', [
            [
                'name'          => 'user_full_manage',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Users - Full access to administer users including ADMIN roles',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
        ]);

        // Set items relationships
        $this->insertMultiple('user_auth_item_child', [
            [
                'parent'    => 'user_full_manage',
                'child'     => 'user.admin.*'
            ],
            [
                'parent'    => 'admin',
                'child'     => 'user_full_manage'
            ],
        ]);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

