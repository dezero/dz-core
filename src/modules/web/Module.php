<?php
/**
 * Module to manage web content
 */

namespace dz\modules\web;

class Module extends \dz\web\Module
{
    /**
     * @var array mapping from controller ID to controller configurations.
     */
    public $controllerMap = [
        'contact' => [
            'class' => 'dz\modules\web\controllers\ContactController',
        ],
        'document' => [
            'class' => 'dz\modules\web\controllers\DocumentController',
        ],
        'page' => [
            'class' => 'dz\modules\web\controllers\PageController',
        ],
        'legal' => [
            'class' => 'dz\modules\web\controllers\LegalController',
        ],
        'slideshow' => [
            'class' => 'dz\modules\web\controllers\SlideshowController',
        ],
        'text' => [
            'class' => 'dz\modules\web\controllers\TextController',
        ],
    ];

    /**
     * Default controller
     */
    public $defaultController = 'text';


    /**
     * Load specific CSS or JS files for this module
     */
    // public $cssFiles = ['web.css'];
    // public $jsFiles = ['web.js'];


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        parent::init();
    }
}
