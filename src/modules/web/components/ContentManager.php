<?php
/**
 * Content Manager
 * 
 * Helper classes to work with WebContent models
 */

namespace dz\modules\web\components;

use dz\base\ApplicationComponent;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\helpers\Transliteration;
use dz\modules\asset\models\AssetImage;
use dz\modules\asset\models\AssetFile;
use dz\modules\web\models\WebContent;
use dz\modules\web\models\Seo;
use Yii;

class ContentManager extends ApplicationComponent
{
    /**
     * Input data
     */
    protected $vec_input_data;


    /**
     * Some error detected?
     */
    protected $is_error = false;


    /**
     * WebContent models
     */
    protected $vec_models = [];


    /**
     * Get a loaded model by internal name
     */
    public function get_model($internal_name)
    {
        if ( !empty($this->vec_models) && isset($this->vec_models[$internal_name]) )
        {
            return $this->vec_models[$internal_name];   
        }

        return null;
    }


    /**
     * Get all the loaded models
     */
    public function get_models()
    {
        return $this->vec_models;
    }


    /**
     * Load a WebContent model by internal name
     */
    public function load_model($internal_name, $language_id = '')
    {
        // Current language
        if ( empty($language_id) )
        {
            $language_id = Yii::currentLanguage();
        }

        return WebContent::get()->where([
            'content_type'  => 'web_content',
            'internal_name' => $internal_name,
            'language_id'   => $language_id
        ])->one();

    }


    /**
     * Add a new model with a default configuration
     */
    public function add_model($internal_name, $vec_config = [])
    {
        // Language
        $language_id = Yii::currentLanguage();
        if ( !empty($vec_config) && isset($vec_config['language_id']) )
        {
            $language_id = $vec_config['language_id'];
        }

        // Load WebContent model
        $content_model = $this->load_model($internal_name, $language_id);
        if ( ! $content_model )
        {
            $content_model = Yii::createObject(WebContent::class);
            $content_model->setAttributes([
                'content_type'  => 'web_content',
                'internal_name' => $internal_name,
                'language_id'   => $language_id
            ]);
        }

        // Load configuration
        if ( !empty($vec_config) )
        {
            $content_model->set_config($vec_config);
        }

        // Add the new model
        $this->vec_models[$internal_name] = $content_model;

        return true;
    }


    /**
     * Add multiple models with a default configuration
     */
    public function add_models($vec_config)
    {
        $language_id = Yii::currentLanguage();

        foreach ( $vec_config as $internal_name => $que_config )
        {
            $this->add_model($internal_name, $que_config);
        }

        return true;
    }


    /**
     * Save multiple WebContent models
     */
    public function save_models($vec_input)
    {
        $is_error = false;

        foreach ( $vec_input as $internal_name => $vec_attributes )
        {
            if ( isset($this->vec_models[$internal_name]) )
            {
                $this->vec_models[$internal_name]->setAttributes($vec_attributes);
                if ( ! $this->vec_models[$internal_name]->save() )
                {
                    $is_error = true;
                }
            }
        }

        return $is_error;
    }


    /**
     * Get all content models of a content_type
     */
    public function content_list($content_type, $language_id = '')
    {
        // Current language
        if ( empty($language_id) )
        {
            $language_id = Yii::currentLanguage();
        }

        return WebContent::get()
            ->where([
                'content_type'  => $content_type,
                'language_id'   => $language_id
            ])
            ->order('weight ASC')
            ->all();
    }



    /**
     * Get all content models of a content_type
     */
    public function get_active_models($content_type, $language_id = '')
    {
        // Current language
        if ( empty($language_id) )
        {
            $language_id = Yii::currentLanguage();
        }

        $now = time();

        return WebContent::get()
            ->where([
                'content_type'  => $content_type,
                'language_id'   => $language_id
            ])
            ->andWhere('t.disable_date IS NULL')
            ->andWhere('t.is_scheduled = 0 || (t.start_date > :now && (t.expiration_date + 86399) < :now)', [':now' => $now])
            ->order('t.weight ASC')
            ->all();
    }


    /**
     * CREATE action for WebContent model
     * 
     * @see BaseContentController::actionCreate()
     */
    public function create_content($content_model, $vec_input_data)
    {
        // Content configuration for content_type
        $vec_config = $content_model->load_config();

        // Save input data
        $this->vec_input_data = $vec_input_data;

        // Default language
        $default_language = Yii::defaultLanguage();

        // Translations
        $vec_translated_models = [];
        $vec_extra_languages = Yii::app()->i18n->get_extra_languages();
        if ( isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] && !empty($vec_extra_languages) )
        {
            foreach ( $vec_extra_languages as $language_id )
            {
                $translated_content_model = Yii::createObject(WebContent::class);
                $translated_content_model->content_type = $content_model->content_type;
                $translated_content_model->language_id = $language_id;
                $vec_translated_models[$language_id] = $translated_content_model;
            }
        }

        // Image
        $image_model = Yii::createObject(AssetImage::class);
        $image_model->setAttributes([
            'entity_type'   => 'WebContent'
        ]);

        // File
        $file_model = Yii::createObject(AssetFile::class);
        $file_model->setAttributes([
            'entity_type'   => 'WebContent'
        ]);

        // SEO fields
        $vec_seo_models = [];
        if ( isset($vec_config['is_seo_fields']) && $vec_config['is_seo_fields'] )
        {
            // First, add SEO for default language
            $vec_seo_attributes = [
                'entity_type'   => 'WebContent',
                'language_id'   => $default_language
            ];
            $seo_model = Yii::createObject(Seo::class);
            $seo_model->setAttributes($vec_seo_attributes);
            $vec_seo_models[$default_language] = $seo_model;
        }

        // Insert new model?
        if ( isset($vec_input_data['WebContent']) )
        {
            $content_model->setAttributes($vec_input_data['WebContent']);

            // Trigger custom "before_save_content" method
            $content_model = $this->before_save_content($content_model, 'create', $vec_input_data);

            // Save content
            if ( $content_model->save() )
            {
                // Image
                $content_model = $this->save_content($content_model, 'image', $image_model, $vec_input_data);

                // File
                $content_model = $this->save_content($content_model, 'file', $file_model, $vec_input_data);

                // SEO
                $content_model = $this->save_content($content_model, 'seo', $vec_seo_models, $vec_input_data);

                // Custom function after content is saved
                $this->after_save_content($content_model, 'create', $vec_input_data);
            }

            // Some error(s) detected
            else
            {
                // Set "error" detected
                $this->is_error = true;
            }

            // Some error(s) detected
            if ( $this->is_error )
            {
                $content_model->afterFind(new \CEvent($content_model));

                // IMAGE - Try to save file as TEMPORARY
                if ( $content_model->is_config('image_id') )
                {
                    $temp_destination_path = $image_model->getTempPath() . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR;
                    $image_model = $content_model->upload_temp_image('image_id', $temp_destination_path, 0);
                    if ( ! $image_model )
                    {
                        $image_model = Yii::createObject(AssetImage::class);
                        $image_model->setAttributes([
                            'entity_type'   => 'WebContent'
                        ]);
                    }
                }

                // FILE - Try to save file as TEMPORARY
                if ( $content_model->is_config('file_id') )
                {
                    $temp_destination_path = $file_model->getTempPath() . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR;
                    $file_model = $content_model->upload_temp_file('file_id', $temp_destination_path, 1);
                    if ( ! $file_model )
                    {
                        $file_model = Yii::createObject(AssetFile::class);
                        $file_model->setAttributes([
                            'entity_type'   => 'WebContent'
                        ]);
                    }
                }

                // Recover SEO data
                if ( isset($vec_input_data['Seo']) )
                {
                    foreach ( $vec_input_data['Seo'] as $language_id => $vec_seo_data )
                    {
                        if ( isset($vec_seo_models[$language_id]) )
                        {
                            $vec_seo_models[$language_id]->setAttributes($vec_seo_data);
                        }
                    }
                }
            }
        }

        return [
            'content_model'         => $content_model,
            'image_model'           => $image_model,
            'file_model'            => $file_model,
            'vec_config'            => $vec_config,
            'vec_translated_models' => $vec_translated_models,
            'vec_seo_models'        => $vec_seo_models,
            'default_language'      => $default_language,
            'vec_extra_languages'   => $vec_extra_languages,
            'form_id'               => 'content-'. $content_model->content_type .'-form'
        ];
    }


    /**
     * UPDATE action for WebContent model
     * 
     * @see BaseContentController::actionUpdate()
     */
    public function update_content($content_model, $vec_input_data)
    {
        // WebContent configuration for content_type
        $vec_config = $content_model->load_config();

        // Save input data
        $this->vec_input_data = $vec_input_data;

        // Default language
        $default_language = Yii::defaultLanguage();

        // Image
        $image_model = $content_model->image;
        if ( ! $image_model )
        {
            $image_model = Yii::createObject(AssetImage::class);
            $image_model->setAttributes([
                'entity_type'   => 'WebContent',
                'entity_id'     => $content_model->content_id
            ]);
        }

        // File
        $file_model = $content_model->file;
        if ( ! $file_model )
        {
            $file_model = Yii::createObject(AssetFile::class);
            $file_model->setAttributes([
                'entity_type'   => 'WebContent',
                'entity_id'     => $content_model->content_id
            ]);
        }

        // Translated content
        $vec_translated_models = [];
        $vec_extra_languages = Yii::app()->i18n->get_extra_languages();
        if ( !empty($vec_extra_languages) && isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] )
        {
            foreach ( $vec_extra_languages as $language_id )
            {
                $translated_content_model = WebContent::get()
                    ->where([
                        'language_id'           => $language_id,
                        'translated_content_id' => $content_model->content_id
                    ])
                    ->one();
                    
                if ( ! $translated_content_model )
                {
                    $translated_content_model = Yii::createObject(WebContent::class);
                    $translated_content_model->content_type = $content_model->content_type;
                    $translated_content_model->translated_content_id = $content_model->content_id;
                    $translated_content_model->language_id = $language_id;
                }
                $vec_translated_models[$language_id] = $translated_content_model;
            }
        }

        // SEO fields
        $vec_seo_models = [];
        if ( isset($vec_config['is_seo_fields']) && $vec_config['is_seo_fields'] )
        {
            $vec_seo_models = $content_model->seo;
            if ( empty($vec_seo_models) ) 
            {
                // First, add SEO for DEFAULT language
                $vec_seo_attributes = [
                    'entity_id'     => $content_model->content_id,
                    'entity_type'   => 'WebContent',
                    'language_id'   => $default_language
                ];
                $seo_model = Yii::createObject(Seo::class);
                $seo_model->setAttributes($vec_seo_attributes);
                $vec_seo_models[$default_language] = $seo_model;

                // Add SEO for more languages
                if ( !empty($vec_extra_languages) )
                {
                    foreach ( $vec_extra_languages as $language_id )
                    {
                        $vec_seo_attributes['language_id'] = $language_id;
                        $seo_model = Yii::createObject(Seo::class);
                        $seo_model->setAttributes($vec_seo_attributes);
                        $vec_seo_models[$language_id] = $seo_model;
                    }
                }
            }
            else
            {
                // Group SEO models by language
                $vec_seo_language_models = [];
                foreach ( $vec_seo_models as $seo_model )
                {
                    $vec_seo_language_models[$seo_model->language_id] = $seo_model;
                }

                // Check if there's some language with no SEO model
                if ( !empty($vec_extra_languages) )
                {
                    foreach ( $vec_extra_languages as $language_id )
                    {
                        if ( !isset($vec_seo_language_models[$language_id]) )
                        {
                            $seo_model = Yii::createObject(Seo::class);
                            $seo_model->setAttributes([
                                'entity_id'     => $content_model->content_id,
                                'entity_type'   => 'web_content',
                                'language_id'   => $language_id
                            ]);
                            $vec_seo_language_models[$language_id] = $seo_model;
                        }
                    }
                }
                
                $vec_seo_models = $vec_seo_language_models;
            }
        }

        // Update model?
        if ( isset($vec_input_data['WebContent']) )
        {
            $content_model->setAttributes($vec_input_data['WebContent']);

            // Trigger custom "before_save_content" method
            $content_model = $this->before_save_content($content_model, 'update', $vec_input_data);

            // Save content
            if ( $content_model->save() )
            {
                // Image
                $content_model = $this->save_content($content_model, 'image', $image_model, $vec_input_data);

                // File
                $content_model = $this->save_content($content_model, 'file', $file_model, $vec_input_data);

                // Translations
                $content_model = $this->save_content($content_model, 'translation', $vec_translated_models, $vec_input_data);

                // SEO
                $content_model = $this->save_content($content_model, 'seo', $vec_seo_models, $vec_input_data);

                // Custom function after content is saved
                $this->after_save_content($content_model, 'update', $vec_input_data);
            }
            else
            {
                // Set "error" detected
                $this->is_error = true;
            }

            // Some error(s) detected
            if ( $this->is_error )
            {
                $content_model->afterFind(new \CEvent($content_model));

                // IMAGE - Try to save file as TEMPORARY
                if ( $content_model->is_config('image_id') )
                {
                    $temp_destination_path = $image_model->getTempPath() . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR;
                    $image_model = $content_model->upload_temp_image('image_id', $temp_destination_path, 0);
                    if ( ! $image_model )
                    {
                        $image_model = Yii::createObject(AssetImage::class);
                        $image_model->setAttributes([
                            'entity_type'   => 'WebContent'
                        ]);
                    }
                }

                // FILE - Try to save file as TEMPORARY
                if ( $content_model->is_config('file_id') )
                {
                    $temp_destination_path = $file_model->getTempPath() . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR;
                    $file_model = $content_model->upload_temp_file('file_id', $temp_destination_path, 1);
                    if ( ! $file_model )
                    {
                        $file_model = Yii::createObject(AssetFile::class);
                        $file_model->setAttributes([
                            'entity_type'   => 'WebContent'
                        ]);
                    }
                }

                // Recover SEO data
                if ( isset($vec_input_data['Seo']) )
                {
                    foreach ( $vec_input_data['Seo'] as $language_id => $vec_seo_data )
                    {
                        if ( isset($vec_seo_models[$language_id]) )
                        {
                            $vec_seo_models[$language_id]->setAttributes($vec_seo_data);
                        }
                    }
                }
            }
        }

        return [
            'content_model'         => $content_model,
            'image_model'           => $image_model,
            'file_model'            => $file_model,
            'vec_config'            => $vec_config,
            'vec_translated_models' => $vec_translated_models,
            'vec_seo_models'        => $vec_seo_models,
            'default_language'      => $default_language,
            'vec_extra_languages'   => $vec_extra_languages,
            'form_id'               => 'content-'. $content_model->content_type .'-form'
        ];
    }


    /**
     * Save entities related with a WebContent: image, file, translations or seo
     */
    public function save_content($content_model, $entity_type, $entity_data = null, $vec_input_data = [])
    {
        $is_save_error = false;

        // Get input data
        if ( empty($vec_input_data) )
        {
            $vec_input_data = $this->vec_input_data;
        }

        // Content type configuration
        $vec_config = $content_model->load_config();

        switch ( $entity_type )
        {
            // Image
            case 'image':
                if ( $content_model->is_config('image_id') )
                {
                    if ( ! $content_model->add_image($entity_data) )
                    {
                        $is_save_error = true;
                    }
                }
            break;

            // File
            case 'file':
                if ( $content_model->is_config('file_id') )
                {
                    if ( ! $content_model->add_file($entity_data) )
                    {
                        $is_save_error = true;
                    }
                }
            break;

            case 'translation':
                $vec_translated_models = $entity_data;
                if ( isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] && !empty($vec_translated_models) && isset($vec_input_data['TranslatedContent']) )
                {
                    foreach ( $vec_input_data['TranslatedContent'] as $language_id => $vec_translated_attributes )
                    {
                        if ( isset($vec_translated_models[$language_id]) && isset($vec_translated_attributes['title']) )
                        {
                            $translated_content_model = $vec_translated_models[$language_id];
                            $vec_translated_attributes['language_id'] = $language_id;
                            $vec_translated_attributes['translated_content_id'] = $content_model->content_id;
                            $translated_content_model->setAttributes($vec_translated_attributes);
                            if ( ! $content_model->save_translation_model($vec_translated_models[$language_id]) )
                            {
                                $is_save_error = true;
                            }
                        }
                    }
                }
            break;

            // SEO
            case 'seo':
                $vec_seo_models = $entity_data;
                if ( isset($vec_config['is_seo_fields']) && $vec_config['is_seo_fields'] && !empty($vec_seo_models) && isset($vec_input_data['Seo']) )
                {
                    foreach ( $vec_input_data['Seo'] as $language_id => $seo_values )
                    {
                        if ( isset($vec_seo_models[$language_id]) && isset($seo_values['meta_title']) )
                        {
                            $seo_model = $vec_seo_models[$language_id];
                            $seo_model->setAttributes($seo_values);
                            $seo_model->entity_id = $content_model->content_id;
                            if ( ! $content_model->save_seo($seo_model) )
                            {
                                $is_save_error = true;
                            }
                        }
                    }
                }
            break;
        }

        // Is error here?
        if ( $is_save_error )
        {
            $this->is_error = true;
            $vec_errors = Log::errors_list();
            foreach ( $vec_errors as $que_error )
            {
                $content_model->addError('content_id', $que_error);
            }
        }

        return $content_model;
    }



    /**
     * This method is invoked before a WebContent is going to be saved via form submitting
     */
    public function before_save_content($content_model, $action = 'update', $vec_input_data)
    {
        return $content_model;
    }


    /**
     * This method is invoked after a WebContent is saved via form submitting
     */
    public function after_save_content($content_model, $action = 'update', $vec_input_data)
    {
        // Web content configuration for current content type
        $vec_config = $content_model->load_config();

        if ( $action == 'create' )
        {
            Yii::app()->user->addFlash('success', $content_model->text('created_success'));

            // Redirect to index page
            Yii::app()->controller->redirect(['index']);
        }

        else if ( $action == 'update' )
        {
            // Has an action been triggered?
            if ( isset($vec_input_data['StatusChange']) && !empty($vec_input_data['StatusChange']) )
            {
                switch ( $vec_input_data['StatusChange'] )
                {
                    case 'disable':
                        if ( $content_model->disable() )
                        {
                            Yii::app()->user->addFlash('success', $content_model->text('disable_success'));
                        }
                        else
                        {
                            Yii::app()->user->addFlash('error', $content_model->text('disable_error'));
                        }
                    break;

                    case 'enable':
                        if ( $content_model->enable() )
                        {
                            Yii::app()->user->addFlash('success', $content_model->text('enable_success'));
                        }
                        else
                        {
                            Yii::app()->user->addFlash('error', $content_model->text('enable_error'));
                        }
                    break;

                    case 'delete':
                        if ( $content_model->delete() )
                        {
                            Yii::app()->user->addFlash('success', $content_model->text('delete_success'));
                        }
                        else
                        {
                            Yii::app()->user->addFlash('error', $content_model->text('delete_error'));
                        }
                    break;
                }
            }
            else
            {
                // Success message
                Yii::app()->user->addFlash('success', $content_model->text('updated_success'));
            }

            // Redirect to index page
            Yii::app()->controller->redirect(['index']);
            // Yii::app()->controller->refresh();
        }
    }
}