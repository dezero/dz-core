<?php
/**
 * Frontend Manager
 * 
 * Helper classes collection for frontend theme
 */

namespace dz\modules\web\components;

use Dz;
use dz\base\ApplicationComponent;
use dz\helpers\StringHelper;
use Yii;

class FrontendManager extends ApplicationComponent
{
    /**
     * @var bool. Unify all CSS files?
     */
    public $is_unify_css = false;


    /**
     * @var bool. Unify all Javascript files?
     */
    public $is_unify_js = false;


    /**
     * @var string. Current controller name
     */
    public $current_controller;


    /**
     * @var string. Current action name
     */
    public $current_action;


    /**
     * @var string. Current module name
     */
    public $current_module;


    /**
     * @var string. Current environment name
     */
    public $current_environment;


    /**
     * Init function. Required!
     */
    public function init()
    {
        parent::init();
        
        if ( !empty(Yii::app()->theme) && Yii::app()->theme->name == 'frontend' )
        {
            $this->current_controller = Yii::currentController(true);
            $this->current_action = Yii::currentAction(true);
            $this->current_module = Yii::currentModule(true);
            $this->current_environment = Dz::get_environment();
        }
    }


    /**
     * Return params used on main layout and partials "_header.tpl.php" and "_footer.tpl.php"
     */
    public function get_params()
    {
        return [
            'current_module'      => $this->current_module,
            'current_controller'  => $this->current_controller,
            'current_action'      => $this->current_action,
            'current_environment' => $this->current_environment,
            'language_id'         => Yii::app()->language,
            'is_logged_in'        => ( Yii::app()->user->id > 0 ),
            'path_info'           => Yii::app()->getRequest()->getPathInfo()
        ];
    }


    /**
     * Get body classes
     */
    public function body_classes()
    {
        $vec_classes = [
            // Current action
            $this->current_action .'-page',

            // Current controller and action
            $this->current_controller .'-'. $this->current_action .'-page',

            // Logged in?
            ( Yii::app()->user->isGuest ? 'not-logged-in' : 'logged-in')
        ];

        return $vec_classes;
    }



    /**
     * Page title
     */
    public function page_title()
    {
        if ( empty($this->pageTitle) )
        {
            Yii::app()->controller->pageTitle = Yii::app()->name;
        }

        return Yii::app()->controller->pageTitle;
    }


    /**
     * Disable core Javascript files
     */
    public function disable_core_js()
    {
        Yii::app()->clientScript->scriptMap = [
            'jquery.js'         => false,
            'jquery.history.js' => false,
            'main.js'           => false,
        ];
    }


    /**
     * Check if current page is home page
     */
    public function is_homepage()
    {
        return $this->current_module === 'frontend' && $this->current_controller === 'home' && $this->current_action === 'index';
    }


    /**
     * Return Google Analytics ID
     */
    public function google_analytics_id($is_only_production = true)
    {
        if ( ! $is_only_production || $this->current_environment === 'prod' )
        {
            return getenv('GOOGLE_ANALYTICS_ID');
        }

        return '';
    }
}