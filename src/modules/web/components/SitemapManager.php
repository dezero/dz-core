<?php
/**
 * Sitemap Manager
 * 
 * Helper classes to generate sitemaps and work with them
 * 
 * INSTALL: Add this line into your composer.json project file
 * 
 * ```json
 *  "require": {
 *     "samdark/sitemap": "^2.2.1",
 *  }
 * ```
 * 
 * @see https://github.com/samdark/sitemap
 */

namespace dz\modules\web\components;

use dz\base\ApplicationComponent;
use dz\helpers\Url;
use samdark\sitemap\Index;
use samdark\sitemap\Sitemap;
use Yii;

/**
 * SitemapManager
 */
class SitemapManager extends ApplicationComponent
{
    /**
     * @var Sitemap object
     */
    public $sitemap;


    /**
     * @var File object
     */
    public $file;


    /**
     * @var array. Items added
     */
    public $vec_items = [];


    /**
     * Init function
     */
    public function init()
    {
        parent::init();

        // Sitemap.xml file will be saved on /www/sitemap.xml
        $this->sitemap = new Sitemap($this->get_file_path());
    }


    /**
     * Return sitemap.xml full file path
     */
    public function get_file_path($file_name = 'sitemap.xml')
    {
        return Yii::app()->params['basePath'] . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR . 'sitemap.xml';

        // $webroot_path = Yii::getAlias('@www');
        // return $webroot_path . '/sitemap.xml';
    }


    /**
     * Load sitemap.xml via File helper
     */
    public function load_file($file_name = 'sitemap.xml')
    {
        $this->file = Yii::app()->file->set($this->get_file_path($file_name));
        return $this->file;
    }


    /**
     * Create a sitemap.xml file
     */
    public function create()
    {
        // add some URLs
        // $this->add_item('mylink1');
        // $this->add_item('mylink2', time());
        // $this->add_item('mylink3', time(), Sitemap::HOURLY);
        // $this->add_item('mylink4', time(), Sitemap::DAILY, 0.3);

        // Write sitemap
        $this->write();

        return true;
    }


    /**
     * Add a new sitemap item
     */
    public function add_item($item_url, $updated_date = null, $change_frequency = null, $priority = null)
    {
        // Internal link? URL does not include http:// or https://
        if ( ! preg_match("/^http(s?)\:\/\//i", $item_url) )
        {
            $item_url = Url::to($item_url);
        }

        $this->vec_items[] = $item_url;

        return $this->sitemap->addItem($item_url, $updated_date, $change_frequency, $priority);
    }


    /**
     * Return an array with all the Sitemap URL's
     */
    public function get_items()
    {
        return $this->vec_items;
    }


    /**
     * Return the total items added into the Sitemap URL's
     */
    public function get_total_items()
    {
        return count($this->vec_items);
    }


    /**
     * Write the items into the sitemap.xml file
     */
    public function write()
    {
        return $this->sitemap->write();
    }
}