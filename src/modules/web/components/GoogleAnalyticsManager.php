<?php
/**
 * Helper classes to work with Google Analytics
 * 
 * This component is just a wrapper around the Google Analytics Measurement Protocol library for PHP.
 * 
 * INSTALL: Add this line into your composer.json project file
 * 
 * ```json
 *  "require": {
 *     "theiconic/php-ga-measurement-protocol": "2.7.*",
 *  }
 * ```
 * 
 * @see https://github.com/theiconic/php-ga-measurement-protocol
 * @see https://developers.google.com/analytics/devguides/collection/protocol/v1/
 */

namespace dz\modules\web\components;

use dz\base\ApplicationComponent;
use dz\helpers\Log;
use dz\helpers\Url;
use TheIconic\Tracking\GoogleAnalytics\Analytics;
use Yii;

/**
 * GoogleAnalyticsManager
 */
class GoogleAnalyticsManager extends ApplicationComponent
{
    /**
     * @var Analytics object
     */
    public $analytics;

    /**
     * @var bool. Use SSL
     */
    public $is_ssl = true;


    /**
     * @var string. Google Analytics tracking ID
     */
    public $tracking_id;


    /**
     * @var int. Google Analytics Measurement Protocol version: 1 or 2
     */
    public $version = 1;


    /**
     * @var bool. Use asynchronous requests (not waiting for a response)
     */
    public $is_async = true;


    /**
     * @var bool. Override the IP address by the user's
     */
    public $is_override_ip = true;


    /**
     * @var bool. Anonymize the IP address of the sender
     */
    public $is_anonymize_ip = false;


    /**
     * @var bool. Try to set ClientId automatically from "_ga" cookie
     */
    public $is_auto_client_id = true;


    /**
     * @var bool. Google Analytics client ID
     */
    public $client_id;


    /**
     * Test mode (log all responses)
     */
    protected $is_test_mode = false;


    /**
     * @var array Google Analytics configuration
     */
    protected $vec_config = [];


    /**
     * Init function
     */
    public function init()
    {
        parent::init();

        // Google Analytics configuration
        $this->vec_config = Yii::app()->config->get('components.google_analytics');
        $this->add_config($this->vec_config);

        // Google Analytics API
        $this->analytics = new Analytics($this->is_ssl);

        if ( !empty($this->tracking_id) )
        {
            // Set GA tracking id and the configuration
            $this->analytics
                ->setTrackingId($this->tracking_id)
                ->setProtocolVersion($this->version)
                ->setAsyncRequest($this->is_async);

            // Override the IP address by the user's
            if ( $this->is_override_ip )
            {
                $ip_address = Yii::app()->request->getUserHostAddress();
                if ( !empty($ip_address) )
                {
                    $this->analytics->setIpOverride($ip_address);
                }
            }

            // Anonymize the IP address of the sender
            if ( $this->is_anonymize_ip )
            {
                $this->analytics->setAnonymizeIp('1');
            }

            // Try to get Google Analytics client Id automatically from "_ga" cookie
            if ( $this->is_auto_client_id )
            {
                $this->client_id = $this->extract_client_id();
            }

            // Set Google Analytics client ID
            if ( !empty($this->client_id) )
            {
                $this->analytics->setClientId($this->client_id);
            }

            // Test mode?
            if ( $this->is_test_mode )
            {
                $this->analytics->setDebug(true);
            }
        }
    }


    /**
     * Return Analytics API library
     */
    public function get_api()
    {
        return $this->analytics;
    }



    /**
     * Set a Google Analytics client ID manually
     */
    public function set_client_id($client_id)
    {
        $this->client_id = $client_id;
    }


    /**
     * Send an Order model to Google Analytics
     */
    public function send_order($order_model, $client_id = null)
    {
        // Set client ID manually
        if ( $client_id !== null && !empty($client_id) )
        {
            $this->client_id = $client_id;
            $this->analytics->setClientId($this->client_id);
        }

        // Add order data (transaction)
        $this->analytics
            ->setTransactionId($order_model->order_id)
            ->setAffiliation(Yii::app()->name)
            ->setRevenue($order_model->raw_attributes['total_price'])
            ->setTax($order_model->raw_attributes['tax_price'])
            ->setShipping($order_model->raw_attributes['shipping_price']);

        // Coupon code
        if ( !empty($order_model->coupon_code) )
        {
            $this->analytics->setCouponCode($order_model->coupon_code);
        }

        // Products (line items)
        if ( $order_model->lineItems )
        {
            $vec_product_attributes = [];
            foreach ( $order_model->lineItems as $num_item => $line_item_model )
            {
                if ( $line_item_model->product )
                {
                    $vec_product_attributes = [
                        'sku'       => $line_item_model->sku,
                        'name'      => $line_item_model->title,
                        'price'     => (float)$line_item_model->raw_attributes['unit_price'],
                        'quantity'  => $line_item_model->quantity,
                        'position'  => $num_item + 1
                    ];

                    // Show price with APPLIED discount (only percentage discounts)
                    if ( $line_item_model->discount && $line_item_model->discount->is_fixed_amount  == 0 )
                    {
                        $vec_product_attributes['price'] = $line_item_model->get_unit_price_with_discount(true);
                    }

                    // Brand
                    if ( $line_item_model->product->mainCategory )
                    {
                        $line_category_model = $line_item_model->product->mainCategory;
                        $vec_product_attributes['brand'] = $line_item_model->product->mainCategory->title();
                    }

                    // Category names separated by "/"
                    /*
                    if ( $line_item_model->product->productCategories )
                    {
                        $vec_product_categories = [];
                        foreach ( $line_item_model->product->productCategories as $product_category_model )
                        {
                            if ( $product_category_model->category_id !== $line_item_model->product->main_category_id && $product_category_model->category )
                            {
                                $vec_product_categories[] = $product_category_model->category->title();
                            }
                        }

                        if ( !empty($vec_product_categories) )
                        {
                            $vec_product_attributes['category'] = implode('/', $vec_product_categories);
                        }
                    }
                    */

                    // Add the product
                    $this->analytics->addProduct($vec_product_attributes);
                }
            }

            // Set action to PURCHASE
            if ( !empty($vec_product_attributes) )
            {
                $this->analytics->setProductActionToPurchase();
            }
        }

        // Finally, you need to send a hit; in this example, we are sending an Event
        $response = $this->analytics
            ->setEventCategory('Checkout')
            ->setEventAction('Purchase')
            ->sendEvent();

        // Test model? Log all the responses
        if ( $this->is_test_mode )
        {
            Log::dev('GoogleAnalyticsManager::send_order() for order_id = '. $order_model->order_id);
            Log::dev($response->getDebugResponse());
        }

        return $response;
    }


    /**
     * Try to get Google Analytics Client Id automatically from "_ga" cookie
     */
    public function extract_client_id()
    {
        if ( isset(Yii::app()->request->cookies['_ga']) )
        {
            $cookie = Yii::app()->request->cookies['_ga']->value;
            $vec_ga_cookie = explode('.', $cookie);
            $vec_client_id =  array_slice($vec_ga_cookie, -2);
            return implode('.', $vec_client_id);
        }

        return null;
    }
}