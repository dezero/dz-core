<?php
/**
 * SEO Manager
 * 
 * Helper classes to work with Seo models
 */

namespace dz\modules\web\components;

use dz\base\ApplicationComponent;
use dz\helpers\StringHelper;
use dz\helpers\Url;
use dz\modules\web\models\Seo;
use Yii;

class SeoManager extends ApplicationComponent
{
    /**
     * Current SEO model
     */
    protected $_seo_model = null;


    /**
     * Return current URL
     */
    public function current_url($language_id = '', $vec_params = [])
    {
        // Current language
        if ( empty($language_id) )
        {
            $language_id = Yii::currentLanguage();
        }

        // Force language_id value as input param
        $vec_params['language'] = $language_id;

        // SEO model from current URL
        $seo_model = $this->get_current_seo();
        if ( $seo_model )
        {
            $current_url = $seo_model->url_manual;
            if ( $seo_model->language_id !== $language_id )
            {
                $seo_url = $this->get_url($seo_model->entity_type, $seo_model->entity_id, $language_id);
                if ( !empty($seo_url) )
                {
                    $current_url = $seo_url;
                }
            }

            return Url::to($current_url, $vec_params);
        }

        return Url::current($vec_params);
    }


    /**
     * Get slug URL from a source url
     */
    public function slug_url($route, $params=[], $schema='', $ampersand='&')
    {
        // Current language
        if ( !isset($params['language']) )
        {
            $params['language'] = Yii::currentLanguage();
        }

        // URL's for "product/<id>" or "category/<id>"
        if ( preg_match("/\//", $route) )
        {
            // Removes slash from the beginning and end of the route string
            $route = trim($route, "/");
            $vec_route = explode("/", $route);
            if ( count($vec_route) == 2 )
            {
                switch ( $vec_route[0] )
                {
                    case 'category':
                    case 'product':
                        $seo_url = $this->get_url($vec_route[0], $vec_route[1], $params['language']);
                        if ( !empty($seo_url) )
                        {
                            return Url::to($seo_url, $params, $schema, $ampersand);
                        }
                    break;
                }
            }
        }

        // Given a SEO URL, try to the translated URL
        else
        {
            $seo_model = Seo::get()
                ->where(['url_manual' => $route])
                ->andWhere('language_id <> "'. $params['language'] .'"')
                ->one();

            if ( $seo_model )
            {
                $seo_url = $this->get_url($seo_model->entity_type, $seo_model->entity_id, $params['language']);
                if ( !empty($seo_url) )
                {
                    return Url::to($seo_url, $params, $schema, $ampersand);
                }
            }
        }

        return Url::to($route, $params, $schema, $ampersand);
    }

    /**
     * Get the URL from a SEO model
     */
    public function get_url($entity_type, $entity_id, $language_id = '')
    {
        // Current language
        if ( empty($language_id) )
        {
            $language_id = Yii::currentLanguage();
        }

        return Seo::get()
            ->select('url_manual')
            ->where([
                'entity_type' => $entity_type,
                'entity_id' => $entity_id,
                'language_id' => $language_id
            ])
            ->scalar();
    }


    /**
     * Returns SEO model from current URL
     */
    public function get_current_seo($path_info = null)
    {
        // Load current SEO model
        if ( ! $this->_seo_model )
        {
            $this->load_current_seo($path_info);
        }

        return $this->_seo_model;
    }


    /**
     * Load a SEO model from current URL
     */
    public function load_current_seo($path_info = null)
    {
        // Current URL path_info. For example, "my-new-product" or "user/login"
        if ( $path_info === null )
        {
            $path_info = Url::path_info();
        }
        
       return $this->load_seo_model($path_info);
    }


    /**
     * Load a SEO model given an URL route
     */
    public function load_seo_model($url_route)
    {
        if ( !empty($url_route) && ! preg_match("/\//", $url_route) )
        {
            // Find SEO model for current language
            $this->_seo_model = $this->get_seo_model($url_route);

            return true;
        }

        return false;
    }


    /**
     * Return a SEO model given an URL route
     */
    public function get_seo_model($url_route, $language_id = null)
    {
        // Current language
        if ( $language_id === null )
        {
            $language_id = Yii::currentLanguage();
        }

        return Seo::get()
            ->where([
                'url_manual'    => $url_route,
                'language_id'   => $language_id
            ])
            ->one();
    }


    /**
     * Check if SEO URL exists
     */
    public function check_url_unique($url_manual, $entity_id, $entity_type, $language_id = '')
    {
        if ( empty($language_id) )
        {
            $language_id = Yii::currentLanguage();
        }

        $seo_model = Seo::get()
            ->where([
                'url_manual'    => $url_manual,
                'entity_type'   => $entity_type,
                'language_id'   => $language_id
            ])
            ->andWhere('entity_id <> '. $entity_id)
            ->one();

        return $seo_model ? true : false;
    }
}
