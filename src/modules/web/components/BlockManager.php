<?php
/**
 * Block Manager
 * 
 * Helper classes to work with WebBlock models
 */

namespace dz\modules\web\components;

use dz\base\ApplicationComponent;
use dz\helpers\StringHelper;
use dz\modules\web\models\WebBlock;
use dz\modules\web\models\WebContent;
use Yii;

class BlockManager extends ApplicationComponent
{
    /**
     * Block configuration in /app/config/components/web_block.php file
     */
    protected $vec_block_configs = [];


    /**
     * WebBlock models
     */
    protected $vec_models = [];


    /**
     * Loaded languages
     */
    protected $vec_languages = [];


    /**
     * Get a preloaded block (WebBlock model) by internal name
     */
    public function get_block($internal_name, $language_id = '')
    {
        if ( !empty($this->vec_models) && isset($this->vec_models[$internal_name]) )
        {
            if ( !empty($language_id) && isset($this->vec_models[$internal_name]['models'][$language_id]) )
            {
                return $this->vec_models[$internal_name]['models'][$language_id];
            }

            return $this->vec_models[$internal_name];
        }

        return null;
    }


    /**
     * Get all the loaded blocks (WebBlock models)
     */
    public function get_blocks()
    {
        return $this->vec_models;
    }


    /**
     * Load a block (WebBlock model) by internal name
     */
    public function load_block($internal_name, $language_id = '')
    {
        // Current language
        if ( empty($language_id) )
        {
            $language_id = Yii::currentLanguage();
        }

        $block_model = WebBlock::get()->where([
            'content_type'  => 'block',
            'internal_name' => $internal_name,
            'language_id'   => $language_id
        ])->one();

        // Block does not exist, try to get from default language
        if ( !$block_model && $language_id !== Yii::defaultLanguage() )
        {
            $block_model = $this->load_block($internal_name, Yii::defaultLanguage());
            if ( $block_model )
            {
                $block_model->language_id = $language_id;
            }
        }

        return $block_model;
    }


    /**
     * Add a new block with a default configuration
     */
    public function add_block($internal_name, $vec_config = [])
    {
        // Load block configuration from /app/config/components/web_block.php file
        if ( empty($vec_config) )
        {
            if ( empty($this->vec_block_configs) )
            {
                $this->vec_block_configs = Yii::app()->config->get('components.web_block');
            }

            if ( isset($this->vec_block_configs[$internal_name]) )
            {
                $vec_config = $this->vec_block_configs[$internal_name];
            }
        }

        // Default language is always loaded
        $default_language = Yii::defaultLanguage();
        $vec_languages = [$default_language];

        // Multilanguage?
        if ( isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] )
        {
            $vec_languages = \CMap::mergeArray($vec_languages, Yii::app()->i18n->get_extra_languages());
        }

        if ( ! empty($vec_languages) )
        {
            $this->vec_models[$internal_name] = [
                'title'     => $vec_config['block_title'],
                'models'    => []
            ];
            foreach ( $vec_languages as $language_id )
            {
                // Load WebBlock model for default language
                $block_model = $this->load_block($internal_name, $language_id);
                if ( ! $block_model )
                {
                    $block_model = Yii::createObject(WebBlock::class);
                    $block_model->setAttributes([
                        'content_type'  => 'block',
                        'internal_name' => $internal_name,
                        'language_id'   => $language_id
                    ]);
                }

                // Load configuration
                if ( !empty($vec_config) )
                {
                    $block_model->set_config($vec_config);
                }

                // Add the new model
                $this->vec_models[$internal_name]['models'][$language_id] = $block_model;

                // Add new language
                if ( !isset($this->vec_languages[$language_id]) )
                {
                    $this->vec_languages[$language_id] = $language_id;
                }
            }
        }

        return true;
    }


    /**
     * Add multiple blocks (WebBlock models) with a default configuration
     */
    public function add_blocks($vec_config)
    {
        foreach ( $vec_config as $internal_name => $que_config )
        {
            if ( is_array($que_config) )
            {
                $this->add_block($internal_name, $que_config);
            }
            else
            {
                $this->add_block($que_config);
            }
        }

        return true;
    }


    /**
     * Save multiple WebBlock models
     */
    public function save_blocks($vec_input)
    {
        $is_error = false;

        foreach ( $vec_input as $internal_name => $block_data )
        {
            if ( isset($this->vec_models[$internal_name]) && isset($this->vec_models[$internal_name]['models']) )
            {
                foreach ( $block_data as $language_id => $vec_attributes )
                {
                    if ( isset($this->vec_models[$internal_name]['models'][$language_id]) )
                    {
                        $block_model = $this->vec_models[$internal_name]['models'][$language_id];

                        // Disable / enable action?
                        if ( isset($vec_attributes['disable_date']) )
                        {
                            if ( !empty($vec_attributes['disable_date']) )
                            {
                                $vec_attributes['disable_date'] = time();
                                $vec_attributes['disable_uid'] = (Yii::app() instanceof \CConsoleApplication) ? 1 : Yii::app()->user->id;
                            }
                            else
                            {
                                $vec_attributes['disable_date'] = null;
                                $vec_attributes['disable_uid'] = null;
                            }
                        }

                        // Set attributes
                        $block_model->setAttributes($vec_attributes);
                        if ( ! $block_model->save() )
                        {
                            $is_error = true;
                        }
                        else
                        {
                            // Image model
                            if ( $block_model->is_config('image_id') )
                            {
                                // Image number in config file?
                                $num_image = 0;
                                if ( $block_model->is_config('image_id', 'num_image') )
                                {
                                    $num_image = $block_model->get_config('image_id', 'num_image');
                                }

                                $block_image_path = $block_model->get_image_path(true);
                                if ( ! $block_model->upload_image('image_id', $block_image_path, $num_image) )
                                {
                                    Yii::app()->controller->showErrors($block_model->get_file_errors());   
                                }
                            }
                        }

                        $this->vec_models[$internal_name]['models'][$language_id] = $block_model;
                    }
                }
            }
        }

        return ! $is_error;
    }


    /**
     * Check if some block is multilanguage
     */
    public function is_multilanguage()
    {
        return count($this->vec_languages) > 1;
    }


    /**
     * Check if a given language is present in one of the loaded blocks
     */
    public function is_language($language_id)
    {
        return count($this->vec_languages) > 1;
    }
}
