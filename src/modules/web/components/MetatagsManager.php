<?php
/**
 * Meta Tags Manager
 * 
 * Helper classes to work with Metatags
 * 
 * Based on Yii2 MetaMaster plugin
 * 
 * @see https://github.com/floor12/yii2-metamaster
 */

namespace dz\modules\web\components;

use dz\base\ApplicationComponent;
use dz\helpers\StringHelper;
use dz\helpers\Url;
use Yii;

class MetatagsManager extends ApplicationComponent
{
    /**
     * @var string. Site name
     */
    public $site_name;


    /**
     * @var string. Page title
     */
    public $title;


    /**
     * @var string. Open Graph Type
     */
    public $type = 'website';


    /**
     * @var string. Facebook APP ID
     */
    public $facebook_app_id;


    /**
     * @var string. Current URL
     */
    public $url;


    /**
     * @var string. Image
     */
    public $image;


    /**
     * @var string. Preset image
     */
    public $preset_image = 'medium';


    /**
     * @var string. Description
     */
    public $description;


    /**
     * Meta Tags <meta>
     */
    private $_vec_metas = [];


    /**
     * Link Tags <link>
     */
    private $_vec_links = [];


    /**
     * Add tags from an array. Usually generated from a model like Category or Product
     */
    public function set_tags($vec_tags)
    {
        // Site Name
        if ( isset($vec_tags['site_name']) )
        {
            $this->set_site_name($vec_tags['site_name']);
        }

        // Title
        if ( isset($vec_tags['title']) )
        {
            $this->set_title($vec_tags['title']);
        }

        // Description
        if ( isset($vec_tags['description']) )
        {
            $this->set_description($vec_tags['description']);
        }

        // Organic Group Type
        if ( isset($vec_tags['type']) )
        {
            $this->set_type($vec_tags['type']);
        }

        // Page URL
        if ( isset($vec_tags['url']) )
        {
            $this->set_url($vec_tags['url']);
        }

        // Image
        if ( isset($vec_tags['image']) )
        {
            $this->set_image($vec_tags['image']);
        }

        // Image
        if ( isset($vec_tags['preset_image']) )
        {
            $this->preset_image = $vec_tags['preset_image'];
        }

        // OG tags (Facebook)
        if ( isset($vec_tags['og']) && is_array($vec_tags['og']) && !empty($vec_tags['og']) )
        {
            foreach ( $vec_tags['og'] as $tag_name => $tag_value )
            {
                $this->add_og_meta(['property' => $tag_name, 'content' => $tag_value]);
            }
        }

        return $this;
    }

    /**
     * Site Name
     */
    public function set_site_name($site_name)
    {
        $this->site_name = $site_name;
        return $this;
    }


    /**
     * Title page
     */
    public function set_title($title)
    {
        $this->title = $title;
        return $this;
    }


    /**
     * Page description
     */
    public function set_description($description)
    {
        $this->description = $description;
        if ( $this->description !== null && StringHelper::strlen($this->description) > 250 )
        {
            $this->description = StringHelper::substr($this->description, 0, 245);
        }

        return $this;
    }


    /**
     * Open Graph Type
     * 
     * @see https://ogp.me/#types
     */
    public function set_type($type)
    {
        $this->type = $type;
        return $this;
    }


    /**
     * Page URL
     */
    public function set_url($url)
    {
        $this->url = $url;
        return $this;
    }


    /**
     * Image
     */
    public function set_image($image_model)
    {
        $this->image = $image_model;
        return $this;
    }



    /**
     * Add a <meta> tag
     */
    public function add_meta($vec_options, $group = '_site', $key = null)
    {
        // Default group is "default"
        if ( $group === null )
        {
            $group = '_site';
        }

        if ( !isset($this->_vec_metas[$group]) )
        {
            $this->_vec_metas[$group] = [];
        }

        // Try to use "name", "itemprop" or "property" as key
        if ( $key === null  )
        {
            if ( isset($vec_options['name']) && !isset($this->_vec_metas[$group][$vec_options['name']]) )
            {
                $key = $vec_options['name'];
            }
            
            else if ( isset($vec_options['itemprop']) && !isset($this->_vec_metas[$group][$vec_options['itemprop']]) )
            {
                $key = $vec_options['itemprop'];
            }
            
            else if ( isset($vec_options['property']) && !isset($this->_vec_metas[$group][$vec_options['property']]) )
            {
                $key = $vec_options['property'];
            }
        }


        // Finally, add the meta tag
        if ( $key === null )
        {
            $this->_vec_metas[$group][] = $vec_options;
        }
        else
        {
            $this->_vec_metas[$group][$key] = $vec_options;
        }

        return $this;
    }


    /**
     * Add a <meta> tag for Organic Groups
     */
    public function add_og_meta($vec_options, $key = null)
    {
        // User "property" as key
        // For example, on <meta property="og:type" content="website">, "og:type" will be the key
        if ( $key === null && isset($vec_options['property']) )
        {
            $key = $vec_options['property'];
        }

        return $this->add_meta($vec_options, 'og', $key);
    }


    /**
     * Add a <meta> tag for Twitter
     */
    public function add_twitter_meta($vec_options, $key = null)
    {
        // User "name" as key
        // For example, on <meta name="twitter:card" content="summary_large_image">, "twitter:card" will be the key
        if ( $key === null && isset($vec_options['name']) )
        {
            $key = $vec_options['name'];
        }

        return $this->add_meta($vec_options, 'twitter', $key);
    }


    /**
     * Add a <link> tag
     */
    public function add_link($vec_options, $key = null)
    {
        if ( $key === null )
        {
            $this->_vec_links[] = $vec_options;
        }
        else
        {
            $this->_vec_links[$key] = $vec_options;
        }

        return $this;
    }


    /**
     * Registers a meta tag.
     * 
     * Port from Yii 1 CClientScript::registerMetaTag to Yii 2 View::registerMetaTag()
     * 
     * Yii 1 - registerMetaTag($content, $name = null, $httpEquiv = null, $options = array(), $id = null)
     * Yii 2 - registerMetaTag($options, $key = null)
     * 
     * @see https://www.yiiframework.com/doc/api/1.1/CClientScript#registerMetaTag-detail
     * @see https://www.yiiframework.com/doc/api/2.0/yii-web-view#registerMetaTag()-detail
     */
    public function register_tag($vec_options, $key = null)
    {
        $content = 'description';
        if ( isset($vec_options['content']) )
        {
            $content = $vec_options['content'];
        }

        return Yii::app()->clientScript->registerMetaTag($content, null, null, $vec_options, $key);
    }


    /**
     * Registers a link tag.
     * 
     * Port from Yii 1 CClientScript::registerLinkTag to Yii 2 View::registerLinkTag()
     * 
     * Yii 1 - registerLinkTag($relation = null, $type = null, $href = null, $media = null, $options = array())
     * Yii 2 - registerLinkTag($options, $key = null)
     * 
     * @see https://www.yiiframework.com/doc/api/1.1/CClientScript#registerLinkTag-detail
     * @see https://www.yiiframework.com/doc/api/2.0/yii-web-view#registerLinkTag()-detail
     */
    public function register_link($vec_options)
    {
        return Yii::app()->clientScript->registerLinkTag(null, null, null, null, $vec_options);
    }


    /**
     * Register meta and link tags on <head>
     */
    public function register()
    {
        // Register SITE meta tags
        $this->register_site();

        // Register TITLE meta tags
        $this->register_title();

        // Register DESCRIPTION meta tags
        $this->register_description();

        // Register IMAGE meta tags
        $this->register_image();

        // Add "fb:app_id" metatag
        if ( !empty($this->facebook_app_id) )
        {
            $this->add_meta(['property' => 'fb:app_id', 'content' => $this->facebook_app_id]);
        }

        // Add META tags into <HEAD>
        if ( !empty($this->_vec_metas) )
        {
            // Sort meta tags by key
            ksort($this->_vec_metas);

            foreach ( $this->_vec_metas as $group => $vec_meta_tags )
            {
                if ( !empty($vec_meta_tags) )
                {
                    foreach ( $vec_meta_tags as $que_meta_tag )
                    {
                        $this->register_tag($que_meta_tag);
                    }
                }
            }
        }

        // Add LINK tags into <HEAD>
        if ( !empty($this->_vec_links) )
        {
            foreach ( $this->_vec_links as $que_link_tag )
            {
                $this->register_link($que_link_tag);
            }
        }

        return true;
    }


    /**
     * Register title
     */
    public function register_title()
    {
        if ( !empty($this->title) )
        {
            Yii::app()->controller->setPageTitle($this->title);
            $this->add_meta(['itemprop' => 'name', 'content' => $this->title]);
            $this->add_og_meta(['property' => 'og:title', 'content' => $this->title]);

            return true;
        }
        else
        {
            $page_title = Yii::app()->controller->getPageTitle();
            if ( !empty($page_title) )
            {
                $this->add_meta(['itemprop' => 'name', 'content' => $page_title]);
                $this->add_og_meta(['property' => 'og:title', 'content' => $page_title]);
            }
        }

        return false;
    }


    /**
     * Register site meta tags
     */
    public function register_site()
    {
        // Site name
        if ( empty($this->site_name) )
        {
            $this->site_name = Yii::app()->name;
        }

        // Current URL
        if ( empty($this->url) )
        {
            $this->url = Url::current();
        }

        $this->add_og_meta(['property' => 'og:site_name', 'content' => $this->site_name]);
        $this->add_og_meta(['property' => 'og:type', 'content' => $this->type]);
        $this->add_og_meta(['property' => 'og:url', 'content' => $this->url]);
        $this->add_twitter_meta(['name' => 'twitter:card', 'content' => 'summary_large_image']);
        $this->add_twitter_meta(['name' => 'twitter:domain', 'content' => Url::base()]);
        $this->add_twitter_meta(['name' => 'twitter:site', 'content' => $this->site_name]);
        // $this->add_link(['rel' => 'canonical', 'href' => Url::canonical()]);

        return true;
    }


    /**
     * Register description tags
     */
    public function register_description()
    {
        if ( !empty($this->description) )
        {
            $this->add_meta(['name' => 'description', 'content' => $this->description]);
            $this->add_og_meta(['property' => 'og:description', 'content' => $this->description]);
            $this->add_twitter_meta(['name' => 'twitter:description', 'content' => $this->description]);

            return true;
        }

        return false;
    }


    /**
     * Register image
     */
    public function register_image($preset = null)
    {
        if ( $preset === null )
        {
            $preset = $this->preset_image;
        }

        if ( $this->image )
        {
            if ( is_object($this->image) )
            {
                // Image URL
                $image_url = $this->image->image_url($preset);
                $this->add_og_meta(['property' => 'og:image', 'content' => $image_url]);
                $this->add_twitter_meta(['name' => 'twitter:image', 'content' => $image_url]);
                $this->add_meta(['itemprop' => 'image', 'content' => $image_url]);

                // Preset image size
                if ( !empty($preset) )
                {
                    $preset_file = $this->image->get_preset_file($preset);
                    if ( $preset_file )
                    {
                        $vec_image_size = $this->image->get_image_size($preset_file->realPath);
                        if ( !empty($vec_image_size) && count($vec_image_size) > 2 )
                        {
                            $this->add_og_meta(['property' => 'og:image:width', 'content' => $vec_image_size[0]]);
                            $this->add_og_meta(['property' => 'og:image:height', 'content' => $vec_image_size[1]]);
                        }
                    }
                }

                // Original image size
                else
                {
                    $vec_image_size = $this->image->get_image_size();
                    if ( !empty($vec_image_size) && count($vec_image_size) > 2 )
                    {
                        $this->add_og_meta(['property' => 'og:image:width', 'content' => $vec_image_size[0]]);
                        $this->add_og_meta(['property' => 'og:image:height', 'content' => $vec_image_size[1]]);
                    }
                }
            }

            else
            {
                $image_url = $this->image;
                $this->add_og_meta(['property' => 'og:image', 'content' => $image_url]);
                $this->add_twitter_meta(['name' => 'twitter:image', 'content' => $image_url]);
                $this->add_meta(['itemprop' => 'image', 'content' => $image_url]);
            }

            return true;
        }

        return false;
    }
}
