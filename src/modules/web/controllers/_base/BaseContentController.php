<?php
/*
|--------------------------------------------------------------------------
| Base controller class for WebContent model
|
| Common class methods used in different controllers of web module
|--------------------------------------------------------------------------
*/

namespace dz\modules\web\controllers\_base;

use dz\helpers\Html;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\modules\web\models\WebContent;
use dz\web\Controller;
use Yii;

abstract class BaseContentController extends Controller
{
    /**
     * Content type
     */
    protected $content_type;


    /**
     * List action for WebContent (Page) models
     */
    public function actionIndex()
    {
        $model = Yii::createObject(WebContent::class, 'search');
        $model->unsetAttributes();
        $model->content_type = $this->content_type;
        
        if ( isset($_GET['WebContent']) )
        {
            $model->setAttributes($_GET['WebContent']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }

        // Web content type configuration
        $vec_config = $model->load_config($this->content_type);

        $this->render($model->get_view_path('index'), [
            'model'         => $model,
            'vec_config'    => $vec_config
        ]);
    }



    /**
     * Create action for WebContent (Page) model
     */
    public function actionCreate()
    {
        // WebContent model
        $content_model = Yii::createObject(WebContent::class);
        $content_model->content_type = $this->content_type;
        $content_model->language_id = Yii::defaultLanguage();

        // Create action
        $vec_data = Yii::app()->contentManager->create_content($content_model, $_POST);

        // Render form page
        $this->render($content_model->get_view_path('create'), $vec_data);
    }


    /**
     * Update action for WebContent model
     */
    public function actionUpdate($id)
    {
        // WebContent model
        $content_model = $this->loadModel($id, WebContent::class);

        // Update action
        $vec_data = Yii::app()->contentManager->update_content($content_model, $_POST);

        // Render update template
        $this->render($content_model->get_view_path('update'), $vec_data);
    }


    /**
     * Update content weights
     */
    public function actionUpdateWeight()
    {
        // Delete action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsPostRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        if ( isset($_POST['nestable']) AND !empty($_POST['nestable']) AND is_array($_POST['nestable']) )
        {
            $vec_nestable = $_POST['nestable'];
            if ( !empty($vec_nestable) )
            {
                foreach ( $vec_nestable as $new_weight => $que_content )
                {
                    if ( isset($que_content['id']) )
                    {
                        // $content_id = substr($que_content['id'], 2);
                        $content_id = $que_content['id'];

                        // UPDATE slideshow weight
                        Yii::app()->db->createCommand()->update('web_content',
                            ['weight' => $new_weight+1],
                            'content_id = :content_id OR translated_content_id = :content_id',
                            [':content_id' => $content_id]);
                    }
                }
            }

            echo Json::encode(['result' => 1]);
        }
        else
        {
            echo Json::encode(['result' => -1]);
        }

        // End application
        Yii::app()->end();
    }


    /**
     * Get content TREE structure for AJAX
     */
    public function actionTree()
    {
        // Create action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        $vec_ajax_output = [
            'result' => 'init'
        ];

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        // Current content type
        $content_model = Yii::createObject(WebContent::class);
        $content_model->content_type = $this->content_type;
        $vec_ajax_output = [
            'result' => 'success',
            'tree' => $this->renderPartial($content_model->get_view_path('_tree'), [
                'model'         => $content_model,
                'vec_config'    => $content_model->load_config($this->content_type),
                'is_ajax'       => true
            ], true, true)
        ];

        // Output in JSON format
        echo Json::encode($vec_ajax_output);
        Yii::app()->end();
    }


    /**
     * Disable a WebContent
     */
    public function actionDisable($id)
    {
        // Baja action only allowed by POST requests
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $content_model = $this->loadModel($id, WebContent::class);
            if ( $content_model->disable() )
            {
                if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
                {
                    Yii::app()->user->addFlash('success', Yii::t('app', $content_model->text('disable_success')) );
                }
                else
                {
                    echo Yii::t('app', $content_model->text('disable_success'));
                }
            }
            else
            {
                $this->showErrors($content_model->getErrors());
            }           
        }
        else
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }
    }
    
    
    
    /**
     * Enable a WebContent
     */
    public function actionEnable($id)
    {
        // Alta action only allowed by POST requests
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $content_model = $this->loadModel($id, WebContent::class);
            if ( $content_model->enable() )
            {
                if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
                {
                    Yii::app()->user->addFlash('success', Yii::t('app', $content_model->text('enable_success')) );
                }
                else
                {
                    echo Yii::t('app', $content_model->text('enable_success'));
                }
            }
            else
            {
                $this->showErrors($content_model->getErrors());
            }           
        }
        else
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }
    }



    /**
     * Delete action for WebContent model
     */
    public function actionDelete($id)
    {
        // Delete action only allowed by POST requests
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $content_model = $this->loadModel($id, WebContent::class);
            if ( $content_model->delete() )
            {
                if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
                {
                    Yii::app()->user->addFlash('success', Yii::t('app', $content_model->text('delete_success')) );
                }
                else
                {
                    echo Yii::t('app', $content_model->text('delete_success'));
                }
            }
            else
            {
                $this->showErrors($content_model->getErrors());
            }
            
            if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
            {
                $this->redirect(['index']);
            }
        }
        else
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }
    }


    /**
     * Custom texts for the content type
     */
    protected function text($text_key)
    {
        $content_model = Yii::createObject(WebContent::class);
        $content_model->content_type = $this->content_type;
        return $content_model->text($text_key);
    }

    
    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index'         => 'web.page.view',
            'tree'          => 'web.page.view',
            'enable'        => 'web.page.update',
            'disable'       => 'web.page.update',
            'updateWeight'  => 'web.page.update',
        ];
    }
}