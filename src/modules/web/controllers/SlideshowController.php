<?php
/*
|--------------------------------------------------------------------------
| Controller class for WebContent model (Slideshow)
|--------------------------------------------------------------------------
*/

namespace dz\modules\web\controllers;

use dz\helpers\Log;
use dz\modules\web\controllers\_base\BaseContentController;
use Yii;

class SlideshowController extends BaseContentController
{
    /**
     * Content type
     */
    protected $content_type = 'slideshow';
}