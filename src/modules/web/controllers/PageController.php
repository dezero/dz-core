<?php
/*
|--------------------------------------------------------------------------
| Controller class for WebContent model (Static Pages)
|--------------------------------------------------------------------------
*/

namespace dz\modules\web\controllers;

use dz\helpers\Log;
use dz\modules\web\controllers\_base\BaseContentController;
use Yii;

class PageController extends BaseContentController
{
    /**
     * Content type
     */
    protected $content_type = 'page';


    /**
     * Custom labels for the static page type
     */
    /*
    protected function text($text_key)
    {
        $vec_labels = [
            'entity_label'      => 'Static page',
            'entities_label'    => 'Static pages',

            'index_title'       => 'Manage static pages',
            'panel_title'       => 'Static pages',
            'list_title'        => 'Static pages list',
            'add_button'        => 'Add page',
            'create_title'      => 'Create page',

            // Success messages
            'created_success'   => 'New page created successfully',
            'updated_success'   => 'Page updated successfully',

            // Disable
            'disable_success'   => 'Page disabled successfully',
            'disable_error'     => 'Page could no be DISABLED',
            'disable_confirm'   => '<h3>Are you sure you want to DISABLE this static page?</h3>',

            // Enable
            'enable_success'    => 'Page enabled successfully',
            'enable_error'      => 'Page could not be ENABLED',
            'enable_confirm'   => '<h3>Are you sure you want to ENABLE this static page?</h3>',

            // Delete
            'delete_success'    => 'Page deleted successfully',
            'delete_error'      => 'Page could not be DELETED',
            'delete_confirm'    => '<h3>Are you sure you want to DELETE this static page?</h3>',

            // Other
            'empty_text'        => 'No static pages found'
        ];

        if ( isset($vec_labels[$text_key]) )
        {
            return $vec_labels[$text_key];
        }

        return '';
    }
    */
}