<?php
/*
|--------------------------------------------------------------------------
| Controller class for WebContent model (Document)
|--------------------------------------------------------------------------
*/

namespace dz\modules\web\controllers;

use dz\helpers\Log;
use dz\modules\web\controllers\_base\BaseContentController;
use Yii;

class DocumentController extends BaseContentController
{
    /**
     * Content type
     */
    protected $content_type = 'document';
}