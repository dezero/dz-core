<?php
/*
|--------------------------------------------------------------------------
| Controller class to admin blocks
|
| THIS IS A SAMPLE CONTROLLER CLASS. YOU DO NOT TO EXTEND IT!
|--------------------------------------------------------------------------
*/

namespace dz\modules\web\controllers;

use dz\modules\web\models\WebBlock;
use dz\web\Controller;
use Yii;

class BlockController extends Controller
{
    /**
     * Default action
     */
    public $defaultAction = 'home';


    /**
     * Home blocks
     */
    public function actionHome()
    {
        // Home header block
        Yii::app()->blockManager->add_block('home_header');

        // Insert new model?
        if ( isset($_POST['WebBlock']) )
        {
            if ( Yii::app()->blockManager->save_blocks($_POST['WebBlock']) )
            {
                Yii::app()->user->addFlash('success', 'Block updated successfully');

                // Redirect to same page
                $this->redirect(['/web/block/home']);
            }
        }

        $this->render('update', [
            'title'         => 'Home blocks',
            'vec_models'    => Yii::app()->blockManager->get_blocks(),
            'form_id'       => 'web-block-form'
        ]);
    }


    /**
     * Footer section
     */
    public function actionFooter()
    {
        // Left column
        Yii::app()->blockManager->add_block('footer_left');

        // Center column
        Yii::app()->blockManager->add_block('footer_center');

        // Right column
        Yii::app()->blockManager->add_block('footer_right');

        // Insert new model?
        if ( isset($_POST['WebBlock']) )
        {
            if ( Yii::app()->blockManager->save_blocks($_POST['WebBlock']) )
            {
                Yii::app()->user->addFlash('success', 'Blocks updated successfully');

                // Redirect to same page
                $this->redirect(['/web/content/footer']);
            }
        }

        $this->render('update', [
            'title'         => 'Footer blocks',
            'vec_models'    => Yii::app()->contentManager->get_models(),
            'form_id'       => 'web-content-form'
        ]);
    }



    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'home'      => 'web_manage',
            'footer'    => 'web_manage',
        ];
    }
}