<?php
/*
|--------------------------------------------------------------------------
| Controller class for WebContact model
|--------------------------------------------------------------------------
*/

namespace dz\modules\web\controllers;

use dz\helpers\Html;
use dz\modules\web\models\WebContact;
use dz\web\Controller;
use Yii;

class ContactController extends Controller
{	
	/**
	 * List action for WebContact models
	 */
	public function actionIndex()
	{
		$model = Yii::createObject(WebContact::class, 'search');
		$model->unsetAttributes();
		
		if ( isset($_GET['WebContact']) )
		{
			$model->setAttributes($_GET['WebContact']);
		}

		// Update GRID with AJAX
		if ( Yii::app()->getRequest()->getIsAjaxRequest() )
		{
			// Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
			Yii::app()->clientScript->reset();
		}
		
		$this->render('index', [
			'model' => $model,
		]);
	}


	/**
     * Delete action for WebContact model
     */
    public function actionDelete($id)
    {
        // Delete action only allowed by POST requests
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $model = $this->loadModel($id, WebContact::class);
            if ( $model->delete() )
            {
                if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
                {
                    Yii::app()->user->addFlash('success', 'Message removed successfully');
                }
                else
                {
                    echo Yii::t('app', 'Message removed successfully');
                }
            }
            else
            {
                $this->showErrors($model->getErrors());
            }
            
            if ( !Yii::app()->getRequest()->getIsAjaxRequest() )
            {
                $this->redirect(['index']);
            }
        }
        else
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }
    }
}
