<?php
/**
 * Migration class m190201_160000_web_content_tables
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m190201_160000_web_content_tables extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        /// Create "web_content" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('web_content', true);

        $this->createTable('web_content', [
            'content_id' => $this->primaryKey(),
            'content_type' => $this->string(32)->notNull()->defaultValue('page'),
            'internal_name' => $this->string(64),
            'language_id' => $this->string(4)->notNull()->defaultValue('es'),
            'translated_content_id' => $this->integer()->unsigned(),
            'title' => $this->string(255),
            'subtitle' => $this->string(255),
            'body' => $this->text(),
            'weight' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(1),
            'link_title' => $this->string(255),
            'link_url' => $this->string(255),
            'image_id' => $this->integer()->unsigned(),
            'disable_date' => $this->date(),
            'disable_uid' => $this->integer()->unsigned(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);
    
        // Create indexes
        $this->createIndex(null, 'web_content', ['content_type'], false);
        $this->createIndex(null, 'web_content', ['content_type', 'language_id'], false);
        $this->createIndex(null, 'web_content', ['content_type', 'internal_name'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'web_content', ['language_id'], 'language', ['language_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'web_content', ['translated_content_id'], 'web_content', ['content_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'web_content', ['image_id'], 'asset_file', ['file_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'web_content', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'web_content', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'web_content', ['disable_uid'], 'user_users', ['id'], 'CASCADE', null);


        /// Create "web_contact" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('web_contact', true);

        $this->createTable('web_contact', [
            'contact_id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'email' => $this->string(64),
            'phone' => $this->string(32),
            'message' => $this->text()->notNull(),
            'language_id' => $this->string(4)->notNull()->defaultValue('es'),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'web_contact', ['language_id'], 'language', ['language_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'web_contact', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);


        // Permissions
        // -------------------------------------------------------------------------
        $this->insertMultiple('user_auth_item', [
            [
                'name'          => 'web_manage',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Web Content - Full access to settings',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'web.page.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Web Content - Pages - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'web.text.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Web Content - Texts - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'web.contact.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Web Content - Contact Submissions - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
        ]);

        // Set items relationships
        $this->insertMultiple('user_auth_item_child', [
            [
                'parent'    => 'admin',
                'child'     => 'web_manage'
            ],
            [
                'parent'    => 'editor',
                'child'     => 'web_manage'
            ],
            [
                'parent'    => 'web_manage',
                'child'     => 'web.page.*'
            ],
            [
                'parent'    => 'web_manage',
                'child'     => 'web.text.*'
            ],
            [
                'parent'    => 'web_manage',
                'child'     => 'web.contact.*'
            ],
        ]);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('web_content');
        // $this->dropTable('web_contact');
		return false;
	}
}

