<?php
/**
 * Migration class m190202_160000_seo_tables
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;

class m190202_160000_seo_tables extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "web_seo" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('web_seo', true);

        $this->createTable('web_seo', [
            'entity_id' => $this->integer()->notNull(),
            'entity_type' => $this->string(32)->notNull(),
            'language_id' => $this->string(4)->notNull()->defaultValue('es'),
            'url_auto' => $this->string(255),
            'url_counter' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(0),
            'url_manual' => $this->string(255),
            'meta_title' => $this->string(255),
            'meta_description' => $this->string(512),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);
    
        // Create primary key
        $this->addPrimaryKey(null, 'web_seo', ['entity_id', 'entity_type', 'language_id']);

        // Create indexes
        $this->createIndex(null, 'web_seo', ['entity_id', 'entity_type'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'web_seo', ['language_id'], 'language', ['language_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'web_seo', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'web_seo', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);
        


        // Create "web_seo_url_history" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('web_seo_url_history', true);

        $this->createTable('web_seo_url_history', [
            'url_history_id' => $this->primaryKey(),
            'entity_id' => $this->integer()->notNull(),
            'entity_type' => $this->string(32)->notNull(),
            'language_id' => $this->string(4)->notNull()->defaultValue('es'),
            'url' => $this->string(255)->notNull(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create indexes
        $this->createIndex(null, 'web_seo_url_history', ['entity_id', 'entity_type'], false);
        $this->createIndex(null, 'web_seo_url_history', ['url'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'web_seo_url_history', ['language_id'], 'language', ['language_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'web_seo_url_history', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'web_seo_url_history', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
        // $this->dropTable('web_seo');
		// $this->dropTable('web_seo_url_history');
		return false;
	}
}

