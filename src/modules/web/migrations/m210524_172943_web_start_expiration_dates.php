<?php
/**
 * Migration class m210524_172943_web_start_expiration_dates
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210524_172943_web_start_expiration_dates extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Add columns to schedule web content
        $this->addColumn('web_content', 'is_scheduled', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0)->after('image_id'));
		$this->addColumn('web_content', 'start_date', $this->date()->after('is_scheduled'));
        $this->addColumn('web_content', 'expiration_date', $this->date()->after('start_date'));
        $this->addColumn('web_content', 'file_id', $this->integer()->unsigned()->after('image_id'));

        // Create index
        $this->createIndex(null, 'web_content', ['content_type', 'is_scheduled'], false);

        // Create Foreign Key
        $this->addForeignKey(null, 'web_content', ['file_id'], 'asset_file', ['file_id'], 'SET NULL', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

