<?php
/**
 * SitemapCommand class file
 *
 * This command is used to generate Sitemaps
 *
 * It requires Samdark's Sitemap library
 *  > composer require samdark/sitemap
 * 
 * @see https://github.com/samdark/sitemap
 * 
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2021 Fabián Ruiz
 * @package commands
 */

namespace dz\modules\web\commands;

use dz\console\CronCommand;
use dz\helpers\Log;
use Yii;

class SitemapCommand extends CronCommand
{
    /**
     * Create a sitemap.xml file
     *
     * ./yiic sitemap create
     */
    public function actionCreate()
    {
        if ( Yii::app()->sitemap->create() )
        {
            $log_message = Yii::app()->sitemap->get_total_items() .' URL\'s added into '. Yii::app()->sitemap->get_file_path();
            Log::seo($log_message);
        }
        else
        {
            Log::seo('Error generating sitemap.xml file');
        }
    }
}