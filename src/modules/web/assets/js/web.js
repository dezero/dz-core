(function(document, window, $) {
  // Update tree widget after delete
  dzAfterDeleteContentGrid = function($link, data) {
    $.nestableContentReload($link.data('content-type')); 
  };

   // Reload tree widget
  $.nestableContentReload = function(content_type) {
    var que_url = window.js_globals.baseUrl + '/web/'+ content_type +'/tree';
    $.dzNestableReload(que_url, content_type);
  };

  $(document).ready(function() {
    // Disable/enable buttons
    $('a[data-plugin="dz-status-button"]').dzStatusButton();

    // When a TAB is shown, refresh SIMPLEMDE MARKDOWN for other languages - WebBlock models
    $('#web-block-form').find('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      var $this = $(this);
      var current_language = $(e.target).data('language');
      if ( current_language != js_globals.defaultLanguage ) {
        setTimeout(function() {
          var block_name = $this.parent().parent().data('block-name');
          window['simplemde_WebBlock_'+ block_name +'_' + current_language +'_body_markdown'].codemirror.refresh();
        }, 0);
      }
    });

    // When a TAB is shown, refresh SIMPLEMDE MARKDOWN for other languages - TraslatedContent models
    $('.web-content-form').find('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      var $this = $(this);
      var current_language = $(e.target).data('language');
      if ( current_language != js_globals.defaultLanguage ) {
        setTimeout(function() {
          window['simplemde_TranslatedContent_'+ current_language +'_body_markdown'].codemirror.refresh();
        }, 0);
      }
    });

    // Date range picker
    var date_options = {
      autoclose: true,
      clearBtn: true,
      format: 'dd/mm/yyyy',
      weekStart: 1,
      language: js_globals.language
    };
    $('#web-content-date-range').datepicker(date_options);
  });
})(document, window, jQuery);