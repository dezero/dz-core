<?php
/**
 * @package dz\modules\web\models 
 */

namespace dz\modules\web\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dz\modules\category\models\Category;
use dzlab\commerce\models\Product;
use dz\modules\settings\models\Language;
use dz\modules\web\models\_base\Seo as BaseSeo;
use dz\modules\web\models\WebContent;
use user\models\User;
use Yii;

/**
 * Seo model class for "web_seo" database table
 *
 * Columns in table "web_seo" available as properties of the model,
 * followed by relations of table "web_seo" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $entity_id
 * @property string $entity_type
 * @property string $language_id
 * @property string $url_auto
 * @property integer $url_counter
 * @property string $url_manual
 * @property string $meta_title
 * @property string $meta_description
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $language
 * @property mixed $updatedUser
 */
class Seo extends BaseSeo
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['entity_id, entity_type, created_date, created_uid, updated_date, updated_uid', 'required'],
			['entity_id, url_counter, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['entity_type', 'length', 'max'=> 32],
			['language_id', 'length', 'max'=> 4],
			['url_auto, url_manual, meta_title', 'length', 'max'=> 255],
			['meta_description', 'length', 'max'=> 512],
            ['uuid', 'length', 'max'=> 36],
			['language_id, url_auto, url_counter, url_manual, meta_title, meta_description', 'default', 'setOnEmpty' => true, 'value' => null],
			['entity_id, entity_type, language_id, url_auto, url_counter, url_manual, meta_title, meta_description, created_date, created_uid, updated_date, updated_uid', 'safe', 'on' => 'search'],

            // Custom rules
            ['url_manual', 'validate_seo_url_exists'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'language' => [self::BELONGS_TO, Language::class, 'language_id'],
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'entity_id' => Yii::t('app', 'Entity'),
			'entity_type' => Yii::t('app', 'Entity Type'),
			'language_id' => Yii::t('app', 'Language'),
			'url_auto' => Yii::t('app', 'Auto URL'),
			'url_counter' => Yii::t('app', 'Url Counter'),
			'url_manual' => Yii::t('app', 'Manual URL'),
			'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_description' => Yii::t('app', 'Meta Description'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
            'uuid' => Yii::t('app', 'Uuid'),
			'createdUser' => null,
			'language' => null,
			'updatedUser' => null,
		];
	}


    /**
     * Event "afterValidate"
     *
     * This method is invoked after validation end
     */
    public function afterValidate()
    {
        if ( !empty($this->entity_id) )
        {
            $vec_seo_data = [];
            $vec_old_attributes = $this->getOldAttributes();

            // Generate AUTO URL and meta-title
            switch ( $this->entity_type )
            {
                // Web content: Pages
                case 'web_content':
                    $web_content_model = WebContent::findOne($this->entity_id);
                    if ( $web_content_model )
                    {
                        // Generate SEO data from WebContent model
                        $vec_seo_data = $web_content_model->generate_seo($this->language_id);
                    }
                break;

                // Categories
                case 'category':
                    $category_model = Category::findOne($this->entity_id);
                    if ( $category_model )
                    {
                        // Generate SEO data from Category model
                        $vec_seo_data = $category_model->generate_seo($this->language_id);
                    }
                break;

                // Products
                case 'product':
                    $product_model = Product::findOne($this->entity_id);
                    if ( $product_model )
                    {
                        // Generate SEO data from Product model
                        $vec_seo_data = $product_model->generate_seo($this->language_id);
                    }
                break;
            }

            // Save new SEO data generated from source models
            if ( !empty($vec_seo_data) )
            {
                // URL
                if ( isset($vec_seo_data['url_auto']) )
                {
                    $this->url_auto = $vec_seo_data['url_auto'];
                    // if ( empty($this->url_manual) || $vec_old_attributes['url_manual'] === $this->url_manual )
                    if ( empty($this->url_manual) )
                    {
                        $this->url_manual = $this->url_auto;
                    }
                }

                // Meta-title
                if ( isset($vec_seo_data['meta_title']) && empty($this->meta_title) )
                {
                    $this->meta_title = $vec_seo_data['meta_title'];
                }

                // Meta-description
                if ( isset($vec_seo_data['meta_description']) && empty($this->meta_description) )
                {
                    $this->meta_description = $vec_seo_data['meta_description'];
                }
            }
        }

        return parent::afterValidate();
    }



    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.entity_id', $this->entity_id);
        $criteria->compare('t.entity_type', $this->entity_type);
        $criteria->compare('t.url_auto', $this->url_auto, true);
        $criteria->compare('t.url_manual', $this->url_manual, true);        
        $criteria->compare('t.meta_title', $this->meta_title, true);
        $criteria->compare('t.meta_description', $this->meta_description, true);

        // $criteria->compare('t.url_counter', $this->url_counter);
        // $criteria->compare('t.created_date', $this->created_date);
        // $criteria->compare('t.updated_date', $this->updated_date);
        // $criteria->compare('t.uuid', $this->uuid);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['created_date' => true]]
        ]);
    }


    /**
     * Check if "URL" is unique
     */
    public function validate_seo_url_exists($attribute, $params)
    {
        if ( !empty($this->url_manual) )
        {
            if ( Yii::app()->seo->check_url_unique($this->url_manual, $this->entity_id, $this->entity_type, $this->language_id) )
            {
                $this->addError('url_manual',  Yii::t('app', 'It already exists a '. $this->entity_type .' with URL <em>'. $this->url_manual .'</em>.'));
            }
        }
    }
}
