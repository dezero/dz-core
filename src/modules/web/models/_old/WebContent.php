<?php
/**
 * @file models/WebContent.php
 * WebContent model class for "web_content" database table
 *
 * Columns in table "web_content" available as properties of the model,
 * followed by relations of table "web_content" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property string $content_id
 * @property string $content_type
 * @property string $content_subtype
 * @property string $language_id
 * @property string $translated_content_id
 * @property string $title
 * @property string $body
 * @property string $subtitle
 * @property integer $weight
 * @property string $link_title
 * @property string $link_url
 * @property string $image_id
 * @property string $json_options
 * @property string $disable_date
 * @property string $disable_uid
 * @property string $created_date
 * @property string $created_uid
 * @property string $updated_date
 * @property string $updated_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property Language $language
 * @property WebContent $translatedContent
 * @property AssetImage $image
 * @property User $disableUser
 * @property User $createdUser
 * @property User $updatedUser
 */

use dz\helpers\Transliteration;

// Import model base class for the table "web_content"
Yii::import('web.models._base.BaseWebContent');

class WebContent extends BaseWebContent
{
	/**
	 * Filtro por "Mostrar Bajas" en CGridView
	 *
	 *    1 => 'Mostrar solo ALTAS'
	 *	  2 => 'Mostrar TODOS'
	 *
	 * @var int
	 */	
	public $disable_filter = 2;


	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return array(
			array('created_date, created_uid, updated_date, updated_uid', 'required'),
			array('weight', 'numerical', 'integerOnly'=>true),
			array('content_type, content_subtype', 'length', 'max'=>32),
			array('language_id', 'length', 'max'=>4),
			array('translated_content_id, image_id, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'length', 'max'=>10),
			array('title, subtitle, link_title, link_url, json_options', 'length', 'max'=>255),
			array('body', 'safe'),
			array('content_type, content_subtype, language_id, translated_content_id, title, body, subtitle, weight, link_title, link_url, image_id, json_options, disable_date, disable_uid', 'default', 'setOnEmpty' => true, 'value' => null),
			array('content_id, content_type, content_subtype, language_id, translated_content_id, title, body, subtitle, weight, link_title, link_url, image_id, json_options, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid, disable_filter', 'safe', 'on'=>'search'),

			// Custom validation rules
			// array('image_id', 'dzValidateImage'),
		);
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return array(
			'language' => array(self::BELONGS_TO, 'Language', 'language_id'),
			'translatedContent' => array(self::BELONGS_TO, 'WebContent', 'translated_content_id'),
			'webContents' => array(self::HAS_MANY, 'WebContent', 'translated_content_id'),
			'image' => array(self::BELONGS_TO, 'AssetImage', array('image_id' => 'file_id')),
			'disableUser' => array(self::BELONGS_TO, 'User', array('disable_uid' => 'id')),
			'createdUser' => array(self::BELONGS_TO, 'User', array('created_uid' => 'id')),
			'updatedUser' => array(self::BELONGS_TO, 'User', array('updated_uid' => 'id')),

			// Custom relations
			'seo' => array(self::BELONGS_TO, 'Seo', array('content_id' => 'entity_id'), 'condition' => 'seo.entity_type = "content_page"', 'order' => 'seo.updated_date DESC'),
		);
	}
	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return array(
			'content_id' => Yii::t('app', 'Content'),
			'content_type' => Yii::t('app', 'Tipo de contenido'),
			'content_subtype' => Yii::t('app', 'Subtipo de contenido'),
			'language_id' => Yii::t('app', 'Idioma'),
			'translated_content_id' => Yii::t('app', 'Contenido en idioma por defecto'),
			'title' => Yii::t('app', 'Título'),
			'body' => Yii::t('app', 'Texto'),
			'subtitle' => Yii::t('app', 'Subtítulo'),
			'weight' => Yii::t('app', 'Orden'),
			'image_id' => Yii::t('app', 'Imagen'),
			'link_title' => Yii::t('app', 'Link Title'),
			'link_url' => Yii::t('app', 'Link Url'),
			'json_options' => Yii::t('app', 'Json Options'),
			'disable_date' => Yii::t('app', 'Disable Date'),
			'disable_uid' => null,
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'language' => Yii::t('app', 'Idioma'),
			'translatedContent' => Yii::t('app', 'Contenido en idioma por defecto'),
			'webContents' => null,
			'image' => Yii::t('app', 'Imagen'),
			'disableUser' => null,
			'createdUser' => null,
			'updatedUser' => null,
		);
	}
	
	
	/*
	|--------------------------------------------------------------------------
	| CUSTOM FUNCTIONS
	|--------------------------------------------------------------------------
	*/


	/**
	 * Event "beforeValidate"
	 *
	 * This method is invoked before validation starts
	 *
	 * @return bool
	 */
	public function beforeValidate()
	{
    	// Create new WebContent model
    	switch ( $this->scenario )
    	{
    		// Create new WebContent
    		case 'insert':
    			$que_internal_name = '';

    			// Get last weight
				$criteria = new CDbCriteria();
				$criteria->compare('content_type', $this->content_type);
				$criteria->order = 'weight DESC';
				$criteria->limit = 1;
				$last_web_content_model = WebContent::model()->find($criteria);
				if ( count($last_web_content_model) > 0 )
				{
					$this->weight = $last_web_content_model->weight + 1;
				}
    		break;
    	}

    	return parent::beforeValidate();
    }


    /**
	 * Generate an ActiveDataProvider for search form of this model
	 *
	 * Used in CGridView
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('t.content_id', $this->content_id);
		$criteria->compare('t.content_type', $this->content_type);
		$criteria->compare('t.content_subtype', $this->content_subtype);
		$criteria->compare('t.title', $this->title, TRUE);
		$criteria->compare('t.body', $this->body, TRUE);
		$criteria->addCondition('t.translated_content_id IS NULL');

		// Disabled/enabled filter
		if ( isset($_GET['disable_filter']) )
		{
			$this->disable_filter = $_GET['disable_filter'];
		}

		// Filtro por altas/bajas
		if ( $this->disable_filter )
		{
			switch ( $this->disable_filter )
			{
				// Mostrar solo ALTAS
				case 1:
					$criteria->addCondition('t.disable_date IS NULL OR t.disable_date = "0"');
				break;
				
				// Mostrar TODOS
				case 2:
				break;
			}
		}

		// Filtro por traducciones
		/*
		if ( $this->name )
		{
			$criteria->distinct = TRUE;
			$criteria->join .= 'LEFT JOIN pim_translated_category category_translation ON (category_translation.category_id = t.category_id)';
			$criteria->addCondition('t.name LIKE "%'. $this->name .'%" OR category_translation.name LIKE "%'. $this->name .'%"');
		}
		*/

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => FALSE,
			'sort' => array(
				'defaultOrder' => array('weight' => FALSE)
			)
		));
	}


	/**
	 * Returns the SEO URL for this model
	 * 
	 * @see CApplication::createUrl() method
	 */
	public function createUrl($vec_params=array(), $ampersand='&')
	{
		$route = 'page/'. $this->content_id;
		if ( $this->seo )
		{
			$route = $this->seo->url_manual;	
		}

		return Yii::app()->createUrl($route, $vec_params, $ampersand);
	}


	/**
	 * Returns the SEO absolute URL for this model
	 * 
	 * @see CApplication::createAbsoluteUrl() method
	 */
	public function createAbsoluteUrl($vec_params=array(), $schema='', $ampersand='&')
	{
		$route = 'page/'. $this->content_id;
		if ( $this->seo )
		{
			$route = $this->seo->url_manual;	
		}
		
		return Yii::app()->createAbsoluteUrl($route, $vec_params, $schema, $ampersand);
	}


	/**
	 * Event "dzValidateBloqueo"
	 *
	 * Validación de bloqueos
	 */
	public function dzValidateImage($attribute, $params)
	{
		if ( ($this->content_type == 'S' OR $this->content_type == 'L' OR $this->content_type == 'M') AND empty($this->image_id) )
		{
			$this->addError('image_id', 'La imagen no puede ser nula');
		}
	}


	/**
	 * Disables model
	 *
	 * @return bool
	 */
	public function disable()
	{
		$this->scenario = 'disable';		
		$this->disable_date = time();
		$this->disable_uid = Yii::app()->user->id;
		if ( $this->save() )
		{
			if ( $this->translatedContent )
			{
				return $this->translatedContent->disable();
			}
			return TRUE;
		}

		return FALSE;
	}


	/**
	 * Enables model
	 *
	 * @return bool
	 */
	public function enable()
	{
		$this->scenario = 'enable';
		$this->disable_date = NULL;
		$this->disable_uid = NULL;
		if ( $this->save() )
		{
			if ( $this->translatedContent )
			{
				return $this->translatedContent->enable();
			}
			return TRUE;
		}

		return FALSE;
	}


	/**
	 * Check if model is enabled
	 */
	public function is_enabled()
	{
		return empty($this->disable_date);
	}


	/**
	 * CDbCriteria (query criteria) for results showed in a <SELECT> list
	 *
	 * Show just enabled web content models
	 *
	 * @return CDbCriteria
	 */
	public function enabledCriteria($vec_content_filter = array())
	{
		$criteria = new CDbCriteria;
		$criteria->addCondition('disable_date IS NULL OR disable_date = "0"');
		$criteria->compare('content_type', $this->content_type);
		$criteria->addCondition('translated_content_id IS NULL');
		if ( !empty($vec_content_filter) )
		{
			$criteria->addCondition('t.content_id IN ('. implode(",", $vec_content_filter) .')');
		}
		$criteria->order = 'weight ASC';
		return $criteria;
	}


	/**
	 * Deletes the model
	 */
	public function delete()
	{
		// Delete image file
		if ( $this->image )
		{
			// Delege original image file
			$this->image->delete();
		}
		
		return parent::delete();
	}


	/**
     * Return image URL
     *
     * $preset can be a string (small, medium, large) or an array with height and width coordinates
     */
    public function image_url($preset = '')
    {
    	if ( $this->image )
    	{ 
	    	$image_path = Yii::app()->params['baseUrl'] . DIRECTORY_SEPARATOR . $this->image->file_path;
	    	
	    	// Remove last "/"
	    	if ( preg_match("/\/$/", $image_path) )
	    	{
	    		$image_path = substr($image_path, 0, -1);
	    	}

	    	// Remove "www" (redirect via htaccess)
	    	$image_path = str_replace("www/files/", "files/", $image_path);

	    	// Presets configuration
	    	$vec_presets_config = $this->image->get_config();

	    	$preset_prefix = '';
	    	if ( !empty($preset) )
	    	{
	    		// PREDEFINED PRESET: small, medium or large
	    		if ( ! is_array($preset) )
	    		{
	    			$preset = strtoupper($preset);
	    			foreach ( $vec_presets_config['presets'] as $preset_id => $que_preset )
	    			{
	    				// Accepted input PRESET parameter accepted
	    				//	- SMALL (by preset_id in uppercase)
	    				//	- S_ 	(by prefix with "_" character)
	    				//	- S 	(by prefix without "_" character)
	    				if ( isset($que_preset['prefix']) AND (strtoupper($preset_id) == $preset OR $que_preset['prefix'] == $preset OR $que_preset['prefix'] == $preset .'_' ) )
	    				{
	    					$preset_prefix = $que_preset['prefix'];
	    				}
	    			}

	    			/*
			    	switch ( $preset )
			    	{
			    		// SMALL: 100x100
			    		case 'SMALL':
			    		case 'S':
			    			$preset_prefix = 'S_';
			    		break;

			    		// MEDIUM: 300x300
			    		case 'MEDIUM':
			    		case 'M':
			    			$preset_prefix = 'M_';
			    		break;

			    		// LARGE: 1200x1200
			    		case 'LARGE':
			    		case 'L':
			    			$preset_prefix = 'L_';
			    		break;
			    	}
			    	*/

			    	if ( !empty($preset_prefix) )
			    	{
			    		// return $image_path . $this->presets_dir . DIRECTORY_SEPARATOR . $preset_prefix . $this->file_name;
			    		return $image_path . DIRECTORY_SEPARATOR . '_' . DIRECTORY_SEPARATOR . $preset_prefix . $this->image->file_name;
			    	}
			    }

			    // DYNAMIC PRESET GENERATION: Array with width and height
			    else if ( count($preset) == 2 )
			    {
			    	if ( $this->image->load_file() )
			    	{
			    		return Yii::app()->iwi->load($this->image->file->realPath)->adaptive($preset[0],$preset[1])->cache();
			    	}
			    }
		    }
		    
		    // No preset. Return original image
		    return $image_path . DIRECTORY_SEPARATOR . $this->image->file_name;
		}

		return '';
    }


	/**
	 * Delete current image
	 */
	public function delete_image()
	{
		if ( $this->image AND $this->image->load_file() )
		{
			// Delete image
			$this->image->delete();

			// Update image_id
			$this->saveAttributes(array('image_id' => NULL));

			return TRUE;
		}

		return FALSE;
	}


	/**
	 * Get SEO URL generated automatically
	 */
	public function get_seo_url_auto()
	{
		return Transliteration::url($this->title);
	}
}