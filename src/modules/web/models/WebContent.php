<?php
/**
 * @package dz\modules\web\models 
 */

namespace dz\modules\web\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Html;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\helpers\Transliteration;
use dz\helpers\Url;
use dz\modules\asset\models\AssetImage;
use dz\modules\asset\models\AssetFile;
use dz\modules\settings\models\Language;
use dz\modules\web\models\_base\WebContent as BaseWebContent;
use user\models\User;
use Yii;

/**
 * WebContent model class for "web_content" database table
 *
 * Columns in table "web_content" available as properties of the model,
 * followed by relations of table "web_content" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $content_id
 * @property string $content_type
 * @property string $internal_name
 * @property string $language_id
 * @property integer $translated_content_id
 * @property string $title
 * @property string $subtitle
 * @property string $body
 * @property integer $weight
 * @property string $link_title
 * @property string $link_url
 * @property integer $image_id
 * @property integer $file_id
 * @property integer $is_scheduled
 * @property integer $start_date
 * @property integer $expiration_date
 * @property integer $disable_date
 * @property integer $disable_uid
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $disableUser
 * @property mixed $image
 * @property mixed $file
 * @property mixed $language
 * @property mixed $translatedContent
 * @property mixed $webContents
 * @property mixed $updatedUser
 */
class WebContent extends BaseWebContent
{
    /**
     * Filtro por "Mostrar Bajas" en CGridView
     *
     *    1 => 'Mostrar solo ALTAS'
     *    2 => 'Mostrar solo BAJAS'
     *    3 => 'Mostrar TODOS'
     *
     * @var string
     */ 
    public $disable_filter = 3;


    /**
     * Model configuration
     */
    protected $vec_config = [];


	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['title, created_date, created_uid, updated_date, updated_uid', 'required'],
			['translated_content_id, weight, image_id, file_id, is_scheduled, start_date, expiration_date, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['content_type', 'length', 'max'=> 32],
			['internal_name', 'length', 'max'=> 64],
			['language_id', 'length', 'max'=> 4],
			['title, subtitle, link_title, link_url', 'length', 'max'=> 255],
            ['uuid', 'length', 'max'=> 36],
			['content_type, internal_name, language_id, translated_content_id, title, subtitle, body, weight, link_title, link_url, image_id, file_id, is_scheduled, start_date, expiration_date, disable_date, disable_uid', 'default', 'setOnEmpty' => true, 'value' => null],
			['body', 'safe'],
			['content_id, content_type, internal_name, language_id, translated_content_id, title, subtitle, body, weight, link_title, link_url, image_id, file_id, is_scheduled, start_date, expiration_date, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid, disable_filter', 'safe', 'on' => 'search'],

            // Custom validation rules
            ['start_date', 'validate_dates', 'on' => 'insert, update'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'language' => [self::BELONGS_TO, Language::class, 'language_id'],
            'image' => [self::BELONGS_TO, AssetImage::class, ['image_id' => 'file_id']],
            'file' => [self::BELONGS_TO, AssetFile::class, 'file_id'],
			'translatedContent' => [self::BELONGS_TO, WebContent::class, 'translated_content_id'],
			'webContents' => [self::HAS_MANY, WebContent::class, 'translated_content_id'],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
            'disableUser' => [self::BELONGS_TO, User::class, ['disable_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
            'translations' => [self::HAS_MANY, WebContent::class, ['translated_content_id' => 'content_id']],
            'seo' => [self::HAS_MANY, Seo::class, ['entity_id' => 'content_id'], 'condition' => 'seo.entity_type = "web_content"', 'order' => 'seo.updated_date DESC'],
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'start_date' => 'd/m/Y',
                    'expiration_date' => 'd/m/Y',
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'content_id' => Yii::t('app', 'Content'),
			'content_type' => Yii::t('app', 'Content Type'),
			'internal_name' => Yii::t('app', 'Internal Name'),
			'language_id' => Yii::t('app', 'Language'),
			'translated_content_id' => Yii::t('app', 'Content for default language'),
			'title' => Yii::t('app', 'Title'),
            'subtitle' => Yii::t('app', 'Subtitle'),
			'body' => Yii::t('app', 'Text'),
			'weight' => Yii::t('app', 'Weight'),
			'link_title' => Yii::t('app', 'Link Title'),
			'link_url' => Yii::t('app', 'Link Url'),
			'image_id' => Yii::t('app', 'Image'),
            'file_id' => Yii::t('app', 'File'),
            'is_scheduled' => Yii::t('app', 'Scheduled?'),
            'start_date' => Yii::t('app', 'Start Date'),
            'expiration_date' => Yii::t('app', 'Expiration Date'),
			'disable_date' => Yii::t('app', 'Disable Date'),
			'disable_uid' => null,
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
            'uuid' => Yii::t('app', 'Uuid'),
			'createdUser' => null,
			'disableUser' => null,
			'image' => Yii::t('app', 'Image'),
            'file' => Yii::t('app', 'File'),
			'language' => Yii::t('app', 'Language'),
			'translatedContent' => null,
			'webContents' => null,
			'updatedUser' => null,
		];
	}


    /**
     * Event "beforeValidate"
     *
     * This method is invoked before validation starts
     *
     * @return bool
     */
    public function beforeValidate()
    {
        // Create new WebContent model
        switch ( $this->scenario )
        {
            // Create new WebContent
            case 'insert':
                // Get last weight
                if ( $this->content_type != 'block' )
                {
                    $criteria = new DbCriteria();
                    $criteria->compare('content_type', $this->content_type);
                    $criteria->compare('language_id', $this->language_id);
                    $criteria->order = 'weight DESC';
                    $criteria->limit = 1;
                    $last_web_content_model = WebContent::model()->find($criteria);
                    if ( $last_web_content_model )
                    {
                        $this->weight = $last_web_content_model->weight + 1;
                    }
                }
            break;
        }

        // Scheduled content?
        if ( $this->scenario === 'insert' || $this->scenario === 'update' )
        {
            $this->is_scheduled = 1;
            if ( empty($this->start_date) && empty($this->expiration_date) )
            {
                $this->is_scheduled = 0;
            }
        }

        return parent::beforeValidate();
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.content_id', $this->content_id);
        $criteria->compare('t.content_type', $this->content_type);
        $criteria->compare('t.internal_name', $this->internal_name);
        $criteria->compare('t.body', $this->body, true);
        $criteria->addCondition('t.translated_content_id IS NULL');

        // $criteria->compare('t.title', $this->title, true);
        // $criteria->compare('t.subtitle', $this->subtitle, true);
        if ( $this->title )
        {
            $criteria->addCondition('t.title LIKE :title OR t.subtitle LIKE :title');
            $criteria->addParams([':title' => '%'. $this->title .'%']);
        }

        // Search by file
        if ( $this->file_id )
        {
            $criteria->with['file'] = 'file';
            $criteria->compare('file.file_name', $this->file_id, 'compare');
        }

        // $criteria->compare('t.weight', $this->weight);
        // $criteria->compare('t.link_title', $this->link_title, true);
        // $criteria->compare('t.link_url', $this->link_url, true);
        // $criteria->compare('t.is_scheduled', $this->is_scheduled);
        // $criteria->compare('t.start_date', $this->start_date);
        // $criteria->compare('t.expiration_date', $this->expiration_date);
        // $criteria->compare('t.disable_date', $this->disable_date);
        // $criteria->compare('t.created_date', $this->created_date);
        // $criteria->compare('t.updated_date', $this->updated_date);
        // $criteria->compare('t.uuid', $this->uuid);

        // Filter just DISABLED models
        if ( $this->disable_filter == 1 )
        {
            $criteria->addCondition('t.disable_date IS NOT NULL AND t.disable_date > 0');
        }

        // Filter just ENABLED models
        else if ( $this->disable_filter == 2 )
        {
            $criteria->addCondition('t.disable_date IS NULL OR t.disable_date = 0');
        }

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['weight' => false]]
        ]);
    }


    /**
     * WebContent models list
     * 
     * @return array
     */
     public function webcontent_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['content_id', 'content_type'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = WebContent::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('content_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Disables WebContent model
     */
    public function disable()
    {
        if ( parent::disable() )
        {
            if ( empty($this->translated_content_id) && $this->translations )
            {
                foreach ( $this->translations as $translated_content_model )
                {
                    $translated_content_model->disable();
                }
            }

            return true;
        }

        return false;
    }


    /**
     * Enables WebContent model
     */
    public function enable()
    {
        if ( parent::enable() )
        {
            if ( empty($this->translated_content_id) && $this->translations )
            {
                foreach ( $this->translations as $translated_content_model )
                {
                    $translated_content_model->enable();
                }
            }

            return true;
        }

        return false;
    }


    /**
     * DbCriteria (query criteria) for results showed in a <SELECT> list
     *
     * Show just enabled web content models
     *
     * @return DbCriteria
     */
    public function enabledCriteria($vec_params = [])
    {
        $criteria = new DbCriteria;
        $criteria->addCondition('disable_date IS NULL OR disable_date = "0"');
        $criteria->compare('content_type', $this->content_type);
        $criteria->addCondition('translated_content_id IS NULL');
        if ( !empty($vec_params) )
        {
            $criteria->addCondition('t.content_id IN ('. implode(",", $vec_params) .')');
        }
        $criteria->order = 'weight ASC';
        return $criteria;
    }


    /**
     * Deletes the model
     */
    public function delete()
    {
        // STEP 1 - Image
        if ( $this->image )
        {
            $this->image->delete();
        }

        // STEP 2 - File
        if ( $this->file )
        {
            $this->file->delete();
        }

        // STEP 3 - TRANSLATIONS
        if ( empty($this->translated_content_id) && $this->translations )
        {
            foreach ( $this->translations as $translated_content_model )
            {
                $translated_content_model->delete();
            }
        }

        // STEP 4 - SEO
        if ( $this->seo )
        {
            foreach ( $this->seo as $seo_model )
            {
                $seo_model->delete();
            }
        }
        
        // Going on with "delete" action
        return parent::delete();
    }


    /**
     * Find a WebContent model by internal name
     */
    public function findByName($internal_name, $language_id = null)
    {
        // Default language
        if ( $language_id === null )
        {
            $language_id = Yii::defaultLanguage();
        }

        return self::get()
            ->where([
                'internal_name' => $internal_name,
                'language_id'   => $language_id
            ])
            ->one();
    }


    /**
     * Get SEO model given a language
     */
    public function get_seo($language_id = null)
    {
        // Default language
        if ( $language_id === null )
        {
            $language_id = Yii::defaultLanguage();
        }

        return Seo::get()
            ->where([
                'entity_id'     => $this->content_id,
                'entity_type'   => 'web_content',
                'language_id'   => $language_id
            ])
            ->one();
    }


    /**
     * Save SEO model
     */
    public function save_seo($seo_model)
    {
        if ( empty($seo_model->entity_id) || $seo_model->entity_id == $this->content_id )
        {
            $seo_model->entity_id = $this->content_id;
            $seo_model->entity_type = 'web_content';

            if ( ! $seo_model->save() )
            {
                Log::save_model_error($seo_model);
                return false;
            }

            // Save "seo_url" cached attribute
            /*
            if ( !empty($seo_model->url_manual) )
            {
                // Non multi-language
                if ( ! $this->is_multilanguage() )
                {
                    $this->seo_url = $seo_model->url_manual;   
                }
                else
                {
                    // Default language
                    if ( $seo_model->language_id == Yii::defaultLanguage() )
                    {
                        $this->seo_url = $seo_model->url_manual;
                    }

                    $translated_model = $this->get_translation($seo_model->language_id);
                    if ( $translated_model )
                    {
                        $translated_model->seo_url = $seo_model->url_manual;
                        $translated_model->saveAttributes([
                            'seo_url' => $seo_model->url_manual
                        ]);
                    }
                }
            }
            */

            return true;
        }

        return false;
    }


    /**
     * Check if this WebContent type has multilanguage option enabled
     */
    public function is_multilanguage()
    {
        $vec_config = $this->get_config($this->content_type);
        return ( !empty($vec_config) && isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] );
    }


    /**
     * Get the translated WebContent model given a language
     */
    public function get_translation($language_id)
    {
        return WebContent::get()
            ->where([
                'translated_content_id' => $this->content_id,
                'language_id'           => $language_id
            ])
            ->one();
    }


    /**
     * Save translation
     */
    public function save_translation($vec_attributes, $language_id)
    {
        $vec_attributes['language_id'] = $language_id;
        $translated_model = $this->get_translation($language_id);

        if ( ! $translated_model )
        {
            $translated_model = Yii::createObject(WebContent::class);
            $translated_model->translated_content_id = $this->content_id;
        }

        $translated_model->setAttributes($vec_attributes);
        return $translated_model->save();
    }


    /**
     * Save a translation model
     */
    public function save_translation_model($translated_content_model)
    {
        if ( empty($translated_content_model->translated_content_id) || $translated_content_model->translated_content_id == $this->content_id )
        {
            $translated_content_model->translated_content_id = $this->content_id;

            if ( ! $translated_content_model->save() )
            {
                Log::save_model_error($translated_content_model);
                return false;
            }

            return true;
        }

        return false;
    }


    /**
     * Returns the SEO URL for this model
     * 
     * @see \dz\helper\Url::to() method
     */
    public function url($vec_params=[], $ampersand='&')
    {
        // Default route (NO SEO way)
        $route = 'content/'. $this->content_id;

        // Current language
        $language_id = Yii::currentLanguage();
        $is_multilanguage = $this->is_multilanguage();

        if ( ! $is_multilanguage )
        {
            if ( !empty($this->seo) )
            {
                $route = $this->seo[0]->url_manual;
            }
        }
        else if ( $is_multilanguage )
        {
            
            $seo_model = $this->get_seo($language_id);
            if ( $seo_model )
            {
                $route = $seo_model->url_manual;
            }
        }

        return Url::to($route, $vec_params, '', $ampersand);
    }


    /**
     * Get SEO URL generated automatically
     */
    public function get_seo_url_auto($language_id = '')
    {
        if ( !empty($language_id) && $language_id != Yii::defaultLanguage() )
        {
            $translated_model = $this->get_translation($language_id);
            if ( $translated_model && !empty($translated_model->title) )
            {
                return Transliteration::url($translated_model->title);
            }
        }
        
        return Transliteration::url($this->title);
    }


    /**
     * Generate SEO data
     */
    public function generate_seo($language_id)
    {
        $translated_model = $this->get_translation($language_id);
        return [
            'url_auto'      => $this->get_seo_url_auto($language_id), 
            'meta_title'    => ($translated_model === null) ? $this->title : $translated_model->title,
        ];
    }


    /**
     * SEO - Return a meta tags array for a WebContent page
     */
    public function get_seo_metatags()
    {
        $vec_meta_tags = [
            'title'         => $this->title(),
            'description'   => $this->subtitle,
            'image'         => $this->image,
            'type'          => 'article'
        ];

        return $vec_meta_tags;
    }


    /**
     * Add an AssetImage model to this web content
     */
    public function add_image($image_model, $num_file = 0)
    {
        // Get destination path
        $content_images_path = $this->get_image_path();

        // Try to save image file
        if ( ! $this->upload_image('image_id', $content_images_path, $num_file) )
        {
            Yii::app()->controller->showErrors($this->get_file_errors());
        }

        // Check if image has been deleted
        $file_model = $this->get_file_model();
        if ( ! $file_model || empty($this->image_id) )
        {
            $image_model = AssetImage::get()->where(['file_id' => $this->image_id])->one();
            if ( ! $image_model )
            {
                $this->image_id = null;
            }
        }

        return true;
    }


    /**
     * Add an AssetFile model to this web content
     */
    public function add_file($file_model, $num_file = 1)
    {
        // Get destination path
        $content_files_path = $this->get_file_path();

        // Try to save the file
        if ( ! $this->upload_file('file_id', $content_files_path, $num_file) )
        {
            Yii::app()->controller->showErrors($this->get_file_errors());
        }

        // Check if the file has been deleted
        $file_model = $this->get_file_model();
        if ( ! $file_model || empty($this->file_id) )
        {
            $file_model = AssetImage::get()->where(['file_id' => $this->file_id])->one();
            if ( ! $file_model )
            {
                $this->file_id = null;
            }
        }

        return true;
    }


    /**
     * Return relative path where content images are stored
     */
    public function get_image_path($is_check_exists = false)
    {
        // Get destination path
        $image_model = Yii::createObject(AssetImage::class);
        $content_images_path = $image_model->getImagesPath() . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR;
        if ( $this->is_config('image_id', 'images_path') )
        {
            $content_images_path = $this->get_config('image_id', 'images_path');
        }

        // Check if directory exists, unless create it
        if ( $is_check_exists )
        {
            $content_images_dir = Yii::app()->file->set($content_images_path);
            if ( ! $content_images_dir->isDir )
            {
                $default_permissions = AssetImage::model()->get_default_permissions();
                $content_images_dir->createDir($default_permissions, $content_images_path);
            }
        }

        return $content_images_path;
    }


    /**
     * Return relative path where content files are stored
     */
    public function get_file_path($is_check_exists = false)
    {
        // Get destination path
        $file_model = Yii::createObject(AssetFile::class);
        $content_files_path = $file_model->getFilesPath() . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR;
        if ( $this->is_config('file_id', 'files_path') )
        {
            $content_files_path = $this->get_config('file_id', 'files_path');
        }

        // Check if directory exists, unless create it
        if ( $is_check_exists )
        {
            $content_files_dir = Yii::app()->file->set($content_files_path);
            if ( ! $content_files_dir->isDir )
            {
                $default_permissions = AssetFile::model()->get_default_permissions();
                $content_files_dir->createDir($default_permissions, $content_files_path);
            }
        }

        return $content_files_path;
    }


    /**
     * Load and returns schema configuration of web content
     * 
     * IMPORTANT: NOT applied for content_type = "block"
     */
    public function load_config($content_type = null)
    {
        // Current content type
        if ( $content_type == null )
        {
            $content_type = $this->content_type;
        }

        // Load configuration for the first time
        if ( empty($this->vec_config) )
        {
            // Full configuration options
            if ( $content_type == 'all' )
            {
                $this->vec_config = Yii::app()->config->get('components.web_content');
            }

            // Configuration of a specific content type
            else
            {
                $vec_component_config = Yii::app()->config->get('components.web_content');
                if ( !empty($vec_component_config) && isset($vec_component_config[$content_type]) )
                {
                    $this->vec_config = $vec_component_config[$content_type];
                }
            }
        }

        // Default values
        if ( empty($this->vec_config) )
        {
            $this->vec_config = [
                'is_editable'           => true,
                'is_multilanguage'      => true,
                'is_seo_fields'         => true,
                'is_sortable'           => true,
                'is_disable_allowed'    => true,
                'is_delete_allowed'     => true,

                // Specific attributes
                'attributes' => [
                    'title' => [
                        'is_translatable'   => true
                    ],
                    'body' => [
                        'is_translatable'   => true,
                        'min_height'        => 100
                    ],
                ],
            ];
        }

        // Default view files path
        if ( ! isset($this->vec_config['views']) )
        {
            
            $this->vec_config['views'] = [
                'index'         => '//web/_base/index',
                'create'        => '//web/_base/create',    
                'update'        => '//web/_base/update',
                '_grid_column'  => '//web/_base/_grid_column',
                '_form'         => '//web/_base/_form',
                '_form_seo'     => '//web/_base/_form_seo',
                '_tree'         => '//web/_base/_tree',
            ];
        }

        // Default texts
        if ( ! isset($this->vec_config['texts']) )
        {
            $this->vec_config['texts'] = [
                'entity_label'      => 'Content',
                'entities_label'    => 'Contents',

                'index_title'       => 'Manage content',
                'add_button'        => 'Add content',
                'create_title'      => 'Create content',
                'panel_title'       => 'Sort content',
                'list_title'        => 'Content list',

                // Success messages
                'created_success'   => 'New content created successfully',
                'updated_success'   => 'Content updated successfully',

                // Disable
                'disable_success'   => 'Content DISABLED successfully',
                'disable_error'     => 'Content could not be DISABLED',
                'disable_confirm'   => '<h3>Are you sure you want to <span class=\'text-danger\'>DISABLE</span> this content?</h3>',

                // Enable
                'enable_success'    => 'Content ENABLED successfully',
                'enable_error'      => 'Content could not be ENABLED',
                'enable_confirm'    => '<h3>Are you sure you want to <span class=\'text-success\'>ENABLE</span> this content?</h3>',

                // Delete
                'delete_success'    => 'Content DELETED successfully',
                'delete_error'      => 'Content could not be DELETED',
                'delete_confirm'    => '<h3><i class="icon wb-alert-circle text-danger"></i> Are you sure you want to <span class=\'text-danger\'>DELETE</span> this content?</h3><p><strong>WARNING:</strong> All the content data will be permanently removed. Consider disabling it.</p>',

                // Other
                'empty_text'        => 'No contents found',
            ];
        }

        // Return configuration options
        return $this->vec_config;
    }


    /**
     * Returns the view file path
     */
    public function get_view_path($view_name)
    {
        // Default view paths
        $vec_views = [
            'index'         => '//web/_base/index',
            'create'        => '//web/_base/create',    
            'update'        => '//web/_base/update',
            '_grid_column'  => '//web/_base/_grid_column',
            '_form'         => '//web/_base/_form',
            '_form_seo'     => '//web/_base/_form_seo',
            '_tree'         => '//web/_base/_tree',
        ];

        // Return view path from configuration file
        if ( !empty($this->content_type) )
        {
            $vec_config = $this->load_config($this->content_type);
            if ( !empty($vec_config) && isset($vec_config['views']) && isset($vec_config['views'][$view_name]) )
            {
                return $vec_config['views'][$view_name];
            }
        }

        // Return view path from default values
        if ( isset($vec_views[$view_name]) )
        {
            return $vec_views[$view_name];
        }

        // No view has been found
        return $view_name;
    }


    /**
     * Return the corresponding text
     */
    public function text($text_key, $content_type = null)
    {
        // Current content type
        if ( $content_type === null )
        {
            $content_type = $this->content_type;
        }

        // Load configuration and return text
        $vec_config = $this->load_config($content_type);
        if ( !empty($vec_config) && isset($vec_config['texts']) && isset($vec_config['texts'][$text_key]) )
        {
            return Yii::t('app', $vec_config['texts'][$text_key]);
        }

        return '';
    }


    /**
     * Set a configuration
     */
    public function set_config($vec_config)
    {
        $this->vec_config = $vec_config;

        // Load default values
        if ( isset($this->vec_config['attributes']) )
        {
            foreach ( $this->vec_config['attributes'] as $config_item => $vec_config_options )
            {
                if ( $this->is_config($config_item, 'default_value') && $this->hasAttribute($config_item) )
                {
                    $attribute_value = trim($this->getAttribute($config_item));
                    if ( empty($attribute_value) )
                    {
                        $this->setAttribute($config_item, $this->get_config($config_item, 'default_value'));
                    }
                }
            }
        }
    }


    /**
     * Get a configuration option
     */
    public function get_config($config_item = '', $config_option = '')
    {
        if ( !empty($this->vec_config) )
        {
            if ( empty($config_item) )
            {
                return $this->vec_config;
            }

            // Global config item?
            if ( isset($this->vec_config[$config_item]) )
            {
                return $this->vec_config[$config_item];
            }

            // Attribute?
            if ( isset($this->vec_config['attributes'][$config_item]) )
            {
                // Attribute option?
                if ( !empty($config_option) )
                {
                    if ( isset($this->vec_config['attributes'][$config_item][$config_option]) )
                    {
                        return $this->vec_config['attributes'][$config_item][$config_option];
                    }

                    return null;
                }

                return $this->vec_config['attributes'][$config_item];
            }
        }

        return null;
    }


    /**
     * Check if exists a configuration option
     * 
     * IMPORTANT: Only used for content_type = "web_content"
     */
    public function is_config($config_item, $config_option = '')
    {
        if ( !empty($this->vec_config) )
        {
            // Global config item?
            if ( isset($this->vec_config[$config_item]) )
            {
                return true;
            }

            // Attribute item?
            if ( isset($this->vec_config['attributes'][$config_item]) )
            {
                // Attribute option?
                if ( !empty($config_option) )
                {
                    return isset($this->vec_config['attributes'][$config_item][$config_option]);
                }

                return true;
            }
        }

        return false;
    }


    /**
     * Get the label name by attribute
     */
    public function get_label($attribute)
    {
        if ( $this->is_config($attribute, 'label') )
        {
            return $this->get_config($attribute, 'label');
        }
        
        return $this->getAttributeLabel($attribute);
    }


    /**
     * Get the input ID for a model attribute.
     */
    public function resolve_id($attribute)
    {
        return Html::getIdByName($this->resolve_name($attribute));
    }


    /**
     * Get the input name for a model attribute.
     */
    public function resolve_name($attribute)
    {
        if ( $this->content_type == 'block' )
        {
            return 'WebBlock['. $this->internal_name .']['. $attribute .']';
        }

        return 'WebContent['. $attribute .']';
    }



    /**
     * Validate start_date and expiration_date fields
     */
    public function validate_dates($attribute, $params)
    {
        if ( !empty($this->start_date) && !empty($this->expiration_date) )
        {
            $raw_start_date = $this->start_date;
            if ( preg_match("/\//", $raw_start_date) )
            {
                $raw_start_date = DateHelper::date_format_to_unix($raw_start_date);
            }

            $raw_expiration_date = $this->expiration_date;
            if ( preg_match("/\//", $raw_expiration_date) )
            {
                $raw_expiration_date = DateHelper::date_format_to_unix($raw_expiration_date);
            }

            if ( $raw_start_date > $raw_expiration_date )
            {
                $this->addError('start_date', 'Available dates - The expiration date cannot be before the start date');
            }
        }
    }


    /**
     * Return expiration label ready to show on a view template 
     */
    public function expiration_label()
    {
        // Add one day to expiration_date because content must be available until 23:59 hours
        $raw_expiration_date = (int)$this->raw_attributes['expiration_date'] + (86400 - 1);
        $now = time();
        if ( !empty($this->expiration_date) && $raw_expiration_date > $now )
        {
            $range_to_days = DateHelper::range_to_days($now, $raw_expiration_date, false);

            // Less than 1 day
            if ( $range_to_days <= 1 )
            {
                $total_hours = DateHelper::range_to_hours($now, $raw_expiration_date);
                return $total_hours > 1 ? $total_hours .' '. Yii::t('app', 'hours') : '1 '. Yii::t('app', 'hour');
            }
            else
            {
                $total_days = floor($range_to_days);
                return $total_days > 1 ? $total_days .' '. Yii::t('app', 'days') : '1 '. Yii::t('app', 'day');
            }
        }

        return '';
    }


    /**
     * Automatic string representation for a model (from GIIX)
     */
    public static function representingColumn()
    {
        return 'title';
    }


    /**
     * Title used for this model
     */
    public function title()
    {
        return $this->title;
    }
}
