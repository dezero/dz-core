<?php
/**
 * @package dz\modules\web\models 
 */

namespace dz\modules\web\models;

use dz\components\Mailer;
use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\settings\models\Language;
use dz\modules\web\models\_base\WebContact as BaseWebContact;
use user\models\User;
use Yii;

/**
 * WebContact model class for "web_contact" database table
 *
 * Columns in table "web_contact" available as properties of the model,
 * followed by relations of table "web_contact" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $contact_id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $message
 * @property string $language_id
 * @property integer $created_date
 * @property integer $created_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $language
 */
class WebContact extends BaseWebContact
{
    /**
     * @var SPAM blocked?
     */
    public $is_spam_blocked = false;


    /**
     * Send mail?
     */
    public $is_sending_mail = true;


    /**
     * @var Mail template alias
     */
    public $mail_template_alias = 'contact_message';


	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['name, message, created_date, created_uid', 'required'],
			['created_date, created_uid', 'numerical', 'integerOnly' => true],
			['name', 'length', 'max'=> 255],
			['email', 'length', 'max'=> 64],
			['phone', 'length', 'max'=> 32],
			['language_id', 'length', 'max'=> 4],
            ['uuid', 'length', 'max'=> 36],
			['email, phone, language_id', 'default', 'setOnEmpty' => true, 'value' => null],
			['contact_id, name, email, phone, message, language_id, created_date, created_uid', 'safe', 'on' => 'search'],

            // Custom validations
            ['email', 'check_spammer'],
            ['email', 'email']
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'language' => [self::BELONGS_TO, Language::class, 'language_id'],
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],

            // Custom relations
		];
	}

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'contact_id' => Yii::t('app', 'Contact'),
            'name' => Yii::t('app', 'Nombre'),
            'email' => Yii::t('app', 'Correo electrónico'),
            'phone' => Yii::t('app', 'Teléfono'),
            'message' => Yii::t('app', 'Mensaje'),
			'language_id' => Yii::t('app', 'Idioma'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
            'uuid' => Yii::t('app', 'Uuid'),
			'createdUser' => null,
			'language' => null,
		];
	}


    /**
     * Event "afterSave"
     *
     * This method is invoked after saving a record successfully
     */
    public function afterSave()
    {
        if ( $this->is_sending_mail )
        {
            $sending_result = Yii::app()->mail
                ->addModel($this, 'contact')
                ->setLanguage($this->language_id)
                ->compose($this->mail_template_alias)
                ->send();

            // Save error into LOG
            if ( ! $sending_result )
            {
                Yii::app()->mail->logError();
            }
        }

        return parent::afterSave();

        /*
        $mail_content  = 'Nuevo mensaje recibido desde el formulario de contacto de '. Yii::app()->params['baseUrl'] .':<br><ul>';
        $mail_content .= '<li>Fecha: '. date('d/m/Y - H:i') .'</li>';
        $mail_content .= '<li>Nombre: '. $this->name .'</li>';
        $mail_content .= '<li>Mail: '. $this->email .'</li>';
        $mail_content .= '<li>Teléfono: '. $this->phone .'</li>';
        $mail_content .= '</ul><br>Mensaje enviado:';
        $mail_content .= '<blockquote><p>'. $this->message .'</p></blockquote>';
        $mail_content .= '<br><hr><br>-- Este es un mensaje enviado automáticamente desde la web de '. Yii::app()->name;

        $mailer = new Mailer();
        $mailer->setTo(Yii::app()->params['mail']['contact']);
        $mailer->setSubject('WEB - Nuevo mensaje recibido desde formulario de contacto');
        $mailer->setView('blank');
        $mailer->setData(['content' => $mail_content]);
        
        // Sending email
        $mail_sending_result = $mailer->Send();
        if ( !$mail_sending_result )
        {
            Yii::log("Error en el envío de ". $mailer->send_type ." a ". CJSON::encode($mailer->getToAddress())." - [Fichero: '". $mailer->filename ."'] - ERROR: ". CJSON::encode($mailer->ErrorInfo), 'mailer', 'profile');
        }
        */
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.contact_id', $this->contact_id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.phone', $this->phone, true);
        $criteria->compare('t.message', $this->message, true);
        // $criteria->compare('t.created_date', $this->created_date);
        // $criteria->compare('t.uuid', $this->uuid);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['contact_id' => true]]
        ]);
    }


    /**
     * WebContact models list
     * 
     * @return array
     */
     public function webcontact_list()
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['contact_id', 'name'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = WebContact::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('contact_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Validate if email address belongs to the SPAMMER list
     */
    public function check_spammer($attribute, $params)
    {
        $vec_spammer_list = Yii::app()->mail->get_spammer_list();
        if ( !empty($vec_spammer_list) )
        {
            $vec_email = explode('@', $this->email);
            if ( !empty($vec_email) && count($vec_email) == 2 )
            {
                $domain_email = $vec_email[1];
                foreach ( $vec_spammer_list as $domain_spammed )
                {
                    $domain_spammed = str_replace(".", "\.", $domain_spammed);
                    if ( preg_match("/". $domain_spammed ."/", $domain_email) )
                    {
                        $this->is_spam_blocked = true;
                        $this->addError('email', 'This mail domain is blocked');
                    }
                }
            }
        }
    }


    /**
     * Returns the mail tokens
     */
    public function mailTokens()
    {
        return [
            '{contact.date}'        => is_numeric($this->created_date) ? DateHelper::unix_to_date($this->created_date) : $this->created_date,
            '{contact.name}'        => $this->name,
            '{contact.email}'       => $this->email,
            '{contact.mail}'        => $this->email,
            '{contact.phone}'       => $this->phone,
            '{contact.message}'     => $this->message,
            '{contact.language}'    => Yii::app()->i18n->language_name($this->language_id),
        ];
    }


    /**
     * Help text for the mail tokens
     */
    public function mailHelp()
    {
        return [
            '{contact.date}'        => 'Contact submission date',
            '{contact.name}'        => 'Contact name',
            '{contact.email}'       => 'Contact email address',
            '{contact.phone}'       => 'Contact phone number',
            '{contact.message}'     => 'Contact message',
            '{contact.language}'    => 'Contact selected language',
        ];
    }
}
