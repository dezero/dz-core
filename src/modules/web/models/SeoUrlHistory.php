<?php
/**
 * @package dz\modules\web\models 
 */

namespace dz\modules\web\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dz\modules\settings\models\Language;
use dz\modules\web\models\_base\SeoUrlHistory as BaseSeoUrlHistory;
use user\models\User;
use Yii;

/**
 * SeoUrlHistory model class for "web_seo_url_history" database table
 *
 * Columns in table "web_seo_url_history" available as properties of the model,
 * followed by relations of table "web_seo_url_history" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $url_history_id
 * @property integer $entity_id
 * @property string $entity_type
 * @property string $language_id
 * @property string $url
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $language
 * @property mixed $updatedUser
 */
class SeoUrlHistory extends BaseSeoUrlHistory
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['entity_id, entity_type, url, created_date, created_uid, updated_date, updated_uid', 'required'],
			['entity_id, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['entity_type', 'length', 'max'=> 32],
			['language_id', 'length', 'max'=> 4],
			['url', 'length', 'max'=> 255],
            ['uuid', 'length', 'max'=> 36],
			['language_id', 'default', 'setOnEmpty' => true, 'value' => null],
			['url_history_id, entity_id, entity_type, language_id, url, created_date, created_uid, updated_date, updated_uid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'language' => [self::BELONGS_TO, Language::class, 'language_id'],
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'url_history_id' => Yii::t('app', 'Url History'),
			'entity_id' => Yii::t('app', 'Entity'),
			'entity_type' => Yii::t('app', 'Entity Type'),
			'language_id' => null,
			'url' => Yii::t('app', 'Url'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
            'uuid' => Yii::t('app', 'Uuid'),
			'createdUser' => null,
			'language' => null,
			'updatedUser' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.url_history_id', $this->url_history_id);
        $criteria->compare('t.entity_id', $this->entity_id);
        $criteria->compare('t.entity_type', $this->entity_type);
        $criteria->compare('t.url', $this->url, true);

        // $criteria->compare('t.created_date', $this->created_date);
        // $criteria->compare('t.updated_date', $this->updated_date);
        // $criteria->compare('t.uuid', $this->uuid);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['url_history_id' => true]]
        ]);
    }
}