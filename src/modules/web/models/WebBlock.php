<?php
/**
 * @package dz\modules\web\models 
 */

namespace dz\modules\web\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Html;
use dz\helpers\StringHelper;
use dz\helpers\Transliteration;
use dz\modules\asset\models\AssetImage;
use dz\modules\web\models\WebContent;
use user\models\User;
use Yii;

class WebBlock extends WebContent
{
    /**
     * @string Content type
     */
    public $content_type = 'block';


    /**
     * Constructor
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     * Define relations with other objects
     *
     * There are four types of relations that may exist between two active record objects:
     *   - BELONGS_TO: e.g. a member belongs to a team;
     *   - HAS_ONE: e.g. a member has at most one profile;
     *   - HAS_MANY: e.g. a team has many members;
     *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
     */
    public function relations()
    {
        $vec_relations = parent::relations();

        // Redefine "WebContent" relations
        $vec_relations['translatedContent'] = [self::BELONGS_TO, WebBlock::class, 'translated_content_id'];
        $vec_relations['webContents'] = [self::HAS_MANY, WebBlock::class, 'translated_content_id'];
        $vec_relations['translations'] = [self::HAS_MANY, WebBlock::class, ['content_id' => 'translated_content_id']];

        return $vec_relations;
    }
    

    /**
     * This method is invoked after each record is instantiated by a find method
     *
     * @return void
     */ 
    public function afterFind()
    {
        parent::afterFind();
    }


    /*
    |--------------------------------------------------------------------------
    | CUSTOM FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * Get the input name for a model attribute.
     */
    public function resolve_name($attribute)
    {
        return 'WebBlock['. $this->internal_name .']['. $this->language_id .']['. $attribute .']';
    }


    /**
     * Get the translated WebBlock model given a language
     */
    public function get_translation($language_id)
    {
        return WebBlock::get()
            ->where([
                'translated_content_id' => $this->content_id,
                'language_id'           => $language_id
            ])
            ->one();
    }


    /**
     * Get Image model
     */
    public function get_image_model()
    {
        // Image
        $image_model = $this->image;
        if ( ! $image_model )
        {
            $image_model = Yii::createObject(AssetImage::class);
            $image_model->setAttributes([
                'entity_type'   => 'WebContent',
                'entity_id'     => !empty($this->content_id) ? $this->content_id : null
            ]);
        }

        return $image_model;
    }


    /**
     * Load and returns schema configuration of web block
     */
    public function load_config($internal_name = '')
    {
        // Current content type
        if ( empty($internal_name) )
        {
            $internal_name = $this->internal_name;
        }

        // Load configuration for the first time
        if ( empty($this->vec_config) )
        {
            // Full configuration options
            if ( $internal_name == 'all' )
            {
                $this->vec_config = Yii::app()->config->get('components.web_block');
            }

            // Configuration of a specific content type
            else
            {
                $vec_component_config = Yii::app()->config->get('components.web_block');
                if ( !empty($vec_component_config) && isset($vec_component_config[$internal_name]) )
                {
                    $this->vec_config = $vec_component_config[$internal_name];
                }
            }
        }

        // Default values
        if ( empty($this->vec_config) )
        {
            $this->vec_config = [
                'block_title'   => 'Custom block',
                'language_id'   => Yii::app()->language,
                'attributes'    => [
                    'title' => [
                        'is_translatable' => true
                    ],
                    'body' => [
                        'label'             => 'Body text',
                        'is_translatable'   => true,
                        'min_height'        => 100
                    ],
                ]
            ];
        }

        // Return configuration options
        return $this->vec_config;
    }
}