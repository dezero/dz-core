<?php
/**
 * CurrencyBehavior class file
 *
 * Automatically convert currency (money) fields
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2013 Dezero
 */

namespace dz\behaviors;

use Yii;

class CurrencyBehavior extends \CActiveRecordBehavior
{
	/**
	 * Columns with currency format
	 * 
	 * array('column_name')
	 *
	 * @var array
	 */
	public $columns;
	
	
	/**
	 * Event "afterFind"
	 *
	 * This method is invoked after each record is instantiated by a find method
	 * @param CEvent $event the event parameter
	 */
    public function afterFind($event)
    {
		if ( $this->columns !== null )
		{
			foreach ( $this->columns as $que_column )
			{
				// Formats the value as a number
        		$event->sender->$que_column = Yii::app()->number->formatNumber($event->sender->$que_column);
			}
		}
    }


	/**
	 * Event "beforeValidate"
	 *
	 * This method is invoked before validation starts
	 * @param CEvent $event the event parameter
	 */
	public function beforeValidate($event)
    {
		if ( $this->columns !== null )
		{
			foreach ( $this->columns as $que_column )
			{
				// Turns the given formatted number (string) into a float
        		$event->sender->$que_column = Yii::app()->number->unformatNumber($event->sender->$que_column);
			}
		}
	}
}
