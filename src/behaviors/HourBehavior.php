<?php
/**
 * HourBehavior class file
 *
 * Automatically convert hour fields. 
 * Transform hour fields from/to total minutes (in DB) from/to "hh:mm" format (in web)
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2013 Dezero
 */

namespace dz\behaviors;

use dz\helpers\DateHelper;
use Yii;

class HourBehavior extends \CActiveRecordBehavior
{
	/**
	 * Columns with hour format
	 * 
	 * array('column_name')
	 *
	 * @var array
	 */
	public $columns;
	
	
	/**
	 * Event "afterFind"
	 *
	 * This method is invoked after each record is instantiated by a find method
	 * @param CEvent $event the event parameter
	 */
    public function afterFind($event)
    {
		if ( $this->columns !== NULL )
		{
			foreach ( $this->columns as $que_column )
			{
				// Transform hour fields from total minutes (integer int DB) to "hh:mm" format
				$hour_value = $event->sender->$que_column;
				$event->sender->$que_column = DateHelper::hour_format($hour_value);
			}
		}
    }


	/**
	 * Event "beforeValidate"
	 *
	 * This method is invoked before validation starts
	 * @param CEvent $event the event parameter
	 */
	public function beforeValidate($event)
    {
		if ( $this->columns !== NULL )
		{
			foreach ( $this->columns as $que_column )
			{
				$event->sender->$que_column = DateHelper::hour_to_minutes($event->sender->$que_column);
			}
		}
	}
}
