<?php
/**
 * DateBehavior class file
 *
 * Automatically convert date and datetime fields
 * Based on http://www.yiiframework.com/extension/edatetimebehavior/
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2013 Dezero
 */

namespace dz\behaviors;

use Yii;

class DateBehavior extends \CActiveRecordBehavior
{
	/**
	 * Output date format for a specific column
	 * 
	 * array('column_name' => 'date_format')
	 *
	 * @var array
	 */
	public $columns;
	
	
	/**
	 * Default date format
	 *
	 * @var string
	 */
    // private $mySQL_date_format = 'Y-m-d';
    // private $mySQL_datetime_format = 'Y-m-d H:i:s';
	public $default_date_format = 'd/m/Y - H:i';
	
	/**
	 * Event "afterFind"
	 *
	 * This method is invoked after each record is instantiated by a find method
	 * @param CEvent $event the event parameter
	 */
    public function afterFind($event)
    {
		// Transform date fields from UNIX time to dd/mm/YYYY 
        foreach( $event->sender->tableSchema->columns as $columnName => $column )
		{
			$column_type = str_replace(" UNSIGNED", "", strtoupper($column->dbType) );
			// if ($column_type == 'DATE' || $column_type == 'DATETIME' || ($column_type == 'INT(10)' AND preg_match("/_date$/", $column->name) ) )
			if ($column_type == 'DATE' || ($column_type == 'INT(10)' AND preg_match("/_date$/", $column->name) ) )
			{
	            if ( !strlen($event->sender->$columnName) )
				{
	                $event->sender->$columnName = null;
	                continue;
	            }
				
				$date_format = $this->default_date_format;
				if ( $this->columns !== NULL AND isset($this->columns[$columnName]) )
				{
					// Excluded column
					if ( $this->columns[$columnName] === false)
					{
						continue;
					}
					
					// Date format
					$date_format = $this->columns[$columnName];
				}
				if ( !empty($event->sender->$columnName) && is_numeric($event->sender->$columnName) )
				{
					$event->sender->$columnName = date($date_format, $event->sender->$columnName);
				}
			}
        }
    }


	/**
	 * Event "beforeValidate"
	 *
	 * This method is invoked before validation starts
	 * @param CEvent $event the event parameter
	 */
	public function beforeValidate($event)
    {
		$is_created_date = false;
		$is_updated_date = false;
		$is_created_uid = false;
		$is_updated_uid = false;
		
		// Transform date fields from dd/mm/YYYY to UNIX time
        foreach( $event->sender->tableSchema->columns as $columnName => $column )
		{
			$column_type = str_replace(" UNSIGNED", "", strtoupper($column->dbType) );
			// if ($column_type == 'DATE' || $column_type == 'DATETIME' || ($column_type == 'INT(10)' AND preg_match("/_date$/", $column->name) ) )
			if ($column_type == 'DATE' || ($column_type == 'INT(10)' AND preg_match("/_date$/", $column->name) ) )
			{
				if ( !empty($event->sender->$columnName) )
				{
					// Excluded column
					if ( $this->columns !== NULL AND isset($this->columns[$columnName]) AND $this->columns[$columnName] === false )
					{
						continue;
					}

					// $que_date = substr($que_date,6,2) .'-'. substr($que_date,4,2) .'-'. substr($que_date,0,4).' '.substr($que_date,8,2) .':'. substr($que_date,10,2);
					if ( preg_match("/\//", $event->sender->$columnName) )
					{
						$vec_date = explode("/", $event->sender->$columnName);
						$que_date = $vec_date[0] .'-'. $vec_date[1] .'-';
						if ( ! preg_match ("/\:/", $vec_date[2]) )
						{
							$que_date .= $vec_date[2] .' 00:00';
						}
						else
						{
							$que_date .= str_replace(" -", "", $vec_date[2]);
						}
						$event->sender->$columnName = strtotime(date($que_date));
						// s(array($columnName, $event->sender->$columnName));
					}
				}
			}
			
			// Check if "created_XX" AND "updated_XX" columns exists in this table
			switch ( $columnName )
			{
				case 'created_date':
					$is_created_date = true;
				break;
				
				case 'updated_date':
					$is_updated_date = true;
				break;
				
				case 'created_uid':
					$is_created_uid = true;
				break;
				
				case 'updated_uid':
					$is_updated_uid = true;
				break;
			}
        }

		// Save created date & user
		if ( $event->sender->getIsNewRecord() AND $is_created_date )
		{
			$event->sender->created_date = time();
			if ( $is_created_uid )
			{
				$event->sender->created_uid = (Yii::app() instanceof \CConsoleApplication) ? 1 : Yii::app()->user->id;
			}
		}
		
		// Save updated date & user
		if ( $is_updated_date )
		{
			$event->sender->updated_date = time(); 
			if ( $is_updated_uid )
			{
				$event->sender->updated_uid = (Yii::app() instanceof \CConsoleApplication) ? 1 : Yii::app()->user->id;
			}
		}

		return parent::beforeValidate($event);
	}
	
	
	/**
	 * Event "beforeSave"
	 *
	 * This method is invoked before saving a record (after validation, if any)
	 * @param CEvent $event the event parameter
	 */
	/*
	public function beforeSave($event)
	{	
		// Save created date & user
		if ( $this->getOwner()->getIsNewRecord() AND isset($this->getOwner()->created_date) )
		{
			$this->getOwner()->created_date = time();
			$this->getOwner()->updated_date = time();
			if ( isset($this->getOwner()->created_uid) )
			{
				$this->getOwner()->created_uid = Yii::app()->user->id;
				$this->getOwner()->updated_uid = Yii::app()->user->id;
			}
		}
		
		// Save updated date & user
		else if ( isset($this->getOwner()->updated_date) )
		{
			$this->getOwner()->updated_date = time(); 
			if ( isset($this->getOwner()->updated_uid) )
			{
				$this->getOwner()->updated_uid = Yii::app()->user->id;
			}
		}
    }
	*/
}
