<?php
/**
 * LoggableBehavior class file
 *
 * Automatically logs all changes in database fields
 * Based on http://www.yiiframework.com/extension/audittrail/
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2016 Dezero
 */

namespace dz\behaviors;

use AuditTrail;
use dz\helpers\DateHelper;
use Yii;

class LoggableBehavior extends \CActiveRecordBehavior
{
	/**
	 * Allowed fields
	 */
	public $allowed = array();


	/**
	 * Ignored / excluded fields
	 */
	public $ignored = array();


	/**
	 * SQL condition
	 */
	public $condition = array();


	/**
	 * Old attributes
	 */
	private $_oldattributes = array();
	
	
	/**
	 * Parent model. Save CREATE, UPDATE and DELETE information to its parent
	 *
	 * Marked code as "// 25/10/2016 - SYNC AUDIT WITH PARENT MODEL"
	 */
	public $parent_model = array();

	/**
	 * Event "afterSave"
	 *
	 * This method is invoked after saving a record successfully.
	 * @param CEvent $event the event parameter
	 */
	public function afterSave($event)
	{
		try
		{
			$username = Yii::app()->user->Name;
			$userid = Yii::app()->user->id;
		}
		catch(Exception $e)
		{
			// If we have no user object, this must be a command line program
			$username = "NO_USER";
			$userid = NULL;
		}
		
		if ( empty($username) )
		{
			$username = "NO_USER";
		}
		
		if ( empty($userid) )
		{
			$userid = NULL;
		}
	
		$allowedFields = $this->allowed;
		$ignoredFields = $this->ignored;
		
		$newattributes = $this->Owner->getAttributes();
		$oldattributes = $this->getOldAttributes();
		
		// <DEZERO> Condition field
		if ( count($this->condition) > 0 )
		{
			foreach ( $this->condition as $que_field => $que_value )
			{
				foreach( $newattributes as $f => $v )
				{
					if ( $que_field == $f )
					{
						if ( is_array($que_value) AND !in_array($v, $que_value))
						{
							$newattributes = array();
							$oldattributes = array();
						}
						else if ( !is_array($que_value) AND $que_value != $v )
						{
							$newattributes = array();
							$oldattributes = array();
						}
					}
				}

				if ( !empty($oldattributes) )
				{
					foreach( $oldattributes as $f => $v )
					{
						if ( $que_field == $f )
						{
							if ( is_array($que_value) AND !in_array($v, $que_value))
							{
								$newattributes = array();
								$oldattributes = array();
							}
							else if ( !is_array($que_value) AND $que_value != $v )
							{
								$newattributes = array();
								$oldattributes = array();
							}
						}
					}
				}
			}
		}
		// </DEZERO> Condition field

		// Let's unset fields which are not allowed
		if ( sizeof($allowedFields) > 0 )
		{
			foreach ( $newattributes as $f => $v )
			{
				if ( array_search($f, $allowedFields) === FALSE)
				{
					unset($newattributes[$f]);
				}
			}

			foreach ( $oldattributes as $f => $v )
			{
				if ( array_search($f, $allowedFields) === FALSE )
				{
					unset($oldattributes[$f]);
				}
			}
		}

		// Let's unset fields which are ignored
		if ( sizeof($ignoredFields) > 0 )
		{
			foreach( $newattributes as $f => $v )
			{
				if ( array_search($f, $ignoredFields) !== FALSE )
				{
					unset($newattributes[$f]);
				}
			}

			foreach( $oldattributes as $f => $v )
			{
				if ( array_search($f, $ignoredFields) !== FALSE )
				{
					unset($oldattributes[$f]);
				}
			}
		}
		
		// If no difference then WHY?
		// There is some kind of problem here that means "0" and 1 do not diff for array_diff so beware: stackoverflow.com/questions/12004231/php-array-diff-weirdness :S
		if ( count(array_diff_assoc($newattributes, $oldattributes)) <= 0 )
		{
			return;
		}
		
		// UPDATE RECORD
		if ( !$this->Owner->isNewRecord )
		{
			// 25/10/2016 - SYNC AUDIT WITH PARENT MODEL
			$is_updated_parent = FALSE;

			// Compare old and new
			foreach ( $newattributes as $name => $value )
			{
				$old = '';
				if ( !empty($oldattributes) )
				{
					$old = $oldattributes[$name];
				}

				// <DEZERO> - Transform dates from dd/mm/YYYY - HH:ii to UNIX timestamp
				if ( preg_match("/\_date$/", $name) )
				{
					// Old value
					if ( preg_match("/\//", $old) )
					{
						$old = DateHelper::date_format_hour_to_unix($old);
					}

					// New value
					if ( preg_match("/\//", $value) )
					{
						$value = DateHelper::date_format_hour_to_unix($old);
					}
				}
				// <DEZERO> - Transform dates from dd/mm/YYYY - HH:ii to UNIX timestamp
				
				// Yii::app()->controller->log(print_r(array($name, $value, $old), TRUE));

				// Dezero - No difference between 0 and NULL values
				if ( (is_numeric($value) AND $value == 0 AND $old == NULL) OR ($value == NULL AND is_numeric($old) AND $old == 0) )
				{
					$value = $old = 0;
				}
				
				if ( $value != $old )
				{
					$log = new AuditTrail();
					$log->old_value = $old;
					$log->new_value = $value;
					$log->action = 'CHANGE';
					$log->model = get_class($this->Owner);
					$log->model_id = $this->Owner->getPrimaryKey();
					$log->field = $name;
					$log->stamp = date('Y-m-d H:i:s');
					$log->user_id = $userid;
					$log->save();

					// 25/10/2016 - SYNC AUDIT WITH PARENT MODEL
					if ( !empty($this->parent_model) AND !$is_updated_parent )
					{
						$this->_updateParent('CHANGE', $log->model_id);
						$is_updated_parent = TRUE;
					}
				}
			}
		}
		
		// CREATE NEW RECORD
		else
		{
			$log = new AuditTrail();
			$log->old_value = '';
			$log->new_value = '';
			$log->action = 'CREATE';
			$log->model = get_class($this->Owner);
			$log->model_id = $this->Owner->getPrimaryKey();
			$log->field = 'N/A';
			$log->stamp = date('Y-m-d H:i:s');
			$log->user_id = $userid;
			$log->save();

			// 25/10/2016 - SYNC AUDIT WITH PARENT MODEL
			if ( !empty($this->parent_model) )
			{
				$this->_updateParent('SET', $log->model_id);
			}
			
			foreach ( $newattributes as $name => $value )
			{
				$log = new AuditTrail();
				$log->old_value = '';
				$log->new_value = $value;
				$log->action ='SET';
				$log->model = get_class($this->Owner);
				$log->model_id = $this->Owner->getPrimaryKey();
				$log->field = $name;
				$log->stamp = date('Y-m-d H:i:s');
				$log->user_id =	$userid;
				$log->save();
			}
		}
		return parent::afterSave($event);
	}


	/**
	 * Event "afterDelete"
	 *
	 * This method is invoked after deleting a record.
	 * @param CEvent $event the event parameter
	 */
	public function afterDelete($event)
	{
		try
		{
			$username = Yii::app()->user->Name;
			$userid = Yii::app()->user->id;
		}
		catch(Exception $e)
		{
			$username = "NO_USER";
			$userid = NULL;
		}

		if ( empty($username) )
		{
			$username = "NO_USER";
		}
		
		if ( empty($userid) )
		{
			$userid = NULL;
		}
		
		$log = new AuditTrail();
		$log->old_value = '';
		$log->new_value = '';
		$log->action = 'DELETE';
		$log->model = get_class($this->Owner);
		$log->model_id = $this->Owner->getPrimaryKey();
		$log->field = 'N/A';
		$log->stamp = date('Y-m-d H:i:s');
		$log->user_id =	$userid;
		$log->save();

		// 25/10/2016 - SYNC AUDIT WITH PARENT MODEL
		if ( !empty($this->parent_model) )
		{
			$this->_updateParent('DELETE', $log->model_id);
		}

		return parent::afterDelete($event);
	}


	/**
	 * Event "afterFind"
	 *
	 * This method is invoked after each record is instantiated by a find method
	 * @param CEvent $event the event parameter
	 */
	public function afterFind($event)
	{
		// Save old values
		$this->setOldAttributes($this->Owner->getAttributes());
		
		return parent::afterFind($event);
	}


	/**
	 * Returns old attributes values
	 */
	public function getOldAttributes()
	{
		return $this->_oldattributes;
	}

	/**
	 * Save old attributes values
	 */
	public function setOldAttributes($value)
	{
		$this->_oldattributes = $value;
	}


	/**
	 * Update parent information
	 */
	private function _updateParent($action, $pk_value)
	{
		// 25/10/2016 - SYNC AUDIT WITH PARENT MODEL
		$relation_field = $this->parent_model['field'];
		$log = new AuditTrail();
		$log->setAttributes(array(
			'action'	=> $action,								// ACTION
			'model'		=> $this->parent_model['model'],		// Shop
			'model_id'	=> $this->Owner->{$relation_field},		// $model->shop_id
			'field'		=> get_class($this->Owner),				// Contact
			'old_value' => '',
			'new_value' => $pk_value,							// PK
			'stamp'		=> date('Y-m-d H:i:s'),
			'user_id'	=> Yii::app()->user->id
		));
		$log->save();
	}
}
?>