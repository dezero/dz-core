<?php
/**
 * MultipleValuesBehavior class file
 *
 * Automatically save and print a database record in different fields
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2013 Dezero
 */

namespace dz\behaviors;

class MultipleValuesBehavior extends \CActiveRecordBehavior
{
	/**
	 * Columns with hour format
	 * 
	 * array('column_name')
	 *
	 * @var array
	 */
	public $columns;
	
	
	/**
	 * Event "afterFind"
	 *
	 * This method is invoked after each record is instantiated by a find method
	 * @param CEvent $event the event parameter
	 */
    public function afterFind($event)
    {
		if ( $this->columns !== NULL )
		{
			foreach ( $this->columns as $que_column )
			{
				// $raw_column = $que_column .'_raw';
				// $event->sender->$raw_column = $event->sender->$que_column;
				if ( !empty($event->sender->$que_column) )
				{
					$event->sender->$que_column = explode(", ", $event->sender->$que_column);
				}
			}
		}
    }


	/**
	 * Event "beforeValidate"
	 *
	 * This method is invoked before validation starts
	 * @param CEvent $event the event parameter
	 */
	public function beforeValidate($event)
    {
		if ( $this->columns !== NULL )
		{
			foreach ( $this->columns as $que_column )
			{
				if ( !empty($event->sender->$que_column) AND is_array($event->sender->$que_column) )
		    	{
		    		$new_value = '';
		    		foreach ( $event->sender->$que_column as $que_value )
		    		{
		    			if ( !empty($que_value) )
		    			{
		    				if ( !empty($new_value) )
		    				{
		    					$new_value .= ', ';
		    				}
		    				$new_value .= $que_value;
		    			}
		    		}
			    	$event->sender->$que_column = $new_value;
		    	}
		    	else
		    	{
		    		$event->sender->$que_column = '';
		    	}
			}
		}
	}
}
