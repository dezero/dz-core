<?php
/**
 * JsonBehavior class file
 *
 * Automatically convert JSON fields to arrays
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2020 Dezero
 */

namespace dz\behaviors;

use dz\helpers\Json;

class JsonBehavior extends \CActiveRecordBehavior
{
    /**
     * Columns with JSON format
     * 
     * @var array
     */
    public $columns;
    
    
    /**
     * Event "afterFind"
     *
     * This method is invoked after each record is instantiated by a find method
     * @param CEvent $event the event parameter
     */
    public function afterFind($event)
    {
        if ( $this->columns !== null )
        {
            foreach ( $this->columns as $column_name )
            {
                if ( !empty($event->sender->$column_name) )
                {
                    $event->sender->$column_name = Json::decode($event->sender->$column_name);
                }
            }
        }
    }


    /**
     * Event "beforeValidate"
     *
     * This method is invoked before validation starts
     * @param CEvent $event the event parameter
     */
    public function beforeValidate($event)
    {
        if ( $this->columns !== null )
        {
            foreach ( $this->columns as $column_name )
            {
                if ( !empty($event->sender->$column_name) && is_array($event->sender->$column_name) )
                {
                    $event->sender->$column_name = Json::encode($event->sender->$column_name);
                }
                // else
                // {
                //     $event->sender->$column_name = null;
                // }
            }
        }
    }
}
