<?php
/**
 * UuidBehavior class file
 *
 * Automatically generate and save UUID values for column "uuid"
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2019 Dezero
 */

namespace dz\behaviors;

use dz\helpers\StringHelper;

class UuidBehavior extends \CActiveRecordBehavior
{
    /**
     * @var array.  Columns with UUID
     */
    public $columns = ['uuid'];


    /**
     * Event "beforeValidate"
     *
     * This method is invoked before validation starts
     * @param CEvent $event the event parameter
     */
    public function beforeValidate($event)
    {
        if ( $this->columns !== NULL )
        {
            foreach ( $this->columns as $column_name )
            {
                if ( isset($event->sender->tableSchema->columns[$column_name]) && empty($event->sender->$column_name) )
                {
                    $event->sender->$column_name = StringHelper::UUID();
                }
            }
        }
    }
}
