<?php

/**
 * Extension for `nicolab/php-ftp-client` package
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link https://github.com/Nicolab/php-ftp-client
 * @license http://www.opensource.org/licenses/bsd-license.php
 */

namespace dz\utils;

use Yii;

class FtpClient extends \FtpClient\FtpClient
{
	/**
	 * Regex filter pattern
	 */
	protected $filter_pattern = '';


	/**
	 * Set a filter pattern
	 */
	public function set_filter_pattern($pattern)
	{
		$this->filter_pattern = $pattern;
	}


	/**
     * Upload files
     * 
     * Based on "putAll" method
     * 
     * @param  string    $source_directory
     * @param  string    $target_directory
     * @param  int       $mode
     * @return FtpClient
     */
    public function syncAll($source_directory, $target_directory, $mode = FTP_BINARY)
    {
        // Put "root" as the current directory to continue working fine with FtpClient class
        $this->chdir("/");

        // If target_directory does not exist, create it!
        if (!$this->isDir($target_directory))
        {
            // create directories that do not yet exist
            $this->mkdir($target_directory);

            // Permissions 755 to this directory
            $this->chmod(0755, $target_directory);
        }

        // If target_directoy exists, delete all the current files
        else
        {
        	$this->chdir($target_directory);
			$vec_files = $this->nlist(".");
			if ( !empty($vec_files) )
			{
				foreach ($vec_files as $que_file)
				{
				    $this->delete($que_file);
				}
			}

			// Put "root" as the current directory to continue working fine with FtpClient class
			$this->chdir("/");
        }

        // Save uplodaded files
        $vec_uploaded_files = array();

        // Ensure source directory exists
        $source_dir = Yii::app()->file->set($source_directory);
        if ( $source_dir->getExists() )
        {
            $d = dir($source_directory);

            // do this for each file in the directory
            while ($file = $d->read())
            {
                // to prevent an infinite loop
                if ($file != "." && $file != "..")
                {
                    // do the following if it is a directory
                    if (is_dir($source_directory.'/'.$file))
                    {
                        if (!$this->isDir($target_directory.'/'.$file))
                        {
                            // create directories that do not yet exist
                            $this->mkdir($target_directory.'/'.$file);

                            // Permissions 755 to this directory
    	                    $this->chmod(0755, $target_directory.'/'.$file);
                        }

                        // recursive part
                        $this->putAll(
                            $source_directory .'/'. $file, $target_directory .'/'. $file,
                            $mode
                        );
                    }
                    else
                    {
                    	// Add filter pattern
                    	if ( empty($this->filter_pattern) OR preg_match("/". $this->filter_pattern ."/", $file) )
                    	{
    	                    // put the files
    	                    $this->put(
    	                        $target_directory .'/'. $file, $source_directory .'/'. $file,
    	                        $mode
    	                    );

                            $vec_uploaded_files[] = $target_directory .'/'. $file;
    	                }
                    }
                }
            }
        }

        // Finally changess permissions to all the files
        if ( !empty($vec_uploaded_files) )
        {
            foreach ( $vec_uploaded_files as $que_uploaded_file )
            {
                $this->chmod(0755, $que_uploaded_file);
            }
        }

        return $this;
    }
}