<?php
/**
 * Special override of CGridView for EExcelView
 */

use dz\batch\ExportExcel;
use dz\helpers\Log;

// Base class is EExcel
Yii::import('@lib.excel.EExcelView');

class ExcelView extends EExcelView
{
    /**
     * Excel sheets - DEZERO extension
     */
    public $sheets = [];


    /**
     * Custom AfterRender event
     */
    public $afterRender ='afterRender'; // '$this->afterRender';


    /**
     * Init callback
     */
    public function init()
    {
        parent::init();

        // Load all sheets information
        foreach ( $this->sheets as $num_sheet => $que_sheet )
        {
            // First sheet
            if ( $num_sheet == 0 )
            {
                $this->columns = $que_sheet['columns'];
                $this->initColumns();
                $this->dataProvider = $que_sheet['data'];
            }

            // Next sheet -> create new one!
            else
            {
                $this->objPHPExcel->createSheet();  
            }

            // Set sheet title
            if ( isset($que_sheet['title']) )
            {
                $this->objPHPExcel->setActiveSheetIndex($num_sheet)->setTitle($que_sheet['title']);
            }
        }

        // Leave first sheet active
        $this->objPHPExcel->setActiveSheetIndex(0);
    }


    /**
     * Override run callback
     */
    public function run()
    {
        if ( $this->grid_mode == 'export' )
        {
            foreach ( $this->sheets as $num_sheet => $que_sheet )
            {
                // Activate current sheet
                $this->objPHPExcel->setActiveSheetIndex($num_sheet);

                $this->columns = $que_sheet['columns'];
                $this->initColumns();
                $this->dataProvider = $que_sheet['data'];

                $this->renderHeader();
                $total_rows = $this->renderBody();
                $this->renderFooter($total_rows);

                // Set auto width
                if($this->autoWidth)
                {
                    foreach($this->columns as $n=>$column)
                    {
                        $this->objPHPExcel->getActiveSheet()->getColumnDimension($this->columnName($n+1))->setAutoSize(true);
                    }
                }

                // <DEZERO> - AfterRender
                if ( is_callable([$this, $this->afterRender]) )
                {
                    call_user_func_array([$this, $this->afterRender], [$total_rows, $num_sheet]);
                }
                // </DEZERO>
            }

            // Create writer for saving
            $objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, $this->exportType);
            if ( !$this->stream )
            {
                $objWriter->save($this->filename);
            }
            
            // Output to browser
            else 
            {
                if ( ! $this->filename )
                {
                    $this->filename = $this->title;
                }
                
                // Force Excel file to be downloaded via HTTP
                $this->cleanOutput();
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-type: '.$this->mimeTypes[$this->exportType]['Content-type']);
                header('Content-Disposition: attachment; filename="'.$this->filename.'.'.$this->mimeTypes[$this->exportType]['extension'].'"');
                header('Cache-Control: max-age=0');             
                $objWriter->save('php://output');

                // Check if we need to update ExportExcel model
                if ( isset($_GET['batch_id']) )
                {
                    $export_excel_model = ExportExcel::findOne($_GET['batch_id']);
                    if ( $export_excel_model )
                    {
                        // This attribute "last_operation_num" will be checked on ExcelTrait::actionCheckExcel()
                        $export_excel_model->last_operation_num = 1;

                        // Get total row number and ignore header
                        $export_excel_model->total_items = $this->objPHPExcel->getActiveSheet()->getHighestRow();
                        if ( $export_excel_model->total_items > 1 )
                        {
                            $export_excel_model->total_items--;
                        }

                        if ( ! $export_excel_model->save() )
                        {
                            Log::save_model_error($export_excel_model);
                        }
                    }
                }

                Yii::app()->end();
            }
        }
        else
        {
            parent::run();
        }
    }


    /**
     * After render callback
     */
    public function afterRender($total_rows, $num_sheet)
    {
        // foreach( $this->sheets as $num_sheet => $que_sheet )
        // {
        // }

        // Activate current sheet
        $this->objPHPExcel->setActiveSheetIndex($num_sheet);

        $last_row = $this->objPHPExcel->getActiveSheet()->getHighestRow();
        $last_column = $this->objPHPExcel->getActiveSheet()->getHighestColumn();
        $full_worksheet_dimension = $this->objPHPExcel->getActiveSheet()->calculateWorksheetDimension();

        // Header in bold
        $this->objPHPExcel->getActiveSheet()->getStyle('A1:'. $last_column .'1')->getFont()->setBold(TRUE);

        // Vertical align
        $this->objPHPExcel->getActiveSheet()->getStyle($full_worksheet_dimension)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        // Freeze columns "A2" and "C1" - Header and first column fixed
        $this->objPHPExcel->getActiveSheet()->freezePane('B2');

        // Apply column formats
        $this->apply_column_formats();

        // We want first sheet selected when user opens Excel for the first time
        $this->objPHPExcel->setActiveSheetIndex(0);

        /*
        // EXAMPLE - Set HTML content
        $this->objPHPExcel->getActiveSheet()->getStyle('N1:N'. $last_row)->getAlignment()->setWrapText(TRUE);

        // EXAMPLE - Set row height
        $this->objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(20);
        $this->objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);

        // EXAMPLE - FORMULA
        if ( $num_sheet == 1 )
        {
            // Add new colum
            $this->objPHPExcel->getActiveSheet()->setCellValue(
                'I2',
                "=VLOOKUP(A2,'Datos de modelos'!A:F,2)"
            );
        }

        // EXAMPLE- Footer color example
        if ( $this->subject == 'Products' )
        {
            // Average background
            $this->objPHPExcel->getActiveSheet()->getStyle('A'. $last_row .':'. $last_column . $last_row)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'FCCBA2')
                    )
                )
            );
        }

        // EXAMPLE - Font size
        $this->objPHPExcel->getActiveSheet()->getStyle($full_worksheet_dimension)->getFont()->setSize(13);
        */
    }


    /**
     * Apply format on each Excel column
     * 
     * @since 27/11/2020
     */
    public function apply_column_formats()
    {
        $last_row = $this->objPHPExcel->getActiveSheet()->getHighestRow();
        $last_column = $this->objPHPExcel->getActiveSheet()->getHighestColumn();
        $full_worksheet_dimension = $this->objPHPExcel->getActiveSheet()->calculateWorksheetDimension();

        // Apply formats
        if ( !empty($this->columns) )
        {
            foreach ( $this->columns as $num_column => $que_column )
            {
                $format_name = 'text';
                $column_name = $this->columnName($num_column+1);
                if ( !empty($que_column->htmlOptions) && isset($que_column->htmlOptions['format']) )
                {
                    $format_name = $que_column->htmlOptions['format'];
                }

                // Text format, by default
                $format_code = PHPExcel_Style_NumberFormat::FORMAT_TEXT;
                switch ( $format_name )
                {
                    // Currency format
                    case 'currency':
                        $format_code = '#,##0.00_-[$€]';
                    break;

                    case 'integer':
                        $format_code = PHPExcel_Style_NumberFormat::FORMAT_NUMBER;

                    break;

                    case 'number':
                    case 'float':
                        $format_code = PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00;
                    break;
                }

                // \DzLog::dev($this->objPHPExcel->getActiveSheet()->getTitle() .' ---- '. $format_name .' ----> '. $column_name .'2:'. $column_name . $last_row);

                // Apply format
                $this->objPHPExcel->getActiveSheet()->getStyle($column_name .'2:'. $column_name . $last_row)->getNumberFormat()->setFormatCode($format_code);
            }
        }
    }


    /**
     * Render header for each sheet
     *
    public function renderHeader()
    {
        foreach($this->sheets as $num_sheet => $que_sheet)
        {
            // Activate current sheet
            $this->objPHPExcel->setActiveSheetIndex($num_sheet);

            $this->columns = $que_sheet['columns'];
            $this->initColumns();
            $this->dataProvider = $que_sheet['data'];

            parent::renderHeader();
        }

        // Leave first sheet active
        $this->objPHPExcel->setActiveSheetIndex(0);
    }


    /**
     * Render body for each sheet
     *
    public function renderBody()
    {
        foreach($this->sheets as $num_sheet => $que_sheet)
        {
            // Activate current sheet
            $this->objPHPExcel->setActiveSheetIndex($num_sheet);

            $this->columns = $que_sheet['columns'];
            $this->initColumns();
            $this->dataProvider = $que_sheet['data'];

            $n = parent::renderBody();
        }

        // Leave first sheet active
        $this->objPHPExcel->setActiveSheetIndex(0);

        return $n;
    }


    /**
     * Render row for each sheet
     *
    public function renderRow($row)
    {
        foreach($this->sheets as $num_sheet => $que_sheet)
        {
            // Activate current sheet
            $this->objPHPExcel->setActiveSheetIndex($num_sheet);

            $this->columns = $que_sheet['columns'];
            $this->initColumns();
            $this->dataProvider = $que_sheet['data'];

            parent::renderRow($row);
        }

        // Leave first sheet active
        $this->objPHPExcel->setActiveSheetIndex(0);
    }


    /**
     * Render footer for each sheet
     *
    public function renderFooter($row)
    {
        foreach($this->sheets as $num_sheet => $que_sheet)
        {
            // Activate current sheet
            $this->objPHPExcel->setActiveSheetIndex($num_sheet);

            $this->columns = $que_sheet['columns'];
            $this->initColumns();
            $this->dataProvider = $que_sheet['data'];

            parent::renderFooter($row);
        }

        // Leave first sheet active
        $this->objPHPExcel->setActiveSheetIndex(0);
    }
    */


    /**
     * Performs cleaning on mutliple levels.
     * 
     * From le_top @ yiiframework.com
     * 
     */
    private static function cleanOutput() 
    {
        for ($level = ob_get_level(); $level > 0; --$level)
        {
            @ob_end_clean();
        }
    }
}