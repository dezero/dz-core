<?php
/**
 * ExcelTrait class file with a list of CONTROLLERS actions
 *
 * Export actions with Excel via BATCH jobs
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2020 Dezero
 */

namespace dz\utils\excel;

use dz\batch\ExportExcel;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\helpers\Url;
use Yii;

trait ExcelTrait
{
    /**
     * Return total items to export
     */
    public function actionExportTotalItems($class, $model_class = '')
    {
        // Delete action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsPostRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        $total_items = 0;
        if ( isset($_POST['export_url']) && !empty($_POST['export_url']) )
        {
            // Get total items to export
            $model = new $class('search');
            $model->unsetAttributes();

            // Grid query params
            if ( empty($model_class) )
            {
                $model_class = StringHelper::basename($class);
            }
            $vec_query_url = [];
            $que_query_url = parse_url($_POST['export_url'], PHP_URL_QUERY); 
            parse_str(html_entity_decode($que_query_url), $vec_query_url);
            if ( !empty($vec_query_url) && isset($vec_query_url[$model_class]) )
            {
                $model->setAttributes($vec_query_url[$model_class]);
            }

            // Extra params
            if ( isset($_POST[$model_class]) && !empty($_POST[$model_class]) )
            {
                $model->setAttributes($_POST[$model_class]);
            }

            // Get total items
            $dataProvider = $model->search('excel_preview');
            if ( !empty($dataProvider) )
            {
                $total_items = $dataProvider->getTotalItemCount();
            }
        }
        
        // Return JSON and end application
        $this->jsonOutput(200, Json::encode(['total_items' => $total_items]));
    }


    /**
     * Create a BATCH job for the Excel generation process
     */
    public function actionCreateExcel($name)
    {
        // Delete action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsPostRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        // By default, batch job is not found (batch_id = -1)
        $vec_ajax_output = ['batch_id' => -1];

        if ( isset($_POST['export_url']) && !empty($_POST['export_url']) )
        {
            $export_excel_model = new ExportExcel;
            $export_excel_model->setAttributes([
                'name'                  => $name,
                'last_operation_num'    => 0,
                'total_operations'      => 1,
                'results_json'          => Json::encode(['export_url' => $_POST['export_url']]),

                // Optional attributes
                'model_id'             => isset($_GET['model_id']) ? $_GET['model_id'] : null,
                'model_class'          => isset($_GET['model_class']) ? $_GET['model_class'] : null,
                'model_type'           => isset($_GET['model_type']) ? $_GET['model_type'] : null,
                'model_scenario'       => isset($_GET['model_scenario']) ? $_GET['model_scenario'] : 'export',
            ]);

            if ( $export_excel_model->save() )
            {
                $vec_ajax_output['batch_id'] = $export_excel_model->batch_id;
            }
            else
            {
                Log::save_model_error($export_excel_model);
            }
        }

        // Return JSON and end application
        $this->jsonOutput(200, Json::encode($vec_ajax_output));
    }


    /**
     * Check if BATCH job has been finished for the Excel generation process
     */
    public function actionCheckExcel()
    {
        // Delete action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsPostRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        // By default, error (result = -1)
        $vec_ajax_output = ['result' => -1];

        if ( isset($_POST['batch_id']) && !empty($_POST['batch_id']) )
        {
            $export_excel_model = ExportExcel::findOne($_POST['batch_id']);
            if ( $export_excel_model && $export_excel_model->is_finished() )
            {
                $vec_ajax_output['result'] = 1;
            }
        }

        // Return JSON and end application
        $this->jsonOutput(200, Json::encode($vec_ajax_output));
    }
}