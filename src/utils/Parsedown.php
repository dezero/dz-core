<?php

/**
 * Extension for `erusev/parsedown` package
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link https://github.com/erusev/parsedown
 * @link https://github.com/erusev/parsedown-extra
 */

namespace dz\utils;

use Yii;

class Parsedown extends \ParsedownExtra
{
    /**
     * Override Parsedown::element() method
     */
    protected function element(array $Element)
    {
        // Links with class (<a href="" class="">) - Change attributes order
        // --> From <a href="" class=""> to <a class="" href="">
        if (isset($Element['attributes']) && isset($Element['attributes']['href']) && isset($Element['attributes']['class']) )
        {
            $href = $Element['attributes']['href'];
            unset($Element['attributes']['href']);
            $Element['attributes']['href'] = $href;
        }

        return parent::element($Element);
    }
}