<?php
/**
 * @package dz\batch
 */

namespace dz\batch;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dz\helpers\Url;
use dz\batch\Batch as Batch;
use dz\modules\asset\models\AssetFile;
use dz\helpers\XmlParser;
use user\models\User;
use XMLReader;
use Yii;

/**
 * Batch model class for "batch" database table
 *
 * Columns in table "batch" available as properties of the model,
 * followed by relations of table "batch" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $batch_id
 * @property string $batch_type
 * @property string $name
 * @property string $description
 * @property string $last_operation_name
 * @property integer $last_operation_num
 * @property integer $total_operations
 * @property string $operations_json
 * @property integer $total_items
 * @property integer $num_errors
 * @property integer $num_warnings
 * @property integer $item_starting_num
 * @property integer $item_ending_num
 * @property string $model_id
 * @property string $model_class
 * @property string $model_type
 * @property string $model_scenario
 * @property integer $file_id
 * @property string $summary_json
 * @property string $results_json
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $file
 * @property mixed $createdUser
 * @property mixed $updatedUser
 */
class ImportXml extends Batch
{
    /**
     * @var string. Batch type
     */
    public $batch_type = 'xml_import';


    /**
     * @var int. Total operations is just one: "Import XML"
     */
    public $total_operations = 1;


    /**
     * @var string. Import action
     */
    public $model_scenario = 'import';


    /**
     * @var string. File path where XML is saved
     */
    public $xml_file_path;


    /**
     * @var int. Current XML node number
     */
    public $num_node;


    /**
     * @var bool. Save XML file?
     */
    protected $is_save_file = true;


    /**
     * @var array. XML tags to process
     */
    protected $vec_xml_tags = [];


    /**
     * @var array. Errors (internal use)
     */
    protected $vec_errors = [];


    /**
     * @var array. Warnings (internal use)
     */
    protected $vec_warnings = [];


    /**
     * @var XMLReader
     */
    protected $xml_reader;


    /**
     * @var string. Raw XML string
     */
    private $raw_xml_string;


    /**
     * Constructor
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     * Event "afterValidate"
     *
     * This method is invoked after validation end
     */
    public function afterValidate()
    {
        // New file
        if ( $this->isNewRecord || ($this->is_save_file && $this->scenario == 'upload_file') )
        {
            $this->import_xml();
        }

        return parent::afterValidate();
    }


    /**
     * Event "afterSave"
     *
     * This method is invoked after each record is saved
     */
    public function afterSave()
    {
        switch ( $this->scenario )
        {
            case 'insert':
            case 'upload_file':
                // Save "batch_id" in AssetFile (double relationship information)
                if ( $this->is_save_file && $this->file )
                {
                    $this->file->saveAttributes([
                        'entity_id'     => $this->batch_id,
                        'entity_type'   => StringHelper::basename(get_class($this)), // self::class
                    ]);
                }
            break;
        }

        parent::afterSave();
    }


    /**
     * Return relative path where imported files are stored
     */
    public function get_import_path()
    {
        return Yii::app()->path->get('importXmlPath');
    }


    /**
     * Return current raw XML string
     */
    public function get_raw_xml_string()
    {
        return $this->raw_xml_string;
    }


    /**
     * Set file path where XML is saved
     */
    public function set_xml_file_path($xml_file_path)
    {
        $this->xml_file_path = $xml_file_path;
    }


    /**
     * Set XML tags to process
     */
    public function set_xml_tags($vec_xml_tags)
    {
        return $this->vec_xml_tags = $vec_xml_tags;
    }


    /**
     * Get private download URL
     */
    public function download_url()
    {
        if ( $this->file )
        {
            return Url::to('/asset/download', ['id' => $this->file->uuid]);
        }

        return Url::base();
    }


    /**
     * Import items from a XML document file
     */
    public function import_xml()
    {
        // Get XML file path
        if ( $this->xml_file_path === null && $this->file && $this->file->load_file() )
        {
            $this->xml_file_path = $this->file->file->getRealPath();
        }

        return $this->import_xml_nodes();
    }


    /**
     * Read a XML document and process selected nodes (XML tags)
     *
     * @see https://www.php.net/manual/en/book.xmlreader.php
     * @see https://www.php.net/manual/en/class.simplexmlelement.php
     */
    public function import_xml_nodes($vec_xml_tags = [])
    {
        if ( !empty($this->xml_file_path) )
        {
            if ( empty($vec_xml_tags) )
            {
                $vec_xml_tags = $this->vec_xml_tags;
            }

            // Process XML file using XMLReader class from PHP core
            $this->num_node = 0;
            $this->xml_reader = new XMLReader();
            if ( $this->xml_reader->open($this->xml_file_path) )
            {
                while ( $this->xml_reader->read() )
                {
                    // Process each XML node. For example, <product>
                    if ( $this->xml_reader->nodeType === XMLReader::ELEMENT && in_array($this->xml_reader->name, $this->vec_xml_tags) )
                    {
                        $this->num_node++;
                        $this->import_xml_node($this->xml_reader->name);
                    }
                }

                $this->last_operation_num = 1;
            }
            else
            {
                // Error opening XML file
                $this->add_error(0, 'open_error');
            }
        }
    }


    /**
     * Import a single XML node with SimpleXMLElement class from PHP core
     */
    public function import_xml_node($xml_node_tag)
    {
        $this->raw_xml_string = $this->xml_reader->readOuterXml();
        $vec_data = XmlParser::xml_to_array($this->raw_xml_string);

        return $this->import_data($vec_data, $xml_node_tag);
    }


    /**
     * Import data from a single XML node
     */
    public function import_data($vec_data, $xml_node_tag)
    {
    }


    /**
     * Return attributes from a XML node
     */
    protected function get_xml_attributes()
    {
        $vec_attributes = [];
        if ( $this->xml_reader->hasAttributes )
        {
            while ( $this->xml_reader->moveToNextAttribute() )
            {
                $vec_attributes[$this->xml_reader->name] = $this->xml_reader->value;
            }
        }

        return $vec_attributes;
    }



    /**
     * Get total number of inserted, updated, errors or warnings
     */
    public function get_totals($type = '')
    {
        // Total items?
        if ( empty($type) )
        {
            return $this->total_items;
        }

        // Specific total type
        if ( !empty($this->summary_json) && is_array($this->summary_json) && isset($this->summary_json[$type]) )
        {
            return $this->summary_json[$type];
        }

        switch ( $type )
        {
            case 'error':
            case 'errors':
                return $this->num_errors;
            break;

            case 'warning':
            case 'warnings':
                return $this->num_warnings;
            break;
        }

        return 0;
    }

    /**
     * Add an error message
     */
    public function add_error($num_node, $error_code, $error_content = '')
    {
        if ( ! isset($this->vec_errors[$num_node]) )
        {
            $this->vec_errors[$num_node] = [];
        }

        $this->vec_errors[$num_node][$error_code] = $error_content;
    }


    /**
     * Check if a XML node has an error
     */
    public function is_error($num_node, $error_code = null)
    {
        // There's some registered error for $num_node?
        if ( isset($this->vec_errors[$num_node]) )
        {
            // There's at least one error for $num_node
            // or there's an specific error code (if it has been given as input param)
            return empty($error_code) || isset($this->vec_errors[$num_node][$error_code]);
        }

        return false;
    }


    /**
     * Get all the detected errors on a XML node
     */
    public function get_errors($num_node)
    {
        if ( ! isset($this->vec_errors[$num_node]) )
        {
            return null;
        }

        return $this->vec_errors[$num_node];
    }


    /**
     * Returns an array with all the errors ready for output
     */
    public function output_errors($vec_errors)
    {
        $vec_output = [];
        if ( !empty($vec_errors) )
        {
            foreach ( $vec_errors as $error_code => $error_content )
            {
                $que_error = $this->get_error_description($error_code, $error_content);
                if ( is_array($que_error) )
                {
                    $vec_output = \CMap::mergeArray($vec_output, $que_error);
                }
                else
                {
                    $vec_output[] = $que_error;
                }
            }
        }

        return $vec_output;
    }


    /**
     * Get error description
     */
    public function get_error_description($error_code, $error_content = '')
    {
        switch ( $error_code )
        {
            case 'open_error':
                return Yii::t('app', 'XML file cannot be opened');
            break;

            case 'validate_error':
            case 'create_error':
            case 'update_error':
            case 'save_error':
                if ( ! empty($error_content) )
                {
                    if ( is_array($error_content) )
                    {
                        $vec_errors = [];
                        foreach ( $error_content as $attribute_name => $vec_attribute_errors )
                        {
                            foreach ( $vec_attribute_errors as $error_msg )
                            {
                                // $vec_errors[] = $attribute_name .' - '. $error_msg;
                                $vec_errors[] = $error_msg;
                            }
                        }
                        return $vec_errors;
                    }

                    return $error_content;
                }

                return Yii::t('app', 'XML node could no be imported');
            break;
        }

        return '';
    }


    /**
     * Add a warning message
     */
    public function add_warning($num_node, $warning_content)
    {
        if ( ! isset($this->vec_warnings[$num_node]) )
        {
            $this->vec_warnings[$num_node] = [];
        }

        $this->vec_warnings[$num_node] = $warning_content;
    }


    /**
     * Check if a XML node has a warning
     */
    public function is_warning($num_node)
    {
        return isset($this->vec_warnings[$num_node]);
    }


    /**
     * Get all the detected warnings on a XML node
     */
    public function get_warnings($num_node)
    {
        if ( ! isset($this->vec_warnings[$num_node]) )
        {
            return null;
        }

        return $this->vec_warnings[$num_node];
    }


    /**
     * Returns an array with all the warnings ready for output
     */
    public function output_warnings($vec_warnings)
    {
        $vec_output = [];
        if ( !empty($vec_warnings) )
        {
            foreach ( $vec_warnings as $que_warning )
            {
                if ( is_array($que_warning) )
                {
                    $vec_output = \CMap::mergeArray($vec_output, $que_warning);
                }
                else
                {
                    $vec_output[] = $que_warning;
                }
            }
        }

        return $vec_output;
    }
}
