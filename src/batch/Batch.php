<?php
/**
 * @package dz\batch 
 */

namespace dz\batch;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Json;
use dz\helpers\StringHelper;
use dz\batch\_base\Batch as BaseBatch;
use dz\modules\asset\models\AssetFile;
use user\models\User;
use Yii;

/**
 * Batch model class for "batch" database table
 *
 * Columns in table "batch" available as properties of the model,
 * followed by relations of table "batch" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $batch_id
 * @property string $batch_type
 * @property string $name
 * @property string $description
 * @property string $last_operation_name
 * @property integer $last_operation_num
 * @property integer $total_operations
 * @property string $operations_json
 * @property integer $total_items
 * @property integer $num_errors
 * @property integer $num_warnings
 * @property integer $item_starting_num
 * @property integer $item_ending_num
 * @property string $model_id
 * @property string $model_class
 * @property string $model_type
 * @property string $model_scenario
 * @property integer $file_id
 * @property string $summary_json
 * @property string $results_json
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $file
 * @property mixed $createdUser
 * @property mixed $updatedUser
 */
class Batch extends BaseBatch
{
    /**
     * Array for operations data
     */
    public $vec_operations_json = [];


    /**
     * Array for summary data
     */
    public $vec_summary_json = [];


    /**
     * Array for results data
     */
    public $vec_results_json = [];


	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['batch_type, name, created_date, created_uid, updated_date, updated_uid', 'required'],
			['last_operation_num, total_operations, total_items, num_errors, num_warnings, item_starting_num, item_ending_num, file_id, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
            ['batch_type, model_id, model_type, model_scenario', 'length', 'max'=> 32],
			['name, last_operation_name', 'length', 'max'=> 128],
            ['description', 'length', 'max'=> 255],
			['operations_json, summary_json', 'length', 'max'=> 512],
            ['model_class', 'length', 'max'=> 64],
			['uuid', 'length', 'max'=> 36],
			['description, last_operation_name, last_operation_num, total_operations, operations_json, total_items, num_errors, num_warnings, item_starting_num, item_ending_num, model_id, model_class, model_type, model_scenario, file_id, summary_json, results_json, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['results_json', 'safe'],
			['batch_id, batch_type, name, description, last_operation_name, last_operation_num, total_operations, operations_json, total_items, num_errors, num_warnings, item_starting_num, item_ending_num, model_id, model_class, model_type, model_scenario, file_id, summary_json, results_json, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'file' => [self::BELONGS_TO, AssetFile::class, 'file_id'],
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'batch_id' => Yii::t('app', 'Batch'),
            'batch_type' => Yii::t('app', 'Batch Type'),
			'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
			'last_operation_name' => Yii::t('app', 'Last Operation Name'),
			'last_operation_num' => Yii::t('app', 'Last Operation Number'),
			'total_operations' => Yii::t('app', 'Total Operations'),
			'operations_json' => Yii::t('app', 'Operations Json'),
			'total_items' => Yii::t('app', 'Total Items'),
            'num_errors' => Yii::t('app', 'Errors'),
            'num_warnings' => Yii::t('app', 'Warnings'),
			'item_starting_num' => Yii::t('app', 'Item Starting Number'),
			'item_ending_num' => Yii::t('app', 'Item Ending Number'),
			'model_id' => Yii::t('app', 'Model'),
			'model_class' => Yii::t('app', 'Model Class'),
            'model_type' => Yii::t('app', 'Model Type'),
			'model_scenario' => Yii::t('app', 'Model Scenario'),
            'file_id' => Yii::t('app', 'File'),
			'summary_json' => Yii::t('app', 'Summary Results'),
			'results_json' => Yii::t('app', 'Results'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
            'file' => null,
			'createdUser' => null,
			'updatedUser' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.batch_id', $this->batch_id);
        $criteria->compare('t.batch_type', $this->batch_type);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.description', $this->description, true);
        $criteria->compare('t.last_operation_name', $this->last_operation_name, true);
        $criteria->compare('t.last_operation_num', $this->last_operation_num);
        $criteria->compare('t.total_operations', $this->total_operations);
        $criteria->compare('t.operations_json', $this->operations_json, true);
        $criteria->compare('t.total_items', $this->total_items);
        $criteria->compare('t.num_errors', $this->num_errors);
        $criteria->compare('t.num_warnings', $this->num_warnings);
        $criteria->compare('t.item_starting_num', $this->item_starting_num);
        $criteria->compare('t.item_ending_num', $this->item_ending_num);
        $criteria->compare('t.model_id', $this->model_id);
        $criteria->compare('t.model_class', $this->model_class);
        $criteria->compare('t.model_type', $this->model_type);
        $criteria->compare('t.model_scenario', $this->model_scenario);
        $criteria->compare('t.file_id', $this->file_id);
        $criteria->compare('t.summary_json', $this->summary_json, true);
        $criteria->compare('t.results_json', $this->results_json, true);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['batch_id' => true]]
        ]);
    }


    /**
     * Batch models list
     * 
     * @return array
     */
     public function batch_list()
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['batch_id', 'name'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = Batch::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('batch_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Event "afterValidate"
     *
     * This method is invoked after validation end
     */
    public function afterValidate()
    {
        // Encode JSON attributes
        $this->json_encode();

        return parent::afterValidate();
    }


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // JSON format
            'JsonBehavior' => [
                'class' => '\dz\behaviors\JsonBehavior',
                'columns' => [
                    'operations_json',
                    'summary_json',
                    'results_json'
                ],
            ],
        ], parent::behaviors());
    }


    /**
     * Custom encode function for JSON fields
     */
    public function json_encode()
    {
        $vec_attributes = [
            'operations_json' => 'vec_operations_json',
            'summary_json' => 'vec_summary_json',
            'results_json' => 'vec_results_json'
        ];

        foreach ( $vec_attributes as $json_attribute => $array_attribute )
        {
            if ( isset($this->$array_attribute) && !empty($this->$array_attribute) && is_array($this->$array_attribute) )
            {
                $this->$json_attribute = Json::encode($this->$array_attribute);
            }
        }
    }
}