<?php
/**
 * @package dz\batch 
 */

namespace dz\batch;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dz\helpers\Url;
use dz\batch\Batch as Batch;
use dz\modules\asset\models\AssetFile;
use user\models\User;
use Yii;

/**
 * Batch model class for "batch" database table
 *
 * Columns in table "batch" available as properties of the model,
 * followed by relations of table "batch" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $batch_id
 * @property string $batch_type
 * @property string $name
 * @property string $description
 * @property string $last_operation_name
 * @property integer $last_operation_num
 * @property integer $total_operations
 * @property string $operations_json
 * @property integer $total_items
 * @property integer $num_errors
 * @property integer $num_warnings
 * @property integer $item_starting_num
 * @property integer $item_ending_num
 * @property string $model_id
 * @property string $model_class
 * @property string $model_type
 * @property string $model_scenario
 * @property integer $file_id
 * @property string $summary_json
 * @property string $results_json
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $file
 * @property mixed $createdUser
 * @property mixed $updatedUser
 */
class ExportExcel extends Batch
{
    /**
     * @var string. Batch type
     */
    public $batch_type = 'excel_export';


    /**
     * @var int. Total operations is just one: "Export Excel"
     */
    public $total_operations = 1;


    /**
     * @var string. Export action
     */
    public $model_scenario = 'export';


    /**
     * Constructor
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     * Check if this batch job has been finished
     */
    public function is_finished()
    {
        return $this->last_operation_num == 1;
    }
}