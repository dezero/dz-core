<?php
/**
 * @package dz\batch 
 */

namespace dz\batch;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dz\helpers\Url;
use dz\batch\Batch as Batch;
use dz\modules\asset\models\AssetFile;
use user\models\User;
use Yii;

/**
 * Batch model class for "batch" database table
 *
 * Columns in table "batch" available as properties of the model,
 * followed by relations of table "batch" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $batch_id
 * @property string $batch_type
 * @property string $name
 * @property string $description
 * @property string $last_operation_name
 * @property integer $last_operation_num
 * @property integer $total_operations
 * @property string $operations_json
 * @property integer $total_items
 * @property integer $num_errors
 * @property integer $num_warnings
 * @property integer $item_starting_num
 * @property integer $item_ending_num
 * @property string $model_id
 * @property string $model_class
 * @property string $model_type
 * @property string $model_scenario
 * @property integer $file_id
 * @property string $summary_json
 * @property string $results_json
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $file
 * @property mixed $createdUser
 * @property mixed $updatedUser
 */
class ImportExcel extends Batch
{
    /**
     * @var string. Batch type
     */
    public $batch_type = 'excel_import';


    /**
     * @var int. Total operations is just one: "Import Excel"
     */
    public $total_operations = 1;


    /**
     * @var string. Import action
     */
    public $model_scenario = 'import';


    /**
     * PHPExcel library path
     */
    public $lib_path = '@lib.PHPExcel.Classes.PHPExcel';


    /**
     * Exclude first row of Excel file. Usually with header information
     */
    public $is_exclude_first_row = true;


    /**
     * @var array. Errors (internal use)
     */
    protected $vec_errors = [];


    /**
     * Constructor
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     * Event "afterValidate"
     *
     * This method is invoked after validation end
     */
    public function afterValidate()
    {
        // New file
        if ( $this->isNewRecord || $this->scenario == 'upload_file' )
        {
            $this->import_excel();
        }

        return parent::afterValidate();
    }


    /**
     * Event "afterSave"
     *
     * This method is invoked after each record is saved
     */
    public function afterSave()
    {   
        switch ( $this->scenario )
        {
            case 'insert':
            case 'upload_file':
                // Save "batch_id" in AssetFile (double relationship information)
                if ( $this->file )
                {
                    $this->file->saveAttributes([
                        'entity_id'     => $this->batch_id,
                        'entity_type'   => StringHelper::basename(get_class($this)), // self::class
                    ]);
                }


            break;
        }

        parent::afterSave();
    }


    /**
     * Return relative path where imported files are stored
     */
    public function get_import_path()
    {
        return Yii::app()->path->get('importExcelPath');
    }


    /**
     * Get private download URL
     */
    public function download_url()
    {
        if ( $this->file )
        {
            return Url::to('/asset/download', ['id' => $this->file->uuid]);
        }

        return Url::base();
    }


    /**
     * Import items from an Excel file
     */
    public function import_excel()
    {
        // Import Excel
        if ( $this->file && $this->file->load_file() )
        {
            // Load PHPExcel library
            spl_autoload_unregister(['YiiBase','autoload']);
            Yii::import($this->lib_path, true);
            $php_excel = \PHPExcel_IOFactory::load($this->file->file->getRealPath());
            spl_autoload_register(['YiiBase','autoload']);
            // Load PHPExcel library

            // Process each row
            $vec_rows = $php_excel->getActiveSheet()->toArray(null, true, true, true);
            if ( !empty($vec_rows) )
            {
                return $this->import_rows($vec_rows);
            }
        }

        return false;
    }


    /**
     * Import rows from Excel file
     */
    public function import_rows($vec_rows)
    {
        $num_row = 0;
        foreach ( $vec_rows as $que_row )
        {
            $num_row++;

            // Exclude first row (header)
            if ( $num_row > 1 || ( ! $this->is_exclude_first_row && $num_row > 0) )
            {
                $this->import_row($que_row, $num_row);
            }
        }

        $this->last_operation_num = 1;
    }


    /**
     * Import a single row from Excel file
     */
    public function import_row($vec_row, $num_row)
    {
    }


    /**
     * Get total number of inserted, updated, errors or warings
     */
    public function get_totals($type = '')
    {
        // Total items?
        if ( empty($type) )
        {
            return $this->total_items;
        }

        // Specific total type
        if ( !empty($this->summary_json) && is_array($this->summary_json) && isset($this->summary_json[$type]) )
        {
            return $this->summary_json[$type];
        }

        switch ( $type )
        {
            case 'error':
            case 'errors':
                return $this->num_errors;
            break;

            case 'warning':
            case 'warnings':
                return $this->num_warnings;
            break;
        }

        return 0;
    }

    /**
     * Add an error
     */
    public function add_error($num_row, $error_code, $error_content = '')
    {
        if ( ! isset($this->vec_errors[$num_row]) )
        {
            $this->vec_errors[$num_row] = [];
        }

        $this->vec_errors[$num_row][$error_code] = $error_content;
    }


    /**
     * Check if an Excel row has an error
     */
    public function is_error($num_row, $error_code = null)
    {
        // There's some registered error for $num_row?
        if ( isset($this->vec_errors[$num_row]) )
        {
            // There's at least one error for $num_row
            // or there's an specific error code (if it has been given as input param)
            return empty($error_code) || isset($this->vec_errors[$num_row][$error_code]);
        }

        return false;
    }


    /**
     * Get all the detected errors on a Excel row
     */
    public function get_errors($num_row)
    {
        if ( ! isset($this->vec_errors[$num_row]) )
        {
            return null;
        }

        return $this->vec_errors[$num_row];
    }


    /**
     * * Returns an array with all the errors ready for output
     */
    public function output_errors($vec_errors)
    {
        $vec_output = [];
        if ( !empty($vec_errors) )
        {
            foreach ( $vec_errors as $error_code => $error_content )
            {
                $que_error = $this->get_error_description($error_code, $error_content);
                if ( is_array($que_error) )
                {
                    $vec_output = \CMap::mergeArray($vec_output, $que_error);
                }
                else
                {
                    $vec_output[] = $que_error;
                }
            }
        }

        return $vec_output;
    }


    /**
     * Get error description
     */
    public function get_error_description($error_code, $error_content = '')
    {
        switch ( $error_code )
        {
            case 'wrong_columns':
                return Yii::t('app', 'Excel file - Incorrect number of columns');
            break;

            case 'validate_error':
            case 'create_error':
            case 'update_error':
            case 'save_error':
                if ( empty($error_content) )
                {
                    if ( is_array($error_content) )
                    {
                        $vec_errors = [];
                        foreach ( $error_content as $attribute_name => $vec_attribute_errors )
                        {
                            foreach ( $vec_attribute_errors as $error_msg )
                            {
                                // $vec_errors[] = $attribute_name .' - '. $error_msg;
                                $vec_errors[] = $error_msg;
                            }
                        }
                        return $vec_errors;
                    }
                    
                    return $error_content;
                }

                return Yii::t('app', 'Row could no be imported');
            break;
        }

        return '';
    }
}
