<?php
/**
 * DzClientScript class file
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2013 Dezero
 */

namespace dz\web;

use Yii;

// Import NLSClientScript class
Yii::import('@lib.NLSClientScript');

class ClientScript extends \NLSClientScript
{
	/**
	 * CSS files added at top of <head>
	 */
	public $cssTopFiles = [];


	/**
	 * CSS files added at the end of <head>
	 */
	public $cssLastFiles = [];


	/**
	 * @param string $appVersion
	 * Optional, version of the application.
	 * If set to not empty, will be appended to the merged js/css urls (helps to handle cached resources).
	 **/
	public $appVersion = '';


	/**
	 * Init function. Required!
	 */
	public function init()
	{
		parent::init();
	}


    /**
     * Override CClientScript::reset()
     * 
     * Cleans all registered scripts.
     */
    public function reset()
    {
        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        parent::reset();
        $this->scriptMap['jquery.js'] = false;
        $this->scriptMap['jquery.min.js'] = false;
    }
	

	/**
	 * Registers a javascript file.
	 */
	public function registerScriptFile($url, $position = null, array $htmlOptions = [])
	{
		// JS version file
		if ( !empty($this->appVersion) )
		{
			$url .= '?'. $this->appVersion;
		}

		return parent::registerScriptFile($url, $position, $htmlOptions);
	}


	/**
	 * Avoid duplicate jquery.js
	 */
	public function remapScripts()
	{
		foreach($this->scriptFiles as $position => $scriptFiles)
		{
			// Remove jquery.js && jquery.min.js from POS_END position
			foreach ( $scriptFiles as $scriptFile=>$scriptFileValue )
			{
				if ( $position == self::POS_END && ( preg_match("/\/jquery.js$/", $scriptFileValue) || preg_match("/\/jquery.min.js$/", $scriptFileValue) ) )
				{
					unset($this->scriptFiles[$position][$scriptFile]);
					return parent::remapScripts();
				}
			}
		}
		parent::remapScripts();
	}


	/**
	 * Registers a piece of CSS code.
	 */
	public function registerCssFile($url, $media='')
	{
		if ( $media == 0 )
		{
			$media = '';
		}

		// CSS version file
		if ( !empty($this->appVersion) )
		{
			$url .= '?'. $this->appVersion;
		}
		
		return parent::registerCssFile($url, $media);
	}


	/**
	 * Registers a piece of CSS code to added it at top of <head> tag element
	 */
	public function registerCssTopFile($url, $media='')
	{
		// CSS version file
		if ( !empty($this->appVersion) )
		{
			$url .= '?'. $this->appVersion;
		}

		$this->hasScripts = true;
		$this->cssTopFiles[$url] = $media;
		$params = func_get_args();
		$this->recordCachingAction('clientScript', 'registerCssFile', $params);

		return $this;
	}


	/**
	 * Registers a piece of CSS code to added it at the end of <head> tag element
	 */
	public function registerCssLastFile($url, $media='')
	{
		// CSS version file
		if ( !empty($this->appVersion) )
		{
			$url .= '?'. $this->appVersion;
		}

		$this->hasScripts = true;
		$this->cssLastFiles[$url] = $media;
		$params = func_get_args();
		$this->recordCachingAction('clientScript', 'registerCssFile', $params);

		return $this;
	}


	/**
	 * Override renderHead() function from CClientScript class
	 * 
	 * Inserts the scripts in the head section.
	 * @param string $output the output to be inserted with scripts.
	 */
	public function renderHead(&$output)
	{
		// Add CSS files at the top
		if ( !empty($this->cssTopFiles) )
		{
			$this->cssFiles = \CMap::mergeArray($this->cssTopFiles, $this->cssFiles);
		}

		// Add CSS files at the end
		if ( !empty($this->cssLastFiles) )
		{
			$this->cssFiles = \CMap::mergeArray($this->cssFiles, $this->cssLastFiles);
		}


		return parent::renderHead($output);
	}
}