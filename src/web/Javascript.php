<?php
/**
 * Javascript widget class file
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2013 Dezero
 */

namespace dz\web;

use Yii;

/**
 * Custom DzJavascript widget
 */
class Javascript extends \CWidget
{	
	/**
	 * Global variable spacename
	 * 
	 * @var string
	 */
	public $name = 'js_globals';


	/**
	 * Global Javascript variables
	 * 
	 * @var array
	 */
	public $variables;
	
	
	/**
	 * Initializes the widget
	 */
	public function init()
	{
		$this->variables = [
            'baseUrl'           => Yii::app()->request->baseUrl,
            'rawBaseUrl'        => Yii::app()->request->baseUrl,
            'currentUrl'        => Yii::app()->request->hostInfo . Yii::app()->request->url,
            'module'            => '',
            'controller'        => Yii::app()->controller->id,
            'action'            => Yii::app()->controller->action->id,
            'language'          => Yii::currentLanguage(),
            'defaultLanguage'   => Yii::defaultLanguage(),
		];

        // Add the language prefix to base URL
        if ( Yii::isMultilanguage() && $this->variables['language'] !== $this->variables['defaultLanguage'] )
        {
            $this->variables['baseUrl'] .= '/'. $this->variables['language'];
        }

        // Get module, if it exists
		if ( isset(Yii::app()->controller->module) )
		{
			$this->variables['module'] = Yii::app()->controller->module->id;
		}
	}
	
	
	/**
	 * Get a Javascript variable value
	 *
	 * @var string/array 	Variable name
	 */
	public function getVariable($variable_name)
	{
		if ( isset($this->variables[$variable_name]) )
		{
			return $this->variables[$variable_name];
		}
		return false;
	}


	/**
	 * Set a new Javascript variable value
	 *
	 * @var string/array 	Variable name or array with "key => value" format
	 * @var mixed 			Variable value
	 */
	public function setVariable($variable_name, $variable_value = '')
	{
		if ( is_array($variable_name) )
		{
			$this->variables = \CMap::mergeArray($this->variables, $variable_name);
		}
		else
		{
			$this->variables[$variable_name] = $variable_value;
		}
		return TRUE;
	}


	/**
	 * Runs the widget
	 */
	public function run()
	{
		$vec_params = Yii::app()->params;
		if ( isset($vec_params['js_globals']) )
		{
			// Merge with Yii "params"
			$this->variables = \CMap::mergeArray($this->variables, $vec_params['js_globals']);

			Yii::app()->clientScript->registerScript(
				'js_globals',
				// '<!--//--><![CDATA[//><!-- var '. $this->name .' = '. CJSON::encode($this->variables) .'; //--><!]]>',
				'window.'. $this->name .' = '. \CJSON::encode($this->variables) .';',
				\CClientScript::POS_END
			);
		}
	}
}		