<?php
/**
 * WebModule class file
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2019 Fabián Ruiz
 */
 
namespace dz\web;

use dz\helpers\Html;
use dz\helpers\StringHelper;
use Dz;
use Yii;

/**
 * WebModule is the base class for the application module. 
 */
abstract class Module extends \CWebModule
{
    /**
     * Items per page
     */
    public $itemsPerPage = 20;


    /**
     * Used theme
     */
    public $theme = 'backend';

    
    /**
     * Change default view path
     */
    public $viewPath;


    /**
     * Load specific CSS files for this module
     */
    public $cssFiles;


    /**
     * Load specific JS files for this module
     */
    public $jsFiles;


    /**
     * Base directory path for this module
     */
    public $baseModulePath;


    /**
     * Initializes the module.
     */
    public function init()
    {
        // Define namespaces for Controllers
        $this->controllerNamespace = '\\'. $this->getName() .'\\controllers';

        // Define alias starting with "@"
        $module_path = Yii::getAlias('@'. $this->getName());
        if ( empty($module_path) )
        {
            $module_path = DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $this->getName();
            Yii::setAlias('@'. $this->getName(), $module_path);
        }

        // Check if controller & module has been initialized before. Avoid load module twice
        if ( Yii::app()->getController() === null || Yii::app()->getController()->getModule() === null )
        {
            // VIEWS -> Set view path
            if ( !empty($this->viewPath) )
            {
                $this->setViewPath($this->viewPath);
            }

            // THEME -> Force theme for entire module
            if ( !empty($this->theme) )
            {
                Yii::app()->theme = $this->theme;
            }

            // ASSETS - CSS assets for this module
            $this->load_css();

            // ASSETS - JS assets for this module
            $this->load_js();
        }

        parent::init();
    }


    /**
     * Init a module (allow to be overrided)
     */
    public function init_module($module_path)
    {
        // Base module path
        $this->baseModulePath = $module_path;

         // Define alias starting with "@"
        Yii::setAlias('@'. $this->getName(), $this->baseModulePath);

        // Check if controller & module has been initialized before. Avoid load module twice
        if ( Yii::app()->getController() === null || Yii::app()->getController()->getModule() === null )
        {
            // VIEWS -> Change view path to MODULE path
            if ( empty($this->viewPath) && file_exists($this->baseModulePath . DIRECTORY_SEPARATOR .'views') )
            {
                $this->viewPath = $this->baseModulePath . DIRECTORY_SEPARATOR .'views';
                $this->setViewPath($this->viewPath);
            }
        }
    }


    /**
     * This method is called before any module controller action is performed
     * you may place customized code here
     */
    public function beforeControllerAction($controller, $action)
    {
        // Changing the model name (class from CHtml Yii class)
        Html::setModelNameConverter(function ($model) { 
            $className = is_object($model) ? StringHelper::basename(get_class($model)) : (string)$model;
            return trim(str_replace('\\','_',$className),'_');
        });

        // Force THEME for entire module
        if ( !empty($this->theme) )
        {
            Yii::app()->theme = $this->theme;
        }

        if ( parent::beforeControllerAction($controller, $action) )
        {
            return true;
        }
        return false;
    }


    /**
     * Load CSS files
     */
    public function load_css()
    {
        // ASSETS - CSS assets for this module
        if ( $this->cssFiles === null )
        {
            $this->cssFiles = [$this->getName() .'.css'];
        }
        if ( !empty($this->cssFiles) && is_array($this->cssFiles) )
        {
            foreach ( $this->cssFiles as $css_file )
            {
                // #1 - THEME --> Check CSS file in <theme>/js/module directory
                $que_url = Yii::app()->theme->baseUrl;
                if ( file_exists(Yii::app()->theme->basePath . DIRECTORY_SEPARATOR .'css'. DIRECTORY_SEPARATOR .'modules'. DIRECTORY_SEPARATOR . $css_file) )
                {
                    Yii::app()->getClientScript()->registerCssLastFile($que_url .'/css/modules/'. $css_file);
                }

                // #2 - CUSTOM MODULE --> Check CSS file in /app/modules/<my_module>/assets/css
                else if ( file_exists(Yii::getAlias($this->getName() .'.assets.css') . DIRECTORY_SEPARATOR . $css_file) )
                {
                    $que_url = Yii::app()->assetManager->publish(Yii::getAlias($this->getName() .'.assets') , false, -1, YII_DEBUG);
                    Yii::app()->getClientScript()->registerCssLastFile($que_url .'/css/'. $css_file);
                }

                // #3 - CONTRIB MODULE -> Check JS file inside CORE module /app/core/src/modules/<my_module></assets/js
                else if ( ! Dz::isCoreModule($this->getName()) && file_exists($this->baseModulePath . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR .'css'. DIRECTORY_SEPARATOR . $css_file ) )
                {
                    $que_url = Yii::app()->assetManager->publish($this->baseModulePath . DIRECTORY_SEPARATOR . 'assets' , false, -1, YII_DEBUG);
                    Yii::app()->getClientScript()->registerCssLastFile($que_url .'/css/'. $css_file);
                }

                // #4 - CORE MODULE -> Check CSS file inside CORE module /app/core/src/modules/<my_module></assets/css
                else if ( Dz::isCoreModule($this->getName()) && file_exists(Yii::getAlias('@core.src.modules.'. $this->getName() .'.assets.css') . DIRECTORY_SEPARATOR . $css_file) )
                {
                    $que_url = Yii::app()->assetManager->publish(Yii::getAlias('@core.src.modules.'. $this->getName() .'.assets') , false, -1, YII_DEBUG);
                    Yii::app()->getClientScript()->registerCssLastFile($que_url .'/css/'. $css_file);
                }
            }
        }
    }


    /**
     * Load JS files
     */
    public function load_js()
    {
        if ( $this->jsFiles === null )
        {
            $this->jsFiles = [$this->getName() .'.js'];
        }
        if ( !empty($this->jsFiles) && is_array($this->jsFiles) )
        {
            foreach ( $this->jsFiles as $js_file )
            {
                // #1 - THEME --> Check JS file in <theme>/js/module directory
                $que_url = Yii::app()->theme->baseUrl;
                if ( file_exists(Yii::app()->theme->basePath . DIRECTORY_SEPARATOR .'js'. DIRECTORY_SEPARATOR .'modules'. DIRECTORY_SEPARATOR . $js_file) )
                {
                    Yii::app()->getClientScript()->registerScriptFile($que_url .'/js/modules/'. $js_file, \CClientScript::POS_END);
                }

                // #2 - CUSTOM MODULE --> Check JS file in /app/modules/<my_module>/assets/js
                else if ( file_exists(Yii::getAlias($this->getName() .'.assets.js') . DIRECTORY_SEPARATOR . $js_file) )
                {
                    $que_url = Yii::app()->assetManager->publish(Yii::getAlias($this->getName() .'.assets') , false, -1, YII_DEBUG);
                    Yii::app()->getClientScript()->registerScriptFile($que_url .'/js/'. $js_file, \CClientScript::POS_END);
                }

                // #3 - CONTRIB MODULE -> Check JS file inside CORE module /app/core/src/modules/<my_module></assets/js
                else if ( ! Dz::isCoreModule($this->getName()) && file_exists($this->baseModulePath . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR .'js'. DIRECTORY_SEPARATOR . $js_file) )
                {
                    $que_url = Yii::app()->assetManager->publish($this->baseModulePath . DIRECTORY_SEPARATOR . 'assets' , false, -1, YII_DEBUG);
                    Yii::app()->getClientScript()->registerScriptFile($que_url .'/js/'. $js_file, \CClientScript::POS_END);
                }

                // #4 - CORE MODULE -> Check JS file inside CORE module /app/core/src/modules/<my_module></assets/js
                else if ( Dz::isCoreModule($this->getName()) && file_exists(Yii::getAlias('@core.src.modules.'. $this->getName() .'.assets.js') . DIRECTORY_SEPARATOR . $js_file) )
                {
                    $que_url = Yii::app()->assetManager->publish(Yii::getAlias('@core.src.modules.'. $this->getName() .'.assets') , false, -1, YII_DEBUG);
                    Yii::app()->getClientScript()->registerScriptFile($que_url .'/js/'. $js_file, \CClientScript::POS_END);
                }
            }
        }
    }
}