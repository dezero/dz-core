<?php
/**
 * HttpRequest class file for Dz Framework
 * 
 * Port from Yii2 - yii\web\Request
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2019 Dezero
 */

namespace dz\web;
 
use Yii;

class HttpRequest extends \CHttpRequest
{
    /**
     * @var string the name of the POST parameter that is used to indicate if a request is a PUT, PATCH or DELETE
     * request tunneled through POST. Defaults to '_method'.
     * 
     * @source Yii2
     * 
     * @see getMethod()
     * @see getBodyParams()
     */
    public $methodParam = '_method';

    /**
     * @var string[] List of headers where proxies store the real client IP.
     * It's not advisable to put insecure headers here.
     * The match of header names is case-insensitive.
     * 
     * @source Yii2
     */
    public $ipHeaders = [
        'X-Forwarded-For', // Common
    ];

    private $_queryParams;

    private $_bodyParams;


    /**
     * @var string pathInfo with language key removed
     */
    protected $_cleanPathInfo;


    /**
     * @var Array of request headers.
     */
    private $_headers;


    /**
     * Returns GET parameter with a given name. If name isn't specified, returns an array of all GET parameters.
     *
     * @source Yii2
     *
     * @param string $name the parameter name
     * @param mixed $defaultValue the default parameter value if the parameter does not exist.
     * @return array|mixed
     */
    public function get($name = null, $defaultValue = null)
    {
        if ($name === null) {
            return $this->getQueryParams();
        }

        return $this->getQueryParam($name, $defaultValue);
    }


    /**
     * Returns POST parameter with a given name. If name isn't specified, returns an array of all POST parameters.
     *
     * @source Yii2
     * 
     * @param string $name the parameter name
     * @param mixed $defaultValue the default parameter value if the parameter does not exist.
     * @return array|mixed
     */
    public function post($name = null, $defaultValue = null)
    {
        if ($name === null) {
            return $this->getBodyParams();
        }

        return $this->getBodyParam($name, $defaultValue);
    }


    /**
     * Returns the request parameters given in the [[queryString]].
     *
     * This method will return the contents of `$_GET` if params where not explicitly set.
     * 
     * @source Yii2
     * 
     * @return array the request GET parameter values.
     * @see setQueryParams()
     */
    public function getQueryParams()
    {
        if ($this->_queryParams === null) {
            return $_GET;
        }

        return $this->_queryParams;
    }


    /**
     * Returns the named GET parameter value.
     * If the GET parameter does not exist, the second parameter passed to this method will be returned.
     * 
     * @source Yii2
     * 
     * @param string $name the GET parameter name.
     * @param mixed $defaultValue the default parameter value if the GET parameter does not exist.
     * @return mixed the GET parameter value
     * @see getBodyParam()
     */
    public function getQueryParam($name, $defaultValue = null)
    {
        $params = $this->getQueryParams();

        return isset($params[$name]) ? $params[$name] : $defaultValue;
    }


    /**
     * Returns the method of the current request (e.g. GET, POST, HEAD, PUT, PATCH, DELETE).
     * 
     * @source Yii2
     * 
     * @return string request method, such as GET, POST, HEAD, PUT, PATCH, DELETE.
     * The value returned is turned into upper case.
     */
    public function getMethod()
    {
        if (isset($_POST[$this->methodParam])) {
            return strtoupper($_POST[$this->methodParam]);
        }

        /*
        if ($this->headers->has('X-Http-Method-Override')) {
            return strtoupper($this->headers->get('X-Http-Method-Override'));
        }
        */

        if (isset($_SERVER['REQUEST_METHOD'])) {
            return strtoupper($_SERVER['REQUEST_METHOD']);
        }

        return 'GET';
    }


    /**
     * Returns the request parameters given in the request body.
     *
     * Request parameters are determined using the parsers configured in [[parsers]] property.
     * If no parsers are configured for the current [[contentType]] it uses the PHP function `mb_parse_str()`
     * to parse the [[rawBody|request body]].
     * 
     * @source Yii2
     * 
     * @return array the request parameters given in the request body.
     * @throws \yii\base\InvalidConfigException if a registered parser does not implement the [[RequestParserInterface]].
     * @see getMethod()
     * @see getBodyParam()
     * @see setBodyParams()
     */
    public function getBodyParams()
    {
        if ($this->_bodyParams === null) {
            if (isset($_POST[$this->methodParam])) {
                $this->_bodyParams = $_POST;
                unset($this->_bodyParams[$this->methodParam]);
                return $this->_bodyParams;
            }

            $rawContentType = $this->getContentType();
            if (($pos = strpos($rawContentType, ';')) !== false) {
                // e.g. text/html; charset=UTF-8
                $contentType = substr($rawContentType, 0, $pos);
            } else {
                $contentType = $rawContentType;
            }

            if ($this->getMethod() === 'POST') {
                // PHP has already parsed the body so we have all params in $_POST
                $this->_bodyParams = $_POST;
            } else {
                $this->_bodyParams = [];
                mb_parse_str($this->getRawBody(), $this->_bodyParams);
            }
        }

        return $this->_bodyParams;
    }


    /**
     * Returns the named request body parameter value.
     * If the parameter does not exist, the second parameter passed to this method will be returned.
     * 
     * @source Yii2
     * 
     * @param string $name the parameter name
     * @param mixed $defaultValue the default parameter value if the parameter does not exist.
     * @return mixed the parameter value
     * @see getBodyParams()
     * @see setBodyParams()
     */
    public function getBodyParam($name, $defaultValue = null)
    {
        $params = $this->getBodyParams();

        if (is_object($params)) {
            // unable to use `ArrayHelper::getValue()` due to different dots in key logic and lack of exception handling
            try {
                return $params->{$name};
            } catch (Exception $e) {
                return $defaultValue;
            }
        }

        return isset($params[$name]) ? $params[$name] : $defaultValue;
    }


    /**
     * Returns the user IP address.
     * The IP is determined using headers and / or `$_SERVER` variables.
     * @return string|null user IP address, null if not available
     */
    public function getUserIP()
    {
        foreach ( $this->ipHeaders as $ipHeader )
        {
            if ( isset($_SERVER[$ipHeader]) )
            {
                return trim(explode(',', $_SERVER[$ipHeader])[0]);
            }
        }

        return $this->getRemoteIP();
    }

    /**
     * Returns the IP on the other end of this connection.
     * This is always the next hop, any headers are ignored.
     * 
     * @source Yii2
     * 
     * @return string|null remote IP address, `null` if not available.
     */
    public function getRemoteIP()
    {
        return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;
    }


    /**
     * Returns the path info of the currently requested URL.
     * This refers to the part that is after the entry script and before the question mark.
     * The starting and ending slashes are stripped off.
     * 
     * @see https://github.com/mikehaertl/localeurls/blob/master/LocaleHttpRequest.php
     */
    public function getPathInfo()
    {
        // Just ONE language defined
        $vec_extra_languages = Yii::app()->i18n->get_extra_languages();
        if ( empty($vec_extra_languages) )
        {
            return parent::getPathInfo();
        }

        // Extra languages
        if ( $this->_cleanPathInfo === null )
        {
            $this->_cleanPathInfo = parent::getPathInfo();

            // Detected URL's starting with some extra language code?
            $pattern = implode('|', $vec_extra_languages);
            if ( preg_match("#^($pattern)\b(/?)#", $this->_cleanPathInfo, $m) )
            {
                $this->_cleanPathInfo = substr($this->_cleanPathInfo, strlen($m[1].$m[2]));
                $language = isset($vec_extra_languages[$m[1]]) ? $vec_extra_languages[$m[1]] : $m[1];
                Yii::app()->language = $language;

                /*
                if($this->persistLanguage) {
                    Yii::app()->user->setState(self::LANGUAGE_KEY, $language);
                    $cookies = $this->cookies;
                    if($this->languageCookieLifetime) {
                        $cookie = new CHttpCookie(self::LANGUAGE_KEY, $language);
                        $cookie->expire = time() + $this->languageCookieLifetime;
                        $cookies->add(self::LANGUAGE_KEY, $cookie);
                    }
                }

                if(!$this->redirectDefault && $language===$this->getDefaultLanguage()) {
                    $url            = $this->getBaseUrl().'/'.$this->_cleanPathInfo;
                    $queryString    = $this->getQueryString();
                    if(!empty($queryString))
                        $url .= '?'.$queryString;

                    $this->redirect($url);
                }
                */

            }
            /*
            else
            {
                $language = null;

                if($this->persistLanguage) {
                    $language = Yii::app()->user->getState(self::LANGUAGE_KEY);

                    if($language===null)
                        $language = $this->getCookies()->itemAt(self::LANGUAGE_KEY);
                }

                if($language===null && $this->detectLanguage) {
                    foreach($this->preferredLanguages as $preferred)
                        if(in_array($preferred, $this->languages)) {
                            $language = $preferred;
                            break;
                        }
                }

                if($language===null || $language===$this->_defaultLanguage) {
                    if(!$this->redirectDefault)
                        return $this->_cleanPathInfo;
                    else
                        $language = $this->_defaultLanguage;
                }

                $key = array_search($language, $this->languages);
                if ($key && is_string($key)) {
                    $language = $key;
                }

                if(($baseUrl = $this->getBaseUrl())==='') {
                    $this->redirect('/'.$language.$this->getRequestUri());
                } else {
                    $this->redirect(strtr($this->getRequestUri(), array($baseUrl => $baseUrl.'/'.$language)));
                }
            }
            */
        }

        return $this->_cleanPathInfo;
    }


    /**
     * Override CHttpRequest::redirect() method with 301 redirects
     */
    public function redirect($url, $terminate = true, $status_code = 302)
    {
        if ( strpos($url,'/') === 0 && strpos($url,'//') !== 0 )
        {
            $url = $this->getHostInfo() . $url;
        }
        
        if ( $status_code == 301 )
        {
            header("HTTP/1.1 301 Moved Permanently");
        }

        header('Location: '.$url, true, $status_code);
        if ( $terminate )
        {
            Yii::app()->end();
        }
    }


    /**
     * Returns User Agent information using the "PHP User Agent Parser" library
     * 
     * @see https://github.com/donatj/PhpUserAgent
     */
    public function getUserAgentInfo($user_agent = '')
    {
        if ( $user_agent === '' )
        {
            $user_agent = Yii::app()->request->getUserAgent();
        }

        if ( !empty($user_agent) )
        {
            $parser = Yii::createObject(\donatj\UserAgent\UserAgentParser::class);
            $agent_result = $parser->parse($user_agent);
            if ( $agent_result )
            {
                return $agent_result->platform() .' ' . $agent_result->browser() .' '. $agent_result->browserVersion();
            }
        }

        return $user_agent;
    }


    /**
     * Returns the header collection.
     * The header collection contains incoming HTTP headers.
     *
     * @source yii2
     */
    public function getHeaders()
    {
        if ($this->_headers === null) {
            if (function_exists('getallheaders')) {
                $this->_headers = getallheaders();
            } elseif (function_exists('http_get_request_headers')) {
                $this->_headers = http_get_request_headers();
            } else {
                // ['prefix' => length]
                $headerPrefixes = ['HTTP_' => 5, 'REDIRECT_HTTP_' => 14];

                foreach ($_SERVER as $name => $value) {
                    foreach ($headerPrefixes as $prefix => $length) {
                        if (strncmp($name, $prefix, $length) === 0) {
                            $name = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, $length)))));
                            $this->_headers[$name] = $value;
                            continue 2;
                        }
                    }
                }
            }
        }

        return $this->_headers;
    }
}
