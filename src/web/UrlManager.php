<?php
/**
 * UrlManager component class file
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2020 Dezero
 */

namespace dz\web;

use Yii;

/**
 * Custom UrlManager component
 */
class UrlManager extends \CUrlManager
{
    /**
     * Query parameter name that contains a language
     */
    public $languageParam = 'language';


    /**
     * Initializes the application component.
     */
    public function init()
    {
        parent::init();
    }


    /**
     * Constructs an URL.
     * 
     * @param string $route the controller and the action (e.g. article/read)
     * @param array $params list of GET parameters (name=>value). Both the name and value will be URL-encoded.
     * If the name is '#', the corresponding value will be treated as an anchor
     * and will be appended at the end of the URL.
     * @param string $ampersand the token separating name-value pairs in the URL. Defaults to '&'.
     * @return string the constructed URL
     */
    public function createUrl($route, $params = [], $ampersand = '&')
    {
        // Current language
        $language = Yii::app()->language;

        // Check if "language" parameter has been defined
        if ( isset($params[$this->languageParam]) )
        {
            $language = $params[$this->languageParam];
            unset($params[$this->languageParam]);
        }

        // Create URL as normal way
        $url = parent::createUrl($route, $params, $ampersand);
        if ( $language === Yii::defaultLanguage() || !in_array($language, Yii::app()->i18n->get_extra_languages()) )
        {
            // 25/01/2022 - Remove duplicate "//" in URL
            if ( preg_match("/^\/\//", $url) )
            {
                $url = substr($url, 1);
            }
            return $url;
        }

        // Add language into the URL
        $base_url = $this->getBaseUrl();
        if ( $base_url === '' )
        {
            return '/'. $language. $url;
        }

        return strtr($url, [
            $base_url => $base_url .'/'. $language
        ]);
    }
}
