<?php
/**
 * Controller class file for WEB applications
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2020 Dezero
 */

namespace dz\web;

use dz\db\ActiveRecord;
use dz\helpers\Json;
use dz\helpers\StringHelper;
use Yii;

/**
 * Controller is the base class for the generated controllers
 */
abstract class Controller extends \CController
{
	/**
	 * @var string. The layout for the controller view
	 */
	public $layout = '//layouts/page';


    /**
     * @string. View name
     */
    public $viewName;
	

	/**
	 * @var array. Context menu items. This property will be assigned to {@link CMenu::items}
	 */
	public $menu = [];
	
	
	/**
	 * @var string. CSS classes added to <body> tag
	 */
	public $bodyClass;


	/**
	 * @var array. Menu with action options (more options) for a model
	 */
	protected $_menu_action = [];


	/**
	 * DZ Framework render options
	 */
	public $render_view;
	public $render_data;

	
	/**
	 * Initializes the controller.
	 * This method is called by the application before the controller starts to execute
	 *
	 * @return void
	 */
	public function init()
	{
		// Jquery History
		Yii::app()->getClientScript()->registerCoreScript('history');

		parent::init();
	}
	

	/**
	 * Returns the filter configurations
	 *
	 * @return array
	 */
	public function filters()
	{
        $vec_filters = Yii::app()->config->get('components.filters');

        if ( empty($vec_filters) )
        {
    		return [
               ['dz\filters\AuthFilter'],
               // ['dz\filters\IpLocationFilter'],
    		];
        }

        return $vec_filters;
	}


    /**
     * @inherit doc
     */
    public function render($view, $data=null, $return=false)
    {
        $view = $this->absoluteView($view);

        return parent::render($view, $data, $return);
    }


    /**
     * @inherit doc
     */
    public function renderPartial($view, $data=null, $return=false, $processOutput=false)
    {
        $view = $this->absoluteView($view);

        return parent::renderPartial($view, $data, $return, $processOutput);
    }


    /**
     * Get the absolute view starting with double slashes //
     */
    public function absoluteView($view)
    {
        // Change from "<view>" to format "//<module>/<controller>/<view>" format
        if ( !preg_match("/\//", $view) )
        {
            $current_module = $this->currentModuleName(true);
            $current_controller = $this->currentControllerName(true);
            $view = $current_module .'/'. $current_controller .'/'. $view;
            $view = '//'. str_replace('//', '/', $view);
        }

        // Change from "<controller>/<view>" to format "//<module>/<controller>/<view>" format
        else if ( !preg_match("/\/\//", $view) )
        {
            $current_module = $this->currentModuleName(true);
            $view = $current_module .'/'. $view;
            $view = '//'. str_replace('//', '/', $view);
        }

        return $view;
    }


    /**
     * Finds a view file based on its name.
     * The view name can be in one of the following formats:
     * <ul>
     * <li>absolute view within a module: the view name starts with a single slash '/'.
     * In this case, the view will be searched for under the currently active module's view path.
     * If there is no active module, the view will be searched for under the application's view path.</li>
     * <li>absolute view within the application: the view name starts with double slashes '//'.
     * In this case, the view will be searched for under the application's view path.
     * This syntax has been available since version 1.1.3.</li>
     * <li>aliased view: the view name contains dots and refers to a path alias.
     * The view file is determined by calling {@link YiiBase::getPathOfAlias()}. Note that aliased views
     * cannot be themed because they can refer to a view file located at arbitrary places.</li>
     * <li>relative view: otherwise. Relative views will be searched for under the currently active
     * controller's view path.</li>
     * </ul>
     * For absolute view and relative view, the corresponding view file is a PHP file
     * whose name is the same as the view name. The file is located under a specified directory.
     * This method will call {@link CApplication::findLocalizedFile} to search for a localized file, if any.
     * @param string $viewName the view name
     * @param string $viewPath the directory that is used to search for a relative view name
     * @param string $basePath the directory that is used to search for an absolute view name under the application
     * @param string $moduleViewPath the directory that is used to search for an absolute view name under the current module.
     * If this is not set, the application base view path will be used.
     * @return mixed the view file path. False if the view file does not exist.
     */
    public function resolveViewFile($viewName, $viewPath, $basePath, $moduleViewPath = null)
    {
        if ( empty($viewName) )
        {
            return false;
        }

        if ( $moduleViewPath === null )
        {
            $moduleViewPath = $basePath;
        }

        $extension = '.php';
        if ( ($renderer = Yii::app()->getViewRenderer()) !== null )
        {
            $extension = $renderer->fileExtension;
        }

        if ( $viewName[0] === '/' )
        {
            if ( strncmp($viewName,'//',2) === 0 )
            {
                $viewFile = $basePath . $viewName;

                // <DEZERO> - Vendor modules
                if ( preg_match("/\/app\/vendor\//", $viewPath) )
                {
                    // Transform from "//commerce/order/index" to "/order/index"
                    $vec_view_name = explode("/", $viewName);
                    if ( count($vec_view_name) > 3 )
                    {
                        unset($vec_view_name[0]);
                        unset($vec_view_name[1]);
                        unset($vec_view_name[2]);
                        $viewFile = $moduleViewPath . "/". implode("/", $vec_view_name);
                    }
                }
                // <DEZERO> - Vendor modules
            }
            else
            {
                $viewFile = $moduleViewPath . $viewName;
            }
        }
        elseif ( strpos($viewName,'.') )
        {
            $viewFile = Yii::getPathOfAlias($viewName);
        }
        else
        {
            $viewFile = $viewPath . DIRECTORY_SEPARATOR . $viewName;
        }

        
        // <DEZERO> - Avoid double dashes
        // $viewFile = str_replace('//', '/', $viewFile);
        /*
        \DzLog::dev([
            'viewName' => $viewName,
            'viewPath' => $viewPath,
            'basePath' => $basePath,
            'moduleViewPath' => $moduleViewPath,
            'viewFile' => $viewFile
        ]);
        */

        if( is_file($viewFile . $extension) )
        {
            return Yii::app()->findLocalizedFile($viewFile . $extension);
        }
        elseif ( $extension !== '.php' && is_file($viewFile.'.php') )
        {
            return Yii::app()->findLocalizedFile($viewFile.'.php');
        }

        return false;
    }
	
	
	/**
	 * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
	 *
	 * array('<defined_action>' => '<operation_name_to_check>')
	 *
	 * @return array
	 */
	static public function checkAliasActions()
	{
		return [];
	}
	
	
	/**
	 * Evaluates a PHP expression or callback under the context of this component.
	 *
	 * Static version of http://www.yiiframework.com/doc/api/1.1/CComponent#evaluateExpression-detail
	 *
	 * @return bool
	 */
	static public function evalExpression($_expression_,$_data_=[])
	{
	    if ( is_string($_expression_) )
	    {
	        extract($_data_);
	        return eval('return '.$_expression_.';');
	    }
	    else
	    {
	        $_data_[] = $this;
	        return call_user_func_array($_expression_, $_data_);
	    }
	}


    /**
     * @inheritdoc
     */
    public function createWidget($className, $properties = [])
    {
        // Widget namespace conversion --> Transform from "dz.widgets.Upload" to "\dz\widgets\Upload"
        if ( preg_match("/^dz\./", $className) )
        {
            $className = '\\'. str_replace('.', '\\', $className);
        }

        return parent::createWidget($className, $properties);
    }
	
	
	// --------------------------------------------------------------
	// Methods from GxController parent class
	// --------------------------------------------------------------

	/**
	 * Returns the data model based on the primary key or another attribute.
	 * This method is designed to work with the values passed via GET.
	 * If the data model is not found or there's a malformed key, an
	 * HTTP exception will be raised.
	 * 
	 * This method is based on the gii generated method controller::loadModel, from version 1.1.7 (r3135). Changes:
	 *   + Support to composite PK.
	 *	 + Support to use any attribute (column) name besides the PK.
	 * 	 + Support to multiple attributes.
	 * 	 + Automatically detects the PK column names.
	 *
	 */
	public function loadModel($key, $modelClass, $scenario = '')
	{
        // 02/09/2020 - Load class namespace?
        $modelClass = Yii::app()->classMap->get($modelClass);

		// Get the static model.
		$staticModel = ActiveRecord::model($modelClass);
		if ( !empty($scenario) )
		{
			$staticModel->scenario = $scenario;
		}

		// The key is an array. Check if there are column names indexing the values in the array.
		if ( is_array($key) )
		{
			reset($key);
			
			// There are no attribute names. Check if there are multiple PK values. 
			if (key($key) === 0)
			{
				// If there's only one, start again using only the value.
				if (count($key) === 1)
				{
					return $this->loadModel($key[0], $modelClass, $scenario);
				}

				// Now we will use the composite PK. Check if the table has composite PK.
				$tablePk = $staticModel->getTableSchema()->primaryKey;
				if ( !is_array($tablePk) )
				{
					throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
				}

				// Check if there are the correct number of keys.
				if ( count($key) !== count($tablePk) )
				{
					throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
				}

				// Get an array of PK values indexed by the column names.
				$pk = $staticModel->fillPkColumnNames($key);

				// Then load the model.
				$model = $staticModel->findByPk($pk);
			}
			else
			{
				// There are attribute names. Then we load the model now.
				$model = $staticModel->findByAttributes($key);
			}
		}
		else
		{
			// The key is not an array. Check if the table has composite PK.
			$tablePk = $staticModel->getTableSchema()->primaryKey;
			if ( is_array($tablePk) )
			{
				// The table has a composite PK. The key must be a string to have a PK separator.
				if ( !is_string($key) )
				{
					throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
				}

				// There must be a PK separator in the key.
				if ( stripos($key, ActiveRecord::$pkSeparator) === FALSE )
				{
					throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
				}

				// Generate an array, splitting by the separator.
				$keyValues = explode(ActiveRecord::$pkSeparator, $key);

				// Start again using the array.
				return $this->loadModel($keyValues, $modelClass, $scenario);
			}
			else
			{
				// The table has a single PK. Then we load the model now.
				$model = $staticModel->findByPk($key);
			}
		}

		// Check if we have a model.
		if ( $model === null )
		{
			throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
		}

		return $model;
	}


	/**
	 * Performs the AJAX validation
	 *
	 * This method is based on the gii generated method controller::performAjaxValidation, from version 1.1.7 (r3135). Changes:
	 *	+ Supports multiple models.
	 *
	 */
	protected function performAjaxValidation($model, $form = null)
	{
		return parent::performAjaxValidation($model, $form);
	}


	/**
	 * Finds the related primary keys specified in the form POST.
	 * Only for HAS_MANY and MANY_MANY relations.
	 *
	 * @param array $form 			The post data.
	 * @param array $relations 		A list of model relations in the format returned by {@link CActiveRecord::relations}.
	 * @param string $uncheckValue 	Since Yii 1.1.7, htmlOptions (in {@link CHtml::activeCheckBoxList})
	 * 								has an option named 'uncheckValue'. If you set it to different values than the default value (''), you will
	 * 								need to set the appropriate value to this argument. This method can't be used when 'uncheckValue' is null.
	 * @return array 				An array where the keys are the relation names (string) and the values are arrays with the related model primary keys (int|string) or composite primary keys (array with pk name (string) => pk value (int|string)).
	 *
	 */
	protected function getRelatedData($form, $relations, $uncheckValue = '')
	{
		return parent::getRelatedData($form, $relations, $uncheckValue);
	}


	/*
	|--------------------------------------------------------------------------
	| CUSTOM FUNCTIONS
	|--------------------------------------------------------------------------
	*/


    /**
     * Returns error message for AJAX and non-AJAX request
     *
     * @param array 	Error array from ActiveRecord::getErrors()
     * @param bool 		Return (TRUE) or display (FALSE) results. Display (FALSE) by default	
     * 
     * @return string
     */
    public function showErrors($vec_errors, $is_return = false)
    {
		if ( !empty($vec_errors) )
		{
			// Return result
			if ( $is_return )
			{
				$output_errors = [];
				foreach ( $vec_errors as $que_error )
				{
					if ( is_array($que_error) )
					{
						foreach ( $que_error as $que_error_message )
						{
							$output_errors[] = $que_error_message;
						}
					}
					else
					{
						$output_errors[] = $que_error;
					}
				}
				return $output_errors;
			}
			else
			{
				// AJAX response
				if ( Yii::app()->getRequest()->getIsAjaxRequest() )
				{
					$output_errors = [];
					foreach ( $vec_errors as $que_error )
					{
						if ( is_array($que_error) )
						{
							foreach ( $que_error as $que_error_message )
							{
								$output_errors[] = $que_error_message;
							}
						}
						else
						{
							$output_errors[] = $que_error;
						}
					}
					echo Json::encode($output_errors);
					Yii::app()->end();
				}

				// Non-AJAX response
				foreach ( $vec_errors as $que_error )
				{
					Yii::app()->user->addFlash('error', $que_error);
				}
			}
		}
    }


    /**
	 * Save a log message
	 */
    public function log($log_message, $category = 'dev')
    {
    	if ( is_array($log_message) )
    	{
    		$log_message = Json::encode($log_message);
    	}
    	Yii::log($log_message, "profile", $category);
    	
        return true;
    }


    /**
     * Get current controller name
     */
    public function currentControllerName($is_lowercase = false)
    {
        $controller_name = $this->getId();
        
        // Lowercase?
        if ( $is_lowercase )
        {
            $controller_name = StringHelper::strtolower($controller_name);
        }

        return $controller_name;
    }


    /**
     * Alias of Controller::currentControllerName()
     */
    public function currentController($is_lowercase = false)
    {
        return $this->currentControllerName($is_lowercase);
    }


    /**
     * Get current action name
     */
    public function currentActionName($is_lowercase = false)
    {
        $action_name = $this->getAction()->getId();

        // Lowercase?
        if ( $is_lowercase )
        {
            $action_name = StringHelper::strtolower($action_name);
        }

        return $action_name;
    }


    /**
     * Alias of Controller::currentActionName()
     */
    public function currentAction($is_lowercase = false)
    {
        return $this->currentActionName($is_lowercase);
    }


    /**
     * Get current module
     */
    public function currentModuleName($is_lowercase = false)
    {
        if ( isset($this->module) )
        {
            $module_name = $this->module->id;

            // Lowercase?
            if ( $is_lowercase )
            {
                $module_name = StringHelper::strtolower($module_name);
            }

            return $module_name;
        }

        return '';
    }


    /**
     * Alias of Controller::currentModuleName()
     */
    public function currentModule($is_lowercase = false)
    {
        return $this->currentModuleName($is_lowercase);
    }



    /*
	|--------------------------------------------------------------------------
	| BACKBONE FUNCTIONS from YiiBackbone extension
	|
	| @see https://github.com/clevertech/YiiBackbone
	|--------------------------------------------------------------------------
	*/


	/**
	 * Gets RestFul data and decodes its JSON request
	 * @return mixed
	 */
	public function jsonInput()
	{
		return Json::decode(file_get_contents('php://input'));
	}


    /**
	 * Send raw HTTP response
	 * 
	 * @param int $status HTTP status code
	 * @param string $body The body of the HTTP response
	 * @param string $contentType Header content-type
	 * @return HTTP response
	 */
	public function jsonOutput($status = 200, $body = '', $contentType = 'application/json')
	{
		// Set the status
		$statusHeader = 'HTTP/1.1 ' . $status . ' ' . $this->get_status_code_message($status);
		header($statusHeader);
		
		// Set the content type
		header('Content-type: ' . $contentType);
		echo $body;
		
		Yii::app()->end();
	}


    
    /**
     * Custom 301 redirect
     */
    public function redirect_301($url, $terminate = true)
    {
        return $this->redirect($url, $terminate, 301);
    }


	/**
	 * Return the http status message based on integer status code
	 * @param int $status HTTP status code
	 * @return string status message
	 */
	protected function get_status_code_message($status)
	{
	    $codes = [
			100 => 'Continue',
			101 => 'Switching Protocols',
			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative Information',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Found',
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',
			306 => '(Unused)',
			307 => 'Temporary Redirect',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Timeout',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested Range Not Satisfiable',
			417 => 'Expectation Failed',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway',
			503 => 'Service Unavailable',
			504 => 'Gateway Timeout',
			505 => 'HTTP Version Not Supported',
	    ];
	    return (isset($codes[$status])) ? $codes[$status] : '';
	}
}
