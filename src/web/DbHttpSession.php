<?php
/**
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2019 Dezero
 */

namespace dz\web;

use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use Yii;

/**
 * DbHttpSession class file for Dz Framework
 * 
 * Some ideas takend from Yii2 - yii\web\Session
 * 
 * @see https://www.yiiframework.com/wiki/687/check-online-users-set-specific-user-offline
 */
class DbHttpSession extends \CDbHttpSession
{
    /**
     * @inheritdoc
     */
    public function getDbConnection()
    {
        return parent::getDbConnection();
    }


    /**
     * Custom event triggered after authenticate process has been made successfully
     */
    public function afterAuthenticate($user_id)
    {
        $db = $this->getDbConnection();
        $db->setActive(true);
        $db->createCommand()->update(
                $this->sessionTableName,
                [
                    'user_id'       => $user_id,
                    'created_date'  => time(),
                    'uuid'          => StringHelper::UUID()
                ],
                'id=:id', [':id' => session_id()]
        );

        return TRUE;
    }


    /**
     * @inheritdoc
     */
    public function regenerateID($deleteOldSession = false)
    {
        $oldID = session_id();

        // if no session is started, there is nothing to regenerate
        if( empty($oldID) )
        {
            return;
        }

        \CHttpSession::regenerateID(false);
        $newID = session_id();
        $db = $this->getDbConnection();

        $row = $db->createCommand()
            ->select()
            ->from($this->sessionTableName)
            ->where('id=:id',array(':id'=>$oldID))
            ->queryRow();

        if ( $row !== false )
        {
            if ( $deleteOldSession )
            {
                $db->createCommand()->update($this->sessionTableName, [
                    'id' => $newID
                ],'id = :oldID', [':oldID'=>$oldID]);
            }
            else
            {
                $row['id'] = $newID;
                $row['created_date'] = time();
                $db->createCommand()->insert($this->sessionTableName, $row);
            }
        }
        else
        {
            // shouldn't reach here normally
            $db->createCommand()->insert($this->sessionTableName, [
                'id' => $newID,
                'expire' => time()+$this->getTimeout(),
                'data' => '',
                'created_date' => time()
            ]);
        }
    }


    /**
     * @inheritdoc
     */
    public function writeSession($id,$data)
    {
        // exception must be caught in session write handler
        // http://us.php.net/manual/en/function.session-set-save-handler.php
        try
        {
            $expire = time() + $this->getTimeout();
            $db = $this->getDbConnection();

            if ( $db->getDriverName()=='pgsql' )
            {
                $data = new \CDbExpression($db->quoteValueWithType($data, \PDO::PARAM_LOB)."::bytea");
            }
            
            if ( $db->getDriverName()=='sqlsrv' || $db->getDriverName()=='mssql' || $db->getDriverName()=='dblib' )
            {
                $data = new \CDbExpression('CONVERT(VARBINARY(MAX), '.$db->quoteValue($data).')');
            }

            if ( $db->createCommand()->select('id')->from($this->sessionTableName)->where('id = :id',[':id' => $id])->queryScalar() === false )
            {
                $db->createCommand()->insert($this->sessionTableName, [
                    'id' => $id,
                    'data' => $data,
                    'expire' => $expire,
                    'created_date' => time()
                ]);
            }
            else
            {
                $db->createCommand()->update($this->sessionTableName, [
                    'data' => $data,
                    'expire' => $expire
                ],'id = :id', [':id'=>$id]);
            }
        }
        catch ( \Exception $e )
        {
            if ( YII_DEBUG )
            {
                echo $e->getMessage();
            }

            return false;
        }

        return true;
    }


    /**
     * Adds a variables session
     * 
     * Alias from CHttpSession::add() method
     * 
     * @see CHttpSession::add()
     */
    public function set($key, $value)
    {
        return $this->add($key, $value);
    }


    /**
     * Check if a variable name exists
     * 
     * Alias from CHttpSession::add() method
     * 
     * @see CHttpSession::contains()
     */
    public function has($key)
    {
        return isset($_SESSION[$key]);
    }



    /**
     * Removes all session variables.
     */
    public function removeAll()
    {
        foreach ( array_keys($_SESSION) as $key )
        {
            unset($_SESSION[$key]);
        }
    }
}
