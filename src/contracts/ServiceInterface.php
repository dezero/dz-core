<?php
/**
 * Service contract for use cases
 */

namespace dz\contracts;

interface ServiceInterface
{
    /**
     * @return bool
     */
    public function run();
}
