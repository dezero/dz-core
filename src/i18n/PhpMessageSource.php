<?php
/**
 * PhpMessageSource class file
 * 
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2020 Dezero
 */

namespace dz\i18n;

use Yii;

class PhpMessageSource extends \CPhpMessageSource
{
    /**
     * @var string the base path for all translated messages
     */
    public $basePath;


    /**
     * Initializes the application component.
     */
    public function init()
    {
        // Base path
        $this->basePath = empty($this->basePath) ? Yii::getAlias('@core.messages') : $this->basePath;

        return parent::init();
    }

    /**
     * Loads the message translation for the specified language and category.
     */
    public function load_messages($category, $language)
    {
        return $this->loadMessages($category, $language);
    }
}