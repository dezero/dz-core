<?php
/**
 * DbMessageSource class file
 * 
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2020 Dezero
 */

namespace dz\i18n;

use Yii;

class DbMessageSource extends \CDbMessageSource
{
    /**
     * @var string the name of the source message table. Defaults to 'translation_source'.
     */
    public $sourceMessageTable = 'translation_source';


    /**
     * @var string the name of the translated message table. Defaults to 'translation_message'.
     */
    public $translatedMessageTable = 'translation_message';


    /**
     * Loads the message translation for the specified language and category.
     */
    public function load_messages($category, $language)
    {
        return $this->loadMessages($category, $language);
    }


    /**
     * Loads the messages from database.
     * You may override this method to customize the message storage in the database.
     * 
     * @param string $category the message category
     * @param string $language the target language
     * @return array the messages loaded from database
     * @since 1.1.5
     */
    protected function loadMessagesFromDb($category, $language)
    {
        $command = $this->getDbConnection()
            ->createCommand()
            ->select("t1.message AS message, t2.translation AS translation")
            ->from(["{$this->sourceMessageTable} t1", "{$this->translatedMessageTable} t2"])
            ->where('t1.id=t2.id AND t1.category=:category AND t2.language_id=:language', [':category'=>$category,':language'=>$language])
        ;
        
        $vec_messages = [];
        foreach ( $command->queryAll() as $row )
        {
            $vec_messages[$row['message']] = $row['translation'];
        }

        return $vec_messages;
    }
}