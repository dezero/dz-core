<?php
/**
 * I18N provides features related with internationalization (I18N) and localization (L10N).
 */

namespace dz\i18n;

use dz\base\ApplicationComponent;
use dz\db\DbCriteria;
use dz\modules\settings\models\Language;
use Yii;

class I18N extends ApplicationComponent
{
    /**
     * @var array Configuration options
     */
    private $_vec_config = null;


    /**
     * @var array Language list (used as cache)
     */
    private $_vec_languages = [];


    /**
     * Init function
     */
    public function init()
    {
        // Load full configuration
        $this->get_config();

        parent::init();
    }


    /**
     * Load and returns i18n configuration
     */
    public function get_config($config_option = '')
    {
        if ( empty($this->_vec_config) )
        {
            $this->_vec_config = Yii::app()->config->get('components.i18n');
        }

        // Default value
        if ( empty($this->_vec_config) )
        {
            $this->_vec_config = [
                // Source from i18n configuration is loaded: 'file' (default) or 'db'
                'source_type' => 'file',

                // Default Language
                'default_language' => 'en',

                // Extra supported languages
                'extra_languages' => [],

                // Translations settings
                'translations' => [
                    // Translations categories
                    'categories' => [
                        'app'       => 'App (global)',
                        'frontend'  => 'Frontend',
                        'backend'   => 'Backend'
                    ],

                    // Default category for translations management pages
                    'default_category'  => 'app'
                ]
            ];
        }

        // Source type validation. Allowed values: 'file' (default) or 'db'
        if ( !isset($this->_vec_config['source_type']) || ( $this->_vec_config['source_type'] !== 'file' && $this->_vec_config['source_type'] !== 'db') )
        {
            $this->_vec_config['source_type'] = 'file';
        }

        // Return an specific config option
        if ( !empty($config_option) && isset($this->_vec_config[$config_option]) )
        {
            return $this->_vec_config[$config_option];
        }

        // Return full config options
        return $this->_vec_config;
    }


    /**
     * Returns source type
     */
    public function get_source_type()
    {
        return $this->get_config('source_type');
    }


    /**
     * Check if the language configurations is loaded from database
     */
    public function is_source_db()
    {
        $source_type = $this->get_source_type();

        return $source_type === 'db';
    }


    /**
     * Check if the language configurations is loaded from the
     * configuration file saved on /app/config/components/components/i18n.php
     */
    public function is_source_file()
    {
        $source_type = $this->get_source_type();

        return $source_type === 'file';
    }


    /**
     * Return current language
     */
    public function get_current_language($is_return_model = false)
    {
        if ( ! $is_return_model )
        {
            return Yii::app()->language;
        }

        // Return Language model
        return Language::findOne(Yii::app()->language);
    }


    /**
     * Return the default language
     */
    public function get_default_language($is_return_model = false)
    {
        if ( ! $is_return_model )
        {
            // Configuration loaded from database
            if ( $this->is_source_db() )
            {
                return Yii::app()->db->createCommand()
                    ->select('language_id')
                    ->from(Language::model()->tableName())
                    ->where('is_default = 1 AND (disable_date IS NULL OR disable_date = 0)')
                    ->queryScalar();
            }

            // Configuration loaded from configuration file
            return $this->get_config('default_language');
        }

        // Return Language model
        return Language::get()
            ->where(['is_default' => 1])
            ->andWhere('t.disable_date IS NULL OR t.disable_date = 0')
            ->one();
    }


    /**
     * Return extra language(s)
     */
    public function get_extra_languages($is_return_model = false)
    {
        if ( ! $is_return_model )
        {
            // Configuration loaded from database
            if ( $this->is_source_db() )
            {
                return Yii::app()->db->createCommand()
                    ->select('language_id')
                    ->from(Language::model()->tableName())
                    ->where('is_default = 0 AND (disable_date IS NULL OR disable_date = 0)')
                    ->order('weight ASC')
                    ->queryColumn();
            }

            return $this->get_config('extra_languages');
        }

        // Return Language model
        return Language::get()
            ->where(['is_default' => 0])
            ->andWhere('t.disable_date IS NULL OR t.disable_date = 0')
            ->all();
    }


    /**
     * Return all ENABLED language(s)
     */
    public function get_all_languages($is_return_model = false, $is_include_disabled = false)
    {
        $search_type = $is_include_disabled ? 'all' : 'enabled';

        $vec_options = [
            'is_return_model' => $is_return_model
        ];

        return $this->language_list($search_type, $vec_options);
    }


    /**
     * Check if the application has been configured as multilanguage
     */
    public function is_multilanguage()
    {
        return !empty($this->get_extra_languages());
    }


    /**
     * Check if language exists and it's enabled
     */
    public function is_enabled_language($language_id)
    {
        // Default language
        if ( $language_id === $this->get_default_language() )
        {
            return true;
        }

        $vec_extra_languages = $this->get_extra_languages();
        if ( in_array($language_id, $vec_extra_languages ) )
        {
            $language_model = Language::findOne($language_id);
            return $language_model && $language_model->is_enabled();
        }

        return false;
    }


    /**
     * Return translation categories used on Yii::t(category, text) method
     * 
     * This translation categories are defined on "i18n.php" config component file
     */
    public function get_translation_categories()
    {
        if ( !empty($this->_vec_config) && isset($this->_vec_config['translations']) && isset($this->_vec_config['translations']['categories']) )
        {
            return $this->_vec_config['translations']['categories'];
        }

        return [];
    }


    /**
     * Return the default translation category
     */
    public function get_default_translation_category()
    {
        if ( !empty($this->_vec_config) && isset($this->_vec_config['translations']) && isset($this->_vec_config['translations']['default_category']) )
        {
            return $this->_vec_config['translations']['default_category'];
        }

        return 'app';
    }


    /**
     * Get languages
     */
    public function language_list($search_type = 'enabled', $vec_options = [])
    {
        // Return Yii models objects or category names?
        $is_return_model = isset($vec_options['is_return_model']) ? $vec_options['is_return_model'] : false;

        // Order list
        $criteria_order = isset($vec_options['order']) ? $vec_options['order'] : 't.weight ASC';

        //--> SEARCH LANGUAGES WITH SELECTED CRITERIA
        $criteria = new DbCriteria;
        $criteria->order = $criteria_order;

        // Disabled/enabled
        if ( $search_type === 'disabled' )
        {
            $criteria->addCondition('t.disable_date IS NOT NULL AND t.disable_date > 0');
        }
        else if ( $search_type !== 'all' )
        {
            $criteria->addCondition('t.disable_date IS NULL OR t.disable_date = 0');
        }

        // Default language?
        if ( $search_type === 'default' )
        {
            $criteria->compare('t.is_default', 1);
        }

        // Extra language?
        else if ( $search_type === 'extras' || $search_type === 'extra' )
        {
            $criteria->compare('t.is_default', 0);   
        }

        // Get all Language models
        $vec_language_models = Language::model()->findAll($criteria);

        // Return models
        if ( $is_return_model )
        {
            return $vec_language_models;
        }

        // Return an associative array indexed by key
        $vec_language_list = [];
        if ( !empty($vec_language_models) )
        {
            foreach ( $vec_language_models as $language_model )
            {
                $vec_language_list[$language_model->language_id] = $language_model->native;
            }
        }

        return $vec_language_list;
    }


    /**
     * Returns language name
     * 
     * Usage:
     *   - Yii::app()->i18n->language_name('es')
     *   - Yii::app()->i18n->language_name('default')
     *   - Yii::app()->i18n->language_name('current')
     */
    public function language_name($language_id)
    {
        // Get all Language list
        if ( empty($this->_vec_languages) )
        {
            $this->_vec_languages = $this->language_list('all');
        }

        // Return default language name
        if ( $language_id === 'default' )
        {
            $language_id = $this->get_default_language();
        }

        // Return current language name
        if ( $language_id === 'current' )
        {
            $language_id = $this->get_current_language();
        }

        // Check Language name
        if ( !empty($this->_vec_languages) && isset($this->_vec_languages[$language_id]) )
        {
            return $this->_vec_languages[$language_id];
        }

        return '';
    }
}
