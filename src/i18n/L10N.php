<?php
/**
 * Locale information - Localization (L10N)
 */

namespace dz\i18n;

use dz\base\ApplicationComponent;
use dz\db\DbCriteria;
use dz\modules\settings\models\Language;
use Yii;

class L10N extends ApplicationComponent
{
    /**
     * Current language
     */
    private $_current_language;


    /**
     * CLocale
     */
    private $_locale;


    /**
     * Init function
     */
    public function init()
    {
        // Current language
        $this->_current_language = Yii::app()->i18n->get_current_language();

        // Locale
        $this->_locale = Yii::app()->getLocale($this->_current_language);

        parent::init();
    }


    /**
     * Return the month name given a month number between 1 and 12
     * 
     * @see CLocale::getMonthName()
     */
    public function get_month($num_month, $type = 'wide')
    {
        return $this->_locale->getMonthName($num_month, $type, true);
    }


    /**
     * Return the full month list
     * 
     * @see CLocale::getMonthNames()
     */
    public function get_months($type = 'wide')
    {
        return $this->_locale->getMonthNames($type, true);
    }
}