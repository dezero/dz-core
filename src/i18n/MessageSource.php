<?php
/**
 * MessageSource class file
 * 
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2020 Dezero
 */

namespace dz\i18n;

use Yii;

class MessageSource extends \CMessageSource
{
    /**
     * @var array Configuration for message translation using the database
     */
    public $dbMessages = [];


    /**
     * @var array Configuration fore message translation using PHP files
     */
    public $phpMessages = [];



    /**
     * Initializes the application component.
     */
    public function init()
    {
        // Init the components
        $vec_components = [];

        // Message translation using database --> Yii::app()->dbMessages
        if ( ! empty($this->dbMessages) )
        {
            $vec_default_config = ['class' => '\dz\i18n\DbMessageSource'];
            $vec_components['dbMessages'] = \CMap::mergeArray($vec_default_config, $this->dbMessages);
        }

        // Message translation using PHP files --> Yii::app()->phpMessages
        if ( ! empty($this->phpMessages) )
        {
            $vec_default_config = ['class' => '\dz\i18n\PhpMessageSource'];
            $vec_components['phpMessages'] = \CMap::mergeArray($vec_default_config, $this->phpMessages);
        }

        // Register components dynamically
        Yii::app()->setComponents($vec_components);
        
        return parent::init();
    }


    /**
     * Loads the message translation for the specified language and category.
     * @param string $category the message category
     * @param string $language the target language
     * @return array the loaded messages
     */
    protected function loadMessages($category, $language)
    {
        $vec_messages = [];

        // Load messages from database
        if ( ! empty($this->dbMessages) )
        {
            $vec_messages = Yii::app()->dbMessages->load_messages($category, $language);
        }

        // Load messages from PHP files
        if ( ! empty($this->phpMessages) )
        {
            $vec_messages = \CMap::mergeArray(Yii::app()->phpMessages->load_messages($category, $language), $vec_messages);
        }

        return $vec_messages;
    }
}