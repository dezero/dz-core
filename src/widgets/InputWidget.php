<?php
/**
 * Input widget class file
 *
 * This extension is used for rendering inputs according to Bootstrap standards.
 * Based on TbInput class file from Yii Bootstrap extension
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2019 Dezero
 */

namespace dz\widgets;

use dz\helpers\Html;
use Yii;

class InputWidget extends \CInputWidget
{
    /**
     * File path to assets
     *
     * @var string
     */
    protected $assetsPath;


    /**
     * URL to assets
     *
     * @var string
     */
    protected $assetsUrl;
    
    
    /**
     * HTML attributes for the widget
     * 
     * @var array
     */
    public $htmlOptions = array();

    
    /**
     * Default options
     * 
     * @var array
     */ 
    // private $_defaultOptions = array();


    /**
     * Text to prepend
     *
     * @var string
     */
    public $prepend = '';
    
    
    /**
     * Text to append
     *
     * @var string
     */
    public $append = '';    

    
    /**
     * Returns the prepend element for the input
     *
     * @return string the element
     */
    protected function getPrepend()
    {
        if ( $this->hasAddOn() )
        {
            ob_start();
            echo '<div class="' . $this->getAddonCssClass() . '">';
            if ( !empty($this->prepend) )
            {
                echo Html::tag('span', array('class' => 'add-on'), $this->prepend);
            }
            return ob_get_clean();
        }
        return '';
    }

    /**
     * Returns the append element for the input.
     *
     * @return string the element
     */
    protected function getAppend()
    {
        if ( $this->hasAddOn() )
        {
            ob_start();
            if ( !empty($this->append) )
            {
                echo Html::tag('span', array('class' => 'add-on'), $this->append);
            }
            echo '</div>';
            return ob_get_clean();
        }
        return '';
    }
    
    
    /**
     * Returns the input container CSS classes.
     *
     * @return string the CSS class
     */
    protected function getAddonCssClass()
    {
        $vec_classes = array();
        if ( !empty($this->prepend) )
        {
            $vec_classes[] = 'input-prepend';
        }
        if ( !empty($this->append) )
        {
            $vec_classes[] = 'input-append';
        }

        return implode(' ', $vec_classes);
    }


    /**
     * Returns whether the input has an add-on (prepend and/or append)
     *
     * @return boolean
     */
    protected function hasAddOn()
    {
        return !empty($this->prepend) || !empty($this->append);
    }
}