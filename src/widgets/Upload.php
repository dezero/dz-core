<?php
/**
 * Upload widget class file
 *
 * This extension is a wrapper for Jasny File Upload plugin (http://jasny.github.com/bootstrap/javascript.html#fileinput)
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2019 Dezero
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */

namespace dz\widgets;

use CClientScript;
use CJavaScript;
use dz\helpers\Html;
use dz\widgets\InputWidget;
use Yii;


/**
 * Upload widget
 * @see http://jasny.github.com/bootstrap/javascript.html#fileinput
 */
class Upload extends InputWidget
{
    /**
     *  @var bool. View mode?
     */
    public $view_mode = false;
    

    /**
     * @var bool. It is the file type an image?
     */
    public $is_image = true;

    
    /**
     * @var string. Fallback image (don't exist an image)
     */
    public $fallback_image = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image';


    /**
     * @var string. Download URL (optional)
     */
    public $download_url;

    /**
     * @var array. Jasny File Upload plugin options
     */
    public $options = [];


    /**
     * @var array. Jasny File Upload event handlers
     */
    public $events = [];


    /**
     * @var string. File id field
     */
    public $file_id_field = 'file_id';


    /**
     * @var string. File name field
     */
    public $file_name_field = 'file_name';


    /**
     * @var string. File path field
     */
    public $file_path_field = 'file_path';


    /**
     * Classes for buttons
     */
    public $vec_classes = [
        'new'       => 'btn-default btn-outline',
        'remove'    => 'btn-default btn-outline',
        'select'    => 'btn-default btn-outline',
    ];
    
    /**
     * Title for buttons
     */
    public $vec_labels = [
        'new'       => 'Add file...',
        'remove'    => 'Remove',
        'change'    => 'Change',
        'select'    => 'Select image',
    ];
    
    
    /**
     * Initializes the widget
     */
    public function init()
    {
        // Assets
        if ( $this->assetsPath === null )
        {
            $this->assetsPath = dirname(__FILE__) . DIRECTORY_SEPARATOR .'assets'. DIRECTORY_SEPARATOR . 'upload';
        }
        
        if ( $this->assetsUrl === null )
        {
            $this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
        }
        
        // HTML Options
        list($this->name, $this->id) = $this->resolveNameID();
        if ( !isset($this->htmlOptions['id']) )
        {
            $this->htmlOptions['id'] = $this->id .'-fileinput';
        }

        // Generate "name" attribute
        if ( !isset($this->options['name']) || empty($this->options['name']) )
        {
            $this->options['name'] = $this->name;
        }
        else
        {
            $this->name = $this->options['name'];
        }

        // Dezero custom fallback image
        $this->fallback_image = Yii::app()->request->baseUrl . '/themes/backend/images/no-image.gif';
    }
    

    /**
     * Runs the widget
     */
    public function run()
    {
        // Classes
        if ( !isset($this->htmlOptions['class']) )
        {
            $this->htmlOptions['class'] = 'dz-fileinput fileinput';
        }
        else
        {
            $this->htmlOptions['class'] .= ' dz-fileinput fileinput';
        }

        if ( $this->is_image )
        {
            $this->htmlOptions['class'] .= ' dz-image col-sm-12';
        }
        else
        {
            $this->htmlOptions['class'] .= ' dz-file';
        }

        // Special classes -> "fileinput-new" y "fileinput-extists"
        $upload_value = '';
        if ( $this->hasModel() )
        {
            $upload_value = Html::resolveValue($this->model, $this->attribute);
        }
        else
        {
            $upload_value = $this->value;
        }

        // Existing uploaded value
        $file_model = null;
        if ( !empty($upload_value) )
        {
            // $file_model = File::model()->findByPk($upload_value);
            $file_model = $this->model;
            if ( $file_model !== null )
            {
                $this->htmlOptions['class'] .= ' fileinput-exists';
                $this->htmlOptions['name'] = 'File';
                // $this->htmlOptions['name'] = 'File['. $this->id .']['. $file_model->file_id .']';
            }
        }

        if ( $file_model === null )
        {
            $this->htmlOptions['class'] .= ' fileinput-new';
            $this->htmlOptions['name'] = 'File[new]['. $this->id .']';
        }

        $this->htmlOptions['data-provides'] = 'fileinput';
        echo Html::openTag('div', $this->htmlOptions);

        // File path
        if ( !empty($file_model) )
        {
            $file_path = Yii::app()->request->baseUrl . DIRECTORY_SEPARATOR  . $file_model->{$this->file_path_field};
            if ( method_exists($file_model, 'remove_webroot_path') )
            {
                $file_path = $file_model->remove_webroot_path($file_path);
            }
        }

        // IMAGE FILE TYPE
        if ( $this->is_image )
        {
            echo '<div class="fileinput-image-wrapper">';

            // Fallback image
            echo '<div class="fileinput-new thumbnail"><img src="'. $this->fallback_image .'" /></div>';

            // Current image
            echo '<div class="fileinput-preview fileinput-exists thumbnail">';
            if ( !empty($file_model) )
            {
                echo '<img src="'. $file_path;
                if ( ! preg_match("/\/$/", $file_path ) )
                {
                    echo DIRECTORY_SEPARATOR;
                }
                echo $file_model->{$this->file_name_field} .'" class="fileinput-image">';
                /*
                Yii::app()->controller->widget('@ext.DzPrettyPhoto.DzPrettyPhoto', [
                    'image_path' => Yii::app()->request->baseUrl .'/'. $file_model->file_path .'/'. $file_model->file_name,
                    'thumb_path' => Yii::app()->iwi->load($file_model->file->realPath)->adaptive(190,150)->cache(),
                    'linkOptions' => [
                        'class' => 'fileinput-image'
                    ]
                ]);
                */
            }
            echo '</div></div>';

            // Buttons & Javacsript for "non-view mode"
            if ( $this->view_mode == false )
            {
                // Buttons
                echo '<div class="fileinput-button-wrapper">';
                echo '<span class="btn '. $this->vec_classes['new'] .' btn-file"><span class="fileinput-new"><i class="wb-plus"></i> '. Yii::t('app', $this->vec_labels['select']) .'</span><span class="fileinput-exists"><i class="wb-pencil"></i> '. Yii::t('app', $this->vec_labels['change']) .'</span>';

                // Input file (hidden). Like this: <input type="file" />
                echo $this->getPrepend();
                if ( $this->hasModel() )
                {
                    // echo Html::activeFileField($this->model, $this->attribute, $this->htmlOptions);
                    if ( $this->is_image )
                    {
                        // echo Html::activeFileField($this->model, $this->attribute, ['accept' => 'image/*', 'class' => 'btn btn-default']);
                        echo Html::fileField($this->name, $this->value, ['accept' => 'image/*', 'class' => 'btn btn-default']);
                    }
                    else
                    {
                        // echo Html::activeFileField($this->model, $this->attribute);
                        echo Html::fileField($this->name, $this->value);
                    }

                    // File ID
                    if ( !empty($file_model) )
                    {
                        echo Html::activeHiddenField($this->model, $this->file_id_field, [
                            'name'  => str_replace($this->file_name_field, $this->file_id_field, $this->name)
                        ]);
                        // echo Html::hiddenField($this->name, $this->value);
                    }
                }
                else
                {
                    // echo Html::fileField($this->name, $this->value, $this->htmlOptions);
                    if ( $this->is_image )
                    {
                        echo Html::fileField($this->name, $this->value, ['accept' => 'image/*', 'class' => 'btn btn-default']);
                    }
                    else
                    {
                        echo Html::fileField($this->name, $this->value);
                    }
                }
                echo $this->getAppend();

                // More buttons
                echo '</span><a href="javascript:void(0);" class="btn '. $this->vec_classes['remove'] .' fileinput-exists" data-dismiss="fileinput"><i class="wb-trash"></i> '. Yii::t('app', $this->vec_labels['remove']) .'</a>';
                echo '</div></div>';

                // Javascript files
                $this->registerClientScript();
            }

            // View mode
            else
            {
                // Register only CSS
                Yii::app()->clientScript->registerCssFile("{$this->assetsUrl}/css/bootstrap-fileinput" . (YII_DEBUG ? '' : '.min') . ".css");   
                echo '</div>';
            }
        }

        // DOCUMENT FILE TYPE
        else
        {
            // Download document URL
            $download_url = '';
            if ( !empty($file_model) )
            {
                // $download_url = !empty($this->download_url) ? $this->download_url : $file_path . $file_model->{$this->file_name_field};
                $download_url = !empty($this->download_url) ? $this->download_url : $file_model->download_url();
            }

            echo '<div class="input-append">';
            if ( $this->view_mode == false )
            {
                echo '<div class="uneditable-input col-sm-12"><i class="file-icon-append wb-file"></i>';
            }
            else
            {
                echo '<div class="uneditable-input col-sm-12">';

                if ( !empty($file_model) )
                {
                    echo '<a href="'. $download_url .'" target="_blank"><i class="file-icon-append wb-file"></i></a>';
                }
            }


            echo '<i class="icon-file fileinput-exists"></i> <span class="fileinput-preview">';
            if ( !empty($file_model) )
            {
                echo '<a href="'. $download_url .'" target="_blank">'. $file_model->{$this->file_name_field} .'</a>';
            }
            echo '</span></div></div>';

            // Buttons & Javacsript for "non-view mode"
            if ( $this->view_mode === false )
            {
                // Buttons
                echo '<div class="fileinput-button-wrapper"><span class="btn '. $this->vec_classes['select'] .' btn-file"><span class="fileinput-new"><i class="icon wb-plus"></i> '. Yii::t('app', $this->vec_labels['new']) .'</span><span class="fileinput-exists"><i class="icon wb-pencil"></i> '. Yii::t('app', $this->vec_labels['change']) .'</span>';

                // Input file (hidden). Like this: <input type="file" />
                if ( $this->hasModel() )
                {
                    
                    // echo Html::activeFileField($this->model, $this->attribute, $this->htmlOptions);
                    // echo Html::activeFileField($this->model, $this->attribute);
                    echo Html::fileField($this->name, $this->value);

                    // File ID
                    if ( !empty($file_model) )
                    {
                        echo Html::activeHiddenField($this->model, $this->file_id_field, [
                            'name'  => str_replace($this->file_name_field, $this->file_id_field, $this->name)
                        ]);
                        // echo Html::hiddenField($this->name, $this->value);
                    }
                }
                else
                {
                    // echo Html::fileField($this->name, $this->value, $this->htmlOptions);
                    echo Html::fileField($this->name, $this->value);
                }
                echo $this->getAppend();

                // More buttons
                echo '</span><a href="javascript:void(0);" class="btn '. $this->vec_classes['remove'] .' fileinput-exists" data-dismiss="fileinput"><i class="wb-trash"></i> '. Yii::t('app', $this->vec_labels['remove']) .'</a>';
                echo '</div></div>';

                // Javascript files
                $this->registerClientScript();
            }

            // View mode
            else
            {
                // Register only CSS
                Yii::app()->clientScript->registerCssFile("{$this->assetsUrl}/css/bootstrap-fileinput" . (YII_DEBUG ? '' : '.min') . ".css");   
                echo '</div>';
            }
        }
    }
    
    
    /**
     * Register required CSS and script files
     */
    protected function registerClientScript()
    {
        $cs = Yii::app()->getClientScript();

        // CSS & Javascript needed for this widget
        $cs->registerCoreScript('jquery');
        $cs->registerCssFile("{$this->assetsUrl}/css/bootstrap-fileinput" . (YII_DEBUG ? '' : '.min') . ".css");
        $cs->registerScriptFile("{$this->assetsUrl}/js/bootstrap-fileinput" . (YII_DEBUG ? '' : '.min') . ".js");
        
        $options = '';
        if ( !empty($this->options) )
        {
            $options = CJavaScript::encode($this->options);
        }

        // Javascript fileinput() with options and events
        ob_start();
        echo "jQuery('#{$this->htmlOptions['id']}').fileinput(". $options .")";
        foreach ($this->events as $event => $handler)
        {
            echo ".on('{$event}', " . CJavaScript::encode($handler) . ")";
        }
        $cs->registerScript(__CLASS__ .'#'. $this->id, ob_get_clean() . ';', CClientScript::POS_READY);

        // $cs->registerScript(__CLASS__ .'#'. $this->id, "jQuery('#{$this->htmlOptions['id']}').fileinput(". $options .");", CClientScript::POS_READY);
        /*
        $cs->registerScript(__CLASS__ .'#'. $this->id,
            "jQuery('#{$this->htmlOptions['id']}').fileinput(". $options .");
             jQuery('#{$this->htmlOptions['id']} .fileinput-image').dzModalImage();",
            CClientScript::POS_READY);
        */
    }
}