<?php
/**
 * Sidebar widget class file
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2012 Dezero
 */

namespace dz\widgets;

use dz\helpers\Html;
use dz\widgets\InputWidget;
use Yii;

class Sidebar extends InputWidget
{
	/**
	 * Sidebar conten top (before link)
	 *
	 * @var HTML
	 */
	public $content_top;
	

	/**
	 * Menu items
	 *
	 * @var array
	 */
	public $menu_items = array();
	
		
	/**
	 * Sidebar buttons
	 *
	 * @var array
	 */
	public $buttons = array();
	
	
	/**
	 * Sidebar conten bottom (after buttons)
	 *
	 * @var HTML
	 */
	public $content_bottom;
	
	
	/**
	 * Uses affix javascript plugin?
	 *
	 * @link http://twitter.github.com/bootstrap/javascript.html#affix
	 * 
	 * @var bool
	 */
	public $use_affix = TRUE;
	
	
	/**
	 * Offset position of affix javascript plugin
	 *
	 * @link http://twitter.github.com/bootstrap/javascript.html#affix
	 * 
	 * @var array
	 */
	public $affix_position = array('top' => 100, 'bottom' => '');
	
	
	/**
	 * Uses tabs javascript plugin?
	 *
	 * @link http://twitter.github.com/bootstrap/javascript.html#tabs
	 * @link http://twitter.github.com/bootstrap/javascript.html#scrollspy
	 * 
	 * @var string
	 */
	public $menu_widget = 'tabs';	


	/**
	 * Scrollspy options
	 *
	 * @link http://twitter.github.com/bootstrap/javascript.html#scrollspy
	 * 
	 * @var array
	 */
	public $scrollspy_options = array('selector' => 'body', 'target' => '#sidebar', 'offset' => '');
	
	
	/**
	 * HTML attributes for the widget container
	 * 
	 * @var array
	 */
	public $htmlOptions = array();


	/**
	 * Number of grid columns
	 * 
	 * @var int
	 */
	public $grid_columns = 2;
	
	
	/**
	 * Initializes the widget
	 */
	public function init()
	{
		if ( !isset($this->htmlOptions['id']) )
		{
			$this->htmlOptions['id'] = $this->getId();
		}
	}
	
	
	/**
	 * Runs the widget
	 */
	public function run()
	{
		$cs = Yii::app()->getClientScript();
		$id = $this->htmlOptions['id'];	

		echo Html::openTag('div', $this->htmlOptions);
				
		// Content top
		if ( !empty($this->content_top) )
		{
			echo '<div class="sidebar-content-top">'. $this->content_top ."</div>";
		}
		echo "<hr/>\n";
		
		// Menu items
		if ( count($this->menu_items) )
		{
			// Use scrollspy Bootstrap javascript plugin? (http://twitter.github.com/bootstrap/javascript.html#scrollspy)
			if ( $this->menu_widget == 'scrollspy' )
			{
				echo Html::openTag('ul', array('class' => 'nav nav-list sidebar-spy-links') );
				foreach ( $this->menu_items as $que_menu_item )
				{
					echo '<li><a href="'. $que_menu_item['url'] .'"><i class="icon-chevron-left"></i> '. $que_menu_item['label'] ."</span></a></li>\n";
				}
				echo "</ul><hr/>\n";
			}

			// Use TABS Bootstrap javascript plugin? (http://twitter.github.com/bootstrap/javascript.html#tabs)
			else
			{
				echo Html::openTag('ul', array('class' => 'nav nav-list nav-tabs') );
				foreach ( $this->menu_items as $num_item => $que_menu_item )
				{
					if ( $num_item == 0 )
					{
						echo '<li class="active">';
					}
					else
					{
						echo '<li>';
					}
					echo '<a href="'. $que_menu_item['url'] .'" data-toggle="tab"><i class="icon-chevron-left"></i> '. $que_menu_item['label'] ."</a></li>\n";
				}
				echo "</ul><hr/>\n";
			}
		}
				
		// Buttons
		if ( count($this->buttons) )
		{
			echo "<div class=\"buttons actions\">";
			foreach ( $this->buttons as $que_button )
			{
				$button_params = array();
				if ( is_array($que_button) )
				{
					if ( isset($que_button['more_links']) )
					{
						echo Html::more_links($que_button['more_links'], array('class' => 'dropdown-menu actionbar-more-links pull-right'));
						continue;
					}
					if ( !isset($que_button['name']) )
					{
						continue;
					}
					$button_params = $que_button;
					$que_button = $que_button['name'];
				}
				
				switch ( $que_button )
				{
					case 'cancel':
						if ( !isset($button_params['label']) )
						{
							$button_params['label'] = Yii::t('app', 'Cancel');
						}
						echo Html::link($button_params['label'], $button_params['url'], array('class' => 'sidebar-button sidebar-'. strtolower($que_button) .' btn') );
					break;
					
					case 'update':
					case 'create':
						if ( !isset($button_params['label']) )
						{
							$button_params['label'] = Yii::t('app', 'Save');
						}
						// $button_label = $que_button == 'create' ? Yii::t('app', 'Create') : Yii::t('app', 'Save');
						
						echo "<a href=\"javascript:void(0);\" id=\"sidebar-submit\" class=\"sidebar-button sidebar-". strtolower($que_button) ." btn btn-primary\">". $button_params['label'] ."</a>\n";

						$cs->registerScript(__CLASS__.'#'. $id .'-submit', 
							"jQuery('#sidebar-submit').on('click', function(){
								$('#". $button_params['form_id']."').submit();
							});"
						);
					break;

					case 'update_icon':
						$vec_htmlOptions = array(
							'class' => 'sidebar-button btn btn-icon btn-spy-link',
							'rel' => 'tooltip',
							'icon' => 'pencil',
							'data-original-title' => Yii::t('app', 'Update'),
						);
						echo Html::link('', $button_params['url'], $vec_htmlOptions);
					break;
					
					default:
						if ( !isset($button_params['label']) )
						{
							$button_params['label'] = $que_button;
						}

						$vec_htmlOptions = array('class' => 'sidebar-button sidebar-'. strtolower($que_button) .' btn');
						if ( isset($button_params['htmlOptions']) )
						{
							$vec_htmlOptions = CMap::mergeArray($button_params['htmlOptions'], $vec_htmlOptions);
						}
						echo Html::link($button_params['label'], $button_params['url'], $vec_htmlOptions);
					break;
				}
			}
			echo "</div>";
		}
		
		// Content bottom
		if ( !empty($this->content_bottom) )
		{
			echo $this->content_bottom;
		}
		
		echo '</div>';
		
		// Affix
		if ( $this->use_affix )
		{
			$offset_js = '';
			if ( count($this->affix_position) )
			{
				$offset_js .= '{offset: {';
				if ( isset($this->affix_position['top']) AND $this->affix_position['top'] > 0 )
				{
					$offset_js .= 'top: '. $this->affix_position['top'];
					if ( isset($this->affix_position['bottom']) AND $this->affix_position['bottom'] > 0 )
					{
						$offset_js .= ',';
					}
				}
				if ( isset($this->affix_position['bottom']) AND $this->affix_position['bottom'] > 0 )
				{
					$offset_js .= 'bottom: '. $this->affix_position['bottom'];
				}
				$offset_js .= '}}';
			}
			$cs->registerScript(__CLASS__.'#'.$id.'-affix', "jQuery('#{$id}').affix({$offset_js}).on('affixed', function(){
				jQuery('#{$id}').css('width', jQuery('#sidebar').css('width'));
			});");			
		}

		// Use scrollspy Bootstrap javascript plugin? (http://twitter.github.com/bootstrap/javascript.html#scrollspy)
		if ( $this->menu_widget == 'scrollspy' )
		{
			$scrollspy_js = "jQuery('". $this->scrollspy_options['selector'] ."').attr('data-spy', 'scroll');";

			if ( !empty($this->scrollspy_options['target']) )
			{
				$scrollspy_js .= "jQuery('". $this->scrollspy_options['selector'] ."').attr('data-target', '{$this->scrollspy_options['target']}');";
			}
			
			if ( !empty($this->scrollspy_options['offset']) )
			{
				$scrollspy_js .= "jQuery('". $this->scrollspy_options['selector'] ."').attr('data-offset', '{$this->scrollspy_options['offset']}');";
			}
			$cs->registerScript(__CLASS__.'#'.$this->scrollspy_options['selector'].'-scrollspy', $scrollspy_js, \CClientScript::POS_BEGIN);
		}
	}
}