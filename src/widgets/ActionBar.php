<?php
/**
 * ActionBar widget class file
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2012 Dezero
 */

namespace dz\widgets;

use dz\helpers\Html;
use dz\widgets\InputWidget;
use Yii;

class ActionBar extends InputWidget
{
	/**
	 * Main buttons
	 *
	 * @var array
	 */
	public $buttons = array();
	
		
	/**
	 * More links in dropdown
	 *
	 * @var array
	 */
	public $more_links = array();
	
	
	/**
	 * HTML attributes for the widget container
	 * 
	 * @var array
	 */
	public $htmlOptions = array();
	
	
	/**
	 * Initializes the widget
	 */
	public function init()
	{
		if ( !isset($this->htmlOptions['id']) )
		{
			$this->htmlOptions['id'] = $this->getId();
		}
	}
	
	/*
	<div class="action-bar btn-group">
		<?php echo CHtml::link('<i class="icon fa-pencil"></i>', array('update', 'id' => $model->cliente_id), array('class' => 'btn update', 'rel' => 'tooltip', 'data-original-title' => Yii::t('app', 'Update'))); ?>
		<a class="btn dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">
			Más <span class="caret"></span>
		</a>
		<ul class="dropdown-menu">
			<li><a href="javascript:void(0);">Action</a></li>
			<li><a href="javascript:void(0);">Another action</a></li>
			<li><a href="javascript:void(0);">Something else here</a></li>
			<li class="divider"></li>
			<li><a href="javascript:void(0);">Separated link</a></li>
		</ul>
	</div>
	*/
	
	/**
	 * Runs the widget
	 */
	public function run()
	{
		$js_code = "";
		$id = $this->htmlOptions['id'];
		echo Html::openTag('div', $this->htmlOptions);
		
		// Buttons
		if ( count($this->buttons) )
		{
			foreach ( $this->buttons as $que_button )
			{
				// Check access
				if ( isset($que_button['visible']) AND !$this->evaluateExpression($que_button['visible']) )
				{
					continue;
				}
				
				// HTML Options
				$htmlOptions = array(
					'class' => 'btn'
				);
				
				if ( isset($que_button['htmlOptions']) )
				{
					$htmlOptions = \CMap::mergeArray(
						$htmlOptions,
						$que_button['htmlOptions']
					);
				}
				
				// Icon button
				if ( isset($que_button['icon']) )
				{
					$htmlOptions['data-original-title'] = $que_button['label'];
					$htmlOptions['rel'] = 'tooltip';
					$que_button['label'] = '<i class="fa-'. $que_button['icon'] .' icon-'. $que_button['icon'] .'"></i>';
				}

				// Link
				echo Html::link($que_button['label'], $que_button['url'], $htmlOptions);
			}
		}
		
		// More links dropdown
		if ( count($this->more_links) )
		{
			echo Html::more_links($this->more_links, array('class' => 'dropdown-menu actionbar-more-links pull-right'));
		}

		// Close main div
		echo '</div>';
	}
	
}