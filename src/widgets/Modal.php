<?php
/**
 * Modal widget class file
 *
 * This extension is a wrapper for Bootstrap Modal extension plugin (https://github.com/jschr/bootstrap-modal)
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2013 Dezero
 * 
 * @see http://twitter.github.com/bootstrap/javascript.html#modals
 * @see https://github.com/jschr/bootstrap-modal
 */

namespace dz\widgets;

use CClientScript;
use CJavaScript;
use dz\helpers\Html;
use dz\widgets\InputWidget;
use Yii;

class Modal extends InputWidget
{
	/**
	 * File path to assets
	 *
	 * @var string
	 */
	protected $assetsPath;


	/**
	 * URL to assets
	 *
	 * @var string
	 */
	protected $assetsUrl;


	/**
	 * Indicates whether to run Javascript code. Defaults to 'true'.
	 * 
	 * @var boolean
	 */
	public $runJavascript = TRUE;


	/**
	 * Indicates whether to automatically open the modal when initialized. Defaults to 'false'.
	 * 
	 * @var boolean
	 */
	public $autoOpen = false;

	
	/**
	 * Indicates whether the modal should use transitions. Defaults to 'true'.
	 * 
	 * @var boolean
	 */
	public $fade = true;
	

	/**
	 * Full width option. Defaults to 'false'.
	 *
	 * @var boolean
	 */
	public $fullWidth = FALSE;


	/**
	 * Content loaded into an iframe. Defaults to 'false'.
	 *
	 * @var boolean
	 */
	public $iframe = FALSE;


	/**
	 * Options for the Bootstrap Javascript plugin.
	 * 
	 * @var array
	 */
	public $options = array();
	

	/**
	 * Modal footer buttons
	 *
	 * @var array
	 */
	public $buttons = array();


	/**
	 * the HTML attributes for the widget container.
	 * 
	 * @var array
	 */
	public $htmlOptions = array();


	/**
	 * the Javascript event handlers.
	 * 
	 * @var string[]
	 */
	public $events = array();


	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		// Assets
		if ( $this->assetsPath === NULL )
		{
            $this->assetsPath = dirname(__FILE__) . DIRECTORY_SEPARATOR .'assets'. DIRECTORY_SEPARATOR . 'modal';
		}
		
		if ( $this->assetsUrl === NULL )
		{
			$this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
		}
		
		// HTML Options
		if (!isset($this->htmlOptions['id']))
		{
			$this->htmlOptions['id'] = $this->getId();
		}

		// AutoOpen
		if ($this->autoOpen === false && !isset($this->options['show']))
		{
			$this->options['show'] = FALSE;
		}

		$classes = array('modal');

		if ( $this->fade === TRUE )
		{
			// $classes[] = 'fade';
			$classes[] = 'dz-fade';
		}

		if ( $this->fullWidth === TRUE )
		{
			$classes[] = 'container';
		}

		if ( !empty($classes) )
		{
			$classes = implode(' ', $classes);
			if (isset($this->htmlOptions['class']) )
			{
				$this->htmlOptions['class'] .= ' '.$classes;
			}
			else
			{
				$this->htmlOptions['class'] = $classes;
			}
		}

		$this->htmlOptions['tabindex'] = '-1';
		// $this->htmlOptions['data-focus'] = 'input:not([type=hidden]):first';

		echo Html::openTag('div', $this->htmlOptions);
	}


	/**
	 * Runs the widget.
	 */
	public function run()
	{
		echo '</div>';

		// Javascript files
		$this->registerClientScript();
	}


	/**
	 * Register required CSS and script files
	 */
	protected function registerClientScript()
	{
		$cs = Yii::app()->getClientScript();
		$id = $this->htmlOptions['id'];

		// CSS & Javascript needed for this widget
		$cs->registerCssFile("{$this->assetsUrl}/css/bootstrap-modal.css");
		// $cs->registerCoreScript('jquery');
		$cs->registerScriptFile("{$this->assetsUrl}/js/bootstrap-modalmanager.js");
		$cs->registerScriptFile("{$this->assetsUrl}/js/bootstrap-modal.js");
		
		$options = '';
		if ( !empty($this->options) )
		{
			$options = CJavaScript::encode($this->options);
		}

		if ( $this->runJavascript === TRUE )
		{
			$cs->registerScript(__CLASS__ .'#'. $id, "jQuery('#{$id}').modal({$options})");

			foreach ($this->events as $name => $handler)
			{
				$handler = CJavaScript::encode($handler);
				$cs->registerScript(__CLASS__ .'#'. $id .'_'. $name, "jQuery('#{$id}').on('{$name}', {$handler});");
			}
		}
	}


	/**
	 * Prints an standard modal layout
	 */
	public static function createModal($id, $options = array())
	{
		Yii::app()->controller->beginWidget('dz.widgets.Modal', array(
			'id' => $id,
			'runJavascript' => FALSE,
			'htmlOptions' => array(
				'aria-hidden' => 'true',
			),
			'fullWidth' => isset($options['fullWidth']) ? $options['fullWidth'] : FALSE
		));
		
		// Modal - HTML Header
		echo '<div class="modal-header">
			    <a class="close" data-dismiss="modal">&times;</a>
			    <h4 class="dz-modal-title"></h4>
			    <div class="dz-modal-subtitle"></div>
			</div>';
			
		// Modal - HTML content
		if ( isset($options['iframe']) AND !empty($options['iframe']) )
		{
			if ( !isset($options['iframe_height']) )
			{
				$options['iframe_height'] = 400;
			}
			echo '<div class="dz-modal-body modal-body"><iframe src="" width="100%" height="'. $options['iframe_height'] .'" class="dz-modal-iframe"></iframe></div>';
		}
		else
		{
			echo '<div class="dz-modal-body modal-body"><p>'. Yii::t('app', 'Loading...') .'</p></div>';
		}

		// Modal - HTML footer
		echo '<div class="dz-modal-footer modal-footer">';

		// Footer buttons
		if ( isset($options['buttons']) AND !empty($options['buttons']) )
		{
			foreach ( $options['buttons'] as $que_button )
			{
				if ( !isset($que_button['htmlOptions']['class']) )
				{
					$que_button['htmlOptions']['class'] = '';
				}
				$que_button['htmlOptions']['class'] .= ' dz-button';
				$que_button['htmlOptions']['data-url'] = $que_button['url'];
				
				Yii::app()->controller->widget('@bootstrap.widgets.TbButton', array(
			        'label' => $que_button['label'],
			        'url' => $que_button['url'],
			        'htmlOptions' => $que_button['htmlOptions'],
			    ));
			}
		}

		// View button
		Yii::app()->controller->widget('@bootstrap.widgets.TbButton', array(
			'label'		=> Yii::t('app', 'More details'),
			'htmlOptions' => array(
				'class' => 'dz-modal-view-button dz-modal-button hide',
				'data-url' => '',
				'target' => '_blank'
			)
		));

		// Update button
		Yii::app()->controller->widget('@bootstrap.widgets.TbButton', array(
			'label'		=> Yii::t('app', 'Update'),
			'htmlOptions' => array(
				'class' => 'dz-modal-update-button dz-modal-button hide',
				'data-url' => '',
				'target' => '_blank'
			)
		));

		// Save/submit button
		if ( !isset($options['submitHtmlOptions']) )
		{
			$options['submitHtmlOptions'] = array(
				'class' => 'dz-modal-submit-button dz-modal-button btn-primary hide',
				'onclick' => 'jQuery("#dz-modal-'. $id .' .dz-modal-body form").submit();'
			);
		}
		Yii::app()->controller->widget('@bootstrap.widgets.TbButton', array(
			'label'		=> Yii::t('app', 'Save'),
			'htmlOptions' => $options['submitHtmlOptions']
		));

		// Close button
		Yii::app()->controller->widget('@bootstrap.widgets.TbButton', array(
	        'label' => Yii::t('app', 'Close'),
			'type' => 'default',
	        'url' => '#',
	        'htmlOptions' => array(
				'data-dismiss' => 'modal',
				'aria-hidden'  => "true",
				'class' => 'btn-close btn-dark'
			),
	    ));

		echo '</div>';
		Yii::app()->controller->endWidget();
		// Yii::app()->getClientScript()->registerScript(__CLASS__.'#dz-modal-'. $id, "jQuery('#dz-modal-". $id ."').appendTo(document.body).modal({show: false});", CClientScript::POS_READY);
		Yii::app()->getClientScript()->registerScript(__CLASS__.'#dz-modal-'. $id, "jQuery('#". $id ."').addClass('dz-modal-scrollable').appendTo(document.body).modal({show: false});", CClientScript::POS_READY);
	}
}
