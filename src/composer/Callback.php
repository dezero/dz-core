<?php
/**
 * dz\composer\Callback provides composer hooks
 *
 * Totally inspired by YiiComposer extension
 *
 */

namespace dz\composer;

use Composer\Script\Event;

defined('YII_PATH') or define('YII_PATH', dirname(__FILE__).'/../../../vendor/yii/framework');
defined('CONSOLE_CONFIG') or define('CONSOLE_CONFIG', dirname(__FILE__).'/../../../config/console.php');
// we don't check YII_PATH, since it will be downloaded with composer

class Callback
{
    const PARAM_WRITABLE = 'yii-install-writable';
    const PARAM_EXECUTABLE = 'yii-install-executable';

    /**
     * Sets the correct permissions of files and directories.
     * @param Event $event
     */
    public static function setPermissions(Event $event)
    {
        return;
        $options = array_merge(
            array(
                self::PARAM_WRITABLE => array(),
                self::PARAM_EXECUTABLE => array(),
            ),
            $event->getComposer()->getPackage()->getExtra()
        );

        foreach ((array)$options[self::PARAM_WRITABLE] as $path) {
            echo "Setting writable: $path ...";
            if (!is_dir($path)) {
                mkdir($path);
            }

            if (is_dir($path)) {
                chmod($path, 0777);
                echo "done\n";
            } else {
                echo "The directory was not found: " . getcwd() . DIRECTORY_SEPARATOR . $path;
                return;
            }
        }

        foreach ((array)$options[self::PARAM_EXECUTABLE] as $path) {
            echo "Setting executable: $path ... ";
            if (is_file($path)) {
                chmod($path, 0755);
                echo "done\n";
            } else {
                echo "\n\tThe file was not found: " . getcwd() . DIRECTORY_SEPARATOR . $path . "\n";
                return;
            }
        }
    }


    /**
     * Creates console application, if Yii is available
     */
    private static function getYiiApplication()
    {
        if (!is_file(YII_PATH.'/yii.php'))
        {
            return null;
        }

        require_once(YII_PATH . '/yii.php');
       
        spl_autoload_register(array('YiiBase', 'autoload'));

        if (\Yii::app() === null)
        {
            if (is_file(CONSOLE_CONFIG))
            {
                $app = \Yii::createConsoleApplication(CONSOLE_CONFIG);
            }
            else
            {
                throw new \Exception("File from CONSOLE_CONFIG not found");   
            }            
        }
        else
        {
            $app = \Yii::app();
        }
        return $app;
    }
}