<?php
/**
 * Component class for working with Composer Autoload ClassLoader class
 */

namespace dz\composer;

use dz\base\ApplicationComponent;
use Yii;

class Composer extends ApplicationComponent
{
    /**
     * @var object \Composer\Autoload\ClassLoader class
     */
    public $classLoader;

    /**
     * Init function
     */
    public function init()
    {
        $this->classLoader = require Yii::getAlias('@vendor') . DIRECTORY_SEPARATOR . 'autoload.php';

        parent::init();
    }


    /**
     * Return \Composer\Autoload\ClassLoader instance class
     */
    public function getClassLoader()
    {
        return $this->classLoader;
    }


    /**
     * Finds the path to the file where the class is defined.
     *
     * @param string $class The name of the class
     *
     * @return string|false The path if found, false otherwise
     */
    public function findFile($class)
    {
        return $this->classLoader->findFile($class);
    }
}