<?php
/**
 * DzDatetimePicker widget class file
 *
 * This extension is a wrapper for Bootstrap v3 datetimepicker widget
 * https://github.com/Eonasdan/bootstrap-datetimepicker
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2014 Dezero
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package extensions.DzDatetimePicker
 */

use dz\widgets\InputWidget;


/**
 * DzDatetimePicker widget
 * @see https://github.com/Eonasdan/bootstrap-datetimepicker
 */
class DzDatetimePicker extends InputWidget
{
	/**
	 * @var TbActiveForm when created via TbActiveForm, this attribute is set to the form that renders the widget
	 * @see TbActionForm->inputRow
	 */
	public $form;
	

	/**
	 * @var array the options for the Bootstrap JavaScript plugin.
	 */
	public $options = array();


	/**
	 * @var string[] the JavaScript event handlers.
	 */
	public $events = array();

	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		// Assets
		if ( $this->assetsPath === NULL )
		{
			$this->assetsPath = dirname(__FILE__) . DIRECTORY_SEPARATOR .'assets';
		}
		
		if ( $this->assetsUrl === NULL )
		{
			$this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
		}

		$this->htmlOptions['type'] = 'text';
		$this->htmlOptions['autocomplete'] = 'off';

		if ( !isset($this->options['language']) )
		{
			$this->options['language'] = Yii::app()->language;
		}

		if ( !isset($this->options['format']) )
		{
			$this->options['format'] = 'DD/MM/YYYY HH:mm';
		}

		// Change icon library from "Glyphicon" yo "Font Awesome"
		$this->options['icons'] = array(
			'time' 	=> 'fa fa-clock-o',
			'date' 	=> 'fa fa-calendar',
			'up'	=> 'fa fa-chevron-up',
			'down'	=> 'fa fa-chevron-down',
		);

	}

	/**
	 * Runs the widget.
	 */
	public function run()
	{
		list($name, $id) = $this->resolveNameID();

		echo $this->getPrepend();
		if ($this->hasModel())
		{
			if ( $this->form )
			{
				echo $this->form->textField($this->model, $this->attribute, $this->htmlOptions);
			}
			else
			{
				echo CHtml::activeTextField($this->model, $this->attribute, $this->htmlOptions);
			}

		}
		else
		{
			echo CHtml::textField($name, $this->value, $this->htmlOptions);
		}
		echo $this->getAppend();

		$this->registerClientScript($id);
	}

	/**
	 * Registers required client script for bootstrap datepicker. It is not used through bootstrap->registerPlugin
	 * in order to attach events if any
	 */
	public function registerClientScript($id)
	{
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile("{$this->assetsUrl}/css/bootstrap-datetimepicker.min.css");
		$cs->registerScriptFile("{$this->assetsUrl}/js/moment-with-locales.min.js");
		// $cs->registerScriptFile("{$this->assetsUrl}/js/moment.min.js");
		$cs->registerScriptFile("{$this->assetsUrl}/js/bootstrap-datetimepicker.min.js");

		/*
		if ( isset($this->options['language']) )
		{
			// Yii::app()->bootstrap->registerAssetJs('locales/bootstrap-datepicker.'.$this->options['language'].'.js', CClientScript::POS_END);
			$cs->registerScriptFile("{$this->assetsUrl}/js/locales/bootstrap-datepicker.". $this->options['language' ] .".js", CClientScript::POS_END);
		}
		*/
		$options = !empty($this->options) ? CJavaScript::encode($this->options) : '';

		ob_start();
		// echo "moment.lang('en', {week : {dow : 1}});";
		echo "jQuery('#{$id}').datetimepicker({$options})";
		foreach ($this->events as $event => $handler)
			echo ".on('{$event}', " . CJavaScript::encode($handler) . ")";

		Yii::app()->getClientScript()->registerScript(__CLASS__ . '#' . $this->getId(), ob_get_clean() . ';');
	}
}
