<?php
/**
 * DzFilepond widget class file
 *
 * This extension is a wrapper for Filepond library
 *
 * @see https://pqina.nl/filepond/
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2019 Dezero
 */

use dz\helpers\Html;
use dz\helpers\StringHelper;
use dz\widgets\InputWidget;


class DzFilepond extends InputWidget
{
    /**
     * @var array.  Markdown plugin options
     */
    public $options = [];


    /**
     * Vendor path (dist files)
     */
    public $vendorPath;


    /**
     * Initializes the widget
     */
    public function init()
    {
        // Vendor path
        if ( empty($this->vendorPath) )
        {
            $this->vendorPath = Yii::getAlias('@ext') . DIRECTORY_SEPARATOR . 'DzFilepond' . DIRECTORY_SEPARATOR . 'assets';
        }

        // Assets
        if ( $this->assetsPath === NULL )
        {
            $this->assetsPath = $this->vendorPath;
        }
        
        if ( $this->assetsUrl === NULL )
        {
            $this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
        }
        
        // HTML Options
        list($this->name, $this->id) = $this->resolveNameID();
        if ( !isset($this->htmlOptions['id']) )
        {
            $this->htmlOptions['id'] = $this->id .'-filepond';
        }
    }


    /**
     * Runs the widget
     */
    public function run()
    {
        // Class
        if ( !isset($this->htmlOptions['class']) )
        {
            $this->htmlOptions['class'] = 'dz-filepond';
        }
        else
        {
            $this->htmlOptions['class'] .= ' dz-filepond';
        }

        // Load file URL for AssetImage or AssetFile models
        $class_name = StringHelper::basename(get_class($this->model));
        if ( $class_name == 'AssetImage' || $class_name == 'AssetFile' )
        {

            // $this->htmlOptions['data-source'] = $this->model->file_url();
            /*
            $this->options['files'] = [[
                'source' => $this->model->file_url(),
                'options' => [
                    'file' => [
                        'name' => $this->model->file_name,
                        'size' => $this->model->file_size,
                        'type' => $this->model->file_mime
                    ],
                ]
            ]];
            */

            // Image previes
            /*
            if ( $class_name == 'AssetImage' )
            {
                $this->options['files'][0]['options']['metadata'] = [
                    'poster' => $this->model->image_url()
                ];
            }
            */
        }

        echo '<div id="'. $this->id .'-wrapper" class="filepond-wrapper">';
        if ( $this->hasModel() )
        {
            echo Html::activeFileField($this->model, $this->attribute, $this->htmlOptions);
        }
        else
        {
            echo Html::fileField($this->name, $this->value, $this->htmlOptions);
        }

        echo '</div>';

        // Javascript files
        $this->registerClientScript();
    }


    /**
     * Register required CSS and script files
     */
    protected function registerClientScript()
    {
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile($this->assetsUrl .'/css/filepond-all.min.css');
        $cs->registerScriptFile($this->assetsUrl .'/js/filepond-all.min.js');
        
        $options = CJSON::encode($this->options);

        $cs->registerScript(__CLASS__, "jQuery(\".dz-filepond\").filepond(". $options .");", CClientScript::POS_READY);
    }
}