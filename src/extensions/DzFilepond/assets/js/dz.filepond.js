// Custom Javascript for DZ framewok
(function($) {

  $(document).ready(function (){
    $.fn.filepond.registerPlugin(
      FilePondPluginFileValidateType,
      FilePondPluginFileValidateSize,
      FilePondPluginFilePoster,
      FilePondPluginFilePoster,
      FilePondPluginImageExifOrientation,
      FilePondPluginImagePreview,
      FilePondPluginImageValidateSize
    );
    $.fn.filepond.setDefaults({
      server: window.js_globals.baseUrl +'/asset/filepond',
      allowMultiple: true,
      dropOnPage: true,
      dropOnElement: true,
      itemInsertLocation: 'after',
      labelIdle: '<span class="dropify-icon"></span><span>Drag & Drop your files or </span><span class="filepond--label-action">Browse</span>',
    });
  });
}(window.jQuery || window.$));