<?php
/**
 * DzAjaxModal class file
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2012 Dezero
 */

use dz\helpers\Html;
use dz\widgets\Modal;

/**
 * Widget that works with Bootstrap Modal vía AJAX
 */
class DzAjaxModal extends CWidget
{
	/**
	 * Link or button label
	 * 
	 * @var string
	 */
	public $label;
	
	
	/**
	 * Icon link
	 * 
	 * @var string
	 */
	public $icon = '';
	
	
	/**
	 * Source URL to get all data showed in Modal
	 * 
	 * @var string
	 */
	public $url;
	

	/**
	 * Model to load in Modal widget
	 * 
	 * @var CModel
	 */
	public $model;
	
	
	/**
	 * Model primary key value (ID)
	 * 
	 * @var string
	 */
	public $model_id;

	
	/**
	 * HTML element selector
	 * 
	 * @var string
	 */
	public $selector = '';
	
	/**
	 * Modal title
	 *
	 * @var array
	 */
	public $title;

	/**
	 * Modal footer buttons
	 *
	 * @var array
	 */
	public $buttons = array();
	
	
	/**
	 * HTML attributes for the widget container
	 * 
	 * @var array
	 */
	public $htmlOptions = array();


	/**
	 * HTML attributes for the link
	 * 
	 * @var array
	 */
	public $linkOptions = array();


	/**
	 * HTML attributes for the modal container
	 * 
	 * @var array
	 */
	public $modalOptions = array();
	
	
	/**
	 * Initializes the widget
	 */
	public function init()
	{
		// Import Bootstrap needed widgets
		// Yii::import('@bootstrap.widgets.TbModal');
		Yii::import('@bootstrap.widgets.TbButton');
		
		if ( !isset($this->htmlOptions['id']) )
		{
			$this->htmlOptions['id'] = $this->getId();
		}
		
		if ( !isset($this->label) )
		{
			$this->label = Yii::t('app', 'View details');
		}
	}
	
	
	/**
	 * Runs the widget
	 */
	public function run()
	{
		$id = $this->htmlOptions['id'];
		$modelClass = NULL;
		if ( empty($this->title) AND $this->model !== NULL )
		{
			$this->title = get_class($this->model);
		}
		
		$html_modal_options = array(
			'id' => 'dz-modal-'. $id,
			/* 
			'htmlOptions' => array(
				'data-width' => 860
			),
			'events' => array(
				'shown' => 'js:function(){$("#dz-modal-'. $id .' .tooltip").remove(); console.log($("#dz-modal-'. $id .' a[rel=\'tooltip\']")); $("#dz-modal-'. $id .' a[rel=\'tooltip\']").tooltip(); }'
			) */
		);

		if ( !empty($this->modalOptions) )
		{
			$html_modal_options['htmlOptions'] = $this->modalOptions;
		}

		$this->beginWidget('dz.widgets.Modal', $html_modal_options);

		echo '<div class="modal-header">
			    <a class="close" data-dismiss="modal">&times;</a>
			    <h4>'. $this->title .' <span id="dz-modal-pk-'. $id .'"></span></h4>
			</div>
			
			<div id="dz-modal-body-'. $id .'" class="modal-body">
			    <p>'. Yii::t('app', 'Loading...') .'</p>
			</div>
			<div id="dz-modal-footer-'. $id .'" class="modal-footer">';
			
				// Footer buttons
				if ( count($this->buttons) )
				{
					foreach ( $this->buttons as $que_button )
					{
						if ( !isset($que_button['htmlOptions']['class']) )
						{
							$que_button['htmlOptions']['class'] = '';
						}
						$que_button['htmlOptions']['class'] .= ' dz-button';
						$que_button['htmlOptions']['data-url'] = $que_button['url'];
						
						$this->widget('@bootstrap.widgets.TbButton', array(
					        'label' => $que_button['label'],
					        'url' => $que_button['url'],
					        'htmlOptions' => $que_button['htmlOptions'],
					    ));
					}
				}
			
				// Close button
				$this->widget('@bootstrap.widgets.TbButton', array(
			        'label' => Yii::t('app', 'Close'),
					'type' => 'inverse',
			        'url' => '#',
			        'htmlOptions' => array(
						'data-dismiss' => 'modal',
						'aria-hidden'  => "true"
					),
			    ));
			echo '</div>';
		$this->endWidget();

		// Icon button
		if ( !empty($this->icon) )
		{
			$vec_link_options = array(
				'class' => 'btn btn-inverse',
				'id' => 'button-modal-'. $id,
				'rel' => 'tooltip',
				'data-original-title' => $this->label,
		        'data-toggle' => 'modal',
		        'data-target' => '#dz-modal-'. $id,
		    );
			if ( !empty($this->linkOptions) )
			{
				$vec_link_options = CMap::mergeArray($vec_link_options, $this->linkOptions);
			}

			echo Html::link('<i class="fa-'. $this->icon .' fa-white"></i>', '#', $vec_link_options);	
		}
		
		// Normal link
		else
		{
			$vec_link_options = array(
				'id' => 'button-modal-'. $id,
		        'data-toggle' => 'modal',
		        'data-target' => '#dz-modal-'. $id,
		    );
			if ( !empty($this->linkOptions) )
			{
				$vec_link_options = CMap::mergeArray($vec_link_options, $this->linkOptions);
			}

			echo Html::link($this->label, '#', $vec_link_options);
		}
		
		// Javascript code
		$vec_modal_options = array(
			'id' => $id,
			'url' => $this->url,
			'modelCass' => $modelClass,
		);
		if ( !empty($this->selector) )
		{
			$vec_modal_options['selector'] = $this->selector;
		}
		else
		{
			$vec_modal_options['model_id'] = $this->model_id;
		}
		
		$cs = Yii::app()->getClientScript();
		$cs->registerScript(__CLASS__.'#'.$id, "jQuery('#button-modal-$id').dzAjaxModal(". CJSON::encode($vec_modal_options) .");", CClientScript::POS_READY);
	}
}