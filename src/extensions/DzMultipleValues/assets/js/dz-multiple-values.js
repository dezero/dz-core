/**
 * Custom jQuery Multiple Values field by Dezero
 */
(function ($) {
    $.fn.multipleValues = function (options) {
		function init() {
			var num_inputs = 0;
			var last_input = 0;
			
			// Init
			$inputs.each(function(){
				if ( $(this).val() === "" ) {
					if ( num_inputs === 0 ) {
						$(this).show();
						last_input = num_inputs;
					}
				}
				else {
					$(this).show();
					last_input = num_inputs;
				}
				num_inputs++;
			});

			if ( last_input !== (num_inputs - 1) ) {
				// Click on "Add more" link
				$base.children(".add-more").on("click", function(e){
					e.preventDefault();
					var next_input = last_input+1;
					if ( next_input < options.max_fields ) {
						$inputs.eq(next_input).show();
						last_input = next_input;

						if ( next_input == (options.max_fields-1) ) {
							$(this).hide();
						}

						if ( (next_input+1) >= Math.round(options.max_fields/2) ) {
							$label.css('min-height', 60);
						}
						else {
							$label.css('min-height', 'auto');
						}
					}
				});
			} else {
				$base.children(".add-more").hide();
			}

			if ( (last_input+1) >= Math.round(options.max_fields/2) ) {
				$label.css('min-height', 60);
			}
		}

		var $base = $(this);

		if ($(this).size() > 0) {
			var $inputs = $base.children(".dz-multiple-inputs");
			var $label = $base.parent().children("label");
			init();
		}
    };
})(jQuery);