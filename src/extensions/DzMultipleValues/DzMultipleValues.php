<?php
/**
 * DzMultipleValues widget class file
 *
 * Custom extension for multiple values purpose fields
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2014 Dezero
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package extensions.DzMultipleValues
 */

use dz\helpers\Html;
use dz\widgets\InputWidget;

/**
 * DzMultipleValues widget
 */
class DzMultipleValues extends InputWidget
{
	/**
	 * Options
	 *
	 * @var array
	 */
	public $options = array();
	
	
	/**
	 * Initializes the widget
	 */
	public function init()
	{
		// Assets
		if ( $this->assetsPath === NULL )
		{
			$this->assetsPath = dirname(__FILE__) . DIRECTORY_SEPARATOR .'assets';
		}
		
		if ( $this->assetsUrl === NULL )
		{
			$this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
		}
		
		// HTML Options
		list($this->name, $this->id) = $this->resolveNameID();
		if ( !isset($this->htmlOptions['id']) )
		{
			$this->htmlOptions['id'] = $this->id .'-multiple';
		}

		// Default options
		if ( !isset($this->options['max_fields']) )
		{
			$this->options['max_fields'] = 3;
		}
	}
	
	/**
	 * Runs the widget
	 */
	public function run()
	{
		if ( !isset($this->htmlOptions['class']) )
		{
			$this->htmlOptions['class'] = 'dz-multiple-values';
		}
		else
		{
			$this->htmlOptions['class'] .= ' dz-multiple-values';
		}
		
		echo Html::openTag('div', $this->htmlOptions);

		// Input HTML Options
		$vec_inputOptions = $this->htmlOptions;
		$vec_inputOptions['class'] = 'dz-multiple-inputs';
		unset($vec_inputOptions['id']);

		if ( $this->hasModel() )
		{
			for ( $i=0; $i < $this->options['max_fields']; $i++ )
			{
				echo Html::activeTextField($this->model, $this->attribute .'['. $i .']', $vec_inputOptions);
			}
			// echo Html::activeTextField($this->model, $this->attribute, $this->htmlOptions);
		}
		else
		{
			echo Html::textField($this->name, $this->value, $this->htmlOptions);
		}
		
		echo '<a class="add-more btn btn-small" href="javascript:void(0);"><i class="icon fa-plus"></i> Añadir otro</a>';


		$this->registerClientScript();
	}
	
	
	/**
	 * Register required CSS and script files
	 */
	protected function registerClientScript()
	{
		$cs = Yii::app()->getClientScript();
		
		// Javascript needed for this widget
		$cs->registerCoreScript('jquery');
		$cs->registerScriptFile("{$this->assetsUrl}/js/dz-multiple-values.js");	
		$cs->registerCssFile("{$this->assetsUrl}/css/dz-multiple-values.css");	

		$options = '';
		if ( !empty($this->options) )
		{
			$options = CJavaScript::encode($this->options);
		}
		$cs->registerScript(__CLASS__ .'#'. $this->id, "jQuery('#{$this->htmlOptions['id']}').multipleValues(". CJavaScript::encode($this->options) .");", CClientScript::POS_READY);
	}
}