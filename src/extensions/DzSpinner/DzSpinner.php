<?php
/**
 * DzSpinner widget class file
 *
 * This extension is a wrapper for Spinner Fuel UX plugin (http://exacttarget.github.com/fuelux/#spinner)
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2013 Dezero
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package extensions.DzSpinner
 */

use dz\helpers\Html;
use dz\widgets\InputWidget;

/**
 * DzSpinner widget
 * @see http://exacttarget.github.com/fuelux/#spinner
 */
class DzSpinner extends InputWidget
{
	/**
	 * Fuel UX Spinner options
	 *
	 * @var array
	 */
	public $options = array();


	/**
	 * HTML attributes for the widget target
	 * 
	 * @var array
	 */
	public $targetHtmlOptions = array();
	
	
	/**
	 * Fuel UX Spinner options by default
	 * 
	 * @var array
	 */	
    // private $_defaultOptions = array();
	
	
	/**
	 * Initializes the widget
	 */
	public function init()
	{
		// Assets
		if ( $this->assetsPath === NULL )
		{
			$this->assetsPath = dirname(__FILE__) . DIRECTORY_SEPARATOR .'assets';
		}
		
		if ( $this->assetsUrl === NULL )
		{
			$this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
		}
		
		// HTML Options
		list($this->name, $this->id) = $this->resolveNameID();
		if ( !isset($this->htmlOptions['id']) )
		{
			$this->htmlOptions['id'] = $this->id .'-spinner';
			$this->targetHtmlOptions['id'] = $this->id;
		}
		if ( !isset($this->targetHtmlOptions['id']) )
		{
			$this->targetHtmlOptions['id'] = $this->id .'-target';
		}		
	}
	
	/**
	 * Runs the widget
	 */
	public function run()
	{
		if ( !isset($this->htmlOptions['class']) )
		{
			$this->htmlOptions['class'] = 'dz-spinner spinner input-group input-small';
		}
		else
		{
			$this->htmlOptions['class'] .= ' dz-spinner spinner input-group input-small';
		}
		
		if ( !isset($this->targetHtmlOptions['class']) )
		{
			$this->targetHtmlOptions['class'] = 'form-control spinner-input';
		}
		else
		{
			$this->targetHtmlOptions['class'] .= ' form-control spinner-input';
		}
		
		echo Html::openTag('div', $this->htmlOptions);

		if ( $this->hasModel() )
		{
			echo Html::activeTextField($this->model, $this->attribute, $this->targetHtmlOptions);
			$this->options['value'] = (int)CHtml::resolveValue($this->model, $this->attribute);
		}
		else
		{
			echo Html::textField($this->name, $this->value, $this->targetHtmlOptions);
			$this->options['value'] = (int)$this->value;
		}
		
		echo
			'<div class="spinner-buttons input-group-btn btn-group-vertical">
		        <a class="btn spinner-up btn-xs btn-default" href="javascript:void(0)">
		            <i class="fa fa-angle-up"></i>
		        </a>
		        <a class="btn spinner-down btn-xs btn-default" href="javascript:void(0)">
		            <i class="fa fa-angle-down"></i>
		        </a>
		    </div>
		</div>';


		$this->registerClientScript();
	}
	
	
	/**
	 * Register required CSS and script files
	 */
	protected function registerClientScript()
	{
		$cs = Yii::app()->getClientScript();

		// Build jQuery Raty options
		// $options = CMap::mergeArray($this->_defaultOptions, $this->options);
		/*
		if ( !isset($this->options['target']) )
		{
			$this->options['target'] = '#'. $this->targetHtmlOptions['id'];
		}

		if ( !empty($this->data) )
		{
			$this->options['hints'] = $this->data;
			$this->options['number'] = count($this->data);
			$this->options['targetType'] = 'hint';
		}
		*/
		
		// Global default options
		/*
		if ( ! $cs->isScriptRegistered(__CLASS__.'#defaults', CClientScript::POS_HEAD) )
		{
			$js_default = '';
			$defaultOptions = $this->_defaultOptions;
			unset($defaultOptions['target']);
			unset($defaultOptions['score']);
			foreach ( $defaultOptions as $id_option => $val_option ) 
			{
				$js_default .= "$.fn.raty.defaults.". $id_option ."=". CJavaScript::encode($val_option) ."; \n";
			}
			$cs->registerScript(__CLASS__.'#defaults', $js_default, CClientScript::POS_HEAD);
		}
		*/
		
		// Javascript needed for this widget
		$cs->registerCoreScript('jquery');
		$cs->registerScriptFile("{$this->assetsUrl}/js/fuelux.spinner.js");	
		$cs->registerCssFile("{$this->assetsUrl}/css/fuelux.spinner.css");	
		$cs->registerScript(__CLASS__ .'#'. $this->id, "jQuery('#{$this->htmlOptions['id']}').spinner(". CJavaScript::encode($this->options) .");", CClientScript::POS_READY);
	}
}