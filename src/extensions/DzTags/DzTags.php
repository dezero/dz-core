<?php
/**
 * DzTags widget class file
 *
 * This extension is a wrapper for jQuery Tags Manager plugin (http://welldonethings.com/tags/manager)
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2013 Dezero
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package extensions.DzTags
 * @package bootstrap.widgets
 */

use dz\helpers\Html;
use dz\widgets\InputWidget;

/**
 * DzTags widget
 * 
 * @see http://welldonethings.com/tags/manager
 */
class DzTags extends InputWidget
{
	/**
	 * jQuery Tags Manager version: [2, 3]
	 *
	 * @var integer
	 */
	public $version = 3;


	/**
	 * jQuery Tags options
	 *
	 * @var array
	 */
	public $options = array();
	

	/**
	 * User typeahead
	 *
	 * @var array
	 */
	public $typeahead = array(
		'active' => FALSE,
		'data' => array()
	);


	/**
	 * jQuery Tags options by default
	 * 
	 * @var array
	 */	
    private $_defaultOptions = array();
	
	
	/**
	 * Initializes the widget
	 */
	public function init()
	{
		// Assets
		if ( $this->assetsPath === NULL )
		{
			$this->assetsPath = dirname(__FILE__) . DIRECTORY_SEPARATOR .'assets';
		}
		
		if ( $this->assetsUrl === NULL )
		{
			$this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
		}
		
		// HTML Options
		list($this->name, $this->id) = $this->resolveNameID();
		if ( !isset($this->htmlOptions['id']) )
		{
			$this->htmlOptions['id'] = $this->id .'-tag';
		}

		// Default jQuery Tags Options
		$this->_defaultOptions = array(
			// 'path' => $this->assetsUrl .'/img',
			'preventSubmitOnEnter' => FALSE,
			'deleteTagsOnBackspace' => FALSE,
			'hiddenTagListName' => $this->name .'[taglist]',		// Hidden input name will be $this->id+"-taglist"
    		'tagsContainer' => "#". $this->htmlOptions['id'] .'container',	// Tag container (push tags after input)
			// 'typeahead' => TRUE,
    		// 'typeaheadAjaxSource' => null,
    		// 'typeaheadSource' => array("Pisa", "Rome", "Milan", "Florence", "New York", "Paris", "Berlin", "London", "Madrid"),
		);
	}
	

	/**
	 * Runs the widget
	 */
	public function run()
	{
		$default_class = 'tm-input';
		if ( $this->version == 2 )
		{
			$default_class = 'tagManager';
		}

		// Class
		if ( !isset($this->htmlOptions['class']) )
		{
			$this->htmlOptions['class'] = 'dz-tags '. $default_class;
		}
		else
		{
			$this->htmlOptions['class'] .= ' dz-tags '. $default_class;
		}

		// Typeahead
		if ( !empty($this->typeahead) AND isset($this->typeahead['active']) AND $this->typeahead['active'] )
		{
			$this->htmlOptions['class'] .= ' tm-input-typeahead';
		}

		if ( !isset($this->htmlOptions['maxlength']) )
		{
			$this->htmlOptions['maxlength'] = 255;
		}

		if ( !isset($this->htmlOptions['autocomplete']) )
		{
			$this->htmlOptions['autocomplete'] = 'off';
		}
		
		/*
		if ( !isset($this->htmlOptions['prepend']) )
		{
			$this->htmlOptions['prepend'] = '<i class="fa fa-tag"></i>';
		}
		echo $this->getPrepend();
		*/
		echo '<div class="input-group dz-tags-input-group"><span class="input-group-addon"><i class="fa fa-tag"></i></span>';


		//echo '<span class="add-on"><i class="icon fa-tag"></i></span>';
		if ( $this->hasModel() )
		{
			echo Html::activeTextField($this->model, $this->attribute, $this->htmlOptions);
		}
		else
		{
			// Values will be stored as this Javascript widget prefilled option like.
			// Ex.: tagsManager({prefilled: ["Angola", "Laos", "Nepal"]});
			if ( !empty($this->value) )
			{
				if ( isset($this->options['prefilled']) AND !empty($this->options['prefilled']) )
				{
					$this->options['prefilled'] = CMap::mergeArray($this->options['prefilled'], $this->value);
				}
				else
				{
					$this->options['prefilled'] = $this->value;
				}
			}
			// echo Html::textField($this->name, $this->value, $this->htmlOptions);
			
			if ( !preg_match("/\[/", $this->name) )
			{
				$this->name = $this->name ."[tags]";
			}
			echo Html::textField($this->name, '', $this->htmlOptions);
		}
		// echo $this->getAppend();
		echo '</div>';

		// Tag container
		echo Html::openTag('div', array(
			'id' => $this->htmlOptions['id'] .'container',
			'class' => 'dz-tags-container input-group'
		)) . '</div>';

		// Javascript files
		$this->registerClientScript();
	}
	
	
	/**
	 * Register required CSS and script files
	 */
	protected function registerClientScript()
	{
		$cs = Yii::app()->getClientScript();

		// Global default options
		if ( !empty($this->_defaultOptions) AND ! $cs->isScriptRegistered(__CLASS__.'#defaults', CClientScript::POS_HEAD) )
		{
			$js_default = '';
			foreach ( $this->_defaultOptions as $id_option => $val_option ) 
			{
				$js_default .= "$.fn.tagsManager.". $id_option ."=". CJavaScript::encode($val_option) ."; \n";
			}
			$cs->registerScript(__CLASS__.'#defaults', $js_default, CClientScript::POS_END);
		}
		
		// CSS & Javascript needed for this widget
		$cs->registerCoreScript('jquery');
		switch ( $this->version )
		{
			case 2:
				$cs->registerCssFile("{$this->assetsUrl}/css/bootstrap-tagmanager.css");
				$cs->registerScriptFile("{$this->assetsUrl}/js/bootstrap-tagmanager.js");

				// Javascript needed for this widget
				$options = CMap::mergeArray($this->_defaultOptions, $this->options);
				$cs->registerScript(__CLASS__ .'#'. $this->id, 
					"jQuery('#{$this->htmlOptions['id']}').tagsManager(". CJavaScript::encode($options) .").on('blur', function(){
						// Let's simulate an ENTER keypress --> http://bresleveloper.blogspot.co.il/2013/03/jsjq-simulate-enter-event.html
						var press = jQuery.Event('keypress');
						press.bubbles = true;
						press.cancelable = true;
						press.charCode = 13;
						press.currentTarget = $(this)[0];
						press.eventPhase = 2;
						press.keyCode = 13;
						press.returnValue = true;
						press.srcElement = $(this)[0];
						press.target = $(this)[0];
						press.type = 'keypress';
						press.view = Window;
						press.which = 13;
						$(this).trigger(press);
					});",
					CClientScript::POS_READY);
			break;

			case 3:
			default:
				$cs->registerCssFile("{$this->assetsUrl}/css/tagmanager-3.css");
				$cs->registerScriptFile("{$this->assetsUrl}/js/tagmanager-3.js");

				$js_var = str_replace("-", "_", $this->htmlOptions['id']);
				// Javascript needed for this widget
				$options = CMap::mergeArray($this->_defaultOptions, $this->options);
				$cs->registerScript(__CLASS__ .'#'. $this->id, 
					"var tag_api_{$js_var} = jQuery('#{$this->htmlOptions['id']}').tagsManager(". CJavaScript::encode($options) .").on('blur', function(){
						// Let's simulate an ENTER keypress --> http://bresleveloper.blogspot.co.il/2013/03/jsjq-simulate-enter-event.html
						var press = jQuery.Event('keypress');
						press.bubbles = true;
						press.cancelable = true;
						press.charCode = 13;
						press.currentTarget = $(this)[0];
						press.eventPhase = 2;
						press.keyCode = 13;
						press.returnValue = true;
						press.srcElement = $(this)[0];
						press.target = $(this)[0];
						press.type = 'keypress';
						press.view = Window;
						press.which = 13;
						$(this).trigger(press);
						$(this).typeahead('val', '');
					});",
					CClientScript::POS_READY);

				// Enable Twitter Typeahead library
				if ( !empty($this->typeahead) AND isset($this->typeahead['active']) AND $this->typeahead['active'] )
				{
					$cs->registerScriptFile("{$this->assetsUrl}/js/twitter-typeahead.bundle.min.js");
					$cs->registerScript(__CLASS__ .'.typeahead#'. $this->id, 
						"var typeahead_names_{$js_var} = ". CJSON::encode($this->typeahead['source']) .";
						var typeahead_suggestion_{$js_var} = new Bloodhound({
							datumTokenizer: Bloodhound.tokenizers.whitespace,
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							local: typeahead_names_{$js_var}
						});

						jQuery('#{$this->htmlOptions['id']}').typeahead(null, {
							name: 'typeahead_suggestion_{$js_var}',
							source: typeahead_suggestion_{$js_var}.ttAdapter()
						}).on('typeahead:selected', function (e, d) {
							tag_api_{$js_var}.tagsManager('pushTag', d);
							jQuery('#{$this->htmlOptions['id']}').typeahead('val', '');
						// First selected
						// }).on('typeahead:render', function(e) {
						//    jQuery('#{$this->htmlOptions['id']}').parent().find('.tt-selectable:first').addClass('tt-cursor');
						});
						",
						CClientScript::POS_READY);
				}
			break;
		}
	}
}