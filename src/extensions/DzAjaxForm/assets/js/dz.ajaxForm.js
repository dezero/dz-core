$(function () {
	// dzAjaxFormSubmit ==============================================
	$.fn.dzAjaxFormSubmit = function(options) {
		function init() {
			$base.on('submit', function(){
				$(this).ajaxSubmit(settings);
				return false;
			});

			$base.find('.btn-close').on('click', function(){
				$modal.modal("hide");
			});
		}

		// Show response for AjaxGridView
		function show_response(data) {
			if ( data.result == 'error' ) {
				$base.parent().parent().html(data.content);
			}
			else if ( data.result == 'success' ) {
				$.pnotify({
					sticker: false,
					text: data.message,
					type: 'success'
				});
				if ($modal.length > 0) {
					$modal.modal("hide");
					$.dzAjaxGridRefresh($base.data('grid_id'));
				}
			}
		}

		var $base = $(this);
		var settings = {
			dataType: 'json',
			success: show_response,
			modal_id: 'dz-modal-ajax-grid'
		};
		if ( $(this).size() > 0 ) {
			if ( ! $.isEmptyObject(options) ) {
				$.extend(settings, options);
			}
			var $modal = $("#"+ settings.modal_id);
			init();
		}
	};


	// This function hijacks regular form submission and turns it into ajax submission with jquery form plugin.
	$.dzAjaxAfterValidate = function(form, data, hasError) {
		if (!hasError) {
			$(form).dzAjaxFormSubmit();
		}
		return false;
	};
});