<?php
/**
 * DzAjaxForm widget class file
 *
 * This extension adds AJAX behavior on Bootstrap BsActiveForm widget
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2013 Dezero
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */

// Import BsActiveForm
Yii::import('@bootstrap.widgets.BsActiveForm');

/**
 * DzAjaxForm
 */
class DzAjaxForm extends BsActiveForm
{	
	/**
	 * File path to assets
	 *
	 * @var string
	 */
	protected $assetsPath;


	/**
	 * URL to assets
	 *
	 * @var string
	 */
	protected $assetsUrl;
	

	/**
	 * AJAX options
	 *
	 * @var array
	 */
	public $dzAjaxOptions;


	/**
	 * Initializes the widget
	 */
	public function init()
	{
		parent::init();

		// Assets
		if ( $this->assetsPath === NULL )
		{
			$this->assetsPath = dirname(__FILE__) . DIRECTORY_SEPARATOR .'assets';
		}
		
		if ( $this->assetsUrl === NULL )
		{
			$this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
		}

		// Javascript files
		$this->registerClientScript();
	}

	
	/**
	 * Register required CSS and script files
	 */
	public function registerClientScript()
	{
		// Javascript needed for this widget
		// $cs = Yii::app()->getClientScript();
		// $cs->registerCoreScript('jquery');
		// $cs->registerScriptFile("{$this->assetsUrl}/js/jquery.form.js", CClientScript::POS_HEAD);
		// $cs->registerScriptFile("{$this->assetsUrl}/js/dz.ajaxForm.js", CClientScript::POS_HEAD);

		$ajax_options = '';
		$this->dzAjaxOptions['action'] = 'init';
		if ( !empty($this->dzAjaxOptions) )
		{
			$ajax_options = CJavaScript::encode($this->dzAjaxOptions);
		}

		// Button links show a Bootstrap modal
		Yii::app()->getClientScript()->registerScript(__CLASS__.'#'. $this->id, "jQuery('#{$this->id}').dzAjaxFormSubmit(". $ajax_options .");", CClientScript::POS_READY);
	}
}