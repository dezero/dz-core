<?php
/**
 * InputWidgetMask widget class file
 *
 * This extension is a wrapper for Jasny Input Mask plugin (http://jasny.github.com/bootstrap/javascript.html#inputmask)
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2013 Dezero
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package extensions.DzInputWidgetMask
 */

use dz\helpers\Html;
use dz\widgets\InputWidget;

/**
 * InputWidgetMask widget
 * @see http://jasny.github.com/bootstrap/javascript.html#inputmask
 */
class DzInputWidgetMask extends InputWidget
{
	/**
	 * A string of formatting and literal characters, defining the input mask
	 *
	 * @var string
	 */
	public $mask = '';
	
	
	/**
	 * Jasny Input Mask plugin options
	 *
	 * @var array
	 */
	public $options = array();
	

	/**
	 * Field type: descuento
	 *
	 * @var string
	 */
	public $field_type = '';
	
	/**
	 * Initializes the widget
	 */
	public function init()
	{
		// Assets
		if ( $this->assetsPath === NULL )
		{
			$this->assetsPath = dirname(__FILE__) . DIRECTORY_SEPARATOR .'assets';
		}
		
		if ( $this->assetsUrl === NULL )
		{
			$this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
		}
		
		// HTML Options
		list($this->name, $this->id) = $this->resolveNameID();
		if ( !isset($this->htmlOptions['id']) )
		{
			$this->htmlOptions['id'] = $this->id .'-inputmask';
		}
	}
	
	/**
	 * Runs the widget
	 */
	public function run()
	{
		// Class
		if ( !isset($this->htmlOptions['class']) )
		{
			$this->htmlOptions['class'] = 'dz-inputmask inputmask';
		}
		else
		{
			$this->htmlOptions['class'] .= ' dz-inputmask inputmask';
		}
		
		// Mask
		if ( isset($this->mask) )
		{
			$this->htmlOptions['data-mask'] = $this->mask;
		}		

		echo $this->getPrepend();
		if ( $this->hasModel() )
		{
			echo Html::activeTextField($this->model, $this->attribute, $this->htmlOptions);
		}
		else
		{
			echo Html::textField($this->name, $this->value, $this->htmlOptions);
		}
		echo $this->getAppend();

		// Javascript files
		$this->registerClientScript();
	}
	
	
	/**
	 * Register required CSS and script files
	 */
	protected function registerClientScript()
	{
		$cs = Yii::app()->getClientScript();

		// Javascript needed for this widget
		$cs->registerCoreScript('jquery');
		$cs->registerScriptFile("{$this->assetsUrl}/js/bootstrap-inputmask" . (YII_DEBUG ? '' : '.min') . ".js");
		
		$options = '';
		if ( !empty($this->options) )
		{
			$options = CJavaScript::encode($this->options);
		}
		else if ( isset($this->mask) )
		{
			$options = CJavascript::encode(array('mask' => $this->mask));
		}
		$cs->registerScript(__CLASS__ .'#'. $this->id, "jQuery('#{$this->htmlOptions['id']}').inputmask(". $options .");", CClientScript::POS_READY);

		if ( $this->field_type == 'descuento' )
		{
			$cs->registerScript(__CLASS__ .'#'. $this->id, "jQuery('#{$this->htmlOptions['id']}').dzDescuento();", CClientScript::POS_READY);
		}
	}
}