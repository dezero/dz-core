/* =============================================================
 * bootstrap-dztypeahead.js v2.3.1
 * http://twitter.github.com/bootstrap/javascript.html#dztypeahead
 * =============================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function($){

  "use strict"; // jshint ;_;


 /* TYPEAHEAD PUBLIC CLASS DEFINITION
  * ================================= */

  var DzTypeahead = function (element, options) {
    this.$element = $(element)
    this.options = $.extend({}, $.fn.dztypeahead.defaults, options)
    this.matcher = this.options.matcher || this.matcher
    this.sorter = this.options.sorter || this.sorter
    this.highlighter = this.options.highlighter || this.highlighter
    this.updater = this.options.updater || this.updater
    this.source = this.options.source
    this.$menu = $(this.options.menu)
    this.shown = false
    this.listen()
  }

  DzTypeahead.prototype = {

    constructor: DzTypeahead

  , select: function () {
    // <DEZERO>
    var $active = this.$menu.find('.active')
    if ( typeof $active.data('url') != 'undefined' ) {
      // window.location = $active.data('url')
      window.open($active.data('url'), '_blank');
    }
    else
    {
      var val = this.$menu.find('.active').attr('data-value')
      this.$element
        .val(this.updater(val))
        .change()
    }
      return this.hide()
    }

  , updater: function (item) {
      return item
    }

  , show: function () {
      var pos = $.extend({}, this.$element.position(), {
        height: this.$element[0].offsetHeight
      })

      this.$menu
        .insertAfter(this.$element)
        .css({
          top: pos.top + pos.height
        , left: pos.left
        })
        .show()

      this.shown = true
      return this
    }

  , hide: function () {
      this.$menu.hide()
      this.shown = false
      return this
    }

  , lookup: function (event) {
      var items

      this.query = this.$element.val()

      // <DEZERO>. Allow search by id with no "minLenght" restriction
      // if (!this.query || this.query.length < this.options.minLength) {
      if (!this.query || ( this.query.length < this.options.minLength && /^\+?\d+$/.test(this.query) == false) ) {
        return this.shown ? this.hide() : this
      }

      items = $.isFunction(this.source) ? this.source(this.query, $.proxy(this.process, this)) : this.source

      return items ? this.process(items) : this
    }

  // <DEZERO> PROCESS REDEFINITION
  , process: function (items) {
      var that = this
      var output = ""

      that.$menu.html("")
      if ( items.results.length == 0 ) {
        var li = $("<li>", {"class" : "search-message", "html" : items.message})
        that.$menu.append(li)
        that.show()
        return that
      }
      else
      {
        $.each(items.results, function(group, vec_data) {
          var vec_items = [], vec_ids = []
          $.each(vec_data.items, function(k, item) {
            var que_output = item.id+' - '+item.name;
            if ( item.description !== "" ) {
              que_output += "<small class='muted'>"+ item.description +"</small>";
            }
            vec_items.push(que_output);

            vec_ids.push(item.id);
          });

          vec_items = $.grep(vec_items, function (item) {
            return that.matcher(item)
          })

          // vec_items = that.sorter(vec_items)

          /*
          if (!vec_items.length) {
            return that.shown ? that.hide() : that
          }
          */
         
          if (vec_items.length) {
            var li = $("<li>", {"class" : "search-title"})
            li.append($("<span>", {"html" : vec_data.title}))
            that.$menu.append(li)
            output += that.render(vec_items.slice(0, that.options.items), vec_data.view_url, vec_ids).show()
            li = $("<li>", {"class" : "search-item", "data-url" : vec_data.list_url.slice(0, -1) + that.query, "data-value" : "all"})
            li.append($("<a>", {"html" : "Ver todos los resultados de <strong>\""+ that.query +"\"</strong>...", "href" : "#", "target": "_blank", "class" : "search-all"}))
            that.$menu.append(li)
          }
          else {
            var li = $("<li>", {"class" : "search-title"})
            li.append($("<span>", {"html" : vec_data.title}))
            that.$menu.append(li)
            output += that.show()
            li = $("<li>", {"class" : "search-item", "data-url" : vec_data.list_url.slice(0, -1) + that.query, "data-value" : "all"})
            li.append($("<a>", {"html" : "Ver todos los resultados de <strong>\""+ that.query +"\"</strong>...", "href" : "#", "target": "_blank", "class" : "search-all"}))
            that.$menu.append(li)
          }
        });
        return output
      }
    }
  // <DEZERO> PROCESS REDEFINITION

  , matcher: function (item) {
      return ~item.toLowerCase().indexOf(this.query.toLowerCase())
    }

  , sorter: function (items) {
      var beginswith = []
        , caseSensitive = []
        , caseInsensitive = []
        , item

      while (item = items.shift()) {
        if (!item.toLowerCase().indexOf(this.query.toLowerCase())) beginswith.push(item)
        else if (~item.indexOf(this.query)) caseSensitive.push(item)
        else caseInsensitive.push(item)
      }

      return beginswith.concat(caseSensitive, caseInsensitive)
    }

  , highlighter: function (item) {
      var query = this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
      return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
        return '<strong>' + match + '</strong>'
      })
    }

  , render: function (items, link, vec_ids) {
      var that = this

      items = $(items).map(function (i, item) {
        i = $(that.options.item).attr('data-value', item).attr('data-url', link+'?id='+vec_ids[i]).attr('data-id', vec_ids[i])
        i.find('a').html(that.highlighter(item))
        return i[0]
      })

      // items.first().addClass('active')
      // this.$menu.html(items)
      this.$menu.append(items)
      return this
    }

  , next: function (event) {
      var active = this.$menu.find('.active').removeClass('active')
        , next = active.next()

      if (!next.length) {
        next = $(this.$menu.find('li')[0])
      }

      next.addClass('active')
    }

  , prev: function (event) {
      var active = this.$menu.find('.active').removeClass('active')
        , prev = active.prev()

      if (!prev.length) {
        prev = this.$menu.find('li').last()
      }

      prev.addClass('active')
    }

  , listen: function () {
      this.$element
        .on('focus',    $.proxy(this.focus, this))
        .on('blur',     $.proxy(this.blur, this))
        .on('keypress', $.proxy(this.keypress, this))
        .on('keyup',    $.proxy(this.keyup, this))

      if (this.eventSupported('keydown')) {
        this.$element.on('keydown', $.proxy(this.keydown, this))
      }

      this.$menu
        .on('click', $.proxy(this.click, this))
        .on('mouseenter', 'li', $.proxy(this.mouseenter, this))
        .on('mouseleave', 'li', $.proxy(this.mouseleave, this))
    }

  , eventSupported: function(eventName) {
      var isSupported = eventName in this.$element
      if (!isSupported) {
        this.$element.setAttribute(eventName, 'return;')
        isSupported = typeof this.$element[eventName] === 'function'
      }
      return isSupported
    }

  , move: function (e) {
      if (!this.shown) return

      switch(e.keyCode) {
        case 9: // tab
        case 13: // enter
        case 27: // escape
          e.preventDefault()
          break

        case 38: // up arrow
          e.preventDefault()
          this.prev()
          break

        case 40: // down arrow
          e.preventDefault()
          this.next()
          break
      }

      e.stopPropagation()
    }

  , keydown: function (e) {
      // this.suppressKeyPressRepeat = ~$.inArray(e.keyCode, [40,38,9,13,27])
      this.suppressKeyPressRepeat = ~$.inArray(e.keyCode, [40,38,9,27])
      this.move(e)
    }

  , keypress: function (e) {
      if (this.suppressKeyPressRepeat) return
      this.move(e)
    }

  , keyup: function (e) {
      switch(e.keyCode) {
        case 40: // down arrow
        case 38: // up arrow
        case 16: // shift
        case 17: // ctrl
        case 18: // alt
          break

        case 9: // tab
        // case 13: // enter
          if (!this.shown) return
          this.select()
          break

        case 27: // escape
          if (!this.shown) return
          this.hide()
          break

        default:
          this.lookup()
      }

      e.stopPropagation()
      e.preventDefault()
  }

  , focus: function (e) {
      this.focused = true
    }

  , blur: function (e) {
      this.focused = false
      if (!this.mousedover && this.shown) this.hide()
    }

  , click: function (e) {
      e.stopPropagation()
      e.preventDefault()
      this.select()
      this.$element.focus()
    }

  , mouseenter: function (e) {
      this.mousedover = true
      this.$menu.find('.active').removeClass('active')
      $(e.currentTarget).addClass('active')
    }

  , mouseleave: function (e) {
      this.mousedover = false
      if (!this.focused && this.shown) this.hide()
    }

  }


  /* TYPEAHEAD PLUGIN DEFINITION
   * =========================== */

  var old = $.fn.dztypeahead

  $.fn.dztypeahead = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('typeahead')
        , options = typeof option == 'object' && option
      if (!data) $this.data('typeahead', (data = new DzTypeahead(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.dztypeahead.defaults = {
    source: []
  , items: 8
  , menu: '<ul class="typeahead dropdown-menu"></ul>'
  , item: '<li class="search-item"><a href="#"></a></li>'
  , minLength: 1
  }

  $.fn.dztypeahead.Constructor = DzTypeahead


 /* TYPEAHEAD NO CONFLICT
  * =================== */

  $.fn.dztypeahead.noConflict = function () {
    $.fn.dztypeahead = old
    return this
  }


 /* TYPEAHEAD DATA-API
  * ================== */

  $(document).on('focus.typeahead.data-api', '[data-provide="typeahead"]', function (e) {
    var $this = $(this)
    if ($this.data('typeahead')) return
    $this.dztypeahead($this.data())
  })

}(window.jQuery);
