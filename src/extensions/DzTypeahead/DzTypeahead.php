<?php
/**
 * DzTypeahead widget class file
 *
 * This extension is a wrapper for Bootstrap Typeahead plugin (http://twitter.github.com/bootstrap/javascript.html#typeahead)
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2013 Dezero
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package extensions.DzSpinner
 */

use dz\widgets\InputWidget;

/**
 * Bootstrap typeahead widget.
 * @see http://twitter.github.com/bootstrap/javascript.html#typeahead
 */
class DzTypeahead extends InputWidget
{
	/**
	 * @var array the options for the Bootstrap Javascript plugin.
	 */
	public $options = array();

	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		// Assets
		if ( $this->assetsPath === NULL )
		{
			$this->assetsPath = dirname(__FILE__) . DIRECTORY_SEPARATOR .'assets';
		}
		
		if ( $this->assetsUrl === NULL )
		{
			$this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
		}

		$this->htmlOptions['type'] = 'text';
		$this->htmlOptions['data-provide'] = 'typeahead';
	}

	/**
	 * Runs the widget.
	 */
	public function run()
	{
		list($name, $id) = $this->resolveNameID();

		if ( !isset($this->htmlOptions['autocomplete']) )
		{
			$this->htmlOptions['autocomplete'] = 'off';
		}

		if (isset($this->htmlOptions['id']))
		{
			$id = $this->htmlOptions['id'];
		}
		else
		{
			$this->htmlOptions['id'] = $id;
		}

		if (isset($this->htmlOptions['name']))
		{
			$name = $this->htmlOptions['name'];
		}

		if ( $this->hasModel() )
		{
			echo CHtml::activeTextField($this->model, $this->attribute, $this->htmlOptions);
		}
		else
		{
			echo CHtml::textField($name, $this->value, $this->htmlOptions);
		}

		// Javascript code
		// Yii::app()->clientScript->registerScriptFile("{$this->assetsUrl}/js/bootstrap-typeahead" . (YII_DEBUG ? '' : '.min') . ".js");
		Yii::app()->clientScript->registerScriptFile("{$this->assetsUrl}/js/bootstrap-typeahead.js");
		$options = !empty($this->options) ? CJavaScript::encode($this->options) : '';
		Yii::app()->clientScript->registerScript(__CLASS__.'#'.$id, "jQuery('#{$id}').typeahead({$options});");
	}
}
