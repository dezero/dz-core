<?php
/**
 * DzPrettyPhoto widget class file
 *
 * This extension is a wrapper for PrettyPhoto extension plugin (http://www.no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/)
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2013 Dezero
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */

use dz\helpers\Html;

/**
 * PrettyPhoto widget
 * @see http://www.no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/
 */
class DzPrettyPhoto extends CWidget
{
	/**
	 * File path to assets
	 *
	 * @var string
	 */
	protected $assetsPath;


	/**
	 * URL to assets
	 *
	 * @var string
	 */
	protected $assetsUrl;
	

	/**
	 * URL path for thumbnail image
	 *
	 * @var string
	 */
	public $thumb_path;


	/**
	 * URL path for original image
	 *
	 * @var string
	 */
	public $image_path;


	/**
	 * Image title
	 *
	 * @var string
	 */
	public $title = '';


	/**
	 * Image description
	 *
	 * @var string
	 */
	public $description = '';


	/**
	 * Gallery name. Useful for joining images
	 *
	 * @var string
	 */
	public $gallery;
	

	/**
	 * HTML attributes for the link tag
	 * 
	 * @var array
	 */
	public $linkOptions = array();


	/**
	 * HTML attributes for the image tag
	 * 
	 * @var array
	 */
	public $thumbOptions = array();


	/**
	 * Pretty Photo plugin options
	 *
	 * @var array
	 */
	public $options = array();


	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		// Assets
		if ( $this->assetsPath === NULL )
		{
			$this->assetsPath = dirname(__FILE__) . DIRECTORY_SEPARATOR .'assets';
		}
		
		if ( $this->assetsUrl === NULL )
		{
			$this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
		}
		
		// Link Options
		if (!isset($this->linkOptions['id']))
		{
			$this->linkOptions['id'] = $this->getId();
		}

		$this->linkOptions['href'] = $this->image_path;
		$this->linkOptions['rel'] = 'prettyPhoto';
		if ( !empty($this->gallery) )
		{
			$this->linkOptions['rel'] .= '['. $this->gallery .']';
		}

		// Thumb options
		$this->thumbOptions['src'] = $this->thumb_path;

		// Title?
		if ( !empty($this->title) )
		{
			$this->thumbOptions['alt'] = $this->title;
		}

		// Description
		if ( !empty($this->description) )
		{
			$this->linkOptions['title'] = $this->description;
		}
	}


	/**
	 * Runs the widget.
	 */
	public function run()
	{
		echo Html::openTag('a', $this->linkOptions);
		echo Html::openTag('img', $this->thumbOptions);
		echo '</img></a>';

		// Javascript files
		$this->registerClientScript();
	}


	/**
	 * Register required CSS and script files
	 */
	protected function registerClientScript()
	{
		// CSS & Javascript needed for this widget
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile("{$this->assetsUrl}/css/prettyPhoto.css");
		$cs->registerCoreScript('jquery');
		$cs->registerScriptFile("{$this->assetsUrl}/js/jquery.prettyPhoto.js");

		// Force some options
		$this->options['social_tools'] = '';
		$this->options['deeplinking'] = FALSE;

		$options = CJavaScript::encode($this->options);

		$cs->registerScript(__CLASS__, "jQuery(\"a[rel^='prettyPhoto']\").prettyPhoto(". $options .");", CClientScript::POS_READY);
	}
}