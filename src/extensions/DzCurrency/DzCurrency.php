<?php
/**
 * DzCurrency widget class file
 *
 * This extension is a wrapper for jQuery Format Currency plugin (http://code.google.com/p/jquery-formatcurrency/)
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2013 Dezero
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package extensions.InputWidgetMask
 */

use dz\helpers\Html;
use dz\widgets\InputWidget;

/**
 * DzCurrency widget
 * @see http://code.google.com/p/jquery-formatcurrency
 */
class DzCurrency extends InputWidget
{	
	/**
	 * Plugin options
	 *
	 * @var array
	 */
	public $options = array();
	
	
	/**
	 * Initializes the widget
	 */
	public function init()
	{
		// Assets
		if ( $this->assetsPath === NULL )
		{
			$this->assetsPath = dirname(__FILE__) . DIRECTORY_SEPARATOR .'assets';
		}
		
		if ( $this->assetsUrl === NULL )
		{
			$this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
		}
		
		// HTML Options
		list($this->name, $this->id) = $this->resolveNameID();
		if ( !isset($this->htmlOptions['id']) )
		{
			$this->htmlOptions['id'] = $this->id .'-currency';
		}
		
		// Region
		$this->options['region'] = 'es';
	}
	
	/**
	 * Runs the widget
	 */
	public function run()
	{
		// Class
		if ( !isset($this->htmlOptions['class']) )
		{
			$this->htmlOptions['class'] = 'dz-currency currency';
		}
		else
		{
			$this->htmlOptions['class'] .= ' dz-currency currency';
		}
				
		echo $this->getPrepend();
		if ( $this->hasModel() )
		{
			echo Html::activeTextField($this->model, $this->attribute, $this->htmlOptions);
		}
		else
		{
			echo Html::textField($this->name, $this->value, $this->htmlOptions);
		}
		echo $this->getAppend();

		// Javascript files
		$this->registerClientScript();
	}
	
	
	/**
	 * Register required CSS and script files
	 */
	protected function registerClientScript()
	{
		$cs = Yii::app()->getClientScript();

		// Javascript needed for this widget
		$cs->registerCoreScript('jquery');
		$cs->registerScriptFile("{$this->assetsUrl}/js/jquery.formatCurrency-1.4.0" . (YII_DEBUG ? '' : '.min') . ".js");
		$cs->registerScriptFile("{$this->assetsUrl}/js/jquery.formatCurrency.es.js");
		
		$options = '';
		if ( !empty($this->options) )
		{
			$options = CJavaScript::encode($this->options);
		}
		$cs->registerScript(__CLASS__ .'#'. $this->id, "jQuery('#{$this->htmlOptions['id']}').on('blur', function() { jQuery('#{$this->htmlOptions['id']}').formatCurrency(". $options ."); });", CClientScript::POS_READY);
	}
}