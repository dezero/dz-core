/**
 * Custom jQuery Timer by Dezero
 */
(function($) {
	// Timer  ==============================================
	$.fn.dzTimer = function() {
		function init() {
			$.dzTimerClock($base.attr('id'));
			$base.knob({
				readOnly: true,
				thickness: 0.3
			});
			$.dzTimerUpdate($base.attr('id'));
		}

		var $base = $(this);

		if ($(this).size() > 0) {
			init();
		}
	};

	$.dzTimerClock = function(que_id) {
		var $base = $("#" + que_id);
		var n = new Date(), v = new Date(), current_time = parseInt($base.val());
		var new_time = current_time - Math.round((n.getTime()-v.getTime())/1000);

		$("#" + que_id +"-timer").html(format_time(new_time));
		$base.val(current_time+1).trigger("change");
		window.setTimeout("$.dzTimerClock('"+ que_id +"');", 1000);

		function format_time(que_time) {
			var que_hour = 0, que_min = 0, que_sec = que_time;
			if (que_sec > 59) {
				que_min = Math.floor(que_sec/60);
				que_sec = que_sec - que_min*60;
			}
			if (que_min > 59) {
				que_hour = Math.floor(que_min/60);
				que_min = que_min - que_hour*60;
			}
			if (que_sec < 10){
				que_sec = "0"+que_sec;
			}
			if (que_min < 10) {
				que_min = "0"+que_min;
			}
			if (que_hour < 10) {
				que_hour = "0"+que_hour;
			}
			return que_hour+":"+que_min+":"+que_sec;
		}
	};

	$.dzTimerUpdate = function(que_id) {
		var $base = $("#" + que_id);
		if ( $base.data('model') == 'Reparacion' ) {
			var que_data = {Reparacion: {reparacion_id: $base.data('id')}};
		}
		else if ( $base.data('model') == 'Presupuesto' ) {
			var que_data = {Presupuesto: {presupuesto_id: $base.data('id')}};
		}
		$.ajax({
			url: $base.data('url'),
			type: "post",
			dataType: "json",
			data: que_data,
			success: function(data) {
				$base.val(data.tiempo * 60).trigger("change");
				window.setTimeout("$.dzTimerUpdate('"+ que_id +"');", 60000);
			},
			error: function(request, status, error) {
				alert('Código incorrecto. \n\nERROR: '+request.responseText);
			},
			cache: false
		});
	};
})(jQuery);
