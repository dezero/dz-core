<?php
/**
 * DzTimer widget class file
 *
 * This extension is a wrapper for a timer custom plugin using jQuery Knob (https://github.com/aterrien/jQuery-Knob)
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2013 Dezero
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package extensions.Dztimer
 */

use dz\helpers\Html;
use dz\widgets\InputWidget;

/**
 * DzCurrency widget
 * @see http://code.google.com/p/jquery-formatcurrency
 */
class DzTimer extends InputWidget
{	
	/**
	 * Plugin options
	 *
	 * @var array
	 */
	public $options = array();
	
	
	/**
	 * HTML attributes for the widget target
	 * 
	 * @var array
	 */
	public $targetHtmlOptions = array();


	/**
	 * Initializes the widget
	 */
	public function init()
	{
		// Assets
		if ( $this->assetsPath === NULL )
		{
			$this->assetsPath = dirname(__FILE__) . DIRECTORY_SEPARATOR .'assets';
		}
		
		if ( $this->assetsUrl === NULL )
		{
			$this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
		}
		
		// HTML Options
		list($this->name, $this->id) = $this->resolveNameID();
		if ( !isset($this->htmlOptions['id']) )
		{
			$this->htmlOptions['id'] = $this->id;
			$this->targetHtmlOptions['id'] = $this->id .'-timer';
		}
	}
	

	/**
	 * Runs the widget
	 */
	public function run()
	{
		// Classes
		if ( !isset($this->htmlOptions['class']) )
		{
			$this->htmlOptions['class'] = 'dz-timer timer';
		}
		else
		{
			$this->htmlOptions['class'] .= ' dz-timer timer';
		}

		if ( !isset($this->targetHtmlOptions['class']) )
		{
			$this->targetHtmlOptions['class'] = 'dz-timer-target timer-target';
		}
		else
		{
			$this->targetHtmlOptions['class'] .= ' dz-timer-target timer-target';
		}
		
		echo $this->getPrepend();
		if ( $this->hasModel() )
		{
			echo Html::activeTextField($this->model, $this->attribute, $this->htmlOptions);
		}
		else
		{
			echo Html::textField($this->name, $this->value, $this->htmlOptions);
		}
		echo $this->getAppend();

		// Target
		echo Html::openTag('div', $this->targetHtmlOptions) . '</div>';

		// Javascript files
		$this->registerClientScript();
	}
	
	
	/**
	 * Register required CSS and script files
	 */
	protected function registerClientScript()
	{
		$cs = Yii::app()->getClientScript();

		// Javascript needed for this widget
		$cs->registerCoreScript('jquery');
		// $cs->registerScriptFile("{$this->assetsUrl}/js/jquery.knob" . (YII_DEBUG ? '' : '.min') . ".js");
		// $cs->registerScriptFile("{$this->assetsUrl}/js/jquery.timer" . (YII_DEBUG ? '' : '.min') . ".js");
		$cs->registerScriptFile("{$this->assetsUrl}/js/jquery.knob.js");
		$cs->registerScriptFile("{$this->assetsUrl}/js/jquery.timer.js");

		$options = '';
		if ( !empty($this->options) )
		{
			$options = CJavaScript::encode($this->options);
		}

		$cs->registerScript(__CLASS__ .'#'. $this->id, "jQuery('#{$this->htmlOptions['id']}').dzTimer();", CClientScript::POS_READY);
	}
}