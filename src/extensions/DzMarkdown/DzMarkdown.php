<?php
/**
 * DzMarkdown widget class file
 *
 * This extension is a wrapper for SimpleMDE Markdown Editor (https://simplemde.com/)
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2019 Dezero
 */

use dz\helpers\Html;
use dz\helpers\Url;
use dz\widgets\InputWidget;

class DzMarkdown extends InputWidget
{
    /**
     * @var array.  Markdown plugin options
     */
    public $options = [];


    /**
     * Vendor path (dist files)
     */
    public $vendorPath;


    /**
     * Initializes the widget
     */
    public function init()
    {
        // Vendor path
        if ( empty($this->vendorPath) )
        {
            $this->vendorPath = Yii::getAlias('@lib') . DIRECTORY_SEPARATOR . 'simplemde'. DIRECTORY_SEPARATOR .'dist';
        }

        // Assets
        if ( $this->assetsPath === NULL )
        {
            $this->assetsPath = $this->vendorPath;
        }
        
        if ( $this->assetsUrl === NULL )
        {
            $this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
        }
        
        // HTML Options
        list($this->name, $this->id) = $this->resolveNameID();
        if ( !isset($this->htmlOptions['id']) )
        {
            $this->htmlOptions['id'] = $this->id .'-markdown';
        }
    }


    /**
     * Runs the widget
     */
    public function run()
    {
        // Class
        if ( !isset($this->htmlOptions['class']) )
        {
            $this->htmlOptions['class'] = 'dz-markdown';
        }
        else
        {
            $this->htmlOptions['class'] .= ' dz-markdown';
        }

        echo '<div id="'. $this->id .'-wrapper" class="markdown-wrapper">';
        if ( $this->hasModel() )
        {
            echo Html::activeTextArea($this->model, $this->attribute, $this->htmlOptions);
        }
        else
        {
            echo Html::textArea($this->name, $this->value, $this->htmlOptions);
        }
        echo '</div>';

        // Javascript files
        $this->registerClientScript();
    }


    /**
     * Register required CSS and script files
     */
    protected function registerClientScript()
    {
        $cs = Yii::app()->getClientScript();
        // $cs->registerCssFile($this->vendor_path . DIRECTORY_SEPARATOR .'simplemde.min.css');
        // $cs->registerScriptFile($this->vendor_path . DIRECTORY_SEPARATOR .'simplemde.min.js');
        $cs->registerCssFile($this->assetsUrl .'/simplemde.min.css');
        $cs->registerScriptFile($this->assetsUrl .'/simplemde.min.js');
        
        
        $options = !empty($this->options) ? $this->options : [];
        $options['element'] = "document.getElementById('{$this->htmlOptions['id']}')";
        $options['spellChecker'] = !isset($options['spellChecker']) ? false : $options['spellChecker'];
        $options['status'] = !isset($options['status']) ? false : $options['status'];
        $options['hideIcons'] = !isset($options['hideIcons']) ? ['side-by-side', 'fullscreen'] : $options['hideIcons'];
        $options['autoDownloadFontAwesome'] = false;
        // $options['showIcons'] = !isset($options['showIcons']) ? ['code', 'table'] : $options['showIcons'];
        // $options['toolbarTips'] = !isset($options['toolbarTips']) ? true : $options['toolbarTips'];

        // Special minHeight option
        $min_height = 0;
        if ( isset($options['minHeight']) )
        {
            $min_height = $options['minHeight'];
            unset($options['minHeight']);
        }

        // Set default toolbar icons
        if ( !isset($options['toolbar']) )
        {
            $options['toolbar'] = [
                'bold', 'italic', 'heading', '|', 'quote', 'unordered-list', 'ordered-list', '|', 'link', 'image', '|', 'preview', '|',
                [
                    'name' => 'guide',
                    'action' => Url::to('help/markdown'),
                    'className' => "fa fa-question-circle"
                ]
            ];
        }

        foreach ($options as $key => $value)
        {
            if ( is_array($value) )
            {
                $options[$key] = $key . ': ' . CJSON::encode($value);
            }
            else if ( is_bool($value) )
            {
                $options[$key] = $key . ': ' . ($value ? 'true' : 'false');
            }
            else if (strpos($value, 'document') !== false || strpos($value, '$') !== false)
            {
                $options[$key] = $key . ': ' . $value;
            }
            else
            {
                $options[$key] = $key . ': "' . $value . '"';
            }
        }
        $js_options = implode(', ', $options);

        // Variable name = simplemde_<id>
        $js_var = "simplemde_". str_replace('-', '_', $this->htmlOptions['id']);

        // Javascript to enable SimpleMDE
        $cs->registerScript(__CLASS__ .'#'. $this->id, $js_var ." = new SimpleMDE({". $js_options ."});", CClientScript::POS_READY);

        // Custom height
        if ( $min_height > 0 )
        {
            $cs->registerScript(__CLASS__ .'#height_'. $this->id,
                "$({$js_var}.element).parent().children('.CodeMirror').css({minHeight: '{$min_height}px'});
                $({$js_var}.element).parent().children('.CodeMirror').children('.CodeMirror-scroll').css({minHeight: '{$min_height}px'});"
                , CClientScript::POS_READY);
        }

    }
}