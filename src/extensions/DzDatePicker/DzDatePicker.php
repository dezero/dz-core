<?php
/**
 * DzDatePicker widget class file
 *
 * This extension is a wrapper for Bootstrap Datepicker library
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2014 Dezero
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package extensions.DzDatePicker
 */

use dz\widgets\InputWidget;


/**
 * DzDatePicker widget
 * @see https://github.com/eternicode/bootstrap-datepicker
 */
class DzDatePicker extends InputWidget
{
	/**
	 * @var TbActiveForm when created via TbActiveForm, this attribute is set to the form that renders the widget
	 * @see TbActionForm->inputRow
	 */
	public $form;
	

	/**
	 * @var array the options for the Bootstrap JavaScript plugin.
	 */
	public $options = array();


	/**
	 * @var string[] the JavaScript event handlers.
	 */
	public $events = array();

	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		// Assets
		if ( $this->assetsPath === NULL )
		{
			$this->assetsPath = dirname(__FILE__) . DIRECTORY_SEPARATOR .'assets';
		}
		
		if ( $this->assetsUrl === NULL )
		{
			$this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
		}

		$this->htmlOptions['type'] = 'text';
		$this->htmlOptions['autocomplete'] = 'off';

		if ( !isset($this->options['language']) )
        {
			$this->options['language'] = Yii::app()->language;
        }

		if ( !isset($this->options['format']) )
        {
			$this->options['format'] = 'mm/dd/yyyy';
        }

		if ( !isset($this->options['weekStart']) )
        {
			$this->options['weekStart'] = 0; // Sunday
        }
	}

	/**
	 * Runs the widget.
	 */
	public function run()
	{
		list($name, $id) = $this->resolveNameID();

		echo $this->getPrepend();
		if ($this->hasModel())
		{
			if ( $this->form )
			{
				echo $this->form->textField($this->model, $this->attribute, $this->htmlOptions);
			}
			else
			{
				echo CHtml::activeTextField($this->model, $this->attribute, $this->htmlOptions);
			}

		}
		else
		{
			echo CHtml::textField($name, $this->value, $this->htmlOptions);
		}
		echo $this->getAppend();

		$this->registerClientScript($id);
	}

	/**
	 * Registers required client script for bootstrap datepicker. It is not used through bootstrap->registerPlugin
	 * in order to attach events if any
	 */
	public function registerClientScript($id)
	{
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile("{$this->assetsUrl}/css/datepicker.css");
		$cs->registerScriptFile("{$this->assetsUrl}/js/bootstrap-datepicker.js");

		if ( isset($this->options['language']) )
		{
			// Yii::app()->bootstrap->registerAssetJs('locales/bootstrap-datepicker.'.$this->options['language'].'.js', CClientScript::POS_END);
			$cs->registerScriptFile("{$this->assetsUrl}/js/locales/bootstrap-datepicker.". $this->options['language' ] .".js", CClientScript::POS_END);
		}
		$options = !empty($this->options) ? CJavaScript::encode($this->options) : '';

		ob_start();
		echo "jQuery('#{$id}').datepicker({$options})";
		foreach ($this->events as $event => $handler)
			echo ".on('{$event}', " . CJavaScript::encode($handler) . ")";

		Yii::app()->getClientScript()->registerScript(__CLASS__ . '#' . $this->getId(), ob_get_clean() . ';');

	}
}