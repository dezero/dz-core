<?php
/**
 * DzSwitch widget class file
 *
 * This extension is a wrapper for Bootstrap Switch 3 library
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2014 Dezero
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package extensions.DzSwitch
 */

use dz\helpers\Html;
use dz\widgets\InputWidget;

/**
 * DzSwitch widget
 * @see http://www.bootstrap-switch.org/
 */
class DzSwitch extends InputWidget
{
	/**
	 * @var TbActiveForm when created via TbActiveForm, this attribute is set to the form that renders the widget
	 * @see TbActionForm->inputRow
	 */
	public $form;
	

	/**
	 * @var array the options for the Bootstrap JavaScript plugin.
	 */
	public $options = array();


	/**
	 * @var string[] the JavaScript event handlers.
	 */
	public $events = array();


	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		// Assets
		if ( $this->assetsPath === NULL )
		{
			$this->assetsPath = dirname(__FILE__) . DIRECTORY_SEPARATOR .'assets';
		}
		
		if ( $this->assetsUrl === NULL )
		{
			$this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
		}
	}


	/**
	 * Runs the widget.
	 */
	public function run()
	{
		list($name, $id) = $this->resolveNameID();

		echo $this->getPrepend();
		if ($this->hasModel())
		{
			if ( $this->form )
			{
				echo $this->form->checkBox($this->model, $this->attribute, $this->htmlOptions);
			}
			else
			{
				echo Html::activeCheckBox($this->model, $this->attribute, $this->htmlOptions);
			}

		}
		else
		{
			echo Html::checkBox($name, $this->value, $this->htmlOptions);
		}
		echo $this->getAppend();

		$this->registerClientScript($id);
	}


	/**
	 * Registers required client script for bootstrap switch.
	 * It is not used through bootstrap->registerPlugin in order to attach events if any
	 */
	public function registerClientScript($id)
	{
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile("{$this->assetsUrl}/css/bs3/bootstrap-switch.min.css");
		$cs->registerScriptFile("{$this->assetsUrl}/js/bootstrap-switch.min.js");

		$options = !empty($this->options) ? CJavaScript::encode($this->options) : '';

		ob_start();
		echo "jQuery('#{$id}').bootstrapSwitch({$options})";
		foreach ($this->events as $event => $handler)
			echo ".on('{$event}', " . CJavaScript::encode($handler) . ")";

		Yii::app()->getClientScript()->registerScript(__CLASS__ . '#' . $this->getId(), ob_get_clean() . ';');
	}
}