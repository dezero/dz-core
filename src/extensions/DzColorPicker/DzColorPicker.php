<?php
/**
 * DzColorPicker widget class file
 *
 * This extension is a wrapper for Bootstrap Colorpicker library
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link http://www.dezero.es
 * @copyright Copyright &copy; 2015 Fabián Ruiz
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package extensions.DzColorPicker
 * @see http://mjolnic.com/bootstrap-colorpicker/
 */

use dz\helpers\Html;
use dz\widgets\InputWidget;

/**
 * DzColorPicker widget
 * @see http://mjolnic.com/bootstrap-colorpicker/
 */
class DzColorPicker extends InputWidget
{
	/**
	 * @var TbActiveForm when created via TbActiveForm, this attribute is set to the form that renders the widget
	 * @see TbActionForm->inputRow
	 */
	public $form;
	

	/**
	 * @var array the options for the Bootstrap JavaScript plugin.
	 */
	public $options = array();


	/**
	 * @var string[] the JavaScript event handlers.
	 */
	public $events = array();


	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		// Assets
		if ( $this->assetsPath === NULL )
		{
			$this->assetsPath = dirname(__FILE__) . DIRECTORY_SEPARATOR .'assets';
		}
		
		if ( $this->assetsUrl === NULL )
		{
			$this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
		}
	}


	/**
	 * Runs the widget.
	 */
	public function run()
	{
		list($name, $id) = $this->resolveNameID();

		echo '<div id="'. $id .'-group" class="input-group">';
		echo $this->getPrepend();
		if ($this->hasModel())
		{
			if ( $this->form )
			{
				echo $this->form->textField($this->model, $this->attribute, $this->htmlOptions);
			}
			else
			{
				echo Html::activeTextField($this->model, $this->attribute, $this->htmlOptions);
			}

		}
		else
		{
			echo Html::textField($name, $this->value, $this->htmlOptions);
		}
		echo '<span class="input-group-addon"><i></i></span>';
		echo $this->getAppend();
		echo '</div>';

		$this->registerClientScript($id);
	}


	/**
	 * Registers required client script for bootstrap switch.
	 * It is not used through bootstrap->registerPlugin in order to attach events if any
	 */
	public function registerClientScript($id)
	{
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile("{$this->assetsUrl}/css/bootstrap-colorpicker.min.css");
		$cs->registerScriptFile("{$this->assetsUrl}/js/bootstrap-colorpicker.min.js");

		$options = !empty($this->options) ? CJavaScript::encode($this->options) : '';

		ob_start();
		echo "jQuery('#{$id}-group').colorpicker({$options})";
		foreach ($this->events as $event => $handler)
			echo ".on('{$event}', " . CJavaScript::encode($handler) . ")";

		Yii::app()->getClientScript()->registerScript(__CLASS__ . '#' . $this->getId(), ob_get_clean() . ';');
	}
}