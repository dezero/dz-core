/**
 * Custom jQuery Batch Operation library developed by Dezero
 */
(function ($) {
    $.dzBatch = {
		url: '',
		operations: [],
		pending_operations: [],
		current_operation: 1,
		total_operations: 1,
		loading_img: '',
		modal_title: 'Cargando...',
		modal_suffix: '<p>Este proceso puede durar varios minutos. <span class="text-danger">Por favor, no refresque la página!</span></p>',
		$modal: {},

		// Callbacks
		after_save: function(data){
			this.$modal.modal("hide");
		},

		init: function() {
			var self = this;

			if ( self.url !== "" ) {
				self.current_operation = 1;
				
				// Modal init
				$('.dz-modal-title', self.$modal).html(self.modal_title);
				$('.dz-modal-body', self.$modal).html("<h4>Inicializando...</h4>"+ self.modal_suffix);
				self.$modal.modal({
					show: true,
					// modalOverflow: true,
					width: 600
				}).width(600);

				// Loading image
				self.loading_img = "<img src='"+ window.js_globals.base_url +"/themes/backend/images/loading.gif' width='20' height='20' class='batch-loading' />";

				// First AJAX call - Get all operations
				$.ajax({
					url: self.url,
					dataType: 'json',
					cache: false,
					type: 'post',
					data: {operation: 'init'},
					success: function(data) {
						// console.log(data);
						if ( data.result == 'success' ) {
							self.operations = data.operations;
							self.pending_operations = self.operations;
							if ( self.pending_operations.length > 0 ) {
								self.total_operations = self.operations.length;
								var que_operation = self.pending_operations.shift();
								if ( que_operation.hasOwnProperty("operation") ) {
									var que_output = "";
									if ( que_operation.hasOwnProperty("description") ) {
										que_output = "<h4>"+ self.loading_img +" <span class='batch-description'>"+ que_operation.description +" "+ que_output + "</span></h4>";
									}
									que_output += self.modal_suffix;
									$('.dz-modal-body', self.$modal).html(que_output);
									$('.dz-modal-title', self.$modal).html(self.modal_title + " ("+ self.current_operation +"/"+ self.total_operations +")");
									self.run_batch(que_operation.operation, data.batch_id);
								}
							}
						} else {
							// alert(data.message);
							$('.dz-modal-title', self.$modal).html("ERROR / ADVERTENCIA");
							$('.dz-modal-body', self.$modal).html("<h3>"+ data.message +"</h3>");
						}
					},
					error: function(request, status, error) {
						alert('ERROR: '+request.responseText);
					}
				});
			}
		},

		run_batch: function(batch_operation, batch_id) {
			var self = this;
			if ( self.url !== "" ) {
				$.ajax({
					url: self.url,
					dataType: 'json',
					cache: false,
					type: 'post',
					data: {
						operation: batch_operation,
						batch_id: batch_id
					},
					success: function(data) {
						// console.log(data);
						if ( data.result == 'success' ) {
							if ( self.pending_operations.length > 0 ) {
								self.current_operation++;
								var que_operation = self.pending_operations.shift();
								if ( que_operation.hasOwnProperty("operation") ) {
									var que_output = "";
									if ( que_operation.hasOwnProperty("description") ) {
										que_output = "<h4>"+ self.loading_img +" <span class='batch-description'>"+ que_operation.description +" "+ que_output + "</span></h4>";
									}
									que_output += self.modal_suffix;
									$('.dz-modal-body', self.$modal).html(que_output);
									$('.dz-modal-title', self.$modal).html(self.modal_title +" ("+ self.current_operation +"/"+ self.total_operations +")");
									self.run_batch(que_operation.operation, batch_id);
								}
							}
							else {
								data.operation = batch_operation;
								data.batch_id = batch_id;
								self.after_save(data);
							}
						} else {
							// alert(data.message);
							$('.dz-modal-title', self.$modal).html("ERROR / ADVERTENCIA");
							$('.dz-modal-body', self.$modal).html("<h3>"+ data.message +"</h3>");
						}
					},
					error: function(request, status, error) {
						alert('ERROR: '+request.responseText);
					}
				});
			}
		}
	};
})(jQuery);