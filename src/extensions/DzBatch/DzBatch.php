<?php
/**
 * DzBatch widget class file
 *
 * @author Fabián Ruiz <fabian@dezero.es>
 * @link http://dezero.es/
 * @copyright Copyright &copy; 2015 Dezero
 */

use dz\widgets\Modal;

/**
 * Custom DzBatch widget
 */
class DzBatch extends CWidget
{	
	/**
	 * File path to assets
	 *
	 * @var string
	 */
	protected $assetsPath;


	/**
	 * URL to assets
	 *
	 * @var string
	 */
	protected $assetsUrl;
	
	/**
	 * Jasny File Upload plugin options
	 *
	 * @var array
	 */
	public $options = array();


	/**
	 * HTML attributes for the widget container
	 * 
	 * @var array
	 */
	public $htmlOptions = array();
	
	
	/**
	 * Initializes the widget
	 */
	public function init()
	{
		// Assets
		if ( $this->assetsPath === NULL )
		{
			$this->assetsPath = dirname(__FILE__) . DIRECTORY_SEPARATOR .'assets';
		}
		
		if ( $this->assetsUrl === NULL )
		{
			$this->assetsUrl = Yii::app()->assetManager->publish($this->assetsPath);
		}

		if ( !isset($this->htmlOptions['id']) )
		{
			$this->htmlOptions['id'] = $this->getId();
		}
	}
	
	
	/**
	 * Runs the widget
	 */
	public function run()
	{
		// MODAL - Load modal
		Modal::createModal($this->htmlOptions['id'].'-batch-results-modal', array(
			'fullWidth' => TRUE,
			'submitHtmlOptions' => array(
				'class' => 'dz-modal-submit-button dz-modal-button btn-primary hide'
			)
		));
		$this->options['modal'] = $this->htmlOptions['id'].'-batch-results-modal';

		// Javascript files
		$this->registerClientScript();
	}


	/**
	 * Register required Javascript files
	 */
	protected function registerClientScript()
	{
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile("{$this->assetsUrl}/js/dz-batch.js");
		
		$js_code = "$.dzBatch.url = '". $this->options['url'] ."'; $.dzBatch.\$modal = $('#". $this->htmlOptions['id'] ."-batch-results-modal');";
		if ( isset($this->options['modal_title']) )
		{
			$js_code .= "$.dzBatch.modal_title = '". $this->options['modal_title'] ."';";
		}
		Yii::app()->getClientScript()->registerScript(__CLASS__.'#dz-batch-'. $this->htmlOptions['id'], $js_code, CClientScript::POS_READY);
	}
}