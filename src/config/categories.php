<?php
/**
 * Categories configuration
 * 
 * Load it via Yii::app()->config->get('components.categories')
 */
return [
    // Type "category"
    'category' => [
        'is_editable' => true,
        'is_multilanguage' => true,
        'is_description' => true,
        'is_image' => true,
        'is_seo_fields' => true,
        'is_disable_allowed' => true,
        'is_first_level_sortable' => true,
        'is_delete_allowed' => true,
        'max_levels' => 3,

        // Custom path for images
        'images_path' => 'www' . DIRECTORY_SEPARATOR .'files'. DIRECTORY_SEPARATOR .'images'. DIRECTORY_SEPARATOR .'category'. DIRECTORY_SEPARATOR,
    ],

    // Type "tag"
    'tag' => [
        'is_editable' => true,
        'is_multilanguage' => false,
        'is_description' => false,
        'is_image' => false,
        'is_seo_fields' => false,
        'is_disable_allowed' => true,
        'is_first_level_sortable' => false,
        'is_delete_allowed' => true,
        'max_levels' => 1,
    ],
];