<?php
/**
 * Yii Aliases
 */
return [
    // Yii 1 predefined aliases
    'application'       => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app',
    'webroot'           => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'www',
    'ext'               => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'src'. DIRECTORY_SEPARATOR . 'extensions',

    // Application aliases
    'app'               => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app',
    '@app'              => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app',
    '@core'             => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'core',
    '@dz'               => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'src',
    '@lib'              => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'lib',
    '@bootstrap'        => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR .'yii-bootstrap-3',
    '@ext'              => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'src'. DIRECTORY_SEPARATOR . 'extensions',
    '@modules'          => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'modules',
    '@vendor'           => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'vendor',
    '@storage'          => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'storage',
    '@logs'             => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'logs',
    '@privateTmp'       => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'tmp',

    // Theming aliases      
    '@www'              => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'www',
    '@assets'           => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR . 'assets',
    '@tmp'              => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR . 'tmp',
    '@theme'            => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR . 'themes' . DIRECTORY_SEPARATOR . 'backend',
    // '@backend_theme'     => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR . 'themes' . DIRECTORY_SEPARATOR . 'backend',
    // '@frontend_theme'  => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR . 'themes' . DIRECTORY_SEPARATOR . 'frontend',

    // Modules
    'user'              => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'user',
    'auth'              => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'auth',
    'asset'             => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'asset',
    'admin'             => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'admin',
    'category'          => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'category',
    'settings'          => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'settings',
    'web'               => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'web',
    'api'               => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'api',

    // Custom modules
    // 'frontend'          => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'frontend',
];