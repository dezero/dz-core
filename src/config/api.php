<?php
/**
 * API configuration
 * 
 * Load it via Yii::app()->config->get('components.api')
 */
return [
    // Default API configuration
    'dz' => [
        // Allowed values: file (log file), db (database)
        'log_destination'   => 'file', // 'db',

        // Authorization is needed
        'auth' => false,
        // 'auth' => [
        //     'prefix'    => 'Bearer',
        //     'token'     => 'YOUR_TOKEN_HERE'
        // ],

        // Allowed hosts
        'allowed_hosts'  => [],
        // 'allowed_hosts'  => ['http://myproject.local', 'https://myproject.local' ,'http://192.168.1.3']
    ],
];
