<?php
/**
 * Site URL rules
 */
return[
    'gii'                           => 'gii',

    // Front page
    ''                              => 'user/login',

    // API path rules
    // 'api/v1/<controller:\w+>' => 'api/<controller>',
    // 'api/v1/<controller:\w+>/<id:\w+>' => 'api/<controller>/item',
    // 'api/v1/<controller:\w+>/<id:\w+>/<action:\w+>' => 'api/<controller>/<action>',
    // 'api/v1/<controller:\w+>/<id:\w+>/<action:\w+>/<entity_id:\w+>' => 'api/<controller>/<action>',
    // 'api/v1/<controller:\w+>/<action:\w+>' => 'api/<controller>',

    // '<action:\w+>' => 'frontend/<action>',
    '<controller:\w+>/<id:\d+>'     => '<controller>/view',
    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
    // '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',

    // Finally, slug (SEO URL)
    '<slug:[a-zA-Z0-9-]+>'          => 'frontend/seo',
];