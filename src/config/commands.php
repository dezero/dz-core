<?php
/**
 * CommandMap definition
 *
 * Load it via Yii::app()->config->get('components.commands')
 */
return [
    // This command is used to check data consistency in auth module
    'authInspector' => [
        'class' => '\dz\modules\auth\commands\AuthInspectorCommand',
    ],

    // This command is used to clear API logs
    'apiLog' => [
        'class' => '\dz\modules\api\commands\ApiLogCommand'
    ],

    // This command is used to perform backups
    'backup' => [
        'class' => '\dz\modules\admin\commands\BackupCommand',
    ],

    // This component is used to work with translations
    'i18n' => [
        'class' => '\dz\modules\settings\commands\I18nCommand'
    ],

    // This command is used to work with images
    'image' => [
        'class' => '\dz\modules\asset\commands\ImageCommand'
    ],

    // This command is used to check data consistency in asset module
    'imageInspector' => [
        'class' => '\dz\modules\asset\commands\ImageInspectorCommand'
    ],

    // This command is used to send emails
    'mail' => [
        'class' => '\dz\modules\admin\commands\MailCommand',
    ],

    // This command is used to manage product search results
    'productSearch' => [
        'class' => '\dzlab\commerce\commands\ProductSearchCommand'
    ],

    // This command is used to work with SEO models
    'seo' => [
        'class' => '\dz\modules\settings\commands\SeoCommand'
    ],

    // This command is used to generate Sitemaps
    'sitemap' => [
        'class' => '\dz\modules\web\commands\SitemapCommand'
    ],
];