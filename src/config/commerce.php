<?php
/**
 * Commerce configuration
 * 
 * Load it via Yii::app()->config->get('components.commerce')
 */
return [    
    // Redsys TPV configuration
    'redsys' => [
        // Connection URL
        'live_url' => 'https://sis.redsys.es/sis/realizarPago',

        // Connection URL for testing
        'test_url' => 'https://sis-t.redsys.es:25443/sis/realizarPago',

        // Número de comercio
        'merchant_code' => '',

        // Número de terminal
        'terminal' => '001',

        // Código divisa operación (978 = €)
        'currency_code' => '978',

        // Clave secreta de encriptación
        'signature_key' => '',

        // Clave secreta de encriptación (TESTNG mode)
        'signature_key_test' => 'sq7HjrUOBfKmC576ILgskD5srU870gJ7',
        
        // Versión de la firma que se está utilizando
        'signature_version' => 'HMAC_SHA256_V1',

        // URL for notifications (TPV response)
        'merchant_url'      => '/checkout/redsys',

        // Redirect URL if OK.
        // It will receive $order_id as required input parameter
        'redirect_ok_url'   => '/checkout/finish',

        // Redirect URL if wrong
        // It will receive $order_id and $response_type = 'tpv_error' as required input parameters
        'redirect_ko_url'   => '/checkout',

        // Prefix to use when order_id is enconded
        'order_prefix' => 'dz',

        // TEST mode?
        // This option will be overrided by the value added into "params.php" local configuration file
        'is_test'  => true,
    ]
];